OJT = {
	processor: './NYSUS_OJT/mes/repetitive/ajax_processors/ojt.php',
	modal: './NYSUS_OJT/mes/repetitive/templates/ojt.html #div_ojt_modal',
	nav: './NYSUS_OJT/mes/repetitive/templates/ojt.html #div_ojt',
	pdfjs_path: './NYSUS_OJT/vendor//pdfjs/web/viewer.html?file='
};

$(document).ready(function () {
	OJT.init();
});

OJT.init = function () {
	$("#div_alerts").replaceWith($('<div>').load(OJT.nav, OJT.afterReplaceButtons));
};

OJT.afterReplaceButtons = function () {
	$('body').append($('<div>').load(OJT.modal, OJT.afterLoadModal));
};

OJT.afterLoadModal = function () {
	var ojt_modal = $('#div_ojt_modal');

	ojt_modal.on('shown.bs.modal', function () {
		fullscreenModal(ojt_modal);
	});

	OJT.refresh();
	setInterval(OJT.refresh, 5000);

};

OJT.refresh = function () {

	if (!all_data || !all_data.machine || all_data.machine.length == 0) {
		return;
	}
	if (operator.ID == null) {
		all_data.ojt_requirements = [];
		all_data.ojt_unsigned_alert = [];
		all_data.ojt_unsigned_requirement = [];
		return;
	}

	$.getJSON(OJT.processor, {"action": 'get_data', "machine_ID": all_data.machine[0].ID}, function (jso) {

		if (jso) {
			all_data.references = jso.references;
			all_data.ojt_alerts = jso.ojt_alerts;
			all_data.ojt_requirements = jso.ojt_requirements;
			all_data.ojt_unsigned_alert = jso.ojt_unsigned_alert;
			all_data.ojt_unsigned_requirement = jso.ojt_unsigned_requirement;
		} else {
			all_data.references = [];
			all_data.ojt_alerts = [];
			all_data.ojt_requirements = [];
			all_data.ojt_unsigned_alert = [];
			all_data.ojt_unsigned_requirement = [];
		}

		$('.btn-references').prop('disabled', (!all_data || !all_data.references || all_data.references.length == 0));
		$('.btn-ojt-alerts').prop('disabled', (!all_data || !all_data.ojt_alerts || all_data.ojt_alerts.length == 0));
		$('.btn-ojt-requirements').prop('disabled', (!all_data || !all_data.ojt_requirements || all_data.ojt_requirements.length == 0));
		$('.btn-ojt-alerts').find('.badge').html(all_data && all_data.ojt_alerts ? all_data.ojt_alerts.length : 0);
		$('.btn-ojt-requirements').find('.badge').html(all_data && all_data.ojt_requirements ? all_data.ojt_requirements.length : 0);

		OJT.signoffNextRequirement();
	});
};

OJT.display = function (data) {
	var ojt_modal = $('#div_ojt_modal');
	var ojt_modal_left = $('#div_ojt_modal_left');
	var ojt_modal_right = $('#div_ojt_modal_right');
	var ul = $('<ul />');

	$('.btn_ojt_close').prop('disabled', false);

	function createTree(a) {
		var markup = '';
		$.each(a, function (k, v) {
			if (v[0] == "directory") {
				markup += '<li>' + v[1];
				markup += '<ul>' + createTree(v[2]) + '</ul>';
				markup += '</li>';
			} else {
				markup += '<li data-path="' + v[2] + '" data-text="' + v[3] + '">' + v[1] + '</li>';
			}
		});
		return markup;
	}

	var nodes = createTree(data);

	ul.append(nodes);
	ojt_modal_left.jstree("destroy");
	ojt_modal_left.empty().append(ul);
	ojt_modal_left.jstree();
	ojt_modal_left.unbind('select_node.jstree');
	ojt_modal_right.find('.media').empty();
	ojt_modal_right.find('.caption').empty();
	ojt_modal_left.on('select_node.jstree', function (e, data) {
		var ojt_modal = $('#div_ojt_modal');
		var ojt_modal_left = $('#div_ojt_modal_left');
		var ojt_modal_right = $('#div_ojt_modal_right');
		var caption = ojt_modal_right.find('.caption');
		var media = ojt_modal_right.find('.media');
		
		OJT.displayFile(media, data.node.data.path);
		caption.html(data.node.data.text);
		
	});

	ojt_modal.modal('show');

};

OJT.displayFile = function (el, uri) {	
	var markup = '<div class="alert alert-danger">CONTENT NOT AVAILABLE FOR THIS SELECTION!</div>';
	
	

	function isImage(url) {
		return (url.match(/\.(jpeg|jpg|gif|png)$/i) != null);
	}

	function isPDF(url) {
		return (url.match(/\.(pdf)$/i) != null);
	}

	function isVideo(url) {
		return (url.match(/\.(mp4|mp3|mov)$/i) != null);
	}

	if (uri && isImage(uri)) {
		markup = '<img class="img-responsive center-block" src="' + uri + '" alt="NO IMAGE FOUND" />';
	}

	if (uri && isPDF(uri)) {
		// NEED TO USE PDFJS FOR ELECTRON SUPPORT
		markup = '<iframe style="width:100%; height:600px" src="' + OJT.pdfjs_path + uri + '"></iframe>';
	}

	if (uri && isVideo(uri)) {
		markup = '<video controls autoplay><source src="' + uri + '"></video>';
	}

	el.html(markup);
};

OJT.signoffNextRequirement = function () {
	var ojt_modal = $('#div_ojt_modal');
	var ojt_modal_title = ojt_modal.find('.modal-title');
	var ojt_modal_left = $('#div_ojt_modal_left');
	var ojt_modal_right = $('#div_ojt_modal_right');
	var caption = ojt_modal_right.find('.caption');
	var media = ojt_modal_right.find('.media');
	var reqs = $.merge(all_data.ojt_unsigned_alert, all_data.ojt_unsigned_requirement);

	if (reqs.length > 0 && !ojt_modal.hasClass('in')) {
		var req = reqs[0];
		var role = req.role_name.toUpperCase();		

		ojt_modal.find('.btn_ojt_close').prop('disabled', true);
		ojt_modal_left.jstree("destroy");
		ojt_modal_title.html('SIGNOFF THIS REQUIREMENT!!');

		if (OJT.hasRole(req.role_ID)) {
			ojt_modal_left.empty().html('<h3>REVIEW THE REQUIREMENT AND CLICK TO CONFIRM THE SIGNOFF.</h3><button class="btn btn-warning" onclick="OJT.doSignoff('
				+ req.ojt_requirement_ID + ','
				+ req.ojt_signoff_template_levels_roles_ID + ','
				+ operator.ID
				+ ')">CONFIRM SIGNOFF</button>');

		} else {
			ojt_modal_left.empty().html('<h3>' + role + '</h3><b>ROLE REQUIRED FOR SIGNOFF. <br>HAVE OPERATOR REVIEW THE REQUIREMENT THEN PLEASE SCAN A <span class="highlight">' + role + '</span> BADGE TO CONFIRM.<br></b><h3><div id="signoff_operator"></div></h3>');

			KEYS.resetKeys();
			KEYS.registerKeyHandler({
				"name": "scan_badge",
				"global": false,
				"match_keys": null,
				"display_el_id": "signoff_operator",
				"mask_character": "*",
				callback: function (scan) {
					OJT.doSignoff(req.ojt_requirement_ID, req.ojt_signoff_template_levels_roles_ID, scan);
				}
			});			

		}
		
		if (caption) {
			caption.html('<p>' + req.requirement_text + '</p>');
		}

		if (media) {
			OJT.displayFile(media, req.image_url);
		}

		ojt_modal.modal('show');

	}

};

OJT.hasRole = function (role_ID) {
	var valid = false;
	if (operator.roles) {
		$.map(operator.roles, function (v) {
			if (v.role_ID == role_ID) {
				valid = true;
				return;
			}
		});
	}
	return valid;
};

OJT.doSignoff = function (ojt_requirement_ID, ojt_signoff_template_levels_roles_ID, signoff_operator_ID) {
	$.ajax(OJT.processor, {
		"data": {
			"action": "signoff_requirement",
			"ojt_requirement_ID": ojt_requirement_ID,
			"ojt_signoff_template_levels_roles_ID": ojt_signoff_template_levels_roles_ID,
			"operator_ID": operator.ID,
			"signoff_operator_ID": signoff_operator_ID
		},
		"dataType": "json",
		"method": "POST",
		"success": OJT.afterDoSignoff
	});
};

OJT.afterDoSignoff = function (jso) {
	var ojt_modal = $('#div_ojt_modal');
	var ojt_modal_left = $('#div_ojt_modal_left');
	var ojt_modal_right = $('#div_ojt_modal_right');
	var caption = ojt_modal_right.find('.caption');

	if (jso.result == 'SUCCESS') {
		ojt_modal_left.empty();
		caption.html('<div class="alert alert-success">SIGNOFF COMPLETE!!!</div>');
		setTimeout(function () {
			ojt_modal.modal('hide');
		}, 1000);
	} else {
		ojt_modal_left.append('<div class="alert alert-danger">' + jso.result + '</div>');
	}
};