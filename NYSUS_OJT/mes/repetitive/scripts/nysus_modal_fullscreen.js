/**
 * Created by sdaviduk on 5/17/2017.
 */

fullscreenModal = function (modal) {
	var modal = $(modal);

	var setHeight = function () {
		var dialog = modal.find('.modal-dialog');
		var header = modal.find('.modal-header');
		var body = modal.find('.modal-body');
		var footer = modal.find('.modal-footer');
		var dialog_margins = 0;
		var head_height = 0;
		var foot_height = 0;
		var height = 0;

		if (dialog) {
			dialog_margins = parseInt(dialog.css('margin-top').replace('px', '')) + parseInt(dialog.css('margin-bottom').replace('px', ''));
		}
		if (header) {
			var head_padding = parseInt(header.css('padding-top').replace('px', '')) + parseInt(header.css('padding-bottom').replace('px', ''));
			head_height = Math.abs(header.height() + head_padding);
		}
		if (footer) {
			var footer_padding = parseInt(footer.css('padding-top').replace('px', '')) + parseInt(footer.css('padding-bottom').replace('px', ''));
			foot_height = Math.abs(footer.height() + footer_padding);
		}
		height = modal.height() - (head_height + foot_height + dialog_margins);

		body.css('height', height);

	};

	$(window).on('resize', function () {
		console.log('resizing modal');
		setHeight();
	});

	modal.on('show.bs.modal', function () {
		console.log('resizing modal');
		setHeight();
	});

	modal.on('hide.bs.modal', function () {
		$(window).off('resize');
	});

	setHeight();
};
