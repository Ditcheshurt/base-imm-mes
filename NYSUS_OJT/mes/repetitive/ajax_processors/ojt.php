<?php

include_once("init.php");
$action = $_REQUEST['action'];

if ($action == "get_data") {
	$machine_ID = $_REQUEST['machine_ID'];
	$res = new StdClass();
	$res->references = array();
	$res->ojt_requirements = array();
	$res->ojt_alerts = array();
	$res->ojt_unsigned_alert = array();
	$res->ojt_unsigned_requirement = array();

	if (isset($GLOBALS['APP_CONFIG']['use_references']) && $GLOBALS['APP_CONFIG']['use_references'] == true) {
		if (isset($GLOBALS['APP_CONFIG']['reference_files_path'])) {
			$res->references = dirToArray($GLOBALS['APP_CONFIG']['base_dir'].$GLOBALS['APP_CONFIG']['reference_files_path']);
		}
	}

	// use ojt true||false
	if (isset($GLOBALS['APP_CONFIG']['use_ojt']) && $GLOBALS['APP_CONFIG']['use_ojt'] == true) {

		// use machine_ID to get ojt_group_ID and current_operator_id
		if ($machine_ID != null && $machine_ID != '') {

			$sql = "SELECT TOP 1 ojt_group_ID, current_operator_ID FROM NYSUS_REPETITIVE_MES.dbo.machines WHERE ID = $machine_ID" ;
			$r = $db->query($sql);

			$ojt_group_ID = $r[0]['ojt_group_ID'];
			$current_operator_ID = $r[0]['current_operator_ID'];

			if ($GLOBALS['APP_CONFIG']['use_requirements'] == true) {
				$sql = "SELECT * FROM MES_COMMON.dbo.fn_getOJTOperatorRequirements($current_operator_ID, 0)";
				$r = $db->query($sql);
				if ($r && count($r) > 0) {
					$arr = array();
					foreach ($r as $key => $value) {
						array_push($arr, array("file", $value["requirement_description"], "//" . $_SERVER['HTTP_HOST'] . "/" . $value["image_url"], $value["requirement_text"]));
					}
					$res->ojt_requirements = $arr;
				} else {
					$res->ojt_requirements = array();
				}
			}

			if ($GLOBALS['APP_CONFIG']['use_alerts'] == true) {
				$sql = "SELECT * FROM MES_COMMON.dbo.fn_getOJTOperatorAlerts($current_operator_ID)";

				$r = $db->query($sql);
				if ($r && count($r) > 0) {
					$arr = array();
					foreach ($r as $key => $value) {
						array_push($arr, array("file", $value["requirement_description"], "//" . $_SERVER['HTTP_HOST'] . "/" . $value["image_url"], $value["requirement_text"]));
					}
					$res->ojt_alerts = $arr;
				} else {
					$res->ojt_alerts = array();;
				}
			}

			if ($GLOBALS['APP_CONFIG']['use_alerts'] == true) {
				$sql = "SELECT * FROM MES_COMMON.dbo.fn_getOJTNextUnsignedRequirement($ojt_group_ID, $current_operator_ID,'alert')";

				$r = $db->query($sql);
				if ($r && count($r) > 0) {
					$arr = array();
					foreach ($r as $key => $value) {
						$value["image_url"] = "//" . $_SERVER['HTTP_HOST'] . "/" . $value["image_url"];
						array_push($arr, $value);
					}
					$res->ojt_unsigned_alert = $arr;
				} else {
					$res->ojt_unsigned_alert = array();
				}
			}

			if ($GLOBALS['APP_CONFIG']['use_requirements'] == true) {
				$sql = "SELECT * FROM MES_COMMON.dbo.fn_getOJTNextUnsignedRequirement($ojt_group_ID, $current_operator_ID,'job')";

				$r = $db->query($sql);
				if ($r && count($r) > 0) {
					$arr = array();
					foreach ($r as $key => $value) {
						$value["image_url"] = "//" . $_SERVER['HTTP_HOST'] . "/" . $value["image_url"];
						array_push($arr, $value);
					}
					$res->ojt_unsigned_requirement = $arr;
				} else {
					$res->ojt_unsigned_requirement = array();
				}
			}

		} // end use machine_ID to get ojt_group_ID and current_operator_id

	} // end use_ojt true||false

	echo json_encode($res);
}

if ($action == "signoff_requirement") {
	$ojt_requirement_ID = $_REQUEST["ojt_requirement_ID"];
	$ojt_signoff_template_levels_roles_ID = $_REQUEST["ojt_signoff_template_levels_roles_ID"];
	$operator_ID = $_REQUEST["operator_ID"];
	$signoff_operator_ID = $_REQUEST["signoff_operator_ID"];

	// validate signoff_operator has role_ID
	$sql = "
				SELECT 
					o.ID 
				FROM MES_COMMON.dbo.ojt_requirements req
					JOIN MES_COMMON.dbo.ojt_signoff_templates st ON st.ID = req.ojt_signoff_template_ID
					JOIN MES_COMMON.dbo.ojt_signoff_template_levels stl ON stl.ojt_signoff_template_ID = st.ID
					JOIN MES_COMMON.dbo.ojt_signoff_template_levels_roles stlr ON stlr.ojt_signoff_template_level_ID = stl.ID
					JOIN MES_COMMON.dbo.roles r ON stlr.role_ID = r.ID
					JOIN MES_COMMON.dbo.operator_roles opr ON opr.role_ID = r.ID
					JOIN MES_COMMON.dbo.operators o ON o.ID = opr.operator_ID
				WHERE 
					req.ID = $ojt_requirement_ID
					AND stlr.ID = $ojt_signoff_template_levels_roles_ID
					AND (o.ID = $signoff_operator_ID or o.badge_ID = '$signoff_operator_ID')";

	$res = $db->query($sql);

	if ($res == null || count($res) == 0) {
		die('{"result": "OPERATOR DOES NOT HAVE REQUIRED ROLE"}');
	}

	$sql = "INSERT INTO MES_COMMON.dbo.ojt_signoffs
						(
							ojt_requirement_ID,
							ojt_signoff_template_levels_roles_ID,
							operator_ID,
							signoff_operator_ID
						)
						VALUES 
						(
							$ojt_requirement_ID
							,$ojt_signoff_template_levels_roles_ID
							,$operator_ID
							,$signoff_operator_ID
						)";

	$res = $db->query($sql);

	die('{"result": "SUCCESS"}');
}

function dirToArray($dir)
{

	if (!file_exists($dir)) {
		return;
	}

	$result = array();

	$cdir = scandir($dir);
	foreach ($cdir as $key => $value) {
		if (!in_array($value, array(".", ".."))) {
			if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
				array_push($result, array("directory", $value, dirToArray($dir . DIRECTORY_SEPARATOR . $value)));
			} else {
				$uri = str_replace("C:\\inetpub\\wwwroot\\", "//" . $_SERVER['HTTP_HOST'] . "/", $dir);
				$uri = str_replace("\\", "/", $uri);
				$uri .= "/" . $value;

				array_push($result, array("file", $value, $uri));
			}
		}
	}

	return $result;
}