<!--#include file="../../../../common/dbConn_MESCOMMON.asp" -->
<!--#include file="../../../../common/library/JSON_2.0.2a.asp" -->
<%
	Response.ContentType = "application/json"

	action = Request("action")
	database = Request("database")
	do_sql_log = False

	Select Case action '{

	    Case "get_ojt_group_ID"
	        process_type = Request("process_type")
	        line_ID = Request("line_ID")
            station_ID = Request("station_ID")

			strSQL = "SELECT 'failed' as status"
			
            if process_type = "SEQUENCED" or process_type = "SEQUENCE" then
                strSQL = "SELECT ojt_group_ID FROM " & database & ".dbo.line_stations WHERE line_ID = " & line_ID & " AND station_ID = " & station_ID
            end if

            if process_type = "BATCH" then
                strSQL = "SELECT ojt_group_ID FROM " & database & ".dbo.stations WHERE cell_ID = " & line_ID & " AND ID = " & station_ID
            end if
			
            QueryToJSON(oConn, strSQL).Flush

		Case "get_next_unsigned_requirement"
            ojt_group_ID = Request("ojt_group_ID")
            operator_ID = Request("operator_ID")
            requirement_type = Request("requirement_type")

            strSQL = "SELECT * FROM dbo.fn_getOJTNextUnsignedRequirement(" & ojt_group_ID & ", " & operator_ID & ", '" & requirement_type & "')"

            QueryToJSON(oConn, strSQL).Flush

		Case "validate_operator"
		    ojt_group_ID = Request("ojt_group_ID")
            operator_ID = Request("operator_ID")

            strSQL = "SELECT * FROM dbo.fn_getOJTValidGroupOperator(" & ojt_group_ID & ", " & operator_ID & ")"

            QueryToJSON(oConn, strSQL).Flush

        Case "get_signoff_roles"
            ojt_requirement_ID = Request("ojt_requirement_ID")

            strSQL = "SELECT r.ID AS ojt_requirement_ID," &_
                     " 	stl.level_order," &_
                     "	stl.level_description," &_
                     "  stlr.ID AS ojt_signoff_template_levels_roles_ID," &_
                     " 	stlr.role_order," &_
                     " 	rle.name AS role_description," &_
                     " 	rle.ID AS role_ID" &_
                     " FROM" &_
                     " 	ojt_requirements r" &_
                     " 	JOIN ojt_signoff_templates st ON st.ID = r.ojt_signoff_template_ID" &_
                     " 	JOIN ojt_signoff_template_levels stl ON st.ID = stl.ojt_signoff_template_ID" &_
                     " 	JOIN ojt_signoff_template_levels_roles stlr ON stl.ID = stlr.ojt_signoff_template_level_ID" &_
                     " 	JOIN roles rle ON rle.ID = stlr.role_ID" &_
                     " WHERE" &_
                     " 	r.ID = " & ojt_requirement_ID &_
                     " 	AND allow_mes_signoff = 1" &_
                     " 	AND stlr.ID NOT IN (SELECT ojt_signoff_template_levels_roles_ID FROM ojt_signoffs WHERE ojt_requirement_ID = " & ojt_requirement_ID & " AND ojt_signoff_template_levels_roles_ID = stlr.ID AND (expire_date >= getDate() OR expire_date IS NULL))" &_
                     " ORDER BY level_order, role_order"
'response.write strSQL
'response.end
            QueryToJSON(oConn, strSQL).Flush

        Case "do_signoff"
            ojt_requirement_ID = Request("ojt_requirement_ID")
            ojt_signoff_template_levels_roles_ID = Request("ojt_signoff_template_levels_roles_ID")
            operator_ID = Request("operator_ID")
            signoff_operator_ID = Request("signoff_operator_ID")

            strSQL = "INSERT INTO ojt_signoffs (ojt_requirement_ID, ojt_signoff_template_levels_roles_ID, operator_ID, signoff_operator_ID, created_date, expire_date) " &_
                "VALUES (" & ojt_requirement_ID & ", " & ojt_signoff_template_levels_roles_ID & ", " & operator_ID & ", " & signoff_operator_ID & ", getDate(), " &_
                " (SELECT CASE WHEN expire_days IS NOT NULL THEN DATEADD(dd, expire_days, getDate()) ELSE NULL END FROM ojt_requirements WHERE ID = " & ojt_requirement_ID & "))"

            oConn.Execute strSQL
            Response.Write "{""success"":1}"

        Case "validate_operator_has_role"
            operator_ID = Request("operator_ID")
            role_ID = Request("role_ID")

            strSQL = "SELECT CASE WHEN (SELECT ID FROM operator_roles WHERE operator_ID = " & operator_ID & " AND role_ID = " & role_ID & ") IS NULL THEN 0 ELSE 1 END AS success"

            QueryToJSON(oConn, strSQL).Flush

        Case "validate_operator_can_bypass"
            operator_ID = Request("operator_ID")

            strSQL = "SELECT CASE WHEN (SELECT TOP 1 ID FROM operator_roles WHERE operator_ID = " & operator_ID & " AND role_ID IN (2,3,4,5,6)) IS NULL THEN 0 ELSE 1 END AS success"

            QueryToJSON(oConn, strSQL).Flush

		Case "do_error"
			Response.Write (3 / 0)
	End Select '}
%>
<!--#include file="../../../../common/dbClose.asp" -->