var OJT = {
	processor: './NYSUS_OJT/mes/mesaas/processors/ojt.asp',
	operator_ID: null,
	ojt_group_ID: null,
	use_ojt_signoffs: false,
	show_alerts: false,
	show_requirements: false,
	requirement_types: ["ALERT", "JOB"],
	current_requirement_types_index: 0,
	ojt_complete_callback: null
};

// override CORE
MES_CORE.afterCellInfo = function () {
	MES_CORE.logFNCall(arguments, this);
	console.log('OJT.js overriding MES_CORE.afterCellIInfo');

	new Ajax.Request('./NYSUS_OJT/mes/mesaas/app_config.json', {
		method: 'GET', onSuccess: function (jso) {
			var config = jso.responseJSON['SYSTEM_SETUP'][MES_CORE['system_ID']];

			OJT.use_ojt_signoffs = config.use_ojt_signoffs;
			OJT.show_alerts = config.show_alerts;
			OJT.show_requirements = config.show_requirements;

			OJT.doOJT(MES_CORE.afterNonPartSpecificOJTCheck);
		}, onError: function (jso) {
			console.log('ERROR with OJT.js, bypassing!');
			MES_CORE.afterNonPartSpecificOJTCheck();
		}
	});

};

OJT.doOJT = function (cb) {
	var data = {};
	OJT.operator_ID = operator.operator_ID;
	OJT.ojt_complete_callback = cb;
	data.action = "get_ojt_group_ID";
	data.process_type = SYSTEM_SETUP[MES_CORE['system_ID']]['process_type'];
	data.database = MES_CORE.database;

	if (OJT.use_ojt_signoffs) {

		switch (SYSTEM_SETUP[MES_CORE['system_ID']]['process_type']) {
			case "SEQUENCED":
				data.line_ID = SEQUENCE.line_ID;
				data.station_ID = SEQUENCE.station_ID;
				break;
			case "BATCH":
				data.line_ID = BATCH.cell_ID;
				data.station_ID = BATCH.station_ID;
				break;
		}

		doAjaxCall(OJT.processor, data, function (jso, req_params) {
			// validate operator can log into station
			if (jso && jso.length > 0 && jso[0].ojt_group_ID != null && jso[0].ojt_group_ID != 0) {
				OJT.ojt_group_ID = jso[0].ojt_group_ID;
				OJT.validateOperatorCanLogin();
			} else {
				MES_CORE.addToDebugLog('OJT GROUP not configured for this station!');
				OJT.ojt_complete_callback();
			}
		});

	} else {
		cb();
	}

};

OJT.validateOperatorCanLogin = function () {
	var data = {
		"action": "validate_operator",
		"ojt_group_ID": OJT.ojt_group_ID,
		"operator_ID": OJT.operator_ID
	};
	doAjaxCall(OJT.processor, data, function (jso, req_params) {
		OJT.afterValidateOperator(jso, req_params);
	});
};

OJT.afterValidateOperator = function (jso) {
	if (jso) {
		if (jso.length == 0 || jso[0].is_valid == 0) {
			showPopup('OJT SYSTEM', '<h3>OJT RESTRICTION</h3><div>YOU HAVE NOT BEEN CONFIGURED IN OJT TO RUN THIS STATION, CONTACT SUPERVISOR</div><div id="div_overide_ojt_result"></div>');
			setTimeout(doLogout, 3000);
		} else {
			OJT.showNextUnsignedRequirement();
		}
	}
};

// show next unsigned off alert
OJT.showNextUnsignedRequirement = function () {
	var type = OJT.requirement_types[OJT.current_requirement_types_index];
	var data = {
		"action": "get_next_unsigned_requirement",
		"ojt_group_ID": OJT.ojt_group_ID,
		"operator_ID": OJT.operator_ID,
		"requirement_type": type
	};

	if (OJT.current_requirement_types_index == OJT.requirement_types.length) {
		OJT.ojt_complete_callback();
	}

	switch (type) {
		case "ALERT":
			if (OJT.show_alerts) {
				doAjaxCall(OJT.processor, data, OJT.afterShowNextUnsignedRequirement);
			} else {
				MES_CORE.addToDebugLog('OJT IGNORING ALERTS!!! MES_CORE override_alerts = true');
				OJT.current_requirement_types_index++;
				OJT.doOJT(OJT.ojt_group_ID, OJT.operator_ID, OJT.ojt_complete_callback);
			}
			break;
		case "JOB":
			if (OJT.show_requirements) {
				doAjaxCall(OJT.processor, data, OJT.afterShowNextUnsignedRequirement);
			} else {
				MES_CORE.addToDebugLog('OJT IGNORING REQUIREMENTS!!! MES_CORE override_requirements = true');
				OJT.current_requirement_types_index++;
				OJT.doOJT(OJT.ojt_group_ID, OJT.operator_ID, OJT.ojt_complete_callback);
			}
			break;
		default:
			OJT.ojt_complete_callback();
	}
};

OJT.afterShowNextUnsignedRequirement = function (jso) {
	var type = OJT.requirement_types[OJT.current_requirement_types_index];

	if (jso && jso.length == 1) {
		showPopup('REQUIRED OJT SIGN OFF', null);
		getContent('./NYSUS_OJT/mes/mesaas/content/ojt.html', 'div_popup_content', null, function () {
			if (jso[0].image_url) {
				OJT.showImage('div_ojt_view', jso[0].image_url);
				OJT.doSignoffRoles(jso[0].ojt_requirement_ID);
			} else if (jso[0].video_url) {
				OJT.showVideo('div_ojt_view', jso[0].video_url);
				OJT.doSignoffRoles(jso[0].ojt_requirement_ID);
			} else {
				document.getElementById('div_ojt_view').innerText = 'NO CONTENT FOR THIS ' + type;
			}
		});
	} else {
		OJT.current_requirement_types_index++;
		OJT.showNextUnsignedRequirement();
	}
};

OJT.doSignoffRoles = function (ojt_requirement_ID) {
	var data = {
		"action": "get_signoff_roles",
		"ojt_requirement_ID": ojt_requirement_ID
	};
	doAjaxCall(OJT.processor, data, OJT.afterDoSignoffRoles);
};

OJT.afterDoSignoffRoles = function (jso) {
	if (jso && jso.length > 0) {
		OJT.doSignoff(jso[0]);
	}
};

OJT.doSignoff = function (jso) {
	var type = OJT.requirement_types[OJT.current_requirement_types_index];
	var ojt_requirement_ID = jso.ojt_requirement_ID;
	var ojt_signoff_template_levels_roles_ID = jso.ojt_signoff_template_levels_roles_ID;

	document.getElementById('div_ojt_signoffs').innerHTML = 'PLEASE SCAN ' + jso.role_description.toUpperCase() + ' BADGE TO SIGN OFF ' + type + '!!!';
	if (jso.role_ID > 1) {
		document.getElementById('div_ojt_signoffs').innerHTML = 'PLEASE SCAN ' + jso.role_description.toUpperCase() + ' BADGE TO SIGN OFF ' + type + '!!!<input type="button" class="ojt_signoff_bypass" onclick="OJT.bypass()" value="BYPASS" />';
	}

	KEYS.registerKeyHandler({
		"name": "ojt_signoff",
		"global_match": false,
		"match_keys": null,
		"display_el_id": "div_ojt_keys",
		"mask_character": '*',
		"callback": function (scan) {
			// validateOperator has role
			OJT.validateOperatorHasRole(scan, jso.role_ID, function (jso) {
				if (jso && jso.length > 0) {
					if (jso[0].success == 1) {
						// do signoff
						OJT.afterDoSignoff(scan, ojt_requirement_ID, ojt_signoff_template_levels_roles_ID);
						KEYS.resetKeys();
					} else {
						// warn
						document.getElementById('div_ojt_msg').innerHTML = 'OPERATOR SCAN DOES NOT HAVE REQUIRED ROLE FOR SIGNOFF!!!<div id="div_ojt_keys" class="key_input"></div>';
					}
				}
			});
		}
	});
};

OJT.afterDoSignoff = function (scan, ojt_requirement_ID, ojt_signoff_template_levels_roles_ID) {
	var data = {
		"action": "do_signoff",
		"ojt_requirement_ID": ojt_requirement_ID,
		"ojt_signoff_template_levels_roles_ID": ojt_signoff_template_levels_roles_ID,
		"operator_ID": OJT.operator_ID,
		"signoff_operator_ID": scan
	};
	doAjaxCall(OJT.processor, data, function (jso) {
		if (jso.success == 1) {
			document.getElementById('div_ojt_msg').innerHTML = 'SIGN OFF RECORDED!!!';
			setTimeout(OJT.showNextUnsignedRequirement, 1000);
		}
	});
};

OJT.validateOperatorHasRole = function (operator_ID, role_ID, cb) {
	var data = {
		"action": "validate_operator_has_role",
		"operator_ID": operator_ID,
		"role_ID": role_ID
	};
	doAjaxCall(OJT.processor, data, cb);
};

OJT.bypass = function () {
	var msg = '<h1>SCAN YOUR BADGE TO BYPASS ALL OJT REQUIREMENTS!</div>';
	var content = '<div id="div_ojt_keys"></div><input type="button" style="font-size: 20px; font-weight:bold; background-color:yellow; width=225px; height:50px" onclick="OJT.doOJT(OJT.ojt_group_ID, OJT.operator_ID, OJT.ojt_complete_callback)" value="CANCEL" />';

	KEYS.resetKeys();
	showPopup('BYPASSING OJT', msg + content);
	KEYS.registerKeyHandler({
		"name": "ojt_bypass",
		"global_match": false,
		"match_keys": null,
		"display_el_id": "div_ojt_keys",
		"mask_character": '*',
		"callback": function (operator_ID) {
			var data = {
				"action": "validate_operator_can_bypass",
				"operator_ID": operator_ID
			};
			doAjaxCall(OJT.processor, data, function (jso) {
				if (jso && jso.length > 0) {
					if (jso[0].success == 1) {
						hidePopup();
						OJT.ojt_complete_callback();
					} else {
						document.getElementById('div_popup_content').innerHTML = '<h1>A SUPERVISOR OR TEAM LEADER SCAN IS REQUIRED! PLEASE SCAN BADGE OR CANCEL!</h1>' + content;
					}
				}
			});
		}
	});
};

OJT.showImage = function (el, url) {
	var container = document.getElementById(el);
	var image = null;

	if (url) {
		if (url.indexOf('.pdf') >= 0) {
			image = document.createElement('iframe');
			image.src = '../common/library/pdfjs/web/viewer.html#zoom=page-width&file=../../../../' + url;
		} else {
			image = document.createElement('img');
			image.src = url;
			image.alt = 'IMAGE UNAVAILABLE...';
		}
		container.appendChild(image);
	} else {
		var s = document.createElement('span');

		s.innerText = 'NO IMAGE AVAILABLE!';
		container.appendChild(s);
	}
};

OJT.showVideo = function (el, url) {
	var container = document.getElementById(el);
	var video = null;

	if (url) {
		if (url.indexOf('https://www.youtube.com/watch?v=') >= 0) {
			url = url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
		}
		video = document.createElement('iframe');
		video.src = url;
	} else {
		var s = document.createElement('span');

		s.innerText = 'NO VIDEO AVAILABLE!';
		container.appendChild(s);
	}
	container.appendChild(video);
};