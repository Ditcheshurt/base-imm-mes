$.extend(NYSUS.OJT, {
	data_tables: [],
	use_datatables: true,
	with_alerts: true,
	ojt_groups_show_inactive: false,
	selected_ojt_group_ID: null,
	selected_ojt_group_description: null,

	init: function () {
		var modal_detail = $('.modal-detail');
		var panel_options = $('.panel-options');
		var btn_show_alerts = $('.btn-show-alerts');
		var btn_groups_show_active = $('.btn-groups-show-active');
		var panel_signoffs = $('.panel-signoffs');

		$(document).on('nodeSelected', '#ojt_group_tree', function (event, data) {
			app.selected_ojt_group_ID = data.ojt_group_ID;
			app.selected_ojt_group_description = data.ojt_group_description;
			app.render(app.render_options);
		});

		panel_options.on('click', '.btn-option', function () {
			if ($(this).prop('selected') == undefined || $(this).prop('selected') == false) {
				app.option_selected($(this), true);
			} else {
				app.option_selected($(this), false);
			}
		});

		btn_show_alerts.on('click', function () {
			app.with_alerts = app.with_alerts != true;
			app.styleButt(this, app.with_alerts);
			app.render(app.render_options);
		});
		app.styleButt(btn_show_alerts, app.with_alerts);

		modal_detail.on('click', '.btn-signoffs-show-expired', function() {
			var requirement_ID = modal_detail.data().requirement_ID;
			var operator_ID = modal_detail.data().operator_ID;

			app.styleButt(this, $(this).hasClass('btn-danger'));
			app.render_detail(requirement_ID, operator_ID, $(this).hasClass('btn-success'));
		});

		btn_groups_show_active.on('click', function () {
			app.ojt_groups_show_inactive = app.ojt_groups_show_inactive != true;
			app.styleButt(this, app.ojt_groups_show_inactive);
			app.render_group_tree();
		});		

		panel_signoffs.on('click', '.table-signoffs > tbody > tr > td', function () {
			var requirement_ID = $(this).data('requirement_id');
			var operator_ID = $(this).closest('tr').data('operator_id');

			if (requirement_ID != undefined && operator_ID != undefined) {
				app.render_detail($(this).data('requirement_id'), operator_ID, false);
			}
		});

	},

	render_options: function () {
		var opts = [];

		$.map($('.btn-option'), function (v) {
			v = $(v);
			if (v.prop('selected')) {
				opts.push(v.val());
			}
		});

		$.map($('.table-signoffs td'), function (v) {
			if (v.cellIndex > 0) {
				$(v).html('');
				$.map(opts, function (opt) {
					if ($.inArray(opt, $(v).data())) {
						var val = '';
						if ($(v).data(opt) != undefined) {
							val = $(v).data(opt) == 'undefined' ? '' : $(v).data(opt);
						}
						$(v).html($(v).html() + ' ' + val);
					}
				});
			}
		});
	},

	option_selected: function (el, selected) {
		var icon = $(el).find('.glyphicon');

		if (selected) {
			el.removeClass('btn-danger').addClass('btn-success');
			icon.removeClass('glyphicon-remove').addClass('glyphicon-ok');
			el.prop('selected', true);
		} else {
			el.removeClass('btn-success').addClass('btn-danger');
			icon.removeClass('glyphicon-ok').addClass('glyphicon-remove');
			el.prop('selected', false);
		}
		app.render_options();
	},

	render_detail: function (requirement_ID, operator_ID, with_expired) {
		var modal = $('.modal-detail');
		var mbody = modal.find('.modal-body');

		mbody.empty();

		modal.data().requirement_ID = requirement_ID;
		modal.data().operator_ID = operator_ID;

		app.get_operator_requirement_signoffs(operator_ID, requirement_ID, with_expired, function (jso) {
			console.log(jso);
			if (jso && jso.length > 0) {
				var table = $('<table class="table table-condensed table-responsive table-bordered table-hover table-detail" style="width:100%"><thead></thead><tbody></tbody></table>');
				var thead = table.find('thead');
				var tbody = table.find('tbody');
				var operator = jso[0].operator_badge_ID + ': ' + jso[0].operator_last_name + ', ' + jso[0].operator_first_name;
				var requirement_description = jso[0].requirement_description;

				mbody.append('<h4>' + operator + ': ' + requirement_description + '</h4>');

				thead.append('<tr><th>Signoff Operator</th><th>Level</th><th>Role</th><th>Date</th><th>Expires</th></tr>');

				$.map(jso, function (v) {
					var signoff_operator = jso[0].operator_badge_ID + ': ' + jso[0].operator_last_name + ', ' + jso[0].operator_first_name;

					tbody.append('<tr><td>' + signoff_operator + '</td><td>' + v.level_description + '</td><td>' + v.role_description + '</td><td>' + v.created_date == null ? '' : v.created_date + '</td><td>' + v.expire_date == null ? '' : v.expire_date + '</td></tr>');
				});
				mbody.append(table);

				if (app.use_datatables) {
					if (tbody.find('tr').size() > app.datatables_options.lengthMenu[0][0]) {
						app.datatables_options.paging = true;
						app.datatables_options.info = true;
					} else {
						app.datatables_options.paging = false;
						app.datatables_options.info = false;
					}

					table.DataTable(app.datatables_options);
				}

			} else {
				mbody.append('<h3>No Active Signoffs</h3>');
			}
		});
		modal.modal('show');
	},

	render: function (cb) {
		var container = $('.panel-signoffs .panel-body');
		var table = $('<table class="table table-condensed table-responsive table-bordered table-signoffs table-hover" style="width:100%"><thead></thead><tbody></tbody></table>');
		var thead = table.find('thead');
		var hr1 = $('<tr>');
		var hr2 = $('<tr>');
		var tbody = table.find('tbody');

		thead.empty().append('<tr><th>Result</th></tr>');
		tbody.empty().append('<tr><td>No data for group</td></tr>');

		container.empty().append(table);

		if (app.selected_ojt_group_ID != null) {
			var data = {
				ojt_group_ID: app.selected_ojt_group_ID,
				with_alerts: app.with_alerts
			};

			app.get_group_summary(app.selected_ojt_group_ID, app.with_alerts, function (jso) {

				if (jso == null || jso.length == 0) {
					return;
				}

				// groom data
				var unique_templates = {};
				var unique_requirements = {};
				var unique_operators = {};
				var templates = [];
				var requirements = [];
				var operators = [];

				$.map(jso, function (v) {

					if (typeof(unique_templates[v.signoff_template_description]) == "undefined") {
						templates.push({
							signoff_template_description: v.signoff_template_description
						});
					}
					unique_templates[v.signoff_template_description] = 0;

					if (typeof(unique_requirements[v.ojt_requirement_description]) == "undefined") {
						requirements.push({
							signoff_template_description: v.signoff_template_description,
							ojt_requirement_description: v.ojt_requirement_description
						});
					}
					unique_requirements[v.ojt_requirement_description] = 0;

					if (typeof(unique_operators[v.operator_ID]) == "undefined") {
						operators.push({
							operator_ID: v.operator_ID,
							operator_badge: v.operator_badge,
							operator_last_name: v.operator_last_name,
							operator_first_name: v.operator_first_name,
							signoffs: []
						});
					}
					unique_operators[v.operator_ID] = 0;

					// add the signoff info to the operator
					var operator = $.grep(operators, function (o) {
						return o.operator_ID == v.operator_ID;
					});

					$.map(operator, function (o) {
						o.signoffs.push({
							signoff_template_description: v.signoff_template_description,
							ojt_requirement_ID: v.ojt_requirement_ID,
							ojt_requirement_description: v.ojt_requirement_description,
							signoff_level_description: v.signoff_level_description == null ? '' : v.signoff_level_description,
							signoff_operator: v.signoff_operator,
							signoff_level_role_description: v.signoff_level_role_description,
							signoff_date: v.signoff_date != null ? v.signoff_date.date : '',
							signoff_expires: v.signoff_expires
						});
					});

				});

				// add requirements to templates
				$.map(templates, function (v) {
					v.requirements = $.grep(requirements, function (n) {
						return n.signoff_template_description === v.signoff_template_description;
					});
				});

				// build the table

				// add operator col
				hr1.append('<th>');
				hr2.append('<th>Operator</th>');

				// build header
				$.map(templates, function (v) {
					hr1.append('<th colspan="' + v.requirements.length + '">' + v.signoff_template_description + '</th>');
					$.map(v.requirements, function (v) {
						hr2.append('<th>' + v.ojt_requirement_description + '</th>');
					});
				});

				thead.empty().append(hr1).append(hr2);

				// build body
				tbody.empty();
				$.map(operators, function (v) {
					var tr = $('<tr data-operator_id="' + v.operator_ID + '">');

					tr.append('<td>' + v.operator_badge + ': ' + v.operator_last_name + ', ' + v.operator_first_name + '</td>');

					$.map(v.signoffs, function (v) {
						var data = 'data-requirement_id="' + v.ojt_requirement_ID +
							'" data-signoff_level="' + v.signoff_level_description +
							'" data-signoff_operator="' + v.signoff_operator +
							'" data-signoff_role="' + v.signoff_level_role_description +
							'" data-signoff_date="' + v.signoff_date +
							'" data-signoff_expires="' + v.signoff_expires + '"';

						tr.append('<td ' + data + '>' + v.signoff_level_description + '</td>');
					});

					tbody.append(tr);
				});

				if (app.use_datatables) {
					if (tbody.find('tr').size() > app.datatables_options.lengthMenu[0][0]) {
						app.datatables_options.paging = true;
						app.datatables_options.info = true;
					} else {
						app.datatables_options.paging = false;
						app.datatables_options.info = false;
					}

					table.DataTable(app.datatables_options);
				}

				if (typeof cb == 'function') {
					cb();
				}

			});

		}

	}

});

var app = NYSUS.OJT;
$(document).ready(function () {
	var btn = $('.btn-option[value=signoff_level]');

	app.render_group_tree('#ojt_group_tree', app.with_alerts);
	app.render();
	app.init();
	app.option_selected(btn, true);
});