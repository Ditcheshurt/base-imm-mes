$.extend(NYSUS.OJT, {
	use_datatables: true,
	with_alerts: true,
	with_inactive_groups: false,
	show_percent: false,
	selected_ojt_group_ID: null,
	selected_ojt_group_description: null,
	group_summary_data: null,

	init: function () {

		$(document).on('nodeSelected', '#ojt_group_tree', function (event, data) {
			app.selected_ojt_group_ID = data.ojt_group_ID;
			app.selected_ojt_group_description = data.ojt_group_description;
			app.render();
		});

		$('.btn-show-alerts').on('click', function () {
			app.with_alerts = app.with_alerts != true;
			app.styleButt(this, app.with_alerts);
			app.render();
		});
		app.styleButt($('.btn-show-alerts'), app.with_alerts);

		$('.btn-show-percent').on('click', function () {
			app.show_percent = app.show_percent != true;
			app.styleButt(this, app.show_percent);
			app.render();
		});

		$('.btn-groups-show-active').on('click', function () {
			app.with_inactive_groups = app.with_inactive_groups != true;
			app.styleButt(this, app.with_inactive_groups);
			app.render_group_tree('#ojt_group_tree', app.with_inactive_groups);
		});		

	},

	render: function () {
		app.get_group_requirements(app.selected_ojt_group_ID, app.with_alerts, function (jso) {

			if (jso == null || jso.length == 0) {
				var container = $('.panel-signoffs .panel-body');
				var table = $('<table class="table table-condensed table-responsive table-bordered table-signoffs table-hover" style="width:100%"><thead></thead><tbody></tbody></table>');
				var thead = table.find('thead');
				var tbody = table.find('tbody');

				thead.empty().append('<tr><th>Result</th></tr>');
				tbody.empty().append('<tr><td>No requirements for group</td></tr>');
				container.empty().append('<h3 class="group-header">Group ' + app.selected_ojt_group_description + '</h3>').append(table);

			} else {
				// groom data
				var unique_templates = {};
				var templates = [];

				$.map(jso, function (v) {
					if (typeof(unique_templates[v.template_description]) == "undefined") {
						templates.push({
							ojt_signoff_template_ID: v.ojt_signoff_template_ID,
							template_description: v.template_description,
							requirements: $.grep(jso, function (r) {
								return r.ojt_signoff_template_ID == v.ojt_signoff_template_ID;
							})
						});
					}
					unique_templates[v.template_description] = 0;

				});

				// get the group summary and build table for each template
				app.get_group_summary(app.selected_ojt_group_ID, app.with_alerts, function (jso) {
					app.group_summary_data = jso;
					app.build_table(templates);
				});

			}

		});

	},

	build_table: function (templates) {
		var container = $('.panel-signoffs .panel-body');

		container.empty().append('<h3 class="group-header">Group ' + app.selected_ojt_group_description + '</h3>');

		$.map(templates, function (v) {
			var table = $('<table class="table table-condensed table-responsive table-bordered table-hover" style="width:100%"><thead></thead><tbody></tbody></table>');
			var thead = table.find('thead');
			var tbody = table.find('tbody');

			app.get_template_levels(v.ojt_signoff_template_ID, function (jso) {
				var hr1 = $('<tr>');
				var hr2 = $('<tr><th>Requirement</th><th>Min Operators</th></tr>');
				var template_row = $('<th>' + v.template_description + '</th>');
				var count = 2;
				var rows = $('<tbody>');

				v.levels = jso;

				hr1.append(template_row);

				$.map(v.levels, function (l) {
					hr2.append('<th>' + l.level_description + '</th>');
					count++;
				});
				template_row.attr('colspan', count);

				// build rows
				$.map(v.requirements, function (r) {
					var min = r.min_operators ? r.min_operators : '';
					var row = $('<tr>');

					row.append('<td>' + r.requirement_description + '</td>');
					row.append('<td>' + min + '</td>');
					$.map(v.levels, function (l) {

						var signed_off = $.grep(app.group_summary_data, function (s) {
							return r.ojt_requirement_ID == s.ojt_requirement_ID && s.signoff_level_description == l.level_description;
						}).length;

						var clazz = 'danger';

						if (r.min_operators == null || r.min_operators <= 0) {
							clazz = "success";
						} else if (signed_off >= r.min_operators) {
							clazz = "success";
						} else if (signed_off < r.min_operators && signed_off >= r.min_operators - Math.ceil(r.min_operators * .1)) {
							clazz = "warning";
						}

						if (app.show_percent) {
							if (r.min_operators == null) {
								signed_off = '100%';
							} else {
								signed_off = Math.round((signed_off / r.min_operators) * 100) + '%';
							}
						}

						row.append('<td class="' + clazz + '">' + signed_off + '</td>');
					});
					rows.append(row);
				});

				thead.append(hr1).append(hr2);
				tbody.append(rows.find('tr'));
				container.append(table).append('<br />');

				if (app.use_datatables) {
					if (tbody.find('tr').size() > app.datatables_options.lengthMenu[0][0]) {
						app.datatables_options.paging = true;
						app.datatables_options.info = true;
					} else {
						app.datatables_options.paging = false;
						app.datatables_options.info = false;
					}

					table.DataTable(app.datatables_options);
				}

			});

		});

		var pct = (container.find('table .success').length / (container.find('table .danger').length + container.find('table .warning').length + container.find('.table .success').length)) * 100;
		if (!isNaN(pct)) {
			$('.group-header').append(' - ' + pct + '% fulfilled');
		}

	}
});

var app = NYSUS.OJT;
$(document).ready(function () {
	app.render_group_tree('#ojt_group_tree', app.with_alerts);
	app.init();
});