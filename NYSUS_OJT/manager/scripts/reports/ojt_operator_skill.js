$.extend(NYSUS.OJT, {
	processor_signoff_table: './ajax_processors/reports/ojt/ojt_operator_skill_table.php',
	use_datatables: true,

	init: function () {
		this.render_operator_combo();

		$("select").on("change", function () {
			app.render();
		});

		$('.panel-options').on('click', '.btn-option', function () {
			app.option_selected($(this));
			app.render();
		});

		$('.alert-button').on('click', '.btn-option', function () {
			app.alert_button($(this));
			app.render();
		});

	},

	render_operator_combo: function (cb) {
		var app = NYSUS;
		var sel_operators = $('.operator_select');

		sel_operators.empty().append('<option></option>');
		app.get_operators(false, function (jso) {
			$.map(jso, function (v) {
				sel_operators.append('<option value="' + v.ID + '">' + v.badge_ID + ': ' + v.last_name + ', ' + v.first_name + '</option>');
			});
		});

		sel_operators.select2({placeholder: 'Operators'});

		if (typeof cb == 'function') {
			cb();
		}

	},

	render: function () {
		var content = $('.panel-signoffs .panel-body');
		var op_ID = $('.operator_select').val();
		var table_info = $('.btn-group').children('.btn-success').val();
		var alert_button = $('.alert-button').children('.btn').val();

		var data = {
			ojt_operator_ID: op_ID,
			table_info: table_info,
			alert_val: alert_button
		};

		content.empty().load(app.processor_signoff_table, data, function () {

			if (app.use_datatables) {
				// destroy old datatables
				$.map(app.data_tables, function (v) {
					v.destroy();
				});
				app.data_tables = [];
				$.map(content.find('table'), function (v) {

					if ($(v).find('tbody tr').size() > app.datatables_options.lengthMenu[0][0]) {
						app.datatables_options.paging = true;
						app.datatables_options.info = true;
					} else {
						app.datatables_options.paging = false;
						app.datatables_options.info = false;
					}
					$(v).DataTable(app.datatables_options);
				});
			}

		});
	},

	option_selected: function (el) {
		var all_icons = $('.btn-group').find('.glyphicon');
		var icon = $(el).find('.glyphicon');
		var button = $('.btn-group').children();

		button.removeClass('btn-success');
		button.addClass('btn-danger');

		all_icons.removeClass('glyphicon-ok');
		all_icons.addClass('glyphicon-remove');

		el.removeClass('btn-danger').addClass('btn-success');
		icon.removeClass('glyphicon-remove').addClass('glyphicon-ok');

		return el.val();
	},

	alert_button: function (el) {
		var icon = $(el).find('.glyphicon');

		if (el.hasClass("btn-danger")) {
			el.removeClass("btn-danger");
			el.addClass("btn-success");
			icon.removeClass("glyphicon-remove");
			icon.addClass("glyphicon-ok");
			el.val(1);
		}

		else {
			el.removeClass("btn-success");
			el.addClass("btn-danger");
			icon.removeClass("glyphicon-ok");
			icon.addClass("glyphicon-remove");
			el.val(0);
		}
	}

});

var app = NYSUS.OJT;
$(document).ready(function () {
	app.init();

});