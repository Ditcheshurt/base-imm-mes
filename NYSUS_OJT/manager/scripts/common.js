var NYSUS = NYSUS || {};

// merge this into NYSUS.OJT
merge({

    get_operator: function (operator_ID, cb) {
        var processor = NYSUS.api_folder + 'common/operators.php';
        var params = '';

        operator_ID ? params += 'operator_ID=' + operator_ID : null;

        NYSUS.ajax(processor, params, cb);
    },

    get_operators: function (with_inactive, cb) {
        var processor = NYSUS.api_folder + 'common/operators.php';
        var params = with_inactive != null && with_inactive ? 'with_inactive=1' : 'with_inactive=0';

        NYSUS.ajax(processor, params, cb);
    },

    get_operator_roles: function (operator_ID, cb) {
        var processor = NYSUS.api_folder + 'common/operator_roles.php';
        var params = '';

        operator_ID ? params += 'operator_ID=' + operator_ID : null;

        NYSUS.ajax(processor, params, cb);
    }

}, NYSUS);