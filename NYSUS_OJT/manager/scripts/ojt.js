var NYSUS = NYSUS || {};
NYSUS.OJT = NYSUS.OJT || {};

// merge this into NYSUS.OJT
merge(
	{
		datatables_options: {
			"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			"paging": true,
			"info": true,
			"width": "100%",
			"scrollX": true,
			"dom": 'T<"clear">lfrtip',
			"tableTools": {
				"sSwfPath": "NYSUS_OJT/vendor/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			}
		},
		pdfjs_path: 'NYSUS_OJT/vendor/pdfjs/web/viewer.html?file=../../../../',

		styleButt: function(btn, bool) {
			var b = $(btn);
			if (bool) {
				b.removeClass('btn-danger').addClass('btn-success');
				b.find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				b.addClass('btn-danger').removeClass('btn-success');
				b.find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
		},

		render_group_tree: function (el, with_inactive) {

			$.getJSON(NYSUS.api_folder + 'ojt/groups.php', {with_inactive: with_inactive}, function (jso) {
				var tree = [];
				$.map(jso, function (v) {
					if (v.parent_group_ID == null || v.parent_group_ID == 0) {
						var node = {
							ojt_group_ID: v.ID,
							ojt_group_description: v.group_description,
							text: build_node_text(v)
						};
						tree.push(node);
						find_children(v, node);
					}
				});
				function find_children(parent, node) {
					$.map(jso, function (v) {
						if (v.parent_group_ID == parent.ID) {
							var child_node = {
								ojt_group_ID: v.ID,
								ojt_group_description: v.group_description,
								text: build_node_text(v)
							};
							if (node.nodes == undefined) {
								node.nodes = [];
							}
							node.nodes.push(child_node);
							find_children(v, child_node);
						}
					});
				}

				function build_node_text(jso) {
					var node_text = jso.group_description;
					if (jso.active) {
						node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-success pull-right">Active</span>';
					} else {
						node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-danger pull-right">Inactive</span>';
					}
					return node_text;
				}

				$(el).treeview({data: tree});

			});

		},

		get_operator_requirements: function (operator_ID, with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = '';

			operator_ID ? params += 'operator_ID=' + operator_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_operator_signoffs: function (operator_ID, with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = '';

			operator_ID ? params += 'operator_ID=' + operator_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_group: function (ojt_group_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/groups.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			NYSUS.ajax(processor, params, cb);
		},

		get_groups: function (with_inactive, cb) {
			var processor = NYSUS.api_folder + 'ojt/groups.php';
			var params = '';

			with_inactive != null && with_inactive ? params += 'with_inactive=1' : params += 'with_inactive=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_group_operators: function (ojt_group_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/operators.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		get_group_operator: function (ojt_group_ID, operator_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/operators.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			operator_ID ? params += '&operator_ID=' + operator_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		get_group_requirements: function (ojt_group_ID, with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_group_summary: function (ojt_group_ID, with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/summary.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_requirement: function (ojt_requirement_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = '';

			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		get_requirements: function (with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = '';

			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_signoffs: function (ojt_group_ID, ojt_requirement_ID, with_alerts, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = '';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';

			NYSUS.ajax(processor, params, cb);
		},

		get_template_levels: function (ojt_signoff_template_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/levels.php';
			var params = '';

			ojt_signoff_template_ID ? params += 'ojt_signoff_template_ID=' + ojt_signoff_template_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		get_template_roles: function (ojt_signoff_template_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/roles.php';
			var params = '';

			ojt_signoff_template_ID ? params += 'ojt_signoff_template_ID=' + ojt_signoff_template_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		get_operator_requirement_signoffs: function (operator_ID, ojt_requirement_ID, with_alerts, with_expired, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = '';

			operator_ID ? params += 'operator_ID=' + operator_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			with_alerts != null && with_alerts ? params += '&with_alerts=1' : params += '&with_alerts=0';
			with_expired != null && with_expired ? params += '&with_expired=1' : params += '&with_expired=0';

			NYSUS.ajax(processor, params, cb);

		},

		create_signoff: function (ojt_requirement_ID, operator_ID, signoff_operator_ID, ojt_signoff_template_levels_roles_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = 'action=create&';

			ojt_requirement_ID ? params += 'ojt_requirement_ID=' + ojt_requirement_ID : null;
			operator_ID ? params += '&operator_ID=' + operator_ID : null;
			signoff_operator_ID ? params += '&signoff_operator_ID=' + signoff_operator_ID : null;
			ojt_signoff_template_levels_roles_ID ? params += '&ojt_signoff_template_levels_roles_ID=' + ojt_signoff_template_levels_roles_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		create_all_group_requirement_signoffs: function (ojt_group_ID, ojt_requirement_ID, ojt_signoff_operator_ID, ojt_signoff_template_levels_roles_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = 'action=create_all_group_requirement_signoffs&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			ojt_signoff_operator_ID ? params += '&ojt_signoff_operator_ID=' + ojt_signoff_operator_ID : null;
			ojt_signoff_template_levels_roles_ID ? params += '&ojt_signoff_template_levels_roles_ID=' + ojt_signoff_template_levels_roles_ID : null;

			NYSUS.ajax(processor, params, cb);

		},

		create_group: function (ojt_parent_group_ID, ojt_group_description, cb) {
			var processor = NYSUS.api_folder + 'ojt/groups.php';
			var params = 'action=create&';

			ojt_parent_group_ID ? params += 'ojt_parent_group_ID=' + ojt_parent_group_ID : null;
			ojt_group_description ? params += '&ojt_group_description=' + ojt_group_description : null;

			NYSUS.ajax(processor, params, cb);
		},

		create_group_operator: function (ojt_group_ID, operator_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/operators.php';
			var params = 'action=create&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			operator_ID ? params += '&operator_ID=' + operator_ID : null;

			NYSUS.ajax(processor, params, cb);
		},

		create_group_requirement: function (ojt_group_ID, ojt_requirement_ID, min_operators, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = 'action=create&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			min_operators ? params += '&min_operators=' + min_operators : null;

			NYSUS.ajax(processor, params, cb);
		},

		update_group: function (ojt_group_ID, ojt_parent_group_ID, ojt_group_description, active, cb) {
			var processor = NYSUS.api_folder + 'ojt/groups.php';
			var params = 'action=update&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_parent_group_ID ? params += '&ojt_parent_group_ID=' + ojt_parent_group_ID : null;
			ojt_group_description ? params += '&ojt_group_description=' + ojt_group_description : null;
			active != null && active ? params += '&active=1' : params += '&active=0';

			NYSUS.ajax(processor, params, cb);
		},

		update_group_requirement: function (ojt_group_ID, ojt_requirement_ID, min_operators, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = 'action=update&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			min_operators ? params += '&min_operators=' + min_operators : null;

			NYSUS.ajax(processor, params, cb);
		},

		delete_group_requirement: function (id, cb) {
			var processor = NYSUS.api_folder + 'ojt/requirements.php';
			var params = 'action=delete&';

			id ? params += 'ID=' + id : null;

			if (id) {
				NYSUS.ajax(processor, params, cb);
			} else {
				if (typeof cb == 'function') {
					cb('{"result": "failed"}');
				}
			}
		},

		delete_all_group_requirement_signoffs: function (ojt_group_ID, ojt_requirement_ID, ojt_signoff_operator_ID, ojt_signoff_template_levels_roles_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = 'action=delete_all_group_requirement_signoffs&';

			ojt_group_ID ? params += 'ojt_group_ID=' + ojt_group_ID : null;
			ojt_requirement_ID ? params += '&ojt_requirement_ID=' + ojt_requirement_ID : null;
			ojt_signoff_operator_ID ? params += '&ojt_signoff_operator_ID=' + ojt_signoff_operator_ID : null;
			ojt_signoff_template_levels_roles_ID ? params += '&ojt_signoff_template_levels_roles_ID=' + ojt_signoff_template_levels_roles_ID : null;

			NYSUS.ajax(processor, params, cb);

		},

		delete_group_operator: function (id, cb) {
			var processor = NYSUS.api_folder + 'ojt/operators.php';
			var params = 'action=delete&';

			id ? params += 'ID=' + id : null;

			if (id) {
				NYSUS.ajax(processor, params, cb);
			} else {
				if (typeof cb == 'function') {
					cb('{"result": "failed"}');
				}
			}
		},

		delete_signoff: function (id, signoff_operator_ID, cb) {
			var processor = NYSUS.api_folder + 'ojt/signoffs.php';
			var params = 'action=delete&';

			id ? params += 'ID=' + id : null;
			signoff_operator_ID ? params += '&signoff_operator_ID=' + signoff_operator_ID : null;

			if (id) {
				NYSUS.ajax(processor, params, cb);
			} else {
				if (typeof cb == 'function') {
					cb('{"result": "failed"}');
				}
			}

		}

	}, NYSUS.OJT);

