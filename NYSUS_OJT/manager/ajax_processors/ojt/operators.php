<?php

require_once("../init.php");

//$db->catalog = "MES_COMMON";
//$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;

	$sql = "SELECT
				*
			FROM
				dbo.fn_getOJTGroupOperators($ojt_group_ID)";

	$res = $db->query($sql);

	echo(json_encode($res));

}

/**
 * creates a ojt group operator
 * @param $db
 */
function create($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;

	$sql = "INSERT INTO
				ojt_group_operators
				(
					ojt_group_ID,
					operator_ID
				)
			VALUES
			(
				$ojt_group_ID,
				$operator_ID
			)";

	$res = $db->query($sql);

	echo(json_encode($res));
}

/**
 * delete a ojt group operator
 * @param $db
 */
function delete($db)
{
	$id = isset($_REQUEST["ID"]) ? fixDB($_REQUEST["ID"]) : null;
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;

	$sql = "DELETE FROM
				ojt_group_operators
			WHERE
				1=1 ";

	if ($id != null) {
		$sql .= " AND ID = $id ";
	}

	// make sure both are set so you dont wipe out a whole group or operator
	if ($ojt_group_ID != null && $operator_ID != null) {
		$sql .= " AND ojt_group_ID = $ojt_group_ID AND operator_ID = $operator_ID ";
	}

	$res = $db->query($sql);

	echo(json_encode($res));
}