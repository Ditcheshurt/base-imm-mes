<?php

require_once("../init.php");

//$db->catalog = "MES_COMMON";
//$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$as_nested = isset($_REQUEST["as_nested"]) ? fixDB($_REQUEST["as_nested"]) : null;
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$with_inactive = isset($_REQUEST["with_inactive"]) ? fixDB($_REQUEST["with_inactive"]) : null;

	$sql = "SELECT
				g.ID,
				g.parent_group_ID,
				g.group_description,
				g.active,
				g.created_date,
				parent.group_description AS parent_group_description
			FROM
				ojt_groups g
				LEFT JOIN ojt_groups parent ON parent.ID = g.parent_group_ID
			WHERE 1=1 ";

	if ($ojt_group_ID != null) {
		$sql .= " AND g.ID = $ojt_group_ID ";
	} else if ($with_inactive == null || $with_inactive == "0" || strtolower($with_inactive) == "false") {
		$sql .= " AND g.active = 1";
	}
//die($sql);
	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}

	if ($as_nested != null || $as_nested == "1" || strtolower($as_nested) == "true") {
		$nest = array();

		function find_children($res, &$parent) {
			foreach($res as $node) {
				if($parent["ID"] == $node["parent_group_ID"]) {
					$node["children"] = array();
					//die(json_encode($parent));
					find_children($res, $node);
					array_push($parent["children"], $node);
				}
			}
		}

		// build parents
		foreach($res as $node) {
			if($node["parent_group_ID"] == null || $node["parent_group_ID"] == 0) {
				$node["children"] = array();
				find_children($res, $node);
				array_push($nest, $node);
			}
		}

		$res = $nest;
	}

	echo(json_encode($res));

}

function create($db)
{
	$ojt_parent_group_ID = isset($_REQUEST["ojt_parent_group_ID"]) && is_numeric($_REQUEST["ojt_parent_group_ID"]) ? fixDB($_REQUEST["ojt_parent_group_ID"]) : "null";
	$ojt_group_description = fixDB($_REQUEST["ojt_group_description"]);

	$sql = "INSERT ojt_groups
				(parent_group_ID, group_description)
			VALUES
				($ojt_parent_group_ID, '$ojt_group_description')";

	$res = $db->query($sql);

	echo(json_encode($res));

}

function update($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) && is_numeric($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_parent_group_ID = isset($_REQUEST["ojt_parent_group_ID"]) && is_numeric($_REQUEST["ojt_parent_group_ID"]) ? fixDB($_REQUEST["ojt_parent_group_ID"]) : "null";
	$ojt_group_description = isset($_REQUEST["ojt_group_description"]) ? fixDB($_REQUEST["ojt_group_description"]) : null;
	$active = isset($_REQUEST["active"]) ? fixDB($_REQUEST["active"]) : null;

	if (!is_numeric($active)) {
		if (strtolower($active) == "false") {
			$active = 0;
		} else {
			$active = 1;
		}
	}

	if ($ojt_group_ID != null) {
		$sql = "UPDATE
					ojt_groups
				SET
					group_description = '$ojt_group_description',
					parent_group_ID = $ojt_parent_group_ID,
					active = $active
				WHERE
					ID = $ojt_group_ID";

		$res = $db->query($sql);

		echo(json_encode($res));

	} else {
		die('{"error":"missing $ojt_group_ID"}');
	}

}