# README #

Quickie instructions for OJT.. This is still in Alpha so expect issues

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Copy NYSUS_OJT into WWWROOT
* Create virtual directory to NYSUS_OJT for your website in your IIS manager
* SEE CUSTOM SETUP BELOW FOR WHAT YOU WANT TO DO
* Dependencies
* jtree, pdfjs, jquery, Datatables+editor
* Database configuration -- TODO

### MESAAS Manager ###
* edit menu.json and add
```
#!json
	options:
		{"name":"Group Manager", "type":"../NYSUS_OJT/manager/templates/options/ojt_options", "icon":"cog", "viewer_roles":["editor"], "editor_roles":["editor"], "hidden":false }
	reports:
		{"name":"Operators Skill Sheet", "type":"../NYSUS_OJT/manager/templates/reports/ojt_operator_skill", "icon":"list", "viewer_roles":["all"], "editor_roles":[], "hidden":false },
		{"name":"Operators Signoffs Report", "type":"../NYSUS_OJT/manager/templates/reports/ojt_operator_signoffs", "icon":"list", "viewer_roles":["all"], "editor_roles":[], "hidden":false },
		{"name":"Group Signoffs Report", "type":"../NYSUS_OJT/manager/templates/reports/ojt_group_signoffs", "icon":"list", "viewer_roles":["all"], "editor_roles":[], "hidden":false },
	manage:
		{"name":"Group Signoffs", "type":"../NYSUS_OJT/manager/templates/manage/ojt_group_signoffs",	"icon":"folder-close", "viewer_roles":["all"], "editor_roles":["editor"], "hidden":false },
		{"name":"Operator Signoffs", "type":"../NYSUS_OJT/manager/templates/manage/ojt_operator_signoffs", "icon":"folder-close", "viewer_roles":["all"], "editor_roles":["editor"], "hidden":false }

```

### Repetitive MES ###
* edit app_config.json and add
```
#!json
	css_files:
	    "./NYSUS_OJT/mes/repetitive/public/css/ojt.css",
	    "./NYSUS_OJT/vendor/jstree/themes/default/style.min.css"
	js_files:
	    "./NYSUS_OJT/mes/repetitive/scripts/ojt.js",
	    "./NYSUS_OJT/vendor/jstree/jstree.min.js"
```

### Who do I talk to? ###

* Repo owner or admin
* Steve Daviduk