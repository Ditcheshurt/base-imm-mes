USE [MES_COMMON]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupOperators]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupOperators]
(
	@ojt_group_ID int
)
RETURNS
@result TABLE
(
	ojt_group_operator_ID int,
	operator_ID int,
	badge_ID varchar(20),
	first_name varchar(20),
	last_name varchar(20),
	is_inherited bit,
	ojt_group_ID int,
	group_description varchar(100)
)
AS
BEGIN
	-- get group hirearchy
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
			FROM ojt_groups
			WHERE ID = @ojt_group_ID
			UNION ALL
			SELECT og.ID, og.parent_group_ID, og.group_description
			FROM ojt_groups og
			JOIN parents ON og.ID = parents.parent_group_ID
	)
	-- work up the hierarchy
	--WITH parents
	--AS (
	--	SELECT ID, parent_group_ID, group_description
	--	FROM ojt_groups
	--	WHERE parent_group_ID = @ojt_group_ID
	--	UNION ALL
	--	SELECT og.ID, og.parent_group_ID, og.group_description
	--	FROM ojt_groups og
	--	INNER JOIN parents ON og.parent_group_ID = parents.ID
	--)
	INSERT INTO @result
	SELECT
		gop.ID AS ojt_group_operator_ID,
		o.ID AS operator_ID,
		o.badge_ID,
		o.first_name,
		o.last_name,
		CASE WHEN gop.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		--gop.active,
		g.ID AS ojt_group_ID,
		g.group_description
	FROM
		ojt_group_operators gop
		JOIN operators o ON gop.operator_ID = o.ID
		JOIN ojt_groups g ON gop.ojt_group_ID = g.ID
	WHERE
		ojt_group_ID IN (SELECT ID FROM parents) OR ojt_group_ID = @ojt_group_ID
	ORDER BY o.last_name

	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupRequirements]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupRequirements]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS
@result TABLE
(
	ojt_requirement_ID int,
	ojt_signoff_template_ID int,
	type_description varchar(100),
	is_alert bit,
	requirement_description varchar(100),
	template_description varchar(100),
	requirement_text varchar(100),
	image_url varchar(100),
	created_date datetime,
	updated_date datetime,
	requirement_active bit,
	is_inherited bit,
	ojt_group_requirement_ID int,
	ojt_group_ID int,
	group_description varchar(100),
	group_active bit,
	min_operators int
)
AS
BEGIN

	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups
		WHERE ID = @ojt_group_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents ON og.ID = parents.parent_group_ID
	)
	INSERT INTO @result
	SELECT
		r.ID AS ojt_requirement_ID,
		r.ojt_signoff_template_ID,
		rt.type_description,
		rt.is_alert,
		r.requirement_description,
		st.template_description,
		r.requirement_text,
		r.image_url,
		r.created_date,
		r.updated_date,
		r.active AS requirement_active,
		CASE WHEN gr.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		gr.ID AS ojt_group_requirement_ID,
		g.ID AS ojt_group_ID,
		g.group_description,
		g.active AS group_active,
		gr.min_operators
	FROM
		ojt_group_requirements gr
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_groups g ON gr.ojt_group_ID = g.ID
		JOIN ojt_signoff_templates st ON r.ojt_signoff_template_ID = st.ID
	WHERE
		ojt_group_ID IN (SELECT ID FROM parents)
		AND (rt.is_alert = 0 OR @with_alerts = 1)

	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupSummary]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupSummary]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS
@result TABLE
(
	operator_ID int,
	operator_badge varchar(10),
	operator_first_name varchar(50),
	operator_last_name varchar(50),
	signoff_template_ID int,
	signoff_template_description varchar(100),
	ojt_requirement_ID int,
	ojt_requirement_description varchar(100),
	signoff_ID int,
	signoff_level_role_ID int,
	signoff_level_role_description varchar(100),
	signoff_level_description varchar(50),
	signoff_operator varchar(100),
	signoff_date datetime,
	signoff_expires datetime
)
AS
BEGIN

	INSERT INTO @result
	SELECT DISTINCT
		o.operator_ID AS operator_ID,
		o.badge_ID AS operator_badge,
		o.first_name AS operator_first_name,
		o.last_name AS operator_last_name,
		t.ID AS signoff_template_ID,
		t.template_description AS signoff_template_description,
		r.ojt_requirement_ID AS ojt_requirement_ID,
		r.requirement_description AS ojt_requirement_description,
		s.ojt_signoff_ID,
		s.ojt_signoff_template_levels_roles_ID AS signoff_level_role_ID,
		rl.name AS signoff_level_role_description,
		l.level_description AS signoff_level_description,
		CAST(o2.badge_ID AS varchar) + ': ' + o2.last_name + ', ' + o2.first_name AS signnoff_operator,
		s.created_date AS signoff_date,
		s.expire_date AS signoff_expires
	FROM
		dbo.fn_getOJTGroupRequirements(@ojt_group_ID, @with_alerts) r
		CROSS APPLY dbo.fn_getOJTGroupOperators(@ojt_group_ID) o
		CROSS APPLY dbo.fn_getOJTOperatorRequirementSignoffLevel(o.operator_ID, r.ojt_requirement_ID) s
		JOIN ojt_signoff_templates t ON t.ID = r.ojt_signoff_template_ID
		LEFT JOIN ojt_signoff_template_levels_roles lr ON lr.ID = s.ojt_signoff_template_levels_roles_ID
		LEFT JOIN roles rl ON rl.ID = lr.role_ID
		LEFT JOIN ojt_signoff_template_levels l ON l.ID = lr.ojt_signoff_template_level_ID
		LEFT JOIN operators o2 ON o2.ID = s.signoff_operator_ID
	ORDER BY operator_ID

	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTNextUnsignedRequirement]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==================================================
-- Author:		Steve Daviduk
-- Create date: 1/2/17
-- Description:	get operators next unsigned requirement
-- ==================================================
CREATE FUNCTION [dbo].[fn_getOJTNextUnsignedRequirement]
(
	@ojt_group_ID int,
	@operator_ID int,
	@requirement_type_description varchar(25)
)
RETURNS
@result TABLE
(
	ojt_requirement_ID int,
	ojt_group_requirement_ID int,
	ojt_signoff_template_ID int,
	requirement_description varchar(100),
	requirement_text varchar(100),
	image_url varchar(250),
	video_url varchar(250),
	ojt_signoff_template_levels_roles_ID int,
	role_ID int,
	role_name varchar(100)
)
AS
BEGIN

	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups
		WHERE ID = @ojt_group_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents ON og.ID = parents.parent_group_ID
	)
	INSERT INTO @result
	SELECT
		TOP 1
		r.ID,
		gr.ID,
		r.ojt_signoff_template_ID,
		r.requirement_description,
		r.requirement_text,
		r.image_url,
		r.video_url,
		stlr.ID AS ojt_signoff_template_levels_roles_ID,
		stlr.role_ID,
		rle.name
	FROM
		ojt_group_requirements gr
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_groups g ON gr.ojt_group_ID = g.ID
		JOIN ojt_signoff_templates st ON r.ojt_signoff_template_ID = st.ID
		JOIN ojt_signoff_template_levels stl ON st.ID = stl.ojt_signoff_template_ID
		JOIN ojt_signoff_template_levels_roles stlr ON stl.ID = stlr.ojt_signoff_template_level_ID
		JOIN roles rle ON rle.ID = stlr.role_ID
		--LEFT JOIN ojt_signoffs s ON s.ojt_requirement_ID = r.ID AND s.operator_ID = @operator_ID
	WHERE
		ojt_group_ID IN (SELECT ID FROM parents)
		AND rt.type_description = @requirement_type_description
		AND r.active = 1
		AND stlr.ID NOT IN (
			SELECT ojt_signoff_template_levels_roles_ID
			FROM ojt_signoffs
			WHERE ojt_requirement_ID = r.ID
				AND ojt_signoff_template_levels_roles_ID = stlr.ID
				AND operator_ID = @operator_ID
				AND (expire_date IS NULL OR expire_date >= getDate()))
	RETURN
END





GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTOperatorAlerts]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/20/16
-- Description:	get operator requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorAlerts]
(
	@operator_ID int
)
RETURNS
@results TABLE
(
	ojt_requirement_ID int,
	requirement_description varchar(100),
	ojt_signoff_template_ID int,
	template_description varchar(100),
	group_active bit,
	--gr.active AS group_requirement_active,
	image_url varchar(200),
	requirement_active bit,
	requirement_text varchar(200)
)
AS
BEGIN

	WITH parents
	AS (
		SELECT g.ID, g.parent_group_ID, g.group_description
		FROM ojt_groups	g
		JOIN ojt_group_operators o ON g.ID = o.ojt_group_ID
		WHERE parent_group_ID IS NULL
			AND o.operator_ID = @operator_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents p ON og.parent_group_ID = p.ID
	)
	--select * from parents

	INSERT INTO @results
    SELECT DISTINCT
		r.ID AS ojt_requirement_ID,
		r.requirement_description,
		r.ojt_signoff_template_ID,
		t.template_description,
		g.active AS group_active,
		--gr.active AS group_requirement_active,
		r.image_url,
		r.active AS requirement_active,
		r.requirement_text
	FROM
		ojt_groups g
		JOIN ojt_group_requirements gr ON gr.ojt_group_ID = g.ID
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_signoff_templates t ON r.ojt_signoff_template_ID = t.ID
	WHERE
		g.ID In (SELECT DISTINCT ID FROM parents)
		AND rt.type_description LIKE '%Alert%'
	ORDER BY
		r.requirement_description

	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTOperatorRequirements]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/20/16
-- Description:	get operator requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorRequirements]
(
	@operator_ID int,
	@with_alerts bit = 0
)
RETURNS
@results TABLE
(
	ojt_requirement_ID int,
	requirement_description varchar(100),
	ojt_signoff_template_ID int,
	template_description varchar(100),
	group_active bit,
	--gr.active AS group_requirement_active,
	image_url varchar(200),
	requirement_active bit,
	is_alert bit,
	requirement_text varchar(200)
)
AS
BEGIN

	WITH parents
	AS (
		SELECT g.ID, g.parent_group_ID, g.group_description
		FROM ojt_groups	g
		JOIN ojt_group_operators o ON g.ID = o.ojt_group_ID
		WHERE parent_group_ID IS NULL
			AND o.operator_ID = @operator_ID
		--WHERE ID IN
		--(
		--	SELECT
		--		ojt_group_ID
		--	FROM
		--		ojt_group_operators ogo
		--	WHERE
		--		ogo.operator_ID = @operator_ID
		--)
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents p ON og.parent_group_ID = p.ID
	)
	--select * from parents

	INSERT INTO @results
    SELECT DISTINCT
		r.ID AS ojt_requirement_ID,
		r.requirement_description,
		r.ojt_signoff_template_ID,
		t.template_description,
		g.active AS group_active,
		--gr.active AS group_requirement_active,
		r.image_url,
		r.active AS requirement_active,
		rt.is_alert,
		r.requirement_text
	FROM
		ojt_groups g
		JOIN ojt_group_requirements gr ON gr.ojt_group_ID = g.ID
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_signoff_templates t ON r.ojt_signoff_template_ID = t.ID
	WHERE
		g.ID In (SELECT DISTINCT ID FROM parents)
		AND (rt.is_alert = 0 OR @with_alerts = 1)
	ORDER BY
		r.requirement_description

	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTOperatorRequirementSignoffLevel]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get operators highest signoff requirement
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorRequirementSignoffLevel]
(
	@ojt_operator_ID int,
	@ojt_requirement_ID int
)
RETURNS
@result TABLE
(
	ojt_signoff_ID int,
	ojt_requirement_ID int,
	ojt_signoff_template_levels_roles_ID int,
	operator_ID int,
	signoff_operator_ID int,
	created_date datetime,
	expire_date datetime,
	expired_by datetime
)
AS
BEGIN

	-- Declares
	DECLARE
		-- loopy value
		@level_role_ID int,
		@max_ojt_signoff_template_levels_role_ID int, -- holds the highest level role id
		-- hold last signoff
		@last_ojt_signoff_ID int,
		@last_ojt_requirement_ID int,
		@last_ojt_signoff_template_levels_roles_ID int,
		@last_operator_ID int,
		@last_signoff_operator_ID int,
		@last_created_date datetime,
		@last_expire_date datetime,
		@last_expired_by datetime

	-- get ojt_signoff_template_levels_roles_ID and loop to make sure it has a signoff
	-- if no signoff is found break out and return the prior record as highest signoff
	DECLARE loopy224 CURSOR FOR

		SELECT
			lr.ID,
			x.max_ojt_signoff_template_levels_role_ID
		FROM
			ojt_requirements req
			JOIN ojt_signoff_templates t ON req.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels l ON l.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels_roles lr ON lr.ojt_signoff_template_level_ID = l.ID
			CROSS APPLY (
				SELECT TOP 1
					slr.ID max_ojt_signoff_template_levels_role_ID
					--slr.role_order AS max_role_order
				FROM
					ojt_signoff_template_levels_roles slr
				WHERE
					slr.ojt_signoff_template_level_ID = l.ID
				ORDER BY
					slr.role_order desc) x
		WHERE
			req.ID = @ojt_requirement_ID
		ORDER BY
			l.level_order asc, lr.role_order asc

	OPEN loopy224

		FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID
		WHILE @@FETCH_STATUS = 0

		BEGIN
			DECLARE
				-- hold current signoff
				@ojt_signoff_ID int = null,
				@ojt_requirement_ID2 int = null,
				@ojt_signoff_template_levels_roles_ID int = null,
				@operator_ID int = null,
				@signoff_operator_ID int = null,
				@created_date datetime = null,
				@expire_date datetime = null,
				@expired_by datetime = null

			SELECT
				@ojt_signoff_ID = so.ID,
				@ojt_requirement_ID2 = so.ojt_requirement_ID,
				@ojt_signoff_template_levels_roles_ID = so.ojt_signoff_template_levels_roles_ID,
				@operator_ID = so.operator_ID,
				@signoff_operator_ID = so.signoff_operator_ID,
				@created_date = so.created_date,
				@expire_date = so.expire_date,
				@expired_by = so.expired_by
			FROM
				ojt_signoffs so
			WHERE
				so.ojt_requirement_ID = @ojt_requirement_ID
				AND so.operator_ID = @ojt_operator_ID
				AND so.ojt_signoff_template_levels_roles_ID = @level_role_ID
				AND (so.expire_date IS NULL OR getDate() <= so.expire_date)

			IF @ojt_signoff_ID IS NULL
				BEGIN
					--INSERT INTO @result
					--SELECT
					--	@last_ojt_signoff_ID,
					--	@last_ojt_requirement_ID,
					--	@last_ojt_signoff_template_levels_roles_ID,
					--	@last_operator_ID,
					--	@last_signoff_operator_ID,
					--	@last_created_date,
					--	@last_expire_date,
					--	@last_expired_by
					BREAK
				END
			ELSE
				BEGIN
					SET	@last_ojt_signoff_ID = @ojt_signoff_ID
					SET @last_ojt_requirement_ID = @ojt_requirement_ID2
					SET	@last_operator_ID = @operator_ID
					SET	@last_signoff_operator_ID = @signoff_operator_ID
					SET	@last_created_date = @created_date
					SET	@last_expire_date = @expire_date
					SET	@last_expired_by = @expired_by
					IF @max_ojt_signoff_template_levels_role_ID = @ojt_signoff_template_levels_roles_ID
						SET	@last_ojt_signoff_template_levels_roles_ID = @ojt_signoff_template_levels_roles_ID

				END

			FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID

		END

	CLOSE loopy224;
	DEALLOCATE loopy224;

	INSERT INTO @result
	SELECT
		@last_ojt_signoff_ID,
		@last_ojt_requirement_ID,
		@last_ojt_signoff_template_levels_roles_ID,
		@last_operator_ID,
		@last_signoff_operator_ID,
		@last_created_date,
		@last_expire_date,
		@last_expired_by


	RETURN
END



GO
/****** Object:  Table [dbo].[ojt_group_operators]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_group_operators](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[operator_ID] [int] NOT NULL,
	[ojt_group_ID] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
 CONSTRAINT [PK_ojt_group_operators] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_group_requirements]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_group_requirements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_group_ID] [int] NOT NULL,
	[ojt_requirement_ID] [int] NOT NULL,
	[min_operators] [int] NULL,
	[active] [bit] NOT NULL,
	[created_date] [datetime] NULL,
 CONSTRAINT [PK_ojt_group_requirements] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_groups]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[parent_group_ID] [int] NULL,
	[group_description] [varchar](100) NULL,
	[group_text] [varchar](max) NULL,
	[active] [bit] NOT NULL,
	[created_date] [datetime] NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_ojt_groups] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_level_descriptions]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_level_descriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_level] [int] NOT NULL,
	[dept_ID] [int] NOT NULL,
	[level_desc] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_requirement_types]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_requirement_types](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[type_description] [varchar](50) NULL,
	[active] [bit] NOT NULL,
	[create_date] [datetime] NOT NULL,
	[update_date] [datetime] NULL,
	[is_alert] [bit] NULL,
 CONSTRAINT [PK_ojt_requirement_types] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_requirements]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_requirements](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_requirement_type_ID] [int] NULL,
	[ojt_signoff_template_ID] [int] NULL,
	[requirement_description] [varchar](50) NULL,
	[requirement_text] [varchar](100) NULL,
	[allow_mes_signoff] [bit] NULL,
	[expire_days] [int] NULL,
	[video_url] [varchar](500) NULL,
	[image_url] [varchar](250) NULL,
	[active] [bit] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[updated_date] [datetime] NULL,
 CONSTRAINT [PK_ojt_requirements] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_signoff_template_levels]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_signoff_template_levels](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_signoff_template_ID] [int] NOT NULL,
	[level_order] [int] NOT NULL,
	[level_description] [varchar](50) NULL,
	[created_date] [datetime] NOT NULL,
 CONSTRAINT [PK_ojt_signoff_template_levels] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_signoff_template_levels_roles]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_signoff_template_levels_roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_signoff_template_level_ID] [int] NOT NULL,
	[role_order] [int] NULL,
	[role_ID] [int] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ojt_signoff_level_roles] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_signoff_templates]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_signoff_templates](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[template_description] [varchar](50) NULL,
	[created_date] [datetime] NOT NULL,
 CONSTRAINT [PK_ojt_signoff_templates] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ojt_signoffs]    Script Date: 5/10/2017 4:06:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ojt_signoffs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ojt_requirement_ID] [int] NOT NULL,
	[ojt_signoff_template_levels_roles_ID] [int] NOT NULL,
	[operator_ID] [int] NOT NULL,
	[signoff_operator_ID] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[expire_date] [datetime] NULL,
	[expired_by] [int] NULL,
 CONSTRAINT [PK_ojt_signoffs] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ojt_groups] ON

GO
INSERT [dbo].[ojt_groups] ([ID], [parent_group_ID], [group_description], [group_text], [active], [created_date], [updated_date]) VALUES (1, NULL, N'Plant Wide', NULL, 1, CAST(N'2016-05-23T12:14:41.770' AS DateTime), NULL)
GO
INSERT [dbo].[ojt_groups] ([ID], [parent_group_ID], [group_description], [group_text], [active], [created_date], [updated_date]) VALUES (2, 34, N'Some Line', NULL, 1, CAST(N'2016-05-23T12:14:41.770' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ojt_groups] OFF
GO
SET IDENTITY_INSERT [dbo].[ojt_level_descriptions] ON

GO
INSERT [dbo].[ojt_level_descriptions] ([ID], [ojt_level], [dept_ID], [level_desc]) VALUES (1, 1, 1, N'In Training with Supervision')
GO
INSERT [dbo].[ojt_level_descriptions] ([ID], [ojt_level], [dept_ID], [level_desc]) VALUES (2, 2, 1, N'Basic Knowledge - Needs Supervision')
GO
INSERT [dbo].[ojt_level_descriptions] ([ID], [ojt_level], [dept_ID], [level_desc]) VALUES (3, 3, 1, N'Able to Follow Work Standardization')
GO
INSERT [dbo].[ojt_level_descriptions] ([ID], [ojt_level], [dept_ID], [level_desc]) VALUES (4, 4, 1, N'Able to Train Others')
GO
SET IDENTITY_INSERT [dbo].[ojt_level_descriptions] OFF
GO
SET IDENTITY_INSERT [dbo].[ojt_requirement_types] ON

GO
INSERT [dbo].[ojt_requirement_types] ([ID], [type_description], [active], [create_date], [update_date], [is_alert]) VALUES (1, N'Alert', 1, CAST(N'2016-05-24T12:52:06.573' AS DateTime), NULL, 1)
GO
INSERT [dbo].[ojt_requirement_types] ([ID], [type_description], [active], [create_date], [update_date], [is_alert]) VALUES (2, N'Job', 1, CAST(N'2016-05-24T12:52:13.010' AS DateTime), NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[ojt_requirement_types] OFF
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_template_levels] ON

GO
INSERT [dbo].[ojt_signoff_template_levels] ([ID], [ojt_signoff_template_ID], [level_order], [level_description], [created_date]) VALUES (1, 1, 10, N'Operator Signoff', CAST(N'2016-05-31T13:54:51.857' AS DateTime))
GO
INSERT [dbo].[ojt_signoff_template_levels] ([ID], [ojt_signoff_template_ID], [level_order], [level_description], [created_date]) VALUES (2, 2, 10, N'Operator, Supervisor', CAST(N'2016-06-01T11:23:31.437' AS DateTime))
GO
INSERT [dbo].[ojt_signoff_template_levels] ([ID], [ojt_signoff_template_ID], [level_order], [level_description], [created_date]) VALUES (3, 3, 10, N'Operator, Supervisor, Team Leader', CAST(N'2016-06-07T12:33:34.220' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_template_levels] OFF
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_template_levels_roles] ON

GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (8, 1, 10, 6, CAST(N'2016-05-31T13:55:23.1500000' AS DateTime2))
GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (9, 2, 10, 6, CAST(N'2016-05-31T16:58:31.0300000' AS DateTime2))
GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (10, 2, 20, 2, CAST(N'2016-05-31T16:58:34.4630000' AS DateTime2))
GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (11, 3, 10, 6, CAST(N'2016-05-31T16:58:36.3570000' AS DateTime2))
GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (12, 3, 20, 2, CAST(N'2016-06-01T11:27:56.0200000' AS DateTime2))
GO
INSERT [dbo].[ojt_signoff_template_levels_roles] ([ID], [ojt_signoff_template_level_ID], [role_order], [role_ID], [created_date]) VALUES (13, 3, 30, 4, CAST(N'2016-06-01T11:27:58.4400000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_template_levels_roles] OFF
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_templates] ON

GO
INSERT [dbo].[ojt_signoff_templates] ([ID], [template_description], [created_date]) VALUES (1, N'Alert Signoffs', CAST(N'2016-05-27T13:37:51.717' AS DateTime))
GO
INSERT [dbo].[ojt_signoff_templates] ([ID], [template_description], [created_date]) VALUES (2, N'Operator, Supervisor', CAST(N'2016-05-27T13:38:16.067' AS DateTime))
GO
INSERT [dbo].[ojt_signoff_templates] ([ID], [template_description], [created_date]) VALUES (3, N'Operator, Supervisor, Team Leader', CAST(N'2016-05-27T13:38:25.290' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[ojt_signoff_templates] OFF
GO
ALTER TABLE [dbo].[ojt_group_operators] ADD  CONSTRAINT [DF_ojt_group_operators_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_group_requirements] ADD  CONSTRAINT [DF_ojt_group_requirements_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[ojt_group_requirements] ADD  CONSTRAINT [DF_ojt_group_requirements_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_groups] ADD  CONSTRAINT [DF_ojt_groups_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[ojt_groups] ADD  CONSTRAINT [DF_ojt_groups_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_requirement_types] ADD  CONSTRAINT [DF_ojt_requirement_types_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[ojt_requirement_types] ADD  CONSTRAINT [DF_ojt_requirement_types_create_date]  DEFAULT (getdate()) FOR [create_date]
GO
ALTER TABLE [dbo].[ojt_requirements] ADD  CONSTRAINT [DF_ojt_requirements_allow_mes_signoff]  DEFAULT ((0)) FOR [allow_mes_signoff]
GO
ALTER TABLE [dbo].[ojt_requirements] ADD  CONSTRAINT [DF_ojt_requirements_active]  DEFAULT ((0)) FOR [active]
GO
ALTER TABLE [dbo].[ojt_requirements] ADD  CONSTRAINT [DF_ojt_requirements_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels] ADD  CONSTRAINT [DF_ojt_signoff_template_levels_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles] ADD  CONSTRAINT [DF_ojt_signoff_level_roles_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_signoff_templates] ADD  CONSTRAINT [DF_ojt_signoff_templates_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_signoffs] ADD  CONSTRAINT [DF_ojt_signoffs_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ojt_group_operators]  WITH CHECK ADD  CONSTRAINT [FK_ojt_group_operators_ojt_groups] FOREIGN KEY([ojt_group_ID])
REFERENCES [dbo].[ojt_groups] ([ID])
GO
ALTER TABLE [dbo].[ojt_group_operators] CHECK CONSTRAINT [FK_ojt_group_operators_ojt_groups]
GO
ALTER TABLE [dbo].[ojt_group_requirements]  WITH CHECK ADD  CONSTRAINT [FK_ojt_group_requirements_ojt_groups] FOREIGN KEY([ojt_group_ID])
REFERENCES [dbo].[ojt_groups] ([ID])
GO
ALTER TABLE [dbo].[ojt_group_requirements] CHECK CONSTRAINT [FK_ojt_group_requirements_ojt_groups]
GO
ALTER TABLE [dbo].[ojt_requirements]  WITH CHECK ADD  CONSTRAINT [FK_ojt_requirements_ojt_requirement_types] FOREIGN KEY([ojt_requirement_type_ID])
REFERENCES [dbo].[ojt_requirement_types] ([ID])
GO
ALTER TABLE [dbo].[ojt_requirements] CHECK CONSTRAINT [FK_ojt_requirements_ojt_requirement_types]
GO
ALTER TABLE [dbo].[ojt_requirements]  WITH CHECK ADD  CONSTRAINT [FK_ojt_requirements_ojt_signoff_templates] FOREIGN KEY([ojt_signoff_template_ID])
REFERENCES [dbo].[ojt_signoff_templates] ([ID])
GO
ALTER TABLE [dbo].[ojt_requirements] CHECK CONSTRAINT [FK_ojt_requirements_ojt_signoff_templates]
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels]  WITH CHECK ADD  CONSTRAINT [FK_ojt_signoff_template_levels_ojt_signoff_templates] FOREIGN KEY([ojt_signoff_template_ID])
REFERENCES [dbo].[ojt_signoff_templates] ([ID])
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels] CHECK CONSTRAINT [FK_ojt_signoff_template_levels_ojt_signoff_templates]
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles]  WITH CHECK ADD  CONSTRAINT [FK_ojt_signoff_level_roles_ojt_signoff_template_levels] FOREIGN KEY([ojt_signoff_template_level_ID])
REFERENCES [dbo].[ojt_signoff_template_levels] ([ID])
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles] CHECK CONSTRAINT [FK_ojt_signoff_level_roles_ojt_signoff_template_levels]
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles]  WITH CHECK ADD  CONSTRAINT [FK_ojt_signoff_level_roles_roles] FOREIGN KEY([role_ID])
REFERENCES [dbo].[roles] ([ID])
GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles] CHECK CONSTRAINT [FK_ojt_signoff_level_roles_roles]
GO
ALTER TABLE [dbo].[ojt_signoffs]  WITH CHECK ADD  CONSTRAINT [FK_ojt_signoffs_ojt_requirements] FOREIGN KEY([ojt_requirement_ID])
REFERENCES [dbo].[ojt_requirements] ([ID])
GO
ALTER TABLE [dbo].[ojt_signoffs] CHECK CONSTRAINT [FK_ojt_signoffs_ojt_requirements]
GO
ALTER TABLE [dbo].[ojt_signoffs]  WITH CHECK ADD  CONSTRAINT [FK_ojt_signoffs_ojt_signoff_template_levels_roles] FOREIGN KEY([ojt_signoff_template_levels_roles_ID])
REFERENCES [dbo].[ojt_signoff_template_levels_roles] ([ID])
GO
ALTER TABLE [dbo].[ojt_signoffs] CHECK CONSTRAINT [FK_ojt_signoffs_ojt_signoff_template_levels_roles]
GO
