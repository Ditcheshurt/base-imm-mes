var keys = '';
var keyActionArr = [];
var key_display_el_name = 'div_keys';
var onlyNumericKeys = false;
var keyPressValidator = null;   ///set this to a function that returns true/false if you need to validate all key presses
var afterKeyProcessor = null;

if (typeof window.event != 'undefined') {
   document.onkeydown = function() {
      var t=event.srcElement.type;
      var kc=event.keyCode;
      if (kc == 8) {
         if (keys != '') {
            keys = keys.substring(0, keys.length - 1);
            updateKeys();
         }
         return false;
      }
      if (kc == 13) {
         checkKeys();
      } else {
         if ((kc >= 65 && kc <= 90) || (kc >= 96 && kc <= 122) || (kc >= 48 && kc <= 57) || kc == 189 || kc == 45) {
            if (kc == 189) {
               if (event.shiftKey) {
                  kc = 95;
               } else {
                  kc = 45;
               }
            }

            if (kc >= 96 && kc <= 105) {
		         kc -= 48;
		      }

            if (onlyNumericKeys && isNaN(String.fromCharCode(kc))) {
            	return;
            }

            if (keyPressValidator != null) {
            	if (!keyPressValidator(String.fromCharCode(kc))) {
            		return;
            	}
            }

            keys += String.fromCharCode(kc).toUpperCase();
            updateKeys();
         } else {
            window.status = kc;
         }
      }
      return true;
   }
} else {
   document.onkeypress = function(e) {
      var t=e.target.type;
      var kc=e.which;

      if (kc == 8) {
         if (keys != '') {
            keys = keys.substring(0, keys.length - 1);
            updateKeys();
         }
         return false;
      }
      if (kc == 13) {
         checkKeys();
      } else {
         if ((kc >= 65 && kc <= 90) || (kc >= 96 && kc <= 122) || (kc >= 48 && kc <= 57) || kc == 189 || kc == 45) {
            if (kc == 189) {
               if (event.shiftKey) {
                  kc = 95;
               } else {
                  kc = 45;
               }
            }

            if (kc >= 96 && kc <= 105) {
		         kc -= 48;
		      }

            if (onlyNumericKeys && isNaN(String.fromCharCode(kc))) {
            	return;
            }

            if (keyPressValidator != null) {
            	if (!keyPressValidator(String.fromCharCode(kc))) {
            		return;
            	}
            }

            keys += String.fromCharCode(kc).toUpperCase();
            updateKeys();
         }
      }

      return true;
   }
}

function updateKeys() {
	var k = $('#' + key_display_el_name);
	if (!k) {
		return;
	}

   if (keyActionArr.length > 0) {
      k.html(keys);

      if (keys == '') {
         //$(key_display_el_name).hide();
      } else {
         k.show();
      }
   } else {
      k.hide();
   }

   if (afterKeyProcessor != null) {
   	afterKeyProcessor();
   }
}

function forceUpdateKeys() {
   $('#' + key_display_el_name).html(keys);
   if (keys == '') {
      //$(key_display_el_name).hide();
   } else {
      $(key_display_el_name).show();
   }
}

function clearKeys() {
   keys = '';
   forceUpdateKeys();
}

function checkKeys() {
   for (var i = 0; i < keyActionArr.length; i++) {
      if (keys == keyActionArr[i][0]) {
         keyActionArr[i][1]();
         return;
      }
      if (keyActionArr[i][0] == '') {
         keyActionArr[i][1](keys);
         return;
      }
   }
   clearKeys();
}