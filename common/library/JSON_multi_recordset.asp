<%
	'This is cool
	' It will take a query that returns multiple select statements, and convert each one into JSON, as part of a larger JSON object

	'Expects: connection object (oConn), the query, and array (may be blank) of names for the recordsets in the order they will happen
	'Returns: will write to the response stream
	Function MultipleRecordsetsToJSON(oConnMRS, query, recordsetNames)
		counterMRS = 0
		Response.Write "{"

		Set oRsMRS = oConnMRS.Execute(strSQL)
		Do Until oRsMRS Is Nothing
			if counterMRS > 0 Then
				Response.Write ", "
			end if
			if counterMRS > UBound(recordsetNames) Then
				Response.Write " ""query_result" & counterMRS & """:"
			else
				Response.Write " """ & recordsetNames(counterMRS) & """:"
			end if
			RecordsetToJSON(oRsMRS).Flush

			Set oRsMRS = oRsMRS.NextRecordset
			counterMRS = counterMRS + 1
		Loop
		Response.Write "}"
		Response.Flush
		Set oRsMRS = Nothing
	End Function

	Function RecordsetToJSON(rs)
		  Dim jsa, col
		  Set jsa = jsArray()
		  While Not (rs.EOF Or rs.BOF)
					 Set jsa(Null) = jsObject()
					 For Each col In rs.Fields
								jsa(Null)(col.Name) = col.Value
					 Next
		  rs.MoveNext
		  Wend
		  Set RecordsetToJSON = jsa
	End Function

%>