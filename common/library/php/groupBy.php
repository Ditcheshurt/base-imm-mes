<?php

class groupBy {
	
	private $group_map;

	public function __construct() {
		$this->group_map = array();
	}

	public function set_group($name, $select, $as = False) {
		$obj = new stdClass();
		$obj->name = $name;
		$obj->select = $select;
		$obj->as = $as;
		array_push($this->group_map, $obj);

		return true;
	}

	public function get_group_by($group_by) {
		$response = new stdClass();
		$response->groups = array();
		$response->selects = array();
		$found = false;
		
		for ($j = 0; $j < count($group_by); $j++) {
			for ($i = 0; $i < count($this->group_map); $i++) {
				if (strtolower($group_by[$j]) == strtolower($this->group_map[$i]->name)) {
					$found = true;
					array_push($response->selects, $this->group_map[$i]->select . " as '" . ($this->group_map[$i]->as ? $this->group_map[$i]->as : $this->group_map[$i]->name) . "'");
					array_push($response->groups, $this->group_map[$i]->select);
					break;
				}
			}
		}

		if (!$found) {
			array_push($response->selects, $this->group_map[0]->select . " as '" . ($this->group_map[0]->as ? $this->group_map[0]->as : $this->group_map[0]->name) . "'");
			array_push($response->groups, $this->group_map[0]->select);
		}

		return $response;
	}

}

?>