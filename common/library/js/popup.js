var popup_visible = false;
var ajax_popup_visible = false;
var popup_width_percent = 0.75;
var popup_el_name = 'div_popup';
//function showPopup(title, content) {
//	var b = $('div_blocker');
//	var p;
//	if (!b) {
//		b = new Element('DIV', {id:'div_blocker'});
//		document.body.appendChild(b);
//		p = new Element('DIV', {id:'div_popup'});
//		p.appendChild(new Element('DIV', {id:'div_popup_title'}));
//		p.appendChild(new Element('DIV', {id:'div_popup_content'}));
//		document.body.appendChild(p);
//	} else {
//		p = $('div_popup');
//	}
//
//	b.setStyle({height:'100%', width:'100%'});
//	b.show();
//
//	var pw = $('popup_wrapper');
//	if (pw) {
//		$('popup_wrapper').show();
//	}
//
//	$('div_popup_title').update(title);
//	$('div_popup_content').update(content);
//
//	centerElement(p);
//
//	p.show();
//	popup_visible = true;
//}

function showPopup(title, content) {
	var b = $('div_blocker');
	var p;
	if (!b) {
		b = new Element('DIV', {id:'div_blocker'});
		b.style.display = 'none';
		document.body.appendChild(b);
	} else {

	}

	p = $(popup_el_name);
	if (!p) {
		p = new Element('DIV', {id:popup_el_name});
		p.style = 'margin: 50px auto auto auto;border:2px solid black;width:90%;background-color:white;display:none;text-align:left;';
		var pw = $('popup_wrapper');
		if (pw) {
			$('popup_wrapper').insert(p);
		} else {
			pw = new Element('DIV');
			pw.id = 'popup_wrapper';
			pw.style = 'position:absolute;top:0px;left:0px;overflow:auto;width:100%;height:100%;display:none;';
			pw.update(p);
			document.body.appendChild(pw);
		}
	}

	var pw = $('popup_wrapper');
	if (pw) {
		$('popup_wrapper').show();
	}

	p.update(new Element('DIV', {id:'div_popup_title'}));
	p.appendChild(new Element('DIV', {id:'div_popup_content'}));

	b.show();

	$('div_popup_title').update(title);
	$('div_popup_content').update(content);

	centerElement(p);

	p.show();
	popup_visible = true;
}

function hidePopup() {
	$(popup_el_name).hide();
	$('div_blocker').hide();
	var pw = $('popup_wrapper');
	if (pw) {
		$('popup_wrapper').hide();
	}
	popup_visible = false;
}

function centerElement(el) {
	el.style.width = ($(document.body).getWidth()*popup_width_percent) + 'px';
	el.style.top = (($(document.body).getHeight()/2) - (el.getHeight()/2)) + 'px';
	el.style.left = (($(document.body).getWidth()/2) - (el.getWidth()/2)) + 'px';
}


function showAjaxPopup(title, content) {
	var b = $('div_ajax_blocker');
	var p;
	if (!b) {
		b = new Element('DIV', {id:'div_ajax_blocker'});
		document.body.appendChild(b);
		p = new Element('DIV', {id:'div_ajax_popup'});
		p.appendChild(new Element('DIV', {id:'div_ajax_popup_title'}));
		p.appendChild(new Element('DIV', {id:'div_ajax_popup_content'}));
		document.body.appendChild(p);
	} else {
		p = $('div_ajax_popup');
	}

	b.setStyle({height:'100%', width:'100%'});
	b.show();

	$('div_ajax_popup_title').update(title);
	$('div_ajax_popup_content').update(content);

	centerElement(p);

	p.show();
	ajax_popup_visible = true;
}

function hideAjaxPopup() {
	if (ajax_popup_visible) {
		$('div_ajax_popup').hide();
		$('div_ajax_blocker').hide();
		ajax_popup_visible = false;
	}
}

function centerPopup() {
	centerElement($('div_popup'));
}