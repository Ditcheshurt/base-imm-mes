

var cur_table_sort_field = 'ID';
var cur_table_sort_order = 'ASC';
var cur_table_name = '';
function selectDataTable(step_num, val) {
	if (cur_table_name != val) {
		cur_table_sort_field = 'ID';
		cur_table_sort_order = 'ASC';
	}
	cur_table_name = val;
	var h = queryHashArr(editable_tables, 'table_name', val);
	if (hashHasKey(h, 'xref_rows')) {
		doAjaxCall(step_num, {"action":"get_table_xref", "table_name":val,
			"xref_rows":h['xref_rows'], "row_filter":h['row_filter'],
			"xref_cols":h['xref_cols'], "col_filter":h['col_filter']}, editTableXref);
	} else {
		doAjaxCall(step_num, {"action":"get_table_info", "table_name":val,
			"hard_coded_field":missingToBlank(h, 'hard_coded_field'), "hard_coded_operator":missingToBlank(h, 'hard_coded_operator'),
			"hard_coded_value":missingToBlank(h, 'hard_coded_value'),
			"sort_field":cur_table_sort_field, "sort_order":cur_table_sort_order,
			"filtered_field":missingToBlank(h, 'filtered_field'), "filtered_clause":missingToBlank(h, 'filtered_clause')}, editTableData);
	}
}

function missingToBlank(hashy, key) {
	if (hashHasKey(hashy, key)) {
		return hashy[key];
	} else {
		return '';
	}
}

function editTableXref(step_num, jso, req_params) {
	var dv = new Element('DIV');
	var t = new Element('TABLE', {'id':'table_data', 'width':'100%', 'border':1, 'bordercolor':'black', 'cellpadding':2, 'cellspacing':0});
	var r = Element.extend(t.insertRow(-1)).setStyle({'fontWeight':'bold', 'backgroundColor':'#C0C0C0'});
	for (f in jso[0]) {
		if (f != 'row_ID') {
			var c = Element.extend(r.insertCell(-1));
			c.update(f.replace(/_/ig, ' ').toUpperCase());
		}
	}
	for (var i = 0; i < jso.length; i++) {
		var r = Element.extend(t.insertRow(-1));
		for (f in jso[i]) {
			if (f != 'row_ID') {
				var c = Element.extend(r.insertCell(-1));
				if (jso[i][f] === true || jso[i][f] === false) {
					var element_ID = 'inp@' + req_params['table_name'] + '@' + jso[i]['row_ID'] + '@' + f.replace(/ /ig, '~~~~') + '@bit';
					c.update('<INPUT ID="' + element_ID + '" TYPE="CHECKBOX" VALUE="1" ' + ((jso[i][f])?'CHECKED':'') + ' ONCLICK="updateXref(this);">');
				} else {
					c.update(jso[i][f]);
				}
			}
		}
	}
	dv.update(t);
	dv.insert('<div style="text-align:left;padding:10px;"><span class="close_button" onclick="closeTableEdit();">Close</span></div>');

	showPopup('Editing ' + queryHashArr(editable_tables, 'table_name', req_params['table_name'], 'desc'), dv);
}

function updateXref(el) {
	var id_arr = el.id.split('@');
	var h = queryHashArr(editable_tables, 'table_name', id_arr[1]);
	el.up().addClassName('saving');
	el.disable();
	doAjaxCall(0, {"action":"update_xref",
		"element_ID":el.id,
		"table_name":id_arr[1], "xref_rows":h['xref_rows'], "xref_cols":h['xref_cols'],
		"row_ID":id_arr[2],
		"field_desc":id_arr[3], "new_value":el.checked}, afterXrefUpdate);
}

function afterXrefUpdate(step_num, jso, req_params) {
	if (field_edit_timer) {
		//clearTimeout(field_edit_timer);
	}
	var el = $(req_params['element_ID']);

	el.enable();
	el.up().removeClassName('saving').addClassName('saved');
	field_edit_timer = Element.removeClassName.delay(2, el.up(), 'saved');

	$('div_current_step_result').update('Data saved!').show();
	Element.hide.delay(2, $('div_current_step_result'));
}

var cur_table_fields = null;
var cur_table_foreign_keys = null;
function editTableData(step_num, jso, req_params) {
	cur_table_fields = jso['fields'];
	cur_table_foreign_keys = jso['foreign_key_data'];
	var dv = new Element('DIV');

	var h = queryHashArr(editable_tables, 'table_name', req_params.table_name);
	if (hashHasKey(h, 'help_text')) {
		dv.insert('<div class="tip">Tip: ' + h.help_text + '</div>');
	}

	var t = new Element('TABLE', {'id':'table_data', 'width':'100%', 'border':1, 'bordercolor':'black', 'cellpadding':2, 'cellspacing':0});

	var r = Element.extend(t.insertRow(-1)).setStyle({'fontWeight':'bold', 'backgroundColor':'#C0C0C0'});
	for (var i = 0; i < jso['fields'].length; i++) {
		var c = Element.extend(r.insertCell(-1));
		c.update(jso['fields'][i]['COLUMN_NAME'].replace('_ID','').replace(/_/ig, ' ').toUpperCase());
		c.noWrap = true;
		var im = new Element('IMG');
		im.addClassName('sorter');
		if (jso['fields'][i]['COLUMN_NAME'] == cur_table_sort_field) {
			if (cur_table_sort_order == 'ASC') {
				im.src = '/images/microupsel.png';
				im.title = 'Click to sort by this field descending';
				im.observe('click', function() {cur_table_sort_order='DESC';selectDataTable(step_num, req_params.table_name)});
			} else {
				im.src = '/images/microdownsel.png';
				im.title = 'Click to sort by this field ascending';
				im.observe('click', function() {cur_table_sort_order='ASC';selectDataTable(step_num, req_params.table_name)});
			}
		} else {
			im.src = '/images/microup.png';
			im.title = 'Click to sort by this field ascending';
			im.writeAttribute({'field_name':jso['fields'][i]['COLUMN_NAME']});
			im.observe('click', function() {cur_table_sort_field=this.readAttribute('field_name');cur_table_sort_order='ASC';selectDataTable(step_num, req_params.table_name)});
		}
		c.insert(im);
	}
	var c = Element.extend(r.insertCell(-1));
	c.update('DELETE');

	for (var i = 0; i < jso['table_data'].length; i++) {
		var d = jso['table_data'][i];
		var r = Element.extend(t.insertRow(-1));
		r.id = 'tr@' + req_params['table_name'] + '@' + d['ID'];
		for (var j = 0; j < jso['fields'].length; j++) {
			var f = jso['fields'][j];
			var c = Element.extend(r.insertCell(-1));
			c.style.position = 'relative';
			c.writeAttribute({'nowrap':true, 'id':'td_' + f['COLUMN_NAME'] + '_' + d['ID']});
			var fc = getCellContents(req_params['table_name'], f, d[f['COLUMN_NAME']], jso['foreign_key_data'], d['ID'], true);
			c.update(fc);
		}
		var c = Element.extend(r.insertCell(-1));
		c.id = 'td_delete_' + d['ID'];
		c.writeAttribute({'table_name':req_params['table_name'], 'row_ID':d['ID']});
		c.update('<span class="linker" onclick="confirmDeleteRow(this.parentElement);">DELETE</span>');
	}

	dv.insert(t);

	dv.insert('<div style="text-align:left;padding:10px;"><span class="close_button" onclick="closeTableEdit();">Close</span> <span class="add_button" onclick="addTableRow(\'' + req_params['table_name'] + '\');">Add New Row</span></div>');

	showPopup('Editing ' + queryHashArr(editable_tables, 'table_name', req_params['table_name'], 'desc'), dv);

	centerPopup.defer();
	key_input_disabled = true;
}

//table name, field (object defining field), val (current value, if editing), keys (foreign key info), row_ID (ID of parent record), is_edit (boolean)
function getCellContents(table_name, field, val, keys, row_ID, is_edit) {
	if (field['IS_IDENTITY'] == 0) {
		var element_ID = 'inp@' + table_name + '@' + row_ID + '@' + field['COLUMN_NAME'] + '@' + field['DATA_TYPE'];
		switch (field['DATA_TYPE']) {
			case 'varchar':
			case 'datetime':
			case 'float':
				if (field['FIELD_DESC'] == 'IMAGE_FIELD') {
					if (is_edit) {
						setTimeout(function() {setupUploadImage(element_ID);}, 100);
						var img = '';
						if (val) {
							img = '<a target="_blank" href="/uploads/DEFECTS/' + company_ID + '/' + row_ID + '~~' + val + '"><img alt="' + val + '" src="/uploads/DEFECTS/' + company_ID + '/' + row_ID + '~~' + val + '" width="50"></a>';
						}
						return '<div id="div_upload_cont_' + row_ID + '">' +
								 '	 <a class="linker" ID="a_' + element_ID + '">SELECT IMAGE</a>' +
								 '	 <input type="hidden" ID="' + element_ID + '" VALUE="' + val + '">' +
								 '	<div id="div_' + element_ID + '">' + img + '</div>' +
								 '</div>';
					} else {
						return '(Save first)<input type="hidden" ID="' + element_ID + '" VALUE="">';
					}
				} else {
					if (val == null) {
						val = '';
					}
					return '<INPUT ID="' + element_ID + '" CLASS="input_edit_field editable" TYPE="text" VALUE="' + val + '" MAXLENGTH=' + field['CHARACTER_MAXIMUM_LENGTH'] + ' ' + ((is_edit)?' ONKEYUP="delayFieldUpdate(this);"':'') + '>';
				}
				break;
			case 'bit':
				return '<INPUT ID="' + element_ID + '@1" TYPE=RADIO NAME="INP_' + row_ID + '_' + field['COLUMN_NAME'] + '" VALUE="' + val + '" ' + ((val)?'CHECKED':'') + ' ' + ((is_edit)?' ONCLICK="saveField(this);"':'') + '>Yes&nbsp;&nbsp;' +
						 '<INPUT ID="' + element_ID + '@0" TYPE=RADIO NAME="INP_' + row_ID + '_' + field['COLUMN_NAME'] + '" VALUE="' + val + '" ' + ((!val)?'CHECKED':'') + ' ' + ((is_edit)?' ONCLICK="saveField(this);"':'') + '>No';
				break;
			case 'int':
				//see if this is a foreign key
				for (var i = 0; i < keys.length; i++) {
					var k = keys[i];
					if (k['COLUMN_NAME'] == field['COLUMN_NAME']) {
						var h = queryHashArr(editable_tables, 'table_name', table_name);
						var filtered_field = '';
						var filtered_clause = '';
						if (missingToBlank(h, 'filtered_field') == field['COLUMN_NAME']) {
							filtered_field = h['filtered_field'];
							filtered_clause = h['filtered_clause'];
						}
						doAjaxCall(0, {"action":"get_foreign_key_data", "field_name":field['COLUMN_NAME'],
							"field_desc":field['FIELD_DESC'], "this_table_name":table_name,
							"table_name":k['REF_TABLE_NAME'], "cur_value":val,
							"row_ID":row_ID, "is_edit":is_edit,
							"filtered_field":filtered_field, "filtered_clause":filtered_clause
							}, populateFKSelect); //kick off ajax request for data
						return 'Loading...';
					}
				}
				val = (val==null)?'':val;
				return '<INPUT ID="' + element_ID + '" class="input_edit_field editable" TYPE=TEXT VALUE="' + val + '" MAXLENGTH=10 ' + ((is_edit)?' ONKEYUP="delayFieldUpdate(this);"':'') + '>';

				break;
		}
	} else {
		if (is_edit) {
			return val;
		} else {
			return 'AUTO';
		}
	}
}

function addTableRow(table_name) {
	var rand = Math.floor(Math.random() * 10000000).toString();
	var r = Element.extend($('table_data').insertRow(-1));
	r.id = 'tr@' + table_name + '@NEW' + rand;
	for (var j = 0; j < cur_table_fields.length; j++) {
		var f = cur_table_fields[j];
		var c = Element.extend(r.insertCell(-1));
		c.writeAttribute({'nowrap':true, 'id':'td_' + f['COLUMN_NAME'] + '_NEW' + rand});
		var fc = getCellContents(table_name, f, null, cur_table_foreign_keys, 'NEW'+rand, false);
		c.update(fc);
	}
	var c = Element.extend(r.insertCell(-1));
	c.update('<span class="linker" onclick="saveNewRow(this, \'' + table_name + '\', ' + rand + ');">SAVE</span>');
}

function saveNewRow(el, table_name, rand) {
	var h = queryHashArr(editable_tables, 'table_name', table_name);
	var params = {"action":"save_new_row", "table_name":table_name, "rand":rand,
		"hard_coded_field":missingToBlank(h, 'hard_coded_field'), "hard_coded_operator":missingToBlank(h, 'hard_coded_operator'), "hard_coded_value":missingToBlank(h, 'hard_coded_value')};
	var val_arr = [];
	for (var j = 0; j < cur_table_fields.length; j++) {
		if (hashHasKey(h, 'hard_coded_field')) {
			if (h['hard_coded_field_position'] == j) {
				val_arr.push(h['hard_coded_value']);
			}
		}
		var f = cur_table_fields[j];
		if (f['IS_IDENTITY'] == 0) {
			var new_value = '';
			if (f['DATA_TYPE'] == 'bit') {
				//get 1 or 0
				new_value = '0';
				if ($('inp@' + table_name + '@NEW' + rand + '@' + f['COLUMN_NAME'] + '@' + f['DATA_TYPE'] + '@1').checked) {
					new_value = '1';
				}
			} else {
				new_value = $F('inp@' + table_name + '@NEW' + rand + '@' + f['COLUMN_NAME'] + '@' + f['DATA_TYPE']);
			}
			val_arr.push(new_value);
		}
	}
	params['new_values'] = val_arr.join('~~');
	Element.extend(el).update('Saving...');
	doAjaxCall(0, params, afterSaveNewRow);
}

function afterSaveNewRow(step_num, jso, req_params) {
	//delete old row
	$('tr@' + req_params['table_name'] + '@NEW' + req_params['rand']).remove();


	var d = jso[0];
	var r = Element.extend($('table_data').insertRow(-1));
	r.id = 'tr@' + req_params['table_name'] + '@' + d['ID'];
	for (var j = 0; j < cur_table_fields.length; j++) {
		var f = cur_table_fields[j];
		var c = Element.extend(r.insertCell(-1));
		c.writeAttribute({'nowrap':true, 'id':'td_' + f['COLUMN_NAME'] + '_' + d['ID']});
		var fc = getCellContents(req_params['table_name'], f, d[f['COLUMN_NAME']], cur_table_foreign_keys, d['ID'], true);
		c.update(fc);
	}
	var c = Element.extend(r.insertCell(-1));
	c.id = 'td_delete_' + d['ID'];
	c.writeAttribute({'table_name':req_params['table_name'], 'row_ID':d['ID']});
	c.update('<span class="linker" onclick="confirmDeleteRow(this.parentElement);">DELETE</span>');

	centerPopup();
}

function populateFKSelect(step_num, jso, req_params) {
	var s = new Element('SELECT');
	if (req_params['is_edit']) {
		s.observe('change', function() {saveField(this);});
	}
	s.id = 'inp@' + req_params['this_table_name'] + '@' + req_params['row_ID'] + '@' + req_params['field_name'] + '@int';
	if (req_params['field_desc'] == 'ALLOW_NULL') {
		var op = new Option('NONE', 'NULL');
		s.options.add(op);
	}
	for (var i = 0; i < jso.length; i++) {
		var op = new Option(jso[i]['text'], jso[i]['value']);
		if (jso[i]['value'] == req_params['cur_value']) {
			op.selected = true;
		}
		s.options.add(op);
	}

	$('td_' + req_params['field_name'] + '_' + req_params['row_ID']).update(s);
}

var field_edit_timer = null;
function delayFieldUpdate(el) {
	if (field_edit_timer) {
		clearTimeout(field_edit_timer);
	}
	field_edit_timer = saveField.delay(.5, el);
}

function saveField(el) {
	el = Element.extend(el);
	var id_arr = el.id.split('@');
	el.writeAttribute({"table_name":id_arr[1], "row_ID":id_arr[2], "field_name":id_arr[3], "data_type":id_arr[4]});
	var new_value = '';
	switch (id_arr[4]) {
		case 'bit':
			new_value = id_arr[5];
			break;
		default:
			new_value = el.value;
	}
	//el.up().addClassName('saving');
	showSavingMessage(el.up(), 'save_message_' + id_arr[2] + '_' + id_arr[3], 'saving...');
	doAjaxCall(0, {"action":"save_field", "element_ID":el.id,
						"table_name":el.readAttribute('table_name'),
						"row_ID":el.readAttribute('row_ID'), "field_name":el.readAttribute('field_name'),
						"data_type":el.readAttribute('data_type'),
						"new_value":new_value}, afterFieldSave);
}

var saving_msg_timers = {};
function showSavingMessage(ref_el, el_id, msg) {
	var el = $(el_id);
	if (!el) {
		el = new Element('DIV');
		el.className = 'save_msg';
		el.id = el_id;
		ref_el.insert(el);
	}
	el.update(msg).show();
}

function afterFieldSave(step_num, jso, req_params) {
	var el_id = req_params.element_ID;
	if (saving_msg_timers[el_id]) {
		clearTimeout(saving_msg_timers[el_id]);
	}

	if (field_edit_timer) {
		clearTimeout(field_edit_timer);
	}
	//$(req_params['element_ID']).up().removeClassName('saving').addClassName('saved');
	var el = $(el_id);
	showSavingMessage(el.up(), 'save_message_' + req_params.row_ID + '_' + req_params.field_name, 'saved');
	//field_edit_timer = Element.removeClassName.delay(2, $(req_params['element_ID']).up(), 'saved');
	saving_msg_timers[el_id] = setTimeout(function() {$('save_message_' + req_params.row_ID + '_' + req_params.field_name).hide();}, 2000);

	Element.hide.delay(2, $('div_current_step_result'));
}

function confirmDeleteRow(el) {
	$(el).update('<span class="del_okay" onclick="deleteRow(this);">Ok</span><span class="del_cancel" onclick="this.parentElement.innerHTML=\'<span class=linker onclick=confirmDeleteRow(this.parentElement);>DELETE</span>\';">Cancel</span>');
}

function deleteRow(el) {
	var id_arr = el.parentElement.parentElement.id.split('@');
	el.parentElement.innerHTML = 'Deleting...';
	doAjaxCall(0, {"action":"delete_row", "table_name":id_arr[1], "row_ID":id_arr[2]}, afterDeleteRow);
}

function afterDeleteRow(step_num, jso, req_params) {
	$('tr@' + req_params['table_name'] + '@' + req_params['row_ID']).remove();
}

function closeTableEdit() {
	key_input_disabled = false;
	hidePopup();
}

var uploaders = {};
function setupUploadImage(element_ID) {
	var id_arr = element_ID.split('@');
	var row_ID = id_arr[2];
	uploaders['row'+row_ID] = new plupload.Uploader({
				runtimes : 'flash',
				browse_button : 'a_' + element_ID,
				container: 'div_upload_cont_' + row_ID,
				max_file_size : '2mb',
				url : '/library/plupload/upload.php',
				flash_swf_url : '/library/plupload/js/plupload.flash.swf',
				multipart:true,
				multipart_params: {application:'DEFECTS', field_ID:id_arr[2], file_id:id_arr[2], company_ID:company_ID},
				filters: [
					{"title":"Image Files (.jpg, .gif, .png)", "extensions":"jpg,gif,png"}
				],
				row_ID:row_ID,
				full_el_id:element_ID,
				uploaded:false
			});

	uploaders['row'+row_ID].bind('FilesAdded', function(up, files) {
			for (var i = 0; i < files.length; i++) {
				if (files[i].id) {
					$(up.settings.browse_button).hide();
					$('div_' + up.settings.full_el_id).update('uploading...');
					setTimeout(function() {up.start();}, 100);
				}
			}
		});

	uploaders['row'+row_ID].bind('Error', function(up, err) {
			alert('MAJOR ERROR UPLOADING FILE: ' + err.message);
		});

	uploaders['row'+row_ID].bind('UploadComplete', function(up, files) {
			var id_arr = up.settings.full_el_id.split('@');
			if (files.length > 0) {
				$('div_' + up.settings.full_el_id).update('<a target="_blank" href="/uploads/DEFECTS/' + company_ID + '/' + id_arr[2] + '~~' + files[0].name + '"><img src="/uploads/DEFECTS/' + company_ID + '/' + id_arr[2] + '~~' + files[0].name + '" width="50"></a>');
				$(up.settings.full_el_id).value = files[0].name;
				saveField($(up.settings.full_el_id));
				$(up.settings.browse_button).show();
				centerPopup.delay(1);
				up.removeFile(files[0]);
			} else {
				$('div_' + up.settings.full_el_id).update();
				$(up.settings.browse_button).show();
			}
		});

	uploaders['row'+row_ID].bind('UploadProgress', function(up, file) {
			$('div_' + up.settings.full_el_id).update('<div>' + file.percent + '%</div><div style="background-color:limegreen;font-size:8pt;height:15px;width:' + file.percent + '%;border:1px solid black;"></div>');
		});

	uploaders['row'+row_ID].init();
}