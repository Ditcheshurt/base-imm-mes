//STANDARD LIBRARY USED FOR STEP-BASED SHOP FLOOR SYSTEMS
//
// ** assumes step array has been defined, and template is there
//
// REVISION LOG:
//	5/31/2011 11:20:58 AM	SS - Added better error handling
//
var cur_step_num = 0;
var last_window_dimensions = {"h":0, "w":0};
function init() {
	fixScreen();
	window.onresize = fixScreen;

	for (var i = 0; i < steps.length; i++) {
		var step_num = i + 1;
		var d = new Element('DIV', {id:'div_step_' + step_num}).addClassName('step');
		d.appendChild(new Element('DIV').addClassName('step_title').update('Step ' + step_num + ': ' + steps[i]['desc']));
		d.appendChild(new Element('DIV', {id:'div_step_' + step_num + '_content'}).addClassName('step_content'));
		$('div_steps').insert(d);
	}

	setStep(1);
}

function fixScreen() {
	var d = getWindowDimensions();
	if (d.w != last_window_dimensions.w || d.h != last_window_dimensions.h) {
		last_window_dimensions = d;
		var p = $('div_popup');
		if (p) {
			centerElement(p);
		}
		$('div_right_side').style.posWidth = $(document.body).getWidth() - $('div_left_side').getWidth() - 25;
	}
}

function getWindowDimensions() {
	var d = {"h":0, "w":0};

	if (document.body && document.body.offsetWidth) {
	 d.w = document.body.offsetWidth;
	 d.h = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' &&
	    document.documentElement &&
	    document.documentElement.offsetWidth ) {
	 d.w = document.documentElement.offsetWidth;
	 d.h = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
	 d.w = window.innerWidth;
	 d.h = window.innerHeight;
	}
	return d;
}

function setStep(step_num) {
	if (step_num > steps.length) {
		finished_function();
		return;
	}

	for (var i = step_num; i <= steps.length; i++) {
		$('div_step_' + i).removeClassName('completed_step').stopObserving('click');
		$('div_step_' + i + '_content').update();
		if (i > step_num) {
			steps[i-1]['cur_value'] = null;
		}
	}
	if (step_num > 1) {
		$('div_step_' + (step_num-1)).addClassName('completed_step').observe('click', function() {setStep(step_num-1);});
	}

	$('div_current_step_title').update(steps[step_num-1]['desc']);

	if (steps[step_num-1]['help']) {
		$('div_current_step_help').update(steps[step_num-1]['help']).show();
	} else {
		$('div_current_step_help').hide();
	}

	$$('.step').each(function(e) {e.removeClassName('active_step');});
	$('div_step_' + step_num).addClassName('active_step');

	setUpInteraction(step_num);
}

function setUpInteraction(step_num) {
	//remove any existing key handlers
	resetKeys();

	cur_step_num = parseInt(step_num);
	$('div_current_step_content').update();
	$('div_right_side').update();
	switch(steps[step_num-1].type) {
		case 'scan':
			//assumes watchKeys is also included
			$('div_current_step_content').update(new Element('DIV', {id:'div_scan'}));
			if (steps[step_num-1]['numbers_only']) {numbers_only = true;} else {numbers_only = false;}
			key_display_el_name = 'div_scan';
			allow_anything = false;
			clearKeys();
			keyActionArr = [['', verifyScan, step_num]];
			updateKeys();
			break;
		case 'textentry':
			key_display_el_name = '';
			clearKeys();
			keyActionArr = [];
			var d = new Element('DIV');
			var te = new Element('TEXTAREA', {id:'ta_text_entry', 'rows':3, 'cols':50}).setStyle({'width':'95%'});
			d.insert(te);

			var d2 = new Element('DIV', {'id':'div_text_entry_finished'}).update('CLICK HERE WHEN YOU ARE FINISHED').observe('click', function() {
				steps[step_num-1]['processor'](step_num, $F('ta_text_entry'), $F('ta_text_entry'));
				hidePopup();
			});
			d.insert(d2);

			showPopup(steps[step_num-1]['help'], d);
			$('ta_text_entry').focus();
			break;
		case 'selection':
			key_display_el_name = '';
			clearKeys();
			hot_key = true;
			keyActionArr = [];
			loadOptions(step_num);
			break;
		case 'loaded_selection':
			if (steps[step_num-1]['options'].length == 0) {
				steps[step_num-1]['loader'](step_num);
			} else {
				key_display_el_name = '';
				clearKeys();
				hot_key = true;
				keyActionArr = [];
				loadOptions(step_num);
			}
			break;
		case 'custom':
			steps[step_num-1]['custom_loader'](step_num);
			break;
		default:
			break;
	}
}

function loadOptions(step_num) {
	$('div_current_step_content').update();
	var o = steps[step_num-1]['options'];
	for (var i = 0; i < o.length; i++) {

		keyActionArr.push([((i < 9)?(i+1).toString():String.fromCharCode(i + 56)), pickOption, {step_num:step_num, value:o[i][steps[step_num-1]['value_field']], text:o[i][steps[step_num-1]['text_field']]}]);

		var d = new Element('BUTTON');
		if (steps[step_num-1]['use_span']) {
			d.setStyle({display:'inline-block', fontSize:'14pt'});
		}
		d.writeAttribute(steps[step_num-1]['value_field'], o[i][steps[step_num-1]['value_field']]);
		d.writeAttribute(steps[step_num-1]['text_field'], o[i][steps[step_num-1]['text_field']]);
		d.writeAttribute('step_num', step_num);
		if (o[i]['bgcolor']) {
			d.setStyle({backgroundColor:o[i]['bgcolor']});
		} else {
			var off = [176 + (i*3), 196 + (i*3), 222 + (i*3)];
			d.setStyle({backgroundColor:('rgb(' + off[0] + ',' + off[1] + ',' + off[2] + ')')});
		}
		if (i < 9) {
			d.update('<span class=hotkey>' + (i+1).toString() + '</span> ' + o[i][steps[step_num-1]['text_field']]).addClassName('select_option');
		} else {
			d.update('<span class=hotkey>' + String.fromCharCode(i + 56) + '</span> ' + o[i][steps[step_num-1]['text_field']]).addClassName('select_option');
		}
		d.observe('click', function() {
			var s = parseInt(this.getAttribute('step_num'));
			steps[step_num-1]['processor'](step_num, this.getAttribute(steps[s-1]['value_field']), this.getAttribute(steps[s-1]['text_field']));
		});
		if (o[i]['tip']) {
			d.writeAttribute('tip', o[i]['tip']);
			d.observe('mouseover', function() {
				$('div_current_step_content').update(this.getAttribute('tip'));
			});
			d.observe('mouseout', function() {
				$('div_current_step_content').update();
			});
		}
		if (steps[step_num-1].left_side) {
			$('div_current_step_content').insert(d);
		} else {
			$('div_right_side').insert(d);
		}
	}
}

function pickOption(opt, params) {
	var step_num = params['step_num'];
	steps[step_num-1]['processor'](step_num, params['value'], params['text']);
}

function verifyScan(scan, step_num) {
	resetKeys();
	doAjaxCall(step_num, {action:steps[step_num-1]['action'], scan_value:scan}, steps[step_num-1]['processor']);
}

var clock_timer = null;
function startClock() {
	if (clock_timer) {
		clearInterval(clock_timer);
	}

	clock_timer = setInterval(updateClock, 5000);

}

function updateClock() {
	var d = new Date();
	var h = d.getHours();
	var ap = ((h > 12) ? 'PM':'AM');
	var m = d.getMinutes();
	if (m < 10) {m = '0' + m;}

	$('div_clock').update(h + ':' + m + ap);
}

function doAjaxCall(step_num, params, callback) {
	$('div_current_step_result').update('Processing...').className = 'processing';
	new Ajax.Request(ajax_processor, {
		parameters:params,
		onSuccess:function(res) {
			hideAjaxPopup();
			if (res.responseJSON) {
				$('div_current_step_result').update().className = '';
				callback(step_num, res.responseJSON, params);
			} else {
				$('div_current_step_result').update('ERROR').className = 'error';
				showPopup('ERROR PROCESSING REQUEST (1001)', 'There was a JSON error processing your request.  Please contact I.T.<br><br><div id=div_json_error>RESPONSE:<br>' + trimErrorText(res.responseText) + '</div>');
			}
		},
		onFailure:function(res) {
			var d = new Date();
			showAjaxPopup('COMMUNICATIONS ERROR PROCESSING REQUEST (1002)', 'There was a communication error processing your request.<br><br><strong>Last attempt: ' + d + '</strong><br><br>The request will be tried again in 5 seconds.<br><br><div id=div_json_error>RESPONSE:<br>' + trimErrorText(res.responseText) + '</div>');
			setTimeout(function() {
				doAjaxCall(step_num, params, callback);
			}, 5000);
		}
	});
}

function getContent(url, target_id, params, callback) {
	var action = '';
	if (params) {
		if (params['action']) {
			action = params['action'];
		}
	}
	new Ajax.Request(url, {
		method:'GET',
		parameters:params,
		onSuccess:function(res) {
			hideAjaxPopup();
			$(target_id).update(res.responseText);
			if (callback) {
				callback.defer(params);
			}
		},
		onFailure:function(res) {
			var d = new Date();
			if (res.status == 404) {
				showAjaxPopup('COMMUNICATIONS ERROR PROCESSING REQUEST (1003)', 'There was a communication error processing your request to: ' + url + '<br>DATA: ' + $H(params).toQueryString() + '<br><br><strong>Last attempt: ' + d + '</strong><br><br>The request will be tried again in 5 seconds.<br><br><div id=div_json_error>RESPONSE:<br><strong>THE PAGE CANNOT BE FOUND</strong></div>');
			} else {
				showAjaxPopup('COMMUNICATIONS ERROR PROCESSING REQUEST (1002)', 'There was a communication error processing your request to: ' + url + '<br>DATA: ' + $H(params).toQueryString() + '<br><br><strong>Last attempt: ' + d + '</strong><br><br>The request will be tried again in 5 seconds.<br><br><div id=div_json_error>RESPONSE:<br>' + trimErrorText(res.responseText) + '</div>');
			}
			setTimeout(function() {
				getContent(url, target_id, params, callback);
			}, 5000);
		}
	});
}



String.prototype.right = function(n){
	var str = this;
	if (n <= 0)
		return '';
	else if (n > String(str).length)
		return str;
	else {
		var iLen = String(str).length;
		return String(str).substring(iLen, iLen - n);
	}
};

String.prototype.left = function(len){
	return (len > this.length) ? this : this.substring(0, len);
};

if(!String.prototype.trim) {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g,'');
	};
}

function trimErrorText(err) {
	err = err.replace('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">', '');
	err = err.replace('<HTML><HEAD><TITLE>The page cannot be displayed</TITLE>', '');
	err = err.replace('<META HTTP-EQUIV="Content-Type" Content="text/html; charset=Windows-1252">', '');
	err = err.replace('<STYLE type="text/css">', '');
	err = err.replace('BODY { font: 8pt/12pt verdana }', '');
	err = err.replace('H1 { font: 13pt/15pt verdana }', '');
	err = err.replace('H2 { font: 8pt/12pt verdana }', '');
	err = err.replace('A:link { color: red }', '');
	err = err.replace('A:visited { color: maroon }', '');
	err = err.replace('</STYLE>', '');
	err = err.replace('</HEAD><BODY><TABLE width=500 border=0 cellspacing=10><TR><TD>', '');
	err = err.replace('<h1>The page cannot be displayed</h1>', '');
	err = err.replace('There is a problem with the page you are trying to reach and it cannot be displayed.', '');
	err = err.replace('<p>Please try the following:</p>', '');
	err = err.replace('<li>Contact the Web site administrator to let them know that this error has occured for this URL address.</li>', '');
	err = err.replace('<h2>HTTP 500.100 - Internal server error: ASP error.<br>Internet Information Services</h2>', '');
	err = err.replace('<p>Technical Information (for support personnel)</p>', '');
	err = err.replace('<li>More information:<br>', '');
	err = err.replace('<li>Click on <a href="http://www.microsoft.com/ContentRedirect.asp?prd=iis&sbp=&pver=5.0&ID=500;100&cat=Microsoft+OLE+DB+Provider+for+SQL+Server&os=&over=&hrd=&Opt1=&Opt2=%2D2147217900&Opt3=Invalid+column+name+%27active%27%2E">Microsoft Support</a> for a links to articles about this error.</li>', '');
	err = err.replace('<li>Go to <a href="http://go.microsoft.com/fwlink/?linkid=8180" target="_blank">Microsoft Product Support Services</a> and perform a title search for the words <b>HTTP</b> and <b>500</b>.</li>', '');
	err = err.replace('<li>Open <b>IIS Help</b>, which is accessible in IIS Manager (inetmgr), and search for topics titled <b>Web Site Administration</b>, and <b>About Custom Error Messages</b>.</li>', '');
	err = err.replace('<li>In the IIS Software Development Kit (SDK) or at the <a href="http://go.microsoft.com/fwlink/?LinkId=8181">MSDN Online Library</a>, search for topics titled <b>Debugging ASP Scripts</b>, <b>Debugging Components</b>, and <b>Debugging ISAPI Extensions and Filters</b>.</li>', '');
	err = err.replace('</TD></TR></TABLE></BODY></HTML>', '');
	return err;
}