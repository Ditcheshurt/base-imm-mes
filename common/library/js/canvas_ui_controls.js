var CANVAS = {};

CANVAS.createDonutChart = function(container, size, fill_color, val, max) {
	var d = document.getElementById(container);
	var c = document.createElement('CANVAS');
	c.height = size;
	c.width = size;
	d.innerHTML = '';
	d.appendChild(c);

	var ctx = c.getContext("2d");
	var off = Math.PI * -0.5;
	var marg = 5;
	var cw = c.width;
	var ch = c.height;

	if (val == max) {
		fill_color = 'limegreen';
	}

	ctx.clearRect(0, 0, cw, ch);

	//draw the chunk of pie
	ctx.beginPath();
	ctx.moveTo(cw/2, ch/2);
	ctx.strokeStyle = fill_color;
	ctx.lineTo(cw/2,marg);
	ctx.stroke();
	ctx.arc(cw/2,ch/2, (cw/2)-marg, off, 2*Math.PI*(parseFloat(val)/parseFloat(max)) + off);
	ctx.stroke();
	ctx.lineTo(cw/2,ch/2);
	ctx.stroke();
	ctx.fillStyle = fill_color;
	ctx.fill();

	//fill middle with white circle
	ctx.beginPath();
	ctx.strokeStyle="#FFFFFF";
	ctx.arc(cw/2,ch/2,((cw/2)-marg)/2,0,2*Math.PI);
	ctx.stroke();
	ctx.fillStyle="white";
	ctx.fill();

	//draw text
	ctx.fillStyle="black";
	ctx.font = (ch/5) + "px Arial";
	var text = (max-val).toString();
	var metrics = ctx.measureText(text);
	var w = metrics.width;
	var h = (ch/5);
	ctx.fillText(text,cw/2-(w/2),(ch/2)+(h/4));

	if (val == max) {
		//get happy
		ctx.beginPath();
		ctx.strokeStyle="#FFFFFF";
		ctx.arc(cw/2,ch/2,((cw/2)+marg)/2,0,2*Math.PI);
		ctx.stroke();
		ctx.fillStyle="white";
		ctx.fill();

		//left eye (from TLC)
		ctx.beginPath();
		ctx.strokeStyle="#000000";
		ctx.arc(cw/3,ch/3,cw/8,0,2*Math.PI);
		ctx.stroke();
		ctx.fillStyle="#000000";
		ctx.fill();

		//right eye
		ctx.beginPath();
		ctx.strokeStyle="#000000";
		ctx.arc(2*cw/3,ch/3,cw/8,0,2*Math.PI);
		ctx.stroke();
		ctx.fillStyle="#000000";
		ctx.fill();

		ctx.beginPath();
		ctx.lineWidth = cw/10;
		ctx.arc(cw/2,ch/2, (cw/2)-3*marg, Math.PI*.25, Math.PI*.75);
		ctx.stroke();
	}
};