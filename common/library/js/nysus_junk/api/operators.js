var NYSUS = NYSUS || {};

NYSUS['api_folder'] = '/common/api/';

//first parameter will override properties of the 2nd parameter
function merge(obj1, obj2) {
	for (attr in obj1)
		obj2[attr] = obj1[attr];
	return obj2;
}

// merge this into NYSUS.OJT
merge({

	get_operator: function (operator_ID, cb) {
		var processor = NYSUS.api_folder + 'operators.php';
		var params = '';

		operator_ID ? params += 'operator_ID=' + operator_ID : null;

		NYSUS.OJT.ajax(processor, params, cb);
	},

	get_operators: function(with_inactive, cb) {
		var xhttp = new XMLHttpRequest();
		var url = NYSUS['api_folder'] + 'operators.php';
		var params = with_inactive != null && with_inactive ? 'with_inactive=1' : 'with_inactive=0';

		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (typeof cb == 'function') {
					cb(JSON.parse(xhttp.responseText));
				}
			}
		};

		xhttp.open("POST", 'http://' + document.location.host + url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(params);

	}

}, NYSUS);