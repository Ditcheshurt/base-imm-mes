function Modal (modal_id, bootstrap, options) {
	options = options || {}; //default content
	options['header'] = options['header'] || '';
	options['body'] = options['body'] || '';
	
	
	this.class_prefix = bootstrap ? '' : 'nysus-';
	this.modal = document.createElement('div');
	this.modal.id = modal_id;
	this.modal.className = this.class_prefix + 'modal';
	this.dialog = document.createElement('div');
	this.dialog.className = this.class_prefix + 'modal-dialog';
	this.content = document.createElement('div');
	this.content.className = this.class_prefix + 'modal-content';
	this.header = document.createElement('div');
	this.header.className = this.class_prefix + 'modal-header';
	this.header.innerHTML = options.header;
	this.body = document.createElement('div');
	this.body.className = this.class_prefix + 'modal-body';
	this.body.innerHTML = options.body;
	this.footer = document.createElement('div');
	this.footer.className = this.class_prefix + 'modal-footer';
	this.close_butt = document.createElement('input');
	this.close_butt.type = 'button';
	this.close_butt.className = bootstrap ? 'btn btn-xs btn-danger btn-close' : this.class_prefix + 'btn-close';
	this.close_butt.value = 'Close';

	this.modal.appendChild(this.dialog);
	this.dialog.appendChild(this.content);
	this.content.appendChild(this.header);
	this.content.appendChild(this.body);
	this.content.appendChild(this.footer);
	this.footer.appendChild(this.close_butt);
	
	this.modal.style.display = 'none';
	document.getElementsByTagName('body')[0].appendChild(this.modal);
}

Modal.prototype.show = function () {
	this.modal.style.display = 'block';
	window.addEventListener('click', this, false);
};

Modal.prototype.hide = function () {
	this.modal.style.display = 'none';
};

Modal.prototype.handleEvent = function(event) {
	if (event.target == this.modal || event.target == this.close_butt) {
		this.hide();
		window.removeEventListener('click', this, false);
	}
};