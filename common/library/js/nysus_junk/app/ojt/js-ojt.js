// do_ojt is the magic do_ojt(ojt_group_ID, operator_ID, with_alerts)
//do_ojt(6, 1005, 1); // get the operator from the login

var NYSUS = NYSUS || {};
NYSUS.OJT = NYSUS.OJT || [];

//first parameter will override properties of the 2nd parameter
function merge(obj1, obj2) {
	for(attr in obj1)
		obj2[attr]=obj1[attr];
	return obj2;
}

// merge this into NYSUS.OJT
merge({
	do_ojt: function (ojt_group_ID, operator_ID, with_alerts, cb) {

		// get the group operator, it it doesnt exist then he isnt valid to login
		NYSUS.OJT.get_group_operator(ojt_group_ID, operator_ID, function (operator) {

			if (operator != null) {
				// get group requirements
				NYSUS.OJT.get_group_requirements(ojt_group_ID, with_alerts, function (jso) {
					
					NYSUS.OJT.current_requirements = (jso || []);
					
					//update buttons
					NYSUS.OJT.update_buttons(jso || []);
					
					if (jso && jso.length > 0) {
						var current_index = 0;
						var after_signoff_requirement = function () {
							current_index++;
							if (current_index < jso.length) {
								NYSUS.OJT.signoff_requirement(jso[current_index], operator_ID, after_signoff_requirement);
							} else {
								var modal = new Modal('ojt_complete');

								modal.header.innerText = 'OJT Requirements';
								modal.body.innerText = "SUCCESS!! OJT REQUIREMENTS COMPLETE...";
								modal.show();

								setTimeout(function () {
									var modal = document.getElementById('ojt_complete');
									if (modal) {
										modal.parentNode.removeChild(modal);
									}
									if (typeof cb == 'function') {
										cb();
									}
								}, 1000);
							}
						};
						// signoff on requirement
						NYSUS.OJT.signoff_requirement(jso[current_index], operator_ID, after_signoff_requirement);
					}
				});
			} else {
				var modal = new Modal('ojt_login_failed');

				modal.header.innerText = 'OJT GROUP RESTRICTION';
				modal.body.innerText = "LOGIN FAILED!! OPERATOR NOT CONFIGURED FOR THIS STATION IN OJT!!";
				modal.show();
			}
		});
	},

	signoff_requirement: function (requirement, operator_ID, cb) {
		var with_alerts = 1;
		var with_expired = 0;
		var ojt_requirement_ID = requirement.ojt_requirement_ID;
		var ojt_signoff_template_ID = requirement.ojt_signoff_template_ID;

		// get the requirement roles
		NYSUS.OJT.get_template_roles(ojt_signoff_template_ID, function (roles) {
			// create array of unfulfilled roles
			var incomplete_roles = [];

			// get the operator requirement signoffs
			NYSUS.OJT.get_operator_requirement_signoffs(operator_ID, ojt_requirement_ID, with_alerts, with_expired, function (signoffs) {
				// show each role
				for (var i = 0; i < roles.length; i++) {
					var role = roles[i];
					// lock down to first level
					if (role.level_order == 10) {
						var complete_signoff = null;
						for (var j = 0; j < signoffs.length; j++) {
							var signoff = signoffs[j];
							if (signoff.requirement_ID == requirement.ojt_requirement_ID && role.signoff_level_role_ID == signoff.signoff_level_role_ID) {
								complete_signoff = signoff;
							}
						}
						if (!complete_signoff) {
							incomplete_roles.push(role);
						}
					}
				}
				// check for more imcomplete signoff roles
				if (incomplete_roles.length > 0) {
					NYSUS.OJT.show_ojt_modal();
					var title = document.getElementsByClassName('nysus-modal-header');
					var container = document.getElementsByClassName('ojt_container');
					title[0].innerText = requirement.requirement_description;
					
					var txt = document.createElement('DIV');
					txt.className = 'well';
					txt.innerHTML = requirement.requirement_text || '';
					container[0].appendChild(txt);

					if (requirement.image_url) {
						NYSUS.OJT.show_image(requirement.image_url);
					} else if (requirement.video_url) {
						NYSUS.OJT.show_video(requirement.video_url);
					} else {
						var s = document.createElement('span');
						s.innerText = 'NO CONTENT FOR THIS REQUIREMENT';
						container[0].appendChild(s);
					}
					NYSUS.OJT.show_signoff_control(requirement, operator_ID, incomplete_roles, cb);
				} else {
					if (typeof cb == 'function') {
						cb();
					}
				}

			});
		});

	},

	show_ojt_modal: function () {
		// get modal if it exists then create one
		var modal = document.getElementById('requirements-modal');

		if (modal) {
			modal.parentNode.removeChild(modal);
		}

		modal = new Modal('requirements-modal');
		var container = document.createElement('div');
		var image_container = document.createElement('div');
		var signoff_container = document.createElement('div');

		container.className = 'ojt_container';
		image_container.className = 'ojt_image';
		signoff_container.className = 'ojt_signoff';

		container.appendChild(signoff_container);
		container.appendChild(image_container);

		modal.header.innerText = 'OJT Requirement';
		modal.body.appendChild(container);
		modal.show();

	},

	show_signoff_control: function (requirement, operator_ID, incomplete_roles, cb) {
		var modal = document.getElementById('requirements-modal');
		var container = document.getElementsByClassName('ojt_signoff');
		var label = document.createElement('div');
		var ctl = document.createElement('span');
		var role_index = 0;

		ctl.id = 'ojt_role_scan';
		ctl.style.float = 'right';
		container[0].appendChild(ctl);
		container[0].appendChild(label);

		var signoff_role = function () {

			if (role_index < incomplete_roles.length) {
				var role = incomplete_roles[role_index];

				label.innerHTML = '<span>SCAN REQUIRED FROM <b>' + role.role_description.toUpperCase() + ' ROLE</b> TO COMPLETE THIS REQUIREMENT.</span>';

				KEYS.resetKeys();
				KEYS.registerKeyHandler({
					"name": "ojt_role_scan",
					"global_match": false,
					"match_keys": null,
					"mask_character":"*",
					"display_el_id": "ojt_role_scan",
					"callback": function (scan) {
						KEYS.resetKeys();
						// get the operator signing off
						NYSUS.OJT.get_operator(scan, function(signoff_operator) {
							if (signoff_operator && signoff_operator.length > 0) {
								// get signoff operator roles
								NYSUS.OJT.get_operator_roles(signoff_operator[0].ID, function(signoff_operator_roles) {
									var has_role = false;
									for(var i = 0; i < signoff_operator_roles.length; i++) {
										var sorole = signoff_operator_roles[i];
										if (sorole.ID == role.role_ID) {
											has_role = true;
											break;
										}
									}
									if (has_role) {
										// do signoff
										NYSUS.OJT.create_signoff(requirement.ojt_requirement_ID, operator_ID, signoff_operator[0].ID, role.signoff_level_role_ID, function (jso) {
											if (jso && jso.length > 0) {
												role_index++;
												label.innerHTML = '<div class="nysus-success"><span>SUCCESS!! </span><span><b>' + role.role_description.toUpperCase() + '</b> role complete.</span></div>';
											} else {
												label.innerHTML = '<div class="nysus-danger"><span>FAILED!! Could not save signoff. </span><span>Scan badge.</span></div>';
											}
										});
									} else {
										label.innerHTML = '<div class="nysus-danger"><span>FAILED!! Operator does not have this role, or invalid badge scanned.</span><span>Scan another badge.</span></div>';
									}
									setTimeout(signoff_role, 1000);

								});
							} else {
								label.innerHTML = '<div class="nysus-danger"><span>FAILED!! Operator does not have this role, or invalid badge scanned.</span><span>Scan another badge.</span></div>';
								setTimeout(signoff_role, 1000);
							}
						});

					}
				});

			} else {
				if (typeof cb == 'function') {
					KEYS.resetKeys();
					if (modal) {
						modal.parentNode.removeChild(modal);
					}
					cb();
				}
			}
		};
		signoff_role();

	},

	show_image: function (url) {
		var container = document.getElementsByClassName('ojt_image');
		var image = null;

		if (url) {
			if (url.indexOf('.pdf') >= 0) {
				image = document.createElement('iframe');
				image.src = '../common/library/pdfjs/web/viewer.html?file=../../../../' + url;
			} else {
				image = document.createElement('img');
				image.src = url;
				image.alt = 'IMAGE UNAVAILABLE...';
			}
			image.style.width = '100%';
			image.style.height = '100%';
			container[0].appendChild(image);
		} else {
			var s = document.createElement('span');

			s.innerText = 'NO IMAGE AVAILABLE!';
			container[0].appendChild(s);
		}
	},

	show_video: function (url) {
		var container = document.getElementsByClassName('ojt_image');
		var video = null;

		if (url) {
			if (url.indexOf('https://www.youtube.com/watch?v=') >= 0) {
				url = url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
			}
			video = document.createElement('iframe');
			video.src = url;
			video.style.width = '100%';
			video.style.height = '100%';
		} else {
			var s = document.createElement('span');

			s.innerText = 'NO VIDEO AVAILABLE!';
			container[0].appendChild(s);
		}
		container[0].appendChild(video);
	},
	
	update_buttons: function(requirements) {
		var btns = document.getElementsByClassName('btn-ojt-requirements');
		if (btns.length == 0) {
			return;
		}
		
		for (var i = 0 ; i < btns.length; i++) {
			var b = btns[i];
			b.disabled = true;
			b.onclick = null; //disable and remove all event handlers
			
			b.getElementsByClassName('badge')[0].innerHTML = '';
			b.onclick = NYSUS.OJT.review_requirements;
		}
		
		var num_alerts = 0;
		var num_training = 0;
		for (var i= 0; i < requirements.length; i++) {
			if (requirements[i].is_alert) {
				num_alerts++;
			} else {
				num_training++;
			}
		}
		
		document.getElementsByClassName('ojt-requirement-training')[0].getElementsByClassName('badge')[0].innerHTML = num_training;
		document.getElementsByClassName('ojt-requirement-training')[0].disabled = (num_training == 0);
		
		document.getElementsByClassName('ojt-requirement-alerts')[0].getElementsByClassName('badge')[0].innerHTML = num_alerts;
		document.getElementsByClassName('ojt-requirement-alerts')[0].disabled = (num_alerts == 0);
	},
	
	review_requirements: function(event) {
		var el = event.target;
		var requirement_type = el.attributes['data-ojt-requirement-type'].value;
		var is_alert = (requirement_type == 'ALERTS');
		var modal = new Modal('ojt_review');
		
		var u = document.createElement('UL');
		u.className = 'ojt-requirement-list';
		for (var i = 0; i < NYSUS.OJT.current_requirements.length; i++) {
			var r = NYSUS.OJT.current_requirements[i];
			if (r.is_alert == is_alert) {
				var l = document.createElement('LI');
				l.innerHTML = r.requirement_description;
				l.onclick = NYSUS.OJT.review_requirement;
				
				l.setAttribute('requirement_index', i);
				
				u.appendChild(l);
			}
		}

		modal.header.innerHTML = 'Review OJT Requirements: ' + requirement_type;
		modal.body.innerHTML = '';
		modal.body.appendChild(u);
		modal.show();
	},
	
	review_requirement: function(event) {
		var el = event.target;
		var requirement_index = parseInt(el.getAttribute('requirement_index'));
		for (var i = 0; i < NYSUS.OJT.current_requirements.length; i++) {
			if (i == requirement_index) {
				var requirement = NYSUS.OJT.current_requirements[i];
				
				var modal = document.getElementById('requirement-modal');

				if (modal) {
					modal.parentNode.removeChild(modal);
				}

				modal = new Modal('requirement-modal', false, {header:requirement.requirement_description, body:(requirement.requirement_text || '')});
				var container = document.createElement('div');
				var image_container = document.createElement('div');

				container.className = 'ojt_container';
				image_container.className = 'ojt_image';

				container.appendChild(image_container);
				modal.body.appendChild(container);

				if (requirement.image_url) {
					NYSUS.OJT.show_image(requirement.image_url);
				} else if (requirement.video_url) {
					NYSUS.OJT.show_video(requirement.video_url);
				} else {
					var s = document.createElement('span');
					s.innerText = 'NO CONTENT FOR THIS REQUIREMENT';
					container[0].appendChild(s);
				}

				modal.show();
				
				return;
			}
		}
	}
}, NYSUS.OJT);