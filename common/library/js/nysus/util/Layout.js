/*
	options = {
		rows: {}
	}
 */

function Layout(el, options) {
	this.defaults = [];
	this.container = document.body.getElementById(el);

	for (attr in options)
		defaults[attr] = options[attr];

	this.build();

	slices.split(' ').forEach(this.buildSlice.bind(this));

	document.body.appendChild(this.container);
}

Layout.prototype.createElement = function (element, attribute, inner) {
	if (typeof(element) === "undefined") {
		return false;
	}
	if (typeof(inner) === "undefined") {
		inner = "";
	}
	var el = document.createElement(element);
	if (typeof(attribute) === 'object') {
		for (var key in attribute) {
			el.setAttribute(key, attribute[key]);
		}
	}
	if (!Array.isArray(inner)) {
		inner = [inner];
	}
	for (var k = 0; k < inner.length; k++) {
		if (inner[k].tagName) {
			el.appendChild(inner[k]);
		} else {
			el.appendChild(document.createTextNode(inner[k]));
		}
	}
	return el;
};

Layout.prototype.build = function () {
	//var r = this.createElement('div', {'class': 'row'});
	//for (var i = 0; i < slice; i++) {
	//	var row = this.container.children.length + 1;
	//	var d = this.createElement('div', {'id': 'r' + row + 'c' + i});
	//	r.appendChild(d);
	//}
	//this.container.appendChild(r);
};