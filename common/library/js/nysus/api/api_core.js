var NYSUS = NYSUS || {};

NYSUS['api_folder'] = '/common/api/';

//first parameter will override properties of the 2nd parameter
function merge(obj1, obj2) {
	for (attr in obj1)
		obj2[attr] = obj1[attr];
	return obj2;
}

merge({

	ajax: function (url, params, cb) {
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (typeof cb == 'function') {
					cb(JSON.parse(xhttp.responseText));
				}
			}
		};
		xhttp.open("POST", 'http://' + document.location.host + url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(params);
	}

}, NYSUS);