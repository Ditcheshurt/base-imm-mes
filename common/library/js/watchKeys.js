// Nysus Key Handler
// 6/16/2014 8:45:00 AM - SS - Removed prototype reliance
//

var keys = '';
var keyActionArr = [];
var key_display_el_name = 'div_keys';
var numbers_only = false;
var hot_key = false;
var allow_anything = false;

if (typeof window.event != 'undefined' || !!window.chrome) {
	document.onkeydown = gotKey;
} else {
	document.onkeypress = gotKey;
}

function gotKey(e) {
	if (keyActionArr.length == 0) {
		return;
	}

	var kc;
	if (window.event) {
		kc = event.keyCode;
	} else {
		kc=e.which;
	}

	if (kc == 8) {
		if (keys != '') {
			keys = keys.substring(0, keys.length - 1);
			updateKeys();
		}
		return false;
	}
	if (kc == 13) {
		checkKeys();
	} else {
		if ((kc >= 65 && kc <= 90) || (kc >= 96 && kc <= 122) || (kc >= 48 && kc <= 57) || kc == 189 || kc == 45 || allow_anything) {
			if (kc == 189) {
				if (event.shiftKey) {
					kc = 95;
				} else {
					kc = 45;
				}
			}

			if (kc >= 96 && kc <= 105) {
				kc -= 48;
			}

			if (numbers_only && !(kc >= 48 && kc <= 57)) {
				return;
			}

			keys += String.fromCharCode(kc).toUpperCase();
			updateKeys();

			if (hot_key) {
				checkKeys();
				clearKeys();
			}
		}
	}

	return true;
}

function updateKeys() {
	var k = document.getElementById(key_display_el_name);
	if (!k) {
		return;
	}

	if (keyActionArr.length > 0) {
		k.innerHTML = keys + '<span class="fake_cursor">&nbsp;</span>';

		if (keys == '') {
			//$(key_display_el_name).hide();
		} else {
			if (k.tagName == 'SPAN') {
				k.style.display = 'inline-block';
			} else {
				k.style.display = 'block';
			}
		}
	} else {
		//k.hide();
		k.style.display = 'none';
	}
}

function forceUpdateKeys() {
	if (key_display_el_name != '') {
		var k = document.getElementById(key_display_el_name);
		if (k) {
			k.innerHTML = keys + '<span class="fake_cursor">&nbsp;</span>';
		}
	} else {
		return;
	}
	if (keys == '') {
		//$(key_display_el_name).hide();
		k.style.display = 'none';
	} else {
		if (k.tagName == 'SPAN') {
			document.getElementById(key_display_el_name).style.display = 'inline-block';
		} else {
			document.getElementById(key_display_el_name).style.display = 'block';
		}
	}
}

function clearKeys() {
	keys = '';
	forceUpdateKeys();
}

function checkKeys() {
	for (var i = 0; i < keyActionArr.length; i++) {
		if (keys == keyActionArr[i][0]) {
			keyActionArr[i][1](keys, keyActionArr[i][2]);
			return;
		}
		if (keyActionArr[i][0] == '') {
			if (keyActionArr[i][2]) {
				keyActionArr[i][1](keys, keyActionArr[i][2]);
			} else {
				keyActionArr[i][1](keys);
			}
			return;
		}
	}
	clearKeys();
}

function resetKeys() {
	key_display_el_name = '';
	numbers_only = false;
	hot_key = false;
	keyActionArr = [];
	clearKeys();
}