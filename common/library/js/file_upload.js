//assumes prototype library is used
//this variable needs to be set
var file_info_url = '';
var file_upload_url = '';
function getFileTable(el, new_rec_rand, rec_ID, msg, msg_status, allow_add, allow_delete) {
	new Ajax.Request(file_info_url, {
		parameters:{"action":"get_file_info", "rec_ID":rec_ID, "new_rec_rand":new_rec_rand},
		onSuccess:function(res) {
			var root_folder = res.responseJSON['root_folder'];
			var jso = res.responseJSON['files'];
			var t = new Element('TABLE').addClassName('file_table').setStyle({'border':'1px solid lightsteelblue'});
			var file_arr = [];
			var r;
			if (jso.length > 0) {
				r = Element.extend(t.insertRow(-1)).setStyle({'fontWeight':'bold'});
				['File Name', 'Description', 'Date', 'By'].each(function(a) {Element.extend(r.insertCell(-1)).update(a).setStyle({'borderBottom':'1px solid lightsteelblue', 'borderRight':'1px solid lightsteelblue'});});
				if (allow_delete) {
					Element.extend(r.insertCell(-1)).update('&nbsp;');
				}
				for (var i = 0; i < jso.length; i++) {
					r = Element.extend(t.insertRow(-1));
					var temp_arr = [];
					['file_name', 'desc', 'uploaded_at', 'uploaded_by'].each(
						function(a) {
							if (a == 'file_name') {
								Element.extend(r.insertCell(-1)).update('<a target="_blank" href="' + root_folder + jso[i][a] + '">' + jso[i][a] + '</a>');
							} else {
								Element.extend(r.insertCell(-1)).update(jso[i][a]);
							}
							temp_arr.push(jso[i][a]);
						}
					);
					file_arr.push(temp_arr.join('@@'));
					if (allow_delete) {
						Element.extend(r.insertCell(-1))
							.update('<img src="/images/tinydelete.gif" class="linker" onclick="deleteFile(\'' + jso[i]['file_name'] + '\', \'' + rec_ID + '\', \'' + new_rec_rand + '\');">');
					}
				}
			} else {
				r = Element.extend(t.insertRow(-1));
				c = Element.extend(r.insertCell(-1)).writeAttribute({"colSpan":4}).update('<i>No files added.</i>');
			}
			if (allow_add) {
				//add another row for uploading
				r = Element.extend(t.insertRow(-1));
				var qs = 'style_sheet=' + style_sheet;
				qs += '&file_upload_url=' + file_upload_url;
				qs += '&rec_ID=' + rec_ID;
				qs += '&new_rec_rand=' + new_rec_rand;
				qs += '&user_name=' + user_name;
				qs += '&num_files=' + jso.length;
				var ifrm = '<iframe src="/common/library/file_upload_frame.asp?' + qs + '" height="25" width="700" frameborder="0" style="borderTop:1px solid lightsteelblue;"></iframe>';
				c = Element.extend(r.insertCell(-1)).writeAttribute({"colSpan":4}).update(ifrm);
			}

			if (msg != '') {
				r = Element.extend(t.insertRow(-1));
				c = Element.extend(r.insertCell(-1)).writeAttribute({"colSpan":4, "align":"center"}).update(msg);
				if (msg_status == 1) {
					c.setStyle({"color":"limegreen"});
				}
			}
			el.update(t);

			el.insert('<input type="hidden" id="inp_field_' + rec_ID.split('_')[2] + '" value="' + file_arr.join('~~') + '">');
			if (new_rec_rand != '') {
				el.insert('<input type="hidden" id="inp_field_' + rec_ID.split('_')[2] + '_rand" value="' + new_rec_rand + '">');
			}
		}
	});
}

function deleteFile(file_name, rec_ID, new_rec_rand) {
	if (confirm('Are you sure you want to delete the file: ' + file_name + '?  This cannot be undone.')) {
		new Ajax.Request(file_info_url, {
			parameters:{"action":"delete_file", "file_name":file_name, "rec_ID":rec_ID, "new_rec_rand":new_rec_rand},
			onSuccess:function(res) {
				getFileTable($('div_' + rec_ID), new_rec_rand, rec_ID, 'File ' + file_name + ' deleted!', 1, true, true);
			}
		});
	}
}