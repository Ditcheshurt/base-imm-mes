var KEYS = {
	"current_keys":"",
	"key_action_arr":[],  //array of key event handlers in form of: {"name":"", "global_match":true/false, "match_keys":"", "callback":function() {}, "display_el_id":""}
	"key_default_display":"div_key_default_display",
	"key_max_length":40,
	"backup_key_action_arr":[],
	"halted":false
};

if (typeof window.event != 'undefined') {
	document.onkeydown = gotKey;
} else {
	document.onkeydown = gotKey;
}

function gotKey(e) {
	if (KEYS.key_action_arr.length == 0) {
		return;
	}
	
	if (KEYS.halted) {
		return;
	}

	var kc;
	var is_FF = false;
	if (Prototype.Browser.is_IE) {
		kc = event.keyCode;
	} else {
		kc=e.which;
		is_FF = true;
		//alert(kc);
	}

	if (kc == 8) {
		if (KEYS.current_keys != '') {
			KEYS.current_keys = KEYS.current_keys.substring(0, KEYS.current_keys.length - 1);
			KEYS.updateKeys();
		}
		return false;
	}
	if (kc == 13) {
		KEYS.checkKeys();
	} else {
		if ((kc >= 65 && kc <= 90) || (kc == 91 && is_FF) || (kc == 93 && is_FF) || (kc >= 96 && kc <= 122) || (kc >= 48 && kc <= 57) || kc == 189 || kc == 45 || kc == 219 || kc == 221) {
			if (kc == 189) {
				if (event.shiftKey) {
					kc = 95;
				} else {
					kc = 45;
				}
			}

			if (kc >= 96 && kc <= 105) {kc -= 48;}  //number pad

			if (kc == 219) {kc = 91;}  //[
			if (kc == 221) {kc = 93;}  //]

//         if (!(kc >= 48 && kc <= 57)) {
//         	return;
//         }

			KEYS.current_keys += String.fromCharCode(kc).toUpperCase();
			KEYS.updateKeys();
		} else {
			console.log('Discarded key: ' + kc);
		}
	}

	return true;
};

KEYS.backupKeys = function() {
	KEYS.backup_key_action_arr = KEYS.key_action_arr.clone();
};

KEYS.restoreKeys = function() {
	KEYS.key_action_arr = KEYS.backup_key_action_arr.clone();
	KEYS.backup_key_action_arr = [];
};

//params expects {name, global_match, match_keys, callback, display_el_id}
// optional: mask_character
KEYS.registerKeyHandler = function(params) {
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (KEYS.key_action_arr[i]['name'] == params['name']) {
			return;
		}
	}

	KEYS.key_action_arr.push(params);
}

KEYS.unregisterKeyHandler = function(name) {
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (KEYS.key_action_arr[i]['name'] == name) {
			KEYS.key_action_arr.splice(i, 1);
			return;
		}
	}
}

//this fires on every keypress
KEYS.updateKeys = function() {
	var k = $(KEYS.key_default_display);
	var specific_found = false;
	if (!k) {
		return;  //not set up properly
	}

	//if there are any specific handlers defined, update those targets, otherwise update the default
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (KEYS.key_action_arr[i]['display_el_id'] != null) {
			var el = $(KEYS.key_action_arr[i]['display_el_id']);
			if (el) {
				if (KEYS.key_action_arr[i]['mask_character']) {
					el.update(Array(KEYS.current_keys.length + 1).join(KEYS.key_action_arr[i]['mask_character']));
				} else {
					el.update(KEYS.current_keys);
				}
				specific_found = true;
			}
		}
	}

	if (!specific_found) {
		k.update(KEYS.current_keys);
	}
}

KEYS.clearKeys = function() {
	KEYS.current_keys = '';
	KEYS.updateKeys();
}

//this fires on the enter key only
KEYS.checkKeys = function() {
	//handle 3 scenarios (in this order):
	//
	//  1: Exact scan for global matching.  i.e. 'LOGOUT', 'REFRESH'
	//  2: Exact scan for specific interaction, and update specific display.  i.e. Scan 'CONTINUE' to continue, 'BYPASS' to bypass
	//  3: Any scan for specific interaction, and update specific display

	//Case 1...find global matching
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (KEYS.key_action_arr[i]['global_match']) {
			if (KEYS.current_keys == KEYS.key_action_arr[i]['match_keys']) {
				KEYS.key_action_arr[i]['callback'](KEYS.current_keys);
				KEYS.clearKeys();
				return;
			}
		}
	}

	//Case 2...specifc match
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (!KEYS.key_action_arr[i]['global_match'] && KEYS.key_action_arr[i]['match_keys'] != null) {
			if (KEYS.current_keys == KEYS.key_action_arr[i]['match_keys']) {
				KEYS.key_action_arr[i]['callback'](KEYS.current_keys, KEYS.key_action_arr[i]);
				KEYS.clearKeys();
				return;
			}
		}
	}

	//Case 3...any scan for a specifc interaction
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (!KEYS.key_action_arr[i]['global_match'] && KEYS.key_action_arr[i]['match_keys'] == null) {
			KEYS.key_action_arr[i]['callback'](KEYS.current_keys, KEYS.key_action_arr[i]);
			KEYS.clearKeys();
			return;
		}
	}
	KEYS.clearKeys();
}

KEYS.resetKeys = function() {
	KEYS.clearKeys();
	//get rid of all specific handlers
	for (var i = 0; i < KEYS.key_action_arr.length; i++) {
		if (!KEYS.key_action_arr[i]['global_match']) {
			KEYS.key_action_arr.splice(i, 1);
			i--;
		}
	}
}