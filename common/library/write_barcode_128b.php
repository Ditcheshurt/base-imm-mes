<?php

// The following function can be used to generate Code 128 B barcodes.
function write_barcode_128b($input_string) {
    
	if (!defined("ASCII_DEL"))	define("ASCII_DEL",       195);
	if (!defined("ASCII_FNC3"))	define("ASCII_FNC3",      196);
	if (!defined("ASCII_FNC2"))	define("ASCII_FNC2",      197);
	if (!defined("ASCII_SHIFT"))	define("ASCII_SHIFT",     198);
	if (!defined("ASCII_CODEC"))	define("ASCII_CODEC",     199);
	if (!defined("ASCII_FNC4"))	define("ASCII_FNC4",      200);
	if (!defined("ASCII_CODEA"))	define("ASCII_CODEA",     201);
	if (!defined("ASCII_FNC1"))	define("ASCII_FNC1",      202);
	if (!defined("ASCII_STARTA"))	define("ASCII_STARTA",    203);
	if (!defined("ASCII_STARTB"))	define("ASCII_STARTB",    204);
	if (!defined("ASCII_STARTC"))	define("ASCII_STARTC",    205);
	if (!defined("ASCII_STOP"))	define("ASCII_STOP",      206);
    
	$valid_ascii['min'] = 32;
	$valid_ascii['max'] = 126;
    
    $bar['width'] = 2;
	$bar['height'] = 70;
	
    $char_setup[] = array();
    for ($char_setup = 0; $char_setup <= 105; $char_setup++) {
        if ($char_setup > 94) {
            $char_value[$char_setup + 100] = $char_setup;
        } else {
            $char_value[$char_setup + 32] = $char_setup;
        }
    }
    
	// Encode the input string
	$input_string = trim($input_string);
    
	$valid_barcode = true;
	$check_digit_value = $char_value[ASCII_STARTB];
	for ($char_pos = 0; $char_pos < strlen($input_string); $char_pos++) {
	    $char_ascii = ord($input_string[$char_pos]);
	    if (($char_ascii < $valid_ascii['min']) || ($char_ascii > $valid_ascii['max'])) {
			$char_ascii = ord("?");
			$valid_barcode = false;
        }
	    $check_digit_value = $check_digit_value + ($char_value[$char_ascii] * ($char_pos + 1));
    }
	$check_digit_value = ($check_digit_value % 103);
    
	if ($check_digit_value < 95) {
        $check_digit_ascii = $check_digit_value + 32;
    } else {
        $check_digit_ascii = $check_digit_value + 100;
    }
    
	$output_string = chr(ASCII_STARTB).$input_string.chr($check_digit_ascii).chr(ASCII_STOP);
    
	// Write the bars
    
	// Each Code 128 character consists of three black and three white bars "BWBWBW"
	// (except for the stop character which has an extra black bar).
	// The bars have four different widths (1, 2, 3 and 4).
	$barcode_pattern[] = array();
	$barcode_pattern[32]  = "212222";		// <SPACE>
	$barcode_pattern[33]  = "222122";		// !
	$barcode_pattern[34]  = "222221";		// "
	$barcode_pattern[35]  = "121223";		// #
	$barcode_pattern[36]  = "121322";		// $
	$barcode_pattern[37]  = "131222";		// %
	$barcode_pattern[38]  = "122213";		// &
	$barcode_pattern[39]  = "122312";		// '
	$barcode_pattern[40]  = "132212";		// (
	$barcode_pattern[41]  = "221213";		// )
	$barcode_pattern[42]  = "221312";		// *
	$barcode_pattern[43]  = "231212";		// +
	$barcode_pattern[44]  = "112232";		// ,
	$barcode_pattern[45]  = "122132";		// -
	$barcode_pattern[46]  = "122231";		// .
	$barcode_pattern[47]  = "113222";		// /
	$barcode_pattern[48]  = "123122";		// 0
	$barcode_pattern[49]  = "123221";		// 1
	$barcode_pattern[50]  = "223211";		// 2
	$barcode_pattern[51]  = "221132";		// 3
	$barcode_pattern[52]  = "221231";		// 4
	$barcode_pattern[53]  = "213212";		// 5
	$barcode_pattern[54]  = "223112";		// 6
	$barcode_pattern[55]  = "312131";		// 7
	$barcode_pattern[56]  = "311222";		// 8
	$barcode_pattern[57]  = "321122";		// 9
	$barcode_pattern[58]  = "321221";		// :
	$barcode_pattern[59]  = "312212";		// ;
	$barcode_pattern[60]  = "322112";		// <
	$barcode_pattern[61]  = "322211";		// =
	$barcode_pattern[62]  = "212123";		// >
	$barcode_pattern[63]  = "212321";		// ?
	$barcode_pattern[64]  = "232121";		// @
	$barcode_pattern[65]  = "111323";		// A
	$barcode_pattern[66]  = "131123";		// B
	$barcode_pattern[67]  = "131321";		// C
	$barcode_pattern[68]  = "112313";		// D
	$barcode_pattern[69]  = "132113";		// E
	$barcode_pattern[70]  = "132311";		// F
	$barcode_pattern[71]  = "211313";		// G
	$barcode_pattern[72]  = "231113";		// H
	$barcode_pattern[73]  = "231311";		// I
	$barcode_pattern[74]  = "112133";		// J
	$barcode_pattern[75]  = "112331";		// K
	$barcode_pattern[76]  = "132131";		// L
	$barcode_pattern[77]  = "113123";		// M
	$barcode_pattern[78]  = "113321";		// N
	$barcode_pattern[79]  = "133121";		// O
	$barcode_pattern[80]  = "313121";		// P
	$barcode_pattern[81]  = "211331";		// Q
	$barcode_pattern[82]  = "231131";		// R
	$barcode_pattern[83]  = "213113";		// S
	$barcode_pattern[84]  = "213311";		// T
	$barcode_pattern[85]  = "213131";		// U
	$barcode_pattern[86]  = "311123";		// V
	$barcode_pattern[87]  = "311321";		// W
	$barcode_pattern[88]  = "331121";		// X
	$barcode_pattern[89]  = "312113";		// Y
	$barcode_pattern[90]  = "312311";		// Z
	$barcode_pattern[91]  = "332111";		// [
	$barcode_pattern[92]  = "314111";		// /
	$barcode_pattern[93]  = "221411";		// ]
	$barcode_pattern[94]  = "431111";		// ^
	$barcode_pattern[95]  = "111224";		// _
	$barcode_pattern[96]  = "111422";		// `
	$barcode_pattern[97]  = "121124";		// a
	$barcode_pattern[98]  = "121421";		// b
	$barcode_pattern[99]  = "141122";		// c
	$barcode_pattern[100] = "141221";		// d
	$barcode_pattern[101] = "112214";		// e
	$barcode_pattern[102] = "112412";		// f
	$barcode_pattern[103] = "122114";		// g
	$barcode_pattern[104] = "122411";		// h
	$barcode_pattern[105] = "142112";		// i
	$barcode_pattern[106] = "142211";		// j
	$barcode_pattern[107] = "241211";		// k
	$barcode_pattern[108] = "221114";		// l
	$barcode_pattern[109] = "413111";		// m
	$barcode_pattern[110] = "241112";		// n
	$barcode_pattern[111] = "134111";		// o
	$barcode_pattern[112] = "111242";		// p
	$barcode_pattern[113] = "121142";		// q
	$barcode_pattern[114] = "121241";		// r
	$barcode_pattern[115] = "114212";		// s
	$barcode_pattern[116] = "124112";		// t
	$barcode_pattern[117] = "124211";		// u
	$barcode_pattern[118] = "411212";		// v
	$barcode_pattern[119] = "421112";		// w
	$barcode_pattern[120] = "421211";		// x
	$barcode_pattern[121] = "212141";		// y
	$barcode_pattern[122] = "214121";		// z
	$barcode_pattern[123] = "412121";		// {
	$barcode_pattern[124] = "111143";		// |
	$barcode_pattern[125] = "111341";		// }
	$barcode_pattern[126] = "131141";		// ~
	$barcode_pattern[ASCII_DEL]    = "114113";
	$barcode_pattern[ASCII_FNC3]   = "114311";
	$barcode_pattern[ASCII_FNC2]   = "411113";
	$barcode_pattern[ASCII_SHIFT]  = "411311";
	$barcode_pattern[ASCII_CODEC]  = "113141";
	$barcode_pattern[ASCII_FNC4]   = "114131";
	$barcode_pattern[ASCII_CODEA]  = "311141";
	$barcode_pattern[ASCII_FNC1]   = "411131";
	$barcode_pattern[ASCII_STARTA] = "211412";
	$barcode_pattern[ASCII_STARTB] = "211214";
	$barcode_pattern[ASCII_STARTC] = "211232";
	$barcode_pattern[ASCII_STOP]   = "2331112";
    
	// Convert ASCII characters to bar widths
	$output_pattern = "";
	for ($char_pos = 0; $char_pos < strlen($output_string); $char_pos++) {
        $output_pattern .= $barcode_pattern[ord($output_string[$char_pos])];
    }
	
    // Output the image tags
    $output = "";
	for ($char_pos = 1; $char_pos < strlen($output_pattern); $char_pos = $char_pos + 2) {
        $output .= "<img src='../common/images/Black.gif' width=".($bar['width'] * $output_pattern[$char_pos - 1])." height=".$bar['height']." border=0>";
        $output .= "<img src='../common/images/Blank.gif' width=".($bar['width'] * $output_pattern[$char_pos])." height=".$bar['height']." border=0>";
    }
    // Close it out
	$output .= "<img src='../common/images/Black.gif' width=".($bar['width'] * $output_pattern[strlen($char_pos) - 2])." height=".$bar['height']." border=0>";
    
    if ($valid_barcode != true) {
        $output = "ERROR!";
    }
    
	// Exit function	
    return($output);
}

?>
