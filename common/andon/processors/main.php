<?php

	require_once("../settings.php");
	header('Content-Type: application/json');

	$action = $_REQUEST['action'];

	switch($action) {
		case "process_login":
			$sql = "SELECT * FROM MES_COMMON.dbo.operators WHERE CAST(ID as varchar(20)) = '".$_REQUEST['scan']. "'";
			$res = $db->query($sql);
			if ($res) {
				echo json_encode($res);
			} else {
				echo "[]";
			}
			break;

		case "load_queue":
			loadQueue();
			break;

		case "mark_acked":
			echo "{\"success\":";
			//make sure it hasn't already been acked
			$sql = "SELECT q.ack_time, o.name
					  FROM ANDON_queue q
					  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o ON q.ack_operator_ID = o.ID
					  WHERE q.ID = ".$_REQUEST['queue_ID'];
			$res = $db->query($sql);
			if ($res) {
				if ($res[0]['ack_time'] == null) {
					echo "1";
				} else {
					echo "0, \"err_msg\":\"Already being handled by: ".$res[0]['name']."\"";
				}
			}

			$sql = "UPDATE ANDON_queue
					  SET ack_time = GETDATE(), ack_operator_ID = ".$_REQUEST['op_ID'].", ack_device_ID = ".$_REQUEST['device_ID']."
					  WHERE id = ".$_REQUEST['queue_ID'];
			$res = $db->query($sql);

			echo ", \"queue_data\":";
			loadQueue();
			echo "}";
			break;

		case "mark_picked":
			$sql = "UPDATE ANDON_queue
					  SET material_pick_time = GETDATE()
					  WHERE id = ".$_REQUEST['queue_ID'];
			$res = $db->query($sql);

			loadQueue();
			break;

		case "mark_closed":
			$sql = "UPDATE ANDON_queue
					  SET closed_time = GETDATE()
					  WHERE id = ".$_REQUEST['queue_ID'];
			$res = $db->query($sql);
			loadQueue();
			break;

		case "get_device_routes":
			$sql = "SELECT r.route_name
					  FROM ANDON_route_devices dr
					     JOIN ANDON_routes r ON dr.route_ID = r.ID
					  WHERE dr.device_ID = ".$_REQUEST['device_ID'];
			$res = $db->query($sql);
			echo json_encode($res);
			break;

//		case "send_WMS_replenish_request"
//			$dbWMS = new db();
//			$dbWMS->server = "172.18.188.16";
//			$dbWMS->catalog = "TransportShopfloor";
//			$dbWMS->username = "nysus";
//			$dbWMS->password = "teamnysus24";
//			$dbWMS->testactive = false;
//
//			$dbWMS->connect();
//
//			$sql = "EXEC dbo.XP_IssueReplenishmentByLocation_V1 @LocationName = '".$_REQUEST['location_name'].", @Operator = '".$_REQUEST['operator_name']."', @AutoScanEmpty = 1";
//			$dbWMS->execute($sql);
//
//			$res = new StdClass();
//			$res->success = 1;
//
//			echo json_encode($res);
//
//			break;
	}


	function loadQueue() {
		global $db;
		echo "{\"cur_time\":";
		$sql = "SELECT GETDATE() as cur_time";
		$res = $db->query($sql);
		echo json_encode($res);

		echo ", \"cur_queue\":";
		$sql = "SELECT q.*,
					l.line_code, ls.station_desc, o.name as enterer, o2.name as acker,
					DATEDIFF(MINUTE, ack_time, GETDATE()) as ack_delta,
					p.part_number, p.part_desc, p.pick_location, p.target_location,
					ISNULL(r.reason_desc, '') as reason_desc,
					ISNULL(ar.route_name, '') as route_name,
					ISNULL(ard.device_ID, 0) as device_ID
				  FROM ANDON_queue q
				  	  LEFT OUTER JOIN lines l ON q.line_ID = l.ID
				  	  LEFT OUTER JOIN line_stations ls ON ls.line_ID = q.line_ID AND q.station_ID = ls.station_ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o ON q.entry_operator_ID = o.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON q.ack_operator_ID = o2.ID
				  	  LEFT OUTER JOIN ANDON_parts p ON q.material_part_ID = p.ID
				  	  LEFT OUTER JOIN ANDON_reasons r ON q.reason_ID = r.ID
				  	  LEFT OUTER JOIN ANDON_route_parts arp ON p.ID = arp.part_ID
				  	  LEFT OUTER JOIN ANDON_routes ar ON arp.route_ID = ar.ID
				  	  LEFT OUTER JOIN ANDON_route_devices ard ON arp.route_ID = ard.route_ID
				  WHERE q.category_ID = ".$_REQUEST['category_ID']."
				  	  AND q.closed_time IS NULL
				  	  AND (ISNULL(ard.device_ID, 0) = 0 OR ard.device_ID = ".$_REQUEST['device_ID'].")
				  ORDER BY q.entry_time";
		$res = $db->query($sql);
		if ($res) {
			echo json_encode($res);
		} else {
			echo "[]";
		}
		echo "}";
	}
?>