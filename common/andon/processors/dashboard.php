<?php

	require_once("../settings.php");
	header('Content-Type: application/json');

	$action = $_REQUEST['action'];

	switch($action) {

		case "load_queue":
			loadQueue();
			break;
	}


	function loadQueue() {
		global $db;
		echo "{\"cur_time\":";
		$sql = "SELECT convert(varchar(19),GETDATE(),120) as cur_time";
		$res = $db->query($sql);
		echo json_encode($res);

		
		echo ", \"cur_queue\":";
		$sql = "SELECT q.ID, q.category_ID, q.line_ID, q.station_ID, datediff(minute, q.entry_time, getDate()) as mins_ago, q.entry_operator_ID, q.reason_ID, q.ack_time, q.ack_operator_ID, q.ack_device_ID, q.material_pick_time, q.closed_time, q.material_part_ID, q.alert_email_time,
					l.line_code, ls.station_desc, o.name as enterer,
					p.part_number, p.part_desc, p.pick_location, p.target_location,
					ISNULL(r.reason_desc, '') as reason_desc,
					cat.category_name, cat.color
				  FROM ANDON_queue q
				  	  LEFT OUTER JOIN lines l ON q.line_ID = l.ID
				  	  LEFT OUTER JOIN line_stations ls ON ls.line_ID = q.line_ID AND q.station_ID = ls.station_ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o ON q.entry_operator_ID = o.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON q.ack_operator_ID = o2.ID
				  	  LEFT OUTER JOIN ANDON_parts p ON q.material_part_ID = p.ID
				  	  LEFT OUTER JOIN ANDON_reasons r ON q.reason_ID = r.ID
				  	  LEFT OUTER JOIN ANDON_route_parts arp ON p.ID = arp.part_ID
				  	  LEFT OUTER JOIN ANDON_routes ar ON arp.route_ID = ar.ID
				  	  LEFT OUTER JOIN ANDON_route_devices ard ON arp.route_ID = ard.route_ID
				  	  LEFT OUTER JOIN ANDON_categories cat ON q.category_ID = cat.ID
				  WHERE q.closed_time IS NULL
				  ORDER BY q.category_ID, q.entry_time";
		$res = $db->query($sql);
		if ($res) {
			echo json_encode($res);
		} else {
			echo "[]";
		}
		echo "}";
	}
?>