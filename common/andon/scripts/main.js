$(function() {
	getLogin();

	$('#tbl_main').on('click', '.btn-ack', ackItem);

	$('#tbl_main').on('click', '.btn-pick', savePick);

	$('#div_modal_footer').on('click', '.btn-save-pick', savePick);
	$('#div_modal_footer').on('click', '.btn-save-pick-and-empty', savePickAndEmpty);

	$('#tbl_main').on('click', '.btn-close', closeItem);

	$('#sp_user').on('click', function(e) {
		op = {"ID":null};
		$('#sp_user').html('');
		getLogin();
	});
});

var ajax_url = './processors/main.php';
var op = {"ID":null};
var queue_timer = null;
var WMS_integration = false;

function getLogin() {
	$('#div_modal_title').html('<img src="/common/images/Nysus_icon_30_by_30.png"> Login to Nysus ANDON');
	$('#div_modal_body').html('Please scan your badge to log in: <span id="sp_login_keys" class="keys" style="display:none;"></span>');
	$('#div_modal_footer').hide();
	$('#btn_close_modal').hide();
	$('#div_modal').modal({
		"backdrop":"static",
		"show":true,
		"keyboard":false
	});

	KEYS.resetKeys();
	KEYS.registerKeyHandler({
		"name":"login",
		"global_match":false,
		"match_keys":null,
		"callback":processLogin,
		"display_el_id":"sp_login_keys",
		"mask_character":"*"
	});
};

function processLogin(scan, key_obj) {
	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"process_login",
			"scan":scan
		},
		"success":function(jso) {
			if (jso.length > 0) {
				KEYS.resetKeys();
				op = jso[0];
				$('#sp_user').html('Logged in as: ' + op.name + ((device_ID == 0)?'':' <b id="b_andon_device">Device: ' + device_ID + ' </b>'));
				$('#div_modal_body').html('<div class="alert alert-success"><span class="glyphicon glyphicon-thumbs-up"></span> Welcome back, ' + op.name + '!</div>');
				setTimeout(afterLogin, 2000);

				if (device_ID != 0) {
					getDeviceRoutes();
				}
			} else {
				$('#div_modal_footer').html('<div class="alert alert-danger" role="alert">Login error.  Please try again.</div>').show();
			}
		}
	});
};

function getDeviceRoutes() {
	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"get_device_routes",
			"device_ID":device_ID
		},
		"success":function(jso) {
			var x = [];
			if (jso.length > 0) {
				for (var i = 0; i < jso.length; i++) {
					x.push(jso[i].route_name);
				}

				$('#b_andon_device').append('Route' + ((x.length == 1)?'':'s') + ': ' + x.join(','));
			}
		}
	});
}

function afterLogin() {
	$('#div_modal').modal('hide');

	loadQueue();
}

function loadQueue() {
	if (op.ID == null) {
		return;
	}
	$('#div_last_update').removeClass('loaded').addClass('loading');
	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"load_queue",
			"category_ID":category_ID,
			"my_IP":my_IP,
			"device_ID":device_ID
		},
		"success":function(jso) {
			displayQueue(jso);
		},
		"error":function(jso) {
			$('#div_last_update').html('<span class="label label-danger">ERROR...</span>');
			setTimeout(loadQueue, 2000);
		}
	});
}

function displayQueue(jso) {
	if (op.ID == null) {
		return;
	}
	$('#div_last_update').html('Last update: ' + jso.cur_time[0]['cur_time'].date).removeClass('loading').addClass('loaded');

	$('.queue_item').each(function(i, el) {$(el).remove();});
	var t = $('#tbl_main')[0];
	if (jso.cur_queue.length == 0) {
		var r = t.insertRow(-1);
		r.className = 'queue_item';
		var c = r.insertCell(-1);
		c.colSpan = 20;
		c.innerHTML = 'No items in queue!';
	} else {
		for (var i = 0; i < jso.cur_queue.length; i++) {
			var q = jso.cur_queue[i];
			var r = t.insertRow(-1);
			r.className = 'queue_item';
			var c = r.insertCell(-1);
			c.innerHTML = '<div>' + q.line_code + ' ' + q.station_desc + '</div><div><i>' + q.station_ID + '</i></div>';
			var c = r.insertCell(-1);
			c.innerHTML = '<div>' + q.entry_time.date + '</div><div><i>By: ' + q.enterer + '</i></div>';

			if (select_part == '1') {
				var c = r.insertCell(-1);
				c.innerHTML = '<div>' + q.part_number + '</div>';
				var c = r.insertCell(-1);
				c.innerHTML = '<div>' + q.part_desc + '</div>';
				var c = r.insertCell(-1);
				if (q.ack_time != null && q.material_pick_time.date == null) {
					c.innerHTML = '<div><b class="bigger">' + q.pick_location + '</b></div>';
				} else {
					c.innerHTML = '<div>' + q.pick_location + '</div>';
				}
				var c = r.insertCell(-1);
				if (q.material_pick_time != null) {
					c.innerHTML = '<div><b class="bigger">' + q.target_location + '</b></div>';
				} else {
					c.innerHTML = '<div>' + q.target_location + '</div>';
				}
			} else {
				var c = r.insertCell(-1);
				c.innerHTML = '<div>' + q.reason_desc + '</div>';
			}
			var c = r.insertCell(-1);
			c.innerHTML = getItemStatus(q);
			var c = r.insertCell(-1);
			$(c).html(getItemAction(q));
		}
	}

	if (queue_timer) {
		clearTimeout(queue_timer);
	}
	queue_timer = setTimeout(loadQueue, 2000);
}

function getItemStatus(q) {
	if (q.ack_time == null) {
		return 'QUEUED';
	}

	if (select_part == '1') {
		if (q.material_pick_time == null) {
			return '<div>PICKING</div><div><i>By: ' + q.acker + '</i></div>';
		} else {
			return '<div>PICKED</div><div><i>By: ' + q.acker + '</i></div>';
		}
	} else {
		return '<div>HANDLING</div><div><i>By: ' + q.acker + ' (' + q.ack_delta + ' min ago)</i></div>';
	}

}

function getItemAction(q) {
	var b = $('<button></button>');
	b.attr("id", "btn_" + q.ID);
	if (q.ack_time == null) {
		b.addClass('btn-ack');
		b.html('HANDLE REQUEST');
	} else {
		if (select_part == '1' && q.ack_time != null) {
			if (q.material_pick_time == null) {
				if (q.ack_operator_ID == op.ID) {
					b.addClass('btn-pick');
					b.html('MARK PICKED');
				} else {
					b = $('<div><i>Being picked...</i></div>');
				}
			} else {
				b.addClass('btn-close');
				b.html('MARK DROPPED');
			}
		} else {
			b.addClass('btn-close');
			b.html('MARK CLOSED');
		}
	}

	b.data(q);
	//$.data(
	return b;
}



function ackItem(e) {
	var o = $(this).data();
	$(this).attr('disabled', true);

	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"mark_acked",
			"category_ID":category_ID,
			"queue_ID":o.ID,
			"my_IP":my_IP,
			"op_ID":op.ID,
			"device_ID":device_ID
		},
		"success":function(jso) {
			if (jso.success == 1) {
				//$('#div_modal').modal('hide');
				displayQueue(jso.queue_data);
			} else {
				$('#div_modal_title').html('<img src="/images/Nysus_icon_30_by_30.png"> Error Marking ANDON Request as ACKNOWLEDGED');
				$('#div_modal_body').html('<div class="well well-danger">' +
												  '	<div>Error marking item as acknowledged: ' + jso.err_msg + '</div>' +
												  '</div>');
				$('#div_modal_footer').html('<div><button type="button" class="btn btn-warning" data-dismiss="modal">Close</button></div>').show();

				$('#div_modal').modal({
					"backdrop":"static",
					"show":true,
					"keyboard":false
				});
			}
		},
		"error":function(jso) {
			$('#div_last_update').html('<span class="label label-danger">ERROR...</span>');
			setTimeout(loadQueue, 2000);
		}
	});
}

function confirmPick(e) {
	var o = $(this).data();
	$('#div_modal_title').html('<img src="/images/Nysus_icon_30_by_30.png"> Mark ANDON Request as PICKED');
	$('#div_modal_body').html('<div><h2>Would you like to mark this request as PICKED?</h2></div>' +
									  '<div class="well well-primary">' +
									  '	<div>Part: ' + o.part_number + '</div>' +
									  '</div>');

	var t = '<div><button type="button" class="btn btn-success btn-save-pick">OK, Mark Picked</button>';
	if (WMS_integration) {
		t += '<button type="button" class="btn btn-success btn-save-pick-and-empty">Mark Picked AND FLAG LOCATION AS EMPTY</button>';
	}
	t += '<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button></div>';
	$('#div_modal_footer').html(t).show();

	$('.btn-save-pick').data(o);  //copy everything

	$('#div_modal').modal({
		"backdrop":"static",
		"show":true,
		"keyboard":false
	});
}

function savePick(e) {
	var o = $(this).data();
	$('#div_modal_body').html('Marking record as picked...');
	$('#div_modal_footer BUTTON').attr('disabled', true);
	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"mark_picked",
			"category_ID":category_ID,
			"queue_ID":o.ID,
			"my_IP":my_IP,
			"op_ID":op.ID,
			"device_ID":device_ID
		},
		"success":function(jso) {
			$('#div_modal').modal('hide');
			displayQueue(jso);
		},
		"error":function(jso) {
			$('#div_last_update').html('<span class="label label-danger">ERROR...</span>');
			setTimeout(loadQueue, 2000);
		}
	});
}

function savePickAndEmpty(e) {
	//confirm location scan
}

function closeItem(e) {
	var o = $(this).data();
	$(this).attr('disabled', true);

	$.ajax({
		"url":ajax_url,
		"type":"POST",
		"dataType":"json",
		"data":{
			"action":"mark_closed",
			"category_ID":category_ID,
			"queue_ID":o.ID,
			"my_IP":my_IP,
			"op_ID":op.ID,
			"device_ID":device_ID
		},
		"success":function(jso) {
			$('#div_modal').modal('hide');
			displayQueue(jso);
		},
		"error":function(jso) {
			$('#div_last_update').html('<span class="label label-danger">ERROR...</span>');
			setTimeout(loadQueue, 2000);
		}
	});
}