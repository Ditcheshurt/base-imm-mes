var queue_timer = null;
var processor = 'processors/dashboard.php';

$(document).on('ready', function() {

	loadQueue();

});

function loadQueue() {
	if (queue_timer) {
		clearTimeout(queue_timer);
	}

	$.getJSON(processor, {action:'load_queue'}, function(jso) {
		$('#div_last_update').html(jso.cur_time[0].cur_time.date);
		var d = $('#div_content').empty();

		jso = jso.cur_queue;
		if (jso.length == 0) {
			d.html('No requests found.');
		} else {
			for (var i = 0; i < jso.length; i++) {
				var x = $('<div />').appendTo(d).addClass('queue-item').html('<div class="cat-name">' + jso[i].category_name + '</div><div class="station-desc">' + jso[i].station_desc + ': ' + (jso[i].reason_desc || jso[i].part_number) + '</div><div class="time-ago ' + getStateClass(jso[i].mins_ago) + '">' + niceDate(jso[i].mins_ago) + '</div>').css('background-color', jso[i].color);
			}
		}

		queue_timer = setTimeout(loadQueue, 5000);
	});
}

function getStateClass(d) {
	//If new request
	if (!d) {
		return '';
	}
	//If less then or 10 mins.
	if(d <= 10){
		return '';
	}
	//More then 10 less then 30
	if((d > 10) && (d <= 30)){
		return 'warning';
	}
	//More then 30
	if(d > 30){
		return 'critical';
	}
	
}

function niceDate(d) {
	//If brand new request
	if (!d) {
		return '0 min. ago';
	}
	
	//If less then an hour
	if(d < 60){
		return d + 'min. ago';
	}
	//If less then a day, but more then a hour
	if((d >= 60)&&(d <= 1140)){
		var hour = (d / 60);
		hour = Math.floor(hour);
		return hour + 'hr. ago';
	}
	//If more then a day
	if(d > 1140){
		var day = (d / 60 / 24 );
		day = Math.floor(day);
		//If only 1 day
		if(day == 1){
			return day + 'day ago';
		}
		//More then 1 day
		else{
			return day + 'days ago';
		}
	}
}