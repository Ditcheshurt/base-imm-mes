<?php

	date_default_timezone_set("America/New_York");
	ini_set('display_errors', 1);

	$company_name = "TRW";

	define('ROOT', $_SERVER['DOCUMENT_ROOT']);

	require_once(ROOT."/common/library/db_class.php");

	$db = new db();
	$db->server = "localhost";
	$db->catalog = "NYSUS_SEQUENCE_MES";
	$db->username = "nysususer";
	$db->password = "nysus2444";
	$db->testactive = false;

	$db->connect();
?>