<?php

	require_once("settings.php");

?>
<!DOCTYPE HTML>
<html>

<head>
	<title>Nysus ANDON - <?php echo $company_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Description" />
	<link rel="stylesheet" href="/common/library/bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="css/dashboard.css" type="text/css" />
</head>

<body>

	<div class="page-header">
	  <h1><img src="/MESAAS/content/images/TRW_logo_small.png" height="40"> ANDON - <?php echo $company_name; ?></h1>
	  <div id="div_last_update">Last Update: none</div>
	</div>
	<div id="div_content">
	</div>
	<footer>
		<div><img src="/common/images/Nysus_icon_30_by_30.png" height="15"> Nysus Solutions &copy; <?php echo date("Y"); ?> <span id="div_key_default_display"></span><span id="sp_user"></span></div>
	</footer>

	<script type="text/javascript" src="/common/library/js/jQuery-1.9.1.js"></script>
	<script type="text/javascript" src="/common/library/js/jsDate.js"></script>
	<script type="text/javascript" src="/common/library/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./scripts/dashboard.js"></script>
</body>

</html>