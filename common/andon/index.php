<?php

	require_once("settings.php");

	if (!isset($_REQUEST['category_ID'])) {
		echo "MUST PASS IN category_ID!!";
		end();
	}

	$category_ID = $_REQUEST['category_ID'];

	$device_ID = 0;
	if (isset($_REQUEST['device_ID'])) {
		$device_ID = $_REQUEST['device_ID'];
	}

	$sql = "SELECT *
			  FROM ANDON_categories
			  WHERE ID = ".$category_ID."
			  ORDER BY category_name";

	$res = $db->query($sql);
	if ($res) {
		if (count($res) > 0) {
			$category_name = $res[0]['category_name'];
			$select_part = $res[0]['select_part'];
		} else {
			echo "CATEGORY NOT FOUND";
		}
	} else {
		echo "CATEGORY NOT FOUND!";
	}

	$my_IP = $_SERVER['REMOTE_ADDR'];
	if (isset($_REQUEST['debug_ip'])) {
		$my_IP = $_REQUEST['debug_ip'];
	}
?>
<!DOCTYPE HTML>
<html>

<head>
	<title>Nysus ANDON - <?php echo $company_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Description" />
	<link rel="stylesheet" href="/common/library/bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="css/main.css" type="text/css" />

	<script type="text/javascript">
		var category_ID = '<?php echo $category_ID; ?>';
		var device_ID = '<?php echo $device_ID; ?>';
		var category_name = '<?php echo $category_name; ?>';
		var my_IP = '<?php echo $my_IP; ?>';
		var select_part = '<?php echo $select_part; ?>';
	</script>
</head>

<body>

	<div class="page-header">
	  <h1><img src="/MESAAS/content/images/TRW_logo_small.png" height="40"> ANDON - <?php echo $company_name; ?> <small><?php echo $category_name; ?></small></h1>
	  <div id="div_last_update">Last Update: none</div>
	</div>
	<div id="div_content">
		<table id="tbl_main">
			<tr class="queue_title">
				<?php if ($select_part == 1) { ?>
					<td>ORIGIN</td>
					<td>TIME</td>
					<td>PART NUMBER</td>
					<td>PART DESCRIPTION</td>
					<td>PICK LOCATION</td>
					<td>DROP LOCATION</td>
					<td>STATUS</td>
					<td>ACTIONS</td>
				<?php } else { ?>
					<td>ORIGIN</td>
					<td>TIME</td>
					<td>REASON</td>
					<td>STATUS</td>
					<td>ACTIONS</td>
				<?php } ?>
			</tr>
			<tr class="queue_item">
				<td colspan="20">Log in first..</td>
			</tr>
		</table>
	</div>
	<footer>
		<div><img src="/common/images/Nysus_icon_30_by_30.png" height="15"> Nysus Solutions &copy; 2015 <span id="div_key_default_display"></span><span id="sp_user"></span></div>
	</footer>

	<div class="modal fade" id="div_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	 <div class="modal-content">
	   <div class="modal-header">
	     <button id="btn_close_modal" type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	     <h4 class="modal-title" id="div_modal_title"></h4>
	   </div>
	   <div class="modal-body" id="div_modal_body"></div>
	   <div class="modal-footer" id="div_modal_footer">
	     <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
	     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	   </div>
	 </div>
	</div>
	</div>

	<script type="text/javascript" src="/common/library/js/jQuery-1.9.1.js"></script>
	<script type="text/javascript" src="/common/library/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./scripts/main.js"></script>
	<script type="text/javascript" src="/common/library/js/WatchKeys_v2_jQuery.js"></script>
</body>

</html>