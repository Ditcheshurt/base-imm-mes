<?php
/*
    db_class.php
        This is a modified version of db_class.inc
        Created due to major change in class construct, as well as to
        eliminate ambiguity with similar VBscript includes
    Handles database connectivity and basic query functions.
    Upon construction, this class creates a new database connection based
    on default values.  When the class is destroyed, the connection to the
    database is closed automatically and the connection handle released.
*/

date_default_timezone_set('America/New_York');

class db {
    // Allocates space for public properties and function result sets:
    // Database schema name, server, username, password, and options
    public $catalog         = 'IT_Projects';
    public $server          = 'localhost';
    public $username        = 'teamsys';
    public $password        = 'teamsys2444';
    public $testactive      = false;
    public $dboptions       = array('ANSI_NULLS' => 'ON',
                                    'ANSI_WARNINGS' => 'ON');

    // Allocate the handle for the database connection
    private $handle;


    // Executes when the class is constructed
    function __construct() {
        // Connection functions moved to connect();
        // Use this space for error checking?
    }


    // Executes when the class is destroyed
    function __destruct() {
        $this->disconnect();
    }


    // Closes, then immediately re-opens the database
    // This is a shortcut to reconnect with changed connection parameters
    function reconnect() {
        $this->disconnect();
        $this->connect();
    }


    // Opens a connection to the database named in the public properties
    function connect() {
        if ((strtoupper($_SERVER['SERVER_NAME']) != strtoupper($this->server)) && ($this->testactive == true)) {
            $this->catalog .= '_TEST';
        }

        $this->handle = sqlsrv_connect($this->server, array('Database'=>$this->catalog, 'UID'=>$this->username, 'PWD'=>$this->password));
		
		//if ($this->handle === false) {
		//	echo "Could not connect to Database.\n";
		//	die(print_r(sqlsrv_errors(),true));
		//} else {
		//	echo "Connected to Database.\n";
		//}
		
        // Set config states as per config option, such "as ANSI_NULLS ON"
        foreach($this->dboptions as $option => $value) {
            $result = sqlsrv_query($this->handle, "SET ".$option." ".$value.";");
        }
    }


    // Disconnects from the database using the handle cerated at construction
    function disconnect() {
		if ($this->handle === true) {
			sqlsrv_close($this->handle);
		}
    }


    // Returns the results from a query in an array
    function query($sql) {
        $recordset = NULL;
        $result = sqlsrv_query($this->handle, $sql);

        // Assign the query results as the return value (with some error checking)
        if ($result === false) {
            // We ran into a problem
            $recordset = false;
        } elseif (($result === true) || ($result === null)) {
            // The query returned no data
            // Set $recordset to the number of rows affected
            $recordset = sqlsrv_rows_affected($this->handle);
        } else {
            while($tmp = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
                // Append each returned record to the $recordset array
                $recordset[] = $tmp;
            }
        }

        return($recordset);
    }


    function get_dbconn() {
        return($this->handle);
    }

    // THIS MEMBER FUNCTION IS WIP
    // Loads a pre-defined 1D array with values returned from a recordset
    // Uses the following parameters:
    //    $array  - user-defined array to be filled
    //    $result - the recordset to insert
    //    $order  - record used if more than one record exists
    //              0 = first record (default), 1 = last record
    function load_array($result, $array, $order = 0) {
        // Set the record index to either the first or last record
        if ($order > 0) {
            $order = sizeof($result);
        }

        // Load the user array with null values
        foreach ($array as $key => $value) {
            $return[$key] = NULL;
        }

        // Load the recordset values
        foreach($result as $key => $value) {
            if(isset($result[0][$key])) {
                $return[$key] = $value;
            }
        }

        // Return the final array
        return($result);
    }

}

?>