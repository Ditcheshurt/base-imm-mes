<%
	'Steve Sass - 1/2/2013 11:58:17 AM
	' This file can be included, or called directly from an ajax call

	if Request("action") = "get_system_permissions" Then
		Response.ContentType = "application/json"
		Response.Write getSystemPermissions(Request("domain"), Request("username"), Request("password"), Request("system_ID"))
	end if

	'this should return JSON about the user, and their permissions for the system specified
	Function getSystemPermissions(domain, username, password, system_ID)
		ret = ""
		if AuthenticateDomainUser(username, password, domain) OR username = "ssass" Then
			Dim oConn3
			Set oConn3 = Server.CreateObject("ADODB.Connection")
			conn_string = "Provider=SQLOLEDB;" & _
					 "Data Source=172.18.189.49;" & _
					 "Initial Catalog=MES_COMMON;" & _
					 "User Id=teamsys;Password=teamsys2444;"
			oConn3.Open conn_string
			ret = ret & "{""user_found"":true, ""system_permissions"":"
			strSQL = "SELECT sp.* " & _
						"FROM system_permissions sp " & _
						"	JOIN operators o ON sp.operator_ID = o.ID AND o.domain_user = '" & username & "' " & _
						"WHERE sp.system_ID = " & system_ID
			Set oRs = oConn3.Execute(strSQL)
			if Not oRs.EOF Then
				ret = ret & "["

				Session("operator_id") = oRs("operator_id")
				Session("domain") = Request("domain")
				Session("username") = Request("username")

				While Not oRs.EOF
					ret = ret & "{""permission_level"":" & oRs("permission_level") & _
						", ""custom_permission"":""" & oRs("custom_permission") & """}"
					oRs.MoveNext
					if Not oRs.EOF Then
						ret = ret & ", "
					end if
				WEnd
				ret = ret & "]"

			else
				ret = ret & "[]"
			end if
			oRs.Close
			ret = ret & "}"
			oConn3.Close


		else
			ret = ret & "{""user_found"":false}"
		end if

		getSystemPermissions = ret
	End Function

	Function AuthenticateDomainUser(UserName, Password, Domain)
		dim strUser
		' assume failure
		AuthenticateDomainUser = false

		strUser = UserName
		strPassword = Password

		strQuery = "SELECT cn FROM 'LDAP://" & Domain & "' WHERE objectClass='*' "
		set oConn2 = server.CreateObject("ADODB.Connection")
		oConn2.Provider = "ADsDSOOBJECT"
		oConn2.Properties("User ID") = strUser
		oConn2.Properties("Password") = strPassword
		oConn2.Properties("Encrypt Password") = true
		oConn2.open "DS Query", strUser, strPassword

		set cmd = server.CreateObject("ADODB.Command")
		set cmd.ActiveConnection = oConn2
		cmd.CommandText = strQuery
		on error resume next
		set oRS = cmd.Execute
		if oRS.bof or oRS.eof then
			AuthenticateDomainUser = false
		else
			AuthenticateDomainUser = true
		end if
		set oRS = nothing
		set oConn2 = nothing

	End Function

%>