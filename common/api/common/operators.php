<?php

require_once("../init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;
	$with_inactive = isset($_REQUEST["with_inactive"]) ? fixDB($_REQUEST["with_inactive"]) : null;

	if ($with_inactive == "true") {
		$with_inactive = 1;
	} else {
		$with_inactive = 0;
	}

	$sql = "SELECT
				ID,
				badge_ID,
				first_name,
				last_name,
				email,
				deactive_date
			FROM
				operators
			WHERE
				1=1";

	if ($operator_ID != null) {
		$sql .= " AND ID = $operator_ID ";
	}

	if ($with_inactive == null || $with_inactive == "0" || strtolower($with_inactive) == "false") {
		$sql .= " AND (deactive_date IS NULL OR deactive_date >= getDate()) ";
	}

	$sql .= " ORDER BY last_name";

	$res = $db->query($sql);

	echo(json_encode($res));

}