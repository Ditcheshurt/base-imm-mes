<?php

require_once("../init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$with_alerts = isset($_REQUEST["with_alerts"]) ? fixDB($_REQUEST["with_alerts"]) : null;
	$with_expired = isset($_REQUEST["with_expired"]) ? fixDB($_REQUEST["with_expired"]) : null;

	if ($with_alerts == null || $with_alerts == 0 || strtolower($with_alerts) == "false") {
		$with_alerts = 0;
	} else {
		$with_alerts = 1;
	}

	if ($with_expired == null || strtolower($with_expired) == "false") {
		$with_expired = 0;
	} else {
		$with_expired = 1;
	}

	$sql = "SELECT
				s.ID AS ojt_signoff_ID,
				o2.ID AS operator_ID,
				o2.badge_ID AS operator_badge_ID,
				o2.first_name AS operator_first_name,
				o2.last_name AS operator_last_name,
				req.ID AS requirement_ID,
				req.requirement_description,
				o.ID AS signoff_operator_ID,
				o.badge_ID AS signoff_operator_badge_ID,
				o.first_name AS signoff_operator_first_name,
				o.last_name AS signoff_operator_last_name,
				stl.level_description,
				r.name AS role_description,
				slr.ID AS signoff_level_role_ID,
				CONVERT(VARCHAR(13), s.created_date, 101) AS created_date,
				CONVERT(VARCHAR(13), s.expire_date, 101) AS expire_date
			FROM
				ojt_signoffs s
				JOIN ojt_signoff_template_levels_roles slr ON s.ojt_signoff_template_levels_roles_ID = slr.ID
				JOIN ojt_signoff_template_levels stl ON slr.ojt_signoff_template_level_ID = stl.ID
				JOIN ojt_signoff_templates st ON stl.ojt_signoff_template_ID = st.ID
				JOIN roles r ON slr.role_ID = r.ID
				JOIN operators o ON s.signoff_operator_ID = o.ID
				JOIN operators o2 ON s.operator_ID = o2.ID
				JOIN ojt_requirements req ON s.ojt_requirement_ID = req.ID
				JOIN ojt_requirement_types rt ON req.ojt_requirement_type_ID = rt.ID";

	if ($ojt_group_ID != null) {
		$sql .= " JOIN dbo.fn_getOJTGroupRequirements($ojt_group_ID, $with_alerts) ogr ON ogr.ojt_requirement_ID = req.ID ";
	}

	$sql .= " WHERE 1=1 ";

	if ($operator_ID != null) {
		$sql .= " AND s.operator_ID = $operator_ID ";
	}

	if ($ojt_requirement_ID != null) {
		$sql .= " AND s.ojt_requirement_ID = $ojt_requirement_ID ";
	}

	if ($with_alerts == 0) {
		$sql .= " AND rt.type_description <> 'Alert' ";
	}

	if ($with_expired == 0) {
		$sql .= " AND (s.expire_date IS NULL OR s.expire_date >= getDate()) ";
	}

	$sql .= " ORDER BY stl.level_order, s.created_date desc";

	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}

	echo(json_encode($res));
}

function create($db)
{

	$signoff_operator_ID = isset($_REQUEST["signoff_operator_ID"]) ? fixDB($_REQUEST["signoff_operator_ID"]) : $_SESSION["op_ID"];
	$requirement_ID = fixDB($_REQUEST["ojt_requirement_ID"]);
	$operator_ID = fixDB($_REQUEST["operator_ID"]);
	$ojt_signoff_template_level_roles_ID = fixDB($_REQUEST["ojt_signoff_template_levels_roles_ID"]);

	// expire any unexpired signoff
	$sql = "UPDATE ojt_signoffs
				SET expire_date = getDate(),
				expired_by = $signoff_operator_ID
			WHERE
				ojt_requirement_ID = $requirement_ID
				AND operator_ID = $operator_ID
				AND ojt_signoff_template_levels_roles_ID = $ojt_signoff_template_level_roles_ID
				AND expire_date IS NULL";

	$db->query($sql);

	// create new signoff
	$sql = "INSERT INTO ojt_signoffs
				(
					ojt_requirement_ID,
					signoff_operator_ID,
					operator_ID,
					ojt_signoff_template_levels_roles_ID,
					expire_date
				)
			VALUES
				(
					$requirement_ID,
					$signoff_operator_ID,
					$operator_ID,
					$ojt_signoff_template_level_roles_ID,
					(
						SELECT
							DATEADD(dd, expire_days, getDate())
						FROM
							ojt_requirements
						WHERE
							ID = $requirement_ID
					)
				)";

	$db->query($sql);

	// return the record
	$sql = "SELECT
				*
			FROM
				ojt_signoffs
			WHERE
				ojt_requirement_ID = $requirement_ID
				AND operator_ID = $operator_ID
				AND ojt_signoff_template_levels_roles_ID = $ojt_signoff_template_level_roles_ID
				AND (expire_date IS NULL OR expire_date >= getDate())";
//die($sql);
	$res = $db->query($sql);

	echo(json_encode($res));

}

function create_all_group_requirement_signoffs($db) {

	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$ojt_signoff_template_levels_roles_ID = isset($_REQUEST["ojt_signoff_template_levels_roles_ID"]) ? fixDB($_REQUEST["ojt_signoff_template_levels_roles_ID"]) : null;
	$ojt_signoff_operator_ID = isset($_REQUEST["ojt_signoff_operator_ID"]) ? fixDB($_REQUEST["ojt_signoff_operator_ID"]) : null;


	$sql = "SELECT DISTINCT
				ogo.operator_ID, last_name
			FROM
				dbo.fn_getOJTGroupOperators($ojt_group_ID) ogo
			WHERE
				ogo.operator_ID NOT IN (
					SELECT
						so.operator_ID
					FROM ojt_signoffs so
					WHERE so.ojt_requirement_ID = $ojt_requirement_ID
					AND so.ojt_signoff_template_levels_roles_ID = $ojt_signoff_template_levels_roles_ID
					AND (so.expire_date IS NULL OR so.expire_date >= getDate())
				)";

	$res = $db->query($sql);

	if ($res == null) {
		die('{"result": "no records created"}');
	}

	foreach ($res as $operator) {
		$op_ID = $operator["operator_ID"];
		$sql = "INSERT INTO ojt_signoffs
					(
						ojt_requirement_ID,
						signoff_operator_ID,
						operator_ID,
						ojt_signoff_template_levels_roles_ID,
						expire_date
					)
				VALUES
					(
						$ojt_requirement_ID,
						$ojt_signoff_operator_ID,
						$op_ID,
						$ojt_signoff_template_levels_roles_ID,
						(
							SELECT
								DATEADD(dd, expire_days, getDate())
							FROM
								ojt_requirements
							WHERE
								ID = $ojt_requirement_ID
						)
					)";

		$db->query($sql);
	}

	echo('{"result": "success"}');

}

function delete($db)
{

	$id = isset($_REQUEST["ID"]) ? fixdb($_REQUEST["ID"]) : null;
	$signoff_operator_ID = isset($_REQUEST["signoff_operator_ID"]) ? fixdb($_REQUEST["signoff_operator_ID"]) : null;

	$sql = "UPDATE ojt_signoffs
				SET expire_date = getDate(), expired_by = $signoff_operator_ID
			WHERE
				ID = $id
				AND expired_by IS NULL";

	$res = $db->query($sql);

	echo(json_encode($res));

}

function delete_all_group_requirement_signoffs($db) {

	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$ojt_signoff_template_levels_roles_ID = isset($_REQUEST["ojt_signoff_template_levels_roles_ID"]) ? fixDB($_REQUEST["ojt_signoff_template_levels_roles_ID"]) : null;
	$ojt_signoff_operator_ID = isset($_REQUEST["ojt_signoff_operator_ID"]) ? fixDB($_REQUEST["ojt_signoff_operator_ID"]) : null;

	$sql = "UPDATE dbo.ojt_signoffs SET
				expire_date = getDate(),
				expired_by = $ojt_signoff_operator_ID
			WHERE
				ojt_requirement_ID = $ojt_requirement_ID
				AND ojt_signoff_template_levels_roles_ID = $ojt_signoff_template_levels_roles_ID
				AND operator_ID IN (SELECT operator_ID FROM dbo.fn_getOJTGroupOperators($ojt_group_ID))
				AND (expire_date IS NULL OR expire_date >= getDate())";

	$res = $db->query($sql);

	echo(json_encode($res));

}