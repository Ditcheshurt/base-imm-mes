<?php

require_once("../init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$with_alerts = isset($_REQUEST["with_alerts"]) ? fixDB($_REQUEST["with_alerts"]) : null;

	if ($ojt_group_ID == null) {
		die(json_encode(array("error", "Missing param: ojt_group_ID=int")));
	}

	if ($with_alerts == null || $with_alerts == 0 || strtolower($with_alerts) == "false") {
		$with_alerts = 0;
	}
	else {
		$with_alerts = 1;
	}

	$sql = "SELECT
				*
			FROM
				dbo.fn_getOJTGroupSummary($ojt_group_ID, $with_alerts)
			WHERE
				1=1";

	if ($ojt_requirement_ID != null) {
		$sql .= " AND ojt_requirement_ID = $ojt_requirement_ID ";
	}

	$res = $db->query($sql);

	echo(json_encode($res));

}