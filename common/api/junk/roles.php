<?php

require_once("init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;

	$sql = "SELECT
				r.ID,
				r.name
			FROM
				roles r";

	if ($operator_ID != null) {
		$sql .= " JOIN operator_roles oroles ON oroles.role_ID = r.ID ";
	}

	$sql .= "WHERE
				1=1";

	if ($operator_ID != null) {
		$sql .= " AND oroles.operator_ID = $operator_ID ";
	}

	$sql .= " ORDER BY r.name";

	$res = $db->query($sql);

	echo(json_encode($res));

}