<?php

/**
 * generic mssql query
 * User: sdaviduk
 * Date: 7/11/2016
 * Time: 1:58 PM
 */
class Query
{
	private $_db;
	private $_table_name = null;
	private $_fields = array();
	private $_join = array();
	private $_where = array();
	private $_order = array();

	function __construct($server, $database, $user, $password)
	{
		$this->_db = new PDO("sqlsrv:server=$server;database=$database", $user, $password);
		$this->_db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$this->_db->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		$this->_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}

	public function get()
	{
		try {
			$stmt = $this->_db->prepare($this->_select());
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e) {
			return array("success" => 0, "error" => $e->getMessage());
		}
	}

	public function insert()
	{
		return array("success" => 0, "error" => "not implemented");
	}

	public function update()
	{
		return array("success" => 0, "error" => "not implemented");
	}

	public function delete()
	{
		return array("success" => 0, "error" => "not implemented");
	}

	public function table($table_name)
	{
		$this->_table_name = $table_name;
		return $this;
	}

	public function fields($field, $value = null)
	{
		array_push($this->_fields, array($field, $value));
		return $this;
	}

	public function join($table, $condition, $type = "LEFT")
	{
		array_push($this->_join, array($table, $condition, $type));
		return $this;
	}

	public function where($field, $value, $op)
	{
		array_push($this->_where, array($field, $op, $value));
		return $this;
	}

	public function order($order)
	{
		array_push($this->_order, $order);
		return $this;
	}

	protected function _select()
	{
		return "SELECT " .
		$this->_build_fields() .
		" FROM " .
		$this->_table_name .
		$this->_build_join() .
		$this->_build_where() .
		$this->_build_order();
	}

	protected function _get_columns()
	{
		$sql = "SELECT
					COLUMN_NAME
				FROM
					INFORMATION_SCHEMA.COLUMNS
				WHERE TABLE_NAME = '" . $this->_table_name . "'
					AND TABLE_SCHEMA = 'dbo'";

		foreach ($this->_db->query($sql) as $res) {
			array_push($this->_fields, $res["COLUMN_NAME"]);
		};
	}

	protected function _build_fields()
	{
		if (count($this->_fields) == 0) {
			$this->_get_columns();
			return implode(",", $this->_fields);

		} else {
			$fields = " ";
			foreach ($this->_fields as $r) {
				$fields .= $r[0] . ", ";
			}
			return rtrim($fields, ", ");
		}

	}

	protected function _build_join()
	{
		if (count($this->_join) > 0) {
			$join = " ";
			foreach ($this->_join as $r) {
				$join .= $r[2] . " JOIN " . $r[0] . " ON " . $r[1] . " ";
			}
			return $join;
		}
	}

	protected function _build_where()
	{
		if (count($this->_where) > 0) {
			$where = " WHERE ";
			foreach ($this->_where as $r) {
				$where .= $r[0] . " " . $r[1] . " " . $r[2] . " AND ";
			}
			return rtrim($where, " AND ");
		}
	}

	protected function _build_order()
	{
		if (count($this->_order) > 0) {
			return " ORDER BY " . implode(",", $this->_order);
		}
	}

}