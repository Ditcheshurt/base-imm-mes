<?php

require_once("../init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$ojt_signoff_template_ID = isset($_REQUEST['ojt_signoff_template_ID']) ? fixDB($_REQUEST["ojt_signoff_template_ID"]) : null;

	$sql = "SELECT
				t.ID AS signoff_template_ID,
				t.template_description,
				l.ID AS signoff_template_level_ID,
				l.level_order,
				l.level_description
			FROM
				ojt_signoff_templates t
				JOIN ojt_signoff_template_levels l ON l.ojt_signoff_template_ID = t.ID";

	if ($ojt_signoff_template_ID != null) {
		$sql .= " WHERE t.ID = $ojt_signoff_template_ID ";
	}

	$sql .= " ORDER BY level_order ";

//die($sql);
	$res = $db->query($sql);

	echo(json_encode($res));

}