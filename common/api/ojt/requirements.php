<?php

require_once("../init.php");

$db->catalog = "MES_COMMON";
$db->reconnect();

if (isset($_REQUEST['action']) && count($_REQUEST["action"]) > 0) {
	call_user_func($_REQUEST["action"], $db);
} else {
	call_user_func("get", $db);
}

function get($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$with_alerts = isset($_REQUEST["with_alerts"]) ? fixDB($_REQUEST["with_alerts"]) : null;

	if ($ojt_group_ID != null) {
		call_user_func("get_group_requirements", $db, $ojt_group_ID, $with_alerts);

	} else if ($operator_ID != null) {
		call_user_func("get_operator_requirements", $db, $operator_ID, $with_alerts);

	} else {
		$sql = "SELECT
					r.*
				FROM
					ojt_requirements r
					JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
				WHERE 1=1 ";

		if ($ojt_requirement_ID != null) {
			$sql .= " AND r.ID = $ojt_requirement_ID";
		}

		if ($with_alerts == null || $with_alerts == 0 || strtolower($with_alerts) == "false") {
			$sql .= " AND rt.is_alert = 0 ";
		}		

		$res = $db->query($sql);

		if ($res == null) {
			$res = array();
		}

		echo(json_encode($res));

	}

}

function get_group_requirements($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$with_alerts = isset($_REQUEST["with_alerts"]) ? fixDB($_REQUEST["with_alerts"]) : null;

	$sql = "SELECT
				*
			FROM
				dbo.fn_getOJTGroupRequirements($ojt_group_ID, $with_alerts)";

	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}

	echo(json_encode($res));
}

function get_operator_requirements($db)
{
	$operator_ID = isset($_REQUEST["operator_ID"]) ? fixDB($_REQUEST["operator_ID"]) : null;
	$with_alerts = isset($_REQUEST["with_alerts"]) ? fixDB($_REQUEST["with_alerts"]) : null;
	$sql = "SELECT
				*
			FROM
				dbo.fn_getOJTOperatorRequirements($operator_ID, $with_alerts)";

	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}

	echo(json_encode($res));
}

function create($db)
{
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$min_operators = isset($_REQUEST["min_operators"]) ? fixDB($_REQUEST["min_operators"]) : null;

	if ($min_operators == "") {
		$min_operators = "NULL";
	}

	$sql = "INSERT INTO ojt_group_requirements
			(
				ojt_group_ID,
				ojt_requirement_ID,
				min_operators,
				created_date
			)
			VALUES
			(
				$ojt_group_ID,
				$ojt_requirement_ID,
				$min_operators,
				getDate()
			)";

	$res = $db->query($sql);

	echo(json_encode($res));
}

function update($db) {
	$ojt_group_ID = isset($_REQUEST["ojt_group_ID"]) ? fixDB($_REQUEST["ojt_group_ID"]) : null;
	$ojt_requirement_ID = isset($_REQUEST["ojt_requirement_ID"]) ? fixDB($_REQUEST["ojt_requirement_ID"]) : null;
	$min_operators = isset($_REQUEST["min_operators"]) ? fixDB($_REQUEST["min_operators"]) : null;

	if ($min_operators == "") {
		$min_operators = "NULL";
	}

	$sql = "UPDATE ojt_group_requirements SET
				min_operators = $min_operators
			WHERE
				ojt_group_ID = $ojt_group_ID
				AND ojt_requirement_ID = $ojt_requirement_ID";

	$res = $db->query($sql);

	echo(json_encode($res));
}

function delete($db)
{
	$id = isset($_REQUEST["ID"]) ? fixDB($_REQUEST["ID"]) : null;

	if ($id == null) {
		die('{"error": "missing param: ID"}');
	}

	$sql = "DELETE FROM
				ojt_group_requirements
			WHERE
				ID = $id";

	$res = $db->query($sql);

	echo(json_encode($res));
}