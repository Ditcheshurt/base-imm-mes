USE [MES_COMMON]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupOperators]    Script Date: 7/23/2016 1:47:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupOperators]
(
	@ojt_group_ID int
)
RETURNS 
@result TABLE 
(
	ojt_group_operator_ID int,
	operator_ID int,
	badge_ID varchar(20),
	first_name varchar(20),
	last_name varchar(20),
	is_inherited bit,	
	ojt_group_ID int,
	group_description varchar(100)
)
AS
BEGIN
	-- get group hirearchy
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
			FROM ojt_groups
			WHERE ID = @ojt_group_ID
			UNION ALL
			SELECT og.ID, og.parent_group_ID, og.group_description
			FROM ojt_groups og
			JOIN parents ON og.ID = parents.parent_group_ID
	)
	-- work up the hierarchy
	--WITH parents
	--AS (
	--	SELECT ID, parent_group_ID, group_description
	--	FROM ojt_groups
	--	WHERE parent_group_ID = @ojt_group_ID
	--	UNION ALL
	--	SELECT og.ID, og.parent_group_ID, og.group_description
	--	FROM ojt_groups og
	--	INNER JOIN parents ON og.parent_group_ID = parents.ID
	--)
	INSERT INTO @result
	SELECT 
		gop.ID AS ojt_group_operator_ID,
		o.ID AS operator_ID,
		o.badge_ID,
		o.first_name,
		o.last_name,
		CASE WHEN gop.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		--gop.active,
		g.ID AS ojt_group_ID,
		g.group_description
	FROM
		ojt_group_operators gop
		JOIN operators o ON gop.operator_ID = o.ID
		JOIN ojt_groups g ON gop.ojt_group_ID = g.ID
	WHERE 
		ojt_group_ID IN (SELECT ID FROM parents) OR ojt_group_ID = @ojt_group_ID
	ORDER BY o.last_name
	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupRequirements]    Script Date: 7/23/2016 1:47:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupRequirements]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS 
@result TABLE 
(
	ojt_requirement_ID int,
	ojt_signoff_template_ID int,
	type_description varchar(100),
	requirement_description varchar(100),
	template_description varchar(100),
	requirement_text varchar(100),		
	image_url varchar(100),
	created_date datetime,
	updated_date datetime,
	requirement_active bit,
	is_inherited bit,
	ojt_group_requirement_ID int,
	ojt_group_ID int,
	group_description varchar(100),
	group_active bit,
	min_operators int
)
AS
BEGIN
	
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups
		WHERE ID = @ojt_group_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents ON og.ID = parents.parent_group_ID		
	)
	INSERT INTO @result
	SELECT 
		r.ID AS ojt_requirement_ID,
		r.ojt_signoff_template_ID,
		rt.type_description,
		r.requirement_description,
		st.template_description,
		r.requirement_text,		
		r.image_url,
		r.created_date,
		r.updated_date,
		r.active AS requirement_active,
		CASE WHEN gr.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		gr.ID AS ojt_group_requirement_ID,
		g.ID AS ojt_group_ID,
		g.group_description,
		g.active AS group_active,
		gr.min_operators
	FROM
		ojt_group_requirements gr
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_groups g ON gr.ojt_group_ID = g.ID
		JOIN ojt_signoff_templates st ON r.ojt_signoff_template_ID = st.ID
	WHERE 
		ojt_group_ID IN (SELECT ID FROM parents)
		AND rt.type_description <> CASE WHEN @with_alerts = 0 THEN 'Alert' ELSE '  24' END
	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTGroupSummary]    Script Date: 7/23/2016 1:47:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupSummary]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS 
@result TABLE 
(
	operator_ID int,
	operator_badge varchar(10),
	operator_first_name varchar(50),
	operator_last_name varchar(50),
	signoff_template_ID int,
	signoff_template_description varchar(100),
	ojt_requirement_ID int,
	ojt_requirement_description varchar(100),
	signoff_ID int,
	signoff_level_role_ID int,
	signoff_level_role_description varchar(100),
	signoff_level_description varchar(50),
	signoff_operator varchar(100),
	signoff_date datetime,
	signoff_expires datetime
)
AS
BEGIN
	
	INSERT INTO @result
	SELECT DISTINCT
		o.operator_ID AS operator_ID,
		o.badge_ID AS operator_badge,
		o.first_name AS operator_first_name,
		o.last_name AS operator_last_name,
		t.ID AS signoff_template_ID,
		t.template_description AS signoff_template_description,
		r.ojt_requirement_ID AS ojt_requirement_ID,
		r.requirement_description AS ojt_requirement_description,
		s.ojt_signoff_ID,
		s.ojt_signoff_template_levels_roles_ID AS signoff_level_role_ID,
		rl.name AS signoff_level_role_description,
		l.level_description AS signoff_level_description,
		CAST(o2.badge_ID AS varchar) + ': ' + o2.last_name + ', ' + o2.first_name AS signnoff_operator, 
		s.created_date AS signoff_date,
		s.expire_date AS signoff_expires
	FROM
		dbo.fn_getOJTGroupRequirements(@ojt_group_ID, @with_alerts) r
		CROSS APPLY dbo.fn_getOJTGroupOperators(@ojt_group_ID) o
		CROSS APPLY dbo.fn_getOJTOperatorRequirementSignoffLevel(o.operator_ID, r.ojt_requirement_ID) s 
		JOIN ojt_signoff_templates t ON t.ID = r.ojt_signoff_template_ID
		LEFT JOIN ojt_signoff_template_levels_roles lr ON lr.ID = s.ojt_signoff_template_levels_roles_ID
		LEFT JOIN roles rl ON rl.ID = lr.role_ID
		LEFT JOIN ojt_signoff_template_levels l ON l.ID = lr.ojt_signoff_template_level_ID
		LEFT JOIN operators o2 ON o2.ID = s.signoff_operator_ID
	ORDER BY operator_ID
	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTOperatorRequirements]    Script Date: 7/23/2016 1:47:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/20/16
-- Description:	get operator requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorRequirements]
(
	@operator_ID int,
	@with_alerts bit = 0
)
RETURNS 
@results TABLE 
(
	ojt_requirement_ID int,
	requirement_description varchar(100),
	ojt_signoff_template_ID int,
	template_description varchar(100),
	group_active bit,
	--gr.active AS group_requirement_active,
	image_url varchar(200),
	requirement_active bit
)
AS
BEGIN
	
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups	
		--WHERE ID IN 
		--(
		--	SELECT 
		--		ojt_group_ID
		--	FROM 
		--		ojt_group_operators ogo
		--	WHERE 
		--		ogo.operator_ID = @operator_ID
		--)
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents p ON og.parent_group_ID = p.ID		
	)
	--select * from parents

	INSERT INTO @results
    SELECT
		r.ID AS ojt_requirement_ID,
		r.requirement_description,
		r.ojt_signoff_template_ID,
		t.template_description,
		g.active AS group_active,
		--gr.active AS group_requirement_active,
		r.image_url,
		r.active AS requirement_active
	FROM
		ojt_groups g
		JOIN ojt_group_requirements gr ON gr.ojt_group_ID = g.ID
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_signoff_templates t ON r.ojt_signoff_template_ID = t.ID				
	WHERE
		g.ID In (SELECT DISTINCT ID FROM parents)
		AND rt.type_description <> CASE WHEN @with_alerts = 0 THEN 'Alert' ELSE '  24' END		
	ORDER BY
		r.requirement_description
	
	RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_getOJTOperatorRequirementSignoffLevel]    Script Date: 7/23/2016 1:47:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get operators highest signoff requirement
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorRequirementSignoffLevel]
(
	@ojt_operator_ID int,
	@ojt_requirement_ID int
)
RETURNS 
@result TABLE 
(
	ojt_signoff_ID int,
	ojt_requirement_ID int,
	ojt_signoff_template_levels_roles_ID int,
	operator_ID int,
	signoff_operator_ID int,
	created_date datetime,
	expire_date datetime,
	expired_by datetime
)
AS
BEGIN

	-- Declares
	DECLARE 
		-- loopy value
		@level_role_ID int,	
		@max_ojt_signoff_template_levels_role_ID int, -- holds the highest level role id	
		-- hold last signoff
		@last_ojt_signoff_ID int,
		@last_ojt_requirement_ID int,
		@last_ojt_signoff_template_levels_roles_ID int,
		@last_operator_ID int,
		@last_signoff_operator_ID int,
		@last_created_date datetime,
		@last_expire_date datetime,
		@last_expired_by datetime

	-- get ojt_signoff_template_levels_roles_ID and loop to make sure it has a signoff
	-- if no signoff is found break out and return the prior record as highest signoff
	DECLARE loopy224 CURSOR FOR

		SELECT
			lr.ID,
			x.max_ojt_signoff_template_levels_role_ID
		FROM
			ojt_requirements req
			JOIN ojt_signoff_templates t ON req.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels l ON l.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels_roles lr ON lr.ojt_signoff_template_level_ID = l.ID	
			CROSS APPLY (
				SELECT TOP 1 
					slr.ID max_ojt_signoff_template_levels_role_ID
					--slr.role_order AS max_role_order 
				FROM
					ojt_signoff_template_levels_roles slr
				WHERE
					slr.ojt_signoff_template_level_ID = l.ID 
				ORDER BY
					slr.role_order desc) x		
		WHERE
			req.ID = @ojt_requirement_ID			
		ORDER BY 
			l.level_order asc, lr.role_order asc

	OPEN loopy224

		FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID
		WHILE @@FETCH_STATUS = 0
		
		BEGIN
			DECLARE 
				-- hold current signoff	
				@ojt_signoff_ID int = null,
				@ojt_requirement_ID2 int = null,
				@ojt_signoff_template_levels_roles_ID int = null,
				@operator_ID int = null,
				@signoff_operator_ID int = null,
				@created_date datetime = null,
				@expire_date datetime = null,
				@expired_by datetime = null
		
			SELECT
				@ojt_signoff_ID = so.ID,
				@ojt_requirement_ID2 = so.ojt_requirement_ID,
				@ojt_signoff_template_levels_roles_ID = so.ojt_signoff_template_levels_roles_ID,
				@operator_ID = so.operator_ID,
				@signoff_operator_ID = so.signoff_operator_ID,
				@created_date = so.created_date,
				@expire_date = so.expire_date,
				@expired_by = so.expired_by				
			FROM				
				ojt_signoffs so								
			WHERE
				so.ojt_requirement_ID = @ojt_requirement_ID
				AND so.operator_ID = @ojt_operator_ID
				AND so.ojt_signoff_template_levels_roles_ID = @level_role_ID
				AND (so.expire_date IS NULL OR getDate() <= so.expire_date)

			IF @ojt_signoff_ID IS NULL	
				BEGIN		
					--INSERT INTO @result
					--SELECT
					--	@last_ojt_signoff_ID,
					--	@last_ojt_requirement_ID,
					--	@last_ojt_signoff_template_levels_roles_ID,
					--	@last_operator_ID,
					--	@last_signoff_operator_ID,
					--	@last_created_date,
					--	@last_expire_date,
					--	@last_expired_by
					BREAK
				END
			ELSE
				BEGIN			
					SET	@last_ojt_signoff_ID = @ojt_signoff_ID
					SET @last_ojt_requirement_ID = @ojt_requirement_ID2				
					SET	@last_operator_ID = @operator_ID
					SET	@last_signoff_operator_ID = @signoff_operator_ID
					SET	@last_created_date = @created_date
					SET	@last_expire_date = @expire_date
					SET	@last_expired_by = @expired_by
					IF @max_ojt_signoff_template_levels_role_ID = @ojt_signoff_template_levels_roles_ID
						SET	@last_ojt_signoff_template_levels_roles_ID = @ojt_signoff_template_levels_roles_ID
				
				END
			
			FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID

		END		

	CLOSE loopy224;
	DEALLOCATE loopy224; 	

	INSERT INTO @result
	SELECT
		@last_ojt_signoff_ID,
		@last_ojt_requirement_ID,
		@last_ojt_signoff_template_levels_roles_ID,
		@last_operator_ID,
		@last_signoff_operator_ID,
		@last_created_date,
		@last_expire_date,
		@last_expired_by

	
	RETURN 
END

GO
