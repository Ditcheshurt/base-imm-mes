<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
  <title>PLC Service Management</title>
	<style>
		BODY, SELECT, INPUT {
			font-family: Arial;
			font-size: 8pt;
		}

		.bolder {
			font-weight: bold;
		}

		.linker {
			color: blue;
			text-decoration:underline;
			cursor: pointer;
		}

		.section {
			margin: 10px;
			border: 1px inset #C0C0C0;
			padding: 5px;
			width: 90%;
		}

		.bit_header {
			background-color: #444444;
			vertical-align: bottom;
		}

		DIV I {
			color: #C0C0C0;
		}

		DIV SPAN {
			margin: 0px 3px 0px 3px;
		}

		TABLE TD {
			padding: 3px;
		}
		TABLE {
			border: 1px inset #D0D0D0;
		}


		TABLE#tbl_bits TD {
			padding: 1px;
			text-align: center;
			font-family: Courier New;
		}
		#div_read, #div_write {
			margin: 3px;
			padding: 3px;
			color:white;
			background-color:black;
		}
	</style>
	<script src="/common/library/prototype.js" type="text/javascript"></script>
	<script src="/common/library/jsDate.js" type="text/javascript"></script>
	<script type="text/javascript">
		function getStatus() {
			new Ajax.Request('/PLCService/PLCWebReadWrite.asmx/listObjectsAsJSON', {
				onSuccess:function(res) {
					var jso = res.responseJSON;
					var t = new Element('TABLE');
					t.width = '100%';
					var r = t.insertRow(-1);
					r.className = 'bolder';
					var c = Element.extend(r.insertCell(-1)).update('PLC IP');
					var c = Element.extend(r.insertCell(-1)).update('Counter');
					var c = Element.extend(r.insertCell(-1));
					var c = Element.extend(r.insertCell(-1));
					for (var i = 0; i < jso.length; i++) {
						var r = Element.extend(t.insertRow(-1));
						r.writeAttribute(jso[i]);
						var c = Element.extend(r.insertCell(-1));
						c.update(jso[i].plc_ip).addClassName('linker').observe('click', function() {showDetail(this.up());});
						var c = Element.extend(r.insertCell(-1));
						c.update(jso[i].counter);
						if (jso[i].plc_path != '0') {
							c.insert('<i>&nbsp;&nbsp;PATH=' + jso[i].plc_path + '</i>');
						}
						var c = Element.extend(r.insertCell(-1))
							.update('read')
							.addClassName('linker')
							.observe('click', function() {$('txt_plc_ip').value = this.up().readAttribute('plc_ip');
																	$('txt_plc_path').value = this.up().readAttribute('plc_path');
																	$('txt_plc_file_addr').focus();});
						var c = Element.extend(r.insertCell(-1))
							.update('write')
							.addClassName('linker')
							.observe('click', function() {$('txt_plc_ip2').value = this.up().readAttribute('plc_ip');
																	$('txt_plc_path2').value = this.up().readAttribute('plc_path');
																	$('txt_plc_file_addr2').focus();});
					}
					$('div_status').update(t);
					$('div_status_detail').update();
				}
			});
		}

		function showDetail(row_el) {
			new Ajax.Request('/PLCService/PLCWebReadWrite.asmx/listObjectsAsJSON', {
				onSuccess:function(res) {
					var jso = res.responseJSON;
					for (var i = 0; i < jso.length; i++) {
						if (jso[i].plc_ip == row_el.readAttribute('plc_ip')) {
							$('div_status_detail').update('<strong>IP:</strong>' + row_el.readAttribute('plc_ip') + '<br>' +
								'<strong>Path:</strong> ' + jso[i].plc_path + '<br>' +
								'<strong>Counter:</strong> ' + jso[i].counter + '<br>' +
								'<strong>Last Operation:</strong> ' + jso[i].last_op +
									' <span class=linker onclick="monitor(\'' + row_el.readAttribute('plc_ip') + '\', \'' + jso[i].plc_path + '\', \'' + jso[i].last_op + '\');">Monitor</span><br>' +
								'<strong>Last Operation Time:</strong> ' + jso[i].last_op_time + '<br>' +
								'<strong>Last Requestor:</strong> ' + jso[i].last_requestor + '<br>' +
								'<span class=linker onclick="clearObject(\'' + row_el.readAttribute('plc_ip') + '\');">Clear Object</span>'
								);
						}
					}
				}
			});
		}

		function monitor(plc_ip, plc_path, addr) {
			addr = addr.replace('read ', '');
			addr = addr.replace('write ', '');
			$('txt_plc_ip').value = plc_ip;
			$('txt_plc_path').value = plc_path;
			$('txt_plc_file_addr').value = addr;
			getValue(1);
		}

		function clearObject(plc_ip) {
			new Ajax.Request('/PLCService/PLCWebReadWrite.asmx/clearObject', {
				parameters:{"plc_ip":plc_ip},
				onSuccess:function(res) {
					$('div_status_detail').update('Cleared!!! Refreshing in 2 seconds.');
					setTimeout(function() {getStatus();}, 2000);
				}
			});
		}

		var read_timer = null;
		function getValue(re_read) {
			var url = '/PLCService/PLCWebReadWrite.asmx/getValue';
			if ($F('hid_read_type') == 'JSON') {
				url += 'AsJSON';
			} else {
				if ($F('txt_plc_path') != '0') {
					url += 'WithPath';
				}
			}

			if ($F('txt_plc_file_addr').indexOf(':') > 0) {
				url = '/PLCService/PLCWebReadWrite.asmx/getABValue';
			}

			new Ajax.Request(url, {
				parameters:{"plc_ip":$F('txt_plc_ip'), "plc_path":$F('txt_plc_path'), "file_addr":$F('txt_plc_file_addr')},
				onSuccess:function(res) {
					if ($F('hid_read_type') == 'JSON') {
						if (res.responseJSON.ErrorCode == '0') {
							$('div_read').update('Value: ' + res.responseJSON.value + ' <i>' + res.responseJSON.TimeStamp + '</i>');

							if ($('cb_showbitmask').checked && !isNaN(res.responseJSON.value)) {
								var s = '';
								var r = '';
								for (var i = 0; i < 32; i++) {
									var b = '';
									if ((i % 8) == 7 && i < 32) {
										b = ' style="border-left: 1px solid white;"';
									}
									if ((Math.pow(2, i) & res.responseJSON.value) == Math.pow(2, i)) {
										s = '<td style=background-color:limegreen;>1</td>' + s;
									} else {
										s = '<td style=background-color:red;>0</td>' + s;
									}

									r = '<td ' + b + '>' + i.toString().split('').join('<br>') + '</td>' + r;
								}
								s = '<table id=tbl_bits cellpadding=0 cellspacing=0 width=100%><tr class="bit_header">' + r + '</tr><tr>' + s + '</tr></table>';
								$('div_read').insert('<br>' + s);
							}
						} else {
							$('div_read').update('ERROR!: ' + res.responseJSON.ErrorCode + ' ' + res.responseJSON.ErrorString);
						}
					} else {
						$('div_read').update(res.responseText);
					}

					if (re_read == 1) {
						$('sp_stop_read').show();
						$('sp_re_read').hide();
						read_timer = setTimeout(function() {getValue(1);}, 1000);
					}
				},
				onFailure: function(res) {
					$('div_read').update('ERROR!! ' + res.responseText);
				}
			});
		}

		function setValue(verify) {
			var url = '/PLCService/PLCWebReadWrite.asmx/setValueAsJSON';

			new Ajax.Request(url, {
				parameters:{"plc_ip":$F('txt_plc_ip2'), "plc_path":$F('txt_plc_path2'), "file_addr":$F('txt_plc_file_addr2'), "new_value":$F('txt_new_value'), "data_type":$F('sel_data_type')},
				onSuccess:function(res) {
					var jso = res.responseJSON;
					if (jso.ErrorCode == '0') {
						$('div_write').update('Write successful! ' + jso.TimeStamp);
					} else {
						$('div_write').update('ERROR!: ' + jso.ErrorCode + ' ' + jso.ErrorString);
					}
				},
				onFailure: function(res) {
					$('div_write').update('ERROR!! ' + res.responseText);
				}
			});
		}
	</script>
</head>
<body>
	<div class="bolder" align="">PLC Service Management Utility</div>
	<div class="section">
		<div class="bolder">Status</div>
		<div class="linker" onclick="getStatus();">Get/Refresh</div>
		<div id="div_status"></div>
		<div id="div_status_detail"></div>
	</div>
	<div class="section">
		<div class="bolder">Read</div>
		<div>
			<div>PLC Type: </div>
			<div>PLC IP: <input type="text" id="txt_plc_ip" value="" class=""></div>
			<div>Path: <input type="text" id="txt_plc_path" value="0" class=""></div>
			<div>File Address: <input type="text" id="txt_plc_file_addr" value="" class=""> <i>Use TAG_NAME.# for bits.</i></div>
			<div>
				<input type="radio" id="rad_read_type_JSON" name="rad_read_type" value="JSON" class="" CHECKED onclick="$('hid_read_type').value='JSON';"> Return JSON<br>
				<input type="radio" id="rad_read_type_TEXT" name="rad_read_type" value="TEXT" class="" onclick="$('hid_read_type').value='TEXT';"> Return Text
				<input type="hidden" id="hid_read_type" value="JSON">
			</div>
			<div>Show bitmask (JSON only)? <input type="checkbox" id="cb_showbitmask" value="" class=""></div>
			<div>
				<span class="linker" onclick="getValue(0);">Read</span>
				<span class="linker" onclick="getValue(1);" id="sp_re_read">Read Every Second</span>
				<span class="linker" onclick="clearTimeout(read_timer);$('sp_re_read').show();$('sp_stop_read').hide();" id="sp_stop_read" style="display:none;">Stop Reading</span>
			</div>
		</div>
		<div id="div_read"></div>
	</div>
	<div class="section">
		<div class="bolder">Write</div>
		<div>
			<div>PLC IP: <input type="text" id="txt_plc_ip2" value="" class=""></div>
			<div>Path: <input type="text" id="txt_plc_path2" value="0" class=""></div>
			<div>File Address: <input type="text" id="txt_plc_file_addr2" value="" class=""> <i>Use TAG_NAME.# for bits.</i></div>
			<div>Value: <input type="text" id="txt_new_value" value="" class=""> <i>Use True/False for bits.</i></div>
			<div>Data type: <select id="sel_data_type">
				<option value="DINT">DINT</option>
				<option value="SINT">SINT</option>
				<option value="BOOL">BOOL</option>
				<option value="INT">INT</option>
				</select></div>
			<div><input type="radio" id="rad_write_type" name="rad_write_type" value="JSON" class="" CHECKED> Return JSON<br><input type="radio" id="rad_write_type" name="rad_write_type" value="TEXT" class=""> Return Text</div>
			<div class="linker" onclick="setValue(0);">Write</div>
		</div>
		<div id="div_write"></div>
	</div>
</body>
</html>
