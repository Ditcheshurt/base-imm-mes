//Requires prototype

function readPLC(plc_ip, file_addr, callback, extra_data) {
	new Ajax.Request('http://10.68.160.58/PLCService/PLCWebReadWrite.asmx/getValueAsJSON',
		{
			"parameters":{
				"plc_ip":plc_ip,
				"file_addr":file_addr,
				"plc_path":0
			},
			"onSuccess":function(res) {
				callback(res.responseJSON.value, extra_data);
			},
			"onCreate": function(response) { // here comes the fix
				  var t = response.transport;
				  t.setRequestHeader = t.setRequestHeader.wrap(function(original, k, v) {
						if (/^(accept|accept-language|content-language)$/i.test(k))
							 return original(k, v);
						if (/^content-type$/i.test(k) &&
							 /^(application\/x-www-form-urlencoded|multipart\/form-data|text\/plain)(;.+)?$/i.test(v))
							 return original(k, v);
						return;
				  });
			 }
		}
	);
}

function readABPLC(plc_ip, file_addr, callback, extra_data) {
	new Ajax.Request('http://EBVMESWEB01/PLCService/PLCWebReadWrite.asmx/getABValue',
		{
			"parameters":{
				"plc_ip":plc_ip,
				"file_addr":file_addr
			},
			"onSuccess":function(res) {
				callback(res.responseJSON.value, extra_data);
			}
		}
	);
}
