SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Michelle Stewart
-- Create date: 3/18/2014
-- Description:	Find downtime between start and finish
--		based off a record ID
-- =============================================
CREATE FUNCTION [dbo].[getDowntime] 
(
	-- Add the parameters for the function here
	@rec_id int,
	@start_date datetime,
	@end_date datetime,
	@state bit
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @time_in_state float

	SELECT @time_in_state = 
		CASE WHEN time_in_state IS NULL THEN  CAST(DATEDIFF(ss, change_time, @end_date) as float)/60.0 ELSE 
			CASE WHEN change_time < @start_date AND DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) < @end_date AND DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) > @start_date THEN 
				CAST(DATEDIFF(n, @start_date, DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time)) As float) 
			   WHEN change_time > @start_date AND DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) > @end_date THEN 
				CAST(DATEDIFF(n, change_time, @end_date) As float) 
			   WHEN change_time > @start_date AND DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) < @end_date THEN 
			   CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float)  
			   WHEN DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) < @start_date THEN 
				0 
			   WHEN change_time > @end_date THEN 
				0 
			   WHEN change_time < @start_date AND DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN 
      								(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
      								ELSE time_in_state END as float) , change_time) > @end_date THEN 
				CAST(DATEDIFF(n, @start_date, @end_date) As float) 
			  END 
			END 
	FROM machine_downtime_log mdl
	WHERE up = @state AND 
		DATEADD(n, CAST(CASE WHEN up = @state AND time_in_state IS NULL THEN
    		(CAST(DATEDIFF(ss, change_time, @end_date) As float)/60.0) 
		  ELSE time_in_state END as float), mdl.change_time) >= @start_date AND
		mdl.change_time <= @end_date
		AND ID = @rec_id

	-- Return the result of the function
	RETURN @time_in_state

END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
