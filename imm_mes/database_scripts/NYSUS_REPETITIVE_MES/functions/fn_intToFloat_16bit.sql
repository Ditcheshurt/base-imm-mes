SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 11/27/2018
-- Description:	This will convert an 16 bit float represented as an integer
-- =============================================
CREATE FUNCTION fn_intToFloat_16bit 
(
	-- Add the parameters for the function here
	@int int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @out float

	-- Add the T-SQL statements to compute the return value here
	DECLARE @sign int
	DECLARE @expn int
	DECLARE @mant float

	SET @sign = CASE CAST(@int AS binary(2)) & 32768 WHEN 0 THEN 1 ELSE -1 END
	SET @expn = ((@int & 31744)/1024)-15
	SET @mant = CAST((@int & 1023) AS float)/1024

	DECLARE @res float = 1, @c int = 1
	WHILE (@c <= 10) BEGIN
		SET @mant = @mant * 2;
		IF (@mant >= 1) BEGIN
			SET @res = @res + (1/CAST(POWER(2,@c) AS float));
			SET @mant = @mant - 1
		END
		SET @c = @c + 1
	END;
	SET @out = (CAST(POWER(2,@expn) AS float) * @res) * @sign

	-- Return the result of the function
	RETURN @out

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
