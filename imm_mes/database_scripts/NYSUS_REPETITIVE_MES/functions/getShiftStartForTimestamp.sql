SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 8/5/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getShiftStartForTimestamp] 
(
	-- Add the parameters for the function here
	@system_ID int,
	@dt datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res datetime
	DECLARE @shift int, @shift_start_time float, @prod_date datetime

	SET @shift = dbo.getShift(@system_ID, @dt)
	SET @shift_start_time = dbo.getShiftStartTime(@system_ID, @shift)
	SET @prod_date = dbo.stripTime(@dt)

	IF @shift = 3 AND DATEPART(HOUR, @dt) < 7
		SET @prod_date = @prod_date - 1

	SET @res = DATEADD(MINUTE, @shift_start_time * 60.0, @prod_date)

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
