SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 9/16/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getMachineIDFromTagDesc] 
(
	-- Add the parameters for the function here
	@tag_desc varchar(50)
)
RETURNS int
AS
BEGIN
	DECLARE @res int

	SET @res = CAST(LEFT(@tag_desc, 5) as int)

	RETURN @res

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
