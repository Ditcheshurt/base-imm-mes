SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[stripTime](@time datetime)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res datetime

	-- Add the T-SQL statements to compute the return value here
	SET @res = CAST(DATEPART(mm, @time) as varchar(50)) + '/' + CAST(DATEPART(dd, @time) As varchar(50)) + '/'+ CAST(DATEPART(yyyy, @time) as varchar(50))

	-- Return the result of the function
	RETURN @res

END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
