SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		R.Kahle
-- Create date: 5/17/2017
-- Description:	This will create a PME serial barcode string based on the current server time <-----THIS IS IMPORTANT
-- =============================================
CREATE FUNCTION [dbo].[fn_createProperSerial] 
(
	-- Add the parameters for the function here
	@prefix varchar(3),
	@separator char(1),
	@postfix varchar(8)
)
RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable
	DECLARE @Result varchar(20)

	-- Declare function variables
	DECLARE @dateG datetime = GETDATE()
	DECLARE @dayG float = CONVERT(float, DATEPART(d, @dateG))
	DECLARE @monthG float = CONVERT(float, DATEPART(m, @dateG))
	DECLARE @yearG float = CONVERT(float, DATEPART(yyyy, @dateG))
	DECLARE @seconds float = ( CONVERT(float, DATEPART(hh, @dateG)*3600 + DATEPART(n, @dateG)*60 + DATEPART(s, @dateG)) + (CONVERT(float, DATEPART(ms, @dateG))/1000) )/86400
	DECLARE @A float, @B float
	DECLARE @dateJ float = 0
	DECLARE @julian bigint = 0
	DECLARE @res varchar(8)

	-- Add the T-SQL statements to compute the return value here
	IF (@monthG <= 12)
		BEGIN
			SET @monthG = @monthG + 12
			SET @yearG = @yearG - 1
		END

	SET @A = FLOOR(@yearG/100)
	SET @B = FLOOR(@A / 4) - @A
	SET @dateJ = ((FLOOR(365.25*(@yearG+4716)) + FLOOR(30.6001*(@monthG+1)) + @dayG + @B) - 1524.5) + @seconds
	SET @julian = CAST(CEILING(@dateJ*1000000) AS bigint)

	WHILE @julian > 0
		BEGIN
			SELECT @res = CONCAT(CHAR(@julian % 36 + CASE WHEN @julian % 36 < 10 THEN 48 ELSE 55 END), @res), @julian = FLOOR(@julian/36)
		END

	SET @Result = CONCAT(@prefix, @res, @separator, @postfix)

	-- Return the result of the function
	RETURN @Result
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
