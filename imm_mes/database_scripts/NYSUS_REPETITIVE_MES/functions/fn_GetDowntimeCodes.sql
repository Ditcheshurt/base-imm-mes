SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetDowntimeCodes] 
(	
	@machine_type_ID int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH hier AS (SELECT        ID, parent_ID, reason_description, count_against, active, CONVERT(varchar(50), ID) AS S, CONVERT(varchar(500), '') AS grp, 0 AS L, machine_type_ID, oee_category, maximum_duration
                               FROM            dbo.machine_downtime_reasons
                               WHERE        (parent_ID = 0 AND ID > 0) AND is_micro_stop IS NULL
								  AND machine_type_ID = @machine_type_ID
                               UNION ALL
                               SELECT        mdr.ID, mdr.parent_ID, mdr.reason_description, mdr.count_against, mdr.active, CONVERT(varchar(50), hier_2.S + '/' + CONVERT(varchar(5), mdr.ID)) AS S, CONVERT(varchar(500), 
                                                        CASE WHEN mdr.parent_ID > 0 THEN CASE WHEN LEN(hier_2.grp) > 0 THEN hier_2.grp + ' - ' + hier_2.reason_description ELSE hier_2.reason_description END ELSE hier_2.reason_description END)
                                                         AS grp, hier_2.L + 1 AS L, mdr.machine_type_ID, mdr.oee_category, mdr.maximum_duration
                               FROM            dbo.machine_downtime_reasons AS mdr INNER JOIN
                                                        hier AS hier_2 ON hier_2.ID = mdr.parent_ID)
    SELECT        ID, parent_ID, reason_description, count_against, active, S, grp, L, machine_type_ID, oee_category, maximum_duration
     FROM            hier AS hier_1
)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
