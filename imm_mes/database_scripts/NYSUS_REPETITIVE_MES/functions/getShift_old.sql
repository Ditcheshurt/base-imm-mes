SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getShift_old] 
(
	-- Add the parameters for the function here
	@dt datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res int
	DECLARE @time_as_float float

	SET @time_as_float = DATEPART(HOUR, @dt) + (CAST(DATEPART(MINUTE, @dt) as float) / 60.0)

	IF @time_as_float >= 6.5 AND @time_as_float < 14.5
		SET @res = 1
	ELSE IF @time_as_float >= 14.5 AND @time_as_float < 22.5
		SET @res = 2
	ELSE
		SET @res = 3 

	-- Return the result of the function
	RETURN @res

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
