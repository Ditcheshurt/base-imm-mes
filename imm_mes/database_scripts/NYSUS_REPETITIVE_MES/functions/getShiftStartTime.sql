SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/20/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getShiftStartTime] 
(
	-- Add the parameters for the function here
	@system_ID int,
	@the_shift int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result float

	--IF @the_shift = 1
	--	SET @Result = 6.75
	--ELSE IF @the_shift = 2
	--	SET @Result = 14.75
	--ELSE
	--	SET @Result = 22.75
	
	SELECT TOP 1 
		@Result = 
		DATEPART(hh, ti.interval_start_time) + CONVERT(float, DATEPART(mi, ti.interval_start_time)) / 60
	FROM
		MES_COMMON.dbo.shifts s
		JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN MES_COMMON.dbo.time_intervals ti ON sti.time_interval_ID = ti.ID
	WHERE
		s.system_ID = @system_ID
		AND s.shift_number = @the_shift
		AND sti.shift_start = 1
		ORDER BY sti.is_day_start DESC, sti.ID ASC-- ti.interval_start_time ASC 

	-- Return the result of the function
	RETURN @Result

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
