SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		R.Kahle
-- Create date: 5/9/2017
-- Description:	This will get the lowest next available container position
-- =============================================
CREATE FUNCTION [dbo].[fn_getNextAvailableContainerPosition] 
(
	-- Add the parameters for the function here
	@container_ID int = 0
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @nextAvailPos int, @last_cp INT = 0, @cp INT = 0

	-- Add the T-SQL statements to compute the return value here
	DECLARE loopy_acc58c CURSOR FOR SELECT container_position FROM container_parts WHERE container_ID = @container_ID ORDER BY container_position ASC
	OPEN loopy_acc58c
	FETCH NEXT FROM loopy_acc58c INTO @cp
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF ((@cp - @last_cp) > 1)
			BEGIN
				BREAK -- found a break in sequence, break
			END
		ELSE
			SET @last_cp = @cp -- no break in sequence, continue on
		FETCH NEXT FROM loopy_acc58c INTO @cp
	END
	CLOSE loopy_acc58c
	DEALLOCATE loopy_acc58c

	SET @nextAvailPos = @last_cp + 1

	-- Return the result of the function
	RETURN @nextAvailPos

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
