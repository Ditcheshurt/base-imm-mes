SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getShift] 
(
	-- Add the parameters for the function here
	@system_ID int = 1,
	@dt datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	--DECLARE @res int
	--DECLARE @time_as_float float

	--SET @time_as_float = DATEPART(HOUR, @dt) + (CAST(DATEPART(MINUTE, @dt) as float) / 60.0)

	--IF @time_as_float >= 6.5 AND @time_as_float < 14.5
	--	SET @res = 1
	--ELSE IF @time_as_float >= 14.5 AND @time_as_float < 22.5
	--	SET @res = 2
	--ELSE
	--	SET @res = 3 

	---- Return the result of the function
	--RETURN @res

	DECLARE @res int

	SELECT 
		@res = s.shift_number 
	FROM
		MES_COMMON.dbo.shifts s
		JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN MES_COMMON.dbo.time_intervals ti ON sti.time_interval_ID = ti.ID
	WHERE
		s.system_ID = @system_ID
		--AND CONVERT(time, @dt) BETWEEN ti.interval_start_time AND ti.interval_end_time
		AND (
			CASE WHEN DATEDIFF(second,ti.interval_start_time,ti.interval_end_time) > 0 THEN
				CASE WHEN (CONVERT(time,@dt) >= ti.interval_start_time) AND (CONVERT(time,@dt) <= ti.interval_end_time) THEN 1 ELSE 0 END
			ELSE
				CASE WHEN (CONVERT(time,@dt) >= ti.interval_start_time) OR (CONVERT(time,@dt) <= ti.interval_end_time) THEN 1 ELSE 0 END
			END
		) > 0
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
