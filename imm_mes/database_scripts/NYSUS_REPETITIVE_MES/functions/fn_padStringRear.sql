SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 2/8/2013
-- Description:	Pads a string
-- =============================================
CREATE FUNCTION [dbo].[fn_padStringRear] 
(
	-- Add the parameters for the function here
	@s varchar(200),
	@length int,
	@pad_char char(1)
)
RETURNS varchar(200)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res varchar(200)
	
	SET @res = @s
	IF LEN(@s) < @length
		SET @res = @s + REPLICATE(@pad_char, @length - LEN(@s))

	-- Return the result of the function
	RETURN @res

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
