SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO






CREATE FUNCTION [dbo].[fn_calculateMachineDowntimeFromLog]
	(@s datetime, @e datetime, @machine_ID int)
RETURNS float
AS
BEGIN
	DECLARE @cycle_time_before_start datetime, @target_cycle_time int
	DECLARE @target_cycle_end datetime
	DECLARE @i int
	DECLARE @cycle_time_after_start datetime
	DECLARE @this_cycle_time datetime, @last_cycle_time datetime
	DECLARE @downtime_minutes float


	SELECT @downtime_minutes = SUM(
			CASE 
				WHEN dl.change_time > @s AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
					dl.time_in_state
				WHEN dl.change_time < @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @s
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
					DATEDIFF(SECOND, @s, DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time)) / 60.0
				WHEN dl.change_time > @s 
						AND dl.change_time < @e
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
					DATEDIFF(SECOND, dl.change_time, @e) / 60.0
				WHEN dl.change_time < @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
					DATEDIFF(SECOND, @s, @e) / 60.0
				WHEN dl.change_time <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @s THEN 0
				WHEN dl.change_time > @e THEN 0
			END)
	FROM machine_downtime_log dl
		JOIN machine_downtime_reasons dr ON dl.reason_ID = dr.ID
		JOIN machine_downtime_reason_oee_categories c ON dr.oee_category = c.ID AND c.count_against_oee = 1
	WHERE dl.machine_ID = @machine_ID
		AND dl.up = 0
		AND change_time > @s - 1

	RETURN ISNULL(@downtime_minutes, 0.0)
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
