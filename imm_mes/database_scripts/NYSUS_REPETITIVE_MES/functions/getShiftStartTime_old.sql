SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/20/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getShiftStartTime_old] 
(
	-- Add the parameters for the function here
	@the_shift int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result float

	IF @the_shift = 1
		SET @Result = 6.75
	ELSE IF @the_shift = 2
		SET @Result = 14.75
	ELSE
		SET @Result = 22.75

	-- Return the result of the function
	RETURN @Result

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
