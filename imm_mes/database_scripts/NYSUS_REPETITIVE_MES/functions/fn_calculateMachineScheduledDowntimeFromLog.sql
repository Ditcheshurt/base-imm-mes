SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO







CREATE FUNCTION [dbo].[fn_calculateMachineScheduledDowntimeFromLog]
	(@s datetime, @e datetime, @machine_ID int)
RETURNS float
AS
BEGIN
	DECLARE @scheduled_downtime_minutes float


	SELECT @scheduled_downtime_minutes = ISNULL(SUM(									
			CASE 
				WHEN  dl.change_time > @s AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
					dl.time_in_state
				WHEN  dl.change_time < @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @s
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
					DATEDIFF(SECOND, @s, DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time)) / 60.0
				WHEN  dl.change_time > @s 
						AND dl.change_time < @e
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
					DATEDIFF(SECOND, dl.change_time, @e) / 60.0
				WHEN  dl.change_time < @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
					DATEDIFF(SECOND, @s, @e) / 60.0
				WHEN  dl.change_time <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               @s 
						AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @s THEN 0
				WHEN  dl.change_time > @e THEN 0
			END
			), 0)
	FROM machine_downtime_log dl
		JOIN machine_downtime_reasons dr ON dl.reason_ID = dr.ID
		JOIN machine_downtime_reason_oee_categories c ON dr.oee_category = c.ID AND c.is_scheduled = 1
	WHERE dl.machine_ID = @machine_ID
		AND dl.up = 0
		AND change_time > @s - 1

	RETURN ISNULL(@scheduled_downtime_minutes, 0.0)
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
