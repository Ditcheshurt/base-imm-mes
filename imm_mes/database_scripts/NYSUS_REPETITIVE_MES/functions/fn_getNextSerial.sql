SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 7/24/2016
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getNextSerial] 
(
	-- Add the parameters for the function here
	@tool_part_ID int
)
RETURNS varchar(5)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res varchar(5)

  
	DECLARE @v INT
	DECLARE @w INT
	DECLARE @x INT
	DECLARE @y INT
	DECLARE @z INT
	DECLARE @theMin INT
	DECLARE @theMax INT
	DECLARE @thisOne VARCHAR(5) = 'ZZZZZ'

    SET @theMax = 90
    SET @theMin = 65


    SELECT TOP 1 @thisOne = ISNULL(serial_number,'ZZZZZ') 
	FROM machine_cycle_parts 
	WHERE tool_part_ID = @tool_part_ID
	ORDER BY ID DESC

    IF Len(@thisOne) <> 5
		SET @thisOne = 'ZZZZZ'

    SET @thisOne = UPPER(@thisOne)      


    SET @v = Ascii(Left(@thisOne, 1))
    SET @w = Ascii(SUBSTRING(@thisOne, 2, 1))
    SET @x = Ascii(SUBSTRING(@thisOne, 3, 1))
    SET @y = Ascii(SUBSTRING(@thisOne, 4, 1))
    SET @z = Ascii(Right(@thisOne, 1))

    IF @z < @theMin 
	SET @z = @theMax
    IF @y < @theMin 
	SET @y = @theMax
    IF @x < @theMin 
	SET @x = @theMax
    IF @w < @theMin 
	SET @w = @theMax
    IF @v < @theMin 
	SET @v = @theMax

           
    SET @z = @z + 1
    If @z > @theMax OR @z < @theMin
      BEGIN
        SET @z = @theMin
        SET @y = @y + 1
        If @y > @theMax OR @y < @theMin
          BEGIN 
            SET @y = @theMin
            SET @x = @x + 1
            If @x > @theMax OR @x < @theMin
	       BEGIN
	         SET @x = @theMin
	         SET @w = @w + 1
                      if @w > @theMax OR @w < @theMin
                         BEGIN 
		SET @w = @theMin
                           SET @v = @v + 1
		   IF @v > @theMax OR @v < @theMin
          		      BEGIN
                                 SET @v = @theMin
		      END
                         END 
	       END
          END
      END

    SET @res = Char(@v) + Char(@w) + Char(@x) + Char(@y) + Char(@z)   


	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
