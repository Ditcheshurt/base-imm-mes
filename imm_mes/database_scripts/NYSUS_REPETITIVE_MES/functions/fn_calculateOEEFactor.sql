SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		R.Kahle
-- Create date: 5/9/2017
-- Description:	This will calculate the performance, quality, or availability OEE factors (use downtime for @actual)
-- =============================================
CREATE FUNCTION [dbo].[fn_calculateOEEFactor]
(
	-- Add the parameters for the function here
	@factor varchar(15),
	@actual int,
	@target int,
	@target_adj int
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @oee_factor float

	-- Add the T-SQL statements to compute the return value here
	IF(@factor = 'perf')
		SELECT @oee_factor = (case when isnull(@target,(0))>(0) then CONVERT([float],@actual)/CONVERT([float],@target) else (0) end)
	IF(@factor = 'qual')
		SELECT @oee_factor = (case when isnull(@target,(0))>(0) then CONVERT([float],isnull(@actual,(0)))/CONVERT([float],@target) else (0) end)
	IF(@factor = 'avail')
		SELECT @oee_factor = (case when isnull(@target,(0))>(0) then (CONVERT([float],(@target-@actual)-@target_adj))/CONVERT([float],@target-@target_adj) else (0) end)

	-- Return the result of the function
	RETURN @oee_factor

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
