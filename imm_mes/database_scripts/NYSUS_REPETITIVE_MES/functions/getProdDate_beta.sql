SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Robert Kahle
-- Create date: 7/24/2017
-- Description:	This is the new version that allows the use of shift tables
-- =============================================
CREATE FUNCTION [dbo].[getProdDate_beta] 
(
	-- Add the parameters for the function here
	@the_time datetime
	,@split_shift_start int = 23
	,@split_shift_end int = 7
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res datetime
	DECLARE @time_as_float float
	DECLARE @date_no_time datetime

	SET @date_no_time = dbo.stripTime(@the_time)
	SET @time_as_float = DATEPART(HOUR, @the_time) + (CAST(DATEPART(MINUTE, @the_time) as float) / 60.0)

	IF @time_as_float >= @split_shift_start OR @time_as_float < @split_shift_end
		SET @res = DATEADD(DAY, -1, @date_no_time)
	ELSE
		SET @res = @date_no_time

	-- Return the result of the function
	RETURN @res

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
