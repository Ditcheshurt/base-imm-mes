SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/21/2015
-- Description:	
-- =============================================
CREATE FUNCTION getShiftOrder 
(
	-- Add the parameters for the function here
	@shift int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	IF @shift = 3
		SET @Result = 1
	ELSE IF @shift = 1
		SET @Result = 2
	ELSE
		SET @Result = 3

	-- Return the result of the function
	RETURN @Result

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
