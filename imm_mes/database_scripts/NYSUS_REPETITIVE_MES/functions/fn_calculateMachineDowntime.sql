SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO





CREATE FUNCTION [dbo].[fn_calculateMachineDowntime]
	(@start_time datetime,
	@end_time datetime,
	@machine_ID int)
RETURNS float
AS
BEGIN
	DECLARE @cycle_time_before_start datetime, @target_cycle_time int
	DECLARE @target_cycle_end datetime
	DECLARE @i int
	DECLARE @cycle_time_after_start datetime
	DECLARE @this_cycle_time datetime, @last_cycle_time datetime
	DECLARE @total_downtime float, @this_downtime float

	SET @total_downtime = 0

	SELECT TOP 1 @cycle_time_before_start = mc.cycle_time, @target_cycle_time = mt.target_cycle_time
	FROM machine_cycles mc
		JOIN machine_tools mt ON mc.machine_ID = mt.machine_ID AND mc.tool_ID = mt.tool_ID
	WHERE mc.machine_ID = @machine_ID
		AND mc.cycle_time < @start_time
	ORDER BY mc.ID DESC

	SET @target_cycle_end = DATEADD(MILLISECOND, @target_cycle_time * 1150, @cycle_time_before_start)

	--PRINT 'TARGET CYCLE TIME BEFORE START: ' + CAST(@target_cycle_time as varchar(50))
	--PRINT 'LAST CYCLE BEFORE START: ' + CAST(@cycle_time_before_start as varchar(50))
	--PRINT 'LAST CYCLE TARGET END: ' + CAST(@target_cycle_end as varchar(50))

	SET @target_cycle_time = NULL
	SELECT TOP 1 @cycle_time_after_start = mc.cycle_time, @target_cycle_time = mt.target_cycle_time
	FROM machine_cycles mc
		JOIN machine_tools mt ON mc.machine_ID = mt.machine_ID AND mc.tool_ID = mt.tool_ID
	WHERE mc.machine_ID = @machine_ID
		AND mc.cycle_time > @start_time
		AND mc.cycle_time < @end_time
	ORDER BY mc.ID ASC

	--PRINT 'TARGET CYCLE TIME AFTER START: ' + CAST(@target_cycle_time as varchar(50))
	--PRINT 'NEXT CYCLE AFTER START: ' + CAST(@cycle_time_after_start as varchar(50))

	IF @cycle_time_after_start IS NULL
		SET @total_downtime = DATEDIFF(MILLISECOND, @start_time, @end_time) / 1000.0
	ELSE
		BEGIN 
			--GET ANY DOWNTIME AFTER START TIME AND BEFORE FIRST CYCLE
			IF @target_cycle_end < @start_time  --DOWNTIME IS FROM START TIME UNTIL FIRST CYCLE
				BEGIN
					SET @total_downtime = DATEDIFF(MILLISECOND, @start_time, @cycle_time_after_start) / 1000.0
					--PRINT 'DOWNTIME FROM START TO FIRST CYCLE: ' + CAST(@total_downtime as varchar(50))
				END
			ELSE
				BEGIN
					IF @target_cycle_end < @cycle_time_after_start  --DOWNTIME IS FROM TARGET END OF LAST CYCLE UNTIL FIRST CYCLE
						BEGIN
							SET @total_downtime = DATEDIFF(MILLISECOND, @target_cycle_end, @cycle_time_after_start) / 1000.0
							--PRINT 'DOWNTIME FROM TARGET END TIME OF LAST CYCLE TO FIRST CYCLE: ' + CAST(@total_downtime as varchar(50))
						END
				END

			--LOOP THROUGH CYCLES
			DECLARE loopy234234 CURSOR FOR
				SELECT mc.cycle_time, mt.target_cycle_time
				FROM machine_cycles mc
					JOIN machine_tools mt ON mc.machine_ID = mt.machine_ID AND mc.tool_ID = mt.tool_ID
				WHERE mc.machine_ID = @machine_ID
					AND mc.cycle_time > @start_time
					AND mc.cycle_time < @end_time
				ORDER BY mc.ID ASC;

			OPEN loopy234234;
			SET @i = 0
			FETCH NEXT FROM loopy234234 INTO @this_cycle_time, @target_cycle_time
			WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @i > 0 --DON'T CARE ABOUT THE FIRST ONE
						BEGIN
							--PRINT 'CHECKING CYCLE: ' + CAST(@this_cycle_time as varchar(50)) + ' TARGET: ' + CAST(@target_cycle_time as varchar(50))
							SET @target_cycle_end = DATEADD(MILLISECOND, @target_cycle_time * 1150, @last_cycle_time)
							--PRINT 'TARGET CYCLE END: ' + CAST(@target_cycle_end as varchar(50))

							IF @this_cycle_time > @target_cycle_end
								BEGIN
									SET @this_downtime = DATEDIFF(MILLISECOND, @target_cycle_end, @this_cycle_time) / 1000.0
									--PRINT 'DOWNTIME FROM TARGET END TIME OF LAST CYCLE TO NEXT CYCLE: ' + CAST(@this_downtime as varchar(50))
									SET @total_downtime = @total_downtime + @this_downtime
								END
						END

					SET @last_cycle_time = @this_cycle_time
					SET @i = @i + 1
					--PRINT ''
					FETCH NEXT FROM loopy234234 INTO @this_cycle_time, @target_cycle_time
				END

			CLOSE loopy234234
			DEALLOCATE loopy234234

			--NOW CHECK LAST CYCLE
			SET @target_cycle_end = DATEADD(MILLISECOND, @target_cycle_time * 1150, @last_cycle_time)
			IF @end_time > @target_cycle_end
				BEGIN
					SET @this_downtime = DATEDIFF(MILLISECOND, @target_cycle_end, @end_time) / 1000.0
					--PRINT 'DOWNTIME FROM TARGET OF LAST CYCLE TO END TIME: ' + CAST(@this_downtime as varchar(50))
					SET @total_downtime = @total_downtime + @this_downtime
				END
		END

	RETURN @total_downtime / 60.0
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
