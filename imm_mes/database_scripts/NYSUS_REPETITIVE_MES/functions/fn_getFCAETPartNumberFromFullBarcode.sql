SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		James Knoblauch
-- Create date: 2015-12-29
-- Description:	Extract the FCA-E&T part number from the full barcode.
-- =============================================
CREATE FUNCTION fn_getFCAETPartNumberFromFullBarcode 
(
	-- Add the parameters for the function here
	@full_barcode varchar(50)
)
RETURNS varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res varchar(50)
	SET @res = ''

	IF LEN(@full_barcode) > 20
		SET @res = SUBSTRING(@full_barcode, 11, 11)

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
