SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




-- =============================================
-- Author:		Steve Sass
-- Create date: 10/28/2015
-- Description:	
-- 1-21-16 changed from 22 to 19 and length 10 to 13 per BOB
-- =============================================
CREATE FUNCTION [dbo].[fn_getSerialNumberFromFullBarcode] 
(
	-- Add the parameters for the function here
	@full_barcode varchar(50)
)
RETURNS bigint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res bigint
	SET @res = 0

	IF LEN(@full_barcode) > 20
	-- changed from 22 to 19 and length 10 to 13 per BOB
		IF ISNUMERIC(SUBSTRING(@full_barcode, 19, 13)) = 1
			SET @res = CAST(SUBSTRING(@full_barcode, 19, 13) as bigint)

	-- Return the result of the function
	RETURN @res

END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
