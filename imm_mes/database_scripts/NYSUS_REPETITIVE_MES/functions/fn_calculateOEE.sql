SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		R.Kahle
-- Create date: 5/9/2017
-- Description:	This will calculate the OEE from OEE factors
-- =============================================
CREATE FUNCTION fn_calculateOEE 
(
	-- Add the parameters for the function here
	@perf float,
	@qual float,
	@avail float
)
RETURNS float
AS
BEGIN
	-- Declare the return variable here
	DECLARE @oee float

	-- Add the T-SQL statements to compute the return value here
	SELECT @oee = @perf * @qual * @avail

	-- Return the result of the function
	RETURN @oee

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
