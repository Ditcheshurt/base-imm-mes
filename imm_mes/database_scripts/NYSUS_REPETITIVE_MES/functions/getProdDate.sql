SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/21/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[getProdDate] 
(
	-- Add the parameters for the function here
	@the_time datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res datetime
	DECLARE @time_as_float float
	DECLARE @date_no_time datetime

	SET @date_no_time = dbo.stripTime(@the_time)
	SET @time_as_float = DATEPART(HOUR, @the_time) + (CAST(DATEPART(MINUTE, @the_time) as float) / 60.0)

	IF @time_as_float >= 23.5 OR @time_as_float < 6.5
		SET @res = DATEADD(DAY, -1, @date_no_time)
	ELSE
		SET @res = @date_no_time

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
