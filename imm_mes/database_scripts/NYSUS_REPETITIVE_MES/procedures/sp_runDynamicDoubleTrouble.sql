SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/8/2018
-- Description:	This will run double dynamic sql...use wisely!!!
-- =============================================
CREATE PROCEDURE [dbo].[sp_runDynamicDoubleTrouble] 
	-- Add the parameters for the stored procedure here
	@cmd varchar(MAX) = '', 
	@vars varchar(MAX) = '',
	@assignments varchar(1000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @sql nvarchar(MAX)
	DECLARE @result int

	-- Insert statements for procedure here
	BEGIN TRY
		SET @sql = REPLACE('sp_executesql @command, @variables, <<assignments>>', '<<assignments>>', @assignments)
		EXECUTE sp_executesql @sql, N'@command nvarchar(MAX),@variables nvarchar(MAX)', @command=@cmd, @variables=@vars
		SET @result = 0
	END TRY
	BEGIN CATCH
		SET @result = -1
	END CATCH

	RETURN(@result)
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
