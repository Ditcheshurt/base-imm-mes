SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Rob Kahle
-- Create date: 8/21/2017
-- Description:	This will handle bulk label reprings
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_bulkPrint] 
	-- Add the parameters for the stored procedure here
	@machine_ID int,
	@tool_ID int,
	@part_ID int,
	@quantity int,
	@operator_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @tool_part_ID int
	DECLARE @num_parts int
	DECLARE @mcp_ID int
	DECLARE @target_cycle_time int
	DECLARE @cycle_sequence int
	DECLARE @cycle_ID int
	DECLARE @serial_number varchar(20)
	DECLARE @barcode varchar(20)

	SET @num_parts = @quantity

	SELECT @tool_part_ID = tp.ID, @target_cycle_time = mt.target_cycle_time
	FROM tool_parts tp
	JOIN machine_tools mt
		ON mt.tool_ID = tp.tool_ID
	WHERE tp.part_ID = @part_ID AND tp.tool_ID = @tool_ID AND mt.machine_ID = @machine_ID

	SELECT TOP 1 @cycle_sequence = ISNULL(cycle_sequence, 0)
	FROM machine_cycles
	WHERE machine_ID = @machine_ID
	ORDER BY ID DESC

	INSERT INTO machine_cycles (machine_ID, tool_ID, cycle_time, cycle_duration, cycle_sequence, operator_ID ) VALUES (
		@machine_ID,
		@tool_ID,
		GETDATE(),
		@target_cycle_time,
		ISNULL(@cycle_sequence, 0) + 1,
		@operator_ID
	)
	SET @cycle_ID = @@IDENTITY
	--SELECT TOP 1 @cycle_ID = ID+1 FROM machine_cycles ORDER BY ID DESC
	--SELECT 'new_machine_cycle' = @cycle_ID
	--	,'machine_ID' = @machine_ID
	--	,'tool_ID' = @tool_ID
	--	,'cycle_time' = GETDATE()
	--	,'cycle_duration' = @target_cycle_time
	--	,'cycle_sequence' = ISNULL(@cycle_sequence, 0) + 1
	--	,'operator_ID' = @operator_ID

	WHILE @num_parts > 0
	BEGIN
		WAITFOR DELAY '00:00:00.080' --ensures serial numbers are unique for multiple part inserts

		SELECT @serial_number = dbo.fn_getNextSerial(@tool_part_ID)
		SET @barcode = dbo.fn_createProperSerial(@machine_ID-1000,'','')

		INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, insert_time, serial_number, full_barcode, disposition, disposition_time) VALUES (
			@cycle_ID,
			@tool_part_ID,
			GETDATE(),
			@serial_number,
			@barcode,
			'BULK_PRINT',
			GETDATE()
		)
		SET @mcp_ID = @@IDENTITY
		--SELECT TOP 1 @mcp_ID = ID+1 FROM machine_cycle_parts ORDER BY ID DESC
		--SELECT 'new_mcp_ID' = @mcp_ID
		--	,'cycle_ID' = @cycle_ID
		--	,'tool_part_ID' = @tool_part_ID
		--	,'insert_time' = GETDATE()
		--	,'serial_number' = @serial_number
		--	,'full_barcode' = @barcode

		DECLARE @ret_code int = 0
		EXEC @ret_code = sp_MACHINE_MES_printPartLabel
			@mcp_ID = @mcp_ID

		SET @num_parts = @num_parts - 1
	END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
