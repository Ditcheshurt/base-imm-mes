SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		James Knoblauch
-- Create date: 2016-02-18
-- Description:	Log all operators out of the machine
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_machineOperatorLogout] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- log any operators out of the machine
	UPDATE oeh
	SET
		oeh.end_time = GETDATE()
	FROM
		machine_operator_event_history moeh
		LEFT JOIN MES_COMMON.dbo.operator_event_history oeh ON moeh.operator_event_history_ID = oeh.ID
	WHERE
		oeh.operator_event_ID = 1
		AND moeh.machine_ID = @machine_ID
		AND oeh.end_time IS NULL

	-- dissociate the current operator from the machine
	UPDATE
		machines
	SET
		current_operator_ID = NULL
	WHERE
		ID = @machine_ID
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
