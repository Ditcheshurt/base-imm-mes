SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 1/1/2016
-- Description:	Import csv autogauge results
-- =============================================
CREATE PROCEDURE [dbo].[sp_CUSTOM_RECORD_AUTOGAUGE] 
	@tag_ID varchar(10),
	@tag_value varchar(30),
	@tag_desc varchar(50),
	@machine_ID int,
	@path_name NVARCHAR(2000) = N'',
	@serial_number NVARCHAR(50),
	@cycle_ID int
AS
	--DECLARE @cycle_ID int
	DECLARE @file_name varchar(200)
	DECLARE @CMD VARCHAR(512)	
	
	SET NOCOUNT ON;

	EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc, @tag_value, 'RecordAutoGauge'

	IF OBJECT_ID('tempdb..#results') IS NOT NULL DROP TABLE #results
   	CREATE TABLE #results
		([part_name] varchar(200)
			,[m1] float
			,[m2] float
			,[m3] float
			,[m4] float
			,[taper] float
			,[sp1_low] float
			,[sp1_high] float
			,[sp2_low] float
			,[sp2_high] float
			,[sp3_low] float
			,[sp3_high] float
			,[sp4_low] float
			,[sp4_high] float
			,[spt_low] float
			,[spt_high] float
			,[good_part] varchar(20)
			,[recorded_date] varchar(100))
	
	-- get the files in the folder
	IF OBJECT_ID('tempdb..#CommandShell') IS NOT NULL DROP TABLE #CommandShell
	CREATE TABLE #CommandShell ( Line VARCHAR(512)) 
 
	SET @CMD = 'DIR ' + @path_name + ' /TC /O-D /A-D' 
 
	PRINT @CMD -- test & debug

	INSERT INTO #CommandShell EXEC MASTER..xp_cmdshell @CMD 

	DELETE 
	FROM 
		#CommandShell 
	WHERE 
		Line NOT LIKE '[0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9] %' 
		OR Line LIKE '%<DIR>%'
		OR Line is null

	SELECT TOP 1
		@file_name = REVERSE( LEFT(REVERSE(Line),CHARINDEX(' ',REVERSE(line))-1 ) )
		--,CONVERT(DATETIME, LEFT(Line,20)) AS create_date
	FROM 
		#CommandShell
	ORDER BY 
		CONVERT(DATETIME, LEFT(Line,20)) DESC

	DROP TABLE #CommandShell
	-- end get files in folder	

	-- get current machine_cycles.cycle_ID for autogauge machine
	--SELECT TOP 1 
	--	@cycle_ID = ID 
	--FROM 
	--	dbo.machine_cycles 
	--WHERE 
	--	machine_ID = @machine_ID 
	--ORDER BY cycle_time DESC
	IF @machine_ID = 10045
	BEGIN
		ALTER TABLE #results
		DROP COLUMN [m4], [sp4_low], [sp4_high]
	END
	-- bulk insert cannot accept a @filePath so need to execute dynamic tsql
	DECLARE @bulkinsert NVARCHAR(2000)
	SET @bulkinsert = 
		   N'BULK INSERT #results FROM ''' + 
		   @path_name + @file_name + 
		   N''' WITH (FIRSTROW = 1, FIELDTERMINATOR = '','', ROWTERMINATOR = ''\n'')'

	EXEC sp_executesql @bulkinsert
	
	IF @machine_ID = 20045
		BEGIN
			INSERT INTO [dbo].[gauge_autogauge]
				   ([cycle_ID]
				   ,[serial_number]
				   ,[part_name]
				   ,[m1]
				   ,[m2]
				   ,[m3]
				   ,[m4]
				   ,[taper]
				   ,[sp1_low]
				   ,[sp1_high]
				   ,[sp2_low]
				   ,[sp2_high]
				   ,[sp3_low]
				   ,[sp3_high]
				   ,[sp4_low]
				   ,[sp4_high]
				   ,[spt_low]
				   ,[spt_high]
				   ,[good_part]
				   ,[recorded_date]
				   ,[file_name])
			SELECT @cycle_ID
					,@serial_number
					,[part_name]
				   ,[m1]
				   ,[m2]
				   ,[m3]
				   ,[m4]
				   ,[taper]
				   ,[sp1_low]
				   ,[sp1_high]
				   ,[sp2_low]
				   ,[sp2_high]
				   ,[sp3_low]
				   ,[sp3_high]
				   ,[sp4_low]
				   ,[sp4_high]
				   ,[spt_low]
				   ,[spt_high]
				   ,[good_part]
				   ,CONVERT(datetime, [recorded_date])
				   ,@file_name
			FROM #results	
		END
	ELSE IF @machine_ID = 10045 
		BEGIN
			INSERT INTO [dbo].[gauge_autogauge]
				   ([cycle_ID]
				   ,[serial_number]
				   ,[part_name]
				   ,[m1]
				   ,[m2]
				   ,[m3]				   
				   ,[taper]
				   ,[sp1_low]
				   ,[sp1_high]
				   ,[sp2_low]
				   ,[sp2_high]
				   ,[sp3_low]
				   ,[sp3_high]
				   ,[spt_low]
				   ,[spt_high]
				   ,[good_part]
				   ,[recorded_date]
				   ,[file_name])
			SELECT @cycle_ID
					,@serial_number
					,[part_name]
				   ,[m1]
				   ,[m2]
				   ,[m3]				   
				   ,[taper]
				   ,[sp1_low]
				   ,[sp1_high]
				   ,[sp2_low]
				   ,[sp2_high]
				   ,[sp3_low]
				   ,[sp3_high]			   
				   ,[spt_low]
				   ,[spt_high]
				   ,[good_part]
				   ,CONVERT(datetime, [recorded_date])
				   ,@file_name
			FROM #results
		END
	-- move to processed folder
	--SET @CMD = 'move /Y ' + @path_name + @file_name + ' ' +  @path_name + '\processed\' + @file_name
	SET @CMD = 'move /Y ' + @path_name + '*.* ' + @path_name + '\processed\'
	EXEC MASTER..xp_cmdshell @CMD
	
	-- delete all files in folder
	SET @CMD = 'del /Q ' + @path_name + '* ' + @path_name
	EXEC MASTER..xp_cmdshell @CMD 



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
