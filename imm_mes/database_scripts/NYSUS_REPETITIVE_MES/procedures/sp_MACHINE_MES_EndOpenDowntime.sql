SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 6/10/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_EndOpenDowntime] 
	@system_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @date_stripped datetime
	DECLARE @s datetime --START OF SHIFT
	DECLARE @e datetime --END OF SHIFT
	DECLARE @this_shift int, @the_time datetime
	DECLARE @current_operator_ID int
	DECLARE @shift_number int
	DECLARE @shift_start time(7);

	SET @the_time = DATEADD(MINUTE, -10, GETDATE())
	SET @date_stripped = dbo.stripTime(@the_time)
	--SET @this_shift = dbo.getShift(@system_ID, @the_time)	
	SET @current_operator_ID = 0

	-- get shift
	SELECT 
		@this_shift = s.ID,
		@shift_number = s.shift_number
	FROM
		MES_COMMON.dbo.shifts s
		JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN MES_COMMON.dbo.time_intervals t ON sti.time_interval_ID = t.ID
	WHERE
		s.system_ID = @system_ID
		AND
			CONVERT(time, @the_time) BETWEEN t.interval_start_time AND t.interval_end_time;		
  
	-- get the shift start time
	SELECT 		
		@shift_start = ti.interval_start_time
	FROM 
		MES_COMMON.dbo.shifts s
	JOIN 
		MES_COMMON.dbo.shift_time_intervals sti ON s.ID = sti.shift_ID
	JOIN 
		MES_COMMON.dbo.time_intervals ti ON ti.ID = sti.time_interval_ID
	WHERE
		s.ID = @this_shift AND sti.shift_start = 1 AND system_ID = @system_ID;	

	--PRINT 'SHIFT: ' + CAST(@this_shift as varchar(50))
	IF @shift_number = 3 
		IF DATEPART(HOUR, @the_time) < 8
			SELECT @s = @date_stripped + ' ' + CAST(@shift_start AS varchar(10))
			--SET @s = DATEADD(MINUTE, @shift_start * 60.0, @date_stripped - 1)
		ELSE
			BEGIN
				SELECT @s = @date_stripped + ' ' + CAST(@shift_start AS varchar(10))
				--SET @s = DATEADD(MINUTE, @shift_start * 60.0, @date_stripped)
				SET @date_stripped = @date_stripped + 1
			END
	ELSE
		SELECT @s = @date_stripped + ' ' + CAST(@shift_start AS varchar(10))
		--SET @s = DATEADD(MINUTE, @shift_start * 60.0, @date_stripped)

    UPDATE machine_downtime_log
	SET reason_ID = 0
	WHERE reason_ID IS NULL
		AND time_in_state IS NOT NULL

	--FOR EACH MACHINE, SEE IF IT'S DOWN
	DECLARE @machine_ID int, @tool_ID int, @last_cycle_time datetime, @target_cycle_time int
	DECLARE @expected_next_cycle_time datetime
	DECLARE @cycle_duration int, @cycle_overage int

	DECLARE loopyEOSHIFT CURSOR FOR 
		SELECT m.ID, m.current_tool_ID, m.last_cycle_time, mt.target_cycle_time, m.current_operator_ID
		FROM machines m
			JOIN machine_tools mt ON m.ID = mt.machine_ID AND m.current_tool_ID = mt.tool_ID
		ORDER BY m.ID;

	OPEN loopyEOSHIFT;
	FETCH NEXT FROM loopyEOSHIFT INTO @machine_ID, @tool_ID, @last_cycle_time, @target_cycle_time, @current_operator_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @expected_next_cycle_time = DATEADD(SECOND, @target_cycle_time, @last_cycle_time)

			-- 9.2.2015 - SS - ADDED CHECK TO MAKE SURE THE LAST CYCLE TIME WAS GREATER THAN THE START OF THE LAST SHIFT....SO WE DON'T KEEP ADDING DT
			IF @expected_next_cycle_time < GETDATE() AND @last_cycle_time > @s
				BEGIN
					SET @cycle_duration = DATEDIFF(SECOND, @last_cycle_time, GETDATE())
					SET @cycle_overage = @cycle_duration - (@target_cycle_time)

					--CREATE DOWNTIME RECORD 
					INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID, time_in_state, reason_ID, comment, recorded_by) VALUES (
						DATEADD(SECOND, 0 - @cycle_overage, GETDATE()),
						0,
						@machine_ID,
						@tool_ID,
						CAST(@cycle_overage as float)/60.0,
						0,
						'END_OF_SHIFT',
						@current_operator_ID)
				END

			FETCH NEXT FROM loopyEOSHIFT INTO @machine_ID, @tool_ID, @last_cycle_time, @target_cycle_time, @current_operator_ID
		END

	CLOSE loopyEOSHIFT
	DEALLOCATE loopyEOSHIFT
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
