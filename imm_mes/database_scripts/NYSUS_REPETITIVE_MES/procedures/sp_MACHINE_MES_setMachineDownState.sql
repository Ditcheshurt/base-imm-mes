SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/28/2017
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_setMachineDownState] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tool_ID int = 0,
	@state bit = 0,
	@operator_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @last_cycle_time datetime, @target_cycle_time int

	INSERT INTO NYSUS_REPETITIVE_MES.dbo.loggy (log_time, log_msg) VALUES (
		GETDATE(), 
		'Machine '+CAST(@machine_ID as varchar(50))+' with tool '+CAST(@tool_ID as varchar(50))+' reported over cycle time'
	)

    IF NOT EXISTS(SELECT * FROM machine_downtime_log WHERE machine_ID = @machine_ID AND up = @state AND time_in_state IS NULL) 
		BEGIN
			INSERT INTO NYSUS_REPETITIVE_MES.dbo.loggy (log_time, log_msg) VALUES (
				GETDATE(), 
				'No open downtime record found for machine '+CAST(@machine_ID as varchar(50))
			)

			SELECT @last_cycle_time = last_cycle_time
			FROM machines 
			WHERE ID = @machine_ID

			SELECT @target_cycle_time = target_cycle_time
			FROM machine_tools
			WHERE machine_ID = @machine_ID
				AND tool_ID = @tool_ID

			IF DATEADD(SECOND, @target_cycle_time, @last_cycle_time) < GETDATE()
				BEGIN
					INSERT INTO NYSUS_REPETITIVE_MES.dbo.loggy (log_time, log_msg) VALUES (GETDATE(), 'STARTED DOWNTIME FOR Machine '+CAST(@machine_ID as varchar(50)))
					INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID) VALUES (
						DATEADD(SECOND, @target_cycle_time, @last_cycle_time),
						@state,
						@machine_ID, 
						@tool_ID
					)

					UPDATE machines
					SET up = 0
					WHERE ID = @machine_ID
				END
		END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
