SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/13/2018
-- Description:	This will handle creating or updating alert monitor settings from the manager
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_processMachineMonitorAlertConfig] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0,
	@tool_ID int = 0,
	@dps_ID int = NULL,
	@mtcl_ID int = NULL,
	@upper_cl float = NULL,
	@lower_cl float = NULL,
	@severity int = NULL,
	@active bit = 0,
	@target_val float = NULL,
	@operator_ID int = 0
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @cls_ID int = 0
	DECLARE @t1 TABLE (ID int)

	BEGIN TRY
		BEGIN TRANSACTION
			IF @mtcl_ID IS NOT NULL
				BEGIN
					UPDATE machine_tool_ctrl_limits
					SET upper_ctrl_limit = @upper_cl, lower_ctrl_limit = @lower_cl, target_value = @target_val
					OUTPUT inserted.ID INTO @t1
					WHERE ID = @mtcl_ID
				END
			ELSE
				BEGIN
					-- Do inserts
					INSERT INTO machine_tool_ctrl_limits (machine_ID, tool_ID, data_point_ID, target_value, upper_ctrl_limit, lower_ctrl_limit)
					OUTPUT inserted.ID INTO @t1
					VALUES (@machine_ID, @tool_ID, @dps_ID, @target_val, @upper_cl, @lower_cl)
				END

			IF EXISTS (SELECT cls.ID FROM machine_monitor_control_limit_severity cls WHERE cls.machine_tool_ctrl_limit_ID IN (SELECT ID FROM @t1))
				BEGIN
					UPDATE cls
					SET machine_monitor_severity_ID = @severity, active = @active, last_modified_by = @operator_ID, last_modified_timestamp = GETDATE()
					OUTPUT inserted.ID
					FROM machine_monitor_control_limit_severity cls WHERE cls.machine_tool_ctrl_limit_ID IN (SELECT ID FROM @t1)
				END
			ELSE
				BEGIN
					INSERT INTO machine_monitor_control_limit_severity (machine_tool_ctrl_limit_ID, machine_monitor_severity_ID, active)
					OUTPUT inserted.ID
					SELECT t.ID, @severity, @active
					FROM @t1 t
				END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorSeverity INT,
				@ErrorNumber   INT,
				@ErrorMessage nvarchar(4000),
				@ErrorState INT,
				@ErrorLine  INT,
				@ErrorProc nvarchar(200)

		-- Grab error information from SQL functions
		SET @ErrorSeverity = ERROR_SEVERITY()
		SET @ErrorNumber   = ERROR_NUMBER()
		SET @ErrorMessage  = ERROR_MESSAGE()
		SET @ErrorState    = ERROR_STATE()
		SET @ErrorLine     = ERROR_LINE()
		SET @ErrorProc     = ERROR_PROCEDURE()
		SET @ErrorMessage  = 'Problem updating information.' + CHAR(13) + 'SQL Server Error Message is: ' + CAST(@ErrorNumber AS VARCHAR(10)) + ' in procedure: ' + @ErrorProc + ' Line: ' + CAST(@ErrorLine AS VARCHAR(10)) + ' Error text: ' + @ErrorMessage
	
		-- Not all errors generate an error state, to set to 1 if it's zero
		IF @ErrorState  = 0
		SET @ErrorState = 1
	
		-- If the error renders the transaction as uncommittable or we have open transactions, we may want to rollback
		IF @@TRANCOUNT > 0
		BEGIN
			--print 'Rollback transaction'
			ROLLBACK TRANSACTION
		END
		RAISERROR (@ErrorMessage , @ErrorSeverity, @ErrorState, @ErrorNumber)
	END CATCH
	RETURN @@ERROR
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
