SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetParts] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT t.ID as tool_ID, t.tool_description, tp.ID as part_ID, p.part_number, p.part_desc, p.part_image, tp.serialized, tp.parts_per_tool, p.part_group_id, pg.part_group_description, tp.part_ID as real_part_id
	FROM tools t
		JOIN tool_parts tp ON t.ID = tp.tool_ID
		JOIN machine_tools mt ON t.ID = mt.tool_ID 
		JOIN parts p ON p.ID = tp.part_ID
		JOIN part_group_hrt pg ON p.part_group_id = pg.ID
	WHERE mt.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP) 
		AND (@machine_ID = 0 OR mt.machine_ID = @machine_ID)	
		AND tp.active = 1
	ORDER BY t.ID ASC, part_group_id ASC, p.ID ASC

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
