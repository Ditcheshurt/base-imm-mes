SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 6/17/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleForSecondaryPart] 
	-- Add the parameters for the stored procedure here
	@tag_ID int = 0, 
	@value varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @machine_number varchar(50)
	DECLARE @machine_ID int
	DECLARE @tool_ID int
	DECLARE @last_cycle_time datetime
	DECLARE @cycle_sequence int, @cycle_duration int
	DECLARE @cycle_ID int
	DECLARE @target_cycle_time int, @cycle_overage int

	SELECT @machine_number = description
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE ID = @tag_ID

	SELECT @machine_ID = m.ID, @tool_ID = ISNULL(m.current_tool_ID, 0)
	FROM machines m
		LEFT OUTER JOIN tools t ON m.current_tool_ID = t.ID
	WHERE m.machine_number = @machine_number

	IF @value = 'True'
		BEGIN
			--GET THE LAST CYCLE ID
			SELECT TOP 1 @cycle_ID = ID
			FROM machine_cycles
			WHERE machine_ID = @machine_ID 
			ORDER BY ID DESC
			
			--INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, insert_time)
			--SELECT @cycle_ID, ID, GETDATE()
			--FROM tool_parts 
			--WHERE tool_ID = @tool_ID
			--	AND sub_tool_order <> 10  
			--	AND active = 1

			;WITH tools_CTE
			AS (
				SELECT TOP (SELECT MAX(parts_per_tool) FROM tool_parts) rn = ROW_NUMBER() 
				OVER (ORDER BY [object_id]) 
				  FROM sys.all_columns 
				  ORDER BY [object_id]
			 )

			INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, insert_time)
			SELECT @cycle_ID, tp.ID, GETDATE()
			FROM tools_CTE
				CROSS JOIN tool_parts AS tp
			WHERE tools_CTE.rn <= tp.parts_per_tool 
				AND tp.tool_ID = @tool_ID
				AND tp.sub_tool_order <> 10
				AND tp.active = 1
		END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
