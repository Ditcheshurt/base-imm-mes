SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 10/13/2018
-- Description:	This will make a request to the ash tester data grabber PHP page and save the results to the database
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CUSTOM_getAshTesterData] 
	-- Add the parameters for the stored procedure here
	@output_select_nPrint bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SET NOCOUNT ON
	DECLARE @select_nPrint bit = 0

	DECLARE @web_server varchar(50) = '10.68.160.79'

	DECLARE @temp TABLE (element_id int, parent_ID int, [Object_ID] int, [NAME] varchar(50), StringValue varchar(1000), ValueType varchar(20))

	DECLARE @test TABLE (parent_ID int, test_timestamp datetime, test_result bit, unique_test_ID varbinary(64))
	DECLARE @test_res TABLE (parent_ID int, mcs_ID int, [name] varchar(50), [value] varchar(50), [range] varchar(50), units varchar(50))

	DECLARE @success bit = 0, @err_msg varchar(1000) = '--- UNDEFINED ERROR MESSAGE ---'
	DECLARE @result varchar(MAX)
	DECLARE @uri varchar(2000)

	SET @result = 'ERROR'
	SET @uri = 'http://' + @web_server + '/mesaas_manager/ajax_processors/ash_tester/getRecentTests.php'

	SET @result = MES_COMMON.dbo.fn_CLR_GET (
		@uri
		,''
		,'')

	SET @select_nPrint = @output_select_nPrint

	INSERT INTO @temp SELECT * FROM MES_COMMON.dbo.parseJSON(@result) WHERE Object_ID IS NULL
	IF NOT EXISTS(SELECT StringValue FROM @temp WHERE NAME LIKE 'success') BEGIN
		SET @success = 1
	END ELSE BEGIN
		SET @success = 0
		SELECT @err_msg = StringValue FROM @temp WHERE [NAME] LIKE 'err_msg'
	END

	INSERT INTO @test_res
		SELECT
			 ash.parent_ID
			,mcs.ID AS mcs_ID
			,ash.[name]
			--,TRY_CAST(ash.StringValue AS float)
			,TRY_CAST(
				CASE WHEN ISNULL(mcs.range_separator,'') = ''
				THEN REPLACE(ash.StringValue, mcs.units, '')
				ELSE REPLACE(LEFT(ash.StringValue, CHARINDEX(mcs.range_separator, ash.StringValue,0)-(LEN(mcs.range_separator)-1)), mcs.units, '')
				END
			AS varchar) AS [value]
			,(
				CASE WHEN ISNULL(mcs.range_separator,'') = ''
				THEN ''
				ELSE REPLACE(RIGHT(ash.StringValue, CHARINDEX(mcs.range_separator, ash.StringValue,0)-(LEN(mcs.range_separator)-1)), mcs.units, '')
				END
			) AS [range]
			,mcs.units
		FROM @temp ash
		JOIN CUSTOM_material_certification_setup mcs ON mcs.data_point_name = ash.[NAME] AND mcs.active = 1
		ORDER BY ash.parent_ID, mcs.ID

	IF @success = 1 BEGIN
		INSERT INTO @test
			SELECT
				p1.parent_ID,
				--MAX(p1.sample_name) AS sample_name,
				--MAX(p1.lot_number) AS lot_number,
				--MAX(p1.product_id) AS product_id,
				--MAX(p1.foil_id) AS foil_id,
				--MAX(p1.foil_weight) AS foil_weight,
				CONVERT(datetime,MAX(p1.[date])+' '+MAX(p1.time_of_day)+':00',104) AS test_timestamp,
				--MAX(p1.final_result) AS final_result,
				CASE MAX(p1.pass_per_fail) WHEN 'pass' THEN 1 WHEN 'fail' THEN 0 END AS test_result,
				--MAX(p1.purity) AS purity,
				--MAX(p1.last_actual) AS last_actual,
				--MAX(p1.start_weight) AS start_weight,
				--MAX(p1.end_weight) AS end_weight,
				--MAX(p1.start_wt_target) AS start_wt_target,
				--DATEDIFF(second, '00:00:00','00:'+MAX(p1.test_duration)) AS test_duration,
				--MAX(p1.start_temp) AS start_temp,
				--MAX(p1.test_temp) AS test_temp,
				--MAX(p1.ashing_rate) AS ashing_rate,
				HASHBYTES('SHA2_256',CONCAT(
					MAX(p1.[date]), ' ', MAX(p1.time_of_day), ':00',
					MAX(p1.final_result), MAX(p1.start_weight), MAX(p1.end_weight),
					MAX(p1.test_duration), MAX(p1.pass_per_fail)
				)) AS unique_test_ID
			FROM (
				SELECT * FROM @temp ash
			) t1
			PIVOT (
				MAX(t1.StringValue)
				FOR t1.NAME IN (
					[sample_name],[lot_number],[product_id],[date],[time_of_day],[final_result],[result_type],[pass_per_fail],[purity],[test_mode],[last_actual],[start_weight],[start_wt_target],[end_weight],[test_duration],[start_temp],[test_temp],[hi_start_temp],[temp_rate],[pan_tare],[sample_tare],[end_rate],[rate_target],[end_reliability],[reliability_target],[correlation_constant],[ashing_rate],[linked_to_next_test],[link-result_type],[link-use_for_total],[foil_id],[foil_weight]
					--[date],[time_of_day],[final_result],[pass_per_fail],[start_weight],[end_weight],[test_duration]
				)
			) AS p1
			GROUP BY p1.parent_ID

		DECLARE @mct_rec TABLE (ID int, unique_test_ID varbinary(64))
		DECLARE @mcr_rec TABLE (ID int)

		INSERT INTO @mct_rec
		SELECT t1.ID, t1.unique_test_ID FROM (
			INSERT INTO CUSTOM_material_certification_test
			OUTPUT inserted.ID, inserted.unique_test_ID
				SELECT NULL AS lot_number, t.test_timestamp AS [timestamp], t.test_result, t.unique_test_ID
				FROM @test t
				WHERE t.unique_test_ID NOT IN (
					SELECT mct.unique_test_ID
					FROM CUSTOM_material_certification_test mct
					JOIN @test t ON t.unique_test_ID = mct.unique_test_ID
				)
		) t1
		
		INSERT INTO @mcr_rec
		SELECT t2.ID FROM (
			INSERT INTO CUSTOM_material_certification_results
			OUTPUT inserted.ID
			SELECT
				 mr.ID AS matcert_test_ID
				,tr.mcs_ID AS matcert_datapoint_ID
				,tr.[value] AS matcert_datapoint_value
				,tr.[range] AS matcert_datapoint_range
			FROM @test_res tr
			JOIN @test t ON t.parent_ID = tr.parent_ID
			JOIN @mct_rec mr ON mr.unique_test_ID = t.unique_test_ID
		) t2

		DECLARE @mct_cnt int, @mcr_cnt int
		SELECT @mct_cnt = COUNT(ID) FROM @mct_rec
		SELECT @mcr_cnt = COUNT(ID) FROM @mcr_rec

		IF (@select_nPrint = 0) BEGIN
			PRINT CONCAT('MCT Records Added: ',CAST(@mct_cnt AS varchar))
			PRINT CONCAT('MCR Records Added: ',CAST(@mcr_cnt AS varchar))
		END ELSE BEGIN
			SELECT 'MCT' AS [table], @mct_cnt AS num_recs
			UNION ALL
			SELECT 'MCR' AS [table], @mcr_cnt AS num_recs
		END
	END ELSE BEGIN
		DECLARE @err_res varchar(2000) = CONCAT('HTML Request to ', @web_server, ' failed with error msg: ', @err_msg)
		IF (@select_nPrint = 0)
			PRINT @err_res
		ELSE
			SELECT @err_res AS result
	END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
