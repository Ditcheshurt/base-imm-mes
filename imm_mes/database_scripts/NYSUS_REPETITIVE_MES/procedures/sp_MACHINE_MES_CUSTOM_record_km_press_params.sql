SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 11/14/2017
-- Description:	This will be called by the FTP directory watcher shell script when a press updates the DATACOL.DAT file with parameter data
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CUSTOM_record_km_press_params]
	
	-- Parameters passed in
	--@press_num int = 1
	--@press_data varchar(500) = 'DATE=20171110|TIME=18:35:13|COUNT=307|ActTimCyc=65.61|ActTimFill[1]=3.36|ActTimPlst[1]=24.70|ActTmpBrlZn[1,5]=465|ActStrCsh[1]=27.6|ActTmpOil=116|SetDescMld=""|SetDescPrt=""|SetDescJob=""|@010CycDataMld\CycClpClsTim\CycVal.PdeValue=3.74|@010CycDataMld\CycClpOpnTim\CycVal.PdeValue=3.83|@010CycDataMld\CycCoolTim\CycVal.PdeValue=36.30|ActPrsXfrSpec[1]=1963.2|ActPrsHldSpecMax[1]=2254.6|ActStrPlst[1]=293.7'
	@press_num int = 0, 
	@press_data varchar(5000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Things we'll need or will create
	DECLARE @cycle_ID int = -1
	DECLARE @tool_ID int = 1

	-- EXPERIMENTAL - variables needed
	DECLARE @machine_number varchar(50), @is_primary bit
	DECLARE @machine_ID int
	DECLARE @operator_ID int
	--DECLARE @tool_ID int
	DECLARE @last_cycle_time datetime
	--DECLARE @cycle_sequence int, @cycle_duration int
	--DECLARE @cycle_ID int
	DECLARE @target_cycle_time int, @cycle_overage int
	DECLARE @parts_per_tool int, @i int
	DECLARE @last_shift_start datetime
	DECLARE @over_cycle_grace_period float  --IN SECONDS
	DECLARE @system_ID int
	DECLARE @auto_label bit = 0
	DECLARE @tool_part_ID int
	DECLARE @serial_number varchar(20)
	DECLARE @barcode varchar(20)
	DECLARE @part_number varchar(50)


	-- Variables we'll use later
	DECLARE @cycle_sequence int = 0
	DECLARE @cycle_duration float = 0.0

	DECLARE @tmp TABLE ([parameter] varchar(100), [value] varchar(100))
	DECLARE @tmp_data TABLE ([cycle_ID] int, [data_point_ID] int, [data_point_value] float)
	DECLARE @mcID TABLE (ID int)

	--INSERT INTO dbo.loggy (log_time,log_msg) VALUES (GETDATE(),CONCAT('Press ',LEFT(CAST(@press_num AS varchar),2),': ',LEFT(@press_data,1990)))

	SET @press_data = REPLACE(@press_data,'\Cp1','')
	SET @press_data = REPLACE(@press_data,'\CycVal.PdeValue','')
	SET @press_data = REPLACE(@press_data,'@010Z','@01Z')
	SET @press_data = REPLACE(@press_data,'MldHtCl1T1.','MldHtCl1T1\')
	SET @press_data = REPLACE(@press_data,'MLD.','MLD\')
	SET @press_data = REPLACE(@press_data,'Inj1.','Inj1\')
	SET @press_data = REPLACE(@press_data,'Inj1T1\','Inj1T1_')
	SET @press_data = REPLACE(@press_data,'\TmpSet','.TempSet')

	INSERT INTO @tmp
	SELECT
		(SELECT 
			(CASE
				WHEN CHARINDEX('@01Z',items) > 0 THEN (SELECT REPLACE(items,'@01Z','') FROM dbo.Split(items,'\') WHERE ID=1)
				WHEN CHARINDEX('@010',items) > 0 THEN (SELECT items FROM dbo.Split(items,'\') WHERE ID=2)
				WHEN CHARINDEX('@020',items) > 0 THEN (SELECT SUBSTRING(items,5,LEN(items)-4) FROM dbo.Split(items,'\') WHERE ID=1)
				WHEN CHARINDEX('@021',items) > 0 THEN (SELECT items FROM dbo.Split(items,'\') WHERE ID=2)
				WHEN CHARINDEX('@022',items) > 0 THEN (SELECT SUBSTRING(items,5,LEN(items)-4) FROM dbo.Split(items,'\') WHERE ID=1)
				ELSE items
			END) FROM dbo.Split(pd1.items,'=') WHERE ID=1) k,
		(SELECT REPLACE(items,'''','') FROM dbo.Split(pd1.items,'=') WHERE ID=2) v
	FROM
		dbo.Split(@press_data,'|') AS pd1

	SELECT @cycle_sequence = t.[value] FROM @tmp t WHERE t.[parameter] = 'COUNT'
	SELECT @cycle_duration = t.[value] FROM @tmp t WHERE t.[parameter] = 'ActTimCyc'

	-- Comment the following to include cycle count and cycle time in machine_cycle_point_data table
	--DELETE FROM @tmp WHERE [parameter] = 'COUNT' OR [parameter] = 'ActTimCyc'
	DELETE FROM @tmp WHERE [parameter] = 'COUNT'

	IF EXISTS (SELECT TOP 1 ID FROM machine_cycles WHERE machine_ID = @press_num AND cycle_sequence = 0 AND cycle_duration IS NULL)
		BEGIN
			-- We already have a machine cycle to link the press params to
			UPDATE machine_cycles
			SET cycle_duration = @cycle_duration, cycle_sequence = @cycle_sequence
			OUTPUT inserted.ID INTO @mcID
			WHERE machine_ID = @press_num AND cycle_sequence = 0 AND cycle_duration IS NULL
			SELECT @cycle_ID = ID FROM @mcID
		END
	ELSE
		-- No existing machine cycle so let's create one...
		BEGIN
			SET @machine_number = CONCAT('KM',RIGHT('00'+CAST(@press_num AS VARCHAR(2)),2))

			--GET SYSTEM AND MACHINE INFO
			SELECT @system_ID = m.system_ID, 
				@operator_ID = current_operator_ID, 
				@machine_ID = m.ID, 
				@tool_ID = ISNULL(m.current_tool_ID, 0), 
				@target_cycle_time = ISNULL(mt.target_cycle_time, 0), 
				@over_cycle_grace_period = ISNULL(mt.over_cycle_grace_period, 0),
				@auto_label = ISNULL(t.auto_label, 0)
			FROM machines m
				LEFT OUTER JOIN tools t ON m.current_tool_ID = t.ID
				LEFT OUTER JOIN machine_tools mt ON mt.machine_ID = m.ID AND mt.tool_ID = m.current_tool_ID
			WHERE m.machine_number = @machine_number

			-- Make sure we have a machine_ID
			SET @machine_ID = ISNULL(@machine_ID, @press_num)

			IF 1=1
				-- Original where we just create the machine cycle and update the machine last cycle time
				BEGIN
					INSERT INTO machine_cycles (machine_ID,tool_ID,cycle_time,cycle_sequence,cycle_duration) VALUES (@press_num,@tool_ID,GETDATE(),@cycle_sequence,@cycle_duration)
					IF @@IDENTITY = (SELECT TOP 1 ID FROM machine_cycles ORDER BY ID DESC)
						SET @cycle_ID = @@IDENTITY
					ELSE
						SELECT TOP 1 @cycle_ID = ID FROM machine_cycles ORDER BY ID DESC

					UPDATE machines
					SET last_cycle_time = GETDATE()
					WHERE ID = @press_num
				END
			ELSE
				BEGIN
					-- Experimental, we didn't get the cycle complete signal, so let's do all that stuff

					--INCREMENT SHOT COUNT FOR TOOL
					UPDATE tools
					SET shot_count = ISNULL(shot_count, 0) + 1
					WHERE ID = @tool_ID

					--GET LAST CYCLE TIME
					SELECT TOP 1 @last_cycle_time = cycle_time, @cycle_sequence = ISNULL(cycle_sequence, 0)
					FROM machine_cycles 
					WHERE machine_ID = @machine_ID
					ORDER BY ID DESC

					--SET MACHINE UP AND RECORD LAST CYCLE TIME
					UPDATE machines 
					SET last_cycle_time = GETDATE(), up = 1
					WHERE ID = @machine_ID

					--GET DURATION OF THAT CYCLE
					IF @last_cycle_time IS NOT NULL
						BEGIN
							SET @last_shift_start = dbo.getShiftStartForTimestamp(@system_ID, GETDATE())

							IF @last_cycle_time < @last_shift_start
								SET @last_cycle_time = @last_shift_start

							SET @cycle_duration = DATEDIFF(SECOND, @last_cycle_time, GETDATE())
						END
					ELSE
						SET @cycle_duration = 0

					--RECORD CYCLE (let KM FTP process update this record by leaving the cycle_sequence and cycle_duration in a known state)
					INSERT INTO machine_cycles (machine_ID, tool_ID, cycle_time, cycle_duration, cycle_sequence, operator_ID) VALUES (
						@machine_ID,
						@tool_ID,
						GETDATE(),
						NULL,
						0,
						@operator_ID)

					SET @cycle_ID = @@IDENTITY

					INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'Inserted Cycle: ' + CAST(@cycle_ID AS varchar(50)))

					--INSERT PARTS BY LOOPING THROUGH THE machine_active_parts
					DECLARE loopyQWQWEEQWEE CURSOR FOR
						SELECT tp.ID
						FROM machine_active_parts map
							JOIN tool_parts tp ON map.tool_id = tp.tool_ID AND map.parts_id = tp.part_ID
						WHERE map.machine_id = @machine_ID
							AND map.tool_id = @tool_ID
							AND start_timestamp <= GETDATE()
							AND end_timestamp IS NULL
						ORDER BY map.cavity_order;

					OPEN loopyQWQWEEQWEE
					FETCH NEXT FROM loopyQWQWEEQWEE INTO @tool_part_ID
					WHILE @@FETCH_STATUS = 0
						BEGIN
							--GET NEXT SERIAL NUMBER
							SELECT @serial_number = dbo.fn_getNextSerial(@tool_part_ID)

							--GET PART NUMBER TO GENERATE FULL BARCODE
							SELECT @part_number = p.part_number
							FROM tool_parts tp
							JOIN parts p ON p.ID = tp.part_ID
							WHERE tp.ID = @tool_part_ID

							INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'Serial number to insert: ' + @serial_number + ' for tool_part_ID: ' + CAST(@tool_part_ID as varchar(50))) 

							--INSERT THE RECORD
							INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, insert_time, serial_number, full_barcode) VALUES (
								@cycle_ID,
								@tool_part_ID,
								GETDATE(),
								@serial_number,
								CONCAT(@part_number, @serial_number)
							)
							FETCH NEXT FROM loopyQWQWEEQWEE INTO @tool_part_ID
						END
	
					CLOSE loopyQWQWEEQWEE
					DEALLOCATE loopyQWQWEEQWEE

					--END ANY OPEN DOWNTIME...
					--BUT IF IT WAS UNDER OVER CYCLE GRACE PERIOD OF TIME PAST THE TARGET, AUTO-DISPOSITION IT AS A MICRO-STOP
					DECLARE @downtime_reason_ID int
					SET @downtime_reason_ID = NULL

					IF @cycle_duration > @target_cycle_time AND @cycle_duration < (@target_cycle_time + @over_cycle_grace_period)
						BEGIN
							--YES, IT IS A MICRO-STOP
							INSERT INTO NYSUS_REPETITIVE_MES.dbo.loggy (log_time, log_msg) VALUES (GETDATE(), 'Machine '+CAST(@machine_ID AS varchar(50))+' with tool '+CAST(@tool_ID AS varchar(50))+' reported in micro-stop')
							SELECT TOP 1 @downtime_reason_ID = ID
							FROM machine_downtime_reasons
							WHERE is_micro_stop = 1
							ORDER BY ID
						END

					--UPDATE ANY OPEN ONGOING DOWNTIME RECORD
					UPDATE machine_downtime_log
					SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0,
						reason_ID = @downtime_reason_ID
					WHERE machine_ID = @machine_ID
						AND reason_ID IS NULL
						AND time_in_state IS NULL



					--PRINT LABELS (IF AUTO-PRINT IS ON)
					IF @auto_label = 1
						BEGIN
							DECLARE @machine_cycle_part_ID int, @part_ID int

							DECLARE loopyQWEQWEEE CURSOR FOR
								SELECT mcp.ID, tp.part_ID
								FROM machine_cycle_parts mcp
									JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID 
								WHERE mcp.cycle_ID = @cycle_ID
								ORDER BY tp.part_order;

							OPEN loopyQWEQWEEE
							FETCH NEXT FROM loopyQWEQWEEE INTO @machine_cycle_part_ID, @part_ID
							WHILE @@FETCH_STATUS = 0
								BEGIN
					
									INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'AUTO-LABELING machine_cycle_part_ID: ' + CAST(@machine_cycle_part_ID AS varchar(50)))

									EXEC dbo.sp_MACHINE_MES_recordGoodPart 
										@machine_ID = @machine_ID,
										@part_ID = @part_ID,
										@operator_ID = @operator_ID,
										@machine_cycle_part_ID = @machine_cycle_part_ID

									FETCH NEXT FROM loopyQWEQWEEE INTO @machine_cycle_part_ID, @part_ID
								END

							CLOSE loopyQWEQWEEE
							DEALLOCATE loopyQWEQWEEE
						END
				END
		END

	--INSERT INTO machine_cycle_data_points (cycle_ID, data_point_ID, data_point_value)
	INSERT INTO @tmp_data (cycle_ID, data_point_ID, data_point_value)
		SELECT @cycle_ID AS cycle_ID, dps.ID AS data_point_ID, TRY_CONVERT(float,ISNULL(t.[value],-1)) AS data_point_value
		FROM @tmp t
		JOIN data_point_setup dps ON t.parameter = dps.data_point_name
			AND dps.extract_method = 'ftp_shell_script'
			AND dps.active = 1

	-- Record ancillary data
	--INSERT INTO machine_cycle_data_points (cycle_ID, data_point_ID, data_point_value)
	--dbo.fn_intToFloat_16bit()
	INSERT INTO @tmp_data (cycle_ID, data_point_ID, data_point_value)
		SELECT
			 @cycle_ID AS cycle_ID
			,dps.ID AS data_point_ID
			,CASE
				WHEN t.value_type = 'FLOAT' THEN dbo.fn_intToFloat_16bit(ISNULL(t.[value],48128))
				ELSE TRY_CONVERT(float, ISNULL(t.[value],-1))
			END AS data_point_value
		FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags t
		JOIN CUSTOM_plc_machine_mapping pmm ON pmm.machine_ID = ISNULL(@machine_ID, @press_num)
			AND pmm.plc_ID = t.plc_ID
			AND pmm.active = 1
		JOIN data_point_setup dps on t.[name] = dps.data_point_name
			AND dps.extract_method = 'plc_polling'
			AND dps.active = 1

	INSERT INTO machine_cycle_data_points (cycle_ID, data_point_ID, data_point_value)
		SELECT cycle_ID, data_point_ID, data_point_value FROM @tmp_data

	-- Enabling alert monitoring
	EXEC sp_handleMachineMonitorAlerting @cycle_ID = @cycle_ID
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
