SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetTimeAndShift]
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @system_ID int

	IF @machine_ID = 0
		SELECT TOP 1 @machine_ID = ID 
		FROM machines 
		WHERE station_IP_address = @my_IP

	SELECT @system_ID = system_ID FROM machines WHERE ID = @machine_ID;

    SELECT GETDATE() as cur_date_time, dbo.getShift(@system_ID, GETDATE()) as cur_shift,
		CONVERT(varchar(12), GETDATE(), 108) as cur_time,
		CONVERT(varchar(12), GETDATE(), 101) as cur_date
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
