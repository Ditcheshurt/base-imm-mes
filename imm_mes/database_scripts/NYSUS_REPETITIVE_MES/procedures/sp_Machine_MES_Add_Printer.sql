SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Machine_MES_Add_Printer] 
	-- Add the parameters for the stored procedure here
	@printer_name varchar(50),
	@printer_IP varchar(50),
	@machine_id int,
	@part_order int,
	@label_type varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @t TABLE (start_id int, next_id int) 
	DECLARE @id int
	DECLARE @temp_machine int = (@machine_id - 1000)*100
	DECLARE @max_printer_id int = @temp_machine+99
	DECLARE @min_printer_id int = @temp_machine+1
	DECLARE @min int = 0 
	DECLARE @max int
	DECLARE @new_id int

	SELECT TOP 1 @min = printer_ID
	FROM machine_printers
	WHERE machine_ID = @machine_id
	AND printer_ID > 100
	ORDER BY printer_ID ASC

	SELECT TOP 1 @max = printer_ID
	FROM machine_printers
	WHERE machine_ID = @machine_id
	ORDER BY printer_ID DESC

	INSERT INTO @t
	SELECT printer_ID, LEAD(printer_ID) OVER(ORDER BY machine_id, printer_ID ASC)
	FROM machine_printers
	WHERE machine_ID = @machine_id
	AND printer_ID <= @max_printer_id
	AND printer_ID > 100

	SELECT TOP 1 @id = start_id
	FROM @t
	WHERE  (start_id + 1 <> next_id OR next_id is NULL)
	ORDER BY start_id ASC

	if @id < @max_printer_id
		BEGIN
			SET @new_id = @id + 1
		END

	 if @min <> @min_printer_id OR @min = 0 
		BEGIN 
			SET @new_id = @min_printer_id
		END

	if @max = @max_printer_id
		BEGIN 
			SELECT 0, 0
			return
		END

	if @new_id >= @min_printer_id AND @new_id <= @max_printer_id
		BEGIN
			INSERT INTO printer_setup
			(ID,printer_name, printer_IP, label_format)
			VALUES
			(@new_id, @printer_name, @printer_IP, 'N/A')
			
			INSERT INTO machine_printers
			(machine_ID, printer_ID, part_order, label_type)
			VALUES
			(@machine_id, @new_id, @part_order, @label_type)

			SELECT @new_id, 1
		END

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
