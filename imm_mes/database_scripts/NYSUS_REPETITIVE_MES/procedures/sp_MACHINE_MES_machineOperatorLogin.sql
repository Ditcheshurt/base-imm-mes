SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		James Knoblauch
-- Create date: 2016-02-18
-- Description:	Log in at a machine
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_machineOperatorLogin] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @current_time datetime
	DECLARE @shift_summary_ID int
	DECLARE @output_table table (ID int)

	SET @current_time = GETDATE()

	-- log the previous operator out of the machine
	EXEC dbo.sp_MACHINE_MES_machineOperatorLogout @machine_ID = @machine_ID

	-- create a login event for the operator 
	INSERT INTO
		MES_COMMON.dbo.operator_event_history (
			operator_ID,
			operator_event_ID,
			start_time
		) 
	OUTPUT INSERTED.ID INTO @output_table (ID)
	VALUES (
		@operator_ID,
		1,
		GETDATE()
	)

	-- associate the login entry with the machine
	INSERT INTO
		machine_operator_event_history (
			machine_ID,
			operator_event_history_ID
		)
	SELECT
		@machine_ID,
		ID
	FROM
		@output_table

	-- associate the operator with the machine
	UPDATE
		machines
	SET
		current_operator_ID = @operator_ID
	WHERE
		ID = @machine_ID

	-- associate the operator with the current shift summary
	SELECT
		@shift_summary_ID = ss.ID
	FROM
		shift_summary ss
		JOIN machines m ON ss.machine_ID = m.ID
	WHERE
		ss.machine_ID = @machine_ID
		AND ss.the_date = dbo.getProdDate(@current_time)
		AND ss.the_shift = dbo.getShift(m.system_ID, @current_time)

	IF NOT EXISTS (SELECT ID FROM operator_shift_summaries WHERE operator_ID = @operator_ID AND shift_summary_ID = @shift_summary_ID) AND @shift_summary_ID IS NOT NULL
		INSERT INTO
			operator_shift_summaries (
				operator_ID,
				shift_summary_ID
			) VALUES (
				@operator_ID,
				@shift_summary_ID
			)

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
