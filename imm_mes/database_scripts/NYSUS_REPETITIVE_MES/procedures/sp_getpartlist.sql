SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Dan Bick
-- Create date: 4-25-2017
-- Description:	base on machine ip will return the 
-- list of tools, groups, and part possible to select
-- =============================================
CREATE PROCEDURE [dbo].[sp_getpartlist] 
	-- Add the parameters for the stored procedure here
	@ip_address varchar(50) = '192.168.0.100'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @machine int = 0

SELECT @machine = ID
FROM machines
WHERE station_IP_address = @ip_address

DECLARE @t TABLE (tool int, part_ID int, group_ID int, part_desc varchar(50), per_tool int, part_number varchar(50))


INSERT INTO @t
SELECT mt.tool_ID, tp.part_ID, p.part_group_id, p.part_desc, tp.parts_per_tool, p.part_number 
FROM machine_tools mt
JOIN tool_parts as tp
ON mt.tool_ID = tp.tool_ID
JOIN parts as p
ON tp.part_ID = p.ID
WHERE mt.machine_ID = @machine
AND p.part_group_id is NOT NULL


SELECT m.tool, t.tool_description, m.group_ID,pg.part_group_description, m.part_ID, m.part_desc, m.per_tool, m.part_number
FROM @t as m
JOIN tools as t
ON m.tool = t.ID
Left Join part_group_hrt as pg
ON m.group_ID = pg.id
WHERE t.active != 0
AND pg.is_active != 0
ORDER BY m.tool ASC, m.group_ID ASC, m.part_ID ASC
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
