SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetTools] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT t.ID as tool_ID, t.tool_description, t.short_description, mt.target_cycle_time
	FROM tools t
		JOIN machine_tools mt ON t.ID = mt.tool_ID 
	WHERE mt.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR mt.machine_ID = @machine_ID) 
		AND t.active = 1

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
