SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 7/30/2018
-- Description:	This will start a end a current tool change (tracking)
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_endToolChange] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tool_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @tool_change_dt int
	DECLARE @downtime_ID TABLE (ID int)
	DECLARE @tool_change_ID TABLE (ID int)

	-- Get current tool change record
	SELECT TOP 1 @tool_change_dt = mdl.tool_change_ID
	FROM machine_downtime_log mdl
	WHERE mdl.machine_ID = @machine_ID
	AND mdl.tool_ID = @tool_ID
	AND mdl.reason_ID = 4
	AND mdl.tool_change_ID IS NOT NULL
	AND mdl.is_tool_change_pause = 0
	ORDER BY change_time DESC

	-- Get any open paused tool change downtime records
	INSERT INTO @downtime_ID
		SELECT mdl.ID 
		FROM machine_downtime_log mdl
		WHERE mdl.machine_ID = @machine_ID
		AND mdl.tool_ID = @tool_ID
		AND mdl.time_in_state IS NULL
		AND mdl.tool_change_ID = @tool_change_dt
		AND mdl.is_tool_change_pause = 1

	-- If there are any paused tool change downtime records, close them as undispositioned
	UPDATE machine_downtime_log
	SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0
	WHERE ID IN (SELECT ID FROM @downtime_ID)

	-- Close any open tool change records
	UPDATE machine_downtime_log
	SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0, startup_time = 0 
	WHERE tool_change_ID = @tool_change_dt
	AND time_in_state IS NULL
	AND reason_ID = 4

	-- Return the tool change record ID
	SELECT @tool_change_dt AS ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
