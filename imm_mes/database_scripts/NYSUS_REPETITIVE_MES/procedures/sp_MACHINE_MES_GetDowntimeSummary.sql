SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetDowntimeSummary] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--TOP 5
	SELECT TOP 5 dr.reason_description, dr.ID, ISNULL(SUM(dl.time_in_state), 0) as downtime_minutes
	FROM machine_downtime_log dl
		JOIN machine_downtime_reasons dr ON dl.reason_ID = dr.ID
		JOIN machines m ON dl.machine_ID = m.ID
	WHERE dl.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR dl.machine_ID = @machine_ID)
		AND dl.up = 0
		AND dl.change_time > DATEADD(DAY, -3, GETDATE())
		AND dbo.getProdDate(change_time) = dbo.getProdDate(GETDATE())
		AND dbo.getShift(m.system_ID, change_time) = dbo.getShift(m.system_ID, GETDATE())
		AND dl.time_in_state IS NOT NULL
	GROUP BY dr.reason_description, dr.ID
	ORDER BY SUM(dl.time_in_state) DESC

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
