SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 4/26/2018
-- Description:	This will add a part to a rack from the machine cycle part ID
-- =============================================
CREATE PROCEDURE sp_MACHINE_MES_packoutAddPartToRack 
	-- Add the parameters for the stored procedure here
	@machine_cycle_part_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @machine_ID int = 0, @part_ID int = 0, @standard_pack_qty int
	DECLARE @container_ID int = 0, @container_position int = 0 

    -- Insert statements for procedure here	
	SELECT @machine_ID = mc.machine_ID, @part_ID = tp.part_ID, @standard_pack_qty = p.standard_pack_qty
	FROM machine_cycle_parts mcp
	JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
	JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
	JOIN parts p ON p.ID = tp.part_ID
	WHERE mcp.ID = @machine_cycle_part_ID

	--ADD PART TO RACK
	SELECT TOP 1 @container_ID = ID 
	FROM containers
	WHERE part_ID = @part_ID
		AND end_time IS NULL

	IF @container_ID = 0
		BEGIN
			INSERT INTO containers (machine_ID, part_ID, start_time) VALUES (
				@machine_ID,
				@part_ID,
				GETDATE()
			)

			SET @container_ID = @@IDENTITY

		END

	--GET CONTAINER POSITION

	-- * * * * * * * * * * * * * *
	-- modified 5/5/2017 by R.Kahle
	--  > included logic to look for the next lowest available container position
	-- modified 5/9/2017 by R.Kahle
	--  > moved exisitng logic to a function 'dbo.fn_getNextAvailableContainerPosition(@container_ID)'
	-- * * * * * * * * * * * * * *

	--SELECT @container_position = COUNT(*)
	--FROM container_parts 
	--WHERE container_ID = @container_ID
	--SET @container_position = @container_position + 1

	SET @container_position = dbo.fn_getNextAvailableContainerPosition(@container_ID)

	--INSERT PART
	INSERT INTO container_parts (container_ID, machine_cycle_part_ID, loaded_time, container_position, operator_ID) VALUES (
		@container_ID,
		@machine_cycle_part_ID,
		GETDATE(),
		@container_position,
		@operator_ID
	)


	--CHECK IF CONTAINER IS FULL
	IF @container_position >= @standard_pack_qty
		BEGIN
			UPDATE containers
			SET end_time = GETDATE()
			WHERE ID = @container_ID

			EXEC sp_MACHINE_MES_printRackLabel @container_id = @container_ID
		END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
