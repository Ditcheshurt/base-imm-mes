SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 7/31/2018
-- Description:	This will get the selected machines current tool change times
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_getToolChangeTimes] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @tc_ID int, @tool_ID int
	DECLARE @tool_change_timer int, @tool_change_time int, @downtime int

	SELECT @tool_ID = m.current_tool_ID
	FROM machines m
	WHERE m.ID = @machine_ID

	SELECT TOP 1 @tool_change_timer = DATEDIFF(second, mdl2.change_time, GETDATE()), @tc_ID = mdl2.tool_change_ID
	FROM machine_downtime_log mdl
	LEFT JOIN machine_downtime_log mdl2 ON mdl2.ID = mdl.tool_change_ID
	WHERE mdl.tool_change_ID IS NOT NULL
	AND mdl.time_in_state IS NULL
	AND mdl.machine_ID = @machine_ID
	AND mdl.tool_ID = @tool_ID
	AND mdl2.ID IS NOT NULL
	ORDER BY mdl2.change_time DESC

	SELECT
		@tool_change_time  = SUM(ISNULL(mdl.time_in_state * 60, DATEDIFF(second, mdl.change_time, GETDATE())))
	FROM machine_downtime_log mdl
	WHERE mdl.tool_change_ID = @tc_ID
	AND mdl.is_tool_change_pause = 0
	GROUP BY mdl.tool_change_ID

	SELECT
		@downtime = SUM(ISNULL(mdl.time_in_state * 60, DATEDIFF(second, mdl.change_time, GETDATE())))
	FROM machine_downtime_log mdl
	WHERE mdl.tool_change_ID = @tc_ID
	AND mdl.is_tool_change_pause = 1
	GROUP BY mdl.tool_change_ID

	SELECT ISNULL(@tc_ID, 0) AS [tool_change_ID], ISNULL(@tool_change_timer, 0) AS [time_since_start], ISNULL(@tool_change_time, 0) AS [tool_change_time], ISNULL(@downtime, 0) AS [downtime]
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
