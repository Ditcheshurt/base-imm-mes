SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetShiftProductionSummary] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @system_ID int;
	--DECLARE @my_IP varchar(50) = '192.168.0.100'
	--DECLARE @machine_ID int = 50000

	--ACTUAL
		SELECT TOP 1 @system_ID = system_ID FROM machines 
		WHERE 
			ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
			AND (@machine_ID = 0 OR ID = @machine_ID)

		--SELECT TOP 3 *
		--FROM shift_summary
		--WHERE 
		--	system_ID = @system_ID
		--	AND machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		--	AND (@machine_ID = 0 OR machine_ID = @machine_ID)
		--	AND the_date > DATEADD(DAY, -2, GETDATE())
		--ORDER BY ID DESC
		
		SELECT TOP 2
			 [ID]
			,[system_ID]
			,[machine_ID]
			,[tool_ID]
			,[line_ID]
			,[the_date]
			,[the_shift]
			,[part_qty]
			,[part_target_qty]
			,[part_goal]
			,[total_target_parts]
			,dbo.fn_calculateOEEFactor('perf',[part_qty],[part_target_qty],0) AS [performance]
			,[scrap_numerator]
			,[scrap_denominator]
			,dbo.fn_calculateOEEFactor('qual',[scrap_numerator],[scrap_denominator],0) AS [quality]
			,[downtime_minutes]
			,[scheduled_minutes]
			,[total_minutes]
			,dbo.fn_calculateOEEFactor('avail',[downtime_minutes],[total_minutes],[scheduled_minutes]) AS [availability]
			,dbo.fn_calculateOEE([performance], [quality], [availability]) AS [oee]
			,[last_updated]
			,[num_calcs]
		FROM shift_summary
		WHERE 
			system_ID = @system_ID
			AND machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
			AND tool_ID IN (SELECT current_tool_ID FROM machines WHERE station_IP_address = @my_IP)
			AND (@machine_ID = 0 OR machine_ID = @machine_ID)
			AND the_date > DATEADD(DAY, -2, GETDATE())
		ORDER BY the_date DESC, the_shift ASC
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
