SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 2/21/2012
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_record_fault] 
	-- Add the parameters for the stored procedure here
	@tag_ID int, 
	@plc_bit int = -1,
	@state int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @fault_code_ID int
	DECLARE @on_since datetime

	SET @fault_code_ID = 0

	IF @plc_bit >= 0
		BEGIN
			--BIT-BASED
			SELECT @fault_code_ID = ID, @on_since = on_since 
			FROM fault_codes 
			WHERE tag_ID = @tag_ID
				AND plc_bit = @plc_bit 

			IF @fault_code_ID <> 0
				BEGIN
					IF @state > 0
						BEGIN
							--BIT FAULT IS ACTIVE, SO SET IT AND CREATE HISTORY RECORD
							IF @on_since IS NULL
								BEGIN
									UPDATE fault_codes
									SET on_since = GETDATE()
									WHERE ID = @fault_code_ID

									INSERT INTO fault_history (fault_id, date_time) VALUES (
										@fault_code_ID,
										GETDATE()
									)
								END
						END
					ELSE
						BEGIN
							--BIT FAULT IS NOT ACTIVE, SO CLEAR IT AND END ANY HISTORY
							IF @on_since IS NOT NULL
								BEGIN
									UPDATE fault_codes 
									SET on_since = NULL
									WHERE id = @fault_code_ID 

									UPDATE fault_history
									SET duration = (DATEDIFF(SECOND, @on_since, GETDATE())/60.0)
									WHERE fault_id = @fault_code_ID
										AND duration IS NULL
								END
						END

				END
		END
	ELSE
		BEGIN
			--CLOSE OUT ANY <> @state
			UPDATE fault_history
			SET duration = (DATEDIFF(SECOND, date_time, GETDATE())/60.0)
			WHERE fault_ID IN (
					SELECT id 
					FROM fault_codes
					WHERE tag_ID = @tag_ID 
						AND fault_word_value <> @state
				)
				AND duration IS NULL

			--TURN OFF ALL <> @state
			UPDATE fault_codes 
			SET on_since = NULL
			WHERE tag_ID = @tag_ID
				AND fault_word_value <> @state

			--TURN ON fault
			IF @state <> 0
				BEGIN
					--GET FAULT CODE AND ON STATE
					SELECT @fault_code_ID = ID, @on_since = on_since 
					FROM fault_codes 
					WHERE tag_ID = @tag_ID
						AND fault_word_value = @state

					IF @on_since IS NULL
						BEGIN
							--SET FAULT AND CREATE HISTORY
							UPDATE fault_codes 
							SET on_since = GETDATE()
							WHERE ID = @fault_code_ID

							INSERT INTO fault_history (fault_id, date_time) VALUES (
										@fault_code_ID,
										GETDATE()
									)
						END
				END
		END

	
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
