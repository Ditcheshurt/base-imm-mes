SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Sass
-- Create date: 1-14-2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CUSTOM_doREVScrapICIFIle] 
	@do_insert bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @t TABLE (machine_cycle_ID int)
	DECLARE @DROP_LOCATION varchar(200)
	DECLARE @dt varchar(12), @d varchar(8)
	DECLARE @file varchar(MAX), @num_recs int = 0
	DECLARE @part_number varchar(100), @qty int
	DECLARE @ffq_ID int, @max_ID int

	SET @DROP_LOCATION = '\\10.68.160.95\SigConnects\in'
	
	--HEADER
	DECLARE @SITE varchar(50)				= '10'
	DECLARE @LOCATION varchar(50)			= '510'
	DECLARE @USERID varchar(50)				= 'REVMSCRP'
	DECLARE @ACCT_NUM varchar(50)			= '4018'
	DECLARE @PROD_LINE varchar(50)			= 'KLLG'
	DECLARE @COST_CNTR varchar(50)			= '000'
	

	--DATE
	SET @d = dbo.fn_padStringFront(CAST(DATEPART(MONTH, GETDATE()) as varchar(2)), 2, '0') + '/' +
			  dbo.fn_padStringFront(CAST(DATEPART(DAY, GETDATE()) as varchar(2)), 2, '0') + '/' +
			  RIGHT(CAST(DATEPART(YEAR, GETDATE()) as varchar(4)), 2)

	--DATETIME
	SET @dt = dbo.fn_padStringFront(CAST(DATEPART(MONTH, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(DAY, GETDATE()) as varchar(2)), 2, '0') + 
			  RIGHT(CAST(DATEPART(YEAR, GETDATE()) as varchar(4)), 2) + 
			  dbo.fn_padStringFront(CAST(DATEPART(HOUR, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(MINUTE, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(SECOND, GETDATE()) as varchar(2)), 2, '0')

	--POPULATE TEMP TABLE
	INSERT INTO @t (machine_cycle_ID)
	SELECT ID 
	FROM machine_cycle_parts
	WHERE disposition = 'REPAIRED'
	 AND rework_datetime > DATEADD(MINUTE, -30, GETDATE()) 
	 and disposition_time >= '2018-11-13'
	 and needs_qad_reverse = 1
	ORDER BY ID

	SELECT @max_ID = MAX(machine_cycle_ID)
	FROM @t

	DECLARE loopyRevScrap CURSOR FOR
		SELECT p.mrp_part_number, (COUNT(*) * -1) as qty
		FROM parts p
			JOIN tool_parts tp ON p.ID = tp.part_ID
			JOIN machine_cycle_parts mcp ON tp.ID = mcp.tool_part_ID
			JOIN @t t ON mcp.ID = t.machine_cycle_ID
		GROUP BY p.mrp_part_number
		ORDER BY p.mrp_part_number
		;

	OPEN loopyRevScrap
	FETCH NEXT FROM loopyRevScrap INTO @part_number, @qty
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @part_number as part_number, @qty as qty
			SET @file = ''

			--INSERT INTO flat_file
			--HEADER:
			SET @file = @file + 
				dbo.fn_padStringRear(@part_number, 18, ' ') + 
				dbo.fn_padStringRear('', 18, ' ') +   --NOT SURE WHAT THIS IS
				dbo.fn_padStringRear(CAST(@qty as varchar(11)), 11, ' ') + 
				dbo.fn_padStringRear(@SITE, 8, ' ') + 
				dbo.fn_padStringRear(@LOCATION, 8, ' ') + 
				dbo.fn_padStringRear(@USERID, 8, ' ') + 
				dbo.fn_padStringRear(@ACCT_NUM, 4, ' ') +
				dbo.fn_padStringRear(@PROD_LINE, 4, ' ') + 
				dbo.fn_padStringRear(@COST_CNTR, 4, ' ') + 
				@d +
				CHAR(13) + CHAR(10)

			--INCREMENT num_recs
			SET @num_recs = @num_recs + 1

			--WRITE A FILE FOR EACH PART...WHICH IS SILLY
			IF @do_insert = 1
				BEGIN
					--INSERT
					INSERT INTO NYSUS_SEQUENCE_MES.dbo.flat_file_queue (posting_app, record_ID, file_text, drop_dir, post_time, drop_time, process_time, use_key_file) VALUES (
						'ICI',
						-3, -- THIS WILL BE OVERWRITTEN BY THE UPDATE
						CAST(@file as text),
						@DROP_LOCATION,
						GETDATE(),
						NULL,
						NULL,
						0)

					SELECT TOP 1 @ffq_ID = ID
					FROM NYSUS_SEQUENCE_MES.dbo.flat_file_queue 
					WHERE record_ID = -3
					ORDER BY ID DESC

					UPDATE NYSUS_SEQUENCE_MES.dbo.flat_file_queue
					SET record_ID = @ffq_ID
					WHERE ID = @ffq_ID
				END
			ELSE
				SELECT @num_recs as num_recs, @file as [file]

			FETCH NEXT FROM loopyRevScrap INTO @part_number, @qty
		END

	CLOSE loopyRevScrap
	DEALLOCATE loopyRevScrap

	--record part has been reversed
	update machine_cycle_parts
	set needs_qad_reverse = 2
	where ID in (SELECT machine_cycle_ID
	            FROM @t)-- AND rework_datetime IS NOT NULL AND 

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
