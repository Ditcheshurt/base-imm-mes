SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CheckPartReadyWrapper] 
	@tag_ID varchar(10),
	@tag_value varchar(30),
	@tag_desc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @tries int = 1

	WHILE @tries < 5
		BEGIN
			BEGIN TRANSACTION
				BEGIN TRY

					EXEC sp_MACHINE_MES_CheckPartReady @tag_ID = @tag_ID, @tag_value = @tag_value, @tag_desc = @tag_desc

					COMMIT
					BREAK
				END TRY
				BEGIN CATCH
					ROLLBACK
					
					INSERT INTO loggy (log_time, log_msg) VALUES (
								GETDATE(),
								'CAUGHT AN ERROR EXECUTING sp_MACHINE_MES_CheckPartReady: ' + CAST(ERROR_NUMBER() as varchar(50)) + ' ' + ERROR_MESSAGE()
							)

					SET @tries = @tries + 1
					CONTINUE
				END CATCH;
		END

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
