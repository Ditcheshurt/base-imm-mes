SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 7/27/2015
-- Description:	
-- =============================================
CREATE PROCEDURE sp_checkOJTRequirements 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--GET ANY REQUIRED JOBS THAT HAVE NOT BEEN SIGNED OFF YET
    SELECT j.job_details, j.job_name
	FROM machine_ojt_jobs moj
		JOIN MES_COMMON.dbo.ojt_jobs j ON moj.ojt_job_ID = j.ID 
		LEFT OUTER JOIN MES_COMMON.dbo.ojt_job_signoffs s ON j.ID = s.ojt_job_ID AND s.operator_ID = @operator_ID
	WHERE moj.machine_ID = @machine_ID
		AND s.ID IS NULL
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
