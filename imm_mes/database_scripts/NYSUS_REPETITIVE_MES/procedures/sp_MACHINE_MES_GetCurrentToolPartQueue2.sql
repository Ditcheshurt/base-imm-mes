SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		R.Kahle
-- Create date: 7/5/2017
-- Description:	This is a test before commiting to official SP
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetCurrentToolPartQueue2] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @temp TABLE ( ID int, cycle_ID int, tool_part_ID int, serial_number varchar(50), full_barcode varchar(50), container_ID int, partial_container_ID int, insert_time datetime, machine_group_rack_ID int, machine_group_rack_part_order int, machine_group_rack_pack_time datetime, machine_group_rack_pack_operator_ID int, plc_op_done_value int, disposition varchar(50), disposition_time datetime, part_ID int, tool_ID int, part_desc varchar(50), part_number int, part_image varchar(1000))
	INSERT INTO @temp (ID, cycle_ID, tool_part_ID, serial_number, full_barcode, container_ID, partial_container_ID, insert_time, machine_group_rack_ID, machine_group_rack_part_order, machine_group_rack_pack_time, machine_group_rack_pack_operator_ID, plc_op_done_value, disposition, disposition_time, part_ID, tool_ID, part_desc, part_number, part_image) 
		SELECT mcp.ID, mcp.cycle_ID, mcp.tool_part_ID, mcp.serial_number, mcp.full_barcode, mcp.container_ID, mcp.partial_container_ID, mcp.insert_time, mcp.machine_group_rack_ID, mcp.machine_group_rack_part_order, mcp.machine_group_rack_pack_time, mcp.machine_group_rack_pack_operator_ID, mcp.plc_op_done_value, mcp.disposition, mcp.disposition_time, tp.part_ID, tp.tool_ID, p.part_desc, p.part_number, p.part_image
		FROM machine_cycle_parts mcp
			JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID 
			JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID AND tp.part_ID IN (
				SELECT parts_id 
				FROM machine_active_parts 
				WHERE end_timestamp IS NULL AND 
					machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
					AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
				)
			JOIN parts p ON tp.part_ID = p.ID
		WHERE mc.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
			AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
			AND mcp.disposition_time IS NULL

	SELECT count(*) AS parts FROM (SELECT part_ID FROM @temp GROUP BY part_ID) as X
	SELECT part_ID, count(part_ID) AS part_count FROM @temp GROUP BY part_ID
	SELECT TOP(100) * FROM @temp
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
