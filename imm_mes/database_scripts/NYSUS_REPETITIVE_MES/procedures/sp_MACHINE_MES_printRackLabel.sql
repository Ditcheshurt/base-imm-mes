SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 12/14/2017
-- Description:	This will print a rack label
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_printRackLabel] 
	-- Add the parameters for the stored procedure here
	@container_id int = 0, 
	@alt_printer_id int = 0,
	@select_return bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @part_number varchar(50), @part_desc varchar(50), @container_num varchar(10)
    DECLARE @print_text varchar(MAX) 
	DECLARE @machine_ID int, @printer_ID int, @default_label_qty int
	DECLARE @part_ID int, @tool_ID int
	DECLARE @container_qty int
	DECLARE @datestamp varchar(10)
	DECLARE @shift varchar(1)
	DECLARE @time datetime
	DECLARE @cure_time datetime --JV 2/27/18

	IF @container_id = 0
		RETURN

    --GET RACK INFO
	SELECT TOP 1
		@printer_ID = ps.ID
		,@container_num = (REPLICATE('0',6-LEN(c.ID)) + CAST(c.ID AS varchar))
		,@part_desc = p.part_desc
		,@part_number = p.part_number
		,@container_qty = COUNT(cp.ID)
	FROM containers c
	JOIN machine_active_parts map ON map.machine_id = c.machine_ID AND map.parts_id = c.part_ID AND map.is_active = 1
	JOIN tool_parts tp ON tp.part_ID = map.parts_ID AND tp.tool_ID = map.tool_ID
	JOIN machine_printers mp ON mp.part_order = tp.part_order AND mp.machine_ID = c.machine_ID
	JOIN printer_setup ps ON ps.ID = mp.printer_ID
	LEFT JOIN parts p ON p.ID = tp.part_ID
	LEFT JOIN container_parts cp ON cp.container_ID = c.ID
	WHERE mp.label_type = 'RACK' AND c.ID = @container_id
	GROUP BY c.ID,p.part_desc,p.part_number,ps.ID,map.map_id

	SELECT @cure_time = DATEADD(hour,4,GETDATE()) -- JV 2/27/18

	IF @alt_printer_ID <> 0
		SET @printer_ID = @alt_printer_ID
    
    SELECT @print_text = label_template, @default_label_qty = ISNULL(default_qty, 1)
    FROM printer_setup 
    WHERE id = @printer_ID

	IF DATEPART(hh, @time) >= 6.5 AND DATEPART(hh, @time) + CAST(DATEPART(mi, @time) As float)/60.0 < CAST(18.5 As float)
		SET @shift = 'A'
	ELSE
		SET @shift = 'B'

	SET @datestamp = CONVERT(varchar(10), GETDATE(), 1) + @shift

	SET @time = GETDATE()

    SET @print_text = REPLACE(@print_text, '<<part_number>>', @part_number)
	IF @part_desc IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<part_desc>>', @part_desc)

	SET @print_text = REPLACE(@print_text, '<<cont_id>>', @container_num)
	SET @print_text = REPLACE(@print_text, '<<cont_qty>>', @container_qty)

    SET @print_text = REPLACE(@print_text, '<<date>>', CONVERT(varchar(20), GETDATE(), 101))
    SET @print_text = REPLACE(@print_text, '<<time>>', CONVERT(varchar(20), GETDATE(), 108))
    SET @print_text = REPLACE(@print_text, '<<DATE>>', CONVERT(varchar(20), GETDATE(), 101))
    SET @print_text = REPLACE(@print_text, '<<TIME>>', CONVERT(varchar(20), GETDATE(), 108))

	SET @print_text = REPLACE(@print_text, '<<cure_time>>', CONVERT(varchar(5), @cure_time, 108)) -- JV 2/27/18
	SET @print_text = REPLACE(@print_text, '<<cure_date>>', CONVERT(varchar(5), @cure_time, 101)) -- JV 2/27/18
        
	SET @print_text = REPLACE(@print_text, '<<date2>>', @datestamp)
            
    --insert record into print queue
	IF @printer_ID <> 0
		INSERT INTO print_queue (mod_ID, printer_ID, print_text, queue_time, qty) VALUES (
			@container_id, 
			@printer_ID, 
			@print_text, 
			GETDATE(),
			@default_label_qty)

	IF @select_return = 1
		SELECT @printer_ID as printer_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
