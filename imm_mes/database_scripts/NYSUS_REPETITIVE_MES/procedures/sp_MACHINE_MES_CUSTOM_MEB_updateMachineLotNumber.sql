SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 10/17/2018
-- Description:	This will update the machines active lot by passing in a valid matcert ID and a machine ID str
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CUSTOM_updateMachineLotNumber] 
	-- Add the parameters for the stored procedure here
	@operator_ID int = 0, 
	@test_ID int = 0,
	@machine_str varchar(20) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @lots TABLE (operator_ID int, machine_ID int, material_cert_test_ID int, start_timestamp datetime, end_timestamp datetime, active bit)
	DECLARE @out TABLE (ID int, machine_ID int, material_cert_test_ID int, [timestamp] datetime, active bit)

	INSERT INTO @lots
		SELECT
			 t1.operator_ID
			,t1.machine_ID
			,t1.material_cert_test_ID
			,t1.start_timestamp
			,t1.end_timestamp
			,t1.active
		FROM (
			SELECT
				 @operator_ID AS operator_ID
				,t.items AS machine_ID
				,@test_ID AS material_cert_test_ID
				,GETDATE() AS start_timestamp
				,NULL AS end_timestamp
				,1 AS active
			FROM dbo.Split(@machine_str,'~') t
		) t1
		JOIN CUSTOM_material_certification_test mct ON mct.ID = t1.material_cert_test_ID AND mct.lot_number IS NOT NULL AND LEN(mct.lot_number) > 0

	INSERT INTO @out
	SELECT ot0.ID, ot0.machine_ID, ot0.material_cert_test_ID, ot0.end_timestamp AS [timestamp], ot0.active
	FROM (
		UPDATE CUSTOM_machine_lot_tracking
		SET end_timestamp = GETDATE(), active = 0
		OUTPUT inserted.ID, inserted.machine_ID, inserted.material_cert_test_ID, inserted.end_timestamp, inserted.active
		WHERE machine_ID NOT IN (SELECT machine_ID FROM @lots)
		AND material_cert_test_ID = @test_ID
		AND end_timestamp IS NULL and active = 1
	) ot0

	INSERT INTO @out
	SELECT ot1.ID, ot1.machine_ID, ot1.material_cert_test_ID, ot1.end_timestamp AS [timestamp], ot1.active
	FROM (
		UPDATE mlt
		SET end_timestamp = GETDATE(), active = 0
		OUTPUT inserted.ID, inserted.machine_ID, inserted.material_cert_test_ID, inserted.end_timestamp, inserted.active
		FROM CUSTOM_machine_lot_tracking mlt
		JOIN @lots t1 ON t1.machine_ID = mlt.machine_ID AND t1.material_cert_test_ID <> mlt.material_cert_test_ID
		WHERE mlt.end_timestamp IS NULL AND mlt.active = 1
	) ot1

	INSERT INTO @out
	SELECT ot2.ID, ot2.machine_ID, ot2.material_cert_test_ID, ot2.start_timestamp AS [timestamp], ot2.active
	FROM (
		INSERT INTO CUSTOM_machine_lot_tracking
		OUTPUT inserted.ID, inserted.machine_ID, inserted.material_cert_test_ID, inserted.start_timestamp, inserted.active
			SELECT
				 l.operator_ID
				,l.machine_ID
				,l.material_cert_test_ID
				,l.start_timestamp
				,l.end_timestamp
				,l.active
			FROM @lots l
			WHERE l.machine_ID NOT IN (
				SELECT mlt.machine_ID
				FROM @lots t1
				JOIN CUSTOM_machine_lot_tracking mlt ON mlt.machine_ID = t1.machine_ID 
				WHERE mlt.active = 1 AND mlt.end_timestamp IS NULL
			)
	) ot2

	SELECT ID, machine_ID, material_cert_test_ID, [timestamp], active FROM @out
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
