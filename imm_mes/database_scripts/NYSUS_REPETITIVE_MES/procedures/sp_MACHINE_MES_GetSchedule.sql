SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetSchedule] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--get current tool
	DECLARE @current_tool_ID int, @current_run_ID int, @current_parts_in_run int = 0


	SELECT @current_tool_ID = current_tool_ID
	FROM machines
	WHERE ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR ID = @machine_ID)

	SELECT TOP 1 @current_run_ID = run_ID
	FROM machine_cycles
	WHERE machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR machine_ID = @machine_ID) 
		AND tool_ID = @current_tool_ID
	ORDER BY ID DESC

	SELECT @current_parts_in_run = COUNT(*)
	FROM machine_cycles mc
		JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
	WHERE mc.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID) 
		AND mc.run_ID = @current_run_ID

	--TOP 5
	SELECT TOP 5 ms.*, t.tool_description, mt.target_cycle_time, @current_parts_in_run as current_parts_in_run, @current_run_ID as current_run_ID, @current_tool_ID as current_tool_ID
	FROM machine_schedules ms
		JOIN tools t ON ms.tool_ID = t.ID
		join machine_tools mt ON ms.machine_ID = mt.machine_ID AND t.ID = mt.tool_ID
	WHERE ms.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR ms.machine_ID = @machine_ID)
		AND ms.actual_start_time IS NULL
	ORDER BY ms.ID DESC

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
