SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 10/12/2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_UpgradeInProcessPartForPackout]
	-- Add the parameters for the stored procedure here
	@serial VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @reject_station_ID int
	DECLARE @rework_history_ID int
	DECLARE @reject_station_date datetime
	DECLARE @machine_ID int
	DECLARE @last_machine_completed_ID int
	DECLARE @last_machine_completed_order int
 
	SELECT TOP 1 
		@rework_history_ID = ID		
	FROM
		rework_history 
	WHERE
		serial = @serial
	ORDER BY 
		rework_date desc;

	--SELECT TOP 1 
	--	@reject_station_ID = last_machine_completed_ID,
	--	@reject_station_date = last_machine_completed_time
	--FROM 
	--	machine_in_process_parts
	--WHERE 
	--	serial = @serial
	--ORDER BY 
	--	created_time desc;

	---- is it long or short shaft
	--SELECT 
	--	@machine_ID = machine_ID 
	--FROM 
	--	machine_cycle_parts mcp
	--JOIN 
	--	machine_cycles mc ON mc.ID = mcp.cycle_ID 
	--WHERE 
	--	mcp.serial_number = @serial;

	---- set to last station on SS line
	--IF @machine_ID >= 2000 AND @machine_ID < 3000
	--BEGIN
	--	SET @last_machine_completed_ID = 20045
	--	SET @last_machine_completed_order = 45
	--END
	
	---- set to last station on LS line
	--IF @machine_ID >= 3000 AND @machine_ID < 4000
	--BEGIN
	--	SET @last_machine_completed_ID = 30080
	--	SET @last_machine_completed_order = 80
	--END

	SET @last_machine_completed_ID = 30080
	SET @last_machine_completed_order = 80

	-- update rework_history
	UPDATE rework_history SET 
		rejected_station_ID = @reject_station_ID, 
		rejected_station_date = @reject_station_date,
		injected_station_ID = @last_machine_completed_ID,
		injected_station_date = getDate()
	WHERE 
		ID = @rework_history_ID;
		
	-- update in process parts for mes
	UPDATE machine_in_process_parts SET 
		last_machine_completed_ID = @last_machine_completed_ID, 
		last_machine_completed_order = 80,
		last_machine_completed_time = getDate()
	WHERE
		serial = @serial;

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
