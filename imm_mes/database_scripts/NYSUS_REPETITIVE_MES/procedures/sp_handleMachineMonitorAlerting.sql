SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/7/2018
-- Description:	This will apply configured machine monitor alerting
-- =============================================
CREATE PROCEDURE [dbo].[sp_handleMachineMonitorAlerting]
	-- Add the parameters for the stored procedure here
	@cycle_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @tmp TABLE (ID int, created_timestamp datetime, machine_cycle_datapoint_ID int, limit_triggered int)

	-- First grab any unprocessed alerts that were already inserted
	INSERT INTO @tmp
		SELECT mma.ID,mma.created_timestamp,mma.machine_cycle_datapoint_ID,mma.limit_triggered
		FROM machine_cycle_data_points mcdp
		JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
		JOIN machine_monitor_alerts mma ON mma.machine_cycle_datapoint_ID = mcdp.ID
		WHERE mcdp.cycle_ID = @cycle_ID
		AND mma.processed_timestamp IS NULL

	-- Next insert any new alerts
	INSERT INTO machine_monitor_alerts (created_timestamp, machine_cycle_datapoint_ID, limit_triggered)
		OUTPUT inserted.ID, inserted.created_timestamp, inserted.machine_cycle_datapoint_ID, inserted.limit_triggered INTO @tmp
		SELECT GETDATE(), mcdp.ID, CASE WHEN mcdp.data_point_value > mtcl.upper_ctrl_limit THEN 1 WHEN mcdp.data_point_value < mtcl.lower_ctrl_limit THEN -1 Else 0 END
		FROM machine_cycle_data_points mcdp
		JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
		JOIN machine_tool_ctrl_limits mtcl ON mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.deactivated = 0
		JOIN machine_monitor_control_limit_severity mmcls ON mmcls.machine_tool_ctrl_limit_ID = mtcl.ID AND mmcls.active = 1 AND ((mcdp.data_point_value > mtcl.upper_ctrl_limit) OR (mcdp.data_point_value < mtcl.lower_ctrl_limit))
		LEFT JOIN machine_monitor_severity_actions mmsa ON mmsa.machine_monitor_severity_ID = mmcls.machine_monitor_severity_ID
		LEFT JOIN machine_monitor_severity mms ON mms.ID = mmsa.machine_monitor_severity_ID
		LEFT JOIN machine_monitor_actions mma ON mma.ID = mmsa.machine_monitor_action_ID
		LEFT JOIN machine_monitor_action_handler mmah ON mmah.ID = mma.action_handler_ID
		WHERE mcdp.cycle_ID = @cycle_ID
		AND mcdp.ID NOT IN (
			SELECT m1.ID FROM machine_cycle_data_points m1 JOIN machine_monitor_alerts malrt ON malrt.machine_cycle_datapoint_ID = m1.ID WHERE m1.cycle_ID = mcdp.cycle_ID
		)
		GROUP BY mcdp.ID, mcdp.data_point_value, mtcl.upper_ctrl_limit, mtcl.lower_ctrl_limit

	DECLARE @tmpID int, @machine_ID int

	DECLARE @cmd nvarchar(MAX) = ''
	DECLARE @vars nvarchar(MAX) = ''
	DECLARE @assignments nvarchar(1000) = ''
	DECLARE @result int

	DECLARE process_monitor_alerts CURSOR LOCAL FORWARD_ONLY FOR
		SELECT t1.ID, ah.action_handler, ah.action_parameter_list, mma.action_parameter_array, mc.machine_ID
		FROM @tmp t1
		JOIN machine_cycle_data_points mcdp ON mcdp.ID = t1.machine_cycle_datapoint_ID
		JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
		JOIN machine_tool_ctrl_limits mtcl ON mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.deactivated = 0
		JOIN machine_monitor_control_limit_severity cls ON cls.machine_tool_ctrl_limit_ID = mtcl.ID
		LEFT JOIN machine_monitor_severity_actions mmsa ON mmsa.machine_monitor_severity_ID = cls.machine_monitor_severity_ID
		LEFT JOIN machine_monitor_actions mma ON mma.ID = mmsa.machine_monitor_action_ID
		LEFT JOIN machine_monitor_action_handler ah ON ah.ID = mma.action_handler_ID

	OPEN process_monitor_alerts
	FETCH NEXT FROM process_monitor_alerts INTO @tmpID, @cmd, @vars, @assignments, @machine_ID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @assignments = REPLACE(@assignments, '<<CYCLE_ID>>', @cycle_ID)
		SET @assignments = REPLACE(@assignments, '<<ALERT_ID>>', @tmpID)
		SET @assignments = REPLACE(@assignments, '<<MACHINE_ID>>', @machine_ID)

		EXEC @result = dbo.sp_runDynamicDoubleTrouble @cmd = @cmd, @vars = @vars, @assignments = @assignments

		UPDATE machine_monitor_alerts
		SET processed_timestamp = CASE WHEN @result >= 0 THEN GETDATE() ELSE NULL END
		WHERE ID = @tmpID
		
		FETCH NEXT FROM process_monitor_alerts INTO @tmpID, @cmd, @vars, @assignments, @machine_ID
	END

	CLOSE process_monitor_alerts
	DEALLOCATE process_monitor_alerts
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
