SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 11/20/2018
-- Description:	This will handle recording of ancillary data
-- =============================================
CREATE PROCEDURE sp_MACHINE_MES_CUSTOM_recordAncillaryData 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@cycle_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO machine_cycle_data_points
		SELECT @cycle_ID AS cycle_ID, dps.ID AS data_point_ID, TRY_CONVERT(float,ISNULL(t.[value],-1)) AS data_point_value
		FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags t
		JOIN CUSTOM_plc_machine_mapping pmm ON pmm.machine_ID = @machine_ID
			AND pmm.plc_ID = t.plc_ID
			AND pmm.active = 1
		JOIN data_point_setup dps on t.[name] = dps.data_point_name
			AND dps.extract_method = 'plc_polling'
			AND dps.active = 1

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
