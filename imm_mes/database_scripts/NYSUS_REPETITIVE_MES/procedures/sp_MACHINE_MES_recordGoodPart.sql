SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/28/2017
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_recordGoodPart] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@part_ID int = 0,
	@operator_ID int = 0,
	@machine_cycle_part_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @container_ID int = 0
	--DECLARE @machine_cycle_part_ID int = 0
	DECLARE @container_position int = 0, @last_cp INT = 0, @cp INT = 0
	DECLARE @standard_pack_qty int
	DECLARE @part_number varchar(100)
	DECLARE @printer_ID int
	DECLARE @part_order int

	--GET STANDARD PACK QTY
	SELECT @standard_pack_qty = standard_pack_qty, @part_number = part_number
	FROM parts 
	WHERE ID = @part_ID

	--RECORD DISPOSITION
	IF @machine_cycle_part_ID = 0
		SELECT TOP 1 @machine_cycle_part_ID = ID
		FROM machine_cycle_parts
		WHERE ID = (SELECT TOP 1 mcp.ID
				  		FROM machine_cycle_parts mcp
				  			JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
				  			JOIN machines m ON mc.machine_ID = m.ID
				  			JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID AND m.current_tool_ID = tp.tool_ID AND tp.part_ID = @part_ID
				  		WHERE mcp.disposition_time IS NULL
				  			AND mc.machine_ID = @machine_ID
				  		ORDER BY mcp.ID ASC)


    UPDATE machine_cycle_parts
	SET disposition = 'GOOD', disposition_time = GETDATE()
	WHERE ID = @machine_cycle_part_ID

	IF @standard_pack_qty <> 0
		-- RKahle 4/26/2018 - Moved logic to a separate stored proc for adding rework parts to rack easier (requested by ALakia)
		EXEC dbo.sp_MACHINE_MES_packoutAddPartToRack @machine_cycle_part_ID = @machine_cycle_part_ID, @operator_ID = @operator_ID

	--R.Kahle 6/28/2017 - Moved out of std_pack_qty IF block so label always prints even if std pck qty is not defined(0)
	--Print the label
	DECLARE @ret_code int = 0
	EXEC @ret_code = sp_MACHINE_MES_printPartLabel @mcp_ID = @machine_cycle_part_ID

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
