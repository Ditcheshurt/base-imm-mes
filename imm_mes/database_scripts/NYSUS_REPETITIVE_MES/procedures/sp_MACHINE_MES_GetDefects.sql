SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetDefects] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--TOP 5
	SELECT TOP 5 dr.defect_description, dr.ID, COUNT(dl.ID) as defect_count
	FROM machine_defect_log dl
		JOIN machine_defect_reasons dr ON dl.defect_ID = dr.ID
	WHERE dl.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR dl.machine_ID = @machine_ID)
		AND dl.defect_time > GETDATE() - 1
	GROUP BY dr.defect_description, dr.ID
	ORDER BY COUNT(dl.ID) DESC

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
