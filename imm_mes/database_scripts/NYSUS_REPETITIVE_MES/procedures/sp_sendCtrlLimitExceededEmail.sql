SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/23/2018
-- Description:	This will add an entry to the email queue when a control limit is exceeded
-- =============================================
CREATE PROCEDURE [dbo].[sp_sendCtrlLimitExceededEmail] 
	-- Add the parameters for the stored procedure here
	@mma_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @app varchar(50) = 'MEB MOLDING MES'
	DECLARE @from_email varchar(50) = 'MEB_MES@magna.com'
	DECLARE @to_email varchar(50) = ''
	DECLARE @subject varchar(50) = '~~THIS NEEDS TO BE SET~~'
	DECLARE @fname varchar(20), @lname varchar(20)

	DECLARE @cycle_ID int
	DECLARE @mcdp_ID int
	DECLARE @machine_ID int
	DECLARE @tool_ID int
	DECLARE @mtcl_ID int
	DECLARE @data_point_ID int
	DECLARE @created_timestamp datetime
	DECLARE @machine_name varchar(50)
	DECLARE @machine_number varchar(50)
	DECLARE @tool_desc varchar(50)
	DECLARE @data_point_name varchar(50)
	DECLARE @data_point_desc varchar(50)
	DECLARE @data_point_value float
	DECLARE @upper_ctrl_limit float
	DECLARE @lower_ctrl_limit float
	DECLARE @limit_triggered int
	DECLARE @deactivated bit
	DECLARE @severity varchar(50)


    -- Insert statements for procedure here
	SELECT
		 @cycle_ID = mcdp.cycle_ID
		,@mcdp_ID = mcdp.ID
		,@machine_ID = mc.machine_ID
		,@tool_ID = mc.tool_ID
		,@mtcl_ID = mtcl.ID
		,@data_point_ID = mcdp.data_point_ID
		,@created_timestamp = mma.created_timestamp
		,@machine_name = m.machine_name
		,@machine_number = m.machine_number
		,@tool_desc = CONCAT(t.tool_description, ' (',t.short_description,')')
		,@data_point_name = dps.data_point_name
		,@data_point_desc = dps.data_point_desc
		,@data_point_value = mcdp.data_point_value
		,@upper_ctrl_limit = mtcl.upper_ctrl_limit
		,@lower_ctrl_limit = mtcl.lower_ctrl_limit
		,@limit_triggered = mma.limit_triggered
		,@deactivated = mtcl.deactivated
		,@severity = mms.severity_description
	FROM machine_monitor_alerts mma
	JOIN machine_cycle_data_points mcdp ON mcdp.ID = mma.machine_cycle_datapoint_ID
	JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
	JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID --AND mtcl.deactivated = 0
	JOIN machine_monitor_control_limit_severity cls ON cls.machine_tool_ctrl_limit_ID = mtcl.ID
	LEFT JOIN machines m ON m.ID = mc.machine_ID
	LEFT JOIN tools t ON t.ID = mc.tool_ID
	LEFT JOIN machine_monitor_severity mms ON mms.ID = cls.machine_monitor_severity_ID
	LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
	WHERE mma.ID = 11090--@mma_ID

	SET @subject = CONCAT(@severity, ' Severity Control Limit Exceeded - ',@machine_name,' (',@data_point_desc,')')

	DECLARE mmael CURSOR FOR
		SELECT o.first_name, o.last_name, o.email
		FROM machine_monitor_email_list mmel
		LEFT JOIN MES_COMMON.dbo.operators o ON mmel.operator_ID = o.ID
		WHERE mmel.machine_tool_ctrl_limit_ID = @mtcl_ID
		AND mmel.active = 1

	OPEN mmael
	FETCH NEXT FROM mmael INTO @fname, @lname, @to_email
	WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO
				MES_COMMON.dbo.email_queue (application, from_addr, to_addr, subject, body, is_html, queue_time)
			SELECT
				@app AS application,
				@from_email AS from_addr,
				@to_email AS to_addr,
				@severity AS subject,
				'<html>
					<body>
						<h2>'+CONCAT(@data_point_desc,' exceeded ', CASE WHEN @data_point_value > @upper_ctrl_limit THEN 'upper' WHEN @data_point_value < @lower_ctrl_limit THEN 'lower' END, ' control limit')+'</h2>
						<p>Machine: ' + CONCAT(@machine_name, ' (', @machine_number, ')') + '</p>
						<p>Tool: ' + @tool_desc + '</p>
						<p>Parameter: ' + @data_point_desc + '</p>
						<p>Alert Time: ' + CONVERT(char(19), @created_timestamp, 126) + '</p>
					</body>
				</html>' AS body,
				1 AS is_html,
				GETDATE() AS queue_time
			FETCH NEXT FROM mmael INTO @fname, @lname, @to_email
		END
	CLOSE mmael
	DEALLOCATE mmael
/*
*/
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
