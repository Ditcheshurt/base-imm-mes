SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
create PROCEDURE [dbo].[sp_MACHINE_MES_GetShiftProductionSummary_old] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--ACTUAL
	
		SELECT TOP 3 *
		FROM shift_summary
		WHERE 
			system_ID = 1
			AND machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
			AND (@machine_ID = 0 OR machine_ID = @machine_ID)
			AND the_date > DATEADD(DAY, -2, GETDATE())
		ORDER BY ID DESC
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
