SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Dan Bick
-- Create date: 4-25-2017
-- Description:	base on machine ip will return the 
-- list of tools, groups, and part possible to select
-- =============================================
CREATE PROCEDURE [dbo].[sp_MES_setActiveParts] 
	-- Add the parameters for the stored procedure here
	@machine_id int = 0,
	@tool_id int = 0,
	@part_id_list varchar(500),
	@operator int = 0,
	@id_true int = 2 -- this is from setting if it is from the button pressing = 1 vs scan = 0 vs unknown = 2

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- R.Kahle 10/31/2018; Clear the machines current part queue
	EXEC dbo.sp_MACHINE_MES_CUSTOM_clearMachinePartQueue
		@machine_ID = @machine_id,
		@operator_ID = @operator

EXEC [dbo].[sp_MOLDING_MES_DeactivateParts] @machineID = @machine_id


DECLARE @t TABLE (ID int, items varchar(50))
DECLARE @part_id int, @tp_id int, @items varchar(50), @order int


INSERT INTO @t
	SELECT ID, items
	FROM dbo.Split(@part_id_list, '-');
 
/* Will be added when we figure out the scan from the part or tool side.
IF @id_true = 0 
	BEGIN
		SET @id_true = 0
		SELECT @tool_id = tp.tool_ID
		FROM tool_parts tp
		INNER JOIN parts p
		ON tp.part_ID = p.ID
		WHERE p.part_number = (SELECT TOP 1 items FROM @t WHERE ID = 1)
	END
*/

-- If any piece of info is missing it will skip the rest of the code
IF @tool_id =0 OR @machine_id = 0 OR @operator = 0
	Begin
	SET @id_true = 2
	SELECT 'FAILED',  @tool_id, @machine_id, @operator
	END
-- check that nothing was missing


IF @id_true != 2
	BEGIN
		UPDATE machines SET current_tool_ID = @tool_id WHERE id = @machine_id

		 DECLARE loopcur CURSOR FOR	
			SELECT ID, items 
			FROM @t;

		OPEN loopcur;
		FETCH NEXT FROM loopcur INTO  @order, @items

		WHILE @@FETCH_STATUS = 0  
		BEGIN 
			IF @id_true = 0
				BEGIN
					SELECT @part_id = p.ID, @tp_id = tp.ID
						FROM parts p
						INNER JOIN tool_parts tp ON tp.part_ID = p.ID
						WHERE p.part_number = @items
				END
			ELSE
				BEGIN
					SET @part_id = @items
				END
				/*SELECT @machine_id as machine_id
				   ,@tool_id as tool_id
				   ,@part_id as part_id
				   ,@order as o
				   ,@operator as op
				   ,GETDATE() as sd
				   ,NULL as ed
				   ,1 as act*/
			INSERT INTO [dbo].[machine_active_parts]
				   ([machine_id]
				   ,[tool_id]
				   ,[parts_id]
				   ,[cavity_order]
				   ,[operator_id]
				   ,[start_timestamp]
				   ,[end_timestamp]
				   ,[is_active])
			 VALUES
				   (@machine_id
				   ,@tool_id
				   ,@part_id
				   ,@order
				   ,@operator
				   ,GETDATE()
				   ,NULL
				   ,1)

			SELECT @tp_id = tp.ID
			FROM parts p
			INNER JOIN tool_parts tp ON tp.part_ID = p.ID
			WHERE p.ID = @items

			UPDATE
				machine_cycle_parts
			SET disposition = 'OLD', disposition_time = GETDATE()
			WHERE  disposition_time IS NULL
				AND tool_part_ID = @tp_id

			FETCH NEXT FROM loopcur INTO  @order, @items
		END
		CLOSE loopcur
		DEALLOCATE loopcur	
	END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
