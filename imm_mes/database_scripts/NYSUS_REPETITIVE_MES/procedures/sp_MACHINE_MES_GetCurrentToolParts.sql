SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetCurrentToolParts] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	--SELECT p.ID, p.part_desc, p.part_number, p.standard_pack_qty, tp.part_order, 
	--	SUM(CASE WHEN mcp.disposition_time IS NULL THEN 1 ELSE 0 END) as current_queue, 
	--	COUNT(c.ID) as current_rack_count,
	--	(CASE WHEN (SUM(c.ID)/COUNT(c.ID)) IS NULL THEN 0 ELSE SUM(c.ID)/COUNT(c.ID) END) as container_ID --added 5/3/2017 by R.Kahle so we can print the current rack ID
	--FROM parts p
	--	JOIN machine_active_parts map ON p.ID = map.parts_id AND map.is_active = 1 AND map.end_timestamp IS NULL
	--	JOIN tool_parts tp ON p.ID = tp.part_ID
	--	JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID 
	--		AND mt.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP) 
	--		AND (@machine_ID = 0 OR mt.machine_ID = @machine_ID)
	--	JOIN machines m ON mt.machine_ID = m.ID AND m.current_tool_ID = tp.tool_ID

	--	LEFT JOIN machine_cycle_parts mcp ON mcp.tool_part_ID = tp.ID
	--	LEFT JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID AND mc.machine_ID = m.ID

	--	LEFT OUTER JOIN container_parts cp ON mcp.ID = cp.machine_cycle_part_ID
	--	LEFT OUTER JOIN containers c ON cp.container_ID = c.ID AND c.end_time IS NULL
	--GROUP BY p.ID, p.part_desc, p.part_number, p.standard_pack_qty, tp.part_order
	--ORDER BY tp.part_order

	SELECT p.ID, p.part_desc, p.part_number, p.standard_pack_qty, p.part_image, tp.part_order, p.print_template_ID
	FROM parts p
		JOIN machine_active_parts map ON p.ID = map.parts_id AND map.is_active = 1 AND map.end_timestamp IS NULL
		JOIN tool_parts tp ON p.ID = tp.part_ID
		JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID 
			AND mt.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP) 
			AND (@machine_ID = 0 OR mt.machine_ID = @machine_ID)
		JOIN machines m ON mt.machine_ID = m.ID AND m.current_tool_ID = tp.tool_ID
	GROUP BY p.ID, p.part_desc, p.part_number, p.standard_pack_qty, p.part_image, tp.part_order, p.print_template_ID
	ORDER BY /*tp.part_order*/ p.part_number

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
