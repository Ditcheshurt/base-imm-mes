SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Daviduk
-- Create date: 9/2/15
-- Description:	record lot for cycle complete
-- requires #Temp table from calling SP
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleComplete_lots] 
	@machine_ID int,
	@cycle_ID int,
	@current_tool_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	-- steps
	-- 1. record a lot_machine_cycle_parts the cycle
	-- 2. if lot is at max set it to the new lot if it exists
	-- 3. write tags to #Temp from calling SP
		
	DECLARE @start_next_lot int

	--4/6/2016 - SS - CHANGED THESE TO BE SUB-QUERIES....OTHERWISE THEY ONLY PULL ONE LOT MATERIAL TYPE...NOT GOOD!
	PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> INSERTING [lot_machine_cycle_parts] cycle_ID = ' + CONVERT(VARCHAR, @cycle_ID)
		
	INSERT INTO [dbo].[lot_machine_cycle_parts] (
		[machine_cycle_part_ID],
		[lot_material_type_ID],
		[lot_number]
	) 							
	SELECT 
		cp.ID, 
		lm.lot_material_type_ID, 
		lmt.current_lot
	FROM machine_cycle_parts cp
		JOIN machine_cycles mc ON cp.cycle_ID = mc.ID
		JOIN lot_machines lm ON mc.machine_ID = lm.machine_ID
		JOIN lot_material_types lmt ON lm.lot_material_type_ID = lmt.ID
		JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lmt.ID
		JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID
	WHERE 
		cp.cycle_ID = @cycle_ID	
		AND lm.machine_ID = @machine_ID

	-- end if nothing was inserted	
	IF (@@ROWCOUNT = 0)
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> NO ROWS INSERTED ENDING SP'
		RETURN	
	END
	ELSE
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> INSERTED [lot_machine_cycle_parts] ' + CONVERT(VARCHAR, @@ROWCOUNT) + ' ROWS'
	END

	-- if the lot has hit its max and has a next_lot set use it
	SELECT 
		@start_next_lot = COUNT(lmt.ID)
	FROM lot_machines lm 
		JOIN lot_material_types lmt ON lm.lot_material_type_ID = lmt.ID 
		JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
		JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
	WHERE 
		lmt.next_lot IS NOT NULL 
		AND lm.machine_ID = @machine_ID
		AND tp.tool_ID = @current_tool_ID
		AND lmt.lot_parts_used >= lmt.parts_per_lot		-- added 10/12/16 SD check parts used >= parts per lot

	IF (@start_next_lot > 0) 
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> UPDATING LOT TO NEW LOT'
		UPDATE lot_material_types SET 
			lot_parts_used = 0,
			current_lot = next_lot,
			parts_per_lot = parts_per_next_lot,
			--parts_per_lot_maximum_offset = parts_per_next_lot_maximum_offset,
			--parts_per_lot_warning_offset = parts_per_next_lot_maximum_offset,
			next_lot = NULL,
			parts_per_next_lot = NULL,
			parts_per_next_lot_maximum_offset = NULL,
			parts_per_next_lot_warning_offset = NULL
		FROM lot_machines lm
			JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
			JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
		WHERE 
			lm.machine_ID = @machine_ID
			AND lot_material_types.ID = lm.lot_material_type_ID
			AND tp.tool_ID = @current_tool_ID
			AND next_lot IS NOT NULL -- added 10/12/16 prevent null lot 			
	END
	ELSE
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> INCREMENTING LOT' 
		UPDATE lot_material_types SET 
			lot_parts_used = ISNULL(lot_parts_used, 0) + ltp.qty_per_part
		FROM lot_machines lm
			JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
			JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
		WHERE 
			lm.machine_ID = @machine_ID
			AND lot_material_types.ID = lm.lot_material_type_ID
			AND tp.tool_ID = @current_tool_ID
	END

	IF object_id('tempdb..#Temp') IS NULL
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> NOT WRITING PLC TAGS, NO #Temp TABLE FROM CALLER'
		RETURN
	END
	ELSE
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> WRITE PLC TAGS TO #Temp'
		-- write the plc tags for the lot
		DECLARE @lot_plc_ip VARCHAR(MAX);
		DECLARE @lot_plc_tag VARCHAR(MAX);
		DECLARE @lot_parts_used int;
		DECLARE @parts_per_lot int;
		DECLARE @parts_per_lot_warning_offset int;
		DECLARE @parts_per_lot_maximum_offset int;

		DECLARE loopyLot224 CURSOR FOR
			SELECT 
				p.ip_address, t.name, mt.lot_parts_used, mt.parts_per_lot, mt.parts_per_lot_warning_offset, mt.parts_per_lot_maximum_offset	
			FROM [dbo].[lot_material_types] mt
				JOIN lot_machines lm ON lm.lot_material_type_ID = mt.ID
				JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
				JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
				JOIN NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON t.ID = mt.plc_tag_ID
				JOIN NYSUS_PLC_POLLER.dbo.nysus_plcs p ON p.ID = t.plc_ID
			WHERE lm.machine_ID = @machine_ID
				AND tp.tool_ID = @current_tool_ID

		OPEN loopyLot224

		FETCH NEXT FROM loopyLot224 INTO @lot_plc_ip, @lot_plc_tag, @lot_parts_used, @parts_per_lot, @parts_per_lot_warning_offset, @parts_per_lot_maximum_offset
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @lot_plc_ip IS NOT NULL
			BEGIN
				IF @lot_parts_used >= @parts_per_lot + @parts_per_lot_maximum_offset
				BEGIN
					PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> PLC ' + @lot_plc_ip + ' WRITE ' + @lot_plc_tag + ' VALUE 3'
					INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
					VALUES ('DATA RECVD TAG', 'WRITE', @lot_plc_ip, @lot_plc_tag, 'DINT', '3');
				END
				ELSE IF @lot_parts_used >= @parts_per_lot
				BEGIN
					PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> PLC ' + @lot_plc_ip + ' WRITE ' + @lot_plc_tag + ' VALUE 2'
					INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
					VALUES ('DATA RECVD TAG', 'WRITE', @lot_plc_ip, @lot_plc_tag, 'DINT', '2');
				END
				ELSE IF @lot_parts_used >= @parts_per_lot - @parts_per_lot_warning_offset
				BEGIN
					PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> PLC ' + @lot_plc_ip + ' WRITE ' + @lot_plc_tag + ' VALUE 1'
					INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
					VALUES ('DATA RECVD TAG', 'WRITE', @lot_plc_ip, @lot_plc_tag, 'DINT', '1');
				END
				ELSE 
				BEGIN
					PRINT 'sp_MACHINE_MES_RecordCycleComplete_lots -> PLC ' + @lot_plc_ip + ' WRITE ' + @lot_plc_tag + ' VALUE 0'
					INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
					VALUES ('DATA RECVD TAG', 'WRITE', @lot_plc_ip, @lot_plc_tag, 'DINT', '0');
				END				
			END
			FETCH NEXT FROM loopyLot224 INTO @lot_plc_ip, @lot_plc_tag, @lot_parts_used, @parts_per_lot, @parts_per_lot_warning_offset, @parts_per_lot_maximum_offset
		END
		CLOSE loopyLot224;
		DEALLOCATE loopyLot224;
	END

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
