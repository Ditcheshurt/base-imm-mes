SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Rob Kahle
-- Create date: 10/19/2018
-- Description:	This will set a PLC value for a machine
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CUSTOM_setPLCTagValue]
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tag_type varchar(10) = 'BOOL',
	@tag_name varchar(255) = 'NEED_THIS',
	@value varchar(30) = 'FALSE',
	@cycle_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @web_server varchar(50) = '10.68.160.79'	-- Hard coded for now
	DECLARE @plc_ip varchar(20) = '127.0.0.1'			-- Unreachable value
	DECLARE @plc_path varchar(1) = '0'
	DECLARE @data_type varchar(10) = @tag_type

	DECLARE @result varchar(500) = ''

    -- Insert statements for procedure here
	SELECT @plc_ip = plc.ip_address
	FROM NYSUS_REPETITIVE_MES.dbo.machines m
	JOIN NYSUS_PLC_POLLER.dbo.nysus_plcs plc ON plc.ID = m.PLC_ID
	WHERE m.ID = @machine_ID

	SELECT @result = MES_COMMON.dbo.fn_setPLCValue (
		 @web_server
		,@plc_ip
		,@tag_name
		,@plc_path
		,@data_type
		,@value
	)

	INSERT INTO dbo.loggy (
		 log_time
		,log_msg
	) VALUES (
		 GETDATE()
		,CONCAT(
			 'Machine_monitor - Set PLC tag value --> machine_ID: ',CAST(@machine_ID AS varchar)
			,', cycle_ID: ',@cycle_ID
			,', machine_plc: ',@plc_ip
			,', tagname: ',@tag_name
			,', setPLCValue result: ',@result
		)
	)

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
