SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetOpenDowntime] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--TOP 5
	SELECT TOP 10 dl.ID, m.machine_name, dl.change_time, dl.time_in_state, DATEDIFF(SECOND, dl.change_time, GETDATE()) / 60.0 as time_since_started, dr.reason_description
	FROM machine_downtime_log dl
		JOIN machines m ON dl.machine_ID = m.ID
		LEFT OUTER JOIN machine_downtime_reasons dr ON dl.reason_ID = dr.ID
	WHERE dl.machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR dl.machine_ID = @machine_ID)
		AND dl.up = 0
		AND (dl.reason_ID IS NULL OR dl.time_in_state IS NULL)
	ORDER BY dl.change_time DESC

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
