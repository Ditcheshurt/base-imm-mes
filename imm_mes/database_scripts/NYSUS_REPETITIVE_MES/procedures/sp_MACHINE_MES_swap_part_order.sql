SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [dbo].[sp_MACHINE_MES_swap_part_order] 
    -- Add the parameters for the stored procedure here
    @tool_ID int = 0,
	@part_ID int = 0, 
    @direction varchar(50) = ''
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
 
    DECLARE @cur_order int
    DECLARE @swap_tool_part_ID int, @swap_part_order int
 
    DECLARE @line_ID int, @station_ID int, @part_order int, @part_number varchar(50), @current_rev_level varchar(10)
 
    SELECT @cur_order = part_order
    FROM tool_parts
    WHERE tool_ID = @tool_ID 
		AND part_ID = @part_ID
 
 
    IF @direction = 'DOWN'
        BEGIN
            SELECT TOP 1 @swap_tool_part_ID = ID, @swap_part_order = part_order
            FROM tool_parts 
            WHERE tool_ID = @tool_ID
                AND part_order > @cur_order 
            ORDER BY part_order ASC
        END
     
    IF @direction = 'UP'
        BEGIN
            SELECT TOP 1 @swap_tool_part_ID = ID, @swap_part_order = part_order
            FROM tool_parts 
            WHERE tool_ID = @tool_ID
                AND part_order < @cur_order
            ORDER BY part_order DESC
        END
 
    UPDATE tool_parts
    SET part_order = @swap_part_order
    WHERE tool_ID = @tool_ID
		AND part_ID = @part_ID
 
    UPDATE tool_parts 
    SET part_order = @cur_order 
    WHERE ID = @swap_tool_part_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
