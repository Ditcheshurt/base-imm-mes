SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Daviduk
-- Create date: 4/29/16
-- Description:	perform the gaguing logic on cycle complete
-- requires #temp from caller
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleComplete_gauging] 
	@machine_ID int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	-- steps
	-- 1. get the tool parts for machine and loop through them incrementing gauging for each
	-- 2. WRITE the PLC tags

	DECLARE @tool_part_ID int

	PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> UPDATING GAUGING COUNT FOR MACHINES TOOL_PARTS' 
	DECLARE loopyGauge244 CURSOR FOR
		SELECT 
			tp.ID
		FROM 
			tool_parts tp
		JOIN 
			machine_tools mt ON tp.tool_ID = mt.tool_ID
		JOIN 
			machines m ON mt.tool_ID = m.current_tool_ID
		WHERE
			m.ID = @machine_ID

	OPEN loopyGauge244

	FETCH NEXT FROM loopyGauge244 INTO @tool_part_ID
		
	WHILE @@FETCH_STATUS = 0

	BEGIN
		UPDATE 
			[dbo].[gauge_material_types] 
		SET 
			gauge_parts_used = gauge_parts_used + 1 
		WHERE 
			tool_part_ID = @tool_part_ID
			AND active = 1

		FETCH NEXT FROM loopyGauge244 INTO @tool_part_ID
	END

	CLOSE loopyGauge244;
	DEALLOCATE loopyGauge244;

	-- write plc tags
	IF object_id('tempdb..#Temp') IS NULL
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> NOT WRITING PLC TAGS, NO #Temp TABLE FROM CALLER'
		RETURN
	END
	ELSE
	BEGIN
		PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> WRITE PLC TAGS TO #Temp'

		DECLARE @gauge_plc_ip VARCHAR(MAX);
		DECLARE @gauge_plc_tag VARCHAR(MAX);
		DECLARE @gauge_parts_used int;
		DECLARE @parts_per_gauging int;
		DECLARE @parts_per_gauging_warning_offset int;
		DECLARE @parts_per_gauging_maximum_offset int;		

		DECLARE loopyGauge224 CURSOR FOR
			SELECT 
				p.ip_address, 
				t.name, 
				gmt.gauge_parts_used, 
				gmt.parts_per_gauging, 
				gmt.parts_per_gauging_warning_offset, 
				gmt.parts_per_gauging_maximum_offset	
			FROM 
				[dbo].[gauge_material_types] gmt
			JOIN 
				NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON t.ID = gmt.plc_tag_ID
			JOIN 
				NYSUS_PLC_POLLER.dbo.nysus_plcs p ON p.ID = t.plc_ID
			WHERE 
				gmt.tool_part_ID = @tool_part_ID
				AND gmt.active = 1

		OPEN loopyGauge224

		FETCH NEXT FROM loopyGauge224 INTO @gauge_plc_ip, @gauge_plc_tag, @gauge_parts_used, @parts_per_gauging, @parts_per_gauging_warning_offset, @parts_per_gauging_maximum_offset
		WHILE @@FETCH_STATUS = 0
		BEGIN

			IF @gauge_plc_ip IS NOT NULL
				BEGIN
					IF @gauge_parts_used >= @parts_per_gauging + @parts_per_gauging_maximum_offset
					BEGIN
						PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> PLC ' + @gauge_plc_ip + ' WRITE ' + @gauge_plc_tag +  'VALUE 3'
						INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
						VALUES ('DATA RECVD TAG', 'WRITE', @gauge_plc_ip, @gauge_plc_tag, 'DINT', '3');
					END
					ELSE IF @gauge_parts_used >= @parts_per_gauging
					BEGIN
						PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> PLC ' + @gauge_plc_ip + ' WRITE ' + @gauge_plc_tag +  'VALUE 2'
						INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
						VALUES ('DATA RECVD TAG', 'WRITE', @gauge_plc_ip, @gauge_plc_tag, 'DINT', '2');
					END
					ELSE IF @gauge_parts_used >= @parts_per_gauging - @parts_per_gauging_warning_offset
					BEGIN
						PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> PLC ' + @gauge_plc_ip + ' WRITE ' + @gauge_plc_tag +  'VALUE 1'
						INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
						VALUES ('DATA RECVD TAG', 'WRITE', @gauge_plc_ip, @gauge_plc_tag, 'DINT', '1');
					END
					ELSE 
					BEGIN
						PRINT 'sp_MACHINE_MES_RecordCycleComplete_gauging -> PLC ' + @gauge_plc_ip + ' WRITE ' + @gauge_plc_tag +  'VALUE 0'
						INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
						VALUES ('DATA RECVD TAG', 'WRITE', @gauge_plc_ip, @gauge_plc_tag, 'DINT', '0');
					END
				END
			FETCH NEXT FROM loopyGauge224 INTO @gauge_plc_ip, @gauge_plc_tag, @gauge_parts_used, @parts_per_gauging, @parts_per_gauging_warning_offset, @parts_per_gauging_maximum_offset
		END
		CLOSE loopyGauge224;
		DEALLOCATE loopyGauge224;
	END

END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
