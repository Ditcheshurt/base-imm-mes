SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 7/31/2018
-- Description:	This will start a tool change (tracking)
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_resumeToolChange] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tool_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @downtime_ID int
	DECLARE @tool_change_dt int
	DECLARE @tool_change_ID TABLE (ID int)

	-- Get current open (paused) tool change record
	SELECT @downtime_ID = mdl.ID, @tool_change_dt = mdl.tool_change_ID
	FROM machine_downtime_log mdl
	WHERE mdl.machine_ID = @machine_ID
	AND mdl.tool_ID = @tool_ID
	AND mdl.time_in_state IS NULL
	AND mdl.tool_change_ID IS NOT NULL
	AND mdl.is_tool_change_pause = 1
				
	-- And close it as undispositioned
	IF (ISNULL(@downtime_ID, 0) > 0)
		UPDATE machine_downtime_log
		SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0
		WHERE ID = @downtime_ID

	-- Create an open tool change record
	INSERT INTO machine_downtime_log (
		 change_time
		,up
		,machine_ID
		,tool_id
		,reason_ID
		,tool_change_ID
		,is_tool_change_pause
	) 
	OUTPUT INSERTED.ID INTO @tool_change_ID
	VALUES (
		 GETDATE()
		,0
		,@machine_ID
		,@tool_ID
		,4
		,@tool_change_dt
		,0
	)

	-- Return the created row ID
	SELECT ID FROM @tool_change_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
