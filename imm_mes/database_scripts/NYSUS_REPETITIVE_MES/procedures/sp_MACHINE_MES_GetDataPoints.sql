SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 4/26/2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetDataPoints] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @tool_part_ID int = 0

	SELECT @tool_part_ID = ID
	FROM tool_parts tp
	WHERE tool_ID = (SELECT current_tool_ID FROM machines WHERE ID = @machine_ID)


	SELECT i.ID, gmt.description, i.description as instruction_description, 0 as auto_gauge,
		ISNULL(i.tolerance_target, 0) as tolerance_target, ISNULL(tolerance_delta, 0) as tolerance_delta,
		ISNULL(i.xbar_ucl, 0) as xbar_ucl, ISNULL(i.xbar_lcl, 0) as xbar_lcl, ISNULL(i.range_ucl, 0) as range_ucl, ISNULL(i.range_lcl, 0) as  range_lcl
	FROM gauge_material_types gmt
		JOIN gauge_material_types_instructions i ON gmt.ID = i.gauge_material_type_ID
	WHERE gmt.tool_part_ID = @tool_part_ID
	UNION
	SELECT 0 - ID, 'AUTO', data_point_column, 1,
		tolerance_target, tolerance_delta,
		xbar_ucl, xbar_lcl, range_ucl, range_lcl
	FROM spc_autogauge_data_points
	ORDER BY gmt.description ASC, i.description

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
