SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/2/2018
-- Description:	This will take a formatted string of control limits and apply them to a machine/tool
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_submitNewMachineToolCtrlLimits] 
	-- Add the parameters for the stored procedure here
	@cl_str varchar(8000) = '', 
	@machine_ID int = 0,
	@tool_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @t1 TABLE (ID int, data_point_ID int, target_value float, upper_ctrl_limit float, lower_ctrl_limit float)

	-- Format input string into usable temp table structure
	INSERT INTO @t1 (ID, data_point_ID, target_value, upper_ctrl_limit, lower_ctrl_limit)
		SELECT CASE WHEN p2.ctrl_limit_ID = 'NULL' THEN NULL ELSE p2.ctrl_limit_ID END, p2.data_point_ID, TRY_CONVERT(float,p2.target_value), TRY_CONVERT(float,p2.upper_ctrl_limit), TRY_CONVERT(float,p2.lower_ctrl_limit)
		FROM (
			SELECT p.G1, p.[1] AS k, p.[2] AS v
			FROM (
				SELECT s1.ID AS G1, s2.ID AS G2, s3.ID AS G3,s3.items
				FROM dbo.Split(@cl_str,'|') s1
				CROSS APPLY dbo.Split(s1.items,',') s2
				CROSS APPLY dbo.Split(s2.items,':') s3
			) t1
			PIVOT (
				MAX([items])
				FOR G3 IN ([1], [2])
			) AS p
		) t2
		PIVOT (
			MAX([v])
			FOR k IN ([ctrl_limit_ID],[data_point_ID],[target_value],[upper_ctrl_limit],[lower_ctrl_limit])
		) as p2

	-- Do updates for target value, upper and lower control limit
	UPDATE t2
	SET t2.upper_ctrl_limit = updt.upper_ctrl_limit, t2.lower_ctrl_limit = updt.lower_ctrl_limit, t2.target_value = updt.target_value
	FROM dbo.machine_tool_ctrl_limits t2
	INNER JOIN (
		SELECT t1.*
		FROM dbo.machine_tool_ctrl_limits mtcl
		JOIN @t1 t1 ON t1.ID = mtcl.ID AND t1.data_point_ID = mtcl.data_point_ID
		WHERE t1.lower_ctrl_limit <> mtcl.lower_ctrl_limit OR t1.upper_ctrl_limit <> mtcl.upper_ctrl_limit AND mtcl.deactivated = 0
	) updt ON updt.ID = t2.ID
	WHERE t2.machine_ID = @machine_ID AND t2.tool_ID = @tool_ID

	-- Do insert
	INSERT INTO dbo.machine_tool_ctrl_limits (machine_ID, tool_ID, data_point_ID, target_value, upper_ctrl_limit, lower_ctrl_limit) 
		SELECT @machine_ID, @tool_ID, t1.data_point_ID, t1.target_value, t1.upper_ctrl_limit, t1.lower_ctrl_limit
		FROM @t1 t1 WHERE t1.ID IS NULL AND (t1.lower_ctrl_limit IS NOT NULL OR t1.upper_ctrl_limit IS NOT NULL)

	-- Do deactivates
	--SELECT mtcl.*
	UPDATE mtcl
	SET mtcl.deactivated = 1, mtcl.deactivated_by = @operator_ID, mtcl.deactivated_timestamp = GETDATE()
	FROM dbo.machine_tool_ctrl_limits mtcl
	INNER JOIN (
		SELECT t1.ID, t1.data_point_ID, t1.target_value, t1.upper_ctrl_limit, t1.lower_ctrl_limit
		FROM @t1 t1 WHERE t1.ID IS NOT NULL AND (t1.lower_ctrl_limit IS NULL AND t1.upper_ctrl_limit IS NULL)
	) dr ON dr.ID = mtcl.ID AND dr.data_point_ID = mtcl.data_point_ID AND mtcl.machine_ID = @machine_ID AND mtcl.tool_ID = @tool_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
