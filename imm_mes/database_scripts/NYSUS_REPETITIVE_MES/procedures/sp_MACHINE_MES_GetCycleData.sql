SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/21/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetCycleData] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @tool_ID int, @system_ID int
	DECLARE @last_cycle_time float, @avg_cycle_time float, @low_cycle_time float
	DECLARE @target_cycle_time float

	SELECT @system_ID = m.system_ID, @tool_ID = m.current_tool_ID, @machine_ID = m.ID, @target_cycle_time = mt.target_cycle_time
	FROM machines m
		LEFT OUTER JOIN tools t ON m.current_tool_ID = t.ID
		LEFT OUTER JOIN machine_tools mt ON m.ID = mt.machine_ID AND t.ID = mt.tool_ID
	WHERE m.station_IP_address = @my_IP
		AND (@machine_ID = 0 OR m.ID = @machine_ID)

	SELECT TOP 1 @last_cycle_time = cycle_duration
	FROM machine_cycles 
	WHERE machine_ID = @machine_ID 
		AND tool_ID = @tool_ID 
	ORDER BY ID DESC

	SELECT @avg_cycle_time = AVG(cycle_duration)
	FROM machine_cycles
	WHERE machine_ID = @machine_ID
		AND tool_ID = @tool_ID
		AND cycle_time > DATEADD(DAY, -2, GETDATE())
		AND dbo.getProdDate(cycle_time) = dbo.getProdDate(GETDATE())
		AND dbo.getShift(@system_ID, cycle_time) = dbo.getShift(@system_ID,GETDATE())

	SELECT @low_cycle_time = MIN(cycle_duration)
	FROM machine_cycles 
	WHERE machine_ID = @machine_ID 
		AND tool_ID = @tool_ID
		AND cycle_time > DATEADD(DAY, -2, GETDATE())
		AND dbo.getProdDate(cycle_time) = dbo.getProdDate(GETDATE())
		AND dbo.getShift(@system_ID, cycle_time) = dbo.getShift(@system_ID, GETDATE())

	SELECT ISNULL(@last_cycle_time, 0) as last_cycle_time,
		   ISNULL(@avg_cycle_time, 0) as avg_cycle_time,
		   ISNULL(@target_cycle_time, 0) as target_cycle_time,
		   ISNULL(@low_cycle_time, 0) as low_cycle_time
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
