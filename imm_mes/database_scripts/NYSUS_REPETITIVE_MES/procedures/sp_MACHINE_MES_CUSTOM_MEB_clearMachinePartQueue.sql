SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 10/31/2018
-- Description:	This will clear the part queue for a machines currently configured parts
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_CUSTOM_clearMachinePartQueue] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @clear_queue bit = 1

	DECLARE @defect_ID int = 35, @defect_origin_ID int = 1, @disposition varchar(20) = 'SCRAP'
	DECLARE @t TABLE (machine_ID int, tool_ID int, part_ID int, defect_time datetime, operator_ID int, defect_ID int, defect_origin_ID int, disposition varchar(20), serial_number varchar(50))

	IF @clear_queue = 1 BEGIN
		--Clear the current queue
		INSERT INTO @t
			SELECT DISTINCT
				@machine_ID AS machine_ID,
				tp.tool_ID,
				tp.part_ID,
				GETDATE() AS defect_time,
				@operator_ID AS operator_ID,
				@defect_ID AS defect_ID,
				@defect_origin_ID AS defect_origin_ID,
				@disposition AS disposition,
				mcp.serial_number
			FROM machine_active_parts map
			JOIN tool_parts tp ON tp.tool_ID = map.tool_id AND tp.part_ID = map.parts_id
			JOIN machine_cycle_parts mcp ON mcp.tool_part_ID = tp.ID AND mcp.disposition_time IS NULL
			WHERE map.end_timestamp IS NULL
				AND map.is_active = 1
				AND map.machine_ID = @machine_ID

		UPDATE mcp
		SET mcp.disposition = 'NO_LABEL', mcp.disposition_time = defect_time
		FROM machine_cycle_parts mcp
		JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
		JOIN @t t1 ON t1.serial_number = mcp.serial_number
			AND t1.tool_ID = tp.tool_ID
			AND t1.part_ID = tp.part_ID
		WHERE mcp.disposition_time IS NULL

		INSERT INTO machine_defect_log (machine_ID, tool_ID, part_ID, defect_time, operator_ID, defect_ID, defect_origin_ID, disposition, serial_number)
			SELECT
				machine_ID,
				tool_ID,
				part_ID,
				defect_time,
				operator_ID,
				defect_ID,
				defect_origin_ID,
				disposition,
				serial_number
			FROM @t
	END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
