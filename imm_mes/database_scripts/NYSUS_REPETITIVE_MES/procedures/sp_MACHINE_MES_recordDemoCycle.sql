SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 7/24/2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_recordDemoCycle] 
	-- Add the parameters for the stored procedure here
	@intolerance bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @machine_ID int = 50000

	DECLARE @tool_ID int, @cycle_ID int, @last_cycle_sequence int = 0, 
		@last_run_ID int, @last_cycle_time datetime, @last_tool_ID int,
		@operator_ID int

	--GET ALL OF THE INFO WE NEED
	SELECT @tool_ID = current_tool_ID, @operator_ID = current_operator_ID, @last_cycle_time = last_cycle_time
	FROM machines 
	WHERE ID = @machine_ID

	SELECT TOP 1 @last_cycle_sequence = cycle_sequence, @last_tool_ID = tool_ID, @last_run_ID = run_ID
	FROM machine_cycles
	WHERE machine_ID = @machine_ID
	ORDER BY ID DESC
		
	--INCREMENT THE CYCLE SEQUENCE AND RUN ID (IF NECESSARY)
	IF @last_tool_ID = @tool_ID
		BEGIN
			SET @last_cycle_sequence = @last_cycle_sequence + 1
		END
	ELSE
		BEGIN
			SET @last_cycle_sequence = 1
			SET @last_run_ID = @last_run_ID + 1
		END

	--INSERT THE CYCLE
    INSERT INTO machine_cycles (machine_ID, tool_ID, cycle_time, cycle_sequence, run_ID, operator_ID, cycle_duration) VALUES (
		@machine_ID, 
		@tool_ID, 
		GETDATE(),
		@last_cycle_sequence,
		@last_run_ID,
		@operator_ID,
		ISNULL(CAST(DATEDIFF(SECOND, @last_cycle_time, GETDATE()) as float) / 60.0, 0)
	)

	SET @cycle_ID = @@IDENTITY

	--INSERT THE PARTS
	INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, serial_number, insert_time) 							
	SELECT @cycle_ID, tp.ID, dbo.fn_getNextSerial(tp.ID), GETDATE()
	FROM tool_parts tp
		JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID AND mt.machine_ID = @machine_ID
		JOIN machines m ON m.ID = @machine_ID AND mt.tool_ID = m.current_tool_ID

	--GENERATE CYCLE DATA

	--UPDATE MACHINES
	UPDATE machines 
	SET last_cycle_time = GETDATE()
	WHERE ID = @machine_ID
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
