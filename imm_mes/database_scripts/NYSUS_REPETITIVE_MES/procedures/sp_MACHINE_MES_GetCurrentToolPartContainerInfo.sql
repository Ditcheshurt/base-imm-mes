SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetCurrentToolPartContainerInfo] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	SELECT cp.*, tp.part_ID
	FROM machine_cycle_parts mcp
		JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID 
		JOIN container_parts cp ON mcp.ID = cp.machine_cycle_part_ID
		JOIN containers c ON cp.container_ID = c.ID
		JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID AND tp.part_ID IN (
				SELECT parts_id 
				FROM machine_active_parts 
				WHERE end_timestamp IS NULL AND 
					machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
					AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
				)
	WHERE mc.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
		AND c.end_time IS NULL

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
