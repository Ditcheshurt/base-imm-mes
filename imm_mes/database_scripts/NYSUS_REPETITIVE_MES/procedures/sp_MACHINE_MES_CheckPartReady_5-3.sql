SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 10/2/15
-- Description:	triggered when plc tag shows part ready, validates wip part with barcode exists
-- =============================================
create PROCEDURE [dbo].[sp_MACHINE_MES_CheckPartReady_5-3] 
	@tag_ID varchar(10),
	@tag_value varchar(30),
	@tag_desc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value,	'CheckPartReady'

	IF @tag_value = 'False' Or @tag_value = '0' 
		RETURN
	
	DECLARE @tag_name varchar(100)
	DECLARE @machine_ID int, @op_order int, @group_ID int
	DECLARE @min_op_order int
	DECLARE @parts_per_cycle int
	DECLARE @barcode_data varchar(100)
	DECLARE @barcode_empty_error int = 1001;
	DECLARE @wip_part_not_found_error int = 1002;
	DECLARE @wip_part_not_ready_error int = 1003;
	DECLARE @duplicate_part_error int = 1004;
	DECLARE @plc_clear_fail_status int = 0;
	DECLARE @plc_tag varchar(50);
	DECLARE @plc_ip varchar(50), @plc_ID int;
	DECLARE @i int = 0;
	DECLARE @webserver varchar(50) = 'localhost'
	DECLARE @part_ready_tag_name varchar(100) = 'PART_READY_TO_MES'
	DECLARE @data_rcvd_tag_name varchar(100) = 'DATA_RCVD_TO_PLC'
	DECLARE @part_status_tag_name varchar(100) = 'PART_STATUS_TO_PLC'
	DECLARE @write_tag_name varchar(100)
	DECLARE @serial bigint = 0
	DECLARE @has_prior_op_group bit = 0

	SELECT @tag_desc = description, @plc_ID = plc_ID, @tag_name = name
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE ID = @tag_ID

	-- Create a temp result table
	CREATE TABLE #Temp ( result varchar(50),plc_action varchar(50),plc_ip varchar(50),tag_name varchar(50),data_type varchar(10),tag_value varchar(max) );

	--get the station ID
	SET @machine_ID = dbo.fn_getmachineIDFromTagDesc(@tag_desc)

	SELECT @group_ID = m.machine_group_ID, @op_order = m.op_order, @parts_per_cycle = SUM(tp.parts_per_tool)
	FROM machines m
		JOIN machine_tools mt ON m.ID = mt.machine_ID AND m.current_tool_ID = mt.tool_ID
		JOIN tools t ON mt.tool_ID = t.ID
		JOIN tool_parts tp ON t.ID = tp.tool_ID
	WHERE m.ID = @machine_ID
	GROUP BY m.machine_group_ID, m.op_order

	SELECT @min_op_order = MIN(op_order)
	FROM machines 
	WHERE machine_group_ID = @group_ID

	IF EXISTS(SELECT * FROM machine_groups WHERE next_op_group = @group_ID)
		SET @has_prior_op_group = 1

	SELECT @plc_ip = ip_address 
	FROM NYSUS_PLC_POLLER.dbo.nysus_plcs 
	WHERE ID = @plc_ID

	PRINT 'GOT HERE'

	WHILE @i < @parts_per_cycle
		BEGIN
			PRINT 'PART: ' + CAST(@i as varchar(4))

			SET @write_tag_name = @part_status_tag_name + '[' + CAST(@i as varchar(2)) + ']'

			--get the current barcode
			SET @barcode_data = NULL

			SELECT @barcode_data = value
			FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
			WHERE description = @tag_desc 
				AND name LIKE '%.BARCODE_READY_TO_MES_' + CAST(@i as varchar(2)) + '_.LEN'

			PRINT 'BARCODE DATA: ' + @barcode_data

			-- make sure external_serial is not null or empty, else validate part exists
			IF @barcode_data IS NULL OR @barcode_data = ''
				BEGIN
					INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
						VALUES ('Barcode Empty', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', @barcode_empty_error);

					PRINT 'NO BARCODE'
					BREAK  --EXIT THE WHILE LOOP
				END
			ELSE
				BEGIN
					DECLARE @mipp_part_serial bigint
					DECLARE @last_machine_completed_ID int, @last_machine_started_ID int, @last_machine_completed_order int, @last_machine_started_order int
					SET @mipp_part_serial = NULL
					SET @serial = dbo.fn_getSerialNumberFromFullBarcode(@barcode_data)

					IF @op_order = @min_op_order AND @has_prior_op_group = 0
						BEGIN
							--AT FIRST STATION NEED TO INSERT PART
							IF NOT EXISTS (SELECT serial FROM machine_cycle_part_history WHERE serial = @serial AND machine_ID = @machine_ID AND check_result = 'INSERTED')
								BEGIN
									INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'INSERTED')

									PRINT 'MIN STATION, DID INSERT'

									--GIVE OK
									INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
										VALUES ('Ok to Cycle', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', 1);
								END
							ELSE
								BEGIN
									INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'DUPLICATE')

									PRINT 'MIN STATION, DUPLICATE PART'

									--GIVE ERROR
									INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
										VALUES ('Ok to Cycle', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', @duplicate_part_error);

									BREAK  --EXIT THE WHILE LOOP
								END								

							SET @i = @i + 1
							CONTINUE
						END
					ELSE
						BEGIN
							--LOOKUP PART
							SELECT TOP 1 @mipp_part_serial = mipp.serial, @last_machine_completed_ID = last_machine_completed_ID, @last_machine_started_ID = last_machine_started_ID, @last_machine_completed_order = last_machine_completed_order, @last_machine_started_order = last_machine_started_order
							FROM machine_in_process_parts mipp
							WHERE mipp.serial = @serial;

							-- if no wip part set a status code, else make sure part is ready
							IF @mipp_part_serial IS NULL
								BEGIN
									INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
										VALUES ('WIP Part Not Found', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', @wip_part_not_found_error);

									INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'NOT_FOUND')

									BREAK  --EXIT THE WHILE LOOP
								END
							ELSE
								BEGIN
									DECLARE @prior_machine_order int, @prior_machine_ID int;

									-- get the machine_ID prior to @op_order
									SELECT @prior_machine_order = MAX(m.op_order)
									FROM machines m
									WHERE m.op_order < @op_order
										AND m.machine_group_ID = @group_ID;	
										
									SELECT @prior_machine_ID = ID
									FROM machines
									WHERE machine_group_ID = @group_ID 
										AND op_order = @prior_machine_order
										
									IF @has_prior_op_group = 1 AND @min_op_order = @op_order
										BEGIN
											SELECT @prior_machine_order = MAX(m.op_order)
											FROM machines m
											WHERE m.op_order < @op_order
												AND m.machine_group_ID = (SELECT machine_group_ID
																		  FROM machines 
																		  WHERE ID = @last_machine_completed_ID) 
										
										END		

									-- if last station equals prior station
									IF @prior_machine_order = @last_machine_completed_order
										IF @last_machine_started_order = @op_order  --make sure it hasn't already started this station
											BEGIN
												--DUPLICATE PART
												INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'DUPLICATE')

												-- part was scanned twice....so fail it
												INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
													VALUES ('Ok to Cycle', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', @duplicate_part_error);

												BREAK  --EXIT THE WHILE LOOP	
											END
										ELSE
											BEGIN
												--ALL GOOD...
												INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'OKAY_TO_RUN')

												-- part should be ready ...all good so far
												INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
													VALUES ('Ok to Cycle', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', 1);
											END
									ELSE
										BEGIN
											-- insert part not ready 
											INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
												VALUES ('WIP Part Not Ready', 'WRITE', @plc_ip, REPLACE(@tag_name, @part_ready_tag_name, @write_tag_name), 'DINT', @wip_part_not_ready_error);	
										
											INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, check_result) VALUES (@serial, @machine_ID, GETDATE(), 'NOT_READY')

											BREAK  --EXIT THE WHILE LOOP						
										END
										-- end if last station equals prior station
								END
						END			
				END

			SET @i = @i + 1
		END
	
	DECLARE @loopy_plc_action varchar(50);
	DECLARE @loopy_plc_tag varchar(50);
	DECLARE @loopy_plc_ip varchar(50);
	DECLARE @loopy_plc_data_type varchar(50);
	DECLARE @loopy_plc_value varchar(50);
	DECLARE loopy224 CURSOR FOR 
		SELECT plc_action, plc_ip, tag_name, data_type, tag_value FROM #Temp	

	OPEN loopy224

	FETCH NEXT FROM loopy224 INTO @loopy_plc_action, @loopy_plc_ip, @loopy_plc_tag, @loopy_plc_data_type, @loopy_plc_value
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--do some shiz
			IF @loopy_plc_action = 'WRITE'
				SELECT MES_COMMON.dbo.fn_setPLCValue(@webserver, @loopy_plc_ip, @loopy_plc_tag, 0, @loopy_plc_data_type, @loopy_plc_value)

			FETCH NEXT FROM loopy224 INTO @loopy_plc_action, @loopy_plc_ip, @loopy_plc_tag, @loopy_plc_data_type, @loopy_plc_value
		END
	
	CLOSE loopy224;
	DEALLOCATE loopy224;
	
	-- Cleanup
	SELECT * FROM #Temp

	DROP TABLE #Temp;

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
