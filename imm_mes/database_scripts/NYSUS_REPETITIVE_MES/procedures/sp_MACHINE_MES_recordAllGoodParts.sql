SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 5/16/2017
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_recordAllGoodParts] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @part_ID int
	DECLARE @tool_ID int

	--GET CURRENT TOOL
	SELECT @tool_ID = current_tool_ID
	FROM machines
	WHERE ID = @machine_ID

    --LOOP THROUGH THE ACTIVE PARTS AND CALL RECORD GOOD PART FOR EACH
	DECLARE loopyQQEQWEQWE CURSOR FOR
		SELECT parts_id
		FROM machine_active_parts
		WHERE machine_ID = @machine_ID
			AND tool_ID = @tool_ID
			AND end_timestamp IS NULL;

	OPEN loopyQQEQWEQWE;
	FETCH NEXT FROM loopyQQEQWEQWE INTO @part_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC sp_MACHINE_MES_recordGoodPart @machine_ID = @machine_ID, @part_ID = @part_ID, @operator_ID = @operator_ID

			FETCH NEXT FROM loopyQQEQWEQWE INTO @part_ID
		END

	CLOSE loopyQQEQWEQWE
	DEALLOCATE loopyQQEQWEQWE
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
