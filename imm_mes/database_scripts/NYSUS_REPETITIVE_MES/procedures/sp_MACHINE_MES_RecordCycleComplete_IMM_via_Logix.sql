SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Sass
-- Create date: 4/28/2017
-- Description:	
-- DW:  8/1/2018 added revision numbers for machine_cycle_parts
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleComplete_IMM_via_Logix] 
	-- Add the parameters for the stored procedure here
	@tag_ID int = 0, 
	@tag_value varchar(50) = '',
	@tag_desc varchar(50) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @machine_number varchar(50), @is_primary bit
	DECLARE @machine_ID int
	DECLARE @operator_ID int
	DECLARE @tool_ID int
	DECLARE @last_cycle_time datetime
	DECLARE @cycle_sequence int, @cycle_duration int
	DECLARE @cycle_ID int
	DECLARE @target_cycle_time int, @cycle_overage int
	DECLARE @parts_per_tool int, @i int
	DECLARE @last_shift_start datetime
	DECLARE @over_cycle_grace_period float  --IN SECONDS
	DECLARE @system_ID int
	DECLARE @auto_label bit = 0
	DECLARE @tool_part_ID int
	DECLARE @serial_number varchar(20)
	DECLARE @barcode varchar(20)
	DECLARE @part_number varchar(50)
	DECLARE @REV_NUMBER varchar(20)

	set @REV_NUMBER = ''

	-- ONLY record cycle complete if we're getting a 'True' signal
	IF (@tag_value = 'False' OR @tag_value = '0')
		RETURN

	IF @tag_desc = ''
		SELECT @tag_desc = name 
		FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
		WHERE ID = CAST(@tag_ID as int)

	SELECT @machine_number = description, @is_primary = is_primary
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE ID = @tag_ID
	

	--GET SYSTEM AND MACHINE INFO
	SELECT @system_ID = m.system_ID, 
		@operator_ID = current_operator_ID, 
		@machine_ID = m.ID, 
		@tool_ID = ISNULL(m.current_tool_ID, 0), 
		@target_cycle_time = ISNULL(mt.target_cycle_time, 0), 
		@over_cycle_grace_period = ISNULL(mt.over_cycle_grace_period, 0),
		@auto_label = ISNULL(t.auto_label, 0)
	FROM machines m
		LEFT OUTER JOIN tools t ON m.current_tool_ID = t.ID
		LEFT OUTER JOIN machine_tools mt ON mt.machine_ID = m.ID AND mt.tool_ID = m.current_tool_ID
	WHERE m.machine_number = @machine_number

	--INCREMENT SHOT COUNT FOR TOOL
	UPDATE tools
	SET shot_count = ISNULL(shot_count, 0) + 1
	WHERE ID = @tool_ID

	--GET REVISION NUMBER
	SELECT @REV_NUMBER = isnull(revision_number,'') from molding_revision_numbers where tool_id = @tool_ID and active = 1
	--GET LAST CYCLE TIME
	SELECT TOP 1 @last_cycle_time = cycle_time, @cycle_sequence = ISNULL(cycle_sequence, 0)
	FROM machine_cycles 
	WHERE machine_ID = @machine_ID
	ORDER BY ID DESC

	--SET MACHINE UP AND RECORD LAST CYCLE TIME
	UPDATE machines 
	SET last_cycle_time = GETDATE(), up = 1
	WHERE ID = @machine_ID

	--GET DURATION OF THAT CYCLE
	IF @last_cycle_time IS NOT NULL
		BEGIN
			SET @last_shift_start = dbo.getShiftStartForTimestamp(@system_ID, GETDATE())

			IF @last_cycle_time < @last_shift_start
				SET @last_cycle_time = @last_shift_start

			SET @cycle_duration = DATEDIFF(SECOND, @last_cycle_time, GETDATE())
		END
	ELSE
		SET @cycle_duration = 0

	--RECORD CYCLE (let KM FTP process update this record by leaving the cycle_sequence and cycle_duration in a known state)
	INSERT INTO machine_cycles (machine_ID, tool_ID, cycle_time, cycle_duration, cycle_sequence, operator_ID) VALUES (
		@machine_ID,
		@tool_ID,
		GETDATE(),
		NULL,
		0,
		@operator_ID)

	SET @cycle_ID = @@IDENTITY

	INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'Inserted Cycle: ' + CAST(@cycle_ID AS varchar(50)))

	--INSERT PARTS BY LOOPING THROUGH THE machine_active_parts
	DECLARE loopyQWQWEEQWEE CURSOR FOR
		SELECT tp.ID
		FROM machine_active_parts map
			JOIN tool_parts tp ON map.tool_id = tp.tool_ID AND map.parts_id = tp.part_ID
		WHERE map.machine_id = @machine_ID
			AND map.tool_id = @tool_ID
			AND start_timestamp <= GETDATE()
			AND end_timestamp IS NULL
		ORDER BY map.cavity_order;

	OPEN loopyQWQWEEQWEE
	FETCH NEXT FROM loopyQWQWEEQWEE INTO @tool_part_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--GET NEXT SERIAL NUMBER
			SELECT @serial_number = dbo.fn_getNextSerial(@tool_part_ID)

			--GET PART NUMBER TO GENERATE FULL BARCODE
			SELECT @part_number = p.part_number
			FROM tool_parts tp
			JOIN parts p ON p.ID = tp.part_ID
			WHERE tp.ID = @tool_part_ID

			INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'Serial number to insert: ' + @serial_number + ' for tool_part_ID: ' + CAST(@tool_part_ID as varchar(50))) 

			--INSERT THE RECORD
			INSERT INTO machine_cycle_parts (cycle_ID, tool_part_ID, insert_time, serial_number, full_barcode, revision_number) VALUES (
				@cycle_ID,
				@tool_part_ID,
				GETDATE(),
				@serial_number,
				CONCAT(@part_number, @serial_number),
				@REV_NUMBER
			)
			FETCH NEXT FROM loopyQWQWEEQWEE INTO @tool_part_ID
		END
	
	CLOSE loopyQWQWEEQWEE
	DEALLOCATE loopyQWQWEEQWEE

	--END ANY OPEN DOWNTIME...
	--BUT IF IT WAS UNDER OVER CYCLE GRACE PERIOD OF TIME PAST THE TARGET, AUTO-DISPOSITION IT AS A MICRO-STOP
	DECLARE @downtime_reason_ID int
	SET @downtime_reason_ID = NULL

	IF @cycle_duration > @target_cycle_time AND @cycle_duration < (@target_cycle_time + @over_cycle_grace_period)
		BEGIN
			--YES, IT IS A MICRO-STOP
			INSERT INTO NYSUS_REPETITIVE_MES.dbo.loggy (log_time, log_msg) VALUES (GETDATE(), 'Machine '+CAST(@machine_ID AS varchar(50))+' with tool '+CAST(@tool_ID AS varchar(50))+' reported in micro-stop')
			SELECT TOP 1 @downtime_reason_ID = ID
			FROM machine_downtime_reasons
			WHERE is_micro_stop = 1
			ORDER BY ID
		END

	--UPDATE ANY OPEN ONGOING DOWNTIME RECORD
	UPDATE machine_downtime_log
	SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0,
		reason_ID = @downtime_reason_ID
	WHERE machine_ID = @machine_ID
		AND reason_ID IS NULL
		AND time_in_state IS NULL



	--PRINT LABELS (IF AUTO-PRINT IS ON)
	IF @auto_label = 1
		BEGIN
			DECLARE @machine_cycle_part_ID int, @part_ID int

			DECLARE loopyQWEQWEEE CURSOR FOR
				SELECT mcp.ID, tp.part_ID
				FROM machine_cycle_parts mcp
					JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID 
				WHERE mcp.cycle_ID = @cycle_ID
				ORDER BY tp.part_order;

			OPEN loopyQWEQWEEE
			FETCH NEXT FROM loopyQWEQWEEE INTO @machine_cycle_part_ID, @part_ID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					
					INSERT INTO loggy (log_time, log_msg) VALUES (GETDATE(), 'AUTO-LABELING machine_cycle_part_ID: ' + CAST(@machine_cycle_part_ID AS varchar(50)))

					EXEC dbo.sp_MACHINE_MES_recordGoodPart 
						@machine_ID = @machine_ID,
						@part_ID = @part_ID,
						@operator_ID = @operator_ID,
						@machine_cycle_part_ID = @machine_cycle_part_ID

					FETCH NEXT FROM loopyQWEQWEEE INTO @machine_cycle_part_ID, @part_ID
				END

			CLOSE loopyQWEQWEEE
			DEALLOCATE loopyQWEQWEEE
		END

	--TODO: RECORD PARAMETERS (IF THE PARAMETER DATA ARRIVES BEFORE THE SHOT COMPLETE SIGNAL)

END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
