SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 11/11/2014
-- Description:	@split_val is in minutes (as a decimal)
-- =============================================
CREATE PROCEDURE [dbo].[sp_splitDowntime] 
	-- Add the parameters for the stored procedure here
	@dt_ID int = 0, 
	@split_val float = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @original_time_in_state float, @original_start_time datetime
	DECLARE @new_time_in_state float 
	DECLARE @new_start_time datetime

	DECLARE @tool_ID int, @machine_ID int

	SELECT @original_time_in_state = time_in_state, @original_start_time = change_time,
		@tool_ID = tool_ID, @machine_ID = machine_ID
	FROM machine_downtime_log 
	WHERE id = @dt_ID

	SET @new_time_in_state = @original_time_in_state - @split_val
	SET @new_start_time = DATEADD(SECOND, @split_val * 60.0, @original_start_time)

	UPDATE machine_downtime_log
	SET time_in_state = @split_val
	WHERE ID = @dt_ID 

	INSERT INTO machine_downtime_log (change_time, up, tool_ID, machine_ID, time_in_state) VALUES (
		@new_start_time,
		0,
		@tool_ID,
		@machine_ID,
		@new_time_in_state)


	SELECT 1 as success
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
