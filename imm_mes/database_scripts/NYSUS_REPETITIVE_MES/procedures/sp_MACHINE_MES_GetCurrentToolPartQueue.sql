SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO




-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetCurrentToolPartQueue] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	--SELECT mcp.*, tp.part_ID, tp.tool_ID, p.part_desc, p.part_number, p.part_image
	--FROM machine_cycle_parts mcp
	--	JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID 
	--	JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID AND tp.part_ID IN (
	--		SELECT parts_id 
	--		FROM machine_active_parts 
	--		WHERE end_timestamp IS NULL AND 
	--			machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
	--			AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
	--		)
	--	JOIN parts p ON tp.part_ID = p.ID
	--WHERE mc.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
	--	AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
	--	AND mcp.disposition_time IS NULL

	SELECT mcp.*, tp.part_ID, tp.tool_ID, p.part_desc, p.part_number, p.part_image
	FROM machine_cycle_parts mcp
		JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
		JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
		JOIN parts p ON p.ID = tp.part_ID
	WHERE mc.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
		AND (@machine_ID = 0 OR mc.machine_ID = @machine_ID)
		AND mcp.disposition_time IS NULL
		AND mcp.tool_part_ID IN (
			SELECT tp.ID FROM tool_parts tp
			JOIN machine_active_parts map ON map.parts_id = tp.part_ID AND map.tool_id = tp.tool_ID
			WHERE map.end_timestamp IS NULL and is_active = 1 AND map.machine_id = @machine_ID
		)

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
