SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_MOLDING_MES_DeactivateParts] 
	-- Add the parameters for the stored procedure here
	@machineID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- UPDATE MAP TABLE to close out all active part with null end datetimes
	UPDATE machine_active_parts
	SET is_active = 0, end_timestamp = GETDATE()
	WHERE machine_id = @machineID
	AND is_active = 1 
	AND end_timestamp IS NULL

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
