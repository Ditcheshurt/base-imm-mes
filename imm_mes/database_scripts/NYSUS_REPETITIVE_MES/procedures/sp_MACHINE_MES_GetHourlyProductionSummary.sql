SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 3/24/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_GetHourlyProductionSummary] 
	-- Add the parameters for the stored procedure here
	@my_IP varchar(50) = '',
	@machine_ID int = 0
AS
BEGIN
	DECLARE @system_ID int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @system_ID = system_ID FROM machines WHERE ID = @machine_ID;

	--ACTUAL
	--SELECT * 
	--FROM (
	--	SELECT TOP 24 the_date, the_hour, part_qty, part_target_qty, part_goal
	--	FROM hourly_summary
	--	WHERE 
	--		system_ID = @system_ID
	--		AND machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
	--		AND (@machine_ID = 0 OR machine_ID = @machine_ID)
	--		AND the_date > DATEADD(DAY, -2, GETDATE())
	--	ORDER BY the_date DESC, the_hour DESC) as v
	--ORDER BY the_date, the_hour
	SELECT * 
	FROM (
		SELECT TOP 8 the_date, ti.interval_desc, the_hour, part_qty, part_target_qty, part_goal
		FROM hourly_summary hs
		JOIN MES_COMMON.dbo.time_intervals ti ON the_hour = DATEPART(HH, ti.interval_start_time)
		JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.time_interval_ID = ti.ID
		JOIN MES_COMMON.dbo.shifts s ON sti.shift_ID = s.ID
		WHERE 
			hs.system_ID = @system_ID
			AND s.system_ID = @system_ID
			AND machine_ID IN (SELECT id FROM machines WHERE station_IP_address = @my_IP)
			AND (@machine_ID = 0 OR machine_ID = @machine_ID)
			AND the_date > DATEADD(DAY, -2, GETDATE())
		ORDER BY the_date DESC, the_hour DESC) as v
	ORDER BY the_date, the_hour;
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
