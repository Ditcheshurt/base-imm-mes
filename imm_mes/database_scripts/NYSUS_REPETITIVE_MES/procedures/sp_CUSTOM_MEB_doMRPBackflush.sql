SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Steve Sass
-- Create date: 1-12-2018
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_CUSTOM_doMRPBackflush] 
	@do_insert bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @t TABLE (cycle_ID int)
	DECLARE @DROP_LOCATION varchar(200)
	DECLARE @dt varchar(12), @d varchar(8)
	DECLARE @file varchar(MAX), @num_recs int = 0
	DECLARE @part_number varchar(100), @qty int
	DECLARE @ffq_ID int, @max_ID int

	SET @DROP_LOCATION = '\\10.68.160.95\SigConnects\in'
	
	--HEADER	
	DECLARE @LOCATION varchar(50)			= '510'
	DECLARE @SITE varchar(50)				= '10'
	DECLARE @LOTSERIAL varchar(50)			= ''
	DECLARE @LOTREF varchar(50)				= ''
	DECLARE @USERID varchar(50)				= 'NYSUS'
	DECLARE @MODIFIED_BACKFLUSH varchar(50) = 'YES'
	
	--DETAIL
	DECLARE @BACKFLUSH_LOCATION varchar(50) = '310'			--CHANGED FROM 510 to 310 on 2-27-2018 - SS
	DECLARE @PICK_LOGIC varchar(50)			= 'NO'

	--DATE
	SET @d = dbo.fn_padStringFront(CAST(DATEPART(MONTH, GETDATE()) as varchar(2)), 2, '0') + '/' +
			  dbo.fn_padStringFront(CAST(DATEPART(DAY, GETDATE()) as varchar(2)), 2, '0') + '/' +
			  RIGHT(CAST(DATEPART(YEAR, GETDATE()) as varchar(4)), 2)

	--DATETIME
	SET @dt = dbo.fn_padStringFront(CAST(DATEPART(MONTH, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(DAY, GETDATE()) as varchar(2)), 2, '0') + 
			  RIGHT(CAST(DATEPART(YEAR, GETDATE()) as varchar(4)), 2) + 
			  dbo.fn_padStringFront(CAST(DATEPART(HOUR, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(MINUTE, GETDATE()) as varchar(2)), 2, '0') + 
			  dbo.fn_padStringFront(CAST(DATEPART(SECOND, GETDATE()) as varchar(2)), 2, '0')

	--POPULATE TEMP TABLE
	INSERT INTO @t (cycle_ID)
	SELECT ID 
	FROM machine_cycles
	WHERE mrp_backflush_time IS NULL
	ORDER BY ID

	SELECT @max_ID = MAX(cycle_ID)
	FROM @t

	DECLARE loopy78876TUTIU CURSOR FOR
		SELECT p.mrp_part_number, COUNT(*) as qty
		FROM parts p
			JOIN tool_parts tp ON p.ID = tp.part_ID
			JOIN machine_cycle_parts mcp ON tp.ID = mcp.tool_part_ID
			JOIN @t t ON mcp.cycle_ID = t.cycle_ID
		GROUP BY p.mrp_part_number
		ORDER BY p.mrp_part_number
		;

	OPEN loopy78876TUTIU
	FETCH NEXT FROM loopy78876TUTIU INTO @part_number, @qty
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @part_number as part_number, @qty as qty
			SET @file = ''

			--INSERT INTO flat_file
			--HEADER:
			SET @file = @file + 
				'H::' + 
				dbo.fn_padStringRear(@part_number, 18, ' ') + 
				dbo.fn_padStringRear(@LOCATION, 8, ' ') + 
				dbo.fn_padStringRear(@SITE, 8, ' ') + 
				dbo.fn_padStringRear(CAST(@qty as varchar(11)), 11, ' ') + 
				dbo.fn_padStringRear(@LOTSERIAL, 18, ' ') +
				dbo.fn_padStringRear(@LOTREF, 8, ' ') + 
				dbo.fn_padStringRear(@USERID, 20, ' ') + 
				@d +
				dbo.fn_padStringRear(@MODIFIED_BACKFLUSH, 3, ' ') + 
				CHAR(13) + CHAR(10)

			--DETAIL
			SET @file = @file + 
				'D::' + 
				dbo.fn_padStringRear(@BACKFLUSH_LOCATION, 8, ' ') + 
				dbo.fn_padStringRear(@PICK_LOGIC, 3, ' ') + 
				dbo.fn_padStringRear(@USERID, 8, ' ') + 
				CHAR(13) + CHAR(10)

			--INCREMENT num_recs
			SET @num_recs = @num_recs + 1

			--WRITE A FILE FOR EACH PART...WHICH IS SILLY
			IF @do_insert = 1
				BEGIN
					--INSERT
					INSERT INTO NYSUS_SEQUENCE_MES.dbo.flat_file_queue (posting_app, record_ID, file_text, drop_dir, post_time, drop_time, process_time, use_key_file) VALUES (
						'RBB',
						-1, -- THIS WILL BE OVERWRITTEN BY THE UPDATE
						CAST(@file as text),
						@DROP_LOCATION,
						GETDATE(),
						NULL,
						NULL,
						0)

					SELECT TOP 1 @ffq_ID = ID
					FROM NYSUS_SEQUENCE_MES.dbo.flat_file_queue 
					WHERE record_ID = -1 
					ORDER BY ID DESC

					UPDATE NYSUS_SEQUENCE_MES.dbo.flat_file_queue
					SET record_ID = @ffq_ID
					WHERE ID = @ffq_ID
				END
			ELSE
				SELECT @num_recs as num_recs, @file as [file]

			FETCH NEXT FROM loopy78876TUTIU INTO @part_number, @qty
		END

	CLOSE loopy78876TUTIU
	DEALLOCATE loopy78876TUTIU

	--UPDATE THE CYCLES AS BACKFLUSHED
	UPDATE machine_cycles
	SET mrp_backflush_time = GETDATE(), mrp_backflush_max_record = @max_ID
	WHERE ID IN (
		SELECT cycle_ID
		FROM @t
	)


END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
