SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 9/2/15
-- Description:	triggered when plc tag shows part ready, validates wip part with barcode exists
-- DW: 8/1/2018 added part revision information to machine_cycle_parts table
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleComplete] 
	@tag_ID varchar(10),
	@tag_value varchar(30),
	@tag_desc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value,	'RecordCycleComplete'
	
	IF @tag_value = '0' OR @tag_value = 'False'
		RETURN
	
	DECLARE @tag_name varchar(100)
	DECLARE @station_ID int, @station_order int
	DECLARE @station_instance int
	DECLARE @parts_per_cycle int
	DECLARE @barcode_data varchar(100)
	DECLARE @barcode_empty_error int = 1001;
	DECLARE @wip_part_not_found_error int = 1002;
	DECLARE @wip_part_not_ready_error int = 1003;
	DECLARE @plc_clear_fail_status int = 0;
	DECLARE @plc_tag varchar(50);
	DECLARE @plc_ip varchar(50), @plc_ID int;
	DECLARE @i int = 0;
	DECLARE @webserver varchar(50) = 'localhost';
	DECLARE @part_ready_tag_name varchar(100) = 'PART_READY_TO_MES'
	DECLARE @data_rcvd_tag_name varchar(100) = 'DATA_RCVD_TO_PLC'
	DECLARE @part_status_tag_name varchar(100) = 'PART_STATUS_TO_PLC'
	DECLARE @op_done_tag_name varchar(100) = 'OP_DONE_TO_MES[0]'
	DECLARE @machine_number varchar(50), @is_primary bit
	DECLARE @machine_ID int
	DECLARE @operator_ID int
	DECLARE @last_cycle_time datetime
	DECLARE @cycle_sequence int, @cycle_duration int
	DECLARE @cycle_ID int
	DECLARE @target_cycle_time int, @cycle_overage int
	DECLARE @parts_per_tool int
	DECLARE @last_shift_start datetime
	DECLARE @over_cycle_grace_period float  --IN SECONDS
	DECLARE @group_ID int, @op_order int
	DECLARE @serial bigint = 0
	DECLARE @current_tool_ID int
	DECLARE @lot_number varchar(50)
	DECLARE @lot_material_type_ID int
	DECLARE @lot_parts_used int
	DECLARE @lot_qty_per_part int
	DECLARE @autogauge_path_10045 varchar(50) = 'd:\GaugeData\LongShaft\'
	DECLARE @autogauge_path_20045 varchar(50) = 'd:\GaugeData\ShortShaft\'
	DECLARE @tool_part_ID int
	DECLARE @start_next_lot int = 0
	DECLARE @system_ID int
	DECLARE @REV_NUMBER varchar(20)

	SET @REV_NUMBER = ''

	--SELECT @tag_desc = description, @plc_ID = plc_ID, @tag_name = name
	--FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	--WHERE ID = @tag_ID

	-- Create a temp result table
	CREATE TABLE #Temp ( result varchar(50),plc_action varchar(50),plc_ip varchar(50),tag_name varchar(50),data_type varchar(10),tag_value varchar(max) );

	SELECT @machine_number = description, @is_primary = is_primary, @plc_ID = plc_ID, @tag_name = name
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE ID = @tag_ID

	SET @machine_ID = dbo.fn_getmachineIDFromTagDesc(@tag_desc)

	SELECT @system_ID = m.system_ID, @operator_ID = current_operator_ID, @group_ID = machine_group_ID, @op_order = op_order, @parts_per_cycle = tp.parts_per_tool, @current_tool_ID = m.current_tool_ID, @target_cycle_time = ISNULL(mt.target_cycle_time, 0), @over_cycle_grace_period = ISNULL(m.over_cycle_grace_period, 0)
	FROM machines m
		JOIN machine_tools mt ON m.ID = mt.machine_ID AND m.current_tool_ID = mt.tool_ID
		JOIN tools t ON mt.tool_ID = t.ID
		JOIN tool_parts tp ON t.ID = tp.tool_ID
		JOIN parts p ON p.ID = tp.part_ID
	WHERE m.ID = @machine_ID

	SELECT @rev_number = isnull(revision_number,'') from molding_revision_numbers where tool_id = @current_tool_ID and active = 1

	SELECT @plc_ip = ip_address 
	FROM NYSUS_PLC_POLLER.dbo.nysus_plcs 
	WHERE ID = @plc_ID

	SELECT TOP 1 @last_cycle_time = cycle_time, @cycle_sequence = ISNULL(cycle_sequence, 0)
	FROM machine_cycles 
	WHERE machine_ID = @machine_ID
	ORDER BY ID DESC

	UPDATE machines 
	SET last_cycle_time = GETDATE()
	WHERE ID = @machine_ID

	IF @last_cycle_time IS NOT NULL
		BEGIN
			SET @last_shift_start = dbo.getShiftStartForTimestamp(@system_ID, GETDATE())

			IF @last_cycle_time < @last_shift_start
				SET @last_cycle_time = @last_shift_start

			SET @cycle_duration = DATEDIFF(SECOND, @last_cycle_time, GETDATE())
		END
	ELSE
		SET @cycle_duration = 0

	IF RIGHT(@tag_name, 3) = '[0]'
		BEGIN
			INSERT INTO machine_cycles (machine_ID, tool_ID, cycle_time, cycle_duration, cycle_sequence) VALUES (
				@machine_ID,
				@current_tool_ID,
				GETDATE(),
				@cycle_duration,
				ISNULL(@cycle_sequence, 0) + 1)

			SET @cycle_ID = @@IDENTITY

			SET @cycle_overage = @cycle_duration - @target_cycle_time
			IF @cycle_duration > @target_cycle_time
				IF @cycle_overage <= @over_cycle_grace_period
					INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID, time_in_state, recorded_by, reason_ID) VALUES (
							DATEADD(SECOND, 0 - @cycle_overage, GETDATE()), 
							0, 
							@machine_ID, 
							@current_tool_ID,
							CAST(@cycle_overage as float)/60.0,
							@operator_ID,
							1
						)  --AUTO ENTER AS A REASON CODE 1: OVER CYCLE
				ELSE
					INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID, time_in_state, recorded_by) VALUES (
						DATEADD(SECOND, 0 - @cycle_overage, GETDATE()), 
						0, 
						@machine_ID, 
						@current_tool_ID,
						CAST(@cycle_overage as float)/60.0,
						@operator_ID 
					)  --NO REASON CODE....THEY MUST DISPOSITION
		END
	ELSE
		BEGIN
			SET @i = CAST(REPLACE(RIGHT(@tag_name, 2), ']', '') as int)

			--NEED TO GET THE LAST CYCLE ID HERE
			SELECT TOP 1 @cycle_ID = ID 
			FROM machine_cycles
			WHERE machine_ID = @machine_ID
			ORDER BY ID DESC
		END

	--NOW DO THE PART TRACEABILITY LOGIC

	--get the current barcode
	SET @barcode_data = NULL

	SELECT @barcode_data = value
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE description = @tag_desc 
		AND name LIKE '%.BARCODE_DONE_TO_MES_' + CAST(@i as varchar(2)) + '_.LEN'

	-- make sure external_serial is not null or empty, else validate part exists
	IF @barcode_data IS NOT NULL
		BEGIN
			DECLARE @wip_part_ID bigint, @part_ID int = 0
			SET @serial = dbo.fn_getSerialNumberFromFullBarcode(@barcode_data)
			SET @wip_part_ID = NULL
			
			SELECT @wip_part_ID = serial
			FROM machine_in_process_parts
			WHERE serial = @serial
				AND last_machine_started_ID = @machine_ID

			SELECT @part_ID = ISNULL(ID, 0)
			FROM parts 
			WHERE LEFT(mrp_part_number, 8) = LEFT(@barcode_data, 8)

			UPDATE machine_cycle_part_history 
			SET cycle_complete_time = GETDATE(), cycle_complete_result = CAST(@tag_value as int)
			WHERE serial = @serial
				AND machine_ID = @machine_ID
				AND cycle_complete_time IS NULL
				AND check_result IN ('INSERTED', 'OKAY_TO_RUN')
				AND cycle_complete_time IS NULL

			-- if no wip part set a status code, else make sure part is ready
			IF @wip_part_ID IS NOT NULL
				BEGIN		
					--READ AND STORE ANY PROCESS DATA HERE
					INSERT INTO machine_cycle_parts_plc_values (serial, machine_ID, tag_name, tag_value)
					SELECT @serial, @machine_ID, name, value 
					FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
					WHERE description = @tag_desc 
						AND name LIKE '%DATA_TO_MES_P' + CAST(@i as varchar(20)) + '%'
						AND ISNULL(value, '') <> ''

					--INSERT CYCLE PART RECORD NO MATTER IF PASSED OR FAILED - 4/6/2015
					INSERT INTO machine_cycle_parts(cycle_ID, tool_part_ID, serial_number, full_barcode, plc_op_done_value, revision_number) 							
					SELECT @cycle_ID, tp.ID, CAST(@serial as varchar(50)), @barcode_data, CAST(@tag_value as int), @REV_NUMBER
					FROM tool_parts tp
						JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID AND mt.machine_ID = @machine_ID
						JOIN machines m ON m.ID = @machine_ID AND mt.tool_ID = m.current_tool_ID
					WHERE tp.part_ID = @part_ID


					--UPDATE THE LAST STATION COMPLETED
					IF @tag_value = '1'
						BEGIN
							UPDATE machine_in_process_parts 
							SET last_machine_completed_ID = @machine_ID, last_machine_completed_time = GETDATE(), last_machine_completed_order = @op_order
							WHERE serial = @wip_part_ID
							--print @wip_part_ID
						END
					ELSE
						BEGIN
							--RECORD DEFECT - 4/6/2015
							DECLARE @tool_ID int, @defect_ID int = 0

							--GET TOOL ID
							SELECT @tool_ID = tp.tool_ID
							FROM tool_parts tp
								JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID AND mt.machine_ID = @machine_ID
								JOIN machines m ON m.ID = @machine_ID AND mt.tool_ID = m.current_tool_ID
							WHERE tp.part_ID = @part_ID

							--GET DEFECT ID 
							SELECT @defect_ID = ID 
							FROM machine_defect_reasons
							WHERE plc_defect_code = CAST(@tag_value as int)

							INSERT INTO machine_defect_log (machine_ID, tool_ID, part_ID, defect_time, operator_ID, defect_ID, disposition, serial_number) VALUES (
								@machine_ID,
								@tool_ID,
								@part_ID,
								GETDATE(),
								@operator_ID,
								@defect_ID,
								'SCRAP',
								CAST(@serial as varchar(50))
							)

						END
				END
			
		END

	
	IF RIGHT(@tag_name, 3) = '[0]'
		INSERT INTO #Temp (result, plc_action, plc_ip, tag_name, data_type, tag_value) 
			VALUES ('DATA RECVD TAG', 'WRITE', @plc_ip, REPLACE(@tag_name, @op_done_tag_name, @data_rcvd_tag_name), 'BOOL', 'True');

	-- AUTOGAUGE FILE GRAB
	SELECT TOP 1 @serial = serial FROM dbo.machine_in_process_parts WHERE last_machine_started_ID = @machine_ID ORDER BY last_machine_started_time DESC
	IF @machine_ID = 10045
	BEGIN
		EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value, 'RecordAutoGauge - fired from RecordCycleComplete'
		EXEC dbo.sp_CUSTOM_RECORD_AUTOGAUGE @tag_ID, @tag_desc, @tag_value, @machine_ID, @autogauge_path_10045, @serial, @cycle_ID
	END
	ELSE IF @machine_ID = 20045
	BEGIN
		EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value, 'RecordAutoGauge - fired from RecordCycleComplete'	
		EXEC dbo.sp_CUSTOM_RECORD_AUTOGAUGE @tag_ID, @tag_desc, @tag_value, @machine_ID, @autogauge_path_20045, @serial, @cycle_ID
	END

	-- LOT ASSIGNMENTS
	IF @machine_ID IN (10010, 20010)	
	BEGIN
		EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value, 'RecordLot'
		--SELECT @lot_material_type_ID = l.ID, @lot_number = l.current_lot, @lot_parts_used = l.lot_parts_used, @lot_qty_per_part = ltp.qty_per_part
		--FROM [dbo].[lot_material_types] l 
		--	JOIN [dbo].[lot_machines] lm ON l.ID = lm.lot_material_type_ID
		--	JOIN [dbo].[lot_tool_parts] ltp ON ltp.lot_material_type_ID = l.ID 
		--	JOIN [dbo].[tool_parts] tp ON tp.ID = ltp.tool_part_ID
		--WHERE lm.machine_ID = @machine_ID
		--	AND tp.tool_ID = @current_tool_ID

		--IF @lot_material_type_ID IS NOT NULL
		--BEGIN
		--	INSERT INTO [dbo].[lot_machine_cycle_parts]([machine_cycle_part_ID],[lot_material_type_ID],[lot_number]) 							
		--	SELECT ID, @lot_material_type_ID, @lot_number
		--	FROM machine_cycle_parts cp
		--	WHERE cp.cycle_ID = @cycle_ID
			
		--	UPDATE [dbo].[lot_material_types]
		--	SET lot_parts_used = ISNULL(@lot_parts_used, 0) + @lot_qty_per_part
		--	WHERE ID = @lot_material_type_ID
		--END

		--4/6/2016 - SS - CHANGED THESE TO BE SUB-QUERIES....OTHERWISE THEY ONLY PULL ONE LOT MATERIAL TYPE...NOT GOOD!
		INSERT INTO [dbo].[lot_machine_cycle_parts]([machine_cycle_part_ID],[lot_material_type_ID],[lot_number]) 							
		SELECT cp.ID, lm.lot_material_type_ID, lmt.current_lot
		FROM machine_cycle_parts cp
			JOIN machine_cycles mc ON cp.cycle_ID = mc.ID
			JOIN lot_machines lm ON mc.machine_ID = lm.machine_ID
			JOIN lot_material_types lmt ON lm.lot_material_type_ID = lmt.ID
			JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lmt.ID
			JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID
		WHERE cp.cycle_ID = @cycle_ID		

		-- if the lot has hit its max and has a next_lot set use it
		SELECT 
			@start_next_lot = COUNT(lmt.ID)
		FROM lot_machines lm 
			JOIN lot_material_types lmt ON lm.lot_material_type_ID = lmt.ID 
			JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
			JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
		WHERE 
			lmt.next_lot IS NOT NULL 
			AND lm.machine_ID = @machine_ID
			AND tp.tool_ID = @current_tool_ID
			AND lmt.lot_parts_used >= lmt.parts_per_lot

		IF (@start_next_lot > 0) 
		BEGIN
			UPDATE lot_material_types SET 
				lot_parts_used = 0,
				current_lot = next_lot,
				parts_per_lot = parts_per_next_lot,
				parts_per_lot_maximum_offset = parts_per_next_lot_maximum_offset,
				parts_per_lot_warning_offset = parts_per_next_lot_maximum_offset,
				next_lot = NULL,
				parts_per_next_lot = NULL,
				parts_per_next_lot_maximum_offset = NULL,
				parts_per_next_lot_warning_offset = NULL
			FROM lot_machines lm
				JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
				JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
			WHERE lm.machine_ID = @machine_ID
				AND lot_material_types.ID = lm.lot_material_type_ID
				AND tp.tool_ID = @current_tool_ID
		END
		ELSE
		BEGIN
			UPDATE lot_material_types SET 
					lot_parts_used = ISNULL(lot_parts_used, 0) + ltp.qty_per_part
			FROM lot_machines lm
				JOIN lot_tool_parts ltp ON ltp.lot_material_type_ID = lm.lot_material_type_ID
				JOIN tool_parts tp ON tp.ID = ltp.tool_part_ID 
			WHERE lm.machine_ID = @machine_ID
				AND lot_material_types.ID = lm.lot_material_type_ID
				AND tp.tool_ID = @current_tool_ID
		END

	END		

	-- GAUGING INCREMENT
	--IF @machine_ID IN (10010, 20010)
	--BEGIN
		EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value, 'RecordGauging'
		SELECT @tool_part_ID = tp.ID
		FROM tool_parts tp
			JOIN machine_tools mt ON tp.tool_ID = mt.tool_ID AND mt.machine_ID = @machine_ID
			JOIN machines m ON m.ID = @machine_ID AND mt.tool_ID = m.current_tool_ID
		UPDATE [dbo].[gauge_material_types] 
		SET gauge_parts_used = gauge_parts_used + 1 
		WHERE tool_part_ID = @tool_part_ID
							
	--END
							
	
	DECLARE @loopy_plc_action varchar(50);
	DECLARE @loopy_plc_tag varchar(50);
	DECLARE @loopy_plc_ip varchar(50);
	DECLARE @loopy_plc_data_type varchar(50);
	DECLARE @loopy_plc_value varchar(50);
	DECLARE loopy224 CURSOR FOR 
		SELECT plc_action, plc_ip, tag_name, data_type, tag_value FROM #Temp	

	OPEN loopy224

	FETCH NEXT FROM loopy224 INTO @loopy_plc_action, @loopy_plc_ip, @loopy_plc_tag, @loopy_plc_data_type, @loopy_plc_value
	WHILE @@FETCH_STATUS = 0
		BEGIN
			--do some shiz
			IF @loopy_plc_action = 'WRITE'
				SELECT MES_COMMON.dbo.fn_setPLCValue(@webserver, @loopy_plc_ip, @loopy_plc_tag, 0, @loopy_plc_data_type, @loopy_plc_value)

			FETCH NEXT FROM loopy224 INTO @loopy_plc_action, @loopy_plc_ip, @loopy_plc_tag, @loopy_plc_data_type, @loopy_plc_value
		END
	
	CLOSE loopy224;
	DEALLOCATE loopy224;
	
	-- Cleanup
	DROP TABLE #Temp;

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
