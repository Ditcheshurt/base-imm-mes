SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE [dbo].[sp_calculateHourlySummary] 
	@system_ID int,
	@the_time datetime
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @machine_ID int, @tool_ID int, @target_cycle_time float, @target_parts_per_hour float, @target_parts_per_interval float
	DECLARE @prev_hour int, @this_hour int
	DECLARE @prev_date datetime
	DECLARE @part_count int
	DECLARE @num_minutes float, @available_minutes float
	DECLARE @downtime_minutes float, @scheduled_downtime_minutes float
	DECLARE @parts_per_cycle int
	DECLARE @last_cycle_time datetime

	DECLARE @num_bad int
	DECLARE @date_stripped datetime
	DECLARE @s datetime --START OF DOWNTIME
	DECLARE @e datetime --END OF DOWNTIME

	DECLARE @total_target_parts int, @num_calcs int
	DECLARE @part_target_qty float, @avg_target_parts float, @part_goal float

	DECLARE @interval_duration float
	DECLARE @interval_ID int
	DECLARE @interval_start_time datetime, @interval_end_time datetime
	
	DECLARE @prior_interval_ID int
	DECLARE @prior_interval_start_time datetime, @prior_interval_end_time datetime

	IF @the_time IS NULL
		SET @the_time = GETDATE()

	SET @date_stripped = dbo.stripTime(@the_time)
	SET @scheduled_downtime_minutes = 0

	SET @part_count = 0
	SET @this_hour = DATEPART(HOUR, @the_time)
	SET @num_minutes = DATEPART(MINUTE, @the_time) + (DATEPART(SECOND, @the_time) / 60.0)

	SET @s = DATEADD(HOUR, @this_hour, @date_stripped)
	SET @e = @the_time

	SET @prev_date = DATEADD(DAY, -1, @the_time)
	SET @prev_date = dbo.stripTime(@prev_date)
	SET @prev_hour = DATEPART(HOUR, @prev_date)

	--4/6/2016 : CHANGE TO INTERVAL
	SET @interval_duration = MES_COMMON.dbo.fn_getTimeIntervalDuration(@system_ID)
	SET @interval_ID = MES_COMMON.dbo.fn_getTimeIntervalFromTime(@system_ID, @the_time)
	SET @interval_start_time = MES_COMMON.dbo.fn_getTimeIntervalStartTime(@system_ID, @the_time)
	SET @interval_end_time = MES_COMMON.dbo.fn_getTimeIntervalEndTime(@system_ID, @the_time)
	
	SET @prior_interval_ID = MES_COMMON.dbo.fn_getPriorTimeIntervalFromTime(@system_ID, @the_time)
	SET @prior_interval_start_time = MES_COMMON.dbo.fn_getTimeIntervalStartTime(@system_ID, DATEADD(MINUTE, 0 - @interval_duration, @the_time))
	SET @prior_interval_end_time = MES_COMMON.dbo.fn_getTimeIntervalEndTime(@system_ID, DATEADD(MINUTE, 0 - @interval_duration, @the_time))
	
	--BLEND THE NEW INTERVAL VARIABLES INTO THE OLD
	SELECT 
		@this_hour = DATEPART(HH, interval_start_time) 
	FROM 
		MES_COMMON.dbo.time_intervals 
	WHERE 
		ID = @interval_ID;
	SET @s = @interval_start_time
	SET @e = @interval_end_time
	SET @num_minutes = DATEDIFF(SECOND, @interval_start_time, @the_time) / 60.0

	PRINT 'THIS HOUR: ' + CAST(@this_hour as varchar(50))
	PRINT 'NUM MIN: ' + CAST(@num_minutes as varchar(50))
	PRINT 'START TIME: ' + CAST(@s as varchar(50))
	
	PRINT 'PREV DATE: ' + CAST(@prev_date as varchar(50))
	PRINT 'PREV HOUR: ' + CAST(@prev_hour as varchar(50))


	DECLARE loopyWERR4434 CURSOR FOR
	SELECT m.ID, t.ID as tool_ID, CAST(ISNULL(mt.target_cycle_time,0) as float)
	FROM machines m
		JOIN tools t ON m.current_tool_ID = t.ID 
		LEFT OUTER JOIN machine_tools mt ON m.ID = mt.machine_ID AND t.ID = mt.tool_ID
	WHERE system_ID = @system_ID
	ORDER BY m.ID;


	OPEN loopyWERR4434;

	FETCH NEXT FROM loopyWERR4434 INTO @machine_ID, @tool_ID, @target_cycle_time
	WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT 'MACH: ' + CAST(@machine_ID as varchar(50)) + ' TOOL: ' + CAST(@tool_ID as varchar(10))

			SET @part_count = 0
			PRINT 'A: ' + CAST(@machine_ID as varchar(50))

			--GET PARTS PER CYCLE
			SET @parts_per_cycle = 0
			--SELECT @parts_per_cycle = SUM(parts_per_tool)
			--FROM tool_parts
			--WHERE tool_ID = @tool_ID
			SELECT @parts_per_cycle = COUNT(*)
			FROM machine_active_parts
			WHERE machine_ID = @machine_ID
				AND tool_ID = @tool_ID
				AND end_timestamp IS NULL
				AND is_active > 0

			SET @target_parts_per_hour = (3600.0 / @target_cycle_time) * @parts_per_cycle

			PRINT 'B: ' + CAST(@target_parts_per_hour as varchar(50))

			--UPDATE PRIOR HOUR IF MINUTE < 15
			IF @num_minutes < 5
				BEGIN
					IF @system_ID > 1000
						SET @part_count = 0

						SELECT @part_count = COUNT(*) 
						FROM machine_cycles mc 
							JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
						WHERE mc.machine_ID = @machine_ID
							AND mc.tool_ID = @tool_ID
							AND mc.cycle_time > DATEADD(HOUR, -2, @prev_date)
							AND mc.cycle_time < DATEADD(HOUR, 1, @prev_date)
							AND MES_COMMON.dbo.fn_getTimeIntervalFromTime(@system_ID, mc.cycle_time) = @prior_interval_ID

					SET @prev_date = dbo.stripTime(@prev_date)

					IF EXISTS(SELECT * FROM hourly_summary WHERE system_ID = @system_ID AND machine_ID = @machine_ID AND tool_ID = @tool_ID AND the_date = @prev_date AND the_hour = @prev_hour)
						UPDATE hourly_summary
						SET part_qty = @part_count, scrap_denominator = @part_count, total_minutes = 60
						WHERE system_ID = @system_ID 
							AND machine_ID = @machine_ID 
							AND tool_ID = @tool_ID 
							AND the_date = @prev_date 
							AND the_hour = @prev_hour
					ELSE
						INSERT INTO hourly_summary (system_ID, machine_ID, tool_ID, the_date, the_hour, part_qty, scrap_denominator, total_minutes) VALUES (
							@system_ID, 
							@machine_ID,
							@tool_ID,
							@prev_date,
							@prev_hour,
							@part_count,
							@part_count,
							60
						)
				END

			--UPDATE CURRENT HOUR
			--IF DATEDIFF(MINUTE, @the_time, GETDATE()) <= 5
					IF @system_ID < 1000
						BEGIN 
							--PERFORMANCE
							SET @part_count = 0
							SELECT @part_count = COUNT(*)
							FROM machine_cycles mc 
								JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
							WHERE mc.machine_ID = @machine_ID
								AND mc.tool_ID = @tool_ID
								AND mc.cycle_time > DATEADD(HOUR, -2, @the_time)
								AND mc.cycle_time < DATEADD(HOUR, 1, @the_time)
								AND MES_COMMON.dbo.fn_getTimeIntervalFromTime(@system_ID, mc.cycle_time) = @interval_ID
								
							PRINT 'Parts this hour: ' + CAST(@part_count as varchar(50))

							--QUALITY
							SET @num_bad = 0
							SELECT @num_bad = COUNT(*)
							FROM machine_defect_log dl
							WHERE dl.machine_ID = @machine_ID
								AND tool_ID = @tool_ID
								AND dl.defect_time > DATEADD(HOUR, -2, @the_time)
								AND dl.defect_time < DATEADD(HOUR, 1, @the_time)
								AND dl.disposition = 'SCRAP'
								AND MES_COMMON.dbo.fn_getTimeIntervalFromTime(@system_ID, dl.defect_time) = @interval_ID

							PRINT 'Bad this hour: ' + CAST(@num_bad as varchar(50))

							--AVAILABILITY
							SET @downtime_minutes = 0
							--SELECT @downtime_minutes = SUM(
							--		CASE 
							--			WHEN dl.change_time > @s AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
							--				dl.time_in_state
							--			WHEN dl.change_time < @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @s
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
							--				DATEDIFF(SECOND, @s, DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time)) / 60.0
							--			WHEN dl.change_time > @s 
							--					AND dl.change_time < @e
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
							--				DATEDIFF(SECOND, dl.change_time, @e) / 60.0
							--			WHEN dl.change_time < @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
							--				DATEDIFF(SECOND, @s, @e) / 60.0
							--			WHEN dl.change_time <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @s THEN 0
							--			WHEN dl.change_time > @e THEN 0
							--		END),
							--	  @scheduled_downtime_minutes = ISNULL(SUM(									
							--		CASE 
							--			WHEN dr.count_against = 0 AND dl.change_time > @s AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
							--				dl.time_in_state
							--			WHEN dr.count_against = 0 AND dl.change_time < @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @s
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @e THEN 
							--				DATEDIFF(SECOND, @s, DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time)) / 60.0
							--			WHEN dr.count_against = 0 AND dl.change_time > @s 
							--					AND dl.change_time < @e
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
							--				DATEDIFF(SECOND, dl.change_time, @e) / 60.0
							--			WHEN dr.count_against = 0 AND dl.change_time < @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) > @e THEN 
							--				DATEDIFF(SECOND, @s, @e) / 60.0
							--			WHEN dr.count_against = 0 AND dl.change_time <                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               @s 
							--					AND DATEADD(SECOND, (dl.time_in_state*60.0), dl.change_time) < @s THEN 0
							--			WHEN dr.count_against = 0 AND dl.change_time > @e THEN 0
							--		END
							--	  ), 0)
							--FROM machine_downtime_log dl
							--	LEFT OUTER JOIN machine_downtime_reasons dr ON dl.reason_ID = dr.ID
							--WHERE dl.machine_ID = @machine_ID
							--	AND dl.up = 0
							--	AND change_time > @s - 1

							--SET @downtime_minutes = dbo.fn_calculateMachineDowntime(@s, @e, @machine_ID) 
							SET @downtime_minutes = dbo.fn_calculateMachineDowntimeFromLog(@s, @e, @machine_ID) 
							SET @scheduled_downtime_minutes = dbo.fn_calculateMachineScheduledDowntimeFromLog(@s, @e, @machine_ID) 

							IF @downtime_minutes > @num_minutes
								SET @downtime_minutes = @num_minutes

							IF @downtime_minutes IS NULL
								SET @downtime_minutes = 0

							IF @scheduled_downtime_minutes > @num_minutes
								SET @scheduled_downtime_minutes = @num_minutes

							IF @scheduled_downtime_minutes IS NULL
								SET @scheduled_downtime_minutes = 0

							SET @available_minutes = @num_minutes - @scheduled_downtime_minutes
						END

					SET @target_parts_per_interval = @target_parts_per_hour / 60.0
					PRINT 'C: ' + CAST(@target_parts_per_interval as varchar(50))

					IF EXISTS(SELECT * FROM hourly_summary WHERE system_ID = @system_ID AND machine_ID = @machine_ID AND tool_ID = @tool_ID AND the_date = @date_stripped AND the_hour = @this_hour)
						BEGIN
							SELECT @total_target_parts = total_target_parts + @target_parts_per_hour, @num_calcs = num_calcs + 1
							FROM hourly_summary 
							WHERE system_ID = @system_ID AND machine_ID = @machine_ID AND tool_ID = @tool_ID AND the_date = @date_stripped AND the_hour = @this_hour

							SET @avg_target_parts = @total_target_parts / @num_calcs --AVERAGE TARGET PARTS PER HOUR
							SET @part_goal = 0

							IF @avg_target_parts > 0
								BEGIN
									SET @part_target_qty = ((@available_minutes - @downtime_minutes) / @available_minutes) * @avg_target_parts * (@available_minutes / 60.0)
									SET @part_goal = @target_parts_per_interval
								END
							ELSE
								SET @part_target_qty = 0

							UPDATE hourly_summary
							SET part_qty = @part_count,
								total_target_parts = ISNULL(total_target_parts, 0) + @target_parts_per_hour,
								scrap_denominator = @part_count,
								scrap_numerator = (@part_count - @num_bad),
								part_target_qty = @part_target_qty, --ISNULL(part_target_qty, 0) + @target_parts_per_interval,
								part_goal = ISNULL(part_goal,0) + ISNULL(@part_goal, 0),
								downtime_minutes = @downtime_minutes,
								scheduled_minutes = @scheduled_downtime_minutes,
								total_minutes = @num_minutes,
								last_updated = GETDATE(),
								num_calcs = ISNULL(num_calcs, 0) + 1
							WHERE system_ID = @system_ID AND machine_ID = @machine_ID AND tool_ID = @tool_ID AND the_date = @date_stripped AND the_hour = @this_hour

						END
					ELSE
						BEGIN
							SET @part_target_qty = ((@num_minutes - @downtime_minutes) / @num_minutes) * @target_parts_per_hour

							INSERT INTO hourly_summary (system_ID, machine_ID, tool_ID, the_date, 
														the_hour, part_qty, part_target_qty, 
														part_goal, total_target_parts, scrap_numerator, 
														scrap_denominator, downtime_minutes, scheduled_minutes, 
														total_minutes, num_calcs) VALUES (
								@system_ID, @machine_ID, @tool_ID, @date_stripped,
								@this_hour,	@part_count, @part_target_qty,
								@target_parts_per_interval, @target_parts_per_hour, (@part_count - @num_bad),
								@part_count, @downtime_minutes,	@scheduled_downtime_minutes,
								@num_minutes, 1
							)
						END


			FETCH NEXT FROM loopyWERR4434 INTO @machine_ID, @tool_ID, @target_cycle_time
		END


	CLOSE loopyWERR4434;
	DEALLOCATE loopyWERR4434;
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
