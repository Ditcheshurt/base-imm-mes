SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE sp_cloneDowntimeReasons
	-- Add the parameters for the stored procedure here
	@from_machine_type_ID int,
	@to_machine_type_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ID int
    DECLARE @parent_ID int
    DECLARE @reason_description varchar(150)
    DECLARE @count_against bit
    DECLARE @tool_change_ignore bit
	DECLARE @active bit
    DECLARE @force_comment bit
    DECLARE @machine_type_ID int

	SELECT [ID]
      ,[parent_ID]
      ,[reason_description]
      ,[count_against]
      ,[tool_change_ignore]
      ,[active]
      ,[force_comment]
      ,[machine_type_ID]
	INTO #TempReasons
	FROM [machine_downtime_reasons] 
	WHERE machine_type_ID = @from_machine_type_ID

	/** create parents **/
	DECLARE loopy CURSOR FOR 
		SELECT [ID]
		  ,[parent_ID]
		  ,[reason_description]
		  ,[count_against]
		  ,[tool_change_ignore]
		  ,[active]
		  ,[force_comment]
		  ,[machine_type_ID]
		FROM #TempReasons
		WHERE parent_ID = 0

	OPEN loopy;

	FETCH NEXT FROM loopy INTO @ID, @parent_ID, @reason_description, @count_against, @tool_change_ignore, @active, @force_comment, @machine_type_ID
	WHILE @@FETCH_STATUS = 0
		BEGIN
		DECLARE @parent_IDENT int

		INSERT INTO machine_downtime_reasons(parent_ID, reason_description, count_against, tool_change_ignore, active, force_comment, machine_type_ID) 
			VALUES (@parent_ID, @reason_description, @count_against, @tool_change_ignore, @active, @force_comment, @to_machine_type_ID);
		
		SET @parent_IDENT = SCOPE_IDENTITY();

		-- create children
		INSERT INTO machine_downtime_reasons(parent_ID, reason_description, count_against, active, tool_change_ignore, force_comment, machine_type_ID)
		SELECT @parent_IDENT, reason_description, count_against, active, tool_change_ignore, force_comment, @to_machine_type_ID
		FROM #TempReasons
		WHERE parent_ID = @ID
		
		-- create all the other kids
		-- TODO		

		FETCH NEXT FROM loopy INTO @ID, @parent_ID, @reason_description, @count_against, @tool_change_ignore, @active, @force_comment, @machine_type_ID
		END


	CLOSE loopy;
	DEALLOCATE loopy;
	/** end create parents **/

	DROP TABLE #TempReasons

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
