SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE sp_CUSTOM_POPULATE_AGILE_FILES_2 AS
BEGIN

		TRUNCATE TABLE custom_agile_files

		DECLARE @pn varchar(50)
		DECLARE @sql varchar(5000)
		DECLARE @i int

		SET @i = 0

		DECLARE loopyIUYKJGKJG CURSOR FOR
			SELECT part_number 
			FROM parts 
			ORDER BY part_number;

		OPEN loopyIUYKJGKJG;
		FETCH NEXT FROM loopyIUYKJGKJG INTO @pn

		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @i = @i + 1

				PRINT 'LOOP VARIABLE: ' + CAST(@i as varchar(20))
				SET @sql = 'INSERT INTO custom_agile_files (SPN_NUMBER, ITEM_NUMBER, AGILE_ID, FILE_TYPE, FILE_PATH, FILENAME, DESCRIPTION, TEXT31) 
					SELECT SPN_NUMBER, ITEM_NUMBER, AGILE_ID, FILE_TYPE, FILE_PATH, FILENAME, DESCRIPTION, TEXT31
					FROM OPENQUERY(AGILE, ''select ''''' + @pn + ''''' as spn_number, i.item_number, to_char(F.ID) as AGILE_ID, F.file_type, (''''\\agileapp\Files\'''' || substr(lpad(to_char(F.ID),11,''''0''''),1,3) || ''''\'''' || substr(lpad(to_char(F.ID),11,''''0''''),4,3) || ''''\'''' || substr(lpad(to_char(F.ID),11,''''0''''),7,3) || ''''\agile'''' || to_char(F.ID) || ''''.'''' || file_type) as FILE_PATH, f.filename, a.description, p3.text31
				   from AGILE.ITEM i, 
					AGILE.REV r, 
					AGILE.ATTACHMENT a, 
					AGILE.VERSION v, 
					AGILE.FILES f, 
					AGILE.VERSION_FILE_MAP vfm,
				   AGILE.ATTACHMENT_MAP am, 
				   AGILE.CHANGE c, 
				   AGILE.PAGE_THREE p3
					  where p3.id = i.id and
					  r.item = i.id and r.released = 1 and
					  r.latest_flag = 1 and
					  a.latest_vsn = v.id and
					  vfm.version_id = v.id and vfm.file_id = f.id and
					  v.attach_id = a.id and
					  am.parent_id = i.id and c.id = am.Parent_id2 and
					  am.latest_vsn = v.id and
					  i.latest_released_eco = c.id and
					  i.item_number in ( Select distinct(b.item_number) FROM AGILE.BOM b, AGILE.ITEM i
											  where i.id = item and b.item_number like ''''QWM%'''' CONNECT BY PRIOR COMPONENT = ITEM
											  START WITH ITEM = (SELECT ID FROM AGILE.ITEM WHERE ITEM_NUMBER=''''' + @pn + ''''')) '')'
					--SELECT @pn as SPN
					EXEC(@sql)

					IF @i % 10 = 0
						WAITFOR DELAY '00:00:05'

				FETCH NEXT FROM loopyIUYKJGKJG INTO @pn
			END

		CLOSE loopyIUYKJGKJG
		DEALLOCATE loopyIUYKJGKJG

	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
