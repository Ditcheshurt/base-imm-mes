SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 6/13/2018
-- Description:	This will modify or add users to the machine parameter alert email list table
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_UpdateAddMachineMonitorEmailUsers] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tool_ID int = 0,
	@data_point_ID int = 0,
	@op_list varchar(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	DECLARE @mtcl_ID int
	DECLARE @op_tbl TABLE (ID int, op_ID int)

	SELECT @mtcl_ID = mtcl.ID
	FROM machine_tool_ctrl_limits mtcl
	WHERE mtcl.machine_ID = @machine_ID AND mtcl.tool_ID = @tool_ID AND mtcl.data_point_ID = @data_point_ID

	INSERT INTO @op_tbl
	SELECT ID, TRY_CAST(items AS int) FROM dbo.Split(@op_list,',')

	DELETE FROM @op_tbl WHERE op_ID = 0

	-- deactivates
	UPDATE machine_monitor_email_list
	SET active = 0, last_updated = GETDATE()
	WHERE operator_ID NOT IN (SELECT op_ID FROM @op_tbl)
	AND machine_tool_ctrl_limit_ID = @mtcl_ID
	AND active = 1

	-- reactivates
	UPDATE machine_monitor_email_list
	SET active = 1, last_updated = GETDATE()
	WHERE operator_ID IN (SELECT op_ID FROM @op_tbl)
	AND machine_tool_ctrl_limit_ID = @mtcl_ID
	AND active = 0

	--inserts
	INSERT INTO machine_monitor_email_list
	SELECT @mtcl_ID, op_ID, 1, GETDATE()
	FROM @op_tbl
	WHERE op_ID NOT IN (SELECT operator_ID FROM machine_monitor_email_list WHERE machine_tool_ctrl_limit_ID = @mtcl_ID)

	SELECT * FROM machine_monitor_email_list WHERE machine_tool_ctrl_limit_ID = @mtcl_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
