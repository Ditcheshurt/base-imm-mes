SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Sass
-- Create date: 10/2/15
-- Description:	triggered when plc tag shows part ready, validates wip part with barcode exists
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_RecordCycleStarted] 
	@tag_ID varchar(10),
	@tag_value varchar(30),
	@tag_desc varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC NYSUS_PLC_POLLER.dbo.sp_addToLoggy @tag_ID, @tag_desc,	@tag_value,	'RecordCycleStarted'
	
	IF @tag_value = 'False'
		RETURN
	
	DECLARE @tag_name varchar(100)
	DECLARE @machine_ID int, @op_order int, @group_ID int
	DECLARE @min_op_order int
	DECLARE @parts_per_cycle int
	DECLARE @barcode_data varchar(100)
	DECLARE @barcode_empty_error int = 1001;
	DECLARE @wip_part_not_found_error int = 1002;
	DECLARE @wip_part_not_ready_error int = 1003;
	DECLARE @plc_clear_fail_status int = 0;
	DECLARE @plc_tag varchar(50);
	DECLARE @plc_ip varchar(50), @plc_ID int;
	DECLARE @i int = 0;
	DECLARE @webserver varchar(50) = 'localhost';
	DECLARE @part_ready_tag_name varchar(100) = 'PART_READY_TO_MES'
	DECLARE @data_rcvd_tag_name varchar(100) = 'DATA_RCVD_TO_PLC'
	DECLARE @part_status_tag_name varchar(100) = 'PART_STATUS_TO_PLC'
	DECLARE @serial bigint = 0
	DECLARE @has_prior_op_group bit = 0

	SELECT @tag_desc = description, @plc_ID = plc_ID, @tag_name = name
	FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
	WHERE ID = @tag_ID

	--get the station ID
	SET @machine_ID = dbo.fn_getmachineIDFromTagDesc(@tag_desc)

	SELECT @group_ID = m.machine_group_ID, @op_order = m.op_order, @parts_per_cycle = SUM(tp.parts_per_tool)
	FROM machines m
		JOIN machine_tools mt ON m.ID = mt.machine_ID AND m.current_tool_ID = mt.tool_ID
		JOIN tools t ON mt.tool_ID = t.ID
		JOIN tool_parts tp ON t.ID = tp.tool_ID
	WHERE m.ID = @machine_ID
	GROUP BY m.machine_group_ID, m.op_order

	SELECT @min_op_order = MIN(op_order)
	FROM machines 
	WHERE machine_group_ID = @group_ID

	IF EXISTS(SELECT * FROM machine_groups WHERE next_op_group = @group_ID)
		SET @has_prior_op_group = 1

	SELECT @plc_ip = ip_address 
	FROM NYSUS_PLC_POLLER.dbo.nysus_plcs 
	WHERE ID = @plc_ID

	--LOOP THROUGH ALL PARTS THAT ARE PART OF THIS CYCLE
	WHILE @i < @parts_per_cycle
		BEGIN
			--get the current barcode for the part
			SET @barcode_data = NULL

			SELECT @barcode_data = value
			FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags
			WHERE description = @tag_desc 
				AND name LIKE '%.BARCODE_IN_CYCLE_TO_MES_' + CAST(@i as varchar(2)) + '_.LEN'

			-- make sure external_serial is not null or empty, else validate part exists
			IF @barcode_data IS NULL OR @barcode_data = ''
				BEGIN
					BREAK  --EXIT THE WHILE LOOP
				END
			ELSE
				BEGIN
					DECLARE @mipp_part_serial int
					DECLARE @last_machine_completed_ID int
					SET @mipp_part_serial = NULL
					SET @serial = dbo.fn_getSerialNumberFromFullBarcode(@barcode_data)
					
					--8/3/2016 - ADDED CHECK IF EXISTS...IF NOT, INSERT AS BYPASSED
					IF EXISTS(
							SELECT TOP 1 ID 
							FROM machine_cycle_part_history 
							WHERE serial = @serial
								AND machine_ID = @machine_ID
								AND cycle_start_time IS NULL
								AND check_result IN ('INSERTED', 'OKAY_TO_RUN'))
						UPDATE machine_cycle_part_history 
						SET cycle_start_time = GETDATE()
						WHERE serial = @serial
							AND machine_ID = @machine_ID
							AND cycle_start_time IS NULL
							AND check_result IN ('INSERTED', 'OKAY_TO_RUN')
					ELSE
						INSERT INTO machine_cycle_part_history (serial, machine_ID, check_time, cycle_start_time, check_result) VALUES (
							@serial, 
							@machine_ID, 
							GETDATE(), 
							GETDATE(),
							'BYPASSED'
						)

					IF @op_order = @min_op_order AND @has_prior_op_group = 0
						BEGIN
							--AT FIRST STATION NEED TO INSERT PART
							IF NOT EXISTS (SELECT serial FROM machine_in_process_parts WHERE serial = @serial)
								INSERT INTO machine_in_process_parts (serial, created_time, last_machine_started_ID, last_machine_started_order, last_machine_started_time) VALUES (
									@serial,
									GETDATE(),
									@machine_ID,
									@op_order,
									GETDATE()
								)
						END	
					ELSE
						UPDATE machine_in_process_parts
						SET last_machine_started_ID = @machine_ID, last_machine_started_order = @op_order, last_machine_started_time = GETDATE()
						WHERE serial = @serial	
				END

			SET @i = @i + 1
		END



END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
