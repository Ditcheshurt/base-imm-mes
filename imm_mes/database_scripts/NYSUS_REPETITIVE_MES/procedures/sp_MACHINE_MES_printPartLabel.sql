SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 5/8/2017
-- Description:	Original code from Sass, modified for reptitive MES by R.Kahle
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_printPartLabel] 
	-- Add the parameters for the stored procedure here
	@mcp_ID int = 0, 
	@select_return bit = 1,
	@reprint_mark varchar(10) = '',
	@alt_printer_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
    DECLARE @op_name varchar(50), @op_shift varchar(10)
    DECLARE @part_number varchar(50), @part_desc varchar(50), @prog_name varchar(50), @tool_short varchar(20)
	DECLARE @cust_pn varchar(150)
    DECLARE @serial varchar(50), @ext_serial varchar(50), @int_serial varchar(50)
    DECLARE @printer_ID int, @print_text varchar(MAX) 
	DECLARE @revision_list varchar(50)
	DECLARE @datestamp varchar(10)
	DECLARE @shift varchar(1)
	DECLARE @time datetime
    DECLARE @position varchar(10)
	DECLARE @machine_ID int
	DECLARE @machine_num varchar(20)
	DECLARE @build_order varchar(20)
	DECLARE @sequence varchar(20)
	DECLARE @vin varchar(20)
	DECLARE @options_1 varchar(1000), @i int
	DECLARE @options_2 varchar(1000)
	DECLARE @module_ID int
	DECLARE @copy_to_printer_ID int
	DECLARE @bom_ID varchar(30)
    DECLARE @rack_pos varchar(50), @rack_seq varchar(50)
    DECLARE @ship_to_name varchar(10)
	DECLARE @insert_ts datetime
	DECLARE @part_ID int
	DECLARE @tool_ID int
	DECLARE @tool_part_ID int
	DECLARE @print_template_ID int = 0
	DECLARE @part_order int
	DECLARE @REV_NUMBER varchar(20)
	DECLARE @lot_number varchar(50)
	DECLARE @mat_number varchar(50)
    
	SET @copy_to_printer_ID = 0
	SET @rack_pos = ''
	SET @rack_seq = ''
	SET @REV_NUMBER = ''

	--GET OPERATOR INFO
	SET @op_name = ''
	SELECT @op_name = ISNULL(name, ''), @op_shift = ISNULL(CAST(s.shift_number AS varchar), '')
	FROM MES_COMMON.dbo.operators o
	LEFT JOIN MES_COMMON.dbo.shifts s ON s.ID = o.[shift]
	WHERE o.ID = (SELECT TOP 1 operator_ID
				FROM container_parts
				WHERE machine_cycle_part_ID = @mcp_ID
				ORDER BY ID DESC
				)

    --GET PART INFO
	SELECT TOP (1)
		--mcp.[cycle_ID]
		@machine_ID = m.[ID]
		,@machine_num = m.[machine_number]
		--,tp.[tool_ID]
		--,tp.[part_ID]
		,@rack_seq = ISNULL(cp.container_ID, 0)
		,@rack_pos = ISNULL(cp.[container_position], 0)
		,@int_serial = mcp.[serial_number]
		,@ext_serial = mcp.[full_barcode]
		,@part_number = p.[part_number]
		,@cust_pn = p.[external_base]
		,@part_desc = p.[part_desc]
		,@prog_name = pg.[part_group_description]
		,@insert_ts = mcp.[insert_time]
		,@part_ID = p.ID
		,@tool_short = t.short_description
		,@tool_ID = tp.tool_ID
		,@tool_part_ID = tp.ID
		,@print_template_ID = ISNULL(p.print_template_ID, 0)
		,@part_order = tp.part_order
		,@REV_NUMBER = mcp.revision_number
		,@mat_number = tmt.mat_number
	FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_parts] mcp
		JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
		JOIN machines m ON m.current_tool_ID = tp.tool_ID
		JOIN parts p ON p.ID = tp.part_ID
		JOIN tools t ON t.ID = tp.tool_ID
		JOIN part_group_hrt pg ON pg.ID = p.part_group_id
		LEFT JOIN container_parts cp ON cp.machine_cycle_part_ID = mcp.ID
		LEFT JOIN CUSTOM_tool_mat_tracking tmt ON tmt.tool_ID = tp.tool_ID AND tmt.start_timestamp < mcp.insert_time AND (tmt.end_timestamp IS NULL OR tmt.end_timestamp > mcp.insert_time)
	WHERE mcp.ID = @mcp_ID

	SELECT @lot_number = mct.lot_number
	FROM machine_cycle_parts mcp
	JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
	LEFT OUTER JOIN CUSTOM_machine_lot_tracking mlt ON mlt.machine_ID = mc.machine_ID AND mlt.start_timestamp < mcp.insert_time AND (mlt.end_timestamp IS NULL or mlt.end_timestamp > mcp.insert_time)
	JOIN CUSTOM_material_certification_test mct ON mct.ID = mlt.material_cert_test_ID
	WHERE mcp.ID = @mcp_ID


	--DO NOT PRINT A LABEL IF NO-LABEL IS SELECTED
	IF @print_template_ID = 0
		RETURN

	SET @serial = CONCAT(@part_number, CONVERT(varchar(50), @insert_ts, 112), DATEPART(HOUR,@insert_ts)*3600+DATEPART(MINUTE,@insert_ts)*60+DATEPART(SECOND,@insert_ts))

	--SELECT @part_number = part_number, @part_desc = part_desc
	--FROM wip_part_setup 
	--WHERE id = (SELECT part_ID FROM wip_parts WHERE id = @wip_part_ID)
		    
	--SELECT @serial = internal_serial, @machine_ID = line_ID, @ext_serial = ISNULL(external_serial, ''), @bom_ID = RIGHT('000000' + CAST(ISNULL(bom_ID, 0) as varchar(30)), 5)
	--FROM wip_parts 
	--WHERE ID = @wip_part_ID 

	--SELECT @build_order = ISNULL(build_order , ''), 
	--	@sequence = ISNULL(sequence, ''), 
	--	@vin = ISNULL(vin, ''), 
	--	@module_ID = ISNULL(ID, 0)
	--FROM modules 
	--WHERE id = (SELECT used_by_module_ID FROM wip_parts WHERE id = @wip_part_ID)
	
	--SELECT @rack_pos = CAST(si.lane_position as varchar(20)),
	--	@rack_seq = RIGHT(CAST(sl.rack_sequence as varchar(20)), 3)
	--FROM shipment_items si
	--	JOIN shipment_lanes sl ON si.lane_ID = sl.ID 
	--WHERE si.mod_ID = @module_ID 
	
    --KEY PART LIST
	--SET @options_1 = ''
	--SET @options_2 = ''

	--DECLARE @hvac varchar(20) = ''

	--SELECT @hvac = 'HVAC: ' + RIGHT(c.part_number, 5)
	--FROM components_in c
	--	JOIN instructions i ON i.station_ID = 140010 
	--		AND i.part_order = 10 
	--		AND i.instr_order = 10
	--		AND c.part_number LIKE i.part_number 
	--WHERE c.veh_id = (SELECT used_by_module_ID FROM wip_parts WHERE id = @wip_part_ID)

	--SET @options_1 = @hvac

    
    --GET PRINTER INFO
    SELECT @printer_ID = ISNULL(printer_ID, 0)
    FROM machine_printers
    WHERE machine_ID = @machine_ID
		AND part_order = @part_order 
		AND label_type = 'PART'

	--IF @alt_printer_ID <> 0
	--	SET @printer_ID = @alt_printer_ID
	--ELSE
	--	BEGIN
	--		SELECT @alt_printer_ID = ISNULL(alt_part_printer_ID, 0)
	--		FROM tool_parts
	--		WHERE tool_ID = @tool_ID
	--			AND part_ID = @part_ID

	--		IF @alt_printer_ID <> 0
	--			SET @printer_ID = @alt_printer_ID
	--	END
    
    SELECT @print_text = template_text
    FROM print_templates 
    WHERE id = @print_template_ID


	IF DATEPART(hh, @time) >= 6.5 AND DATEPART(hh, @time) + CAST(DATEPART(mi, @time) As float)/60.0 < CAST(18.5 As float)
		SET @shift = 'A'
	ELSE
		SET @shift = 'B'
	SET @datestamp = CONVERT(varchar(10), GETDATE(), 1) + @shift

	SET @time = GETDATE()


	SET @print_text = REPLACE(@print_text, '<<press_num>>', ISNULL(@machine_num,''))
    SET @print_text = REPLACE(@print_text, '<<part_number>>', @part_number)
    SET @print_text = REPLACE(@print_text, '<<part_desc>>', @part_desc)
	SET @print_text = REPLACE(@print_text, '<<tool_short>>', ISNULL(@tool_short,''))
	SET @print_text = REPLACE(@print_text, '<<prog_name>>', @prog_name)
    SET @print_text = REPLACE(@print_text, '<<options_1>>', ISNULL(@options_1,''))
    SET @print_text = REPLACE(@print_text, '<<options_2>>', ISNULL(@options_2,''))
    
    SET @print_text = REPLACE(@print_text, '<<date>>', CONVERT(varchar(20), GETDATE(), 101))
    SET @print_text = REPLACE(@print_text, '<<time>>', CONVERT(varchar(20), GETDATE(), 108))
    SET @print_text = REPLACE(@print_text, '<<DATE>>', CONVERT(varchar(20), GETDATE(), 101))
    SET @print_text = REPLACE(@print_text, '<<TIME>>', CONVERT(varchar(20), GETDATE(), 108))
    
    SET @print_text = REPLACE(@print_text, '<<operator>>', ISNULL(@op_name,''))
	SET @print_text = REPLACE(@print_text, '<<op_shift>>', ISNULL(@op_shift,''))
    
    SET @print_text = REPLACE(@print_text, '<<rack_pos>>', ISNULL(@rack_pos,''))
    SET @print_text = REPLACE(@print_text, '<<rack_seq>>', ISNULL(@rack_seq,''))
    
	IF @part_desc IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<part_desc>>', @part_desc)

	IF @tool_part_ID IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<tool_part>>', RIGHT('00'+CAST(@tool_part_ID AS VARCHAR(2)),2))

	IF @serial IS NOT NULL
		BEGIN
			SET @print_text = REPLACE(@print_text, '<<serial>>', @serial)
			SET @print_text = REPLACE(@print_text, '<<odometer_serial>>', @serial)
			SET @print_text = REPLACE(@print_text, '<<short_serial>>', @serial)
			SET @print_text = REPLACE(@print_text, '<<serial_number>>', @serial)
		END 
	IF @int_serial IS NOT NULL
		BEGIN
			SET @print_text = REPLACE(@print_text, '<<int_serial>>', @int_serial)
		END
	IF @ext_serial IS NOT NULL
		BEGIN
			SET @print_text = REPLACE(@print_text, '<<ext_serial>>', @ext_serial)
		END

	IF @cust_pn IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<cust_pn>>', ISNULL(@cust_pn,''))
	ELSE
		SET @print_text = REPLACE(@print_text, '<<cust_pn>>', '')

	IF @position IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<position>>', @position)

	IF @revision_list IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<revision_list>>', @revision_list)

	IF @REV_NUMBER IS NOT NULL
		SET @print_text = REPLACE(@print_text, '<<rev_number>>', @REV_NUMBER)

	SET @print_text = REPLACE(@print_text, '<<date>>', @datestamp)
    
	
	SET @print_text = REPLACE(@print_text, '<<reprint_mark>>', ISNULL(@reprint_mark,''))

	SET @print_text = REPLACE(@print_text, '<<lot_number>>', ISNULL(@lot_number,'N/A'))
	SET @print_text = REPLACE(@print_text, '<<mat_number>>', ISNULL(@mat_number,'N/A'))
        
    --insert record into print queue
	--added 5 minute @mcp_ID check to prevent duplicate labels from double disposition - JV 5/21/2018
	IF @printer_ID <> 0 AND @mcp_ID NOT IN (SELECT
											mod_ID
											FROM
											print_queue
											WHERE
											queue_time > DATEADD(minute, -5, GETDATE()))
							
		INSERT INTO print_queue (mod_ID, printer_ID, print_text, queue_time, qty) VALUES (
			@mcp_ID, 
			@printer_ID, 
			@print_text, 
			GETDATE(),
			1)

	IF @select_return = 1
		SELECT @printer_ID as printer_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
