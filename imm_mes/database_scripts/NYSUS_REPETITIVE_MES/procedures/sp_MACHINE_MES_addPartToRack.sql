SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 6-15-2016
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_addPartToRack] 
	-- Add the parameters for the stored procedure here
	@machine_group_ID int = 0, 
	@machine_cycle_part_ID int = 0,
	@operator_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @parts_per_rack int 
	DECLARE @rack_ID int = 0
	DECLARE @max_pack_order int = 0
	DECLARE @rack_count int = 0

	--GET PARTS PER RACK
	SELECT @parts_per_rack = rack_layers * rack_boxes_across * rack_boxes_deep * rack_parts_per_box 
	FROM machine_groups 
	WHERE ID = @machine_group_ID

	--GET EXISTING OPEN RACK
	SELECT @rack_ID = ID
	FROM machine_group_racks
	WHERE machine_group_ID = @machine_group_ID
		AND end_time IS NULL

	IF ISNULL(@rack_ID, 0) = 0
		BEGIN
			--INSERT NEW RACK
			INSERT INTO machine_group_racks (machine_group_ID, start_time) VALUES (
				@machine_group_ID,
				GETDATE()
			)

			SET @rack_ID = @@IDENTITY 
		END

	--GET MAX PACK ORDER
	SELECT @max_pack_order = MAX(machine_group_rack_part_order)
	FROM machine_cycle_parts
	WHERE machine_group_rack_ID = @rack_ID 

	--INCREMENT BY 1
	SET @max_pack_order = ISNULL(@max_pack_order, 0) + 1

	--ADD PART TO RACK
	UPDATE machine_cycle_parts
	SET machine_group_rack_ID = @rack_ID,
		machine_group_rack_part_order = @max_pack_order,
		machine_group_rack_pack_time = GETDATE(),
		machine_group_rack_pack_operator_ID = @operator_ID
	WHERE ID = @machine_cycle_part_ID

	--GET NEW COUNT To SEE IF IT IS FULL
	SELECT @rack_count = COUNT(*)
	FROM machine_cycle_parts 
	WHERE machine_group_rack_ID = @rack_ID 

	--IF FULL, SET END TIME
	IF @rack_count >= @parts_per_rack
		UPDATE machine_group_racks
		SET end_time = GETDATE()
		WHERE ID = @rack_ID

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
