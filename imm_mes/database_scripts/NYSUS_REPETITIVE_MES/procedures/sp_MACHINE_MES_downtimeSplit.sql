SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_downtimeSplit] 
	-- Add the parameters for the stored procedure here
	@downtime_ID int = 0, 
	@split_minutes float = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @time_in_state float
	DECLARE @remaining_minutes float

	--SPLIT_MINUTES IS THE AMOUNT IN THE 'FRONT' OF THE DOWNTIME THAT YOU WANT TO SPLIT OFF
	
	--GET TIME IN STATE
	SELECT @time_in_state = time_in_state 
	FROM machine_downtime_log
	WHERE ID = @downtime_ID

	IF ISNULL(@split_minutes, 0) > 0
		BEGIN
			--GET REMAINING MINUTES
			SET @remaining_minutes = @time_in_state - @split_minutes

			UPDATE machine_downtime_log
			SET time_in_state = @split_minutes
			WHERE ID = @downtime_ID

			--INSERT A NEW RECORD FOR THE SPLIT-OFF PART
			INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID, time_in_state, reason_ID) 
			SELECT TOP 1 DATEADD(SECOND, @split_minutes / 60.0, change_time), up, machine_ID, tool_ID, @remaining_minutes, reason_ID
			FROM machine_downtime_log 
			WHERE ID = @downtime_ID
			
		END
	ELSE
		BEGIN
			SET @remaining_minutes = @time_in_state - @split_minutes

			UPDATE machine_downtime_log
			SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0
			WHERE ID = @downtime_ID

			-- 5/18/2017; RK; Since the split minutes was 0 then we shouldn't be trying to insert a new record for something that doesn't have a split time
			----INSERT A NEW RECORD FOR THE SPLIT-OFF PART
			--INSERT INTO machine_downtime_log (change_time, up, machine_ID, tool_ID, time_in_state, reason_ID) 
			--SELECT TOP 1 GETDATE(), up, machine_ID, tool_ID, NULL, reason_ID
			--FROM machine_downtime_log 
			--WHERE ID = @downtime_ID
		END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
