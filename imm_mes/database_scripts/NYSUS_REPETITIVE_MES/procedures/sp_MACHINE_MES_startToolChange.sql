SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 7/30/2018
-- Description:	This will start a tool change (tracking)
-- =============================================
CREATE PROCEDURE [dbo].[sp_MACHINE_MES_startToolChange] 
	-- Add the parameters for the stored procedure here
	@machine_ID int = 0, 
	@tool_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @downtime_ID TABLE (ID int)
	DECLARE @tool_change_ID TABLE (ID int)

	-- Get current open downtime record, if there is one
	INSERT INTO @downtime_ID
		SELECT mdl.ID 
		FROM machine_downtime_log mdl
		WHERE mdl.machine_ID = @machine_ID
		--AND mdl.tool_ID = @tool_ID
		AND mdl.time_in_state IS NULL
		AND mdl.reason_ID IS NULL
		AND mdl.is_tool_change_pause IS NULL

	-- If there are any current open downtime records, close them
	UPDATE machine_downtime_log
	SET time_in_state = DATEDIFF(SECOND, change_time, GETDATE()) / 60.0
	WHERE ID IN (SELECT ID FROM @downtime_ID)

	-- Create an open tool change record
	INSERT INTO machine_downtime_log (
		 change_time
		,up
		,machine_ID
		,tool_id
		,reason_ID
		,is_tool_change_pause
	) 
	OUTPUT INSERTED.ID INTO @tool_change_ID
	VALUES (
		 GETDATE()
		,0
		,@machine_ID
		,@tool_ID
		,4
		,0
	)

	-- Update the tool_change_ID with the created ID so we can track all instances related to this tool change
	UPDATE machine_downtime_log
	SET tool_change_ID = tcID.ID
	OUTPUT INSERTED.ID
	FROM machine_downtime_log mdl
	JOIN @tool_change_ID tcID ON tcID.ID = mdl.ID AND mdl.machine_ID = @machine_ID AND mdl.tool_ID = @tool_ID
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
