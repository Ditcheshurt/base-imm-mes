SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



CREATE VIEW [dbo].[v_tool_part_groups] AS
SELECT t.ID as tool_ID, h.ID as part_group_ID, h.part_group_description
FROM tools t
	JOIN tool_parts tp ON tp.tool_ID = t.ID
	JOIN parts p ON tp.part_ID = p.ID
	JOIN part_group_hrt h ON p.part_group_id = h.ID AND h.is_active = 1


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
