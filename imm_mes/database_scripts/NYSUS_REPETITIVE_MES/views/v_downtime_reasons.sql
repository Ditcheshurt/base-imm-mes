SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_downtime_reasons
AS
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](1)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](2)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](3)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](4)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](5)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](6)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](7)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](8)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](9)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](10)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](11)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](12)
UNION
SELECT        *
FROM            [dbo].[fn_GetDowntimeCodes](13)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
