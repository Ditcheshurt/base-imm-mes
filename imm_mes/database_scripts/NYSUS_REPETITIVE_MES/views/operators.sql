SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE VIEW dbo.operators
AS
SELECT        TOP (100) PERCENT o.ID, o.name, CAST(o.ID AS varchar(10)) AS badgeID, o.domain_user, ISNULL(sp.permission_level, - 1) AS sec_level, CASE WHEN sp.permission_level IS NOT NULL AND 
                         o.deactive_date IS NULL THEN 1 ELSE 0 END AS active
FROM            MES_COMMON.dbo.operators AS o LEFT OUTER JOIN
                         MES_COMMON.dbo.system_permissions AS sp ON o.ID = sp.operator_ID AND sp.system_ID = 1
ORDER BY sec_level DESC

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
