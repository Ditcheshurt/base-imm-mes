SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE VIEW dbo.v_defect_reasons
AS
WITH hier AS (SELECT        ID, series, parent_ID, defect_description, active, CONVERT(varchar(50), ID) AS S, CONVERT(varchar(500), '') AS grp, 0 AS L, machine_type_ID
                               FROM            dbo.machine_defect_reasons
                               WHERE        (parent_ID = 0)
                               UNION ALL
                               SELECT        mdr.ID, mdr.series, mdr.parent_ID, mdr.defect_description, mdr.active, CONVERT(varchar(50), hier_2.S + '/' + CONVERT(varchar(5), mdr.ID)) AS S, 
                                                        CONVERT(varchar(500), CASE WHEN mdr.parent_ID > 0 THEN CASE WHEN LEN(hier_2.grp) 
                                                        > 0 THEN hier_2.grp + ' - ' + hier_2.defect_description ELSE hier_2.defect_description END ELSE hier_2.defect_description END) AS grp, 
                                                        hier_2.L + 1 AS L, mdr.machine_type_ID
                               FROM            dbo.machine_defect_reasons AS mdr INNER JOIN
                                                        hier AS hier_2 ON hier_2.ID = mdr.parent_ID)
    SELECT        ID, series, parent_ID, defect_description, active, S, grp, L, machine_type_ID
     FROM            hier AS hier_1


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
