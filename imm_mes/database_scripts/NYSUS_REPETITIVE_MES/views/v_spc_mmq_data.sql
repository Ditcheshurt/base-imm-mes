SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE VIEW [dbo].[v_spc_mmq_data] AS
SELECT MAX(g.ID) as ID, mcp.cycle_ID, g.machine_cycle_part_ID, 
	MAX(CASE WHEN name = 'Round Bottom' THEN value ELSE 0 END) as 'Round Bottom', 
	MAX(CASE WHEN name = 'Round Top' THEN value ELSE 0 END) as 'Round Top',
	MAX(g.comment) as comment
FROM gauge_mahrmmq g
	JOIN machine_cycle_parts mcp ON g.machine_cycle_part_ID = mcp.ID
GROUP BY mcp.cycle_ID, g.machine_cycle_part_ID



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
