CREATE TABLE [dbo].[machine_downtime_log] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [change_time] [datetime] NOT NULL   ,
   [up] [bit] NOT NULL   ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NULL   ,
   [time_in_state] [float] NULL   ,
   [startup_time] [float] NULL   ,
   [reason_ID] [int] NULL   ,
   [comment] [varchar](255) NULL ,
   [tool_change_ID] [int] NULL   ,
   [is_tool_change_pause] [bit] NULL   ,
   [recorded_by] [int] NULL   ,
   [sub_machine_ID] [int] NULL   

   ,CONSTRAINT [PK_machine_downtime_log] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [_dta_index_machine_downtime_log_13_1668200993__K5_K2_K3_K12_K8_K4_1_6] ON [dbo].[machine_downtime_log] ([tool_ID], [change_time], [up], [recorded_by], [reason_ID], [machine_ID]) INCLUDE ([ID], [time_in_state])
CREATE  NONCLUSTERED INDEX [_dta_index_machine_downtime_log_6_1890105774__K4_K3_K8_2_6] ON [dbo].[machine_downtime_log] ([machine_ID], [up], [reason_ID]) INCLUDE ([change_time], [time_in_state])

GO
