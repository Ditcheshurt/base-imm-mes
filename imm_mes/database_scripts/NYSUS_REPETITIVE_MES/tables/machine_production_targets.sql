CREATE TABLE [dbo].[machine_production_targets] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [the_hour] [int] NOT NULL   ,
   [target_percent] [float] NOT NULL   

   ,CONSTRAINT [PK_machine_production_targets] PRIMARY KEY CLUSTERED ([ID])
)


GO
