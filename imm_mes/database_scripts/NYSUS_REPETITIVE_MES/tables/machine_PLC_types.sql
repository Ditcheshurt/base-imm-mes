CREATE TABLE [dbo].[machine_PLC_types] (
   [ID] [int] NOT NULL   ,
   [type_description] [varchar](50) NULL ,
   [driver_type] [varchar](50) NULL ,
   [cpu_type] [varchar](50) NULL 

   ,CONSTRAINT [PK_machine_PLC_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
