CREATE TABLE [dbo].[lot_tool_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [lot_material_type_ID] [int] NULL   ,
   [tool_part_ID] [int] NULL   ,
   [qty_per_part] [int] NULL   ,
   [last_update] [datetime] NOT NULL 
      CONSTRAINT [DF_lot_tool_parts_last_update] DEFAULT (getdate())  

   ,CONSTRAINT [PK_lot_tool_parts] PRIMARY KEY CLUSTERED ([ID])
)


GO
