CREATE TABLE [dbo].[CommandLog] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [DatabaseName] [nvarchar](128) NULL ,
   [SchemaName] [nvarchar](128) NULL ,
   [ObjectName] [nvarchar](128) NULL ,
   [ObjectType] [char](2) NULL ,
   [IndexName] [nvarchar](128) NULL ,
   [IndexType] [tinyint] NULL   ,
   [StatisticsName] [nvarchar](128) NULL ,
   [PartitionNumber] [int] NULL   ,
   [ExtendedInfo] [xml] NULL   ,
   [Command] [nvarchar](max) NOT NULL ,
   [CommandType] [nvarchar](60) NOT NULL ,
   [StartTime] [datetime] NOT NULL   ,
   [EndTime] [datetime] NULL   ,
   [ErrorNumber] [int] NULL   ,
   [ErrorMessage] [nvarchar](max) NULL 

   ,CONSTRAINT [PK_CommandLog] PRIMARY KEY CLUSTERED ([ID])
)


GO
