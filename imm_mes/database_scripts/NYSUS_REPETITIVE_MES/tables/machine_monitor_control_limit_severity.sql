CREATE TABLE [dbo].[machine_monitor_control_limit_severity] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_tool_ctrl_limit_ID] [int] NOT NULL   ,
   [machine_monitor_severity_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL   ,
   [last_modified_by] [int] NOT NULL 
      CONSTRAINT [DF_machine_monitor_control_limit_severity_modified_by] DEFAULT ((0))  ,
   [last_modified_timestamp] [datetime] NULL   
)


GO
