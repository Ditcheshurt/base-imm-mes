CREATE TABLE [dbo].[print_queue] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [mod_ID] [int] NULL   ,
   [printer_ID] [int] NOT NULL   ,
   [print_text] [varchar](8000) NOT NULL ,
   [queue_time] [datetime] NOT NULL   ,
   [printed_time] [datetime] NULL   ,
   [reprint_reason] [int] NULL   ,
   [qty] [int] NULL   ,
   [last_error_time] [datetime] NULL   ,
   [error_count] [int] NULL   ,
   [override_file_name] [varchar](50) NULL 

   ,CONSTRAINT [PK_print_queue] PRIMARY KEY CLUSTERED ([ID])
)


GO
