CREATE TABLE [dbo].[machine_printers] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [part_order] [int] NOT NULL   ,
   [label_type] [varchar](50) NOT NULL ,
   [printer_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_printers] PRIMARY KEY CLUSTERED ([ID])
)


GO
