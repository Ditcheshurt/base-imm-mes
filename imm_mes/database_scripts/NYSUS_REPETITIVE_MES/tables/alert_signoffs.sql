CREATE TABLE [dbo].[alert_signoffs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [alert_type] [varchar](50) NOT NULL ,
   [file_name] [varchar](100) NOT NULL ,
   [operator_ID] [int] NOT NULL   ,
   [signoff_time] [datetime] NOT NULL   

   ,CONSTRAINT [PK_alert_signoffs] PRIMARY KEY CLUSTERED ([ID])
)


GO
