CREATE TABLE [dbo].[alert_operators] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NOT NULL   ,
   [alert_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_operator_downtime_alerts] PRIMARY KEY CLUSTERED ([ID])
)


GO
