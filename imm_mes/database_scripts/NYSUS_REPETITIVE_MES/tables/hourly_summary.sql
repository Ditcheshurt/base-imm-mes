CREATE TABLE [dbo].[hourly_summary] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [system_ID] [int] NOT NULL   ,
   [machine_ID] [int] NULL   ,
   [tool_ID] [int] NULL   ,
   [line_ID] [int] NULL   ,
   [the_date] [datetime] NOT NULL   ,
   [the_hour] [int] NOT NULL   ,
   [part_qty] [int] NULL   ,
   [part_target_qty] [float] NULL   ,
   [part_goal] [float] NULL   ,
   [total_target_parts] [float] NULL   ,
   [performance] AS (case when isnull([part_target_qty],(0))>(0) then CONVERT([float],[part_qty])/CONVERT([float],[part_target_qty]) else (0) end),
   [scrap_numerator] [float] NULL   ,
   [scrap_denominator] [float] NULL   ,
   [quality] AS (case when isnull([scrap_denominator],(0))>(0) then isnull([scrap_numerator],(0))/[scrap_denominator] else (0) end),
   [downtime_minutes] [float] NULL   ,
   [scheduled_minutes] [float] NULL   ,
   [total_minutes] [float] NULL   ,
   [availability] AS (case when isnull([total_minutes],(0))>(0) then (([total_minutes]-[downtime_minutes])-[scheduled_minutes])/([total_minutes]-[scheduled_minutes]) else (0) end),
   [oee] AS ((case when isnull([part_target_qty],(0))>(0) then CONVERT([float],[part_qty])/CONVERT([float],[part_target_qty]) else (0) end*case when isnull([scrap_denominator],(0))>(0) then [scrap_numerator]/[scrap_denominator] else (0) end)*case when isnull([total_minutes],(0))>(0) then (([total_minutes]-[downtime_minutes])-[scheduled_minutes])/([total_minutes]-[scheduled_minutes]) else (0) end),
   [last_updated] [datetime] NULL   ,
   [num_calcs] [int] NULL   

   ,CONSTRAINT [PK_hourly_production_summary] PRIMARY KEY CLUSTERED ([ID])
)


GO
