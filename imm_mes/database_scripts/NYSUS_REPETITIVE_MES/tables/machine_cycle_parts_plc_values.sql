CREATE TABLE [dbo].[machine_cycle_parts_plc_values] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [serial] [bigint] NOT NULL   ,
   [machine_ID] [int] NOT NULL   ,
   [tag_name] [varchar](50) NOT NULL ,
   [tag_value] [float] NOT NULL   

   ,CONSTRAINT [PK_machine_cycle_parts_plc_values] PRIMARY KEY CLUSTERED ([ID])
)


GO
