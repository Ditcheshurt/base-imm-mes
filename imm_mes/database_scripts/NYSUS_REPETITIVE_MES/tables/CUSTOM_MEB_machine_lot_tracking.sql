CREATE TABLE [dbo].[CUSTOM_machine_lot_tracking] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NULL   ,
   [machine_ID] [int] NOT NULL   ,
   [material_cert_test_ID] [int] NOT NULL   ,
   [start_timestamp] [datetime] NOT NULL   ,
   [end_timestamp] [datetime] NULL   ,
   [active] [bit] NULL   

   ,CONSTRAINT [PK_CUSTOM_machine_lot_tracking] PRIMARY KEY CLUSTERED ([ID])
)


GO
