CREATE TABLE [dbo].[CUSTOM_material_certification_results] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [matcert_test_ID] [int] NOT NULL   ,
   [matcert_datapoint_ID] [int] NOT NULL   ,
   [matcert_datapoint_value] [varchar](50) NOT NULL ,
   [matcert_datapoint_range] [varchar](50) NULL 

   ,CONSTRAINT [PK_CUSTOM_material_certification_results] PRIMARY KEY CLUSTERED ([ID])
)


GO
