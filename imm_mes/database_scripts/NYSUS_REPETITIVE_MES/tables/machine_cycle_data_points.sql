CREATE TABLE [dbo].[machine_cycle_data_points] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [cycle_ID] [int] NOT NULL   ,
   [data_point_ID] [int] NOT NULL   ,
   [data_point_value] [float] NULL   

   ,CONSTRAINT [PK_machine_cycle_data_points] PRIMARY KEY CLUSTERED ([ID])
)


GO
