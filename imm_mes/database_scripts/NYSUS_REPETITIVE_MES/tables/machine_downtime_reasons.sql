CREATE TABLE [dbo].[machine_downtime_reasons] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [parent_ID] [int] NOT NULL   ,
   [reason_description] [varchar](150) NOT NULL ,
   [count_against] [bit] NULL   ,
   [tool_change_ignore] [bit] NULL   ,
   [active] [bit] NOT NULL   ,
   [force_comment] [bit] NULL   ,
   [machine_type_ID] [int] NULL   ,
   [oee_category] [int] NULL 
      CONSTRAINT [DF_machine_downtime_reasons_oee_category] DEFAULT ((1))  ,
   [maximum_duration] [int] NULL 
      CONSTRAINT [DF_machine_downtime_reasons_maximum_duration] DEFAULT ((9999))  ,
   [is_micro_stop] [bit] NULL   

   ,CONSTRAINT [PK_machine_downtime_reasons] PRIMARY KEY CLUSTERED ([ID])
)


GO
