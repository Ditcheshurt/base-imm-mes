CREATE TABLE [dbo].[machine_ojt_jobs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [ojt_job_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_ojt_jobs] PRIMARY KEY CLUSTERED ([ID])
)


GO
