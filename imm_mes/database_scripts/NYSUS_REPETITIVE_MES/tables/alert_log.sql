CREATE TABLE [dbo].[alert_log] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [alert_ID] [int] NOT NULL   ,
   [instance_ID] [int] NULL   ,
   [alert_time] [datetime] NOT NULL   ,
   [queued] [bit] NOT NULL 
      CONSTRAINT [DF_alert_log_queued] DEFAULT ((0))  ,
   [alert_value] [float] NULL   ,
   [machine_ID] [int] NULL   

   ,CONSTRAINT [PK_downtime_alert_log] PRIMARY KEY CLUSTERED ([ID])
)


GO
