CREATE TABLE [dbo].[tool_defect_reasons] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tool_ID] [int] NOT NULL   ,
   [defect_reason_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_tool_defect_reasons] PRIMARY KEY CLUSTERED ([ID])
)


GO
