CREATE TABLE [dbo].[gauge_machine_cycle_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_cycle_part_ID] [int] NOT NULL   ,
   [gauge_material_types_instruction_ID] [int] NOT NULL   ,
   [sample_group] [int] NOT NULL 
      CONSTRAINT [DF_gauge_machine_cycle_parts_sample_group] DEFAULT ((1))  ,
   [data_point_value] [float] NOT NULL   ,
   [recorded_date] [datetime] NULL 
      CONSTRAINT [DF_gauge_machine_cycle_parts_recorded_date] DEFAULT (getdate())  ,
   [comment] [varchar](500) NULL 

   ,CONSTRAINT [PK_machine_cycle_part_gauge_reading] PRIMARY KEY CLUSTERED ([ID])
)


GO
