CREATE TABLE [dbo].[container_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [container_ID] [int] NOT NULL   ,
   [machine_cycle_part_ID] [int] NOT NULL   ,
   [loaded_time] [datetime] NOT NULL   ,
   [container_position] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_container_parts] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [_dta_index_container_parts_5_597577167__K3_K2_1_4_5_6] ON [dbo].[container_parts] ([machine_cycle_part_ID], [container_ID]) INCLUDE ([ID], [loaded_time], [container_position], [operator_ID])

GO
