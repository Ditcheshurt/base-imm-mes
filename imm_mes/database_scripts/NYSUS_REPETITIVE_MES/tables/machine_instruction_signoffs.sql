CREATE TABLE [dbo].[machine_instruction_signoffs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [instr_ID] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [signoff_time] [datetime] NOT NULL   

   ,CONSTRAINT [PK_machine_instruction_signoffs] PRIMARY KEY CLUSTERED ([ID])
)


GO
