CREATE TABLE [dbo].[lot_material_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [part_number] [varchar](50) NOT NULL ,
   [alt_part_number] [varchar](50) NULL ,
   [description] [varchar](50) NULL ,
   [current_lot] [varchar](50) NULL ,
   [next_lot] [varchar](50) NULL ,
   [lot_parts_used] [int] NULL   ,
   [parts_per_lot] [int] NULL   ,
   [parts_per_next_lot] [int] NULL   ,
   [parts_per_lot_maximum_offset] [int] NULL   ,
   [parts_per_next_lot_maximum_offset] [int] NULL   ,
   [parts_per_lot_warning_offset] [int] NULL   ,
   [parts_per_next_lot_warning_offset] [int] NULL   ,
   [station_ip_address] [varchar](15) NULL ,
   [parts_per_lot_default] [int] NULL   ,
   [last_update] [datetime] NULL 
      CONSTRAINT [DF_lot_material_types_last_update] DEFAULT (getdate())  ,
   [plc_tag_ID] [int] NULL   

   ,CONSTRAINT [PK_machine_group_material_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
