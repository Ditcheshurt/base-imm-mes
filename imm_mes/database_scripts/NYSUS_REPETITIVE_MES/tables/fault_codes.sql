CREATE TABLE [dbo].[fault_codes] (
   [id] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tag_ID] [int] NULL   ,
   [plc_bit] [int] NULL   ,
   [fault_word_value] [int] NULL   ,
   [description] [varchar](255) NULL ,
   [severity] [varchar](50) NULL ,
   [on_since] [datetime] NULL   

   ,CONSTRAINT [PK_fault_codes] PRIMARY KEY CLUSTERED ([id])
)


GO
