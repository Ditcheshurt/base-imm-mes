CREATE TABLE [dbo].[machine_monitor_email_list] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_tool_ctrl_limit_ID] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_machine_monitor_email_list_active] DEFAULT ((0))  ,
   [last_updated] [datetime] NULL   
)


GO
