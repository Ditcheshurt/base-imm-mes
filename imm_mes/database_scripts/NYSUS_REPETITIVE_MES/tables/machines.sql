CREATE TABLE [dbo].[machines] (
   [ID] [int] NOT NULL   ,
   [machine_type_ID] [int] NOT NULL   ,
   [machine_name] [varchar](50) NOT NULL ,
   [machine_number] [varchar](50) NULL ,
   [machine_group_ID] [int] NULL   ,
   [op_order] [int] NULL   ,
   [up] [bit] NOT NULL   ,
   [current_tool_ID] [int] NULL   ,
   [current_PLC_tool_ID] [int] NULL   ,
   [alt_tool_ID_set_time] [datetime] NULL   ,
   [current_std_pack] [int] NULL   ,
   [ignored] [bit] NOT NULL   ,
   [ignore_reason] [varchar](50) NULL ,
   [ignore_time] [datetime] NULL   ,
   [ignore_operator_ID] [datetime] NULL   ,
   [queue_type] [int] NULL   ,
   [per_part_printer_ID] [int] NULL   ,
   [rack_printer_ID] [int] NULL   ,
   [PLC_ID] [int] NULL   ,
   [PLC_file_address] [varchar](100) NULL ,
   [station_IP_address] [varchar](50) NULL ,
   [last_cycle_time] [datetime] NULL   ,
   [img_src] [varchar](50) NULL ,
   [master_machine_ID] [int] NULL   ,
   [current_operator_ID] [int] NULL   ,
   [system_ID] [int] NULL   ,
   [over_cycle_grace_period] [float] NULL   ,
   [show_reporting] [bit] NOT NULL 
      CONSTRAINT [DF_machines_report] DEFAULT ((1))  ,
   [is_station_default] [bit] NULL 
      CONSTRAINT [DF_machines_is_station_default] DEFAULT ((0))  ,
   [allow_duplicates] [bit] NULL   ,
   [packout_machine_group_ID] [int] NULL   ,
   [spc_use_auto_gauge_data] [bit] NULL   ,
   [spc_use_mmq_data] [bit] NULL   ,
   [machine_dashboard_group_ID] [int] NULL   ,
   [ojt_group_ID] [int] NULL   

   ,CONSTRAINT [PK_machines] PRIMARY KEY CLUSTERED ([ID])
)


GO
