CREATE TABLE [dbo].[operator_shift_summaries] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NOT NULL   ,
   [shift_summary_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_operator_shift_summaries] PRIMARY KEY CLUSTERED ([ID])
)


GO
