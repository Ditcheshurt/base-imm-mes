CREATE TABLE [dbo].[alert_machines] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [alert_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_alerts] PRIMARY KEY CLUSTERED ([ID])
)


GO
