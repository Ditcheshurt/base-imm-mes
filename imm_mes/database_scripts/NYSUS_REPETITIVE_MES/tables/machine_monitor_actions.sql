CREATE TABLE [dbo].[machine_monitor_actions] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [action_handler_ID] [varchar](10) NOT NULL ,
   [action_description] [varchar](50) NOT NULL ,
   [action_parameter_array] [varchar](max) NOT NULL 
)


GO
