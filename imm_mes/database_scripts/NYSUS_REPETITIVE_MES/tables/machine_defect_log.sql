CREATE TABLE [dbo].[machine_defect_log] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [part_ID] [int] NOT NULL   ,
   [defect_time] [datetime] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [defect_ID] [int] NOT NULL   ,
   [defect_origin_ID] [int] NULL   ,
   [disposition] [varchar](20) NULL ,
   [serial_number] [varchar](50) NULL ,
   [mrp_transaction_time] [datetime] NULL   

   ,CONSTRAINT [PK_machine_defects] PRIMARY KEY CLUSTERED ([ID])
)


GO
