CREATE TABLE [dbo].[machine_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_type_description] [varchar](100) NOT NULL 

   ,CONSTRAINT [PK_machine_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
