CREATE TABLE [dbo].[machine_in_process_parts] (
   [serial] [bigint] NOT NULL   ,
   [created_time] [datetime] NOT NULL   ,
   [last_machine_started_ID] [int] NOT NULL   ,
   [last_machine_started_order] [int] NOT NULL   ,
   [last_machine_started_time] [datetime] NOT NULL   ,
   [last_machine_completed_ID] [int] NULL   ,
   [last_machine_completed_order] [int] NULL   ,
   [last_machine_completed_time] [datetime] NULL   

   ,CONSTRAINT [PK_machine_in_process_parts] PRIMARY KEY CLUSTERED ([serial])
)


GO
