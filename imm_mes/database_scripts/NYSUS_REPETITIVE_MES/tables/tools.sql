CREATE TABLE [dbo].[tools] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tool_description] [varchar](100) NOT NULL ,
   [short_description] [varchar](25) NULL ,
   [active] [bit] NOT NULL   ,
   [shot_count] [int] NOT NULL 
      CONSTRAINT [DF_tools_shot_count] DEFAULT ((0))  ,
   [parent_tool_ID] [int] NULL   ,
   [current_part_ID] [int] NULL   ,
   [ojt_group_ID] [int] NULL   ,
   [auto_label] [bit] NULL   

   ,CONSTRAINT [PK_tools] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20180813-121711] ON [dbo].[tools] ([ID]) INCLUDE ([tool_description])

GO
