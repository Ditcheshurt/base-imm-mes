CREATE TABLE [dbo].[gauge_material_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tool_part_ID] [int] NOT NULL   ,
   [description] [varchar](50) NULL ,
   [gauge_parts_used] [int] NULL 
      CONSTRAINT [DF_gauge_data_point_part_setup_number_cycles] DEFAULT ((0))  ,
   [parts_per_gauging] [int] NOT NULL   ,
   [parts_per_gauging_warning_offset] [int] NOT NULL   ,
   [parts_per_gauging_maximum_offset] [int] NOT NULL   ,
   [station_ip_address] [varchar](15) NULL ,
   [skip_part_scan] [bit] NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_gauge_data_point_part_setup_active] DEFAULT ((1))  ,
   [plc_tag_ID] [int] NULL   ,
   [warn_at_shift_change] [bit] NULL   

   ,CONSTRAINT [PK_gauge_data_point_part_setup] PRIMARY KEY CLUSTERED ([ID])
)


GO
