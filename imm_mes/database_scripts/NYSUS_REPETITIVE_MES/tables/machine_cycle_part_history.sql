CREATE TABLE [dbo].[machine_cycle_part_history] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [serial] [bigint] NOT NULL   ,
   [machine_ID] [int] NOT NULL   ,
   [check_time] [datetime] NOT NULL   ,
   [check_result] [varchar](20) NOT NULL ,
   [cycle_start_time] [datetime] NULL   ,
   [cycle_complete_time] [datetime] NULL   ,
   [cycle_complete_result] [int] NULL   

   ,CONSTRAINT [PK_machine_cycle_part_history] PRIMARY KEY CLUSTERED ([ID])
)


GO
