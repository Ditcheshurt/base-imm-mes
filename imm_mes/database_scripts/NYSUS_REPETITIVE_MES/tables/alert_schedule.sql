CREATE TABLE [dbo].[alert_schedule] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [alert_ID] [int] NOT NULL   ,
   [the_day] [int] NOT NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_downtime_alert_schedule_active] DEFAULT ((0))  

   ,CONSTRAINT [PK_downtime_alert_schedule] PRIMARY KEY CLUSTERED ([ID])
)


GO
