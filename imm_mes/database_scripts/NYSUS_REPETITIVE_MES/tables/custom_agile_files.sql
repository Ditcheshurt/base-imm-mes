CREATE TABLE [dbo].[custom_agile_files] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [SPN_NUMBER] [varchar](100) NULL ,
   [ITEM_NUMBER] [varchar](50) NOT NULL ,
   [AGILE_ID] [int] NOT NULL   ,
   [FILE_TYPE] [varchar](100) NULL ,
   [FILE_PATH] [varchar](500) NULL ,
   [FILENAME] [varchar](500) NULL ,
   [DESCRIPTION] [varchar](2000) NULL ,
   [TEXT31] [varchar](500) NULL ,
   [last_update_time] [datetime] NULL   ,
   [flag_for_convert] [datetime] NULL   

   ,CONSTRAINT [PK_custom_agile_files] PRIMARY KEY CLUSTERED ([ID])
)


GO
