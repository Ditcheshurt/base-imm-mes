CREATE TABLE [dbo].[CUSTOM_tool_mat_tracking] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [mat_number] [varchar](50) NOT NULL ,
   [start_timestamp] [datetime] NOT NULL   ,
   [end_timestamp] [datetime] NULL   ,
   [active] [bit] NULL   

   ,CONSTRAINT [PK_CUSTOM_tool_lot_tracker] PRIMARY KEY CLUSTERED ([ID])
)


GO
