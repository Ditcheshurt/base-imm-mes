CREATE TABLE [dbo].[fault_history] (
   [id] [int] NOT NULL  
      IDENTITY (1,1) ,
   [fault_id] [int] NULL   ,
   [date_time] [datetime] NULL   ,
   [duration] [float] NULL   

   ,CONSTRAINT [PK_fault_history] PRIMARY KEY CLUSTERED ([id])
)


GO
