CREATE TABLE [dbo].[lot_machines] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [lot_material_type_ID] [int] NULL   ,
   [machine_ID] [int] NULL   ,
   [last_update] [datetime] NULL 
      CONSTRAINT [DF_lot_machines_last_update] DEFAULT (getdate())  

   ,CONSTRAINT [PK_lot_lot_machines] PRIMARY KEY CLUSTERED ([ID])
)


GO
