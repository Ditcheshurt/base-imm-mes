CREATE TABLE [dbo].[tool_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tool_ID] [int] NOT NULL   ,
   [part_ID] [int] NOT NULL   ,
   [parts_per_tool] [int] NULL   ,
   [operation] [int] NULL   ,
   [scrap_cost] [float] NULL   ,
   [sub_tool_order] [int] NULL   ,
   [cur_rack_count] [int] NULL   ,
   [serialized] [bit] NOT NULL   ,
   [zoned] [bit] NOT NULL   ,
   [active] [bit] NOT NULL   ,
   [active_start] [datetime] NULL   ,
   [active_end] [datetime] NULL   ,
   [last_update] [datetime] NULL 
      CONSTRAINT [DF_tool_parts_last_update] DEFAULT (getdate())  ,
   [alt_sub_tool_ID] [int] NULL   ,
   [part_order] [int] NULL   ,
   [process_ID] [int] NULL   

   ,CONSTRAINT [PK_sub_tools] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20180813-121224] ON [dbo].[tool_parts] ([tool_ID])

GO
