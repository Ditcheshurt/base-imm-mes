CREATE TABLE [dbo].[machine_groups] (
   [ID] [int] NOT NULL   ,
   [name] [varchar](255) NULL ,
   [description] [varchar](2000) NULL ,
   [next_op_group] [int] NULL   ,
   [rack_layers] [int] NULL   ,
   [rack_boxes_across] [int] NULL   ,
   [rack_boxes_deep] [int] NULL   ,
   [rack_parts_per_box] [int] NULL   

   ,CONSTRAINT [PK_machine_groups] PRIMARY KEY CLUSTERED ([ID])
)


GO
