CREATE TABLE [dbo].[parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [part_desc] [varchar](50) NOT NULL ,
   [part_number] [varchar](9) NOT NULL ,
   [mrp_part_number] [varchar](50) NULL ,
   [part_image] [varchar](500) NULL ,
   [part_video] [varchar](500) NULL ,
   [standard_pack_qty] [int] NOT NULL 
      CONSTRAINT [DF_parts_standard_pack_qty] DEFAULT ((0))  ,
   [unit_cost] [float] NULL   ,
   [part_weight] [float] NULL   ,
   [box_weight] [float] NULL   ,
   [mrp_company_code] [varchar](3) NULL ,
   [fg_part_ID] [int] NULL   ,
   [active] [bit] NULL   ,
   [active_start] [datetime] NULL   ,
   [active_end] [datetime] NULL   ,
   [last_update] [datetime] NULL   ,
   [part_group_id] [int] NULL   ,
   [part_type_id] [int] NULL   ,
   [print_template_ID] [int] NULL   ,
   [external_base] [varchar](150) NULL 

   ,CONSTRAINT [PK_parts] PRIMARY KEY CLUSTERED ([ID])
)


GO
