CREATE TABLE [dbo].[machine_downtime_reason_oee_categories] (
   [ID] [int] NOT NULL   ,
   [oee_category_desc] [varchar](50) NOT NULL ,
   [count_against_oee] [bit] NOT NULL   ,
   [is_scheduled] [bit] NULL   ,
   [require_team_leader] [bit] NULL   

   ,CONSTRAINT [PK_machine_downtime_reason_oee_categories] PRIMARY KEY CLUSTERED ([ID])
)


GO
