CREATE TABLE [dbo].[lot_machine_cycle_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_cycle_part_ID] [int] NOT NULL   ,
   [lot_material_type_ID] [int] NOT NULL   ,
   [lot_number] [varchar](50) NULL ,
   [last_update] [datetime] NULL 
      CONSTRAINT [DF_lot_machine_cycle_parts_last_update] DEFAULT (getdate())  

   ,CONSTRAINT [PK_machine_cycle_part_lot_numbers] PRIMARY KEY CLUSTERED ([ID])
)


GO
