CREATE TABLE [dbo].[alert_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [name] [varchar](255) NOT NULL ,
   [description] [varchar](255) NOT NULL 

   ,CONSTRAINT [PK_alert_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
