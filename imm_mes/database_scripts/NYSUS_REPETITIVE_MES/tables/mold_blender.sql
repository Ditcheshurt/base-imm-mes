CREATE TABLE [dbo].[mold_blender] (
   [recid] [int] NOT NULL  
      IDENTITY (1,1) ,
   [blender] [tinyint] NULL   ,
   [press] [tinyint] NULL   ,
   [datetimestamp] [datetime] NULL   ,
   [component1] [real] NULL   ,
   [component2] [real] NULL   ,
   [component3] [real] NULL   ,
   [component4] [real] NULL   ,
   [batchnumber] [int] NULL   

   ,CONSTRAINT [PK_mold_blender] PRIMARY KEY CLUSTERED ([recid])
)

CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20190204-123200] ON [dbo].[mold_blender] ([datetimestamp])
CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20190204-123259] ON [dbo].[mold_blender] ([blender], [datetimestamp])
CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20190204-123433] ON [dbo].[mold_blender] ([datetimestamp] DESC)

GO
