CREATE TABLE [dbo].[alert_shifts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [alert_ID] [int] NOT NULL   ,
   [the_shift] [int] NOT NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_alert_shifts_active] DEFAULT ((0))  

   ,CONSTRAINT [PK_alert_shifts] PRIMARY KEY CLUSTERED ([ID])
)


GO
