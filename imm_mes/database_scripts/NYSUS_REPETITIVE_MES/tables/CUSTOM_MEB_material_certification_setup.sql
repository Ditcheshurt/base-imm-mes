CREATE TABLE [dbo].[CUSTOM_material_certification_setup] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [data_point_name] [varchar](50) NOT NULL ,
   [data_point_desc] [varchar](50) NULL ,
   [units] [varchar](50) NULL ,
   [range_separator] [varchar](20) NULL ,
   [display_order] [int] NULL   ,
   [active] [bit] NOT NULL   

   ,CONSTRAINT [PK_CUSTOM_material_certification_setup] PRIMARY KEY CLUSTERED ([ID])
)


GO
