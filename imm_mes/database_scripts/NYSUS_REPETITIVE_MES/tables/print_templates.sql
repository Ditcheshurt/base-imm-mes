CREATE TABLE [dbo].[print_templates] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [template_name] [varchar](200) NOT NULL ,
   [template_text] [varchar](max) NOT NULL ,
   [created_timestamp] [datetime] NULL   ,
   [created_by] [varchar](50) NULL ,
   [modified_timestamp] [datetime] NULL   ,
   [modified_by] [varchar](50) NULL ,
   [archived_timestamp] [datetime] NULL   ,
   [active] [bit] NULL   ,
   [parent_id] [int] NULL   ,
   [revision] [int] NULL   

   ,CONSTRAINT [PK_print_templates] PRIMARY KEY CLUSTERED ([ID])
)


GO
