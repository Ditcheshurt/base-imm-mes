CREATE TABLE [dbo].[alerts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [description] [varchar](255) NOT NULL ,
   [min_threshold] [float] NULL   ,
   [max_threshold] [float] NULL   ,
   [alert_type_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_alerts_active] DEFAULT ((1))  ,
   [removed_at] [datetime] NULL   ,
   [frequency] [int] NULL   

   ,CONSTRAINT [PK_email_alerts] PRIMARY KEY CLUSTERED ([ID])
)


GO
