CREATE TABLE [dbo].[machine_material_certification_log] (
   [ID] [int] NOT NULL   ,
   [entry_time] [datetime] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [lot_number] [varchar](50) NULL ,
   [ash_percentage] [float] NULL   ,
   [moisture_content] [float] NULL   ,
   [specific_gravity] [float] NULL   ,
   [active_lot] [bit] NULL   ,
   [machine_active_part_ID] [int] NULL   
)


GO
