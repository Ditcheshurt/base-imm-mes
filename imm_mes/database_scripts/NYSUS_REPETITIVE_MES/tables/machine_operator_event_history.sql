CREATE TABLE [dbo].[machine_operator_event_history] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [operator_event_history_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_operator_event_history] PRIMARY KEY CLUSTERED ([ID])
)


GO
