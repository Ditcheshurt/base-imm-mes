CREATE TABLE [dbo].[printer_setup] (
   [id] [int] NOT NULL   ,
   [printer_name] [varchar](50) NOT NULL ,
   [label_format] [varchar](50) NOT NULL ,
   [printer_IP] [varchar](50) NOT NULL ,
   [printer_MAC] [varchar](50) NULL ,
   [label_template] [text] NULL   ,
   [default_qty] [int] NULL   ,
   [ftp_username] [varchar](50) NULL ,
   [ftp_password] [varchar](50) NULL ,
   [file_name] [varchar](50) NULL ,
   [queue_group] [int] NULL   ,
   [target_dir] [varchar](150) NULL 

   ,CONSTRAINT [PK_printer_setup] PRIMARY KEY CLUSTERED ([id])
)


GO
