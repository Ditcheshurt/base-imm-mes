CREATE TABLE [dbo].[data_point_setup] (
   [ID] [int] NOT NULL   ,
   [data_point_name] [varchar](50) NOT NULL ,
   [data_point_desc] [varchar](50) NULL ,
   [extract_method] [varchar](50) NOT NULL ,
   [extract_info] [varchar](50) NULL ,
   [multiplier] [float] NOT NULL 
      CONSTRAINT [DF_data_point_setup_multiplier] DEFAULT ((1))  ,
   [units] [varchar](50) NOT NULL ,
   [is_downtime_indicator] [bit] NOT NULL 
      CONSTRAINT [DF_data_point_setup_is_downtime_indicator] DEFAULT ((0))  ,
   [is_product_ID] [bit] NOT NULL 
      CONSTRAINT [DF_data_point_setup_is_product_ID] DEFAULT ((0))  ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_data_point_setup_active] DEFAULT ((1))  ,
   [default_ucl] [float] NULL   ,
   [default_lcl] [float] NULL   

   ,CONSTRAINT [PK_data_point_setup] PRIMARY KEY CLUSTERED ([ID])
)


GO
