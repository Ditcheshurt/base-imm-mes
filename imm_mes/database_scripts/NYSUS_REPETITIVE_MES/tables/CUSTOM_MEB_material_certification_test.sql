CREATE TABLE [dbo].[CUSTOM_material_certification_test] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [lot_number] [varchar](50) NULL ,
   [timestamp] [datetime] NOT NULL   ,
   [test_result] [bit] NULL   ,
   [unique_test_ID] [varbinary](64) NULL 

   ,CONSTRAINT [PK_CUSTOM_material_certification_test] PRIMARY KEY CLUSTERED ([ID])
)


GO
