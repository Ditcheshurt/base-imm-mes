CREATE TABLE [dbo].[spc_autogauge_data_points] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [data_point_column] [varchar](50) NOT NULL ,
   [tolerance_target] [float] NULL   ,
   [tolerance_delta] [float] NULL   ,
   [xbar_ucl] [float] NULL   ,
   [xbar_lcl] [float] NULL   ,
   [range_ucl] [float] NULL   ,
   [range_lcl] [float] NULL   

   ,CONSTRAINT [PK_spc_autogauge_data_points] PRIMARY KEY CLUSTERED ([ID])
)


GO
