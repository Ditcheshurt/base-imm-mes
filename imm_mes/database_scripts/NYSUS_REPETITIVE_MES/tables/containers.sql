CREATE TABLE [dbo].[containers] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [part_ID] [int] NOT NULL   ,
   [start_time] [datetime] NOT NULL   ,
   [end_time] [datetime] NULL   ,
   [external_serial] [varchar](50) NULL 

   ,CONSTRAINT [PK_containers] PRIMARY KEY CLUSTERED ([ID])
)


GO
