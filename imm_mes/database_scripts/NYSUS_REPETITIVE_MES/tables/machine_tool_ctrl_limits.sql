CREATE TABLE [dbo].[machine_tool_ctrl_limits] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [data_point_ID] [int] NOT NULL   ,
   [target_value] [float] NULL   ,
   [upper_ctrl_limit] [float] NULL   ,
   [lower_ctrl_limit] [float] NULL   ,
   [deactivated] [bit] NOT NULL 
      CONSTRAINT [DF_machine_tool_ctrl_limits_deactivated] DEFAULT ((0))  ,
   [deactivated_by] [int] NULL   ,
   [deactivated_timestamp] [datetime] NULL   
)


GO
