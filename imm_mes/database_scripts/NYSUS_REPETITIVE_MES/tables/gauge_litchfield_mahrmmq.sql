CREATE TABLE [dbo].[gauge_litchfield_mahrmmq] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_cycle_part_ID] [int] NOT NULL   ,
   [name] [varchar](50) NULL ,
   [datum] [varchar](50) NULL ,
   [nominal] [float] NULL   ,
   [l_tol] [float] NULL   ,
   [u_tol] [float] NULL   ,
   [value] [float] NULL   ,
   [unit] [nvarchar](50) NULL ,
   [feature] [int] NULL   ,
   [low_nat_border] [int] NULL   ,
   [type] [int] NULL   ,
   [no] [int] NULL   ,
   [date] [varchar](50) NULL ,
   [time] [varchar](50) NULL ,
   [operator] [varchar](50) NULL 

   ,CONSTRAINT [PK_gauge_litchfield_mahrmmq] PRIMARY KEY CLUSTERED ([ID])
)


GO
