CREATE TABLE [dbo].[machine_instructions] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [instr_order] [int] NOT NULL   ,
   [title] [varchar](2000) NOT NULL ,
   [instr_text] [varchar](max) NOT NULL ,
   [instr_image] [varchar](200) NULL ,
   [last_updated_time] [datetime] NULL   

   ,CONSTRAINT [PK_machine_instructions] PRIMARY KEY CLUSTERED ([ID])
)


GO
