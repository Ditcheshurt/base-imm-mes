CREATE TABLE [dbo].[machine_defect_reasons] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [parent_ID] [int] NOT NULL   ,
   [machine_type_ID] [int] NULL   ,
   [defect_description] [varchar](100) NOT NULL ,
   [series] [varchar](4) NULL ,
   [active] [bit] NOT NULL   ,
   [plc_defect_code] [int] NULL   ,
   [is_reworkable] [bit] NULL   ,
   [rework_instr_url] [varchar](100) NULL ,
   [approval_instr_url] [varchar](100) NULL 

   ,CONSTRAINT [PK_machine_defect_reasons] PRIMARY KEY CLUSTERED ([ID])
)


GO
