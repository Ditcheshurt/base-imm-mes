CREATE TABLE [dbo].[part_group_hrt] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [part_group_description] [varchar](150) NOT NULL 
      CONSTRAINT [DF_part_group_hrt_part_group_description] DEFAULT ('!!!NO GROUP ENTERED!!!'),
   [is_active] [int] NULL   
)


GO
