CREATE TABLE [dbo].[machine_defect_locations] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_defect_ID] [int] NOT NULL   ,
   [defect_location_x] [int] NOT NULL   ,
   [defect_location_y] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_defect_locations] PRIMARY KEY CLUSTERED ([ID])
)


GO
