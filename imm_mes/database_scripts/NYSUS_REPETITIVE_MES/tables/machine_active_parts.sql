CREATE TABLE [dbo].[machine_active_parts] (
   [map_id] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_id] [int] NOT NULL   ,
   [tool_id] [int] NOT NULL   ,
   [parts_id] [int] NOT NULL   ,
   [cavity_order] [int] NULL   ,
   [operator_id] [int] NOT NULL   ,
   [start_timestamp] [datetime] NOT NULL 
      CONSTRAINT [DF_machine_active_parts_start_timestamp] DEFAULT (getdate())  ,
   [end_timestamp] [datetime] NULL   ,
   [is_active] [int] NULL   
)


GO
