CREATE TABLE [dbo].[machine_group_racks] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_group_ID] [int] NOT NULL   ,
   [start_time] [datetime] NOT NULL   ,
   [end_time] [datetime] NULL   

   ,CONSTRAINT [PK_machine_group_racks] PRIMARY KEY CLUSTERED ([ID])
)


GO
