CREATE TABLE [dbo].[production_dashboard_slides] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [slide_order] [int] NOT NULL   ,
   [image_src] [varchar](255) NULL ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_production_dashboard_slides_active] DEFAULT ((0))  ,
   [duration] [int] NULL   ,
   [machine_group_ID] [int] NULL   

   ,CONSTRAINT [PK_dashboard_slides] PRIMARY KEY CLUSTERED ([ID])
)


GO
