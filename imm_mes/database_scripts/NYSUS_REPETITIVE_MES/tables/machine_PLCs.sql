CREATE TABLE [dbo].[machine_PLCs] (
   [ID] [int] NOT NULL   ,
   [plc_name] [varchar](50) NOT NULL ,
   [plc_IP_address] [varchar](20) NOT NULL ,
   [plc_type_ID] [int] NOT NULL   

   ,CONSTRAINT [PK_machine_PLCs] PRIMARY KEY CLUSTERED ([ID])
)


GO
