CREATE TABLE [dbo].[machine_monitor_alerts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_cycle_datapoint_ID] [int] NOT NULL   ,
   [limit_triggered] [int] NOT NULL   ,
   [created_timestamp] [datetime] NULL   ,
   [processed_timestamp] [datetime] NULL   ,
   [acknowledged_timestamp] [datetime] NULL   ,
   [acknowledged_by] [int] NULL   
)


GO
