CREATE TABLE [dbo].[machine_schedules] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [cycle_qty] [int] NOT NULL   ,
   [actual_start_time] [datetime] NULL   

   ,CONSTRAINT [PK_machine_schedules] PRIMARY KEY CLUSTERED ([ID])
)


GO
