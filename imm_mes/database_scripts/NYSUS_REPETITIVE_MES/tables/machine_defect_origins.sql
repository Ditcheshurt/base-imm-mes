CREATE TABLE [dbo].[machine_defect_origins] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [parent_ID] [int] NOT NULL   ,
   [machine_type_ID] [int] NULL   ,
   [origin_description] [varchar](100) NOT NULL ,
   [series] [varchar](2) NULL ,
   [active] [bit] NULL   

   ,CONSTRAINT [PK_machine_defect_origins] PRIMARY KEY CLUSTERED ([ID])
)


GO
