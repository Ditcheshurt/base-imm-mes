CREATE TABLE [dbo].[molding_revision_numbers] (
   [id] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tool_id] [int] NOT NULL   ,
   [revision_number] [varchar](20) NOT NULL ,
   [effective_datetime] [datetime] NOT NULL   ,
   [change_desc] [varchar](max) NOT NULL ,
   [user_id] [int] NOT NULL   ,
   [created_datetime] [datetime] NULL   ,
   [active] [int] NULL   

   ,CONSTRAINT [PK_molding_revision_numbers] PRIMARY KEY CLUSTERED ([id])
)

CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20180730-150554] ON [dbo].[molding_revision_numbers] ([tool_id], [active])
CREATE  NONCLUSTERED INDEX [NonClusteredIndex-20180730-150622] ON [dbo].[molding_revision_numbers] ([tool_id], [created_datetime]) INCLUDE ([change_desc], [user_id])

GO
