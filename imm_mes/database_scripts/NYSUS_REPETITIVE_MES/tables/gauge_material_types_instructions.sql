CREATE TABLE [dbo].[gauge_material_types_instructions] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [gauge_material_type_ID] [int] NOT NULL   ,
   [instr_order] [int] NULL   ,
   [description] [varchar](50) NULL ,
   [is_pass_fail] [bit] NULL   ,
   [is_calculated] [bit] NULL   ,
   [is_critical] [bit] NULL   ,
   [base_value] [float] NULL   ,
   [min_value] [float] NULL   ,
   [max_value] [float] NULL   ,
   [image_url] [varchar](150) NULL ,
   [instruction] [varchar](500) NULL ,
   [units] [nvarchar](10) NULL ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_gauge_data_point_setup_active] DEFAULT ((0))  ,
   [xbar_ucl] [float] NULL   ,
   [xbar_lcl] [float] NULL   ,
   [range_ucl] [float] NULL   ,
   [range_lcl] [float] NULL   ,
   [tolerance_target] [float] NULL   ,
   [tolerance_delta] [float] NULL   

   ,CONSTRAINT [PK_gauge_data_point_setup] PRIMARY KEY CLUSTERED ([ID])
)


GO
