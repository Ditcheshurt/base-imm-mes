CREATE TABLE [dbo].[machine_cycles] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [cycle_time] [datetime] NOT NULL   ,
   [cycle_sequence] [int] NOT NULL   ,
   [run_ID] [int] NULL   ,
   [operator_ID] [int] NULL   ,
   [mrp_backflush_time] [datetime] NULL   ,
   [mrp_backflush_max_record] [int] NULL   ,
   [cycle_duration] [float] NULL   ,
   [lotnum] [varchar](50) NULL 

   ,CONSTRAINT [PK_shots] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [_dta_index_machine_cycles_13_933578364__K1_K4] ON [dbo].[machine_cycles] ([ID], [cycle_time])
CREATE  NONCLUSTERED INDEX [_dta_index_machine_cycles_5_1525580473__K1_K2] ON [dbo].[machine_cycles] ([ID], [machine_ID])
CREATE  NONCLUSTERED INDEX [_dta_index_machine_cycles_6_933578364__K2_K4_10] ON [dbo].[machine_cycles] ([machine_ID], [cycle_time]) INCLUDE ([cycle_duration])

GO
