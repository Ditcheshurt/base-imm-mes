CREATE TABLE [dbo].[loggy] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [log_time] [datetime] NOT NULL   ,
   [log_msg] [varchar](2000) NULL 

   ,CONSTRAINT [PK_loggy] PRIMARY KEY CLUSTERED ([ID])
)


GO
