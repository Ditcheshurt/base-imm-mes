CREATE TABLE [dbo].[gauge_autogauge] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [cycle_ID] [int] NULL   ,
   [serial_number] [varchar](50) NULL ,
   [part_name] [varchar](200) NULL ,
   [m1] [float] NULL   ,
   [m2] [float] NULL   ,
   [m3] [float] NULL   ,
   [m4] [float] NULL   ,
   [taper] [float] NULL   ,
   [sp1_low] [float] NULL   ,
   [sp1_high] [float] NULL   ,
   [sp2_low] [float] NULL   ,
   [sp2_high] [float] NULL   ,
   [sp3_low] [float] NULL   ,
   [sp3_high] [float] NULL   ,
   [sp4_low] [float] NULL   ,
   [sp4_high] [float] NULL   ,
   [spt_low] [float] NULL   ,
   [spt_high] [float] NULL   ,
   [good_part] [bit] NULL   ,
   [recorded_date] [datetime] NULL   ,
   [file_name] [nvarchar](100) NULL ,
   [comment] [varchar](500) NULL 

   ,CONSTRAINT [PK_gauge_litchfield_autogauge2] PRIMARY KEY CLUSTERED ([ID])
)


GO
