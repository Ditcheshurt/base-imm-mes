CREATE TABLE [dbo].[machine_monitor_action_handler] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [action_handler_type] [varchar](50) NOT NULL ,
   [action_handler] [varchar](200) NOT NULL ,
   [action_parameter_list] [varchar](1000) NOT NULL ,
   [action_parameter_type_list] [varchar](1000) NOT NULL 
)


GO
