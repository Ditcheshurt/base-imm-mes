CREATE TABLE [dbo].[CUSTOM_plc_machine_mapping] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [plc_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_CUSTOM_plc_machine_mapping_active] DEFAULT ((0))  

   ,CONSTRAINT [PK_CUSTOM_plc_machine_mapping] PRIMARY KEY CLUSTERED ([ID])
)


GO
