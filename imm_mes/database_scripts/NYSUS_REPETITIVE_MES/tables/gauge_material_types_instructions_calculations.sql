CREATE TABLE [dbo].[gauge_material_types_instructions_calculations] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [gauge_material_types_instruction_ID] [int] NULL   ,
   [operand1_gauge_material_types_instruction_ID] [int] NULL   ,
   [operand2_gauge_material_types_instruction_ID] [int] NULL   ,
   [math_operator] [char](1) NULL 

   ,CONSTRAINT [PK_gauge_material_types_instructions_calculations] PRIMARY KEY CLUSTERED ([ID])
)


GO
