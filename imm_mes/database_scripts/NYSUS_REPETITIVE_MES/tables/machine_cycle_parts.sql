CREATE TABLE [dbo].[machine_cycle_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [cycle_ID] [int] NOT NULL   ,
   [tool_part_ID] [int] NOT NULL   ,
   [serial_number] [varchar](50) NULL ,
   [full_barcode] [varchar](50) NULL ,
   [container_ID] [int] NULL   ,
   [partial_container_ID] [int] NULL   ,
   [insert_time] [datetime] NULL   ,
   [machine_group_rack_ID] [int] NULL   ,
   [machine_group_rack_part_order] [int] NULL   ,
   [machine_group_rack_pack_time] [datetime] NULL   ,
   [machine_group_rack_pack_operator_ID] [int] NULL   ,
   [plc_op_done_value] [int] NULL   ,
   [disposition] [varchar](50) NULL ,
   [disposition_time] [datetime] NULL   ,
   [revision_number] [varchar](50) NULL ,
   [serial_calc] AS (concat(left([full_barcode],len([full_barcode])-(5)),[tool_part_id],[serial_number])),
   [rework_datetime] [datetime] NULL   ,
   [needs_qad_reverse] [int] NULL 
      CONSTRAINT [DF_machine_cycle_parts_needs_qad_reverse] DEFAULT ((0))  

   ,CONSTRAINT [PK_machine_cycle_parts] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [_dta_index_machine_cycle_parts_13_1076198884__K2] ON [dbo].[machine_cycle_parts] ([cycle_ID])
CREATE  NONCLUSTERED INDEX [IX_machine_cycle_parts] ON [dbo].[machine_cycle_parts] ([cycle_ID], [tool_part_ID])
CREATE  NONCLUSTERED INDEX [IX_machine_cycle_parts__tool_part_ID] ON [dbo].[machine_cycle_parts] ([tool_part_ID]) INCLUDE ([ID], [cycle_ID])
CREATE  NONCLUSTERED INDEX [NonClusteredIndex-dispo] ON [dbo].[machine_cycle_parts] ([disposition]) INCLUDE ([ID], [tool_part_ID], [serial_number])
CREATE  NONCLUSTERED INDEX [NonClusteredIndex-serial_number_full_barcode_disp] ON [dbo].[machine_cycle_parts] ([serial_number], [full_barcode], [disposition], [tool_part_ID]) INCLUDE ([ID])

GO
