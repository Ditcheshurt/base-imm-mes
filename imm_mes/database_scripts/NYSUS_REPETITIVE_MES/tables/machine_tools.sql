CREATE TABLE [dbo].[machine_tools] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [machine_ID] [int] NOT NULL   ,
   [tool_ID] [int] NOT NULL   ,
   [target_cycle_time] [int] NULL   ,
   [over_cycle_grace_period] [int] NULL   

   ,CONSTRAINT [PK_machine_tools] PRIMARY KEY CLUSTERED ([ID])
)


GO
