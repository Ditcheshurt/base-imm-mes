ALTER TABLE [dbo].[alerts] WITH CHECK ADD CONSTRAINT [FK_alerts_alert_types]
   FOREIGN KEY([alert_type_ID]) REFERENCES [dbo].[alert_types] ([ID])

GO
