ALTER TABLE [dbo].[container_parts] WITH CHECK ADD CONSTRAINT [FK_container_parts_machine_cycle_parts]
   FOREIGN KEY([machine_cycle_part_ID]) REFERENCES [dbo].[machine_cycle_parts] ([ID])
   ON DELETE CASCADE

GO
