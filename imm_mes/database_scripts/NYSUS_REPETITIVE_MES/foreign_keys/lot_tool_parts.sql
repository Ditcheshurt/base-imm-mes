ALTER TABLE [dbo].[lot_tool_parts] WITH CHECK ADD CONSTRAINT [FK_lot_tool_parts_lot_material_types]
   FOREIGN KEY([lot_material_type_ID]) REFERENCES [dbo].[lot_material_types] ([ID])

GO
ALTER TABLE [dbo].[lot_tool_parts] WITH CHECK ADD CONSTRAINT [FK_lot_tool_parts_tool_parts]
   FOREIGN KEY([tool_part_ID]) REFERENCES [dbo].[tool_parts] ([ID])

GO
