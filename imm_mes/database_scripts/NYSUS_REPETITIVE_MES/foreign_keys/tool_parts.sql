ALTER TABLE [dbo].[tool_parts] WITH CHECK ADD CONSTRAINT [FK_tool_parts_parts]
   FOREIGN KEY([part_ID]) REFERENCES [dbo].[parts] ([ID])

GO
ALTER TABLE [dbo].[tool_parts] WITH CHECK ADD CONSTRAINT [FK_tool_parts_tools]
   FOREIGN KEY([tool_ID]) REFERENCES [dbo].[tools] ([ID])

GO
