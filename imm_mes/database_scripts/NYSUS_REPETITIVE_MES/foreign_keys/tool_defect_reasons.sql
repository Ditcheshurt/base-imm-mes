ALTER TABLE [dbo].[tool_defect_reasons] WITH CHECK ADD CONSTRAINT [FK_tool_defect_reasons_machine_defect_reasons]
   FOREIGN KEY([defect_reason_ID]) REFERENCES [dbo].[machine_defect_reasons] ([ID])

GO
ALTER TABLE [dbo].[tool_defect_reasons] WITH CHECK ADD CONSTRAINT [FK_tool_defect_reasons_tools]
   FOREIGN KEY([tool_ID]) REFERENCES [dbo].[tools] ([ID])

GO
