ALTER TABLE [dbo].[lot_machines] WITH CHECK ADD CONSTRAINT [FK_lot_lot_machines_lot_machine_group_lots]
   FOREIGN KEY([lot_material_type_ID]) REFERENCES [dbo].[lot_material_types] ([ID])

GO
ALTER TABLE [dbo].[lot_machines] WITH CHECK ADD CONSTRAINT [FK_lot_lot_machines_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
