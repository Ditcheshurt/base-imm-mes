ALTER TABLE [dbo].[alert_schedule] WITH CHECK ADD CONSTRAINT [FK_alert_schedule_alerts]
   FOREIGN KEY([alert_ID]) REFERENCES [dbo].[alerts] ([ID])

GO
