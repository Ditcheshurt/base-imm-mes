ALTER TABLE [dbo].[machine_operator_event_history] WITH CHECK ADD CONSTRAINT [FK_machine_operator_event_history_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
