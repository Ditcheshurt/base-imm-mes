ALTER TABLE [dbo].[lot_machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_part_lot_numbers_machine_cycle_parts]
   FOREIGN KEY([machine_cycle_part_ID]) REFERENCES [dbo].[machine_cycle_parts] ([ID])

GO
ALTER TABLE [dbo].[lot_machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_part_lot_numbers_machine_group_material_types]
   FOREIGN KEY([lot_material_type_ID]) REFERENCES [dbo].[lot_material_types] ([ID])

GO
