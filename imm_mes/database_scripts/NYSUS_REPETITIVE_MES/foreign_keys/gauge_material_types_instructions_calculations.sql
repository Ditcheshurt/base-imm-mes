ALTER TABLE [dbo].[gauge_material_types_instructions_calculations] WITH CHECK ADD CONSTRAINT [FK_gauge_material_types_instructions_calculations_gauge_material_types_instructions]
   FOREIGN KEY([gauge_material_types_instruction_ID]) REFERENCES [dbo].[gauge_material_types_instructions] ([ID])

GO
ALTER TABLE [dbo].[gauge_material_types_instructions_calculations] WITH CHECK ADD CONSTRAINT [FK_gauge_material_types_instructions_calculations_gauge_material_types_instructions1]
   FOREIGN KEY([operand1_gauge_material_types_instruction_ID]) REFERENCES [dbo].[gauge_material_types_instructions] ([ID])

GO
ALTER TABLE [dbo].[gauge_material_types_instructions_calculations] WITH CHECK ADD CONSTRAINT [FK_gauge_material_types_instructions_calculations_gauge_material_types_instructions2]
   FOREIGN KEY([operand1_gauge_material_types_instruction_ID]) REFERENCES [dbo].[gauge_material_types_instructions] ([ID])

GO
