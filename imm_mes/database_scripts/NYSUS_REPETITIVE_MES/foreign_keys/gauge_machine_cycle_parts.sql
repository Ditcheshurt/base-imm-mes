ALTER TABLE [dbo].[gauge_machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_gauge_machine_cycle_parts_gauge_material_types_steps]
   FOREIGN KEY([gauge_material_types_instruction_ID]) REFERENCES [dbo].[gauge_material_types_instructions] ([ID])

GO
ALTER TABLE [dbo].[gauge_machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_gauge_machine_cycle_parts_machine_cycle_parts]
   FOREIGN KEY([machine_cycle_part_ID]) REFERENCES [dbo].[machine_cycle_parts] ([ID])

GO
