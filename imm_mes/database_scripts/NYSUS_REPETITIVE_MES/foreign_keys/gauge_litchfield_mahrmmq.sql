ALTER TABLE [dbo].[gauge_litchfield_mahrmmq] WITH CHECK ADD CONSTRAINT [FK_gauge_litchfield_mahrmmq_machine_cycle_parts]
   FOREIGN KEY([machine_cycle_part_ID]) REFERENCES [dbo].[machine_cycle_parts] ([ID])

GO
