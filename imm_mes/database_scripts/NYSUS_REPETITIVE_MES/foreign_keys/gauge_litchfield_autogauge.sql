ALTER TABLE [dbo].[gauge_litchfield_autogauge] WITH CHECK ADD CONSTRAINT [FK_gauge_litchfield_autogauge_machine_cycles]
   FOREIGN KEY([cycle_ID]) REFERENCES [dbo].[machine_cycles] ([ID])

GO
