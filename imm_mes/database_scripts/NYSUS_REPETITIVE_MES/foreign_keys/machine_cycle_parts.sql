ALTER TABLE [dbo].[machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_parts_machine_cycles]
   FOREIGN KEY([cycle_ID]) REFERENCES [dbo].[machine_cycles] ([ID])
   ON DELETE CASCADE

GO
ALTER TABLE [dbo].[machine_cycle_parts] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_parts_tool_parts]
   FOREIGN KEY([tool_part_ID]) REFERENCES [dbo].[tool_parts] ([ID])
   ON DELETE CASCADE

GO
