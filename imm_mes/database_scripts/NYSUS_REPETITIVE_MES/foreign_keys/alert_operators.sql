ALTER TABLE [dbo].[alert_operators] WITH CHECK ADD CONSTRAINT [FK_operator_alerts_alerts]
   FOREIGN KEY([alert_ID]) REFERENCES [dbo].[alerts] ([ID])

GO
