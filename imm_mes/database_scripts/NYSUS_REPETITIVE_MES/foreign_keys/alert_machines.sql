ALTER TABLE [dbo].[alert_machines] WITH CHECK ADD CONSTRAINT [FK_machine_alerts_alerts]
   FOREIGN KEY([alert_ID]) REFERENCES [dbo].[alerts] ([ID])

GO
ALTER TABLE [dbo].[alert_machines] WITH CHECK ADD CONSTRAINT [FK_machine_alerts_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
