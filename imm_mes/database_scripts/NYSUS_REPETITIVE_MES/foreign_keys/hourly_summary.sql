ALTER TABLE [dbo].[hourly_summary] WITH CHECK ADD CONSTRAINT [FK_hourly_summary_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
