ALTER TABLE [dbo].[gauge_material_types] WITH CHECK ADD CONSTRAINT [FK_gauge_material_types_tool_parts]
   FOREIGN KEY([tool_part_ID]) REFERENCES [dbo].[tool_parts] ([ID])

GO
