ALTER TABLE [dbo].[machine_downtime_log] WITH CHECK ADD CONSTRAINT [FK_machine_downtime_log_machine_downtime_reasons]
   FOREIGN KEY([reason_ID]) REFERENCES [dbo].[machine_downtime_reasons] ([ID])

GO
ALTER TABLE [dbo].[machine_downtime_log] WITH CHECK ADD CONSTRAINT [FK_machine_downtime_log_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
ALTER TABLE [dbo].[machine_downtime_log] WITH CHECK ADD CONSTRAINT [FK_machine_downtime_log_tools]
   FOREIGN KEY([tool_ID]) REFERENCES [dbo].[tools] ([ID])

GO
