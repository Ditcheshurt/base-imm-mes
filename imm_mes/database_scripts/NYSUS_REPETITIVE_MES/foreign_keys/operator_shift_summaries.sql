ALTER TABLE [dbo].[operator_shift_summaries] WITH CHECK ADD CONSTRAINT [FK_operator_shift_summaries_shift_summary]
   FOREIGN KEY([shift_summary_ID]) REFERENCES [dbo].[shift_summary] ([ID])
   ON DELETE CASCADE

GO
