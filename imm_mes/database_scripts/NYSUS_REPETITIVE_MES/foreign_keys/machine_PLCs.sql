ALTER TABLE [dbo].[machine_PLCs] WITH CHECK ADD CONSTRAINT [FK_machine_PLCs_machine_PLC_types]
   FOREIGN KEY([plc_type_ID]) REFERENCES [dbo].[machine_PLC_types] ([ID])

GO
