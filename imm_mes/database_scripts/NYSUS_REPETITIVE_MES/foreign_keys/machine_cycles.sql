ALTER TABLE [dbo].[machine_cycles] WITH CHECK ADD CONSTRAINT [FK_machine_cycles_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
ALTER TABLE [dbo].[machine_cycles] WITH CHECK ADD CONSTRAINT [FK_machine_cycles_tools]
   FOREIGN KEY([tool_ID]) REFERENCES [dbo].[tools] ([ID])

GO
