ALTER TABLE [dbo].[machine_ojt_jobs] WITH CHECK ADD CONSTRAINT [FK_machine_ojt_jobs_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
