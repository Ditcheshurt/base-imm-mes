ALTER TABLE [dbo].[shift_summary] WITH CHECK ADD CONSTRAINT [FK_shift_summary_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
