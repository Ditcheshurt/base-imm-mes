ALTER TABLE [dbo].[machine_cycle_data_points] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_data_points_data_point_setup]
   FOREIGN KEY([data_point_ID]) REFERENCES [dbo].[data_point_setup] ([ID])

GO
ALTER TABLE [dbo].[machine_cycle_data_points] WITH CHECK ADD CONSTRAINT [FK_machine_cycle_data_points_machine_cycles]
   FOREIGN KEY([cycle_ID]) REFERENCES [dbo].[machine_cycles] ([ID])

GO
