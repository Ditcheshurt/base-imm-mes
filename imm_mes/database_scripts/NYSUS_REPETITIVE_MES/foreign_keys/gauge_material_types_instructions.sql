ALTER TABLE [dbo].[gauge_material_types_instructions] WITH CHECK ADD CONSTRAINT [FK_gauge_material_types_steps_gauge_material_types]
   FOREIGN KEY([gauge_material_type_ID]) REFERENCES [dbo].[gauge_material_types] ([ID])

GO
