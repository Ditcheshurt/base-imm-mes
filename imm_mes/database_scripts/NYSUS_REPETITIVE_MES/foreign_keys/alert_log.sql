ALTER TABLE [dbo].[alert_log] WITH CHECK ADD CONSTRAINT [FK_alert_log_alerts]
   FOREIGN KEY([alert_ID]) REFERENCES [dbo].[alerts] ([ID])

GO
ALTER TABLE [dbo].[alert_log] WITH CHECK ADD CONSTRAINT [FK_alert_log_machines]
   FOREIGN KEY([machine_ID]) REFERENCES [dbo].[machines] ([ID])

GO
