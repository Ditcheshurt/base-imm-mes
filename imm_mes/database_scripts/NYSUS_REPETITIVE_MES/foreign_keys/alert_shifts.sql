ALTER TABLE [dbo].[alert_shifts] WITH CHECK ADD CONSTRAINT [FK_alert_shifts_alerts]
   FOREIGN KEY([alert_ID]) REFERENCES [dbo].[alerts] ([ID])

GO
