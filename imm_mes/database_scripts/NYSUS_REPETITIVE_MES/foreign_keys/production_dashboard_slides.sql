ALTER TABLE [dbo].[production_dashboard_slides] WITH CHECK ADD CONSTRAINT [FK_production_dashboard_slides_machine_groups]
   FOREIGN KEY([machine_group_ID]) REFERENCES [dbo].[machine_groups] ([ID])

GO
