ALTER TABLE [dbo].[nysus_plc_tags] WITH CHECK ADD CONSTRAINT [FK_nysus_plc_tags_nysus_plcs]
   FOREIGN KEY([plc_ID]) REFERENCES [dbo].[nysus_plcs] ([ID])

GO
