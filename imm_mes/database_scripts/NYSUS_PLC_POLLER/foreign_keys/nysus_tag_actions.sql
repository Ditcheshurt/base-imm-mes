ALTER TABLE [dbo].[nysus_tag_actions] WITH CHECK ADD CONSTRAINT [FK_nysus_tag_actions_nysus_actions]
   FOREIGN KEY([action_ID]) REFERENCES [dbo].[nysus_actions] ([ID])

GO
ALTER TABLE [dbo].[nysus_tag_actions] WITH CHECK ADD CONSTRAINT [FK_nysus_tag_actions_nysus_plc_tags]
   FOREIGN KEY([tag_ID]) REFERENCES [dbo].[nysus_plc_tags] ([ID])

GO
