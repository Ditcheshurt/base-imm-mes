ALTER TABLE [dbo].[nysus_tag_action_params] WITH CHECK ADD CONSTRAINT [FK_nysus_tag_action_params_nysus_actions]
   FOREIGN KEY([action_ID]) REFERENCES [dbo].[nysus_actions] ([ID])

GO
