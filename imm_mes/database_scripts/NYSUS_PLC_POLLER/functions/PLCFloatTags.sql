SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Rob Kahle
-- Create date: 6/1/2018
-- Description:	This will return all the 'REAL' floating point values out of the plc tags table
-- =============================================
CREATE FUNCTION [dbo].[PLCFloatTags] 
(	
	-- Add the parameters for the function here
	@tag_desc varchar(50)
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		tags.tag, tags.base_addr,
		(
			SIGN((TRY_CAST(tagH.value AS INT) * 65536 + TRY_CAST(tagL.value AS INT)))
			* (1.0 + ((TRY_CAST(tagH.value AS INT) * 65536 + TRY_CAST(tagL.value AS INT)) & 0x007FFFFF)
			* POWER(CAST(2 AS REAL), -23)) * POWER(CAST(2 AS REAL), ((TRY_CAST(tagH.value AS INT) * 65536 + TRY_CAST(tagL.value AS INT)) & 0x7f800000) / 0x00800000 - 127)
		) AS [value]
	FROM (
		SELECT p1.* FROM (
			SELECT
				(SELECT items FROM dbo.Split(npt1.description,'_') WHERE ID = 1) AS tag,
				(SELECT items FROM dbo.Split(npt1.description,'_') WHERE ID = 2) AS base_addr,
				(SELECT TRY_CAST(items AS int) FROM dbo.Split(npt1.description,'_') WHERE ID = 2) + (TRY_CAST(npt1.name AS int) - (SELECT TRY_CAST(items AS int) FROM dbo.Split(npt1.description,'_') WHERE ID = 2) ) AS addr,
				(SELECT items FROM dbo.Split(npt1.description,'_') WHERE ID = 3) AS part
			FROM nysus_plc_tags npt1
			WHERE npt1.value_type = 'REAL'
		) t1
		PIVOT (
			MAX(t1.addr)
			FOR part IN ([HIGH],[LOW])
		) p1
	) tags
	LEFT JOIN nysus_plc_tags tagH ON tagH.name = TRY_CAST(tags.HIGH AS varchar)
	LEFT JOIN nysus_plc_tags tagL ON tagL.name = TRY_CAST(tags.LOW AS varchar)
	WHERE tags.tag = @tag_desc OR tags.base_addr = @tag_desc
)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
