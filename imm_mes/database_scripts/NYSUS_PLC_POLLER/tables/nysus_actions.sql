CREATE TABLE [dbo].[nysus_actions] (
   [ID] [int] NOT NULL   ,
   [command_text] [varchar](1000) NOT NULL ,
   [command_type] [varchar](50) NOT NULL 

   ,CONSTRAINT [PK_nysus_actions] PRIMARY KEY CLUSTERED ([ID])
)


GO
