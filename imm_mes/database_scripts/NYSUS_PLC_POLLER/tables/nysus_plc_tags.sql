CREATE TABLE [dbo].[nysus_plc_tags] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [plc_ID] [int] NOT NULL   ,
   [name] [varchar](100) NOT NULL ,
   [value] [varchar](50) NULL ,
   [description] [varchar](50) NULL ,
   [value_type] [varchar](50) NOT NULL ,
   [last_update] [datetime] NULL   ,
   [write_enable] [bit] NOT NULL   ,
   [active] [bit] NOT NULL   ,
   [is_primary] [bit] NOT NULL 
      CONSTRAINT [DF_nysus_plc_tags_is_primary] DEFAULT ((1))  ,
   [read_order] [int] NULL   

   ,CONSTRAINT [PK_nysus_plc_tags] PRIMARY KEY CLUSTERED ([ID])
)

CREATE  NONCLUSTERED INDEX [IX_nysus_plc_tags] ON [dbo].[nysus_plc_tags] ([description])

GO
