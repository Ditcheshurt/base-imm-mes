CREATE TABLE [dbo].[nysus_plc_types] (
   [ID] [varchar](50) NOT NULL ,
   [value] [varchar](50) NOT NULL ,
   [description] [varchar](100) NULL ,
   [cpu_type] [varchar](50) NULL ,
   [enet_type] [varchar](50) NULL 

   ,CONSTRAINT [PK_nysus_plc_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
