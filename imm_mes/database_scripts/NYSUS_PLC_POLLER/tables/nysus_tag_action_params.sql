CREATE TABLE [dbo].[nysus_tag_action_params] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [action_ID] [int] NOT NULL   ,
   [name] [varchar](50) NULL ,
   [value] [varchar](50) NULL 

   ,CONSTRAINT [PK_nysus_tag_action_params] PRIMARY KEY CLUSTERED ([ID])
)


GO
