CREATE TABLE [dbo].[nysus_tag_actions] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [tag_ID] [int] NOT NULL   ,
   [action_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL   

   ,CONSTRAINT [PK_nysus_plc_action] PRIMARY KEY CLUSTERED ([ID])
)


GO
