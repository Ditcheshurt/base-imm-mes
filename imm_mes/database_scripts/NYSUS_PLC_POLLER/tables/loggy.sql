CREATE TABLE [dbo].[loggy] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [log_time] [datetime] NOT NULL   ,
   [log_msg] [varchar](2000) NOT NULL ,
   [tag_ID] [varchar](50) NULL ,
   [tag_desc] [varchar](50) NULL ,
   [tag_value] [varchar](100) NULL 

   ,CONSTRAINT [PK_loggy] PRIMARY KEY CLUSTERED ([ID])
)


GO
