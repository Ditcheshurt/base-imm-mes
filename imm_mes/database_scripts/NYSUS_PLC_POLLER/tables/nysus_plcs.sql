CREATE TABLE [dbo].[nysus_plcs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ip_address] [varchar](15) NOT NULL ,
   [description] [varchar](50) NULL ,
   [poll_interval] [int] NOT NULL   ,
   [type_ID] [int] NOT NULL   ,
   [active] [bit] NOT NULL   ,
   [tag_update_interval] [int] NULL   

   ,CONSTRAINT [PK_nysus_plcs] PRIMARY KEY CLUSTERED ([ID])
)


GO
