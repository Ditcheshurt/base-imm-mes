SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Sass
-- Create date: 1/8/2016
-- Description:	Logs things
-- =============================================
CREATE PROCEDURE [dbo].[sp_addToLoggy]
	@tag_ID varchar(50) = '',
	@tag_desc varchar(50) = '',
	@tag_value varchar(100) = '',
	@comment varchar(2000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @comment = ''
		SELECT @comment = name 
		FROM nysus_plc_tags
		WHERE ID = CAST(@tag_ID as int)

    INSERT INTO loggy (log_time, log_msg, tag_ID, tag_desc, tag_value) VALUES (
		GETDATE(),
		@comment, 
		@tag_ID, 
		@tag_desc,
		@tag_value
	)
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
