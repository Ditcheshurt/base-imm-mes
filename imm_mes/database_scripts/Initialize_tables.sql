/****** Script for SelectTopNRows command from SSMS  ******/
USE MES_COMMON
GO
	DECLARE @sys_ID TABLE (ID int)
	DECLARE @op_ID TABLE (ID int)

	DECLARE @sysID int
	DECLARE @opID int

	INSERT INTO @sys_ID
	SELECT t0.ID FROM (
		INSERT INTO systems (system_name, [database_name], process_type, system_active)
		OUTPUT inserted.ID
		VALUES ('Test','NYSUS_REPETITIVE_MES','REPETITIVE',1)
	) t0

	SELECT TOP 1 @sysID = ID FROM @sys_ID

	INSERT INTO system_ip_ranges (system_ID, starting_ip, ending_ip)
	VALUES (@sysID,'192.168.0.100','192.168.0.200')

	INSERT INTO @op_ID
	SELECT t1.ID FROM (
		INSERT INTO operators (email, badge_ID, first_name, last_name,[password])
		OUTPUT inserted.ID
		VALUES ('support@nysus.net',1005,'Support','Nysus','test24')
	) t1

	SELECT TOP 1 @opID = ID FROM @op_ID

	INSERT INTO roles ([name], role_priority)
	VALUES ('operator',0), ('team leader',1), ('supervisor',2), ('editor',3), ('admin',4), ('support',5)

	INSERT INTO operator_roles
	SELECT @opID, ID FROM roles

USE NYSUS_REPETITIVE_MES
GO
	INSERT INTO machine_defect_reasons (parent_ID, defect_description, active)
	VALUES  (0,'SPLAY',1),(0,'VARIANCE',0),(0,'SHORTS',1),(0,'NICKS/SCRATCHES',1),(0,'CONTAMINATION',1),(0,'FLASH',1),(0,'FLOWLINES',1),(0,'COLD SLUGS',1),(0,'CRACKS',0),(0,'BUBBLES/BLISTERS',1)

	INSERT INTO machine_downtime_reason_oee_categories (ID,oee_category_desc,count_against_oee,is_scheduled,require_team_leader)
	VALUES (1,'Performance Issue',0,0,0),(2,'Availability Issue',1,0,0),(3,'Planned DT',0,1,1)

	INSERT INTO machine_downtime_reasons (parent_ID,reason_description,count_against,tool_change_ignore,active,machine_type_ID,oee_category,maximum_duration,is_micro_stop)
	VALUES (0,'No Reason Entered',1,0,0,1,1,9999,NULL),(0,'Micro-stop',0,0,1,1,1,9999,1),(0,'Process',0,0,1,1,1,9999,NULL),(0,'Mold Change',0,0,1,1,2,9999,NULL)

	INSERT INTO machine_tools (machine_ID,tool_ID,target_cycle_time,over_cycle_grace_period)
	VALUES (1,1,30,5)

	INSERT INTO machine_types (machine_type_description)
	VALUES ('Test_Injection_Molding')

	INSERT INTO machines (ID,machine_type_ID,machine_name,machine_number,up,current_tool_ID,ignored,PLC_ID,station_IP_address,system_ID,over_cycle_grace_period,show_reporting,is_station_default)
	VALUES (1,1,'Test Machine','TM01',0,1,0,1,'192.168.0.100',1,5,1,0)

USE NYSUS_PLC_POLLER
GO
	INSERT INTO nysus_plcs (ip_address,[description],poll_interval,[type_ID],active,tag_update_interval)
	VALUES ('192.168.24.21','Test PLC',250,1,1,3600)

	INSERT INTO nysus_plc_tags (plc_ID,[name],[value],[description],value_type,write_enable,active,is_primary,read_order)
	VALUES (1,'Test_PLC',0,'TM01','DINT',0,1,1,10)