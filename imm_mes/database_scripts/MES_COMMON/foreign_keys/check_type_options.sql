ALTER TABLE [dbo].[check_type_options] WITH CHECK ADD CONSTRAINT [FK_check_type_options_check_types]
   FOREIGN KEY([check_type_ID]) REFERENCES [dbo].[check_types] ([ID])

GO
