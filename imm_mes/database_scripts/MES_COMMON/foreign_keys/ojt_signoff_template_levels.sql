ALTER TABLE [dbo].[ojt_signoff_template_levels] WITH CHECK ADD CONSTRAINT [FK_ojt_signoff_template_levels_ojt_signoff_templates]
   FOREIGN KEY([ojt_signoff_template_ID]) REFERENCES [dbo].[ojt_signoff_templates] ([ID])

GO
