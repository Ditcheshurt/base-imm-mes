ALTER TABLE [dbo].[check_history] WITH CHECK ADD CONSTRAINT [FK_check_history_check_types]
   FOREIGN KEY([check_type_ID]) REFERENCES [dbo].[check_types] ([ID])

GO
