ALTER TABLE [dbo].[shift_time_intervals] WITH CHECK ADD CONSTRAINT [FK_shift_time_intervals_shifts]
   FOREIGN KEY([shift_ID]) REFERENCES [dbo].[shifts] ([ID])

GO
ALTER TABLE [dbo].[shift_time_intervals] WITH CHECK ADD CONSTRAINT [FK_shift_time_intervals_time_intervals]
   FOREIGN KEY([time_interval_ID]) REFERENCES [dbo].[time_intervals] ([ID])

GO
