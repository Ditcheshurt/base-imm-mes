ALTER TABLE [dbo].[ojt_signoff_template_levels_roles] WITH CHECK ADD CONSTRAINT [FK_ojt_signoff_level_roles_ojt_signoff_template_levels]
   FOREIGN KEY([ojt_signoff_template_level_ID]) REFERENCES [dbo].[ojt_signoff_template_levels] ([ID])

GO
ALTER TABLE [dbo].[ojt_signoff_template_levels_roles] WITH CHECK ADD CONSTRAINT [FK_ojt_signoff_level_roles_roles]
   FOREIGN KEY([role_ID]) REFERENCES [dbo].[roles] ([ID])

GO
