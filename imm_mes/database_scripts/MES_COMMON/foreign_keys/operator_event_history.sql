ALTER TABLE [dbo].[operator_event_history] WITH CHECK ADD CONSTRAINT [FK_operator_event_history_operator_events]
   FOREIGN KEY([operator_event_ID]) REFERENCES [dbo].[operator_events] ([ID])

GO
ALTER TABLE [dbo].[operator_event_history] WITH CHECK ADD CONSTRAINT [FK_operator_event_history_operators]
   FOREIGN KEY([operator_ID]) REFERENCES [dbo].[operators] ([ID])

GO
