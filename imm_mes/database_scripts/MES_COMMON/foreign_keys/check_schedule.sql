ALTER TABLE [dbo].[check_schedule] WITH CHECK ADD CONSTRAINT [FK_check_schedule_check_types]
   FOREIGN KEY([check_type_ID]) REFERENCES [dbo].[check_types] ([ID])

GO
