ALTER TABLE [dbo].[ojt_signoffs] WITH CHECK ADD CONSTRAINT [FK_ojt_signoffs_ojt_requirements]
   FOREIGN KEY([ojt_requirement_ID]) REFERENCES [dbo].[ojt_requirements] ([ID])

GO
ALTER TABLE [dbo].[ojt_signoffs] WITH CHECK ADD CONSTRAINT [FK_ojt_signoffs_ojt_signoff_template_levels_roles]
   FOREIGN KEY([ojt_signoff_template_levels_roles_ID]) REFERENCES [dbo].[ojt_signoff_template_levels_roles] ([ID])

GO
