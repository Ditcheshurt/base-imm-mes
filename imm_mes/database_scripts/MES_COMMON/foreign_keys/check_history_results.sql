ALTER TABLE [dbo].[check_history_results] WITH CHECK ADD CONSTRAINT [FK_check_history_results_check_history_parts]
   FOREIGN KEY([check_history_part_ID]) REFERENCES [dbo].[check_history_parts] ([ID])

GO
