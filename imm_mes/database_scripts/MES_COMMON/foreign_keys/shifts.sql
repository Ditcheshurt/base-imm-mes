ALTER TABLE [dbo].[shifts] WITH CHECK ADD CONSTRAINT [FK_shifts_systems]
   FOREIGN KEY([system_ID]) REFERENCES [dbo].[systems] ([ID])

GO
