ALTER TABLE [dbo].[ojt_requirements] WITH CHECK ADD CONSTRAINT [FK_ojt_requirements_ojt_requirement_types]
   FOREIGN KEY([ojt_requirement_type_ID]) REFERENCES [dbo].[ojt_requirement_types] ([ID])

GO
ALTER TABLE [dbo].[ojt_requirements] WITH CHECK ADD CONSTRAINT [FK_ojt_requirements_ojt_signoff_templates]
   FOREIGN KEY([ojt_signoff_template_ID]) REFERENCES [dbo].[ojt_signoff_templates] ([ID])

GO
