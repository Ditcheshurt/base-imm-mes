ALTER TABLE [dbo].[ojt_group_requirements] WITH CHECK ADD CONSTRAINT [FK_ojt_group_requirements_ojt_groups]
   FOREIGN KEY([ojt_group_ID]) REFERENCES [dbo].[ojt_groups] ([ID])

GO
