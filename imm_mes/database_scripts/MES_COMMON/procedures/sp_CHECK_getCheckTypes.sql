SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 9/8/2016
-- Description:	Gets all checks, or one specific
-- =============================================
CREATE PROCEDURE [dbo].[sp_CHECK_getCheckTypes] 
	-- Add the parameters for the stored procedure here
	@check_type_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM check_types
	WHERE (@check_type_ID = 0 OR ID = @check_type_ID)
	ORDER BY ID

	
	--SELECT *
	--FROM check_type_options
	--WHERE (@check_type_ID = 0 OR check_type_ID = @check_type_ID)
	--ORDER BY ID
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
