SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 2/26/16
-- Description:	get shift start and end times
-- =============================================
CREATE PROCEDURE [dbo].[sp_getShift]
	@system_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @reqdate datetime = GETDATE()

	SELECT 
		s.ID,
		s.shift_desc
	FROM
		shifts s
		JOIN shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN time_intervals t ON sti.time_interval_ID = t.ID
	WHERE
		s.system_ID = @system_ID
		AND
			CONVERT(time, @reqdate) BETWEEN t.interval_start_time AND t.interval_end_time


END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
