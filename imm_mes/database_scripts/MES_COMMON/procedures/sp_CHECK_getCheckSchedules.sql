SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 9/8/2016
-- Description:	Gets all check scheduless, or one specific
-- =============================================
CREATE PROCEDURE sp_CHECK_getCheckSchedules 
	-- Add the parameters for the stored procedure here
	@check_schedule_ID int = 0,
	@check_type_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @check_schedule_ID, @check_type_ID
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
