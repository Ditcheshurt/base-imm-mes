SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 9/8/2016
-- Description:	Gets all check options, or one specific
-- =============================================
CREATE PROCEDURE [dbo].[sp_CHECK_getCheckHistory]
	-- Add the parameters for the stored procedure here
	@check_history_ID int = 0,
	@check_type_ID int = 0,
	@start_date datetime = NULL,
	@end_date datetime = NULL,
	@station_ID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @check_history_ID as pooh, @check_type_ID as pooh2
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
