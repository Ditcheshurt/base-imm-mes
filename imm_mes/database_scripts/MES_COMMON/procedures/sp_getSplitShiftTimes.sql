SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Robert Kahle
-- Create date: 7/24/2017
-- Description:	This grabs the start and end times with the shift that is split between days
-- =============================================
CREATE PROCEDURE [dbo].[sp_getSplitShiftTimes] 
	-- Add the parameters for the stored procedure here
	@shift int output,
	@start int output,
	@end int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	WITH shifty AS (
		SELECT
			 sti.shift_ID
			,ti.interval_start_time
			,sti.is_day_start
		FROM shift_time_intervals sti
		JOIN time_intervals ti
			ON sti.time_interval_ID = ti.ID
	), shifter AS (
		SELECT
			 (SELECT shifty.shift_ID FROM shifty WHERE DATEPART(HOUR, shifty.interval_start_time) = 23) AS [shift]
			,[split_shift_start]
			,[split_shift_end]
		FROM (
			SELECT 'split_shift_start' AS [shift_desc],DATEPART(HOUR,B.interval_start_time) AS [times]
			FROM shifty B
			WHERE DATEPART(HOUR, B.interval_start_time) = 23
			UNION
			SELECT 'split_shift_end' AS [shift_desc], max(DATEPART(HOUR,A.interval_start_time))+1 AS [times]
			FROM shifty A
			WHERE A.shift_ID = (
				SELECT shifty.shift_ID
				FROM shifty
				WHERE DATEPART(HOUR, shifty.interval_start_time) = 23
			) AND DATEPART(HOUR,A.interval_start_time) < 23
		) AS X
		PIVOT
		(
			AVG(times)
			FOR shift_desc
			IN ([split_shift_start],[split_shift_end])
		) AS PivotTable
	)
	SELECT @shift=[shift], @start=[split_shift_start], @end=[split_shift_end] FROM shifter 
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
