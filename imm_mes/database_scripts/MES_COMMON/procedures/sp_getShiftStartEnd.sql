SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 2/26/16
-- Description:	get shift start and end times
-- =============================================
CREATE PROCEDURE [dbo].[sp_getShiftStartEnd]
	@system_ID int,
	@shift_ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @shift_start time(7);
	DECLARE @shift_end time(7);
	DECLARE @tbl TABLE(
		[system_ID] [int],
		[shift_ID] [int],
		[shift_start] time(7) NULL,
		[shift_end] time(7) NULL 
	);	
  
	-- get the shift start time
	SELECT 		
		@shift_start = ti.interval_start_time
	FROM 
		shifts s
	JOIN 
		shift_time_intervals sti ON s.ID = sti.shift_ID
	JOIN 
		time_intervals ti ON ti.ID = sti.time_interval_ID
	WHERE
		s.ID = @shift_ID AND sti.shift_start = 1 AND system_ID = @system_ID;

	-- get the shift end time
	SELECT 		
		@shift_end = ti.interval_end_time
	FROM 
		shifts s
	JOIN 
		shift_time_intervals sti ON s.ID = sti.shift_ID
	JOIN 
		time_intervals ti ON ti.ID = sti.time_interval_ID
	WHERE
		s.ID = @shift_ID AND sti.shift_end = 1 AND system_ID = @system_ID

	INSERT INTO @tbl (system_ID, shift_ID, [shift_start], [shift_end]) VALUES (@system_ID, @shift_ID, @shift_start, @shift_end);

	SELECT TOP 1 * FROM @tbl;

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
