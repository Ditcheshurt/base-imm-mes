SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE VIEW dbo.v_roles
AS
SELECT        ID, name, (CASE WHEN name = 'operator' THEN 0 WHEN name = 'team leader' THEN 1 WHEN name = 'supervisor' THEN 2 WHEN name = 'trainer' THEN 3 WHEN name = 'editor' THEN 4 WHEN name = 'support' THEN 5 ELSE 0 END)
                          AS role_priority
FROM            mes_common.dbo.roles AS roles_1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
