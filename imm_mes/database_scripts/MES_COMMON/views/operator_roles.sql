SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE VIEW dbo.v_operator_roles
AS
SELECT        ID, operator_ID, role_ID, role
FROM            mes_common.dbo.operator_roles AS operator_roles_1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
