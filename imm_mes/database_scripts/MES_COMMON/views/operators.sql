SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
create view dbo.v_operators as

SELECT * FROM mes_common.dbo.operators
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
