CREATE TABLE [dbo].[cards_programs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [program_name] [varchar](50) NOT NULL ,
   [program_group] [varchar](50) NULL ,
   [mgr_ID] [int] NULL   ,
   [process_ID] [int] NULL   ,
   [materials_ID] [int] NULL   ,
   [purchasing_ID] [int] NULL   ,
   [tooling_ID] [int] NULL   ,
   [account_ID] [int] NULL   ,
   [quality_ID] [int] NULL   ,
   [facilities_ID] [int] NULL   ,
   [sup_1] [int] NULL   ,
   [sup_2] [int] NULL   ,
   [sup_3] [int] NULL   ,
   [plant_ID] [int] NULL   ,
   [Active] [bit] NULL   

   ,CONSTRAINT [PK_card_programs] PRIMARY KEY CLUSTERED ([ID])
)


GO
