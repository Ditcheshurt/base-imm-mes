CREATE TABLE [dbo].[ojt_requirements] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_requirement_type_ID] [int] NULL   ,
   [ojt_signoff_template_ID] [int] NULL   ,
   [requirement_description] [varchar](50) NULL ,
   [requirement_text] [varchar](100) NULL ,
   [allow_mes_signoff] [bit] NULL 
      CONSTRAINT [DF_ojt_requirements_allow_mes_signoff] DEFAULT ((0))  ,
   [expire_days] [int] NULL   ,
   [video_url] [varchar](500) NULL ,
   [image_url] [varchar](250) NULL ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_ojt_requirements_active] DEFAULT ((0))  ,
   [created_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_requirements_created_date] DEFAULT (getdate())  ,
   [updated_date] [datetime] NULL   

   ,CONSTRAINT [PK_ojt_requirements] PRIMARY KEY CLUSTERED ([ID])
)


GO
