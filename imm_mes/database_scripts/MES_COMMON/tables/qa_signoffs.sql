CREATE TABLE [dbo].[qa_signoffs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [qa_item_ID] [int] NOT NULL   ,
   [team_leader_ID] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [signoff_time] [datetime] NOT NULL   ,
   [expired_date] [datetime] NULL   
)


GO
