CREATE TABLE [dbo].[ojt_signoff_template_levels] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_signoff_template_ID] [int] NOT NULL   ,
   [level_order] [int] NOT NULL   ,
   [level_description] [varchar](50) NULL ,
   [created_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_signoff_template_levels_created_date] DEFAULT (getdate())  

   ,CONSTRAINT [PK_ojt_signoff_template_levels] PRIMARY KEY CLUSTERED ([ID])
)


GO
