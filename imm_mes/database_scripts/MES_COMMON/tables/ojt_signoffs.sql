CREATE TABLE [dbo].[ojt_signoffs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_requirement_ID] [int] NOT NULL   ,
   [ojt_signoff_template_levels_roles_ID] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [signoff_operator_ID] [int] NOT NULL   ,
   [created_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_signoffs_created_date] DEFAULT (getdate())  ,
   [expire_date] [datetime] NULL   ,
   [expired_by] [int] NULL   

   ,CONSTRAINT [PK_ojt_signoffs] PRIMARY KEY CLUSTERED ([ID])
)


GO
