CREATE TABLE [dbo].[check_history_results] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [check_history_part_ID] [int] NOT NULL   ,
   [result_name] [varchar](100) NULL ,
   [result_numeric_value] [float] NULL   ,
   [result_string_value] [varchar](500) NULL 

   ,CONSTRAINT [PK_check_history_results] PRIMARY KEY CLUSTERED ([ID])
)


GO
