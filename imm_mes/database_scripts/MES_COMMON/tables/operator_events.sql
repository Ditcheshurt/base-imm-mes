CREATE TABLE [dbo].[operator_events] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [event_desc] [varchar](255) NOT NULL 

   ,CONSTRAINT [PK_operator_events] PRIMARY KEY CLUSTERED ([ID])
)


GO
