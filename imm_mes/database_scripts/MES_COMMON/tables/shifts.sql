CREATE TABLE [dbo].[shifts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [system_ID] [int] NULL   ,
   [shift_desc] [varchar](50) NULL ,
   [shift_number] [int] NULL   

   ,CONSTRAINT [PK_shifts] PRIMARY KEY CLUSTERED ([ID])
)


GO
