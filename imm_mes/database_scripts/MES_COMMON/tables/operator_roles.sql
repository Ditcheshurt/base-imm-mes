CREATE TABLE [dbo].[operator_roles] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NOT NULL   ,
   [role_ID] [int] NOT NULL   ,
   [role] AS ([dbo].[fn_PopulateOperatorRolesRoleComputedColumn]([role_ID]))
)


GO
