CREATE TABLE [dbo].[roles] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [name] [varchar](50) NOT NULL ,
   [role_priority] [int] NULL   

   ,CONSTRAINT [PK_roles_ID] PRIMARY KEY CLUSTERED ([ID])
)

CREATE UNIQUE NONCLUSTERED INDEX [IX_unique_name] ON [dbo].[roles] ([name]) INCLUDE ([ID])

GO
