CREATE TABLE [dbo].[check_schedule] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [check_type_ID] [int] NOT NULL   ,
   [station_ID] [int] NOT NULL   ,
   [station_instance] [varchar](50) NULL ,
   [hour_of_day] [float] NOT NULL   ,
   [interval_in_minutes] [int] NOT NULL 
      CONSTRAINT [DF_check_schedule_interval_in_minutes] DEFAULT ((0))  

   ,CONSTRAINT [PK_check_schedule] PRIMARY KEY CLUSTERED ([ID])
)


GO
