CREATE TABLE [dbo].[ojt_groups] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [parent_group_ID] [int] NULL   ,
   [group_description] [varchar](100) NULL ,
   [group_text] [varchar](max) NULL ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_ojt_groups_active] DEFAULT ((0))  ,
   [created_date] [datetime] NULL 
      CONSTRAINT [DF_ojt_groups_created_date] DEFAULT (getdate())  ,
   [updated_date] [datetime] NULL   

   ,CONSTRAINT [PK_ojt_groups] PRIMARY KEY CLUSTERED ([ID])
)


GO
