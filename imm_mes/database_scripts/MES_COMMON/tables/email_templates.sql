CREATE TABLE [dbo].[email_templates] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [application] [varchar](50) NOT NULL ,
   [description] [varchar](100) NOT NULL ,
   [subject_template] [varchar](2000) NULL ,
   [email_template] [varchar](max) NOT NULL ,
   [bcc] [varchar](1000) NULL 

   ,CONSTRAINT [PK_email_templates] PRIMARY KEY CLUSTERED ([ID])
)


GO
