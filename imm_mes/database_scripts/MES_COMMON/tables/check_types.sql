CREATE TABLE [dbo].[check_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [type_desc] [varchar](100) NOT NULL ,
   [import_type] [varchar](50) NOT NULL ,
   [first_part_req_minutes] [float] NOT NULL 
      CONSTRAINT [DF_check_types_first_part_req_minutes] DEFAULT ((0))  ,
   [last_part_max_minutes] [float] NOT NULL 
      CONSTRAINT [DF_check_types_last_part_max_minutes] DEFAULT ((0))  ,
   [no_check_alert_minutes] [float] NOT NULL 
      CONSTRAINT [DF_check_types_no_check_alert_minutes] DEFAULT ((0))  

   ,CONSTRAINT [PK_check_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
