CREATE TABLE [dbo].[common_label_templates] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [template_name] [varchar](200) NOT NULL ,
   [label_template] [varchar](max) NOT NULL 

   ,CONSTRAINT [PK_common_templates] PRIMARY KEY CLUSTERED ([ID])
)


GO
