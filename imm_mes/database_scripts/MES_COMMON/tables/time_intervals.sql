CREATE TABLE [dbo].[time_intervals] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [interval_desc] [varchar](50) NULL ,
   [interval_start_time] [time] NULL   ,
   [interval_end_time] [time] NULL   ,
   [date_overlap] [bit] NOT NULL 
      CONSTRAINT [DF_time_intervals_date_overlap] DEFAULT ((0))  

   ,CONSTRAINT [PK_time_intervals] PRIMARY KEY CLUSTERED ([ID])
)


GO
