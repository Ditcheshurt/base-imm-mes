CREATE TABLE [dbo].[check_history] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [check_type_ID] [int] NOT NULL   ,
   [station_ID] [int] NULL   ,
   [station_instance] [int] NULL   ,
   [history_order] [int] NULL   ,
   [entry_time] [datetime] NULL   ,
   [starting_history_part_ID] [int] NULL   ,
   [ending_history_part_ID] [int] NULL   ,
   [first_part_produced_time] [datetime] NULL   

   ,CONSTRAINT [PK_check_history] PRIMARY KEY CLUSTERED ([ID])
)


GO
