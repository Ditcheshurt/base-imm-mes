CREATE TABLE [dbo].[plants] (
   [ID] [int] NOT NULL   ,
   [plant_name] [varchar](50) NOT NULL ,
   [domain_name] [varchar](50) NULL ,
   [ip_octet3_start] [int] NULL   ,
   [ip_octet3_end] [int] NULL   

   ,CONSTRAINT [PK_plants] PRIMARY KEY CLUSTERED ([ID])
)


GO
