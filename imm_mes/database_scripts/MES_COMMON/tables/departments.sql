CREATE TABLE [dbo].[departments] (
   [ID] [int] NOT NULL   ,
   [dept_name] [varchar](50) NOT NULL ,
   [plant_ID] [int] NULL   ,
   [parent_dept_ID] [int] NULL   ,
   [dept_mgr_ID] [int] NULL   ,
   [dept_email] [varchar](50) NULL ,
   [all_access] [bit] NULL   ,
   [ideas_acct_code] [varchar](5) NULL ,
   [backup_dept_mgr_ID] [int] NULL   ,
   [department_name] AS ([dept_name])

   ,CONSTRAINT [PK_departments] PRIMARY KEY CLUSTERED ([ID])
)


GO
