CREATE TABLE [dbo].[ojt_group_requirements] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_group_ID] [int] NOT NULL   ,
   [ojt_requirement_ID] [int] NOT NULL   ,
   [min_operators] [int] NULL   ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_ojt_group_requirements_active] DEFAULT ((0))  ,
   [created_date] [datetime] NULL 
      CONSTRAINT [DF_ojt_group_requirements_created_date] DEFAULT (getdate())  

   ,CONSTRAINT [PK_ojt_group_requirements] PRIMARY KEY CLUSTERED ([ID])
)


GO
