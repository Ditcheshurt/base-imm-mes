CREATE TABLE [dbo].[system_permissions] (
   [system_ID] [int] NOT NULL   ,
   [operator_ID] [int] NOT NULL   ,
   [option_ID] [int] NOT NULL   ,
   [permission_level] [int] NOT NULL   ,
   [custom_permission] [varchar](100) NULL 

   ,CONSTRAINT [PK_mes_system_permissions_1] PRIMARY KEY CLUSTERED ([system_ID], [operator_ID], [option_ID])
)


GO
