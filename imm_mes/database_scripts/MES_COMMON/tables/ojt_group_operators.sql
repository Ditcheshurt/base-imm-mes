CREATE TABLE [dbo].[ojt_group_operators] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NOT NULL   ,
   [ojt_group_ID] [int] NOT NULL   ,
   [created_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_group_operators_created_date] DEFAULT (getdate())  

   ,CONSTRAINT [PK_ojt_group_operators] PRIMARY KEY CLUSTERED ([ID])
)


GO
