CREATE TABLE [dbo].[shift_time_intervals] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [shift_ID] [int] NOT NULL   ,
   [time_interval_ID] [int] NOT NULL   ,
   [shift_start] [bit] NULL   ,
   [shift_end] [bit] NULL   ,
   [is_day_start] [bit] NULL   

   ,CONSTRAINT [PK_shift_intervals] PRIMARY KEY CLUSTERED ([ID])
)


GO
