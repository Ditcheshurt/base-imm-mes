CREATE TABLE [dbo].[ojt_jobs] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [department_ID] [int] NULL   ,
   [area_ID] [int] NULL   ,
   [job_name] [varchar](200) NOT NULL ,
   [job_details] [varchar](5000) NULL ,
   [active] [bit] NULL   
)


GO
