CREATE TABLE [dbo].[systems] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [system_name] [varchar](50) NOT NULL ,
   [notes] [varchar](5000) NULL ,
   [root_path] [varchar](200) NULL ,
   [mrp_company_code] [varchar](10) NULL ,
   [database_name] [varchar](30) NULL ,
   [process_type] [varchar](50) NULL ,
   [system_active] [bit] NULL   ,
   [has_options] [bit] NULL   ,
   [options_list] [nvarchar](150) NULL ,
   [oem] [varchar](25) NULL ,
   [broadcast_type] [varchar](25) NULL 

   ,CONSTRAINT [PK_mes_systems] PRIMARY KEY CLUSTERED ([ID])
)


GO
