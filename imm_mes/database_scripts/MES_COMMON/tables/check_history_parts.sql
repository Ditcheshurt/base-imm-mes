CREATE TABLE [dbo].[check_history_parts] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [check_history_ID] [int] NOT NULL   ,
   [wip_part_ID] [int] NULL   ,
   [module_ID] [int] NULL   ,
   [machine_cycle_part_ID] [int] NULL   ,
   [test_passed] [bit] NULL   ,
   [test_time] [datetime] NULL   ,
   [bypassed] [bit] NULL   ,
   [supervisor_ID] [int] NULL   

   ,CONSTRAINT [PK_check_history_parts] PRIMARY KEY CLUSTERED ([ID])
)


GO
