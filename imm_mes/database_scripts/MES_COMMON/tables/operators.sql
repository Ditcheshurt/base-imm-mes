CREATE TABLE [dbo].[operators] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [dept_ID] [int] NULL   ,
   [domain_user] [varchar](50) NULL ,
   [email] [varchar](100) NULL ,
   [bb_user] [bit] NULL   ,
   [deactive_date] [datetime] NULL   ,
   [badge_ID] [int] NULL   ,
   [program1] [varchar](50) NULL ,
   [program2] [varchar](50) NULL ,
   [badge_replacement] [int] NULL   ,
   [badge_prefix] [int] NULL   ,
   [attendance_percentage] [float] NULL   ,
   [attendance_updated_at] [datetime] NULL   ,
   [first_name] [varchar](30) NOT NULL ,
   [last_name] [varchar](30) NOT NULL ,
   [password] [varchar](50) NULL ,
   [badgeID] AS ([badge_ID]),
   [shift] [int] NULL   ,
   [name] AS (([last_name]+', ')+[first_name])

   ,CONSTRAINT [PK_operators] PRIMARY KEY CLUSTERED ([ID])
)


GO
