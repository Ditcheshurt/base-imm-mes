CREATE TABLE [dbo].[ojt_requirement_types] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [type_description] [varchar](50) NULL ,
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_ojt_requirement_types_active] DEFAULT ((0))  ,
   [create_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_requirement_types_create_date] DEFAULT (getdate())  ,
   [update_date] [datetime] NULL   ,
   [is_alert] [bit] NULL   

   ,CONSTRAINT [PK_ojt_requirement_types] PRIMARY KEY CLUSTERED ([ID])
)


GO
