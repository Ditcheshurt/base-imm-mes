CREATE TABLE [dbo].[ojt_signoff_template_levels_roles] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_signoff_template_level_ID] [int] NOT NULL   ,
   [role_order] [int] NULL   ,
   [role_ID] [int] NOT NULL   ,
   [created_date] [datetime2] NOT NULL 
      CONSTRAINT [DF_ojt_signoff_level_roles_created_date] DEFAULT (getdate())  

   ,CONSTRAINT [PK_ojt_signoff_level_roles] PRIMARY KEY CLUSTERED ([ID])
)


GO
