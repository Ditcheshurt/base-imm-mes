CREATE TABLE [dbo].[ojt_signoff_templates] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [template_description] [varchar](50) NULL ,
   [created_date] [datetime] NOT NULL 
      CONSTRAINT [DF_ojt_signoff_templates_created_date] DEFAULT (getdate())  ,
   [active] [bit] NULL   

   ,CONSTRAINT [PK_ojt_signoff_templates] PRIMARY KEY CLUSTERED ([ID])
)


GO
