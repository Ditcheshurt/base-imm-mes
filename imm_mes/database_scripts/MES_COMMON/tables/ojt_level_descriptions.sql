CREATE TABLE [dbo].[ojt_level_descriptions] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [ojt_level] [int] NOT NULL   ,
   [dept_ID] [int] NOT NULL   ,
   [level_desc] [varchar](50) NOT NULL 
)


GO
