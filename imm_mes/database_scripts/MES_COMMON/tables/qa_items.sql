CREATE TABLE [dbo].[qa_items] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [item_prefix] [varchar](3) NULL ,
   [qa_area_ID] [int] NOT NULL   ,
   [entered_time] [datetime] NOT NULL   ,
   [entered_by] [int] NOT NULL   ,
   [last_updated] [datetime] NULL   ,
   [last_updated_by] [int] NULL   ,
   [qa_title] [varchar](5000) NOT NULL ,
   [qa_link] [varchar](7000) NOT NULL ,
   [start_date] [datetime] NOT NULL   ,
   [end_date] [datetime] NOT NULL   ,
   [expiring_interval] [int] NOT NULL   ,
   [deleted_time] [datetime] NULL   ,
   [severity] [int] NULL   
)


GO
