CREATE TABLE [dbo].[operator_event_history] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [operator_ID] [int] NOT NULL   ,
   [operator_event_ID] [int] NOT NULL   ,
   [start_time] [datetime] NOT NULL   ,
   [end_time] [datetime] NULL   ,
   [duration] AS (datediff(second,[start_time],[end_time]))

   ,CONSTRAINT [PK_operator_event_history] PRIMARY KEY CLUSTERED ([ID])
)


GO
