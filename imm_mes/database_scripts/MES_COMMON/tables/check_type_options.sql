CREATE TABLE [dbo].[check_type_options] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [check_type_ID] [int] NOT NULL   ,
   [option_name] [varchar](50) NULL ,
   [option_value] [varchar](50) NULL 

   ,CONSTRAINT [PK_check_type_options] PRIMARY KEY CLUSTERED ([ID])
)


GO
