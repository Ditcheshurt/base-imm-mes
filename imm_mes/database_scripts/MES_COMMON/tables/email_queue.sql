CREATE TABLE [dbo].[email_queue] (
   [ID] [int] NOT NULL  
      IDENTITY (1000,1) ,
   [company_ID] [int] NULL   ,
   [application] [varchar](50) NOT NULL ,
   [record_ID] [varchar](50) NULL ,
   [from_addr] [varchar](1000) NOT NULL ,
   [to_addr] [varchar](1000) NOT NULL ,
   [subject] [varchar](2000) NOT NULL ,
   [body] [varchar](max) NOT NULL ,
   [is_html] [bit] NOT NULL   ,
   [queue_time] [datetime] NOT NULL 
      CONSTRAINT [DF_email_queue_queue_time] DEFAULT (getdate())  ,
   [send_time] [datetime] NULL   ,
   [num_retries] [int] NOT NULL 
      CONSTRAINT [DF_email_queue_num_retries] DEFAULT ((0))  ,
   [template_ID] [int] NULL   

   ,CONSTRAINT [PK_email_queue] PRIMARY KEY CLUSTERED ([ID])
)


GO
