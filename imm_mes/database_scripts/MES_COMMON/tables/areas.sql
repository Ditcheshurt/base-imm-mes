CREATE TABLE [dbo].[areas] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [department_ID] [int] NULL   ,
   [area_name] [varchar](50) NOT NULL ,
   [dept_ID] AS ([department_ID]),
   [area_desc] AS ([area_name]),
   [active] [bit] NOT NULL 
      CONSTRAINT [DF_areas_active] DEFAULT ((1))  

   ,CONSTRAINT [PK_areas] PRIMARY KEY CLUSTERED ([ID])
)


GO
