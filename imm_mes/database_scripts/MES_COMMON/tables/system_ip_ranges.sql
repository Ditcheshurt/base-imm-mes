CREATE TABLE [dbo].[system_ip_ranges] (
   [ID] [int] NOT NULL  
      IDENTITY (1,1) ,
   [system_ID] [int] NOT NULL   ,
   [starting_ip] [varchar](50) NOT NULL ,
   [ending_ip] [varchar](50) NULL ,
   [specific_version] [varchar](50) NULL 

   ,CONSTRAINT [PK_system_ip_ranges] PRIMARY KEY CLUSTERED ([ID])
)


GO
