CREATE TABLE [dbo].[ebv_connect_log] (
   [id] [int] NOT NULL  
      IDENTITY (1,1) ,
   [Run_Date_Time] [datetime] NULL   ,
   [Database_Name] [varchar](100) NULL ,
   [Connection_Count] [int] NULL   ,
   [Login_Name] [varchar](100) NULL ,
   [Program_Name] [varchar](100) NULL ,
   [Host_Name] [varchar](100) NULL 

   ,CONSTRAINT [PK_ebv_connect_log] PRIMARY KEY CLUSTERED ([id])
)


GO
