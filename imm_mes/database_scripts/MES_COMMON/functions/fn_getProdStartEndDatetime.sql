SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 4/15/16
-- Description:	Return the start and end times 
--		for a production day
-- =============================================
CREATE FUNCTION [dbo].[fn_getProdStartEndDatetime]
(	
	-- Add the parameters for the function here
	@system_ID int
)
RETURNS @table TABLE (
	day_start datetime NULL, 
    day_end datetime NULL
)
AS
BEGIN
	DECLARE 
		@day_start_shift int, -- which shift signals start of production day
		@shift_count int,
		@day_start datetime,
		@day_end datetime

	SELECT TOP 1		
		@day_start = CONVERT(DATETIME, CONVERT(VARCHAR(13), getDate(), 110) + ' ' + CONVERT(VARCHAR(11), ti.interval_start_time)),
		@day_start_shift = s.ID
	FROM 
		shifts s
		JOIN shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN time_intervals ti ON sti.time_interval_ID = ti.ID
	WHERE 
		s.system_ID = @system_ID
		AND sti.is_day_start = 1
	ORDER BY 
		ti.interval_start_time;

	-- if only 1 shift in the day set @day_start_shift = 0
	SELECT 
		@day_start_shift = CASE WHEN COUNT(DISTINCT s.ID) = 1 THEN  0 ELSE @day_start_shift END
	FROM 
		shifts s
		JOIN shift_time_intervals sti ON sti.shift_ID = s.ID
	WHERE 
		s.system_ID = @system_ID	

	SELECT TOP 1
		@day_end = CONVERT(DATETIME, CONVERT(VARCHAR(13), getDate(), 110) + ' ' + CONVERT(VARCHAR(11), ti.interval_end_time))
	FROM 
		shifts s
		JOIN shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN time_intervals ti ON sti.time_interval_ID = ti.ID
	WHERE 
		s.system_ID = @system_ID
		AND s.ID <> @day_start_shift
	ORDER BY 
		ti.interval_end_time DESC;

	INSERT @table
	SELECT 
		CASE WHEN @day_start > @day_end THEN DATEADD(DD, -1, @day_start) ELSE @day_start END
		,@day_end

RETURN; 
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
