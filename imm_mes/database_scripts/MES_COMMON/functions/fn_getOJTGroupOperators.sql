SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupOperators]
(
	@ojt_group_ID int
)
RETURNS 
@result TABLE 
(
	ojt_group_operator_ID int,
	operator_ID int,
	badge_ID varchar(20),
	first_name varchar(20),
	last_name varchar(20),
	is_inherited bit,	
	ojt_group_ID int,
	group_description varchar(100)
)
AS
BEGIN
	-- get group hirearchy
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
			FROM ojt_groups
			WHERE ID = @ojt_group_ID
			UNION ALL
			SELECT og.ID, og.parent_group_ID, og.group_description
			FROM ojt_groups og
			JOIN parents ON og.ID = parents.parent_group_ID
	)
	-- work up the hierarchy
	--WITH parents
	--AS (
	--	SELECT ID, parent_group_ID, group_description
	--	FROM ojt_groups
	--	WHERE parent_group_ID = @ojt_group_ID
	--	UNION ALL
	--	SELECT og.ID, og.parent_group_ID, og.group_description
	--	FROM ojt_groups og
	--	INNER JOIN parents ON og.parent_group_ID = parents.ID
	--)
	INSERT INTO @result
	SELECT 
		gop.ID AS ojt_group_operator_ID,
		o.ID AS operator_ID,
		o.badge_ID,
		o.first_name,
		o.last_name,
		CASE WHEN gop.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		--gop.active,
		g.ID AS ojt_group_ID,
		g.group_description
	FROM
		ojt_group_operators gop
		JOIN operators o ON gop.operator_ID = o.ID
		JOIN ojt_groups g ON gop.ojt_group_ID = g.ID
	WHERE 
		ojt_group_ID IN (SELECT ID FROM parents) OR ojt_group_ID = @ojt_group_ID
	ORDER BY o.last_name
	
	RETURN 
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
