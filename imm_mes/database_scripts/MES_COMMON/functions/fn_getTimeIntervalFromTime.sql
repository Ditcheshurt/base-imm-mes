SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/6/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getTimeIntervalFromTime] 
(
	-- Add the parameters for the function here
	@system_ID int,
	@dt datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res int = 0

	SELECT 
		@res = ti.ID 
	FROM 
		time_intervals ti
		JOIN shift_time_intervals sti ON sti.time_interval_ID = ti.ID
		JOIN shifts s ON sti.shift_ID = s.ID
	WHERE 
		s.system_ID = @system_ID 
		AND CONVERT(time, @dt) BETWEEN interval_start_time AND interval_end_time
		/*AND (
			CASE WHEN DATEDIFF(second,ti.interval_start_time,ti.interval_end_time) > 0 THEN
				CASE WHEN (CONVERT(time,@dt) > ti.interval_start_time) AND (CONVERT(time,@dt) < ti.interval_end_time) THEN 1 ELSE 0 END
			ELSE
				CASE WHEN (CONVERT(time,@dt) > ti.interval_start_time) OR (CONVERT(time,@dt) < ti.interval_end_time) THEN 1 ELSE 0 END
			END
		) > 0
		*/


	--HANDLE MIDNIGHT
	IF @res = 0
		SELECT 
			@res = ti.ID 
		FROM time_intervals ti
			JOIN shift_time_intervals sti ON sti.time_interval_ID = ti.ID
			JOIN shifts s ON sti.shift_ID = s.ID
		WHERE 
			s.system_ID = @system_ID 
			AND date_overlap = 1

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
