SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

--SELECT DATEDIFF(SECOND, '4/6/2016 11:46', GETDATE()) / 60.0


--SELECT dbo.fn_getPriorIntervalFromTime('4/6/2016 23:36')

--SELECT dbo.fn_getIntervalStartTime('4/6/2016 01:45')

CREATE FUNCTION [dbo].[fn_getTimeIntervalDuration] 
(
	@system_ID int
)
RETURNS float
AS
	BEGIN
		DECLARE @res float = 0.0

		SELECT @res = ROUND(AVG(DATEDIFF(SECOND, interval_start_time, interval_end_time)) / 60.0, 0, 0)
		FROM time_intervals ti
		JOIN shift_time_intervals sti ON sti.time_interval_ID = ti.ID
		JOIN shifts s ON sti.shift_ID = s.ID
		WHERE s.system_ID = @system_ID AND date_overlap = 0

		RETURN @res
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
