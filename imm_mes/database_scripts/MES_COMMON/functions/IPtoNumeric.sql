SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE FUNCTION [dbo].[IPtoNumeric] (@strIP varchar(255)) 
	RETURNS bigint 
AS 
BEGIN 
	DECLARE @intIPNum bigint 
	SET @intIPNum = 0
	
	IF (LEN(@strIP)-LEN(REPLACE(@strIP,'.','')))/LEN('.') = 3 -- check to ensure there are 3 dots 
		SET @intIPNum = (16777216 * CAST(PARSENAME(@strIP,4) as bigint) + 65536 * PARSENAME(@strIP,3) + 256 * PARSENAME(@strIP,2) + PARSENAME(@strIP,1)) 
	RETURN @intIPNum 
END 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
