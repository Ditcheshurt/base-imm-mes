SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
-- =============================================
-- Author:		Rob Kahle
-- Create date: 3/19/2018
-- Description:	This will get a modbus value
-- =============================================
CREATE FUNCTION fn_getMBValue 
(
	-- Add the parameters for the function here
	@web_server varchar(50) = 'localhost',
	@plc_IP varchar(20),
	@mb_addr varchar(20),
	@plc_path varchar(1) = '0'
)
RETURNS int
AS
BEGIN
--http://10.68.160.79/plcservice/plcwebreadwrite.asmx/getMBValue?plc_ip=10.49.105.104&plc_path=0&file_addr=464611&debug_mode=0&callback=
	-- Declare the return variable here
	DECLARE @result varchar(MAX)
	DECLARE @uri varchar(2000)
	SET @result = 'ERROR'
	SET @uri = 'http://' + @web_server + '/PLCService/PLCWebReadWrite.asmx/getMBValue?plc_ip=' + @plc_IP + '&file_addr=' + @mb_addr + '&plc_path=' + @plc_path + '&debug_mode=0&callback='

	-- Add the T-SQL statements to compute the return value here
	SET @result = [dbo].[fn_CLR_GET] (
		@uri
		,''
		,'')

	SET @result = dbo.fn_getJSONStringValue(@result, 'value')

	-- Return the result of the function
	RETURN @result

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
