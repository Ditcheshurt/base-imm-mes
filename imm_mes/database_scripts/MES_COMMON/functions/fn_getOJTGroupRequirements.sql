SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupRequirements]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS 
@result TABLE 
(
	ojt_requirement_ID int,
	ojt_signoff_template_ID int,
	type_description varchar(100),
	is_alert bit,
	requirement_description varchar(100),
	template_description varchar(100),
	requirement_text varchar(100),		
	image_url varchar(100),
	created_date datetime,
	updated_date datetime,
	requirement_active bit,
	is_inherited bit,
	ojt_group_requirement_ID int,
	ojt_group_ID int,
	group_description varchar(100),
	group_active bit,
	min_operators int
)
AS
BEGIN
	
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups
		WHERE ID = @ojt_group_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents ON og.ID = parents.parent_group_ID		
	)
	INSERT INTO @result
	SELECT 
		r.ID AS ojt_requirement_ID,
		r.ojt_signoff_template_ID,
		rt.type_description,
		rt.is_alert,
		r.requirement_description,
		st.template_description,
		r.requirement_text,		
		r.image_url,
		r.created_date,
		r.updated_date,
		r.active AS requirement_active,
		CASE WHEN gr.ojt_group_ID = @ojt_group_ID THEN 0 ELSE 1 END AS is_inherited,
		gr.ID AS ojt_group_requirement_ID,
		g.ID AS ojt_group_ID,
		g.group_description,
		g.active AS group_active,
		gr.min_operators
	FROM
		ojt_group_requirements gr
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_groups g ON gr.ojt_group_ID = g.ID
		JOIN ojt_signoff_templates st ON r.ojt_signoff_template_ID = st.ID
	WHERE 
		ojt_group_ID IN (SELECT ID FROM parents)
		AND (rt.is_alert = 0 OR @with_alerts = 1)
	
	RETURN 
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
