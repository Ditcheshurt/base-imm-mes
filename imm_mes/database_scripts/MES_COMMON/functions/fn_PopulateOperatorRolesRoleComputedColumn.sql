SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Lazette, Chad
-- Create date: 10 Feb 2016
-- Description:	Used to populate operator_roles.role column to avoid collateral 
-- damage after creating a roles table for a true many-to-many relationship
-- between operators and roles.
-- =============================================
CREATE FUNCTION fn_PopulateOperatorRolesRoleComputedColumn( @roleId INT )
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @roleName VARCHAR(50)

	SELECT @roleName = name FROM dbo.roles WHERE ID = @roleId

	RETURN @roleName

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
