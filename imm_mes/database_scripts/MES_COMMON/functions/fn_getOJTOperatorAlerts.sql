SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/20/16
-- Description:	get operator requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorAlerts]
(
	@operator_ID int
)
RETURNS 
@results TABLE 
(
	ojt_requirement_ID int,
	requirement_description varchar(100),
	ojt_signoff_template_ID int,
	template_description varchar(100),
	group_active bit,
	--gr.active AS group_requirement_active,
	image_url varchar(200),
	requirement_active bit,
	requirement_text varchar(100)
)
AS
BEGIN
	
	WITH parents
	AS (
		SELECT g.ID, g.parent_group_ID, g.group_description
		FROM ojt_groups	g
		JOIN ojt_group_operators o ON g.ID = o.ojt_group_ID
		WHERE parent_group_ID IS NULL 
			AND o.operator_ID = @operator_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents p ON og.parent_group_ID = p.ID		
	)
	--select * from parents

	INSERT INTO @results
    SELECT DISTINCT
		r.ID AS ojt_requirement_ID,
		r.requirement_description,
		r.ojt_signoff_template_ID,
		t.template_description,
		g.active AS group_active,
		--gr.active AS group_requirement_active,
		r.image_url,
		r.active AS requirement_active,
		r.requirement_text
	FROM
		ojt_groups g
		JOIN ojt_group_requirements gr ON gr.ojt_group_ID = g.ID
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_signoff_templates t ON r.ojt_signoff_template_ID = t.ID				
	WHERE
		g.ID In (SELECT DISTINCT ID FROM parents)
		AND rt.type_description LIKE '%Alert%'		
	ORDER BY
		r.requirement_description
	
	RETURN 
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
