SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


-- =============================================
-- Author:		Steve Daviduk
-- Create date: 4/13/16
-- Description:	get shift from the shifts
-- =============================================
CREATE FUNCTION [dbo].[fn_getShift]
(
	@system_ID int,
	@the_date datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @shift_ID int

	-- select the value
	SELECT 
		@shift_ID = s.ID
	FROM
		shifts s
		JOIN shift_time_intervals sti ON sti.shift_ID = s.ID
		JOIN time_intervals t ON sti.time_interval_ID = t.ID
	WHERE
		s.system_ID = @system_ID
		AND
			CONVERT(time, @the_date) BETWEEN t.interval_start_time AND t.interval_end_time

	-- Return the result of the function
	RETURN @shift_ID

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
