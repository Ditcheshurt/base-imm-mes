SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/6/2016
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getTimeIntervalStartTime] 
(
	-- Add the parameters for the function here
	@system_ID int,
	@dt datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res datetime
	DECLARE @time_val time

	SET @time_val = CAST(@dt as time)

	SELECT @res = DATEADD(SECOND, DATEDIFF(SECOND, @time_val, interval_start_time), @dt)
	FROM time_intervals ti
	JOIN shift_time_intervals sti ON sti.time_interval_ID = ti.ID
	JOIN shifts s ON sti.shift_ID = s.ID
	WHERE s.system_ID = @system_ID AND @time_val > interval_start_time AND @time_val < interval_end_time

	--HANDLE MIDNIGHT
	IF @res IS NULL
		SELECT @res = CASE WHEN @time_val >= '00:00:00' THEN
				DATEADD(SECOND, 0 - DATEDIFF(SECOND, interval_start_time, '23:59:59.999') - DATEDIFF(SECOND, '00:00:00', @time_val) - 1 , @dt)
			ELSE
				DATEADD(SECOND, DATEDIFF(SECOND, @time_val, interval_start_time), @dt)  --SUBTRACT FROM CURRENT TIME
			END
		FROM time_intervals ti
		JOIN shift_time_intervals sti ON sti.time_interval_ID = ti.ID
		JOIN shifts s ON sti.shift_ID = s.ID
		WHERE s.system_ID = @system_ID AND date_overlap = 1

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
