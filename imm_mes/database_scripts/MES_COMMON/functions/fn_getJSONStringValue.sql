SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- =============================================
-- Author:		Steve Sass
-- Create date: 7/20/2013
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getJSONStringValue] 
(
	-- Add the parameters for the function here
	@JSON_string varchar(2000),
	@param_name varchar(200)
)
RETURNS varchar(200)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(200)

	IF @JSON_string IS NULL
		SET @JSON_string = '{}'

	IF @JSON_string = '' 
		SET @JSON_string = '{}'

	SET @Result = ''
	SELECT @Result = StringValue 
	FROM dbo.parseJSON(@JSON_string)
	WHERE name = @param_name

	-- Return the result of the function
	RETURN @Result

END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
