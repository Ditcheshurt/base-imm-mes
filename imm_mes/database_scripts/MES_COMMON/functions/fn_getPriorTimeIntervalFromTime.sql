SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 4/6/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_getPriorTimeIntervalFromTime] 
(
	-- Add the parameters for the function here
	@system_ID int,
	@dt datetime
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @res int = 0, @time_val time
	DECLARE @duration float

	SET @time_val = CAST(dbo.fn_getTimeIntervalStartTime(@system_ID, @dt) as time)
	SET @duration = dbo.fn_getTimeIntervalDuration(@system_ID)

	SET @res = dbo.fn_getTimeIntervalFromTime(@system_ID, DATEADD(MINUTE, 0 - @duration, @dt))

	-- Return the result of the function
	RETURN @res

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
