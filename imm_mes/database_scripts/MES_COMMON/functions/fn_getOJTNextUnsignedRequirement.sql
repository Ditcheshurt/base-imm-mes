SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



-- ==================================================
-- Author:		Steve Daviduk
-- Create date: 1/2/17
-- Description:	get operators next unsigned requirement
-- ==================================================
CREATE FUNCTION [dbo].[fn_getOJTNextUnsignedRequirement]
(
	@ojt_group_ID int,
	@operator_ID int,
	@requirement_type_description varchar(25)
)
RETURNS 
@result TABLE 
(
	ojt_requirement_ID int,
	ojt_group_requirement_ID int,
	ojt_signoff_template_ID int,
	requirement_description varchar(100),
	requirement_text varchar(100),		
	image_url varchar(250),
	video_url varchar(250),
	ojt_signoff_template_levels_roles_ID int,
	role_ID int,
	role_name varchar(100)
)
AS
BEGIN
	
	WITH parents
	AS (
		SELECT ID, parent_group_ID, group_description
		FROM ojt_groups
		WHERE ID = @ojt_group_ID
		UNION ALL
		SELECT og.ID, og.parent_group_ID, og.group_description
		FROM ojt_groups og
		JOIN parents ON og.ID = parents.parent_group_ID		
	)
	INSERT INTO @result
	SELECT 
		TOP 1
		r.ID,
		gr.ID,
		r.ojt_signoff_template_ID,
		r.requirement_description,
		r.requirement_text,		
		r.image_url,
		r.video_url,
		stlr.ID AS ojt_signoff_template_levels_roles_ID,
		stlr.role_ID,
		rle.name
	FROM
		ojt_group_requirements gr
		JOIN ojt_requirements r ON gr.ojt_requirement_ID = r.ID
		JOIN ojt_requirement_types rt ON r.ojt_requirement_type_ID = rt.ID
		JOIN ojt_groups g ON gr.ojt_group_ID = g.ID
		JOIN ojt_signoff_templates st ON r.ojt_signoff_template_ID = st.ID
		JOIN ojt_signoff_template_levels stl ON st.ID = stl.ojt_signoff_template_ID
		JOIN ojt_signoff_template_levels_roles stlr ON stl.ID = stlr.ojt_signoff_template_level_ID
		JOIN roles rle ON rle.ID = stlr.role_ID
		--LEFT JOIN ojt_signoffs s ON s.ojt_requirement_ID = r.ID AND s.operator_ID = @operator_ID
	WHERE
		ojt_group_ID IN (SELECT ID FROM parents)
		AND rt.type_description = @requirement_type_description
		AND r.active = 1
		AND stlr.ID NOT IN (
			SELECT ojt_signoff_template_levels_roles_ID 
			FROM ojt_signoffs
			WHERE ojt_requirement_ID = r.ID 
				AND ojt_signoff_template_levels_roles_ID = stlr.ID 
				AND operator_ID = @operator_ID
				AND (expire_date IS NULL OR expire_date >= getDate()))
	RETURN 
END






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
