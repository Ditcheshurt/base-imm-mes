SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get operators highest signoff requirement
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTOperatorRequirementSignoffLevel]
(
	@ojt_operator_ID int,
	@ojt_requirement_ID int
)
RETURNS 
@result TABLE 
(
	ojt_signoff_ID int,
	ojt_requirement_ID int,
	ojt_signoff_template_levels_roles_ID int,
	operator_ID int,
	signoff_operator_ID int,
	created_date datetime,
	expire_date datetime,
	expired_by datetime
)
AS
BEGIN

	-- Declares
	DECLARE 
		-- loopy value
		@level_role_ID int,	
		@max_ojt_signoff_template_levels_role_ID int, -- holds the highest level role id	
		-- hold last signoff
		@last_ojt_signoff_ID int,
		@last_ojt_requirement_ID int,
		@last_ojt_signoff_template_levels_roles_ID int,
		@last_operator_ID int,
		@last_signoff_operator_ID int,
		@last_created_date datetime,
		@last_expire_date datetime,
		@last_expired_by datetime

	-- get ojt_signoff_template_levels_roles_ID and loop to make sure it has a signoff
	-- if no signoff is found break out and return the prior record as highest signoff
	DECLARE loopy224 CURSOR FOR

		SELECT
			lr.ID,
			x.max_ojt_signoff_template_levels_role_ID
		FROM
			ojt_requirements req
			JOIN ojt_signoff_templates t ON req.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels l ON l.ojt_signoff_template_ID = t.ID
			JOIN ojt_signoff_template_levels_roles lr ON lr.ojt_signoff_template_level_ID = l.ID	
			CROSS APPLY (
				SELECT TOP 1 
					slr.ID max_ojt_signoff_template_levels_role_ID
					--slr.role_order AS max_role_order 
				FROM
					ojt_signoff_template_levels_roles slr
				WHERE
					slr.ojt_signoff_template_level_ID = l.ID 
				ORDER BY
					slr.role_order desc) x		
		WHERE
			req.ID = @ojt_requirement_ID			
		ORDER BY 
			l.level_order asc, lr.role_order asc

	OPEN loopy224

		FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID
		WHILE @@FETCH_STATUS = 0
		
		BEGIN
			DECLARE 
				-- hold current signoff	
				@ojt_signoff_ID int = null,
				@ojt_requirement_ID2 int = null,
				@ojt_signoff_template_levels_roles_ID int = null,
				@operator_ID int = null,
				@signoff_operator_ID int = null,
				@created_date datetime = null,
				@expire_date datetime = null,
				@expired_by datetime = null
		
			SELECT
				@ojt_signoff_ID = so.ID,
				@ojt_requirement_ID2 = so.ojt_requirement_ID,
				@ojt_signoff_template_levels_roles_ID = so.ojt_signoff_template_levels_roles_ID,
				@operator_ID = so.operator_ID,
				@signoff_operator_ID = so.signoff_operator_ID,
				@created_date = so.created_date,
				@expire_date = so.expire_date,
				@expired_by = so.expired_by				
			FROM				
				ojt_signoffs so								
			WHERE
				so.ojt_requirement_ID = @ojt_requirement_ID
				AND so.operator_ID = @ojt_operator_ID
				AND so.ojt_signoff_template_levels_roles_ID = @level_role_ID
				AND (so.expire_date IS NULL OR getDate() <= so.expire_date)

			IF @ojt_signoff_ID IS NULL	
				BEGIN		
					--INSERT INTO @result
					--SELECT
					--	@last_ojt_signoff_ID,
					--	@last_ojt_requirement_ID,
					--	@last_ojt_signoff_template_levels_roles_ID,
					--	@last_operator_ID,
					--	@last_signoff_operator_ID,
					--	@last_created_date,
					--	@last_expire_date,
					--	@last_expired_by
					BREAK
				END
			ELSE
				BEGIN			
					SET	@last_ojt_signoff_ID = @ojt_signoff_ID
					SET @last_ojt_requirement_ID = @ojt_requirement_ID2				
					SET	@last_operator_ID = @operator_ID
					SET	@last_signoff_operator_ID = @signoff_operator_ID
					SET	@last_created_date = @created_date
					SET	@last_expire_date = @expire_date
					SET	@last_expired_by = @expired_by
					IF @max_ojt_signoff_template_levels_role_ID = @ojt_signoff_template_levels_roles_ID
						SET	@last_ojt_signoff_template_levels_roles_ID = @ojt_signoff_template_levels_roles_ID
				
				END
			
			FETCH NEXT FROM loopy224 INTO @level_role_ID, @max_ojt_signoff_template_levels_role_ID

		END		

	CLOSE loopy224;
	DEALLOCATE loopy224; 	

	INSERT INTO @result
	SELECT
		@last_ojt_signoff_ID,
		@last_ojt_requirement_ID,
		@last_ojt_signoff_template_levels_roles_ID,
		@last_operator_ID,
		@last_signoff_operator_ID,
		@last_created_date,
		@last_expire_date,
		@last_expired_by

	
	RETURN 
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
