SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Daviduk
-- Create date: 7/15/16
-- Description:	get group operators
-- =============================================
CREATE FUNCTION [dbo].[fn_getOJTGroupSummary]
(
	@ojt_group_ID int,
	@with_alerts bit
)
RETURNS 
@result TABLE 
(
	operator_ID int,
	operator_badge varchar(10),
	operator_first_name varchar(50),
	operator_last_name varchar(50),
	signoff_template_ID int,
	signoff_template_description varchar(100),
	ojt_requirement_ID int,
	ojt_requirement_description varchar(100),
	signoff_ID int,
	signoff_level_role_ID int,
	signoff_level_role_description varchar(100),
	signoff_level_description varchar(50),
	signoff_operator varchar(100),
	signoff_date datetime,
	signoff_expires datetime
)
AS
BEGIN
	
	INSERT INTO @result
	SELECT DISTINCT
		o.operator_ID AS operator_ID,
		o.badge_ID AS operator_badge,
		o.first_name AS operator_first_name,
		o.last_name AS operator_last_name,
		t.ID AS signoff_template_ID,
		t.template_description AS signoff_template_description,
		r.ojt_requirement_ID AS ojt_requirement_ID,
		r.requirement_description AS ojt_requirement_description,
		s.ojt_signoff_ID,
		s.ojt_signoff_template_levels_roles_ID AS signoff_level_role_ID,
		rl.name AS signoff_level_role_description,
		l.level_description AS signoff_level_description,
		CAST(o2.badge_ID AS varchar) + ': ' + o2.last_name + ', ' + o2.first_name AS signnoff_operator, 
		s.created_date AS signoff_date,
		s.expire_date AS signoff_expires
	FROM
		dbo.fn_getOJTGroupRequirements(@ojt_group_ID, @with_alerts) r
		CROSS APPLY dbo.fn_getOJTGroupOperators(@ojt_group_ID) o
		CROSS APPLY dbo.fn_getOJTOperatorRequirementSignoffLevel(o.operator_ID, r.ojt_requirement_ID) s 
		JOIN ojt_signoff_templates t ON t.ID = r.ojt_signoff_template_ID
		LEFT JOIN ojt_signoff_template_levels_roles lr ON lr.ID = s.ojt_signoff_template_levels_roles_ID
		LEFT JOIN roles rl ON rl.ID = lr.role_ID
		LEFT JOIN ojt_signoff_template_levels l ON l.ID = lr.ojt_signoff_template_level_ID
		LEFT JOIN operators o2 ON o2.ID = s.signoff_operator_ID
	ORDER BY operator_ID
	
	RETURN 
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
