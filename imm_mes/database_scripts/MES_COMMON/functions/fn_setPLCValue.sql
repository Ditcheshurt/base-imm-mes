SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

-- =============================================
-- Author:		Steve Sass
-- Create date: 9/16/2015
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fn_setPLCValue] 
(
	@web_server varchar(50) = 'localhost',
	@plc_IP varchar(20),
	@tag_name varchar(255),
	@plc_path varchar(1) = '0',
	@data_type varchar(10),
	@new_value varchar(30)
)
RETURNS varchar(500)
AS
BEGIN
	
	DECLARE @result varchar(MAX)
	DECLARE @uri varchar(2000)
	SET @result = 'ERROR'
	SET @uri = 'http://' + @web_server + '/PLCService/PLCWebReadWrite.asmx/setValueAsJSON?plc_ip=' + @plc_IP + '&file_addr=' + @tag_name + '&plc_path=' + @plc_path + '&data_type=' + @data_type + '&new_value=' + @new_value

	SET @result = [dbo].[fn_CLR_GET] (
	   @uri
	  ,''
	  ,'')


	SET @result = CONCAT(dbo.fn_getJSONStringValue(@result, 'ErrorCode'), ' - ', dbo.fn_getJSONStringValue(@result, 'ErrorString'))

	RETURN @result

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

GO
