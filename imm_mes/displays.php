<?php
	ini_set("display_errors", true);
	error_reporting(E_ALL);

	require('load_config.php');
	require('load_template_engine.php');

	$smarty->assign('globals', $GLOBALS['APP_CONFIG']);  //assign all variables
	$smarty->assign('strings', $GLOBALS['STRINGS']);

	if (!isset($_SESSION['user_ID']) && false) {
		$smarty->display('./templates/bad_session.html');
	} else {
		//load page based on type
		switch($_REQUEST['type']) {
			case "production_dashboard":
				$smarty->assign('page_styles', 'public/css/production_dashboard.css');
				$smarty->assign('page_script', './public/scripts/custom/litchfield/production_dashboard.js');
				$smarty->assign('page_title', 'Machine Production');

				if (isset($_REQUEST['machine_group_id'])) {
					$smarty->assign('machine_group_id', $_REQUEST['machine_group_id']);
				} else {
					$smarty->assign('machine_group_id', '');
				}
				
				$smarty->display('./templates/custom/litchfield/production_dashboard.html');
				break;
			default:
				break;
		}
	}


?>