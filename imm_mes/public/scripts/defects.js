//handles all defect stuff
var cur_defect_part = {};
var cur_defect_disp = '';
var cur_defect_type = 0;
var cur_defect_origin = 0;
var cur_defect_types = [];
var cur_defect_origins = [];
var cur_defect_chain = '';
var cur_defect_origin_chain = '';
var cur_defect_locs = [];
var cur_defect_serial = '';
var cur_defect_machine_cycle_part_ID = 0;

var defect_options = {"scan_serial":false, "only_scrap":false, "single_origin_ID":1};

$(function () {

	$('#div_defect_part').collapse('hide');
	$('#div_defect_disposition').collapse('hide');
	$('#div_defect_type').collapse('hide');
	$('#div_defect_origin').collapse('hide');
	$('#div_defect_location').collapse('hide');


	//$('.btn-defect').on('click', function() {
	$('#tbl_label TBODY').on('click', '.btn-record-reject', function() {
			var d = $(this).data();
			$(this).addClass('disabled');
			defect_options.scan_serial = false;
			recordRejectedPart(d);
		}
	);

	$('#tbl_label tbody').on('click','.btn-remove-rack-part', function(){
		var d = $(this).data();
		defect_options.scan_serial = true;
		removePartFromRack(d);
	})

	function recordRejectedPart(d) {
		$('#div_defect_part DIV').html('');
		$('#sp_selected_part').html('');
		cur_defect_machine_cycle_part_ID = 0;
		cur_defect_serial = '';

		var has_serialized_parts = defect_options.scan_serial;
		var queue_parts = []; //this keeps a list of only the parts we're interested in
		var pq_id = 0, lpc_id = 0; //pq_id is the

		for (var i = 0; i < current_tool.part_queue.length; i++) {
			if (current_tool.part_queue[i].part_ID == d.ID) {
				// we're only interested in the parts for the part queue the button was clicked for
				queue_parts.push(current_tool.part_queue[i]);
				if (lpc_id == 0) {
					lpc_id = current_tool.part_queue[i].cycle_ID;
					pq_id = queue_parts.findIndex(x => x.cycle_ID == lpc_id);
				}
				if (current_tool.part_queue[i].cycle_ID < lpc_id) {
					pq_id = queue_parts.findIndex(x => x.cycle_ID == lpc_id);
				}
				lpc_id = current_tool.part_queue[i].cycle_ID;
			}
		}
//		//allow selecting (click or scan) of parts from the queue
//		if (queue_parts.length > 1 || has_serialized_parts) {
//			for (var i = 0; i < queue_parts.length; i++) {
//					var b = $('<BUTTON />');
//					b.addClass('btn btn-primary btn-md defect-select-part-button');
//					b.data(queue_parts[i]);
//					b.html(queue_parts[i].part_desc + '<br>' + queue_parts[i].serial_number);
//					$('#div_defect_part DIV.panel-body').append(b);
//			}
//
//			if (defect_options.scan_serial) {
//				var d = $('<DIV />').html(' - OR - ').css('margin', '10px 0px');
//				$('#div_defect_part DIV.panel-body').append(d);
//				var d = $('<DIV />').html('SCAN SERIAL: <span id="sp_scan_defect_serial" style="font-weight:bold;"></span>').addClass('alert alert-info');
//				$('#div_defect_part DIV.panel-body').append(d);
//
//				KEYS.registerKeyHandler({"name":"scan_serial", "global":false, "match_keys":null, "callback":scanDefectSerial, "display_el_id":"sp_scan_defect_serial"});
//
//				var d = $('<DIV />').html('').addClass('alert alert-danger').hide().attr('id', 'div_serial_scan_result');
//				$('#div_defect_part DIV.panel-body').append(d);
//			}
//
//			$('#div_defect_part').collapse('show');
//			$('#div_defect_disposition').collapse('hide');
//		} else {
//			// only one part exists for this part queue
//			cur_defect_part = queue_parts[0];
//			$('#sp_selected_part').html('<b>(' + cur_defect_part.part_desc + ' - ' + cur_defect_part.serial_number + ')</b>');
//			$('#div_defect_part').collapse('hide');
//			$('#div_defect_disposition').collapse('show');
//			$('#btn_back_to_part').hide();
//		}

		// get the next part from the queue
		cur_defect_part = queue_parts[pq_id];
		cur_defect_serial = cur_defect_part.serial_number;
		$('#sp_selected_part').html('<b>(' + cur_defect_part.part_desc + ' - ' + cur_defect_part.serial_number + ')</b>');
		$('#div_defect_part').collapse('hide');
		if (!defect_options.only_scrap) {
			$('#div_defect_disposition').collapse('show');
			$('#div_defect_type').collapse('hide');
		} else {
			$('#div_defect_disposition').collapse('hide');
			$('#div_defect_type').collapse('show');
		}
		$('#btn_back_to_part').hide();

		$('#div_defect_location').collapse('hide');
		$('#btn_back_to_disp').hide();
		$('#btn_back_to_type').hide();
		$('#btn_back_to_origin').hide();
		$('#div_defect_modal').modal('show');

		if (defect_options.only_scrap) {
			$('#div_defect_disposition').collapse('hide');
			selectDisposition('SCRAP');
		}
	}

	function removePartFromRack(d) {
		// if we're removing parts from a rack
		// need to scan or select them by serial number
		$('#div_defect_part DIV').html('');
		$('#sp_selected_part').html('');
		cur_defect_machine_cycle_part_ID = 0;
		cur_defect_serial = '';

		var has_serialized_parts = defect_options.scan_serial;
		var rack_parts = []; //this keeps a list of only the parts we're interested in
		var pq_id = 0, lpc_id = 0; //pq_id is the

		//populate rack_parts here with AJAX call to production.php?action=get_open_rack_parts&machine_ID={{current_tool.machine_ID[0]}}&rack_ID={{d.container_ID}}&part_ID={{d.part_ID}}
		$.getJSON(ajax_procs.production, {
				"action":"get_open_rack_parts",
				"machine_ID":all_data.machine[0].ID,
				"rack_ID":d.container_ID,
				"part_ID":d.ID
			},
			function(jso) {
				if(jso) {
					if (jso.success == 1) {
						rack_parts = jso.parts;
						//allow selecting (click or scan) of part from the rack
						if (rack_parts.length > 1 || has_serialized_parts) {
							for (var i = 0; i < rack_parts.length; i++) {
									var b = $('<BUTTON />');
									b.addClass('btn btn-primary btn-md defect-select-part-button');
									b.data(rack_parts[i]);
									//b.html(rack_parts[i].part_desc + '<br>' + rack_parts[i].barcode + '<br>' + rack_parts[i].serial_number);
									b.html(rack_parts[i].part_desc + '<br>' + rack_parts[i].serial_number);
									$('#div_defect_part DIV.panel-body').append(b);
							}

							if (defect_options.scan_serial) {
								var d = $('<DIV />').html(' - OR - ').css('margin', '10px 0px');
								$('#div_defect_part DIV.panel-body').append(d);
								var d = $('<DIV />').html('SCAN SERIAL: <span id="sp_scan_defect_serial" style="font-weight:bold;"></span>').addClass('alert alert-info');
								$('#div_defect_part DIV.panel-body').append(d);

								KEYS.registerKeyHandler({"name":"scan_serial", "global":false, "match_keys":null, "callback":scanDefectSerial, "display_el_id":"sp_scan_defect_serial"});

								var d = $('<DIV />').html('').addClass('alert alert-danger').hide().attr('id', 'div_serial_scan_result');
								$('#div_defect_part DIV.panel-body').append(d);
							}

							$('#div_defect_part').collapse('show');
							$('#div_defect_disposition').collapse('hide');
						} else {
							// only one part exists for this part queue
							cur_defect_part = rack_parts[0];
							cur_defect_serial = cur_defect_part.barcode;
							$('#sp_selected_part').html('<b>(' + cur_defect_part.part_desc + ': ' + cur_defect_serial + ')</b>');
							$('#div_defect_part').collapse('hide');
							if (defect_options.only_scrap) {
								$('#div_defect_disposition').collapse('hide');
								selectDisposition('SCRAP');
							} else {
								$('#div_defect_disposition').collapse('show');
							}
							$('#btn_back_to_part').hide();

							if (defect_options.only_scrap) {
								$('#div_defect_disposition').collapse('hide');
								selectDisposition('SCRAP');
							}
						}

						$('#div_defect_type').collapse('hide');
						$('#div_defect_location').collapse('hide');
						$('#btn_back_to_disp').hide();
						$('#btn_back_to_type').hide();
						$('#btn_back_to_origin').hide();
						$('#div_defect_modal').modal('show');
					} else {
						// what do we do if unsuccessful?
					}
				}
			}
		);
	}

	$('#div_defect_modal').on('shown.bs.modal', function (e) {
		$('#sp_selected_disp').html('');
		$('#sp_selected_type').html('');
		$('#sp_selected_origin').html('');
		$('#sp_selected_locations').html('');
//		$('#btn_back_to_part').hide();
//		$('#btn_back_to_disp').hide();
//		$('#btn_back_to_type').hide();
//		$('#btn_back_to_origin').hide();
//		$('#div_defect_disp').collapse('hide');
//		$('#div_defect_type').collapse('hide');
//		$('#div_defect_origin').collapse('hide');
//		$('#div_defect_location').collapse('hide');
	});

	$('#div_defect_part').on('click', '.defect-select-part-button', function() {
		cur_defect_part = $(this).data();
		cur_defect_serial = cur_defect_part.serial_number;
		$('#sp_selected_part').html('<b>(' + cur_defect_part.part_desc + ': ' + cur_defect_serial + ')</b>');
		$('#div_defect_part').collapse('hide');
		if (defect_options.only_scrap) {
			$('#div_defect_disposition').collapse('hide');
			$('#btn_back_to_part').hide();
			selectDisposition('SCRAP');
		} else {
			$('#div_defect_disposition').collapse('show');
			$('#btn_back_to_part').show();
		}
		$('#div_defect_type').collapse('hide');
		$('#div_defect_origin').collapse('hide');
		$('#div_defect_location').collapse('hide');

	});

	$('.btn-defect-disp').on('click', function() {
		cur_defect_disp = $(this).data('disp');

		$('#sp_selected_disp').html('<b>(' + cur_defect_disp + ')</b>');
		$('#btn_back_to_disp').show();

		$('#div_defect_disposition').collapse('hide');
		$('#div_defect_type').collapse('show');
		$('#div_defect_origin').collapse('hide');
		$('#div_defect_location').collapse('hide');
		getDefectTypes();
	});

	$('#btn_back_to_part').on('click', function() {
		$('#sp_selected_disp').html('');
		$('#sp_selected_part').html('');
		$('#sp_selected_type').html('');
		$('#sp_selected_origin').html('');
		$('#sp_selected_locations').html('');

		$('#btn_back_to_part').hide();
		$('#btn_back_to_disp').hide();
		$('#btn_back_to_type').hide();
		$('#btn_back_to_origin').hide();

		$('#div_defect_part').collapse('show');
		$('#div_defect_disposition').collapse('hide');
		$('#div_defect_type').collapse('hide');
		$('#div_defect_origin').collapse('hide');
		$('#div_defect_location').collapse('hide');
		KEYS.registerKeyHandler({"name":"scan_serial", "global":false, "match_keys":null, "callback":scanDefectSerial, "display_el_id":"sp_scan_defect_serial"});
	});

	$('#btn_back_to_disp').on('click', function() {

		if (defect_options.only_scrap) {
			$('#sp_selected_disp').html('');
			$('#sp_selected_part').html('');
			$('#sp_selected_type').html('');
			$('#sp_selected_origin').html('');
			$('#sp_selected_locations').html('');

			$('#btn_back_to_part').hide();
			$('#btn_back_to_disp').hide();
			$('#btn_back_to_type').hide();
			$('#btn_back_to_origin').hide();

			$('#div_defect_part').collapse('show');
			$('#div_defect_disposition').collapse('hide');
			$('#div_defect_type').collapse('hide');
			$('#div_defect_origin').collapse('hide');
			$('#div_defect_location').collapse('hide');
			KEYS.registerKeyHandler({"name":"scan_serial", "global":false, "match_keys":null, "callback":scanDefectSerial, "display_el_id":"sp_scan_defect_serial"});
		} else {
			$('#sp_selected_disp').html('');
			$('#sp_selected_type').html('');
			$('#sp_selected_origin').html('');
			$('#sp_selected_locations').html('');
			$('#btn_back_to_disp').hide();
			$('#btn_back_to_type').hide();
			$('#div_defect_disposition').collapse('show');
			$('#div_defect_type').collapse('hide');
			$('#div_defect_location').collapse('hide');
		}
	});

	$('#btn_back_to_type').on('click', function() {
		$('#sp_selected_type').html('');
		$('#sp_selected_origin').html('');
		$('#sp_selected_locations').html('');
		$('#btn_back_to_type').hide();
		$('#div_defect_disposition').collapse('hide');
		$('#div_defect_type').collapse('show');
		$('#div_defect_origin').collapse('hide');
		$('#div_defect_location').collapse('hide');
	});

	$('#btn_back_to_origin').on('click', function() {
		if (defect_options.single_origin_ID == 0) {
			$('#sp_selected_type').html('');
			$('#sp_selected_origin').html('');
			$('#sp_selected_locations').html('');
			$('#btn_back_to_origin').hide();
			$('#div_defect_disposition').collapse('hide');
			$('#div_defect_type').collapse('hide');
			$('#div_defect_origin').collapse('show');
			$('#div_defect_location').collapse('hide');
		} else {
			$('#sp_selected_type').html('');
			$('#sp_selected_origin').html('');
			$('#sp_selected_locations').html('');
			$('#btn_back_to_type').hide();
			$('#btn_back_to_disp').show();
			$('#div_defect_disposition').collapse('hide');
			$('#div_defect_type').collapse('show');
			$('#div_defect_origin').collapse('hide');
			$('#div_defect_location').collapse('hide');
		}
	});

	$('#div_defect_location_container').on('click', function(event) {
		console.log(event.offsetX + ',' + event.offsetY);

		// var c = document.getElementById("div_defect_location_container");
		// var ctx = c.getContext("2d");
		// ctx.beginPath();
		// ctx.arc(event.offsetX, event.offsetY, 10, 0, 2*Math.PI);
		// ctx.stroke();


		$("#div_defect_location_container").html($("#div_defect_location_container").html());

		cur_defect_locs.push({"x":event.offsetX, "y":event.offsetY});

		updateDots();
	});

	$('.btn-save-defect').on('click', function() {
		event.preventDefault();
		$(this).attr("disabled", true).html('Saving Defect...');
		$('.btn-record-good,.btn-record-reject').addClass('disabled');
		saveDefect();
	});


	//	$('#div_top5_defects').highcharts({
	//		chart: {
	//			type: 'column',
	//			spacingBottom: 0,
	//			spacingTop: 5,
	//			borderWidth: 1,
	//			borderRadius: 5
	//		},
	//		title: {
	//			text: 'Top 5 Defects',
	//			enabled: false,
	//			style: {"font-size":"10pt", "font-weight":"bold"},
	//			margin: 0
	//		},
	//		credits: {
	//			enabled: false
	//		},
	//		xAxis: {
	//			categories: []
	//		},
	//		yAxis: [{
	//			min: 0,
	//			endOnTick: false,
	//			labels: {enabled: false},
	//			lineWidth: 0,
	//			lineColor: 'transparent',
	//			gridLineColor: 'transparent',
	//			minorGridLineWidth: 0,
	//			minorTickLength: 0,
	//			tickLength: 0,
	//			title: {
	//				text: 'Top 5',
	//				enabled: false
	//			}
	//		}],
	//		legend: {
	//			shadow: false,
	//			enabled: false
	//		},
	//		tooltip: {
	//			shared: true
	//		},
	//		plotOptions: {
	//			column: {
	//				grouping: false,
	//				shadow: false,
	//				borderWidth: 0
	//			}
	//		},
	//		series: [{
	//			name: 'Defects',
	//			color: 'rgba(22,170,2,.7)',
	//			data: [],
	//			pointPadding: 0.1,
	//			pointPlacement: 0,
	//			dataLabels: { enabled: true }
	//		}]
	//	});
});

function scanDefectSerial(scan) {
	console.log('Got serial scan: ' + scan);
	KEYS.halted = true;
	$('#div_serial_scan_result').html('Looking up serial scan: ' + scan + '...').show();

	$.getJSON(ajax_procs.defects, {"action":"lookup_serial", "scan":scan, "tool_ID":current_tool.tool_ID}, function(jso) {
		KEYS.halted = false;
		jso = jso || [];
		if (jso) {
			if (jso.length > 0) {
				if (jso[0].disposition != 'SCRAP') {
					KEYS.resetKeys();
					cur_defect_serial = jso[0].barcode;
					cur_defect_part = {"part_ID":jso[0].part_ID, "part_desc":jso[0].part_desc, "part_image":jso[0].part_image};
					cur_defect_machine_cycle_part_ID = jso[0].ID;
					$('#sp_selected_part').html('<b>(' + cur_defect_part.part_desc + ': ' + cur_defect_serial + ')</b>');
					$('#div_serial_scan_result').html('PART FOUND! ' + scan).removeClass('alert-danger').addClass('alert-success');
					
					$('#div_defect_part').collapse('hide');
					if (defect_options.only_scrap) {
						$('#btn_back_to_part').hide();
						$('#div_defect_disposition').collapse('hide');
						selectDisposition('SCRAP');
					} else {
						$('#btn_back_to_part').show();
						$('#div_defect_disposition').collapse('show');
					}
					$('#div_serial_scan_result').html('').hide();
				} else if(jso[0].disposition == 'SCRAP') {
					$('#div_serial_scan_result').html('PART HAS ALREADY BEEN SCRAPPED! ').removeClass('alert-success').addClass('alert-danger');
				} else {
					$('#div_serial_scan_result').html('PART NOT FOUND! ' + scan).removeClass('alert-success').addClass('alert-danger');
				}
			}
		} else {
			$('#div_serial_scan_result').html('NO RESPONSE!! ' + scan).removeClass('alert-success').addClass('alert-danger');
		}
	});
}

function selectDisposition(disp) {
	cur_defect_disp = disp;

	$('#sp_selected_disp').html('<b>(' + cur_defect_disp + ')</b>');
	$('#btn_back_to_disp').show();

	$('#div_defect_disposition').collapse('hide');
	$('#div_defect_type').collapse('show');
	$('#div_defect_origin').collapse('hide');
	$('#div_defect_location').collapse('hide');
	getDefectTypes();
}

function updateDefectTop5() {
	//update defect top 5 chart
//	var chart = $('#div_top5_defects').highcharts();
//	var labels = [];
//	var data = [];
//	for (var i = 0; i < all_data.defects.length; i++) {
//		labels.push(all_data.defects[i].defect_description);
//		data.push(all_data.defects[i].defect_count);
//	}
//	chart.xAxis[0].setCategories(labels, false);
//	chart.series[0].setData(data);
}


function getDefectTypes() {
	$('#div_defect_chain').html('');
	$('#div_defect_type_buttons').html('Loading defect types...');
	$.ajax({
		"url": ajax_procs.defects,
		"type": "POST",
		"dataType": "json",
		"cache": false,
		"data": {
			"action": "get_defect_types",
			"machine_type_ID":all_data.machine[0].machine_type_ID
		},
		"success": function(jso) {
			cur_defect_types = jso || [];
			loadDefectTypes(0, 0);
		}

	});
}

function loadDefectTypes(parent_ID, level) {
	if (parent_ID == 0) {
		cur_defect_chain = '';
	}
	$('#div_defect_type_buttons').html('');
	$('#div_defect_chain').html(cur_defect_chain);

	if (cur_defect_types.length == 0) {
		$('#div_defect_type_buttons').html('<h3 class="label label-danger">No defects defined!!</h3>');
	} else {
		for (var i = 0; i < cur_defect_types.length; i++) {
			if (cur_defect_types[i].parent_ID == parent_ID) {
				var b = document.createElement('BUTTON');
				var series = cur_defect_types[i].series || '';

				$(b).addClass('btn btn-primary btn-lg defect-button');
				$(b).data('dtrid', cur_defect_types[i].ID);
				$(b).data('desc', cur_defect_types[i].defect_description);
				$(b).data('series', series);

				var num_children = 0;
				for (var j = 0; j < cur_defect_types.length; j++) {
					if (cur_defect_types[j].parent_ID == cur_defect_types[i].ID) {
						num_children++;
					}
				}

				if (series != '') {
					series = series + ' - ';
				}

				if (num_children > 0) {
					$(b).html(series + cur_defect_types[i].defect_description + ' (' + num_children + ')');
					$(b).on('click', function(e) { cur_defect_chain += $(e.target).data('desc') + '-> ';  loadDefectTypes($(e.target).data('dtrid'), level + 1); });
				} else {
					$(b).html(series + cur_defect_types[i].defect_description);
					$(b).on('click', function(e) { e.stopPropagation(); e.preventDefault(); selectDefectType(this); });
				}

				$('#div_defect_type_buttons').append(b);
			}
		}
	}

	if (level > 0) {
		var b = document.createElement('BUTTON');
		$(b).addClass('btn btn-warning defect-button').html('Start Over');
		$(b).on('click', function(e) { loadDefectTypes(0, 0); });
		$('#div_defect_type_buttons').append(b);
	}
}

function selectDefectType(el) {
	var data = $(el).data();
	cur_defect_type = data.dtrid;
	$('#sp_selected_type').html('<b>(' + data.desc + ')</b>');
	$('#div_defect_disposition').collapse('hide');
	$('#div_defect_type').collapse('hide');
	if (defect_options.single_origin_ID == 0) {
		$('#div_defect_origin').collapse('show');
		$('#btn_back_to_type').show();

		getDefectOrigins();
	} else {
		$('#div_defect_origin').collapse('hide');
		var b = $('<BUTTON />');
		b.data('dtrid', defect_options.single_origin_ID);
		b.data('desc', 'DEFAULT');
		b.data('series', '');
		$('#btn_back_to_disp').hide();
		selectDefectOrigin(b);
	}
}

function getDefectOrigins() {
	$.ajax({
		"url": ajax_procs.defects,
		"type": "POST",
		"dataType": "json",
		"cache": false,
		"data": {
			"action": "get_defect_origins",
			"machine_type_ID":all_data.machine[0].machine_type_ID
		},
		"success": function(jso) {
			cur_defect_origins = jso || [];
			loadDefectOrigins(0, 0);
		}

	});
}

function loadDefectOrigins(parent_ID, level) {
	if (parent_ID == 0) {
		cur_defect_origin_chain = '';
	}
	$('#div_defect_origin_buttons').html('');
	$('#div_defect_origin_chain').html(cur_defect_origin_chain);

	if (cur_defect_origins.length == 0) {
		$('#div_defect_origin_buttons').html('<h3 class="label label-danger">No defects origins defined!!</h3>');
	} else {
		if (cur_defect_origins.length == 1) {
			var series = cur_defect_origins[0].series || '';

			if (series != '') {
				series = series + ' - ';
			}
			var b = $('<BUTTON />').data(cur_defect_origins[0]).addClass('btn btn-primary btn-lg defect-button').html(series + cur_defect_origins[0].origin_description);
			b.data('dtrid', cur_defect_origins[0].ID);
			b.data('desc', cur_defect_origins[0].origin_description);
			b.data('series', series);
			$('#div_defect_origin_buttons').append(b);
			selectDefectOrigin(b);
			return;
		}
		for (var i = 0; i < cur_defect_origins.length; i++) {
			if (cur_defect_origins[i].parent_ID == parent_ID) {
				var b = document.createElement('BUTTON');
				var series = cur_defect_origins[i].series || '';

				$(b).addClass('btn btn-primary btn-lg defect-button');
				$(b).data('dtrid', cur_defect_origins[i].ID);
				$(b).data('desc', cur_defect_origins[i].origin_description);
				$(b).data('series', series);

				var num_children = 0;
				for (var j = 0; j < cur_defect_origins.length; j++) {
					if (cur_defect_origins[j].parent_ID == cur_defect_origins[i].ID) {
						num_children++;
					}
				}

				if (series != '') {
					series = series + ' - ';
				}

				if (num_children > 0) {
					$(b).html(series + cur_defect_origins[i].origin_description + ' (' + num_children + ')');
					$(b).on('click', function(e) { cur_defect_origin_chain += $(e.target).data('desc') + '-> ';  loadDefectOrigins($(e.target).data('dtrid'), level + 1); });
				} else {
					$(b).html(series + cur_defect_origins[i].origin_description);
					$(b).on('click', function(e) { e.stopPropagation(); e.preventDefault(); selectDefectOrigin(this); });
				}

				$('#div_defect_origin_buttons').append(b);
			}
		}
	}

	if (level > 0) {
		var b = document.createElement('BUTTON');
		$(b).addClass('btn btn-warning defect-button').html('Start Over');
		$(b).on('click', function(e) { loadDefectOrigins(0, 0); });
		$('#div_defect_origin_buttons').append(b);
	}
}

function selectDefectOrigin(el) {
	var data = $(el).data();
	cur_defect_origin = data.dtrid;
	
	$('#btn_back_to_origin').show();
	$('#sp_selected_origin').html('<b>(' + data.desc + ')</b>');
	$('#div_defect_origin').collapse('hide');
	$('#div_defect_location').collapse('show');
	$('.btn-save-defect').attr("disabled", false).html('Save Defect');

	cur_defect_locs = [];
	updateDots();

	//$('#div_defect_location_container').css('background-image', 'url(./public/images/part_images/part_' + cur_defect_part.part_ID +'/photo.jpg)').show();
	//$('#div_defect_location_container').css('background-image', 'url(' + cur_defect_part.part_image +')').show();
	// $('#div_defect_location_container').attr('style', "background-image: url('" + cur_defect_part.part_image +
	// 	"');background-repeat: no-repeat;").show();
	var img = document.createElementNS('http://www.w3.org/2000/svg','image');
	img.setAttributeNS(null,'height','600');
	img.setAttributeNS(null,'width','600');
	img.setAttributeNS('http://www.w3.org/1999/xlink','href', cur_defect_part.part_image );
	img.setAttributeNS(null,'x','0');
	img.setAttributeNS(null,'y','0');
	img.setAttributeNS(null, 'visibility', 'visible');
	$('#div_defect_location_container').append(img).show();
}

function removeDot(event) {
	event.cancelBubble = true;

	var obj = event.target;

	for (var i = 0; i < cur_defect_locs.length; i++) {
		if ($(obj).attr("cx") == cur_defect_locs[i].x) {
			cur_defect_locs.splice(i, 1);
			break;
		}
	}

	updateDots();
}

function updateDots() {
	$('#div_defect_location_container CIRCLE').remove();
	for (var i = 0; i < cur_defect_locs.length; i++) {
		var c = $('<circle />');
		var obj = cur_defect_locs[i];
		c.addClass('defect-dot');
		c.attr("cx", obj.x);
		c.attr("cy", obj.y);
		c.attr("r", 5);
		c.attr("stroke", "#CCCCCC");
		c.attr("stroke-width", 2);
		c.attr("fill", "orange");
		c.attr("pointer-events", "visible");
		c.attr("onclick", "removeDot(event);");

		$('#div_defect_location_container').append(c);
	}
	$("#div_defect_location_container").html($("#div_defect_location_container").html());

	var suff = ((cur_defect_locs.length == 1)?'':'s');
	$('#sp_selected_locations').html(' <b>(' + cur_defect_locs.length + ' location' + suff + ')</b>');
}

function saveDefect() {
	var locs = [];
	for (var i = 0; i < cur_defect_locs.length; i++) {
		locs.push(cur_defect_locs[i].x + '~' + cur_defect_locs[i].y);
	}

	$.getJSON(ajax_procs.defects,
		{
			"action": "save_defect",
			"disposition":cur_defect_disp,
			"defect_ID":cur_defect_type,
			"defect_origin_ID":cur_defect_origin,
			"locations":locs.join(','),
			"machine_ID":all_data.machine[0].ID,
			"tool_ID":current_tool.tool_ID,
			"part_ID":cur_defect_part.part_ID,
			"operator_ID":operator.ID,
			"cur_defect_machine_cycle_part_ID":cur_defect_machine_cycle_part_ID,
			"serial_number":cur_defect_serial
		},
		function(jso) {
			if(jso) {
				$('.btn-record-good,.btn-record-reject').addClass('disabled');
				$('#div_defect_modal').modal('hide');
				if(jso.success == 1) {
					$.growl(
						{"title":" Defect Recorded ", "message":"Defect successfully saved!", "icon":"glyphicon glyphicon-saved"},
						{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
				} else {
					$.growl(
						{"title":" Record Defect Failed: ", "message":"Was not able to save the defect!", "icon":"glyphicon glyphicon-exclamation-sign"},
						{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
				}
			}
		}
	)

//	$.ajax({
//		"url": ajax_procs.defects,
//		"type": "POST",
//		"dataType": "json",
//		"cache": false,
//		"data": {
//			"action": "save_defect",
//			"disposition":cur_defect_disp,
//			"defect_ID":cur_defect_type,
//			"defect_origin_ID":cur_defect_origin,
//			"locations":locs.join(','),
//			"machine_ID":all_data.machine[0].ID,
//			"tool_ID":current_tool.tool_ID,
//			"part_ID":cur_defect_part.part_ID,
//			"operator_ID":operator.ID,
//			"cur_defect_machine_cycle_part_ID":cur_defect_machine_cycle_part_ID,
//			"serial_number":cur_defect_serial
//		},
//		"success": function(jso) {
//			$('#div_defect_modal').modal('hide');
//			$.growl(
//				{"title":" Defect Recorded ", "message":"Defect successfully saved!", "icon":"glyphicon glyphicon-exclamation-sign"},
//				{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
//			);
//		}
//	});
}