

function updateCurrentPartTable() {
	if (!current_tool) {
		return;
	}

	if (!current_tool.active_parts) {
		return;
	}

	var h = $('#tbl_label THEAD').empty();
	var b = $('#tbl_label TBODY').empty();

	var num_active = current_tool.active_parts.length;
	var w = 100.0 / num_active;

	var r_queue = $('<TR />').appendTo(b);
	if (num_active > 1) {
		var r_all_good = $('<TR />').appendTo(b);
	}
	var r_good = $('<TR />').appendTo(b);
	var r_bad = $('<TR />').appendTo(b);
	var r_rack_title = $('<TR />').appendTo(b);
	var r_rack_summary = $('<TR />').appendTo(b);
	var r_rack_buttons1 = $('<TR />').appendTo(b);
	var r_rack_buttons2 = $('<TR />').appendTo(b);

	for (var i = 0; i < num_active; i++) {
		var a = current_tool.active_parts[i];
		$('<TH />').appendTo(h).html('<h4>' + a.part_desc + '<br><span class="badge bigger">' + a.part_number + '</span></h4>').css('width', w + '%');

		//get current queue
		a.current_queue = 0;
		// if (current_tool.part_queue_count.length > 0) {
			// for (var j = 0; j < current_tool.part_queue_count.length; j++) {
				// if(a.ID == current_tool.part_queue_count[j].part_ID) {
					// a.current_queue = current_tool.part_queue_count.part_count;
				// }
			// }
		// } else {
			for (var j = 0; j < current_tool.part_queue.length; j++) {
				if (a.ID == current_tool.part_queue[j].part_ID) {
					a.current_queue++;
				}
			}
		// }
		

		$('<TD />').appendTo(r_queue).html('<h4>QUEUE: ' + a.current_queue + '</h4>');

		if (num_active > 1 && i == 0) {
			var c = $('<TD />').appendTo(r_all_good).attr('colspan', num_active);
			var good_btn = $('<BUTTON />').addClass('btn btn-success btn-lg btn-record-all-good').html('<span class="glyphicon glyphicon-thumbs-up"></span> ALL GOOD');
			c.append(good_btn);
		}

		var good_btn = $('<BUTTON />').addClass('btn btn-success btn-lg btn-record-good').html('<span class="glyphicon glyphicon-thumbs-up"></span> GOOD').data(a);
		if (a.current_queue == 0) {
			good_btn.addClass('disabled').prop('disabled', true);
		}
		$('<TD />').appendTo(r_good).append(good_btn);

		var bad_btn = $('<BUTTON />').addClass('btn btn-danger btn-lg btn-record-reject').html('<span class="glyphicon glyphicon-thumbs-down"></span> REJECT').data(a);
		if (a.current_queue == 0) {
			bad_btn.addClass('disabled').prop('disabled', true);
		}
		$('<TD />').appendTo(r_bad).append(bad_btn);

		a.current_rack_count = 0;
		a.container_ID = 0;
		for (var j = 0; j < current_tool.container_info.length; j++) {
			if (a.ID == current_tool.container_info[j].part_ID) {
				a.current_rack_count++;
				a.container_ID = current_tool.container_info[j].container_ID;
			}
		}
		var current_rack_count = a.current_rack_count;
		var current_rack_std_pack = a.standard_pack_qty;
		var current_rack_percent = current_rack_count / current_rack_std_pack * 100.0;

		if (a.standard_pack_qty > 0) {
			if (a.container_ID > 0) {
				$('<TD />').appendTo(r_rack_title).html('<h4>CURRENT RACK (ID: '+a.container_ID+')</h4>');
			} else {
				$('<TD />').appendTo(r_rack_title).html('<h4>CURRENT RACK (new)</h4>');
			}

			var c = $('<TD />').appendTo(r_rack_summary).html('<h4>' + current_rack_count + ' of ' + current_rack_std_pack + '</h4>');
			var d = $('<div />').appendTo(c).css('height', '10px').css('border', 'thin solid #CCCCCC');
			d.css('background-image', 'linear-gradient(to right, rgb(0, 150, 0) 0%, rgb(0, 175, 0) 17%, rgb(0, 190, 0) 33%, rgb(82, 210, 82) 67%, rgb(131, 230, 131) 83%, rgb(180, 221, 180) 100%)');
			d.css('background-size', current_rack_percent + '% 10px');
			d.css('background-repeat','no-repeat');
			d.css('background-position','left bottom');

			var c1 = $('<TD />').appendTo(r_rack_buttons1);
			if (current_rack_count > 0) {
				//allow them to enter partial and remove a part

				$('<BUTTON />').addClass('btn btn-warning btn-lg btn-full-width btn-end-as-partial').html('<span class="glyphicon glyphicon-credit-card"></span> END AS<br>PARTIAL').data(a).appendTo(c1);

				var c2 = $('<TD />').appendTo(r_rack_buttons2);
				$('<BUTTON />').addClass('btn btn-warning btn-lg btn-full-width btn-remove-rack-part').html('<span class="glyphicon glyphicon-remove"></span> REMOVE<br>PART').data(a).appendTo(c2);
			} else {
				//allow them to resume a partial
				var d = $('<DIV />').appendTo(c);
				$('<BUTTON />').addClass('btn btn-warning btn-lg btn-full-width btn-resume-partial').html('<span class="glyphicon glyphicon-credit-card"></span> RESUME<BR>PARTIAL').data(a).appendTo(c1);

				var c2 = $('<TD />').appendTo(r_rack_buttons2).html('&nbsp;');
			}
		} else {
			$('<TD />').appendTo(r_rack_title).html('&nbsp;');
			$('<TD />').appendTo(r_rack_summary).html('&nbsp;');
			$('<TD />').appendTo(r_rack_buttons1).html('&nbsp;');
			$('<TD />').appendTo(r_rack_buttons2).html('&nbsp;');
		}
	}
}

function recordGoodPart(part_ID, print_template_ID) {
	var action = "record_good_part";
	if (part_ID == 0) {
		action = "record_all_good_parts";
	}
	$.getJSON(ajax_procs.main, {
			"action":action,
			"machine_ID":all_data.machine[0].ID,
			"part_ID":part_ID,
			"operator_ID":operator.ID
		}, function(jso) {
				if (part_ID == 0) {
					$.growl("All parts recorded as GOOD!", {"type":"success"});
				} else {
					if (print_template_ID == 0) {
						$.growl("GOOD part recorded!", {"type":"success"});
					} else {
						$.growl("Label printed!", {"type":"success"});
					}
				}
			}
	);
}