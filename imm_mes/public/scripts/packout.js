var operator = {};
var ajax_procs = {
							"login":"./ajax_processors/login.php",
							"packout":"./ajax_processors/packout.php",
							"downtime":"./ajax_processors/downtime.php",
							"defects":"./ajax_processors/defects.php",
							"production":"./ajax_processors/production.php",
							"PLC":"/PLCService/PLCWebReadWrite.asmx"
						};
var all_data = {};
var cycle_timer = null;
var cycle_timer_count = 0;
var current_tool = {};
var time_timer = null;
var last_shift = 0;
var initial_data_loaded = false;
var current_version = '';

$(function () {
	$(document).ajaxError(function(a, b, c) {
		console.log('MAJOR AJAX ERROR: ' + b.responseText);
		$.growl(
			{"title":" ERROR: ", "message":"Major error processing request: " + b.statusText, "icon":"glyphicon glyphicon-exclamation-sign"},
			{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
	});

		// if a modal is shown give it focus
	$('.modal').on('shown.bs.modal', function() {
		$(this).focus();
	});

	$(document).on('click', '#btn_work_instructions', function () {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		displayCurrentWorkInstructions();
	});

	$('#div_instructions_carousel').on('slid.bs.carousel', function () {
		setTimeout(recordWorkInstructionSignoff, 500);
	});

	$('#div_instructions_carousel').on('shown.bs.modal', function () {
		recordWorkInstructionSignoff();
	});

	$('#div_packout_content').on('click', '.btn-reset-rack', function() {
		var d = $(this).data();
		$('#div_confirm_reset_modal').modal('show').data(d);
	});
	
	$('.btn-reset-rack').on('click', function() {
		$.getJSON(ajax_procs.packout, {"action":"reset_rack", "machine_group_ID":$('#div_confirm_reset_modal').data('machine_group_ID')}, function(jso) {
			$.growl(
				{"title":" RACK RESET! ", "message":"All parts removed from rack!", "icon":"glyphicon glyphicon-exclamation-sign"},
				{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
			);
			$('#div_confirm_reset_modal').modal('hide').removeData();
		});
	});

	$('#div_packout_content').on('click', '.btn-remove-part', function() {
		var d = $(this).data();
		$('#div_confirm_remove_part').empty().hide();
		$('#sp_remove_group_desc').html('<h3>' + d.group_desc + '</h3>');
		$('#div_remove_part_modal').modal('show').data(d);
	});

	$('#div_packout_content').on('click', '.btn-insert-part', function() {
		var d = $(this).data();
		$('#div_confirm_insert_part').empty().hide();
		$('#sp_insert_group_desc').html('<h3>' + d.group_desc + '</h3>');
		$('#div_insert_part_modal').modal('show').data(d);
	});

	$('#div_remove_part_modal').on('shown.bs.modal', function() {
		$('#inp_remove_part_scan').focus().val('');
	});

	$('body').on('keydown', '#inp_remove_part_scan', function() {
		if (event.which == 13 || event.which == 9) {
			event.preventDefault();
			
			var scan_value = $('#inp_remove_part_scan').val();
			$('#div_confirm_remove_part').html('<i>Looking up part...</i>').show();
			$('#inp_remove_part_scan').val('').focus();
			
			//lookup part to remove 
			$.getJSON(ajax_procs.packout, {"action":"lookup_part", "in_current_rack":1, "scan":scan_value, "machine_group_ID":$('#div_remove_part_modal').data('machine_group_ID')}, function(jso) {
				jso = jso || [];
				if (jso.length == 0) {
					//not found
					$('#div_confirm_remove_part').html('<div class="alert alert-danger">Part not found: ' + scan_value + '</div>');
				} else {
					if (jso[0].rack_ID == null) {
						$('#div_confirm_remove_part').html('<div class="alert alert-danger">Part is not in a rack: ' + scan_value + '</div>');
					} else {
						if (jso[0].end_time != null) {
							$('#div_confirm_remove_part').html('<div class="alert alert-danger">Part is in an already ended rack: ' + scan_value + '</div>');
						} else {
							//remove it?
							$('#div_confirm_remove_part')
								.html('<div class="alert alert-success">' + 
									  '   <div>Found part: ' + scan_value + '!  Are you sure you want to remove it from the current rack?</div>' +
									  '	  <div><button class="btn btn-primary btn-confirm-remove-from-rack">Yes, Remove Part From Rack</button></div>' + 
									  '</div>').data(jso[0]);
								
						}
					}
				}
			});
		}
	});
	
	$('#div_remove_part_modal').on('click', '.btn-confirm-remove-from-rack', function() {
		$.getJSON(ajax_procs.packout, {"action":"remove_part", "machine_cycle_part_ID":$('#div_confirm_remove_part').data('ID')}, function(jso) {
			$.growl(
				{"title":" PART REMOVED! ", "message":"Part removed from rack!", "icon":"glyphicon glyphicon-exclamation-sign"},
				{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
			);
			$('#div_confirm_remove_part').html('<div class="alert alert-success">Part Removed!</div>');
			$('#div_remove_part_modal').modal('hide').removeData();
		});
	});
	
	
	//INSERT PART
	$('#div_insert_part_modal').on('shown.bs.modal', function() {
		$('#inp_insert_part_scan').focus().val('');
	});

	// $('#inp_insert_part_scan').on('keypress', function() {
	// 	if (event.which == 13) {
	// 		event.preventDefault();
			
	// 		var scan_value = $('#inp_insert_part_scan').val();
	// 		$('#div_confirm_insert_part').html('<i>Looking up part...</i>').show();
	// 		$('#inp_insert_part_scan').val('').focus();
			
	// 		//lookup part to insert 
	// 		$.getJSON(ajax_procs.packout, {"action":"lookup_part", "in_current_rack":0, "scan":scan_value, "machine_group_ID":$('#div_insert_part_modal').data('machine_group_ID')}, function(jso) {
	// 			jso = jso || [];
	// 			if (jso.length == 0) {
	// 				//not found
	// 				$('#div_confirm_insert_part').html('<div class="alert alert-danger">Part not found: ' + scan_value + '</div>');
	// 			} else {
	// 				if (jso[0].rack_ID != null) {
	// 					$('#div_confirm_insert_part').html('<div class="alert alert-danger">Part is already in a rack: ' + scan_value + '</div>');
	// 				} else {
	// 					//add it?
	// 					$('#div_confirm_insert_part')
	// 						.html('<div class="alert alert-success">' + 
	// 							  '   <div>Found part: ' + scan_value + '!  Are you sure you want to insert it into the current rack?</div>' +
	// 							  '	  <div><button class="btn btn-primary btn-confirm-insert-into-rack">Yes, Insert Into Current Rack</button></div>' + 
	// 							  '</div>').data(jso[0]);
	// 				}
	// 			}
	// 		});
	// 	}
	// });
	//. add in check for reworked part
	$('body').on('keydown', '#inp_insert_part_scan', function() {
		if (event.which == 13 || event.which == 9) {
			event.preventDefault();
			
			var scan_value = $('#inp_insert_part_scan').val();
			$('#div_confirm_insert_part').html('<i>Looking up part...</i>').show();
			$('#inp_insert_part_scan').val('').focus();
			
			//lookup part to insert 
			$.getJSON(ajax_procs.packout, {"action":"do_manual_packout", "barcode":scan_value, "operator_ID": all_data.machine[0].current_operator_ID, "machine_group_ID":$('#div_insert_part_modal').data('machine_group_ID')}, function(jso) {
				if (jso && jso.length > 0) {
					var alert_type = 'alert-danger';
					if (jso[0].result == 'SUCCESS') {
						alert_type = 'alert-success';
					}
					$('#div_confirm_insert_part').html('<div class="alert ' + alert_type + '"">' + jso[0].result + '</div>');
				}				
			});
		}
	});
	
	// $('#div_insert_part_modal').on('click', '.btn-confirm-insert-into-rack', function() {
	// 	$.getJSON(ajax_procs.packout, {
	// 			"action":"insert_part", 
	// 			"machine_cycle_part_ID":$('#div_confirm_insert_part').data('ID'), 
	// 			"machine_group_ID":$('#div_insert_part_modal').data('machine_group_ID'),
	// 			"operator_ID":operator.ID
	// 		}, function(jso) {
	// 		$.growl(
	// 			{"title":" PART INSERTED! ", "message":"Part inserted into rack!", "icon":"glyphicon glyphicon-exclamation-sign"},
	// 			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
	// 		);
	// 		$('#div_confirm_insert_part').html('<div class="alert alert-success">Part Inserted!</div>');
	// 		$('#div_insert_part_modal').modal('hide').removeData();
	// 	});
	// });

	$('#div_current_part DIV').hide();
	loadAllData();
	//setTimeout(doLogin, 1000);

	setInterval(checkStuck, 30000);
});

function checkStuck() {
	if (time_timer == null) {
		console.log('DETECTED TIMER STUCK....reloading all data.');
		$.growl(
			{"title":" WARNING: ", "message":"Detected timer stuck.  Reloading all data...", "icon":"glyphicon glyphicon-exclamation-sign"},
			{"type":"warning", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);

		refreshData();
	}
}

function loadAllData() {
	if (initial_data_loaded) {
		$('#div_loading').modal('show');
	}

	if (my_IP.length < 8) {
		alert('INVALID IP ADDRESS: ' + my_IP);
		return;
	}

	var arr = ['machine', 'production', 'packout', 'time_and_shift', 'version', 'work_instructions', 'work_instruction_signoffs',
	'quality_alerts', 'safety_alerts', 'alert_signoffs', 'data_points'];

	$('#sp_loading_status').html('Loading all data...');
	$.ajax(ajax_procs.packout, {
		"data":{
			"action":"load_data",
			"data_types":arr.join('~'),
			"my_IP":my_IP
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			all_data = jso;

			for (x in all_data) {
				if (!all_data[x]) {
					all_data[x] = [];
				}
			}

			if (!initial_data_loaded) {
				drawPackout();
			}
			if (!is_logged_in) {
				setTimeout(doLogin, 1000);
			}

			//set machine name
			$('#sp_machine_name').html(all_data.machine[0].machine_name);

			setCurrentTime();
			updateTime();

			updateProductionSummary();

			updateShiftCounts();

			if (current_version == '') {
				current_version = all_data.version;
			} else {
				if (current_version != all_data.version) {
					//reload js
					document.location = document.location;
				}
			}

			$('#sp_version').html(current_version);

			$('#div_loading').modal('hide');
		}
	});


}

function setCurrentTime() {
	if (time_timer) {
		clearInterval(time_timer);
		time_timer = null;
	}

	time_timer = setInterval(incrementTime, 1000);
}

function incrementTime() {
	all_data.time_and_shift[0].cur_time = Date.DateAdd("s", 1, all_data.time_and_shift[0].cur_time);
	updateTime();

	if (all_data.time_and_shift[0].cur_time.getSeconds() % 5 == 0) {
		clearInterval(time_timer);
		time_timer = null;
		refreshData();
	}
}

function updateTime() {
	//set time and shift
	$('#sp_time').html(formatTime(all_data.time_and_shift[0].cur_time));
	$('#sp_shift').html('Shift ' + all_data.time_and_shift[0].cur_shift);

	if (last_shift != 0) {
		if (all_data.time_and_shift[0].cur_shift != last_shift) {
			//logout
			last_shift = 0;
			operator = {};
			$('#div_operator').slideUp();
			doLogin();
		}
	} else {
		last_shift = all_data.time_and_shift[0].cur_shift;
	}
}

function drawPackout() {
	$('#div_packout_content').html('');

	for (var i = 0; i < all_data.packout.container_info.length; i++) {
		var holes = 0;
		var c = $('<DIV />').appendTo($('#div_packout_content'));
		var rack = all_data.packout.container_info[i];
		var num_packed = rack.parts.length;
		var parts_per_layer = rack.rack_boxes_across * rack.rack_boxes_deep * rack.rack_parts_per_box;
		var parts_per_rack = parts_per_layer * rack.rack_layers;
		var layers_loaded = Math.floor(num_packed / parts_per_layer);
		var top_layer_parts_packed = num_packed - (layers_loaded * parts_per_layer);
		var parts_loaded_in_box = num_packed % rack.rack_parts_per_box;

		c.addClass('rack');
		c.css('width', (100 / all_data.packout.container_info.length) + '%');
		c.css('left', (100 / all_data.packout.container_info.length * i) + '%');
		//c.html(all_data.packout.container_info[i].name);

		var t = $('<TABLE />').addClass('table box-layer').appendTo(c);  //top down view
		var head = $('<THEAD />');
		var th   = $('<TH />');
		th.html(rack.name + ' :: TOP LAYER (' + (layers_loaded + 1) + ') VIEW');
		th.attr('colspan', rack.rack_boxes_across);
		head.append(th);
		t.append(head);

		var b = $('<TBODY />').appendTo(t);
		var showed_loading = false;

		for (var j = 0; j < rack.rack_boxes_deep; j++) {
			var r = $('<TR />').appendTo(b);

			for (var k = 0; k < rack.rack_boxes_across; k++) {
				var td = $('<TD />').appendTo(r);
				td.css('width', (100 / rack.rack_parts_per_box) + '%');

				for (var l = 0; l < rack.rack_parts_per_box; l++) {
					holes++;
				}

				if (holes <= top_layer_parts_packed) {
					td.html('LOADED').addClass('loaded');
				} else {
					if (holes <= (top_layer_parts_packed + rack.rack_parts_per_box - parts_loaded_in_box)) {
						td.html(parts_loaded_in_box + ' of ' + rack.rack_parts_per_box).removeClass('loaded').addClass('loading');
					} else {
						td.html('&nbsp;');
					}
				}
			}
		}

		var r = $('<TR />').appendTo(t);
		var c2 = $('<TD />').appendTo(r).attr('colspan', 3);
		c2.html('<span class="pull-left">In rack: <span class="badge bigbadge">' + num_packed + ' of ' + parts_per_rack + '</span>' +
				  '</span><span class="pull-right">In box: <span class="badge bigbadge">' + parts_loaded_in_box + ' of ' + rack.rack_parts_per_box + '</span></span>');
		c.append(t);


		var t = $('<TABLE />').addClass('table').appendTo(c);  //side down view
		var head = $('<THEAD />');
		var th   = $('<TH />');
		th.html(rack.name + ' :: SIDE VIEW (LAYERS LOADED/LOADING)');
		head.append(th);
		t.append(head);

		var b = $('<TBODY />').appendTo(t);

		for (var j = 0; j < rack.rack_layers; j++) {
			var r = $('<TR />');

			var td = $('<TD />');
			if ((rack.rack_layers - j) <= layers_loaded) {
				td.html('<span class="pull_left pill">LAYER ' + (rack.rack_layers - j) + '</span> LOADED').addClass('loaded');
			} else {
				if ((rack.rack_layers - j) == (layers_loaded + 1)) {
					td.html('<span class="pull_left pill">LAYER ' + (rack.rack_layers - j) + '</span> <I>LOADING</I>').addClass('loading');
				} else {
					td.html('<span class="pull_left pill">LAYER ' + (rack.rack_layers - j) + '</span>');
				}
			}

			r.append(td);

			b.append(r);
		}

		//buttons:
		var d = $('<DIV />').appendTo(c);
		
		var data = {"machine_group_ID":all_data.packout.container_info[i].ID, "group_desc":all_data.packout.container_info[i].name};
		var btn = $('<BUTTON />').addClass('btn btn-success dt-button btn-insert-part').html('<span class="glyphicon glyphicon-plus"></span> Manually Insert Part').appendTo(d).data(data);
		var btn = $('<BUTTON />').addClass('btn btn-primary dt-button btn-remove-part').html('<span class="glyphicon glyphicon-minus"></span> Remove Part').appendTo(d).data(data);
		var btn = $('<BUTTON />').addClass('btn btn-warning dt-button btn-reset-rack').html('<span class="glyphicon glyphicon-refresh"></span> Reset Rack').appendTo(d).data(data);
		
		if (all_data.barcode_status) {
			var v = all_data.barcode_status.statuses[i][0].value;
			console.log('BC status for ' + i + ' = ' + v);
			if (v == 1) {
				$('<SPAN />').addClass('label label-success label-big label-barcode-status').html('BARCODE STATUS: OK').appendTo(d).data(data);
			}
			if (v > 1) {
				$('<SPAN />').addClass('label label-danger label-big label-barcode-status').html('BARCODE STATUS: FAILED').appendTo(d).data(data);
			}
		}
	}
}

//
function refreshData() {
	var arr = ['machine', 'production', 'packout', 'time_and_shift', 'defects', 'PLC_status', 'shift_summary', 'cycle', 'version', 'work_instructions', 'work_instruction_signoffs',
	'quality_alerts', 'safety_alerts', 'alert_signoffs', 'barcode_status'];

	$('#sp_loading_status').html('Loading all data...');
	$.ajax(ajax_procs.packout, {
		"data":{
			"action":"load_data",
			"data_types":arr.join('~'),
			"my_IP":my_IP
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			initial_data_loaded = true;
			for (x in jso) {
				all_data[x] = jso[x]||[];
			}

			setCurrentTime();
			updateTime();

			updateProductionSummary();

			updateShiftCounts();
			//updateCycleData();

			//updateDowtimeTop5();
			//updateOpenDowntime();

			//updateDefectTop5();

			//startCycleTimer();
			drawPackout();
			
			updateBarcodeStatus();

			var d = new Date();
			$('#sp_last_update').html('Last update: ' + all_data.time_and_shift[0].cur_time);

			if (all_data.machine[0].master_machine_ID != null) {
				if (all_data.machine[0].master_machine_ID != null) {
					if (all_data.machine[0].master_operator_ID != null) {
						if (!is_logged_in) {
							var jso_op = {};
							jso_op['operator'] = [{"ID":all_data.machine[0].master_operator_ID, "name":all_data.machine[0].master_operator_name, "login_time":all_data.machine[0].master_operator_login_time}];
							afterProcessLogin(jso_op)
						}
					} else {
						if (is_logged_in) {
							doLogout();
						}
					}
				}
			}

			if (current_version == '') {
				current_version = all_data.version;
			} else {
				if (current_version != all_data.version) {
					//reload js
					document.location = document.location;
				}
			}
		}
	});
}

function updateBarcodeStatus() {
	
}

//GENERIC FUNCTIONS

function getColorClass(actual_value, target_value) {
	if (actual_value <= target_value) {
		return 'success';
	}

	if (actual_value >= (target_value * 1.15)) {
		return 'danger';
	}

	if (actual_value > (target_value)) {
		return 'warning';
	}

	return '';
}

function getThumb(actual_value, target_value) {
	if (actual_value <= target_value) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (actual_value >= (target_value * 1.15)) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';
}

function getColorClassForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <=  -0.1) {
		return 'success';
	}

	if (percent_under >= 0) {
		return 'danger';
	}

	if (percent_under > -0.1) {
		return 'warning';
	}

	return '';
}

function getThumbForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <=  -0.1) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (percent_under >= 0) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';

}

function formatHour(h) {
	var suff = ((h >= 12)?'PM':'AM');
	h = ((h > 12)?h-12:h);
	h = ((h == 0)?12:h);

	return h + ' ' + suff;
}

function formatTime(t) {
//	var arr = t.split(':');
//	var suff = ((arr[0] >= 12)?'PM':'AM');
//	arr[0] = ((arr[0] > 12)?arr[0]-12:arr[0]);
//	arr[0] = ((arr[0] == 0)?12:arr[0]);
//
//	return arr[0] + ':' + arr[1] + ' ' + suff;
	return Date.Format(t, 'XX');
}

function formatMinutes(x) {
	var minutes = Math.floor(x);
	var seconds = Math.round((x - minutes) * 60);
	if (seconds < 10) { seconds = '0' + seconds.toString(); }

	if (minutes > 60) {
		var hours = Math.floor(minutes / 60.0);
		minutes = minutes - (hours * 60);
		if (minutes < 10) { minutes = '0' + minutes.toString(); }

		return hours + ':' + minutes + ':' + seconds;
	} else {
		return minutes + ':' + seconds;
	}
}

function formatRelativeTime(t) {

}

//END OF GENERIC FUNCTIONS