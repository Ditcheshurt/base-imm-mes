var operator = {};
var ajax_procs = {
	"main": "../ajax_processors/displays/training_alerts.php",
	"parameters":"./ajax_processors/parameters.php"
};
var initial_load_data = true;
var tmr_loadMTP;
var current_version = '';
var machine_config = {};
var current_machine = {"ID":0};
var current_tool = {"ID":0};
var current_parts = [];
var current_param_chart;
var current_video = 0;

$(function () {
	$(document).ajaxError(function (a, b, c) {
		$('#div_network_error').modal('show');
		console.log('MAJOR AJAX ERROR: ' + b.responseText);
		$.growl({
			"title": " ERROR: ",
			"message": "Major error processing request: " + b.statusText,
			"icon": "glyphicon glyphicon-exclamation-sign"
		}, {
			"type": "danger",
			"allow_dismiss": false,
			"placement": {
				"from": "top",
				"align": "right"
			},
			"offset": {
				"x": 10,
				"y": 80
			}
		});
	});

	// if a modal is shown give it focus
	$('.modal').on('shown.bs.modal', function() {
		$(this).focus();
	});

	$('#modal_select_machine').on('click', function() {
			event.preventDefault();
			loadConfig();
			selectMachine();
	});
	
	$('#ul_select_machines').on('click', '.btn-machine-select', function() {
		event.preventDefault();
		var d = $(this).data();

		current_tool = machine_config.active_tools[machine_config.active_tools.findIndex(x => x.tool_ID == d.current_tool_ID)];

		current_parts = [];
		for(i=0; i<machine_config.active_parts.length; i++) {
			if(machine_config.active_parts[i].ID == d.ID && machine_config.active_parts[i].part_ID != 0) {
				current_parts.push(machine_config.active_parts[i]);
			}
		}
		
		if (current_tool.tool_ID > 0 && current_parts.length > 0) {
			console.log("OK to set current_machine")
			current_machine = d;

			m_sp = $('SPAN#sp_machine_name');m_sp.empty();
			t_sp = $('SPAN#sp_current_tool');t_sp.empty();
			p_sp = $('SPAN#sp_current_part_number');p_sp.empty();

			m = $('<SPAN />').addClass('badge').addClass('badge-info').html(current_machine.machine_name);
			m.appendTo(m_sp);

			tool_name = current_tool.tool_desc + " (" + current_tool.short_desc + ")";
			t = $('<SPAN />').addClass('badge').addClass('badge-info').html(tool_name);
			t.appendTo(t_sp);

			for(i=0; i<current_parts.length; i++) {
				p = $('<SPAN />').addClass('badge').addClass('badge-info').html(current_parts[i].part_number);
				p.appendTo(p_sp);
				if(i < current_parts.length-1){$('<br />').appendTo(p_sp);}
				console.log(current_parts[i].part_video);
			}
						
			$.growl(
				{"title":" Machine Selected ", "message":"Selected machine: " + d.machine_name, "icon":"glyphicon glyphicon-exclamation-sign"},
				{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":2000}
			);

			$('#div_select_machine_modal').modal('hide');
			initial_load_data = false;
			loadMTPData();
			loadVideoContent(current_parts[current_video].part_video);
		} else {
			console.log("No tool configured for " + d.machine_name);
			$.growl(
				{"title":" Machine Select Error: ", "message":"No tool currently configured for " + d.machine_name, "icon":"glyphicon glyphicon-exclamation-sign"},
				{"type":"warning", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":5000, "z_index":10000}
			);
		}
	});

	$('#div_params').on('click', '.btn-select-data-point', function(e) {
		btn = $(this);
		
		$('BUTTON.btn-select-data-point').each(function(idx) {
			if ($(this).attr('id') == btn.attr('id')) {
				$(this).toggleClass('btn-xs').toggleClass('btn-sm');
				if ($(this).hasClass('btn-sm')) {
					generateParameterChart($(this).data());
					$('DIV#div_history').css('height','450px');
				} else {
					$('#div_chart_' + $(this).data('ID')).remove();
					$('DIV#div_history').css('height','0px');
				}
			} else {
				$(this).removeClass('btn-sm').addClass('btn-xs');
				$('#div_chart_' + $(this).data('ID')).remove();
			}
		});
		
		console.log($(this).attr('id'));
	});

	loadConfig();
});

function loadConfig() {
	if (initial_load_data) {
		$('#div_loading').modal('show');
	}

	var arr = ['version', 'all_machines', 'active_tools', 'active_parts', 'data_points'];

	$('#sp_loading_status').html('Loading configuration...');
	$.ajax(ajax_procs.main, {
		"data": {
			"action": "get_config",
			"data_types": arr.join('~')
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			machine_config = jso;

			for (x in machine_config) {
				if (!machine_config[x]) {
					machine_config[x] = [];
				}
			}
			
			if (machine_config.all_machines.length > 0) {
				if (machine_config.all_machines.length == 1 || current_machine.ID != 0) {
					//normal case...just one machine per station so keep going
					if (current_machine.ID == 0) {
						$('#sp_machines').html('Machine Center');
						current_machine = machine_config.all_machines[0];
					}
				} else {
					// find the default machine
					$.each(machine_config.all_machines, function(k, v) {
						if (v.is_station_default) {
							current_machine = v;
							initial_load_data = false;
							laodMTPData();
							return;
						}
					});
					// no default machine
					if (current_machine.ID == null || current_machine.ID == 0) {
						$('#sp_machine_name').html('<span class="badge badge-info">' + machine_config.all_machines.length + ' available');
						//multiple machines, so stop...need to select machine
						selectMachine();
						return;
					}
				}
			} else {
				//error....not set up
			}

			//set machine name
			if (current_machine.ID > 0 || machine_config.all_machines.length == 1) {
				$('#sp_machine_name').html('<span class="badge badge-info">' + current_machine.machine_name + '</span>');
			} else {
				var b = $('<BUTTON />').addClass('btn btn-info btn-select-machine');
				b.html(machine_name);
				b.on('click', selectMachine);
				$('#sp_machine_name').html(b);
			}

			//setCurrentTime();
			//updateTime();

			//updateAlerts();
			//updateWorkInstructions();

			if (current_version == '') {
				current_version = machine_config.version;
			} else {
				if (current_version != machine_config.version) {
					//reload js
					document.location = document.location;
				}
			}
			$('#sp_version').html(current_version);

			$('#div_loading').modal('hide');
		}
	});
}

function loadMTPData() {
	console.log("Need to load all data for " + current_machine.machine_name + " (ID: " + current_machine.ID + ")");
	
	current_chart_param = $('BUTTON.btn-select-data-point.btn-sm').attr('id');
	
	dp_d = $('DIV#div_params');dp_d.empty();
	
	for(i=0; i<machine_config.data_points.length; i++) {
		b = $('<BUTTON />').addClass('btn').addClass('btn-default').addClass('btn-xs').addClass('btn-select-data-point').attr('id',machine_config.data_points[i].ID).css('margin','5px');
		b.html(machine_config.data_points[i].data_point_desc).data(machine_config.data_points[i]);
		b.appendTo(dp_d);
	}
	
	$.ajax(ajax_procs.main, {
		"data": {
			"action": "get_last_shot_data",
			"machine_ID": current_machine.ID,
			"tool_ID": current_tool.tool_ID
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			var shot_data = jso.last_shot_data;
			var ctrl_lmts = jso.ctrl_lmts;
			for(i=0; i<shot_data.length; i++) {
				dp_b = $('BUTTON.btn-select-data-point#'+shot_data[i].data_point_ID);
				dp_b.removeClass('btn-default').removeClass('btn-success').removeClass('btn-error');
				if (shot_data[i].data_point_ID == current_chart_param) {
					dp_b.removeClass('btn-xs').addClass('btn-sm');
					generateParameterChart(dp_b.data());
				}
				if (shot_data[i].ctrl_limit_exceeded != 0) {
					dp_b.addClass('btn-danger');
				} else {
					dp_b.addClass('btn-success');
				}
			}
		}
	});
	
	$.getJSON(ajax_procs.main, {
			"action":"get_tool_chg_status",
			"machine_ID":current_machine.ID,
			"tool_ID":current_tool.tool_ID
		}, function (jso) {
			var jso = jso || [];
			var dtc = $("#div_tool_change_timer");
			
			dtc.empty();
			
			if (jso.length > 0) {
				var t = jso[0];
				if (t.tool_change_ID > 0) {
					// currently in a tool change
					dtc.append(
						$('<SPAN />')
						.addClass('glyphicon glyphicon-wrench')
					).append(
						$('<SPAN />')
						.html(' Overall: '+t.time_since_start.formatSeconds())
					).append(
						$('<SPAN />')
						.html(' Tool change: '+t.tool_change_time.formatSeconds())
					).append(
						$('<SPAN />')
						.html(' Downtime: '+t.downtime.formatSeconds())
					);
				} else {
					// not in a tool change
					dtc.append(
						$('<SPAN />')
						.addClass('glyphicon glyphicon-wrench')
					).append(
						$('<SPAN />')
						.html(' No Current Tool Change')
					);
				}
			}
		}
	);
	
	if (!tmr_loadMTP) {
		tmr_loadMTP = setInterval(loadMTPData, 10000);
	}
	$('#div_loading').modal('hide');
}

function setCurrentTime() {
	if (time_timer) {
		clearInterval(time_timer);
		time_timer = null;
	}

	time_timer = setInterval(incrementTime, 1000);
}

function incrementTime() {

	if (machine_config.time.length > 0) {
		machine_config.time[0].cur_time = Date.DateAdd("s", 1, machine_config.time[0].cur_time);
		updateTime();

		if (machine_config.time[0].cur_time.getSeconds() % 5 == 0) {
			clearInterval(time_timer);
			time_timer = null;
			refreshData();
		}
	} else {
		clearInterval(time_timer);
		time_timer = null;
		refreshData();
	}
}

function updateTime() {

	if (machine_config.time.length > 0) {
		//set time and shift
		$('#sp_time').html(formatTime(machine_config.time[0].cur_time));
		$('#sp_shift').html('Shift ' + machine_config.time[0].cur_shift);

		if (last_shift != 0) {
	/*		if (machine_config.time[0].cur_shift != last_shift) {
				//logout
				last_shift = 0;
				operator = {};
				$('#div_operator').slideUp();
				doLogin();
			}*/
		} else {
			last_shift = machine_config.time[0].cur_shift;
		}
	}
}

function refreshData() {
	var arr = ['machine', 'production', 'downtime_summary', 'open_downtime', 'time',
	'defects', 'PLC_status', 'shift_summary', 'cycle', 'version', 'work_instructions', 'work_instruction_signoffs',
	'quality_alerts', 'safety_alerts', 'alert_signoffs', 'labeling', 'schedule'];

	$('#sp_loading_status').html('Loading all data...');
	$.ajax(ajax_procs.main, {
		"data": {
			"action": "load_data",
			"data_types": arr.join('~'),
			"my_IP": my_IP,
			"machine_ID":current_machine.ID
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			initial_load_data = true;
			for (x in jso) {
				machine_config[x] = jso[x] || [];
			}

			setCurrentTime();
			updateTime();

			updateProductionSummary();

			updateShiftCounts();
			updateCycleData();

			updateDowtimeTop5();
			updateOpenDowntime();

			//updateDefectTop5();

			updateAlerts();

			updateWorkInstructions();

			updateLabelingQueue();
			if (is_logged_in) getCurrentToolPartConfiguration();

			updateSchedule();

			//startCycleTimer();

			var d = new Date();
			$('#sp_last_update').html('Last update: ' + machine_config.time[0].cur_time);

			if (machine_config.machine[0].master_machine_ID != null) {
				if (machine_config.machine[0].master_machine_ID != null) {
					if (machine_config.machine[0].master_operator_ID != null) {
						if (!is_logged_in) {
							var jso_op = {};
							jso_op['operator'] = [{
								"ID": machine_config.machine[0].master_operator_ID,
								"name": machine_config.machine[0].master_operator_name,
								"login_time": machine_config.machine[0].master_operator_login_time
							}];
							afterProcessLogin(jso_op)
						}
					} else {
						if (is_logged_in) {
							doLogout();
						}
					}
				}
			}

			if (current_version == '') {
				current_version = machine_config.version;
			} else {
				if (current_version != machine_config.version) {
					//reload js
					document.location = document.location;
				}
			}
		}
	});
}

//MACHINE SELECTION
function selectMachine() {
	current_machine = {"ID":0};

	var u = $('#ul_select_machines').html('');

	for (var i = 0; i < machine_config.all_machines.length; i++) {
		var l = $('<LI />');
		var b = $('<BUTTON />').addClass('btn btn-machine-select').data(machine_config.all_machines[i]);

		b.html('<div class="panel panel-primary">' +
				 '	<div class="panel-body">' + machine_config.all_machines[i].machine_name + '</div>' +
				 '</div>');
		l.append(b);
		u.append(l);
	}

	$('#div_select_machine_modal').modal('show');
}
//END OF MACHINE SELECTION

//GENERIC FUNCTIONS
function getColorClass(actual_value, target_value) {
	if (actual_value <= target_value) {
		return 'success';
	}

	if (actual_value >= (target_value * 1.15)) {
		return 'danger';
	}

	if (actual_value > (target_value)) {
		return 'warning';
	}

	return '';
}

function getThumb(actual_value, target_value) {
	if (actual_value <= target_value) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (actual_value >= (target_value * 1.15)) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';
}

function getColorClassForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return 'success';
	}

	if (percent_under >= 0) {
		return 'danger';
	}

	if (percent_under > -0.1) {
		return 'warning';
	}

	return '';
}

function getThumbForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (percent_under >= 0) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';

}

function formatHour(h) {
	var suff = ((h >= 12) ? 'PM' : 'AM');
	h = ((h > 12) ? h - 12 : h);
	h = ((h == 0) ? 12 : h);

	return h + ' ' + suff;
}

function formatTime(t) {
	//	var arr = t.split(':');
	//	var suff = ((arr[0] >= 12)?'PM':'AM');
	//	arr[0] = ((arr[0] > 12)?arr[0]-12:arr[0]);
	//	arr[0] = ((arr[0] == 0)?12:arr[0]);
	//
	//	return arr[0] + ':' + arr[1] + ' ' + suff;
	return Date.Format(t, 'XX');
}

function formatMinutes(x) {
	var minutes = Math.floor(x);
	var seconds = Math.round((x - minutes) * 60);
	if (seconds < 10) {
		seconds = '0' + seconds.toString();
	}

	if (minutes > 60) {
		var hours = Math.floor(minutes / 60.0);
		minutes = minutes - (hours * 60);
		if (minutes < 10) {
			minutes = '0' + minutes.toString();
		}

		return hours + ':' + minutes + ':' + seconds;
	} else {
		return minutes + ':' + seconds;
	}
}

function formatRelativeTime(t) {

}

Number.prototype.formatSeconds = function() {
    var hh = Math.floor(this/3600).toString().padStart(2,'0');
    var mm = Math.floor((this%3600)/60).toString().padStart(2,'0');
    var ss = Math.floor((this%3600)%60).toString().padStart(2,'0');
	return hh+':'+mm+':'+ss;
}
//END OF GENERIC FUNCTIONS

function generateParameterChart(d) {
	var data = {};
	var ucl = 1000;
	var lcl = 0;
	var units = '';
	
	var cont = $('#div_chart_' + d.ID);
	if (cont.length == 0) {
		cont = $('<div />').attr('id', 'div_chart_' + d.ID).appendTo($('#div_parameter_chart')).addClass('parameter-container');
	}
	
	$.getJSON(ajax_procs.main, {
			"action":"get_data_point_values",
			"machine_ID": current_machine.ID,
			"tool_ID": current_tool.tool_ID,
			"data_point_name":d.data_point_name
		},
		function(jso){
			data = jso.data_pts;
			if (jso.ctrl_lmts[0].ucl) {
				ucl = jso.ctrl_lmts[0].ucl;
			} else {
				ucl = d.ucl;
			}
			if (jso.ctrl_lmts[0].lcl) {
				lcl = jso.ctrl_lmts[0].lcl;
			} else {
				lcl = d.lcl;
			}
			
			units = jso.ctrl_lmts[0].data_point_desc + ' (' + jso.ctrl_lmts[0].units + ')';
			
			cont.highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: d.data_point_desc,
					enabled: true
				},
				credits: {
					enabled: false
				},
				xAxis: {
					categories: [],
					labels: {enabled: false},
					tickInterval: 20,
					visible: false
				},
				yAxis: {
					plotBands: [
						{
							from: ucl,
							to: 10000,
							color: 'rgba(255, 0, 0, 0.1)'
						},
						{
							from: -10000,
							to: lcl,
							color: 'rgba(255, 0, 0, 0.1)'
						}
					],
					title: {
						enabled: true,
						text:units.length>0?units:'Values',
						style:{
							fontWeight:'bold'
						}
					},
				},
				legend: {
					shadow: true,
					enabled: true
				},
				tooltip: {
					shared: true
				},
				plotOptions: {
					line: {
						grouping: false,
						shadow: true,
						borderWidth: 0
					}
				},
				series: [{
					name: units.length>0?units:'Values',
					color: 'rgba(22,170,2,.7)',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					point: {
						events: {
							mouseOver: function() {
								console.log('x: ' + this.x + ', y: ' + this.y);
								console.log(this);
							}
						}
					}
				},{
					name: 'Upper Limit',
					color: 'red',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				},{
					name: 'Lower Limit',
					color: 'red',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				}]
			});


			var arr = [];
			var arr2 = [];
			var arr3 = [];

			for (var i = 0; i < data.length; i++) {
				var v = data[i].data_point_value;
				var c = 'limegreen';
				if (v < lcl || v > ucl) {
					c = 'red';
				}
				arr.push({y:parseFloat(v.toFixed(3)), name:(i + ' shots ago'), color:c});
				arr2.push(ucl);
				arr3.push(lcl);
			}
			var chart = cont.highcharts();
			chart.setTitle(d.data_point_desc);
			chart.series[0].setData(arr);
			chart.series[1].setData(arr2);
			chart.series[2].setData(arr3);
			chart.redraw();
		});

}

//video player stuff
loadVideoContent = function (vid_url) {
	var wiv_plr = $('#wiv_video');
	var vid_src = $('#wiv_video_src');
	
	wiv_plr[0].removeEventListener('ended',afterVideoEnded);
	
	vid_src[0].src = vid_url;
	wiv_plr[0].load();
	
	afterLoadVideoContent();
}
afterLoadVideoContent = function () {
	var plr_bdy = $('#modal_body_player');
	var wiv_plr = $('#wiv_video');

	$('#wiv_part_name').html(current_parts[current_video].part_desc);
	$('#wiv_part_number').html(current_parts[current_video].part_number);
	
	playVideoContent();
}
playVideoContent = function () {
	var wiv_plr = $('#wiv_video');

	wiv_plr[0].play();
	
	wiv_plr[0].addEventListener('ended',afterVideoEnded,false);
}
afterVideoEnded = function () {
	$('wiv_vid_play').show();
	
	if (current_video < current_parts.length-1) {
		current_video++;
	} else {
		current_video = 0;
	}
	loadVideoContent(current_parts[current_video].part_video);
}