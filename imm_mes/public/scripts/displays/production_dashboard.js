var processor = '/imm_mes/ajax_processors/displays/production_dashboard.php';
var system_ID = location.search.substring(11, location.search.length);
var oee_green = 100;
var oee_red = 50;
var image_height_over_width = (253 / 637);
var machine_selected = -1;
var machine_selected_max = 0;
var container_height = (window.innerHeight - 60 - 39);
var container_width = (window.innerWidth) * 0.67;

function getColorClassForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return 'success';
	}

	if (percent_under >= 0) {
		return 'bad';
	}

	if (percent_under > -0.1) {
		return 'warning';
	}

	return '';
}

function getThumbForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (percent_under >= 0) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';

}

function getColorClass(actual_value, target_value) {
	if (actual_value <= target_value) {
		return 'success';
	}

	if (actual_value >= (target_value * 1.15)) {
		return 'bad';
	}

	if (actual_value > (target_value)) {
		return 'warning';
	}

	return '';
}

function getThumb(actual_value, target_value) {
	if (actual_value <= target_value) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (actual_value >= (target_value * 1.15)) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';
}

function make_chart() {
	var chart_container_height = (window.innerHeight - 60 - 39);
	var chart_container_width = window.innerWidth;
	$('#div_production_summary').height(chart_container_height * 0.67);
	$('#div_production_summary').width(chart_container_width * 0.32);
	$('#div_production_summary').highcharts({
		chart: {
			type: 'column',
			spacingBottom: 0,
			spacingTop: 0,
			backgroundColor: '#000000'

		},
		title: {
			text: '',
			enabled: false
		},
		credits: {
			enabled: false
		},
		xAxis: {
			categories: [],
			labels:{
				style:{
					color: '#FFF'
				}
			}
		},
		yAxis: [{
			min: 0,
			endOnTick: false,
			title: {
				text: 'Target',
				enabled: false
			},
			labels:{
				style:{
					color: '#FFF'
				}
			}
		}, {
			endOnTick: false,
			title: {
				text: 'Actual',
				enabled: false
			},
			labels:{
				style:{
					color: '#FFF'
				}
			}
		}],
		legend: {
			shadow: false,
			enabled: false
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0,
			}
		},
		series: [{
			name: 'Target',
			color: 'rgba(126,126,126,.7)',
			data: [],
			pointPadding: 0.0,
			pointPlacement: 0
		},{
			name: 'Actual',
			color: 'rgba(22,170,2,.7)',
			data: [],
			pointPadding: 0.1,
			pointPlacement: 0,
			dataLabels: { enabled: true }
		}]
	});
}


function updateProductionSummary(jso, machine_name) {
	//update production summary chart
	var chart = $('#div_production_summary').highcharts();
	var hour_labels = [];
	var hour_data = [];
	var hour_targets = [];
	//var cur_hour = parseInt(all_data.time_and_shift[0].cur_time.split(':')[0]);
	var hourly_summary_length = jso == null ? 0 : jso.length;

	for (var i = 0; i < 8 && i < hourly_summary_length; i++) {
		hour_labels.push(jso[i].interval_desc);
		hour_data.push(jso[i].part_qty);
		hour_targets.push(jso[i].part_goal); //part_target_qty);
	}
	chart.xAxis[0].setCategories(hour_labels, false);
	chart.series[0].setData(hour_targets);
	chart.series[1].setData(hour_data);

	// set red if under target
	for (var i = 0; i < 8 && i < hourly_summary_length; i++) {
		if (chart.series[0].data.length == 0) {
			return;
		}
		var target = chart.series[0].data[i].y;
		var built = chart.series[1].data[i].y;
		if (built < target) {
			$($($('.highcharts-series')[1]).children()[i]).attr('fill', 'rgba(203, 45, 13, 0.7)');
		} else {
			$($($('.highcharts-series')[1]).children()[i]).attr('fill', 'rgba(22,170,2,.7)');
			
		}
	}
	var title = {
		align: "center",
		text: "Machine Name: " + machine_name,
		style: {"color": "#FFFFFF"}
	};

	chart.setTitle(title, {}, true);

}

function load_machines(){
$.ajax({
			"url": processor,
			"type": "GET",
			"dataType": "json",
			"cache": false,
			"data": {
				"action": "get_machines",
				"system_ID":system_ID,
			},
			"success": function(jso) {
				//set size of image container
				$('#machine_image_container').remove();
				$('#header').after('<table id="machine_image_container"></table>');
				$('#machine_image_container').width(container_width).height(container_height);
				// get layout for images. how many rows and how many columns
				var num_machines = jso.length;
				machine_selected_max = jso.length - 1;
				//var num_rows = Math.ceil(Math.sqrt(num_machines));
				var num_rows = num_machines;
				var num_cols = Math.ceil(num_machines / num_rows);
				var multiplyer = 1;
				var doesnt_fit = true;
				while(doesnt_fit){
					var image_width = (container_width / num_cols) * multiplyer;
					var image_height = image_height_over_width * image_width;
					if((image_height * num_rows) > container_height){
						multiplyer = multiplyer - .05;
					}
					else{
						doesnt_fit = false;
					}

				}
				//populate the image container
				var image_html = '<tbody>';
				for(var i = 0; i < num_rows; i++){
						image_html += '<tr class="dont_wrap">';
					for(var j = 0; j < num_cols; j++){
						var overall_oee = 0;
						var performance_oee = 0;
						var quality_oee = 0;
						var available_oee = 0;
						var machine_ID = jso[(num_cols * i) + j].ID;
						$.ajax({
							"url": processor,
							"type": "GET",
							"dataType": "json",
							"cache": false,
							"data": {
								"action": "get_machine_overall_oee",
								"system_ID":system_ID,
								"machine_ID":machine_ID ,
							},
							"success": function(jso) {
								if(jso){
									var overall_oee_percent = jso[0].overall_oee * 100;
									
									$('#' + jso[0].machine_ID.toString()).siblings('.machine_overall_oee_div').html(overall_oee_percent.toFixed(1) + '%'); 
								}		
							}

						});

						if(((num_cols * i) + j < jso.length) && jso[(num_cols * i) + j].up){ //press is up
							image_html += '<td class="single_image_container"><img id="' + jso[(num_cols * i) + j].ID + 
							'" src="../../public/images/press_up.png"></img>' + 
							'<div class="machine_name_div">' + jso[(num_cols * i) + j].machine_name + '</div>' + 
							'<div class="machine_overall_oee_div">' + overall_oee.toString() + '%' + '</div></td>';
								
						}
						else if(((num_cols * i) + j < jso.length) && !(jso[(num_cols * i) + j].up)){ //press is down
							image_html += '<td class="single_image_container"><img id="' + jso[(num_cols * i) + j].ID + 
							'" src="../../public/images/press_down.png"></img>' + 
							'<div class="machine_name_div">' + jso[(num_cols * i) + j].machine_name + '</div>' + 
							'<div class="machine_overall_oee_div">' + overall_oee.toString() + '%' + '</div></td>';
						}
					}
					image_html += '</tr>';	
				}
				image_html += '</tbody>';
				
				$('#machine_image_container').append(image_html);
				for(var i = 0; i < jso.length; i++){
					$('#' + jso[i].ID).width(image_width * .99);
				}

				$('.single_image_container').width(image_width * .99);
				

				machine_selected++;
				if(machine_selected > machine_selected_max){
					machine_selected = 0;
				}
				$($('.single_image_container')[machine_selected]).addClass("machine_selected");
				var selected_machine_ID = $($('.single_image_container')[machine_selected]).children('img').attr('id');
				var selected_machine_name = $($('.single_image_container')[machine_selected]).children('.machine_name_div').html();
				$.ajax({
							"url": processor,
							"type": "GET",
							"dataType": "json",
							"cache": false,
							"data": {
								"action": "get_machine_overall_oee",
								"system_ID":system_ID,
								"machine_ID":selected_machine_ID,
							},
							"success": function(jso) {
								
								if(jso){
									
									overall_oee = jso[0].overall_oee * 100;
									performance_oee = jso[0].performance_oee * 100;
									quality_oee = jso[0].quality_oee * 100;
									available_oee = jso[0].available_oee * 100;
									var overall_oee_string = overall_oee.toString();
									var performance_oee_string = performance_oee.toString();
									var quality_oee_string = quality_oee.toString();
									var available_oee_string = available_oee.toString();

									if(overall_oee_string.length >= 6){
										overall_oee_string = overall_oee_string.substring(0, 6);
									}
									if(performance_oee_string.length >= 6){
										performance_oee_string = performance_oee_string.substring(0, 6);
									}
									if(quality_oee_string.length >= 6){
										quality_oee_string = quality_oee_string.substring(0, 6);
									}
									if(available_oee_string.length >= 6){
										available_oee_string = available_oee_string.substring(0, 6);
									}

									var oee_divs = '<h1>' + selected_machine_name+ '</h1>' + '<div class="oee_numbers">Overall OEE: <span class="badge ' + getColorClassForPercent(jso[0].overall_oee, 0.85) + 
									'"> ' + overall_oee.toFixed(2) + '%' + getThumbForPercent(jso[0].overall_oee, 0.85) + '</span></div>' + 

									'<div class="oee_numbers">Performance OEE: <span class="badge ' + getColorClassForPercent(jso[0].performance_oee, 0.85) + 
									'"> ' + performance_oee.toFixed(2) + '%' + getThumbForPercent(jso[0].performance_oee, 0.85) + ' (' + jso[0].part_qty + '/' + jso[0].part_target_qty.toFixed(2) + ')' + '</span></div>' + 

									'<div class="oee_numbers">Quality OEE: <span class="badge ' + getColorClassForPercent(jso[0].quality_oee, 0.85) + 
									'"> ' + quality_oee.toFixed(2) + '%' + getThumbForPercent(jso[0].quality_oee, 0.85) + ' (' + jso[0].scrap_numerator + '/' + jso[0].scrap_denominator + ')' + '</span></div>' + 

									'<div class="oee_numbers">Available OEE: <span class="badge ' + getColorClassForPercent(jso[0].available_oee, 0.85) + 
									'"> ' + available_oee.toFixed(2) + '%' + getThumbForPercent(jso[0].available_oee, 0.85) + ' (' + (jso[0].total_minutes - jso[0].scheduled_minutes - jso[0].downtime_minutes).toFixed(2) + ') / (' + (jso[0].total_minutes - jso[0].scheduled_minutes).toFixed(2) + ')' + '</span></div>';

									$('#div_oee_numbers').empty();
									$('#div_oee_numbers').append(oee_divs);
									$('#div_oee_numbers').css("right", "2%");
								}
								else{

									var oee_divs = '<h1>' + selected_machine_name + '</h1>' + '<div class="oee_numbers">Overall OEE: ' + 'No OEE Reported For Shift' + '</div>' +  
									'<div class="oee_numbers">Performance OEE: ' + 'No OEE Reported For Shift' + '</div>' + 
									'<div class="oee_numbers">Quality OEE: ' + 'No OEE Reported For Shift' + '</div>' + 
									'<div class="oee_numbers">Available OEE: ' + 'No OEE Reported For Shift' + '</div>';
									$('#div_oee_numbers').empty();
									$('#div_oee_numbers').append(oee_divs);
									$('#div_oee_numbers').css("right", "3%");
								}		
							}

				});

				$.ajax({
							"url": processor,
							"type": "GET",
							"dataType": "json",
							"cache": false,
							"data": {
								"action": "get_machine_cycle_numbers",
								"system_ID":system_ID,
								"machine_ID":selected_machine_ID,
							},
							"success": function(jso) {
								
								if(jso){
									var last_cycle_time = jso[0].last_cycle_time;
									var avg_cycle_time = jso[0].avg_cycle_time;
									var target_cycle_time = jso[0].target_cycle_time;
									var lowest_cycle_time = jso[0].low_cycle_time;
									var part_queue = jso[0].part_queue;

									var cycle_time_divs = '<hr style="margin:10px;">' + 
									'<div id="cycle_time_container"><div class="cycle_times">Avg Cycle Time: <span class="badge ' + getColorClass(avg_cycle_time, target_cycle_time) + 
									'"> ' + avg_cycle_time.toFixed(2) + getThumb(avg_cycle_time, target_cycle_time) + '</span></div>' + 

									'<div class="cycle_times">Last Cycle Time: <span class="badge ' + getColorClass(last_cycle_time, target_cycle_time) + 
									'"> ' + last_cycle_time.toFixed(2) + getThumb(last_cycle_time, target_cycle_time) + '</span></div>' + 

									'<div class="cycle_times">Target Cycle Time: <span class="badge ' + 
									'"> ' + target_cycle_time.toFixed(2) + '</span></div>' + 

									'<div class="cycle_times">Lowest Cycle Time: <span class="badge ' + 
									'"> ' + lowest_cycle_time.toFixed(2) + '</span></div><div>' + 
									
									'<div class="cycle_times">Part Queue: <span class="badge ' + 
									'"> ' + part_queue + '</span></div><div>';
									$('#cycle_time_container').remove();
									$('#div_oee_numbers').append(cycle_time_divs);
									$('#cycle_times_container').css("right", "7%");

								}
								else{
									var cycle_time_divs = '<hr style="margin:10px;">' +
									'<div class="cycle_times">Avg Cycle Time: ' + 'No Cycles Reported' + '</div>' +  
									'<div class="cycle_times">Last Cycle Time: ' + 'No Cycles Reported' + '</div>' + 
									'<div class="cycle_times">Target Cycle Time: ' + 'No Cycles Reported' + '</div>' + 
									'<div class="cycle_times">Lowest Cycle Time: ' + 'No Cycles Reported' + '</div>';
									$('#cycle_time_container').remove();
									$('#div_oee_numbers').append(cycle_time_divs);
									$('#cycle_times_container').css("right", "3%");
								}		
							}

				});

				$.ajax({
							"url": processor,
							"type": "GET",
							"dataType": "json",
							"cache": false,
							"data": {
								"action": "get_hourly_production",
								"system_ID":system_ID,
								"machine_ID":selected_machine_ID,
							},
							"success": function(jso) {
								
								if(jso){
									updateProductionSummary(jso, selected_machine_name);
									
								}		
							}

				});
			}


		});
}
function keep_screen_live(){
	load_machines();
	setTimeout(keep_screen_live, 10000);
}


$(document).ready(function(){
	make_chart();
	keep_screen_live();


});