var operator = {};
var ajax_procs = {
	"login": "./ajax_processors/login.php",
	"main": "./ajax_processors/main.php",
	"downtime": "./ajax_processors/downtime.php",
	"defects": "./ajax_processors/defects.php",
	"production": "./ajax_processors/production.php",
	"PLC": "/PLCService/PLCWebReadWrite.asmx",
	"SPC":"./ajax_processors/spc.php",
	"parameters":"./ajax_processors/parameters.php"
};
var auth_op_id = 0;
var all_data = {};
var cycle_timer = null;
var cycle_timer_count = 0;
var last_cycle_timer = null;
var current_machine = {"ID":0};
var current_tool = {};
var time_timer = null;
var last_shift = 0;
var initial_data_loaded = false;
var current_version = '';
var machine_down_state_set = false;
var auth_check_callback;
var use_ojt = false; // added 5/3/2017 by R.Kahle - was causing an error in 'afterSelectTool' from being undefined
var use_tool_change = true;

$(function () {
	$(document).ajaxError(function (a, b, c) {
		$('#div_network_error').modal('show');
		console.log('MAJOR AJAX ERROR: ' + b.responseText);
		$.growl({
			"title": " ERROR: ",
			"message": "Major error processing request: " + b.statusText,
			"icon": "glyphicon glyphicon-exclamation-sign"
		}, {
			"type": "danger",
			"allow_dismiss": false,
			"placement": {
				"from": "top",
				"align": "right"
			},
			"offset": {
				"x": 10,
				"y": 80
			}
		});
	});

	// if a modal is shown give it focus
	$('.modal').on('shown.bs.modal', function() {
		$(this).focus();
	});

	$(document).on('click', '#btn_work_instructions', function () {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		displayCurrentWorkInstructions();
	});

	$('#div_instructions_carousel').on('slid.bs.carousel', function () {
		setTimeout(recordWorkInstructionSignoff, 500);
	});

	$('#div_instructions_carousel').on('shown.bs.modal', function () {
		recordWorkInstructionSignoff();
	});

	$('#ul_select_machines').on('click', '.btn-machine-select', function() {
		event.preventDefault();
		var d = $(this).data();
		$.growl(
			{"title":" Machine Selected ", "message":"Selected machine: " + d.machine_name, "icon":"glyphicon glyphicon-exclamation-sign"},
			{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
		);

		current_machine = d;
		$('#div_select_machine_modal').modal('hide');
		initial_data_loaded = false;
		loadAllData();

		if (is_logged_in) {
			setTimeout(afterLogin, 1000);
		}
	});

	$('#tbl_label TBODY').on('click', '.btn-record-good', function(){
		var part_ID = $(this).data('ID');
		var print_template_ID = $(this).data('print_template_ID');

		$(this).addClass('disabled');//.prop('disabled', true);

		recordGoodPart(part_ID, print_template_ID);

		getCurrentToolPartConfiguration();
	});

	$('#tbl_label TBODY').on('click', '.btn-record-all-good', function(){
		var part_ID = $(this).data('ID');

		$(this).addClass('disabled');//.prop('disabled', true);

		recordGoodPart(0, 0);

		getCurrentToolPartConfiguration();
	});

	$('#div_current_part DIV').hide();
	loadAllData();
	loadMachineLastcycle();

	setInterval(checkStuck, 30000);
	});

function loadMachineLastcycle() {
	if (last_cycle_timer) {
		clearTimeout(last_cycle_timer);
	}

	$.ajax(ajax_procs.main, {
		"data": {
			"action": "load_data",
			"data_types": "machine_last_cycle",
			"my_IP": my_IP,
			"machine_ID":current_machine.ID
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			$('#div_network_error').modal('hide');

			last_cycle_timer = setTimeout(loadMachineLastcycle, 500);
			if (!jso) {
				return;
			}
			if (!jso.machine_last_cycle) {
				return;
			}
			if (jso.machine_last_cycle.length == 0) {
				return;
			}
			if (!all_data.machine) {
				return;
			}
			if (all_data.machine.length == 0) {
				return;
			}

			all_data.machine[0].last_cycle_time_ago = jso.machine_last_cycle[0].last_cycle_time_ago || 0;
			cycle_timer_count = all_data.machine[0].last_cycle_time_ago;
			updateCycleTimer();
		},
		"error": function (jso) {
			$('#div_network_error').modal('show');
			last_cycle_timer = setTimeout(loadMachineLastcycle, 500);
		}
	});
}

function checkStuck() {
	if (time_timer == null) {
		console.log('DETECTED TIMER STUCK....reloading all data.');
		// $.growl({
			// "title": " WARNING: ",
			// "message": "Detected timer stuck.  Reloading all data...",
			// "icon": "glyphicon glyphicon-exclamation-sign"
		// }, {
			// "type": "warning",
			// "allow_dismiss": false,
			// "placement": {
				// "from": "top",
				// "align": "right"
			// },
			// "offset": {
				// "x": 10,
				// "y": 80
			// }
		// });

		refreshData();
	}
}

function loadAllData() {
	if (initial_data_loaded) {
		$('#div_loading').modal('show');
	}

	if (my_IP.length < 8) {
		alert('INVALID IP ADDRESS: ' + my_IP);
		return;
	}

	var arr = ['all_machines', 'machine', 'production', 'downtime_summary', 'open_downtime', 'time_and_shift',
	'defects', 'PLC_status', 'tools', 'parts', 'cycle', 'version', 'work_instructions', 'work_instruction_signoffs',
	'quality_alerts', 'safety_alerts', 'alert_signoffs', 'data_points', 'labeling', 'schedule'];

	$('#sp_loading_status').html('Loading all data...');
	$.ajax(ajax_procs.main, {
		"data": {
			"action": "load_data",
			"data_types": arr.join('~'),
			"my_IP": my_IP,
			"machine_ID":current_machine.ID
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			all_data = jso;

			for (x in all_data) {
				if (!all_data[x]) {
					all_data[x] = [];
				}
			}

			if (all_data.all_machines.length > 0) {
				if (all_data.all_machines.length == 1 || current_machine.ID != 0) {
					//normal case...just one machine per station so keep going
					if (current_machine.ID == 0) {
						$('#sp_machines').html('Machine Center');
						current_machine = all_data.all_machines[0];
					}
					if (!is_logged_in) {
						setTimeout(doLogin, 1000);
					}
				} else {
					// find the default machine
					$.each(all_data.all_machines, function(k, v) {
						if (v.is_station_default) {
							current_machine = v;
							if (!is_logged_in) {
								setTimeout(doLogin, 1000);
							}
							initial_data_loaded = false;
							loadAllData();
							return;
						}
					});
					// no default machine
					if (current_machine.ID == null || current_machine.ID == 0) {
						$('#sp_machines').html('Machine Center (' + all_data.all_machines.length + ' available)');
						//multiple machines, so stop...need to select machine
						selectMachine();
						return;
					}
				}
			} else {
				//error....not set up
			}


			var machine_name = all_data.machine.length == 0 ? '' : all_data.machine[0].machine_name;

			//set machine name
			if (all_data.all_machines.length == 1) {
				$('#sp_machine_name').html('<span class="badge badge-info">' + machine_name + '</span>');
			} else {
				var b = $('<BUTTON />').addClass('btn btn-info btn-select-machine');
				b.html(machine_name);
				b.on('click', selectMachine);
				$('#sp_machine_name').html(b);
			}

			setCurrentTime();
			updateTime();

			updateProductionSummary();

			updateShiftCounts();
			updateCycleData();

			updateDowtimeTop5();
			updateOpenDowntime();

			//updateDefectTop5();

			updateAlerts();

			updateWorkInstructions();

			updateLabelingQueue();

			if (current_version == '') {
				current_version = all_data.version;
			} else {
				if (current_version != all_data.version) {
					//reload js
					document.location = document.location;
				}
			}

			$('#sp_version').html(current_version);

			$('#div_loading').modal('hide');
		}
	});
}

function setCurrentTime() {
	if (time_timer) {
		clearInterval(time_timer);
		time_timer = null;
	}

	time_timer = setInterval(incrementTime, 1000);
}

function incrementTime() {

	if (all_data.time_and_shift.length > 0) {
		all_data.time_and_shift[0].cur_time = Date.DateAdd("s", 1, all_data.time_and_shift[0].cur_time);
		updateTime();

		if (all_data.time_and_shift[0].cur_time.getSeconds() % 5 == 0) {
			clearInterval(time_timer);
			time_timer = null;
			refreshData();
		}
	} else {
		clearInterval(time_timer);
		time_timer = null;
		refreshData();
	}
}

function updateTime() {

	if (all_data.time_and_shift.length > 0) {
		//set time and shift
		$('#sp_time').html(formatTime(all_data.time_and_shift[0].cur_time));
		$('#sp_shift').html('Shift ' + all_data.time_and_shift[0].cur_shift);

		if (last_shift != 0) {
	/*		if (all_data.time_and_shift[0].cur_shift != last_shift) {
				//logout
				last_shift = 0;
				operator = {};
				$('#div_operator').slideUp();
				doLogin();
			}*/
		} else {
			last_shift = all_data.time_and_shift[0].cur_shift;
		}
	}
}

//
function refreshData() {
	var arr = ['machine', 'production', 'downtime_summary', 'open_downtime', 'time_and_shift',
	'defects', 'PLC_status', 'shift_summary', 'cycle', 'version', 'work_instructions', 'work_instruction_signoffs',
	'quality_alerts', 'safety_alerts', 'alert_signoffs', 'labeling', 'schedule'];

	$('#sp_loading_status').html('Loading all data...');
	$.ajax(ajax_procs.main, {
		"data": {
			"action": "load_data",
			"data_types": arr.join('~'),
			"my_IP": my_IP,
			"machine_ID":current_machine.ID
		},
		"dataType": "json",
		"method": "POST",
		"success": function (jso) {
			initial_data_loaded = true;
			for (x in jso) {
				all_data[x] = jso[x] || [];
			}

			setCurrentTime();
			updateTime();

			updateProductionSummary();

			updateShiftCounts();
			updateCycleData();

			updateDowtimeTop5();
			updateOpenDowntime();

			//updateDefectTop5();

			updateAlerts();

			updateWorkInstructions();

			updateLabelingQueue();
			if (is_logged_in) getCurrentToolPartConfiguration();

			updateSchedule();

			//startCycleTimer();

			var d = new Date();
			$('#sp_last_update').html('Last update: ' + all_data.time_and_shift[0].cur_time);

			if (all_data.machine[0].master_machine_ID != null) {
				if (all_data.machine[0].master_machine_ID != null) {
					if (all_data.machine[0].master_operator_ID != null) {
						if (!is_logged_in) {
							var jso_op = {};
							jso_op['operator'] = [{
								"ID": all_data.machine[0].master_operator_ID,
								"name": all_data.machine[0].master_operator_name,
								"login_time": all_data.machine[0].master_operator_login_time
							}];
							afterProcessLogin(jso_op)
						}
					} else {
						if (is_logged_in) {
							doLogout();
						}
					}
				}
			}

			if (current_version == '') {
				current_version = all_data.version;
			} else {
				if (current_version != all_data.version) {
					//reload js
					document.location = document.location;
				}
			}
		}
	});
}

function updateLabelingQueue() {
	var q = 0;
	if (all_data.labeling.length > 0) {
		q = all_data.labeling[0].queue || 0;
	}

	$('#sp_labeling_queue').html(q);
	if (q > 0) {
		$('.btn-record-good,.btn-record-reject').removeClass('disabled');
	} else {
		$('.btn-record-good,.btn-record-reject').addClass('disabled');
	}
}

//MACHINE SELECTION
function selectMachine() {
	current_machine = {"ID":0};

	var u = $('#ul_select_machines').html('');

	for (var i = 0; i < all_data.all_machines.length; i++) {
		var l = $('<LI />');
		var b = $('<BUTTON />').addClass('btn btn-machine-select').data(all_data.all_machines[i]);

		b.html('<div class="panel panel-primary">' +
				 '	<div class="panel-body">' + all_data.all_machines[i].machine_name + '</div>' +
				 '</div>');
		l.append(b);
		u.append(l);
	}

	$('#div_select_machine_modal').modal('show');
}
//END OF PART/TOOL SELECTION

//GENERIC FUNCTIONS

function getColorClass(actual_value, target_value) {
	if (actual_value <= target_value) {
		return 'success';
	}

	if (actual_value >= (target_value * 1.15)) {
		return 'danger';
	}

	if (actual_value > (target_value)) {
		return 'warning';
	}

	return '';
}

function getThumb(actual_value, target_value) {
	if (actual_value <= target_value) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (actual_value >= (target_value * 1.15)) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';
}

function getColorClassForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return 'success';
	}

	if (percent_under >= 0) {
		return 'danger';
	}

	if (percent_under > -0.1) {
		return 'warning';
	}

	return '';
}

function getThumbForPercent(actual_value, target_value) {
	var percent_under = (target_value - actual_value) / target_value;
	if (percent_under <= -0.1) {
		return ' <span class="glyphicon glyphicon-thumbs-up"></span>';
	}

	if (percent_under >= 0) {
		return ' <span class="glyphicon glyphicon-thumbs-down"></span>';
	}

	return '';

}

function formatHour(h) {
	var suff = ((h >= 12) ? 'PM' : 'AM');
	h = ((h > 12) ? h - 12 : h);
	h = ((h == 0) ? 12 : h);

	return h + ' ' + suff;
}

function formatTime(t) {
	//	var arr = t.split(':');
	//	var suff = ((arr[0] >= 12)?'PM':'AM');
	//	arr[0] = ((arr[0] > 12)?arr[0]-12:arr[0]);
	//	arr[0] = ((arr[0] == 0)?12:arr[0]);
	//
	//	return arr[0] + ':' + arr[1] + ' ' + suff;
	return Date.Format(t, 'XX');
}

function formatMinutes(x) {
	var minutes = Math.floor(x);
	var seconds = Math.round((x - minutes) * 60);
	if (seconds < 10) {
		seconds = '0' + seconds.toString();
	}

	if (minutes > 60) {
		var hours = Math.floor(minutes / 60.0);
		minutes = minutes - (hours * 60);
		if (minutes < 10) {
			minutes = '0' + minutes.toString();
		}

		return hours + ':' + minutes + ':' + seconds;
	} else {
		return minutes + ':' + seconds;
	}
}

function formatRelativeTime(t) {

}


function auth_check(role, desc, cb){
	$("#div_auth").modal("show");
	$('#role_level').val(role);
	//setTimeout(function() { $('#div_auth_keys').focus() }, 500);
	$('#div_auth_keys').empty();
	$('#div_auth_result').empty();
	$('#sp_auth_desc').html(desc);

	KEYS.resetKeys();
	KEYS.unregisterKeyHandler('toolChange');
	KEYS.registerKeyHandler({"name":"auth_check_login", "global":false, "match_keys":null, "callback":processAuthCheckLogin, "display_el_id":"div_auth_keys", "mask_character":"*"});
	auth_check_callback = cb;
}

function processAuthCheckLogin(scan) {
	auth_op_id = scan;
	// alert(auth_op_id);
	$.getJSON(ajax_procs.main, {
		"action":"get_auth",
		"role":$('#role_level').val(),
		"badgeID":scan
	}, function(jso) {
			if (jso) {
				if (jso.length > 0) {
					auth_op_id = jso[0]['ID'];
					$("#div_auth").modal("hide");
					KEYS.unregisterKeyHandler('auth_check_login');
					KEYS.registerKeyHandler({"name":"toolChange", "global_match":true, "match_keys":"TOOLCHANGE", "callback":modalToolSelectionHandler, "display_el_id":"sp_get_keys", "mask_character":"*"});
					auth_check_callback();
				} else {
					$('#div_auth_result').html('<div class="alert alert-danger">Invalid login!</div>');
				}
			} else {
				$('#div_auth_result').html('<div class="alert alert-danger">Invalid login!</div>');
			}

	});
}
//END OF GENERIC FUNCTIONS