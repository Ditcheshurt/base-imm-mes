//handles all operator stuff (login/logout)
var is_logged_in = false;
var use_OJT = false;

$(function () {
	$('#btn_log_out').on('click', function() {
		event.preventDefault();
		doLogout();
	});
});

function doLogout() {
	operator = {};
	is_logged_in = false;
	$('#div_operator').slideUp();
	$.getJSON(ajax_procs.login, {
		"action":"logout",
		"my_IP":my_IP
	}, afterLogout);
	doLogin();
}

function afterLogout() {
	KEYS.unregisterKeyHandler('toolChange');
	KEYS.unregisterKeyHandler('auth_check_login');
	KEYS.unregisterKeyHandler('scan_serial');
}

function doLogin() {
	writeMachineEnable(false);
	$('#div_login').modal('show');
	$('#div_OJT').hide();
	$('#div_login_result').hide();

	var master_machine_ID = 0;
	if (all_data.machine) {
		if (all_data.machine.length > 0) {
			if (all_data.machine[0].master_machine_ID != null) {
				master_machine_ID = all_data.machine[0].master_machine_ID;
			}
		}
	}

	if (master_machine_ID == 0) {
		$('#div_login_via_scan').show();
		$('#div_login_instr').show();
		KEYS.registerKeyHandler({"name":"login", "global":false, "match_keys":null, "callback":processLogin, "display_el_id":"sp_login_keys", "mask_character":"*"});
	} else {
		$('#div_login_via_scan').hide();
		$('#div_login_via_master').show();
	}
}

function processLogin(scan) {
	$.getJSON(ajax_procs.login, {
			"action":"verify_login",
			"scan":scan,
			"my_IP":my_IP
		}, afterProcessLogin);
}

function afterProcessLogin(jso) {
	if (jso) {
		if (jso.operator) {
			KEYS.unregisterKeyHandler('login');
			operator = jso.operator[0];
			operator.roles = jso.operator_roles;

			$('#div_login_instr').slideUp();
			$('#div_login_result').attr('class', 'alert alert-success').html('Welcome back, ' + operator.name + '!').show();
			$('#sp_operator_details').html('Logged in: <b>' + operator.name + '</b> <i>Since: ' + formatTime(operator.login_time) + '</i>');
			$('#div_operator').slideDown();
			is_logged_in = true;

			setTimeout(afterLogin, 2000);
			// if (use_OJT) {
				// YO STEVO HERES THE CODE STICH
				// new OJT SD 8/5/16
				// var ojt_group_ID = 32; // need to get the machines group
				// var with_alerts = 1;
				// var cb = afterLogin;
				// NYSUS.OJT.do_ojt(ojt_group_ID, operator.ID, with_alerts, cb)

				// old ojt
				// checkOJTRequirements();
			// } else {

			// }
		} else {
			$('#div_login_result').attr('class', 'alert alert-danger').html('Login failed.').show();
		}
	} else {
		$('#div_login_result').attr('class', 'alert alert-danger').html('Login failed.').show();
	}
}

function checkOJTRequirements() {
	$.getJSON(ajax_procs.login, {
		"action":"check_OJT_requirements",
		"machine_ID":all_data.machine[0].ID,
		"operator_ID":operator.ID
	}, afterCheckOJTRequirements);
}

function afterCheckOJTRequirements(jso) {
	if (jso) {
		if (jso.length > 0) {
			var arr = [];
			for (var i = 0; i < jso.length; i++) {
				arr.push(jso[i].job_name);
			}

			$('#div_OJT').show();
			$('#div_OJT_detail H3').html(jso.length + ' OJT Job(s) Not Signed Off: ' + arr.join(', '));
		} else {
			$.growl(
				{"title":" All OJT Requirements Met! ", "message":"All OJT Jobs Signed Off", "icon":"glyphicon glyphicon-ok"},
				{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
			);
			setTimeout(afterLogin, 2000);
		}
	}
}

function afterLogin() {
	$('#div_login').modal('hide');

	writeMachineEnable(true);

	KEYS.registerKeyHandler({"name":"toolChange", "global_match":true, "match_keys":"TOOLCHANGE", "callback":modalToolSelectionHandler, "display_el_id":"sp_get_keys", "mask_character":"*"});

	//var num_unseen_alerts = getUnseenAlerts();

	// if (num_unseen_alerts[0] > 0 || num_unseen_alerts[1] > 0) {
		//show alerts
		// if (num_unseen_alerts[0] > 0) {
			// displayAlerts('SAFETY');
		// } else {
			// displayAlerts('QUALITY');
		// }
	// } else {
	//	selectTool();

	getCurrentToolPartConfiguration();
	
	var part_list = [];
	var current_tool = [];
	current_tool.parts = [];
	for (var i = 0; i < all_data.tools.length; i++) {
		if (all_data.tools[i].tool_ID == all_data.machine[0].current_tool_ID){
			current_tool.tool_description = all_data.tools[i].tool_description;
			break;
		}
	}
	for (var j = 0; j < all_data.parts.length; j++) {
		if (all_data.parts[j].tool_ID == all_data.machine[0].current_tool_ID) {
			current_tool.parts.push(all_data.parts[j]);
			part_list.push(all_data.parts[j].part_number);
		}
	}
	$('#btn_select_tool').html(current_tool.tool_description);
	$('#sp_current_part_number').html(part_list.join('<br>'));
	
	//resetModalToolSelectKeys();

	// }
}

function writeMachineEnable(enabled) {
	if (all_data.machine) {
		if (all_data.machine.length > 0) {
			if (all_data.machine[0].PLC_file_address != null && all_data.machine[0].PLC_IP_address != null) {
				var new_value = (enabled?'True':'False');

				$.ajax({
					"type":"POST",
					"url":ajax_procs.PLC + "/setValueAsJSON",
					"data":{
						"plc_ip":all_data.machine[0].PLC_IP_address,
						"plc_path":"0",
						"file_addr":all_data.machine[0].PLC_file_address,
						"new_value":new_value,
						"data_type":"BOOL"
					},
					"success":afterWriteMachineEnable,
					"dataType":"json"
				});
			}
		}
	}
}

function afterWriteMachineEnable(jso) {
	if (jso.ErrorCode == '0') {
		console.log("Machine write enable written");
	}
}