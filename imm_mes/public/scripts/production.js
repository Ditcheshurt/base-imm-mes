//handles all production stuff
$(function () {
	$('#div_production_summary').highcharts({
		chart: {
			type: 'column',
			spacingBottom: 0,
			spacingTop: 0
		},
		title: {
			text: '',
			enabled: false,
			style: {"display":"none"}
		},
		credits: {
			enabled: false
		},
		xAxis: {
			categories: []
		},
		yAxis: [{
			min: 0,
			endOnTick: false,
			title: {
				text: 'Target',
				enabled: false
			}
		}, {
			endOnTick: false,
			title: {
				text: 'Actual',
				enabled: false
			}
		}],
		legend: {
			shadow: false,
			enabled: false
		},
		tooltip: {
			enabled: false
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0,
				borderRadius: 5,
				states: {
					hover: {
						enabled: false
					}
				}
			},
			series: {
				states: {
					hover: {
						enabled: false
					}
				}
			}
		},
		series: [{
			name: 'Target',
			color: 'rgba(126,126,126,.7)',
			data: [],
			pointPadding: 0.0,
			pointPlacement: 0
		},{
			name: 'Actual',
			//color: 'rgba(22,170,2,.7)',
			data: [],
			pointPadding: 0.1,
			pointPlacement: 0,
			dataLabels: { enabled: true }
		}]
	});

	$('#tbl_label tbody').on('click','.btn-resume-partial', function(){
		var d = $(this).data();
		getPartialRacks(d);
	})
	$('#tbl_label tbody').on('click','.btn-end-as-partial', function(){
		var d = $(this).data();
		endAsPartialRack(d)
	})

	// moved to defect.js (line 33) by R.Kahle 5/5/2017
	//$('#tbl_label tbody').on('click','.btn-remove-rack-part', function(){
	//})

	$('#div_select_partial').on('click','.btn-partial-rack', function(){
		var d = $(this).data();
		resumePartialRack(d);
	})

	$('#modal_change_tool').on('click', function(){
		event.preventDefault();
		modalToolSelectionHandler();
	});

//	$('#btn_select_tool').on('click', function() {
//		event.preventDefault();
//
//		$('#div_select_tool_modal').modal('show');
//	});

	$('#ul_select_tools').on('click', '.btn-tool-select', function() {
		event.preventDefault();
		var d = $(this).data();
		group_select(d);
	});

	$('#ul_select_groups').on('click', '.btn-group-select', function() {
		event.preventDefault();
		var d = $(this).data();
		part_select(d);
	});

	$('#ul_select_parts').on('click', '.btn-part-select', function() {
		event.preventDefault();
		var info = $(this).data();
		if($(this).hasClass("current_tool")){
			$(this).removeClass("current_tool");
			$(this).find( ".part_status").html('<div style="color:red;"><span class="glyphicon glyphicon-remove"></span>OFF</div>');
		}else{
			$(this).addClass("current_tool");
			$(this).find( ".part_status").html('<div style="color:green;"><span class="glyphicon glyphicon-ok"></span>ON</div>');
		}

	});

	$("#submit_partlist").click(function(){
		var partlist = '';
		var tool;
		$( '.btn-part-select' ).each(function( index ){
			if($(this).hasClass("current_tool")){
				var info = $(this).data();
				partlist = partlist+info.real_part_id;
				tool = info.tool_ID;
				partlist = partlist+'-';
			}
		});
		partlist=partlist.slice(0,-1);
		$.getJSON(ajax_procs.production, {
		"action":"insert_part_change",
		"machine_ID":all_data.machine[0].ID,
		"tool_id": tool,
		"op_id": auth_op_id,
		"part_list":partlist},function(e){
			auth_op_id = 0;
			getCurrentToolPartConfiguration();
		});
		$('#div_select_part_modal').modal('hide');
	});

	$(".restart_tcp").click(function(){
		$('#div_select_group_modal').modal('hide');
		$('#div_select_part_modal').modal('hide');
		$('#div_select_tool_modal').modal('show');
	});

});

function displayUnseenWorkInstructions() {
	//check work instructions
	var num_unseen_instructions = getUnseenWorkInstructions();

	if (num_unseen_instructions > 0) {
		displayCurrentWorkInstructions();
	}
}

function updateProductionSummary() {
	//update production summary chart
	var chart = $('#div_production_summary').highcharts();
	var hour_labels = [];
	var hour_data = [];
	var hour_targets = [];
	var cur_hour = parseInt(all_data.time_and_shift[0].cur_time.split(':')[0]);
	var hourly_summary_length = all_data.production.hourly_summary == null ? 0 : all_data.production.hourly_summary.length;

	for (var i = 0; i < 24 && i < hourly_summary_length; i++) {
		hour_labels.push(all_data.production.hourly_summary[i].interval_desc);

		var c = 'limegreen';
		if (all_data.production.hourly_summary[i].part_qty < all_data.production.hourly_summary[i].part_goal) {
			c = 'red';
		}

		var d = {
						y:all_data.production.hourly_summary[i].part_qty,
						color:c
					};
		hour_data.push(d);
		//hour_data.push(all_data.production.hourly_summary[i].part_qty);
		hour_targets.push(all_data.production.hourly_summary[i].part_goal); //part_target_qty);
	}
	chart.xAxis[0].setCategories(hour_labels, false);
	chart.series[0].setData(hour_targets);
	chart.series[1].setData(hour_data);

	// set red if under target
//	for (var i = 0; i < 24 && i < hourly_summary_length; i++) {
//		if (chart.series[0].data.length == 0) {
//			return;
//		}
//		var target = chart.series[0].data[i].y;
//		var built = chart.series[1].data[i].y;
//		if (built < target || target == 0 || target == null) {
//			chart.series[1].data[i].color = 'red';
//		} else {
//			chart.series[1].data[i].color = 'rgba(22,170,2,.7)';
//		}
//	}
//	chart.redraw();
}

function updateShiftCounts() {

	var shift_summary_length = all_data.production.shift_summary == null ? 0 : all_data.production.shift_summary.length;
	var cur_shift = all_data.time_and_shift[0].cur_shift;
	//update shift counts
	$('#sp_shift_1').hide();
	$('#sp_shift_2').hide();
	$('#sp_shift_3').hide();
	for (var i = 0; i < shift_summary_length; i++) {
		$('#sp_shift_' + (i + 1)).html('Shift ' + all_data.production.shift_summary[i].the_shift + ': ' + all_data.production.shift_summary[i].part_qty).show();
	}

	if (shift_summary_length > 0) {
		for (var i = 0; i < shift_summary_length; i++) {
			if (cur_shift == all_data.production.shift_summary[i].the_shift) {
				var cur_shift_data = all_data.production.shift_summary[i];
				$('#sp_summary_oee')
					.html((cur_shift_data.oee * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.oee, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.oee, 0.85));
				$('#sp_summary_availability')
					.html((cur_shift_data.availability * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.availability, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.availability, 0.85));
				$('#sp_summary_performance')
					.html((cur_shift_data.performance * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.performance, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.performance, 0.85));
				$('#sp_summary_quality')
					.html((cur_shift_data.quality * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.quality, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.quality, 0.85));
				break;
			}
		}
	} else {
		$('#sp_summary_oee')
			.html("--- %")
			.attr("class", "badge " + getColorClassForPercent(0.9, 0.85));
		$('#sp_summary_availability')
			.html("--- %")
			.attr("class", "badge " + getColorClassForPercent(0.9, 0.85));
		$('#sp_summary_performance')
			.html("--- %")
			.attr("class", "badge " + getColorClassForPercent(0.9, 0.85));
		$('#sp_summary_quality')
			.html("--- %")
			.attr("class", "badge " + getColorClassForPercent(0.9, 0.85));
	}
}


function updateCycleData() {
	$('#sp_last_cycle_time')
		.html(all_data.cycle[0].last_cycle_time.toFixed(2) + getThumb(all_data.cycle[0].last_cycle_time, all_data.cycle[0].target_cycle_time))
		.attr("class", "badge " + getColorClass(all_data.cycle[0].last_cycle_time, all_data.cycle[0].target_cycle_time));
	$('#sp_avg_cycle_time')
		.html(all_data.cycle[0].avg_cycle_time.toFixed(2) + getThumb(all_data.cycle[0].avg_cycle_time, all_data.cycle[0].target_cycle_time))
		.attr("class", "badge " + getColorClass(all_data.cycle[0].avg_cycle_time, all_data.cycle[0].target_cycle_time));;
	$('#sp_target_cycle_time')
		.html(all_data.cycle[0].target_cycle_time.toFixed(2));
	$('#sp_low_cycle_time')
		.html(all_data.cycle[0].low_cycle_time.toFixed(2));
}

//PARTIAL RACK FUNCTIONS
function endAsPartialRack(d) {
	$.getJSON(ajax_procs.production, {
		"action":"end_as_partial_rack",
		"machine_ID":all_data.machine[0].ID,
		"part_ID":d.ID}
		,function(jso) {
			if (jso) {
				if (jso.success == 1) {
					$.growl(
						{"title":" End as partial success - ", "message":"Rack ID "+jso.rack_ID+" ended as partial", "icon":"glyphicon glyphicon-user"},
						{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
					$('#div_resume_partial_modal').modal('hide');
					getCurrentToolPartConfiguration();
				} else {
					$.growl(
						{"title":" End as partial failure - ", "message":"Rack wasn't found", "icon":"glyphicon glyphicon-user"},
						{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
				}
			}
		}
	);
}
function resumePartialRack(d) {
	$.getJSON(ajax_procs.production, {
		"action":"resume_partial_rack",
		"machine_ID":all_data.machine[0].ID,
		"rack_ID":d.ID}
		,function(jso) {
			if (jso) {
				if (jso.success == 1) {
					$.growl(
						{"title":" Resume partial success - ", "message":"Partial rack ID "+jso.rack_ID+" resumed", "icon":"glyphicon glyphicon-user"},
						{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
					$('#div_resume_partial_modal').modal('hide');
					getCurrentToolPartConfiguration();
				} else {
					$.growl(
						{"title":" Resume partial failure - ", "message":"Resumable partial rack (ID="+jso.rack_ID+") not found", "icon":"glyphicon glyphicon-user"},
						{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
				}
			}
		}
	);
}
function getPartialRacks(d) {
	$.getJSON(ajax_procs.production, {
		"action":"get_partial_racks",
		"machine_ID":all_data.machine[0].ID,
		"tool_ID":current_tool.tool_ID,
		"part_ID":d.ID}
		, function(jso) {
			if (jso) {
				if (jso.success == 1) {
					console.log('Got partial racks!');
					var pr_div = $('#div_select_partial').empty();
					for (var i = 0; i < jso.racks.length; i++) {
						var jr = jso.racks[i];
						var pr_btn = $('<BUTTON>').addClass('btn btn-success btn-lg btn-partial-rack').html('<span class="glyphicon glyphicon-credit-card"></span> Rack ID '+jr.ID+" ("+jr.rack_part_count+"/"+jr.standard_pack_qty+")").data(jr);
						pr_div.append(pr_btn);
					}

					$('#div_resume_partial_modal').modal('show');
				} else {
					$.growl(
						{"title":" Resume partial - ", "message":"No partial racks found for this machine/tool/part#", "icon":"glyphicon glyphicon-user"},
						{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
				}
			}
		}
	)
}
//END PARTIAL RACK FUNCTIONS

//PART/TOOL SELECTION
function modalToolSelectionHandler() {
	//need supervisor permission to use this!
	auth_check(5, ' Change Tool', function(){
		if (use_tool_change) {
			$('#div_tool_change_modal').modal('show');
			getToolChangeStatus();
		} else {
			$('#div_select_tool_modal').modal('show');
			selectTool();
		}
	});
}

function getToolChangeStatus() {
	$.getJSON(ajax_procs.production, {
		"action":"get_tool_chg_status",
		"machine_ID":all_data.machine[0].ID,
		"tool_ID":all_data.machine[0].current_tool_ID
	}, afterGetToolChgStatus);
}
function afterGetToolChgStatus(jso) {
	var jso = jso || [];
	jso.active = jso.active || [];
	jso.paused = jso.paused || [];
	
	var toolChg = $('#div_tool_change_modal');
	var toolChgOpts = $('#div_tool_change_options');
	
	toolChgOpts.empty();
	
	if (jso.active.length == 0) {
		// begin tool change
		// create a new tool change downtime record
		$('#div_tool_change_footer').html('No Active Tool Change Found');

		toolChgOpts.append(
			$('<BUTTON />')
				.addClass('btn btn-success')
				.attr('id','beginToolChange')
				.html('<span class="glyphicon glyphicon-log-out"></span> Begin Tool Change')
		);
		$('#beginToolChange').on('click', function(e) {
			toolChangeHandler('start', afterToolChange);
		});
	} else if (jso.paused.length == 0) {
		// current tool change in progress
		// allow selecting to pause current tool change which will create new open downtime record
		$('#div_tool_change_footer').html('Active tool change as of ' + jso.active[0].change_time.date);

		toolChgOpts.append(
			$('<BUTTON />')
				.addClass('btn btn-warning')
				.attr('id', 'pauseToolChange')
				.html('<span class="glyphicon glyphicon-pause"></span> Pause Tool Change')
		);
		$('#pauseToolChange').on('click', function(e) {
			toolChangeHandler('pause', afterToolChange);
		});

		toolChgOpts.append(
			$('<BUTTON />')
				.addClass('btn btn-danger')
				.attr('id', 'endToolChange')
				.html('<span class="glyphicon glyhpicon-stop"></span> End Tool Change')
		);
		$('#endToolChange').on('click', function(e) {
			toolChangeHandler('end', afterToolChange);
		});
	} else if (jso.paused.length > 0) {
		// current tool change has been paused
		// allow selecting to resume current tool change, closing open paused tool change downtime record leaving it undispositioned
		$('#div_tool_change_footer').html('Active tool change as of ' + jso.active[0].change_time.date);

		toolChgOpts.append(
			$('<BUTTON />')
				.addClass('btn btn-success')
				.attr('id', 'resumeToolChange')
				.html('<span class="glyphicon glyphicon-forward"></span> Resume Tool Change')
		);
		$('#resumeToolChange').on('click', function(e) {
			toolChangeHandler('resume', afterToolChange);
		});
		
		toolChgOpts.append(
			$('<BUTTON />')
				.addClass('btn btn-danger')
				.attr('id', 'endToolChange')
				.html('<span class="glyphicon glyhpicon-stop"></span> End Tool Change')
		);
		$('#endToolChange').on('click', function(e) {
			toolChangeHandler('end', afterToolChange);
		});
	}
}

function toolChangeHandler(type, cb) {
	$.getJSON(ajax_procs.production, {
		"action":"tool_change",
		"type":type,
		"machine_ID":all_data.machine[0].ID,
		"tool_ID":all_data.machine[0].current_tool_ID
	}, cb);
}
function afterToolChange(jso) {
	var jso = jso || [];
	jso.type = jso.type || '';
	
	console.log(jso.type + ' tool change');
	
	$('#div_tool_change_modal').modal('hide');
	if (jso.type == 'end') {		
		$('#div_select_tool_modal').modal('show');
		selectTool();
	}
}

//PART/TOOL SELECTION
function selectTool() {
	if (all_data.tools.length == 0) {
		$('#btn_select_tool').prop('disabled', true);
		// processSelectedTool(all_data.tools[0]);
		return;
	}

	$('#btn_select_tool').prop('disabled', false);

	$('#div_current_tool DIV').hide();

	var u = $('#ul_select_tools').html('');

	for (var i = 0; i < all_data.tools.length; i++) {
		var l = $('<LI />');
		var b = $('<BUTTON />').addClass('btn btn-tool-select').data(all_data.tools[i]);

		if (all_data.tools[i].tool_ID == all_data.machine[0].current_tool_ID) {
			b.addClass('current_tool');
		}

		b.html('<div class="panel panel-primary">' +
				 '	<div class="panel-heading">' + all_data.tools[i].short_description + '</div>' +
				 '	<div class="panel-body"><b>' + all_data.tools[i].tool_description + '</b></div>' +
				 '</div>');
		l.append(b);
		u.append(l);
	}

	$('#div_select_tool_modal').modal('show');
}

function group_select(d){
	var selected_tool = d.tool_ID;
	var last_group = 0;
	var u = $('#ul_select_groups').html('');
	for (var i = 0; i < all_data.parts.length; i++) {
		if (all_data.parts[i].tool_ID == selected_tool && last_group != all_data.parts[i].part_group_id) {
			$('#group_header').html("TOOL: "+ all_data.parts[i].tool_description);
			last_group =  all_data.parts[i].part_group_id;
			var l = $('<LI />');
			var b = $('<BUTTON />').addClass('btn btn-group-select').data({ group: all_data.parts[i].part_group_id, tool: selected_tool });

			b.html('<div class="panel panel-primary">' +
					'	<div class="panel-heading">' + all_data.parts[i].part_group_id + '</div>' +
					'	<div class="panel-body"><b>' + all_data.parts[i].part_group_description + '</b></div>' +
					'</div>');
			l.append(b);
			u.append(l);
		}
	}
	$('#div_select_tool_modal').modal('hide');
	$('#div_select_group_modal').modal('show');
}

function part_select(d){
	var tool_id = d.tool;
	var group_id = d.group;
	var u = $('#ul_select_parts').html('');
	for (var i = 0; i < all_data.parts.length; i++) {
		if (all_data.parts[i].tool_ID == tool_id && all_data.parts[i].part_group_id == group_id) {
			for(j = 0; j < all_data.parts[i].parts_per_tool; j++){
				$('#parts_header').html("<div><b>TOOL: </b>"+ all_data.parts[i].tool_description+" </div><div> <b>GROUP: </b>"+all_data.parts[i].part_group_description)+"</div>";
				var l = $('<LI />');
				var b = $('<BUTTON />').addClass('btn btn-part-select current_tool').data(all_data.parts[i]);

				b.html('<div class="panel panel-primary">'+
						 '	<div class="panel-heading">' + all_data.parts[i].part_number + '</div>' +
						 '	<div class="panel-body"><b>' + all_data.parts[i].part_desc + '</b></div>' +
						 '<div class="part_status"><div style="color:green;"><span class="glyphicon glyphicon-ok"></span>ON</div></div>'+
						 '</div>');
				l.append(b);
				u.append(l);
			}
		}
	}
	$('#div_select_group_modal').modal('hide');
	$('#div_select_part_modal').modal('show');

}
//END OF PART/TOOL SELECTION

function processSelectedTool(d) {
	$.growl(
		{"title":" Tool Selected ", "message":"Selected tool: " + d.tool_description, "icon":"glyphicon glyphicon-exclamation-sign"},
		{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
	);

	current_tool = d;
	current_tool.parts = [];

	var part_list = [];
	for (var j = 0; j < all_data.parts.length; j++) {
		if (all_data.parts[j].tool_ID == d.tool_ID) {
			var z = current_tool.active_parts.findIndex(x => x.ID == all_data.parts[j].part_ID);
			current_tool.parts.push(all_data.parts[j]);
			if (z > -1) part_list.push(current_tool.active_parts[z].part_number);
		}
	}

	$('#btn_select_tool').html(current_tool.tool_description);
	$('#sp_current_part_number').html(part_list.join('<br>'));
	$('#div_current_tool DIV').show();

	$('#div_top5_defects').hide();

	$('#div_select_tool_modal').modal('hide');

	//TODO: make call to set current tool
	$.getJSON(ajax_procs.production, {
		"action":"set_current_tool",
		"machine_ID":all_data.machine[0].ID,
		"tool_ID":current_tool.tool_ID}
		, function(jso) {
			if (jso) {
				if (jso.success == 1) {
					console.log('Tool set!');
					afterSelectTool();
				} else {
					$.growl(
						{"title":" Invalid Tool Selection ", "message":"Selected tool is not valid for this machine", "icon":"glyphicon glyphicon-user"},
						{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
					);
					selectTool();
				}
			}
		}
	);
}

function afterSelectTool() {
	var ojt_group_ID = current_tool.ojt_group_ID || 0;
	if (use_ojt) {
		if (ojt_group_ID != 0) {
			NYSUS.OJT.do_ojt(ojt_group_ID, operator.ID, 1, displayUnseenWorkInstructions);
		} else {
			console.log('Tool is not part of an OJT group.');
			$.growl(
				{"title":" No OJT Group ", "message":"Selected tool has no OJT group defined", "icon":"glyphicon glyphicon-user"},
				{"type":"warning", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
			);
			displayUnseenWorkInstructions();
		}
	} else {
		console.log('OJT is turned OFF');
		displayUnseenWorkInstructions();
	}
	resetModalToolSelectKeys();

	$('#btn_select_tool').html(current_tool.tool_description);
	$('#sp_current_part_number').html(part_list.join('<br>'));
	$('#div_current_tool DIV').show();
	getCurrentToolPartConfiguration();

	//startCycleTimer();
	loadMachineLastcycle();
}

function getCurrentToolPartConfiguration() {
	$.getJSON(ajax_procs.main, {
			"action":"load_data",
			"data_types":"tool_part_configuration",
			"machine_ID":all_data.machine[0].ID,
			"my_IP":my_IP
		}, function(jso) {
				if (jso.tool) {
					if (jso.tool.length > 0) {
						current_tool = jso.tool[0];
						current_tool.active_parts = jso.active_parts;
						current_tool.part_queue = jso.part_queue || [];
						//current_tool.part_queue_count = jso.part_queue_count || [];
						current_tool.container_info = jso.container_info || [];

						var part_list = [];
						current_tool.parts = [];
						for (var i = 0; i < all_data.tools.length; i++) {
							if (all_data.tools[i].tool_ID == all_data.machine[0].current_tool_ID){
								current_tool.tool_description = all_data.tools[i].tool_description;
								break;
							}
						}
						for (var j = 0; j < all_data.parts.length; j++) {
							if (all_data.parts[j].tool_ID == all_data.machine[0].current_tool_ID) {
								//var z = current_tool.active_parts.findIndex(function(x) { return x.ID == all_data.parts[j].part_ID; });
								current_tool.parts.push(all_data.parts[j]);
								//if (z > -1) part_list.push(current_tool.active_parts[z].part_number);
							}
						}

						for (var i = 0; i < current_tool.active_parts.length; i++) {
							part_list.push(current_tool.active_parts[i].part_number);
						}
						$('#btn_select_tool').html(current_tool.tool_description);
						$('#sp_current_part_number').html(part_list.join('<br>'));

						updateCurrentPartTable();

					}
				}
		}
	);
}

//END OF PART/TOOL SELECTION


//CYCLE TIMER
function startCycleTimer() {
	if (cycle_timer) {
		clearTimeout(cycle_timer);
	}
    cycle_timer_count = all_data.machine[0].last_cycle_time_ago;

	cycle_timer = setTimeout(updateCycleTimer, 1000);
}

function updateCycleTimer() {
	//cycle_timer_count++;

	var minutes = Math.floor(cycle_timer_count/60.0);
	var seconds = cycle_timer_count - (minutes * 60);
	if (seconds < 10) {
		seconds = '0' + seconds.toString();
	}

	var target = current_tool.target_cycle_time;
	var c = "";
	var f = 'white';
	var d = new Date();
	var s = d.getSeconds();

	if (target) {
		if (cycle_timer_count > target) {
			c = 'yellow';
			f = 'black';
		} else {
			machine_down_state_set = false;
		}
		if (cycle_timer_count > (target * 1.10)) {
			if ((s % 2) == 0) {
				c = 'red';
			} else {
				c = 'white';
				f = 'black';
			}
		}
	}

	$('#h_cycle_timer').html(minutes + ':' + seconds).css('background-color', c).css('color', f);
	//cycle_timer = setTimeout(updateCycleTimer, 1000);

	if (target) {
		if (cycle_timer_count > target && all_data.machine[0].ID != 0 && !machine_down_state_set) {
			$.getJSON(ajax_procs.downtime, {
				"action":"set_machine_down_state",
				"machine_ID":all_data.machine[0].ID,
				"state":0,
				"tool_ID":current_tool.tool_ID,
				"operator_ID":operator.ID}, function(jso) {
					machine_down_state_set = true;
				}
			);
		}
	}
}

//END OF CYCLE TIMER


//WORK INSTRUCTIONS
function updateWorkInstructions() {
	//all_data.work_instructions = [];
	$('#btn_work_instructions SPAN.badge').html(all_data.work_instructions.length);
	$('#btn_work_instructions').prop('disabled', (all_data.work_instructions.length == 0));
}

function displayCurrentWorkInstructions() {
	var ol = $('#div_instructions_carousel OL.carousel-indicators').html('');
	var slides = $('#div_instructions_carousel DIV.carousel-inner').html('');
	var jso = all_data.work_instructions;

	for (var i = 0; i < jso.length; i++) {
		var li = $('<LI />').data('target', '#div_instructions_carousel').data('slide-to', i).data('instr_ID', jso[i].ID);
		var item = $('<DIV />').addClass('item');

		var tbl = $('<TABLE />').appendTo(item).addClass('table table-bordered').css('width','90%').attr('align', 'center');
		var r = $('<TR />').appendTo(tbl);
		var top_title = $('<TD />').attr('colspan', 2).css('text-align', 'center').appendTo(r).html(jso[i].title);

		var r = $('<TR />').appendTo(tbl);
		var lh_side = $('<TD />').css('width','50%').css('font-size','24pt').css('text-align', 'center').appendTo(r).html(jso[i].instr_text);

		var rh_side = $('<TD />').css('width','50%').appendTo(r);
		var img = $('<IMG />').attr('src', '/machine_mes2/public/images/work_instructions/machine_' + all_data.machine[0].ID + '/' + jso[i].instr_image);
		img.css('min-height', '300px');
		img.appendTo(rh_side);

		var r = $('<TR />').appendTo(tbl);
		var signoff_time = '<span class="label label-warning">You are viewing this instruction for the first time.</span>';
		for (var j = 0; j < all_data.work_instruction_signoffs.length; j++) {
			var so = all_data.work_instruction_signoffs[j];
			if (jso[i].ID == so.instr_ID && so.operator_ID == operator.ID) {
				signoff_time = '<span class="label label-success">You viewed this instruction at: ' + so.signoff_time.date + '</span>';
			}
		}
		var bottom_info = $('<TD />').attr('colspan', 2).css('text-align', 'left').appendTo(r).html(signoff_time);

		if (i == 0) {
			li.addClass('active');
			item.addClass('active');
		}

		li.appendTo(ol);
		item.appendTo(slides);
	}

	if (jso.length == 1) {
		$('.carousel-control').hide();
		$('.carousel-indicators').hide();
	} else {
		$('.carousel-control').show();
		$('.carousel-indicators').show();
	}

	$('#div_instructions_carousel').carousel({"interval":false});

	$('#div_work_instruction_modal').modal('show');
}

function getUnseenWorkInstructions() {
	var res = 0;

	for (var i = 0; i < all_data.work_instructions.length; i++) {
		var wi = all_data.work_instructions[i];
		var seen = false;
		for (var j = 0; j < all_data.work_instruction_signoffs.length; j++) {
			var so = all_data.work_instruction_signoffs[j];
			if (wi.ID == so.instr_ID && so.operator_ID == operator.ID) {
				seen = true;
			}
		}
		if (!seen) {
			res++;
		}
	}

	return res;
}

function recordWorkInstructionSignoff() {
	var instr_ID = $('#div_instructions_carousel OL LI.active').data('instr_ID');
	console.log('Saw ' + instr_ID);
	$.getJSON(ajax_procs.production, {"action":"record_work_instruction_signoff", "instr_ID":instr_ID, "operator_ID":operator.ID}, afterRecordWorkInstructionSignoff);
}

function afterRecordWorkInstructionSignoff(jso) {
	$.growl(
		{"title":" Work Instruction Recorded ", "message":"Work instruction signoff recorded!", "icon":"glyphicon glyphicon-ok"},
		{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
	);
}
//END OF WORK INSTRUCTIONS

