var idleTime = 0;
var shift = {};

$(document).ready(function () {
    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
	atstartup();
    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 9) { // 10 minutes
       $.getJSON('./ajax_processors/timeout.php', {"action":"machine_cycle_active", "machine_ID":50000}, 
	   function(e){
		   console.log(e);
			if(e){
				// skip out
				// continue;
			}else{
				doLogin();
			}
	   });			
    }
	// time check to if it is end of shift
	check_shift();
}

function check_shift(){
	var current_time = Math.floor(Date.now() / 1000);
	var end = 0;
	if(shift['end'] < shift['start']){
		// plus 24 hours to the end shift to cover for over flow shifts 
		end = shift['end'] + (((24 * 60)*60)* 1000);
	}else{
		end = shift['end']; 
	}
	if(current_time >= end ){
		// doLogin();
		alert('TimeOUT!!! ->' + Date.now());		
	}
}

function atstartup(){
	$.getJSON('./ajax_processors/timeout.php', {"action":"get_shifts"}, set_shift);	
}

function set_shift(jso){
	var time = 0;
	$.each(jso, function (k , v) {
			shift["start"] = Math.floor(new Date(v.shift_start.date).getTime()/1000);
			shift["end"] = Math.floor(new Date(v.shift_end.date).getTime()/1000);
			time = shift['end'] + (((24 * 60)*60)* 1000);
			var d = new Date(time);
	});	
	check_shift();
}