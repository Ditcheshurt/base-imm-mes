//handles all alerts

$(function() {
	$('.btn-alerts').on('click', function() {
		event.preventDefault();

		displayAlerts($(this).data('alert-type'));
	});

	$('#div_alerts_modal').on('hidden.bs.modal', function() {
		var num_unseen_alerts = getUnseenAlerts();

		if (num_unseen_alerts[0] > 0 || num_unseen_alerts[1] > 0) {
			//show alerts
			if (num_unseen_alerts[0] > 0) {
				displayAlerts('SAFETY');
			} else {
				displayAlerts('QUALITY');
			}
			return;
		}

		if (!current_tool.tool_ID) {
			selectTool();
		}
	});



	$('#div_alerts_carousel').on('slid.bs.carousel', function () {
		setTimeout(recordAlertSignoff, 500);
	});

	$('#div_alerts_modal').on('shown.bs.modal', function() {
		if (($('#div_alerts_modal').data('bs.modal') || {isShown: false}).isShown) {
			recordAlertSignoff();
		}
	});
});


function updateAlerts() {
	var alerts = [];
	for (var i = 0; i < all_data.safety_alerts.length; i++) {
		var a = all_data.safety_alerts[i];
		if (a != '.' && a != '..' && a.split('.').pop().toUpperCase() == 'PDF') {
			alerts.push(a);
		}
	}

	all_data.safety_alerts = alerts;
	$('.safety-alerts SPAN.badge').html(all_data.safety_alerts.length);
	$('.safety-alerts').prop('disabled', (all_data.safety_alerts.length == 0));

	var alerts = [];
	for (var i = 0; i < all_data.quality_alerts.length; i++) {
		var a = all_data.quality_alerts[i];
		if (a != '.' && a != '..' && a.split('.').pop().toUpperCase() == 'PDF') {
			alerts.push(a);
		}
	}

	all_data.quality_alerts = alerts;
	$('.quality-alerts SPAN.badge').html(all_data.quality_alerts.length);
	$('.quality-alerts').prop('disabled', (all_data.quality_alerts.length == 0));
}

function displayAlerts(alert_type) {
	var config = $.get('./config/app_config.json', function (data) {

		$('#div_alerts_modal .modal-title').html(alert_type + ' Alerts');
		$('#div_alerts_modal .modal-content').css('background-color', (alert_type == 'SAFETY') ? '#e74c3c' : '#f39c12');
		var ol = $('#div_alerts_carousel OL.carousel-indicators').html('');
		var slides = $('#div_alerts_carousel DIV.carousel-inner').html('');

		var arr = all_data.safety_alerts;

		var path = './public/scripts/pdfjs/web/viewer.html?file=' + data.alert_files_url + '/safety/';
		if (alert_type == 'QUALITY') {
			arr = all_data.quality_alerts;
			path = './public/scripts/pdfjs/web/viewer.html?file=' + data.alert_files_url + '/quality/' + all_data.machine[0].system_name + '/';
		}

		for (var i = 0; i < arr.length; i++) {
			var li = $('<LI />').data('target', '#div_instructions_carousel').data('slide-to', i).data('file_name', arr[i]).data('alert_type', alert_type);
			var item = $('<DIV />').addClass('item').addClass('text-center');

			//var ifrm = $('<IFRAME />').css('width','90%').css('height','850px').css('border', 'none').appendTo(item).delay(500).attr('src', path + arr[i] + '#page=1&toolbar=0&statusbar=0&messages=0&navpanes=0&scrollbar=0');
			var ifrm = '<iframe style="width:100%; height:850px" src="' + path + arr[i] + '"></iframe>';
			item.append(ifrm);
			if (i == 0) {
				li.addClass('active');
				item.addClass('active');
			}

			li.appendTo(ol);
			item.appendTo(slides);
		}

		if (arr.length == 1) {
			$('.carousel-control').hide();
			$('.carousel-indicators').hide();
		} else {
			$('.carousel-control').show();
			$('.carousel-indicators').show();
		}

		$('#div_alerts_modal').modal('show');
	});
}

function getUnseenAlerts() {
	var res = [0, 0];

	for (var i = 0; i < all_data.safety_alerts.length; i++) {
		var a = all_data.safety_alerts[i];
		var seen = false;
		for (var j = 0; j < all_data.alert_signoffs.length; j++) {
			var so = all_data.alert_signoffs[j];
			if (so.alert_type == 'SAFETY' && a == so.file_name && so.operator_ID == operator.ID) {
				seen = true;
			}
		}
		if (!seen) {
			res[0]++;
		}
	}

	for (var i = 0; i < all_data.quality_alerts.length; i++) {
		var a = all_data.quality_alerts[i];
		var seen = false;
		for (var j = 0; j < all_data.alert_signoffs.length; j++) {
			var so = all_data.alert_signoffs[j];
			if (so.alert_type == 'QUALITY' && a == so.file_name && so.operator_ID == operator.ID) {
				seen = true;
			}
		}
		if (!seen) {
			res[1]++;
		}
	}

	return res;
}

function recordAlertSignoff() {
	var file_name = $('#div_alerts_carousel OL LI.active').data('file_name');
	var alert_type = $('#div_alerts_carousel OL LI.active').data('alert_type');
	console.log('Saw alert: ' + file_name);
	$.getJSON(ajax_procs.production, {"action":"record_alert_signoff", "file_name":file_name, "operator_ID":operator.ID, "alert_type":alert_type}, afterRecordAlertSignoff);
}

function afterRecordAlertSignoff(jso) {
	$.growl(
		{"title":" Alert Signoff Recorded ", "message":"Alert signoff recorded!", "icon":"glyphicon glyphicon-ok"},
		{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
	);
}