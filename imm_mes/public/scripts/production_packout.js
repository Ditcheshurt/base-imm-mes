//handles all production stuff

$(function () {
	var op ={
		chart: {
			type: 'column',
			spacingBottom: 0,
			spacingTop: 0,
			height: 220
		},
		title: {
			text: '',
			enabled: false,
			style: {"display":"none"}
		},
		credits: {
			enabled: false
		},
		xAxis: {
			categories: []
		},
		yAxis: [{
			min: 0,
			endOnTick: false,
			title: {
				text: 'Target',
				enabled: false
			}
		}, {
			endOnTick: false,
			title: {
				text: 'Actual',
				enabled: false
			}
		}],
		legend: {
			shadow: false,
			enabled: false
		},
		tooltip: {
			shared: true
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0
			}
		},
		series: [{
			name: 'Target',
			color: 'rgba(126,126,126,.7)',
			data: [],
			pointPadding: 0.0,
			pointPlacement: 0
		},{
			name: 'Actual',
			color: 'rgba(22,170,2,.7)',
			data: [],
			pointPadding: 0.1,
			pointPlacement: 0,
			dataLabels: { enabled: true }
		}]
	};

	$('#div_production_summaryA').highcharts(op);
	$('#div_production_summaryB').highcharts(op);


	$('#btn_select_tool').on('click', function() {
		event.preventDefault();

		$('#div_select_tool_modal').modal('show');
	});

	$('#ul_select_tools').on('click', '.btn-tool-select', function() {
		event.preventDefault();
		var d = $(this).data();
		processSelectedTool(d);
	});
});

function processSelectedTool(d) {
	$.growl(
		{"title":" Tool Selected ", "message":"Selected tool: " + d.tool_description, "icon":"glyphicon glyphicon-exclamation-sign"},
		{"type":"info", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}, "delay":1500}
	);

	current_tool = d;
	current_tool.parts = [];

	var part_list = [];
	for (var j = 0; j < all_data.parts.length; j++) {
		if (all_data.parts[j].tool_ID == d.tool_ID) {
			var z = current_tool.active_parts.findIndex(x => x.ID == all_data.parts[j].part_ID);
			current_tool.parts.push(all_data.parts[j]);
			if (z > -1) part_list.push(current_tool.active_parts[z].part_number);
		}
	}

	$('#btn_select_tool').html(current_tool.tool_description);
	$('#sp_current_part_number').html(part_list.join('<br>'));
	$('#div_current_tool DIV').show();

	$('#div_top5_defects').hide();

	$('#div_select_tool_modal').modal('hide');

	//TODO: make call to set current tool
	$.getJSON(ajax_procs.production, {
		"action":"set_current_tool",
		"machine_ID":all_data.machine[0].ID,
		"tool_ID":current_tool.tool_ID}
		, function() {
			console.log('Tool set!');
		}
	);

	//check work instructions
	var num_unseen_instructions = getUnseenWorkInstructions();

	if (num_unseen_instructions > 0) {
		displayCurrentWorkInstructions();
	}

	//startCycleTimer();
}

function updateProductionSummary() {
	//update production summary chart
	var cur_hour = parseInt(all_data.time_and_shift[0].cur_time.split(':')[0]);

	if (all_data.production.hourly_summary.length > 0) {
		for (var i = 0; i < all_data.production.hourly_summary.length; i++) {
			var chart = $('#div_production_summary' + String.fromCharCode(66 - i)).highcharts(); //the long and short shaft were backwards change to 66 'B' - i
			var hourly_summary_length = ((all_data.production.hourly_summary[i] == null) ? 0 : all_data.production.hourly_summary[i].length);
			var hour_labels = [];
			var hour_data = [];
			var hour_targets = [];

			for (var j = 0; j < 24 && j < hourly_summary_length; j++) {
				hour_labels.push(all_data.production.hourly_summary[i][j].interval_desc);
				hour_data.push(all_data.production.hourly_summary[i][j].part_qty);
				hour_targets.push(all_data.production.hourly_summary[i][j].part_goal); //part_target_qty);
			}
			chart.xAxis[0].setCategories(hour_labels, false);
			chart.series[0].setData(hour_targets);
			chart.series[1].setData(hour_data);
		}
	} else {
		//handle the single packout station some other day
	}
}

function updateShiftCounts() {
	if (all_data.production.shift_summary.length > 0) {
		for (var i = 0; i < all_data.production.shift_summary.length; i++) {
			var shift_summary_length = ((all_data.production.shift_summary[i] == null) ? 0 : all_data.production.shift_summary[i].length);
			//update shift counts
			$('#sp_shift_1' + String.fromCharCode(66 - i)).hide();
			$('#sp_shift_2' + String.fromCharCode(66 - i)).hide();
			$('#sp_shift_3' + String.fromCharCode(66 - i)).hide();
			for (var j = 0; j < shift_summary_length; j++) {
				$('#sp_shift_' + (j + 1) + String.fromCharCode(66 - i)).html('Shift ' + all_data.production.shift_summary[i][j].the_shift + ': ' + all_data.production.shift_summary[i][j].part_qty).show();
			}

			if (shift_summary_length > 0) {
				var cur_shift_data = all_data.production.shift_summary[i][0];
				$('#sp_summary_oee')
					.html((cur_shift_data.oee * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.oee, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.oee, 0.85));
				$('#sp_summary_availability')
					.html((cur_shift_data.availability * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.availability, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.availability, 0.85));
				$('#sp_summary_performance')
					.html((cur_shift_data.performance * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.performance, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.performance, 0.85));
				$('#sp_summary_quality')
					.html((cur_shift_data.quality * 100).toFixed(2) + '%' + getThumbForPercent(cur_shift_data.quality, 0.85))
					.attr("class", "badge " + getColorClassForPercent(cur_shift_data.quality, 0.85));
			}
		}
	} else {
		//handle the single packout station some other day
	}
}


function updateCycleData() {
	$('#sp_last_cycle_time')
		.html(all_data.cycle[0].last_cycle_time.toFixed(2) + getThumb(all_data.cycle[0].last_cycle_time, all_data.cycle[0].target_cycle_time))
		.attr("class", "badge " + getColorClass(all_data.cycle[0].last_cycle_time, all_data.cycle[0].target_cycle_time));
	$('#sp_avg_cycle_time')
		.html(all_data.cycle[0].avg_cycle_time.toFixed(2) + getThumb(all_data.cycle[0].avg_cycle_time, all_data.cycle[0].target_cycle_time))
		.attr("class", "badge " + getColorClass(all_data.cycle[0].avg_cycle_time, all_data.cycle[0].target_cycle_time));;
	$('#sp_target_cycle_time')
		.html(all_data.cycle[0].target_cycle_time.toFixed(2));
	$('#sp_low_cycle_time')
		.html(all_data.cycle[0].low_cycle_time.toFixed(2));
}


//PART/TOOL SELECTION
function selectTool() {
	//don't need to select a tool for packout, but this is what kicks everythign off

	return;

	/*if (all_data.tools.length == 1) {
		$('#btn_select_tool').prop('disabled', true);
		processSelectedTool(all_data.tools[0]);
		return;
	}

	$('#btn_select_tool').prop('disabled', false);

	$('#div_current_tool DIV').hide();

	var u = $('#ul_select_tools').html('');

	for (var i = 0; i < all_data.tools.length; i++) {
		var l = $('<LI />');
		var b = $('<BUTTON />').addClass('btn btn-tool-select').data(all_data.tools[i]);

		if (all_data.tools[i].tool_ID == all_data.machine[0].current_tool_ID) {
			b.addClass('current_tool');
		}

		var part_list = '';
		for (var j = 0; j < all_data.parts.length; j++) {
			if (all_data.parts[j].tool_ID == all_data.tools[i].tool_ID) {
				part_list += '<div>' + all_data.parts[j].part_desc + ' (' + all_data.parts[j].part_number + ')</div>';
			}
		}

		b.html('<div class="panel panel-primary">' +
				 '	<div class="panel-heading">' + all_data.tools[i].short_description + '</div>' +
				 '	<div class="panel-body"><b>' + all_data.tools[i].tool_description + '</b>' + part_list + '</div>' +
				 '</div>');
		l.append(b);
		u.append(l);
	}

	$('#div_select_tool_modal').modal('show');*/
}
//END OF PART/TOOL SELECTION


//CYCLE TIMER
function startCycleTimer() {
	if (cycle_timer) {
		clearTimeout(cycle_timer);
	}
    cycle_timer_count = all_data.machine[0].last_cycle_time_ago;

	cycle_timer = setTimeout(updateCycleTimer, 1000);
}

function updateCycleTimer() {
	cycle_timer_count++;

	var minutes = Math.floor(cycle_timer_count/60.0);
	var seconds = cycle_timer_count - (minutes * 60);
	if (seconds < 10) {
		seconds = '0' + seconds.toString();
	}

	var target = current_tool.target_cycle_time;
	var c = "";
	var f = 'white';
	var d = new Date();
	var s = d.getSeconds();

	if (target) {
		if (cycle_timer_count > target) {
			c = 'yellow';
			f = 'black';
		}
		if (cycle_timer_count > (target * 1.10)) {
			if ((s % 2) == 0) {
				c = 'red';
			} else {
				c = 'white';
				f = 'black';
			}
		}
	}

	$('#h_cycle_timer').html(minutes + ':' + seconds).css('background-color', c).css('color', f);
	//cycle_timer = setTimeout(updateCycleTimer, 1000);
}

//END OF CYCLE TIMER


//WORK INSTRUCTIONS
function updateWorkInstructions() {
	//all_data.work_instructions = [];
	$('#btn_work_instructions SPAN.badge').html(all_data.work_instructions.length);
	$('#btn_work_instructions').prop('disabled', (all_data.work_instructions.length == 0));
}

function displayCurrentWorkInstructions() {
	var ol = $('#div_instructions_carousel OL.carousel-indicators').html('');
	var slides = $('#div_instructions_carousel DIV.carousel-inner').html('');
	var jso = all_data.work_instructions;

	for (var i = 0; i < jso.length; i++) {
		var li = $('<LI />').data('target', '#div_instructions_carousel').data('slide-to', i).data('instr_ID', jso[i].ID);
		var item = $('<DIV />').addClass('item');

		var tbl = $('<TABLE />').appendTo(item).addClass('table table-bordered').css('width','90%').attr('align', 'center');
		var r = $('<TR />').appendTo(tbl);
		var top_title = $('<TD />').attr('colspan', 2).css('text-align', 'center').appendTo(r).html(jso[i].title);

		var r = $('<TR />').appendTo(tbl);
		var lh_side = $('<TD />').css('width','50%').css('font-size','24pt').css('text-align', 'center').appendTo(r).html(jso[i].instr_text);

		var rh_side = $('<TD />').css('width','50%').appendTo(r);
		var img = $('<IMG />').attr('src', '/machine_mes2/public/images/work_instructions/machine_' + all_data.machine[0].ID + '/' + jso[i].instr_image);
		img.css('min-height', '300px');
		img.appendTo(rh_side);

		var r = $('<TR />').appendTo(tbl);
		var signoff_time = '<span class="label label-warning">You are viewing this instruction for the first time.</span>';
		for (var j = 0; j < all_data.work_instruction_signoffs.length; j++) {
			var so = all_data.work_instruction_signoffs[j];
			if (jso[i].ID == so.instr_ID && so.operator_ID == operator.ID) {
				signoff_time = '<span class="label label-success">You viewed this instruction at: ' + so.signoff_time.date + '</span>';
			}
		}
		var bottom_info = $('<TD />').attr('colspan', 2).css('text-align', 'left').appendTo(r).html(signoff_time);

		if (i == 0) {
			li.addClass('active');
			item.addClass('active');
		}

		li.appendTo(ol);
		item.appendTo(slides);
	}

	if (jso.length == 1) {
		$('.carousel-control').hide();
		$('.carousel-indicators').hide();
	} else {
		$('.carousel-control').show();
		$('.carousel-indicators').show();
	}

	$('#div_instructions_carousel').carousel({"interval":false});

	$('#div_work_instruction_modal').modal('show');
}

function getUnseenWorkInstructions() {
	var res = 0;

	for (var i = 0; i < all_data.work_instructions.length; i++) {
		var wi = all_data.work_instructions[i];
		var seen = false;
		for (var j = 0; j < all_data.work_instruction_signoffs.length; j++) {
			var so = all_data.work_instruction_signoffs[j];
			if (wi.ID == so.instr_ID && so.operator_ID == operator.ID) {
				seen = true;
			}
		}
		if (!seen) {
			res++;
		}
	}

	return res;
}

function recordWorkInstructionSignoff() {
	var instr_ID = $('#div_instructions_carousel OL LI.active').data('instr_ID');
	console.log('Saw ' + instr_ID);
	$.getJSON(ajax_procs.production, {"action":"record_work_instruction_signoff", "instr_ID":instr_ID, "operator_ID":operator.ID}, afterRecordWorkInstructionSignoff);
}

function afterRecordWorkInstructionSignoff(jso) {
	$.growl(
		{"title":" Work Instruction Recorded ", "message":"Work instruction signoff recorded!", "icon":"glyphicon glyphicon-ok"},
		{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
	);
}
//END OF WORK INSTRUCTIONS

