var config_path = './config/displays.json';
var processor_path = './ajax_processors/displays_production_dashboard.php';
var config;

var machine_index = -1;
var machines = [];
var machine_interval;

var slides = [];
var slide_index = -1;
var slide_interval;

var carousel;

$(document).ready(function () {
	 carousel = $('#slideshow');

	 $.getJSON(config_path)
	  .done(function (data) {
		  config = data;
		  init_machines();
	  })
	  .fail(function () {
		  console.error('Unable to load config file.');
		  config = {production_dashboard: {update_interval: 60000, cycle_interval: 30000}};
		  init_machines();
	  });

	  $('.slideshow-container').on('slide.bs.carousel', '.carousel.slide', function (e) {
		  var slide_from = $(this).find('.item.active').index();
		  var slide_to = $(e.relatedTarget).index();
		  if (slide_to <= slide_from) {
				slideshow_finished();
		  } else {
				setTimeout(function () {carousel.carousel('next'); carousel.carousel('pause');}, slides[slide_to].duration*1000);
		  }
	  });

	 // refresh the page after 20 minutes
	 setTimeout(function () {window.location.reload(true);}, 1200000);
});

function init_machines () {
	 machine_dashboard_group_ID = $('#content').data('machine-dashboard-group-id');

	 load_machine_group(machine_dashboard_group_ID, function (data) {
		  machine_group_loaded(data);
		  cycle_machines();
		  machine_interval = setInterval(cycle_machines, config.production_dashboard.cycle_interval);
	 });
}

function show_machine_dashboard (show) {
	 if (show) {
		  $('.dashboard-container').show();
		  $('.header-data').show();
		  $('#page_title').show();
	 } else {
		  $('.dashboard-container').hide();
		  $('.header-data').hide();
		  $('#page_title').hide();
	 }
}

function cycle_machines () {
	 if (++machine_index >= machines.length) {
		  machine_index = -1;
		  clearInterval(machine_interval);
		  show_machine_dashboard(false);

		  // show the slide show if we've gone through all the machines in the group
		  init_slides();
	 } else {
		  show_machine_info(machines[machine_index]);
		  show_machine_statuses(machines);
	 }

}

function load_machine_group (machine_dashboard_group_ID, cb) {
	 $.getJSON(
		  processor_path,
		  {action: 'get_machine_group', machine_dashboard_group_ID: machine_dashboard_group_ID}
	 ).done(function (data) {
		  if (cb == null) {
				machine_group_loaded(data);
		  } else {
				cb(data);
		  }
	 }).fail(function () {
		  console.error('Unable to load machine group data.');
		  setTimeout(function () {load_machine_group (machine_dashboard_group_ID, cb)}, 5000);
	 });
}

function machine_group_loaded (data) {
	 if (data) {
		  machines = data;
	 } else {
		  machines = [];
	 }

	 show_machine_dashboard(true);
}

function show_machine_info (machine) {
	 try {
		  if (!machine) {throw {message: 'no machine info'};}

		  $('.machine_number').html(machine.machine_number ? machine.machine_number : 'None');
		  $('.machine_name').html(machine.machine_name ? machine.machine_name : 'None');
		  // $('.current_part_number').html(machine.current_part_number ? machine.current_part_number : 'None');

		  $('.total_produced').html(machine.shift_summary ? machine.shift_summary.part_qty : 0);
		  $('.total_scrap').html(machine.shift_summary ? machine.shift_summary.scrap : 0);
		  $('.total_downtime').html(machine.shift_summary ? Math.ceil(machine.shift_summary.downtime_minutes) : 0);

		  build_chart('production_chart', machine.hourly_summary);
		  build_gauges(machine.shift_summary);
	 } catch (err) {
		  console.error(err.message);
	 }
}

function build_chart (element_id, data) {
	 $('#'+element_id).empty();

	 try {
		  var target = [];
		  var produced = [];
		  var categories = [];

		  if (data.summary) {
				for (var i=0; i<24; i++) {
					 var entry = data.summary.pop();
					 if (entry) {
						 target.push(Math.ceil(entry.part_goal));
						 produced.push(entry.part_qty);

						 //var hour_label;
						 //if (entry.the_hour === 0) {
							//  hour_label = (entry.the_hour + 12) + 'am';
						 //} else if (entry.the_hour < 12) {
							//  hour_label = entry.the_hour + 'am';
						 //} else if (entry.the_hour === 12) {
							//  hour_label = entry.the_hour + 'pm';
						 //} else {
							//  hour_label = (entry.the_hour - 12) + 'pm';
						 //}
						 //categories.push(hour_label);
						 categories.push(entry.interval_desc);
					 }
				}
		  } else {
				console.error('ERROR: no hourly data');
		  }

		  //hourly production report
		  var font_size = '24px';

		  var series = [
				{
					 name: 'Target',
					 data: target,
					 pointPadding : 0,
					 pointPlacement : 0,
					 color : 'rgba(0, 100, 200, 0.7)'
				},
				{
					 name: 'Produced',
					 data: produced,
					 pointPadding : 0.2,
					 pointPlacement : 0,
					 dataLabels: {
						  enabled: true,
						  color: '#FFFFFF',
						  align: 'center',
						  format: '{point.y:.0f}', // one decimal
						  y: -5, // 10 pixels down from the top
						  style: {
								fontSize: font_size,
								fontFamily: 'monospace'
						  }
					 },
					 color : 'rgba(200, 100, 0, 0.7)'
				}
		  ];

		  var options = {
				credits : {
					 enabled : false
				},
				chart : {
					 type : 'column'
				},
				title : {
					 text : null
				},
				xAxis : {
					 title : {
						  text : null
					 },
					 categories : categories,
					 labels: {
						  style: {
								fontSize: font_size,
								fontFamily: 'monospace'
						  }
					 }
				},
				yAxis : {
					 min : 0,
					 title : {
						  text : null
					 },
					 labels: {
						  style: {
								fontSize: font_size,
								fontFamily: 'monospace'
						  }
					 }
				},
				series : series,
				subtitle : {
					 text : null
				},
				legend : {
					 enabled : false
				},
				plotOptions : {
					 column : {
						  grouping : false,
						  shadow : false,
						  borderWidth : 0
					 }
				},
		  }

		  $('#'+element_id).highcharts(options);
	 } catch (err) {
		  console.error(err.message);
	 }
}

function show_machine_statuses (data) {
	if (data.statuses && data.statuses.length > 0) {
		$('#machine_statuses').empty();
		$.map(data.statuses, function (v) {
			if (v.warning != null) {
				$('#machine_statuses').append('<div class="alert alert-danger text-center"><span class="glyphicon glyphicon-warning-sign"></span><b>' + v.warning + '</b></div>');
			}
		});
	}
	 //$('#machine_statuses').empty();
	 //try {
		//  if (!data) {throw {message: 'no machines'};}
	 //
		//  for (var i=0; i<data.length; i++) {
		//		show_machine_status(data[i], i == machine_index);
		//  }
	 //} catch (err) {
		//  console.error(err.message);
	 //}
}

function show_machine_status (machine, current_machine) {
	 try {
		  if (!machine) {throw {message: 'no machine data'};}

		  var status_color;
		  var status_degrees = 0;
		  var bc = '';

		  if (machine.status === 0) {
				status_color = 'red';
				status_degrees = 90;
				bc = 'red';
		  } else if (machine.status === 1) {
				status_color = 'green';
				status_degrees = 225;
				bc = 'limegreen';
		  } else {
				status_color = 'yellow';
				status_degrees = 135;
				bc = 'yellow';
		  }

		  var machine_div = $('<div class="text-center machine-graphic-container"></div>');

		  var machine_label = $('<div><span class="machine-label">'+machine.machine_name+'</span></div>');
		  if (current_machine) {
				machine_label.find('.machine-label').addClass('current-machine');
		  }
		  machine_div.append(machine_label);

		  var img_markup = '<div class="machine-image-container" style="margin-top: 10px;">' +
								 '    <div class="color-overlay"></div>' +
								 '</div>';
		  var img = $(img_markup).css('background', 'url(./public/images/machine_images/'+machine.machine_number+'.PNG) no-repeat')
										 .css('background-size', '100% 100%')
										 .css('background-position', 'center');
		  //img.find('.color-overlay').addClass(status_color);
		  //img.css('filter', 'hue-rotate(' + status_degrees + ')');
		  img.css('background-color', bc);
		  img.css('border-radius', '10px');
		  if (current_machine) {
		  	  img.css('box-shadow', '0px 0px 20px #CCCCFF');
		  }
		  machine_div.append(img);

		  $('#machine_statuses').append(machine_div);
	 } catch (err) {
		  console.error(err.message);
	 }
}

function build_gauges (shift_summary) {
	 if (!shift_summary) {console.error('no shift summary');}

	 var gauge_data = [
		  {name: 'oee', title: 'OEE', value: shift_summary ? Math.round(shift_summary.oee * 100) : 0},
		  {name: 'availability', title: 'Availability', value: shift_summary ? Math.round(shift_summary.availability * 100) : 0},
		  {name: 'quality', title: 'Quality', value: shift_summary ? Math.round(shift_summary.quality * 100) : 0},
		  {name: 'performance', title: 'Performance', value: shift_summary ? Math.round(shift_summary.performance * 100) : 0}
	 ];

	 for(var i=0; i<gauge_data.length; i++) {
		  build_gauge(gauge_data[i]);
	 }
}

function build_gauge (data) {
	 var series = [{
		  name: data.title,
		  data: [data.value],
		  dataLabels: {
				format: '<div style="text-align:center"><span class="gauge-value-text">{y}</span>' +
						 '<span class="gauge-value-sign">%</span></div>'
		  }
	 }];

	 var gaugeOptions = {

		  chart: {
				type: 'solidgauge'
		  },

		  title: null,

		  pane: {
				center: ['50%', '85%'],
				size: '140%',
				startAngle: -90,
				endAngle: 90,
				background: {
					 backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
					 innerRadius: '60%',
					 outerRadius: '100%',
					 shape: 'arc'
				}
		  },

		  tooltip: {
				enabled: false
		  },

		  // the value axis
		  yAxis: {
				stops: [
					 [0.85, '#DF5353'], // red
					 [0.90, '#DDDF0D'], // yellow
					 [1,'#55BF3B'] // green
				],
				lineWidth: 0,
				minorTickInterval: null,
				tickPixelInterval: 400,
				tickWidth: 0,
				title: {
					 y: -70
				},
				labels: {
					 y: 16
				}
		  },

		  plotOptions: {
				solidgauge: {
					 dataLabels: {
						  y: 5,
						  borderWidth: 0,
						  useHTML: true
					 }
				}
		  }
	 };

	 $('#'+data.name+'_gauge').highcharts(Highcharts.merge(gaugeOptions, {
		  yAxis: {
				min: 0,
				max: 100,
				title: null
		  },

		  credits: {
				enabled: false
		  },

		  series: series

	 }));
}


function init_slides () {
	 $('.dashboard-container').hide();
	 load_slides(slides_loaded);
}

function show_slideshow (show) {
	 if (show) {
		  $('.slideshow-container').show();
	 } else {
		  $('.slideshow-container').hide();
	 }
}

function slideshow_finished () {
	 clearInterval(machine_interval);
	 show_slideshow(false);
	 carousel.remove();
	 init_machines();
}

function load_slides (cb) {
	 $.getJSON(
		  processor_path,
		  {action: 'get_slides', machine_dashboard_group_ID: machine_dashboard_group_ID, active: 1}
	 ).done(function (data) {
		  if (cb == null) {
				slides_loaded(data);
		  } else {
				cb(data);
		  }
	 }).fail(function () {
		  console.error('Unable to load slides.');
		  setTimeout(function () {load_slides (cb)}, 10000);
	 });
}

function slides_loaded (data) {
	 if (data) {
		  slides = data;
		  build_carousel(slides);
		  show_slideshow(true);
		  if (slides.length > 1) {
				setTimeout(function () {carousel.carousel('next'); carousel.carousel('pause');}, slides[0].duration*1000);
		  } else {
				setTimeout(slideshow_finished, slides[0].duration*1000);
		  }

	 } else {
		  slideshow_finished();
	 }



	 // start slideshow
}

function build_carousel (data) {
	 carousel.remove();
	 carousel = $('<div></div>').prop({id: 'slideshow'})
										 .addClass('carousel slide')
										 .appendTo($('.slideshow-container'));
	 var carousel_inner = $('<div></div>').addClass('carousel-inner').appendTo(carousel);

	 for (var i=0; i<slides.length; i++) {
		  var item = $('<div></div>').addClass('item').appendTo(carousel_inner);
		  if (i === 0) {
				item.addClass('active');
		  }
		  var image = $('<img src="'+slides[i].image_src+'">').appendTo(item);
	 }

	 carousel = carousel.carousel();
}
