var sel_data_point = {};
var NUM_DATA_POINTS = 5;

$(function() {
	$('.btn-spc').on('click', function() {
		event.preventDefault();
		$('#div_spc_modal .modal-title').html('SPC Data Analysis - Select Data Point');
		$('#div_spc_modal').modal('show');
		loadDataPoints();
	});

	$('#div_spc_modal .modal-body').on('click', '.btn-data-point-select', function() {
		sel_data_point = $(this).data();
		selectDataPoint();
	});
	
	

	$('#div_spc_modal .modal-title').on('click', '.btn-change-data-point', function() {
		$('#div_spc_modal .modal-title').html('SPC Data Analysis - Select Data Point');
		loadDataPoints();
	});
	
	//comments
	$('#div_spc_modal .modal-body').on('click', '.add-comment', function() {
		$('#inp_comment').val('');
		if ($(this).data('comment')) {
			$('#inp_comment').val($(this).data('comment'));
		}
		$('#div_modal_spc_comment').modal('show').data($(this).data());
	});
	
	$('#div_modal_spc_comment').on('shown.bs.modal', function() { $('#inp_comment').focus(); });

	$('BUTTON.btn-save-comment').on('click', function() {
		$('#comment_' + $('#div_modal_spc_comment').data('ID')).attr('data-original-title', $('#inp_comment').val()).attr('title', $('#inp_comment').val()).tooltip().removeClass('no-comment').addClass('has-comment').data('comment', $('#inp_comment').val());
		if ($('#inp_comment').val() == '') {
			$('#comment_' + $('#div_modal_spc_comment').data('ID')).removeClass('has-comment').addClass('no-comment');
		}
		$.getJSON(ajax_procs.SPC, {
				"action":"save_comment",
				"auto_gauge":sel_data_point.auto_gauge,
				"use_mmq":sel_data_point.use_mmq,
				"record_ID":$('#div_modal_spc_comment').data('ID'),
				"comment":$('#inp_comment').val()
			}, function() {
				$.growl('Comment saved!', {type: 'success'});
				$('#div_modal_spc_comment').modal('hide');
			}
		);
	});
});

function loadDataPoints() {
	$('#div_spc_modal .modal-body').html('Loading data points...');

	var u = $('<UL />').prop('id', 'ul_select_data_points');
	for (var i = 0; i < all_data.data_points.length; i++) {
		var l = $('<LI />').appendTo(u);
		var b = $('<BUTTON />').addClass('btn btn-data-point-select').data(all_data.data_points[i]);
		b.html('<div class="panel panel-primary">' +
				 '	<div class="panel-heading">' + all_data.data_points[i].instruction_description + '</div>' +
				 '</div>');
		l.append(b);
	}
	$('#div_spc_modal .modal-body').html(u);
}

function selectDataPoint() {
	$('#div_spc_modal .modal-title').html('SPC Data Analysis - Selected Data Point: ' + sel_data_point.instruction_description);
	$('<SPAN />').addClass('pull-right btn btn-xs btn-primary btn-change-data-point').html('Change Data Point').appendTo($('#div_spc_modal .modal-title'));
	var params = {"action":"run_report",
					  "tool_ID":current_tool.tool_ID,
					  "data_point_ID":sel_data_point.ID,
					  "auto_gauge":sel_data_point.auto_gauge,
					  "use_mmq":sel_data_point.use_mmq,
					  "data_point_desc":sel_data_point.instruction_description
					};
	$('#div_spc_modal .modal-body').html('Loading report...');
	$.getJSON(ajax_procs.SPC, params, showReport);
}

function showReport(data) {
	data = data.results || [];
	data = rotateData(data, NUM_DATA_POINTS);	
	
	var tol_min = sel_data_point.tolerance_target - sel_data_point.tolerance_delta;
	var tol_max = sel_data_point.tolerance_target + sel_data_point.tolerance_delta;

	var b = $('#div_spc_modal .modal-body').empty()
		.html('<table id="spc_table" class="table table-striped table-clickable table-hover table-bordered table-condensed">' +
				'	<thead>' +
				'		<tr>' +
				'			<th rowspan="2">Part</th>' +
				'			<th colspan="10" class="span_all">Instances</th>' +
				'		</tr>' +
				'		<tr id="tr_instance_numbers"></tr>' +
				'	</thead>' +
				'	<tbody id="parts_body"></tbody>' +
				'	<thead>' +
				'		<tr>' +
				'			<th>Legend</th>' +
				'			<th colspan="10" class="span_all">' +
				'				<span class="over_tol" style="padding:1px 4px;margin:0px 10px;">Over Tolerance</span>' +
				'				<span class="under_tol" style="padding:1px 4px;margin:0px 10px;">Under Tolerance</span>' +
				'				<span class="over_xbar_ucl" style="padding:1px 4px;margin:0px 10px;">Over xBar UCL</span>' +
				'				<span class="under_xbar_lcl" style="padding:1px 4px;margin:0px 10px;">Under xBar LCL</span>' +
				'				<span class="over_range_ucl" style="padding:1px 4px;margin:0px 10px;">Over Range UCL</span>' +
				'				<span class="under_range_lcl" style="padding:1px 4px;margin:0px 10px;">Under Range LCL</span>' +
				'			</th>' +
				'		</tr>' +
				'	</thead>' +
				'	<tbody>' +
				'		<tr>' +
				'			<td><b style="font-size:7pt;">Saved <br>Control <br> Limits </b></td>' +
				'			<td colspan="11" class="span_all" id="td_process_capability"></td>' +
				'		</tr>' +
				'	</tbody>' +
				'</table>');

	$('<DIV />').prop('id', 'div_chart_xbar').appendTo(b);
	$('<DIV />').prop('id', 'div_chart_range').appendTo(b);

	$('#td_process_capability').empty();
	if (sel_data_point.xbar_ucl != null) {
		var t = $('<TABLE />').css('width', '100%');
		var r = $('<tr />').appendTo(t);
		$('<td />').appendTo(r).html('<b>UCL-Xbar: </b>' + sel_data_point.xbar_ucl.toFixed(6));
		$('<td />').appendTo(r).html('<b>LCL-Xbar:</b> ' + sel_data_point.xbar_lcl.toFixed(6));
		$('<td />').appendTo(r).html('<b>UCL-Range:</b> ' + sel_data_point.range_ucl.toFixed(6));
		$('<td />').appendTo(r).html('<b>LCL-Range:</b> ' + sel_data_point.range_lcl.toFixed(6));
		$('#td_process_capability').html(t);
	}

	var tr = $('#tr_instance_numbers').empty();
	var values = [];
	var chart_series = {
		"name":"Avg",
		"data":[]
	};
	var chart_ucl = {
		"name":"UCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};
	var chart_lcl = {
		"name":"LCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};

	var chart_mean = {
		"name":"Mean",
		"data":[],
		"dashStyle": "Dash",
		"color": "black",
		"marker": {
			"enabled":false
		}
	};

	var chart_range_series = {
		"name":"Range",
		"data":[]
	};

	var chart_range_ucl = {
		"name":"UCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};

	var chart_range_mean = {
		"name":"Mean",
		"data":[],
		"dashStyle": "Dash",
		"color": "black",
		"marker": {
			"enabled":false
		}
	};

	var ranges = [];
	var xBars  = [];
	var dp_names = [];
	var split_date = '';
	var time_split = '';
	var total = 0.0;

	$('.span_all').attr('colSpan', data[0].length);

	for (var i = 0; i < data[NUM_DATA_POINTS + 4].length; i++) {

		$('<th />').html(i+1).appendTo(tr);

		dp_names.push(data[NUM_DATA_POINTS + 4][i]);
	}

	var tb = $('#parts_body').empty();

	//LOOP THROUGH EACH SET OF DATA POINTS
	for (var i = 0; i < NUM_DATA_POINTS; i++) {
		var tr = $('<tr />').appendTo(tb);
		var td = $('<td />').appendTo(tr).html(i + 1);
		var data_row = data[i];

		for (var j = 0; j < data_row.length; j++) {
			var dp = data_row[j];  //the data point record
			var comment_btn = $('<div />').css('font-size', '7pt').addClass('btn btn-xs btn-default glyphicon glyphicon-comment add-comment no-comment').data(dp).prop('id', 'comment_' + dp.ID);
			if (dp.comment) {
				if (dp.comment != null && dp.comment != '') {
					comment_btn.addClass('has-comment').removeClass('no-comment').attr('title', dp.comment).tooltip();
				}
			}
			var td = $('<td />').html('<div class="tt" title="' + dp.cycle_time.date + ' Serial: ' + dp.serial_number + '">' + dp.data_point_value + '</div>').append(comment_btn).appendTo(tr);
			if (dp.data_point_value < tol_min) {
				td.addClass('under_tol');
			}
			if (dp.data_point_value > tol_max) {
				td.addClass('over_tol');
			}
		}
	}

	//MIN
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('MIN').css('border-top', '2px solid black');
	for (var i = 0; i < data[NUM_DATA_POINTS+2].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+2][i]).appendTo(tr).css('border-top', '2px solid black');
	}

	//MAX
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('MAX');
	for (var i = 0; i < data[NUM_DATA_POINTS+3].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+3][i]).appendTo(tr);
	}

	//RANGE
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('Range');
	var total_of_ranges = 0.0;
	for (var i = 0; i < data[NUM_DATA_POINTS+1].length; i++) {
		var range_val = data[NUM_DATA_POINTS+1][i];
		$('<td />').html(range_val.toFixed(6)).appendTo(tr).addClass(getAlertClassRange(range_val));
		total_of_ranges += range_val;

		var point = {"y":range_val};
		if (parseFloat(sel_data_point.range_ucl) < range_val) {
			point.marker = {"fillColor": "red"};
			point.color = "red";
			//if (cur_instances_info[j].instance_comment != null)
			point.name = ''; //cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name;
				}
			};
		} else if (parseFloat(sel_data_point.range_lcl) > range_val) {
			point.marker = {"fillColor": "red"};
			point.color = "red";
			//if (cur_instances_info[j].instance_comment != null)
			point.name = ''; //cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name;
				}
			};
		}

		chart_range_series.data.push(point);
		chart_range_ucl.data.push(sel_data_point.range_ucl);
	}
	var average_of_ranges = total_of_ranges / data[NUM_DATA_POINTS+1].length;
	$('<span />').addClass('glyphicon glyphicon-info-sign tt').attr('title', 'Average of ranges: ' + average_of_ranges).appendTo(td);


	//AVERAGES
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('xBar');
	var total_of_averages = 0.0;
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		var avg_val = data[NUM_DATA_POINTS][i];
		$('<td />').html(avg_val.toFixed(6)).appendTo(tr).addClass(getAlertClassXBAR(avg_val));
		total_of_averages += avg_val;
		var point = {
			"y":avg_val
		};

		if (parseFloat(avg_val) > sel_data_point.xbar_ucl) {
			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = '';
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};

		} else if (parseFloat(avg_val) < sel_data_point.xbar_lcl) {
			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = '';
			point.dataLabels = {
				"align": "left",
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};
		}
		chart_series.data.push(point);
		chart_ucl.data.push(sel_data_point.xbar_ucl);
		chart_lcl.data.push(sel_data_point.xbar_lcl);
	}
	var average_of_averages = total_of_averages / data[NUM_DATA_POINTS].length;
	$('<span />').addClass('glyphicon glyphicon-info-sign tt').attr('title', 'Average of averages: ' + average_of_averages).appendTo(td);


	//Standard Deviation
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('&sigma;');
	for (var i = 0; i < data[0].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+6][i].toFixed(6)).appendTo(tr);
	}


	//charting starts here!
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		chart_mean.data.push(average_of_averages);
	}
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		chart_range_mean.data.push(average_of_ranges);
	}


	var label = '<strong>Mean xBar</strong>: ' + average_of_averages.toFixed(6) + ' <strong>UCL-xBar</strong>: ' + sel_data_point.xbar_ucl.toFixed(6) +
					' <strong>LCL-xBar</strong>: ' + sel_data_point.xbar_lcl.toFixed(6);

	console.log('dp_names:');
	console.log(dp_names);
	console.log('chart_series.data:');
	console.log(chart_series.data);

	var chart_xbar = new Highcharts.Chart({
			"chart": {
				"renderTo": "div_chart_xbar",
				"type": "line",
				"borderWidth":1,
				"marginBottom": 130,
				"borderRadius":5
			},
			"title": {
				"margin": 25,
				"text":"SPC Xbar Chart: " + sel_data_point.instruction_description
			},
			"subtitle": {
				"text":label,
				"useHTML":true
			},
			"xAxis": {
				"categories": dp_names,
				"labels": {
					"rotation": 45,
					"y": 10
				},
				"max": data[0].length - 1,
				"maxPadding": 0.2,
				"minPadding": 0.2
			},
			"yAxis": {
				"title": {
					"text": 'mm'
				}
			},
			"tooltip": {
				"crosshairs": [true, true]
			},
			"plotOptions":{
				"line":{
					"marker":{
						"symbol":"circle"
					}
				},
				"series":{
					"lineWidth":3,
					"color":"rgb(69, 114, 167)"
				}
			},
			"legend": {
				"align": 'right',
				"verticalAlign": 'top',
				"backgroundColor":"white",
				"x": -10,
				"y": 40,
				"shadow": true,
			},
			"series": [chart_ucl, chart_lcl, chart_mean, chart_series],
			"credits": {
				"text": "Nysus",
				"href": "http://www.nysus.net"
			}
		});
	label = '<strong>Mean Range</strong>: ' + average_of_ranges.toFixed(6) + ' <strong>Range UCL</strong>: ' + sel_data_point.xbar_ucl.toFixed(6);

	console.log(chart_range_series);

	var chart_range = new Highcharts.Chart({
			"chart": {
				"renderTo": "div_chart_range",
				"type": "line",
				"borderWidth":1,
				"marginBottom": 130,
				"borderRadius":5
			},
			"title": {
				"margin": 25,
				"text":"SPC Range Chart: " + sel_data_point.instruction_description
			},
			"subtitle": {
				"text":label,
				"useHTML":true
			},
			"xAxis": {
				"categories": dp_names,
				"labels": {
					"rotation": 45,
					"y": 10
				},
				"max": data[0].length - 1,
				"maxPadding": 0.2,
				"minPadding": 0.2
			},
			"yAxis": {
				"title": {
					"text": 'mm'
				}
			},
			"tooltip": {
				"crosshairs": [true, true]
			},
			"plotOptions":{
				"line":{
					"marker":{
						"symbol":"circle"
					}
				},
				"series":{
					"lineWidth":3,
					"color":"rgb(69, 114, 167)"
				}
			},
			"legend": {
				"align": 'right',
				"verticalAlign": 'top',
				"backgroundColor":"white",
				"x": -10,
				"y": 40,
				"shadow": true,
			},
			"series": [chart_range_ucl, chart_range_series, chart_range_mean],
			"credits": {
				"text": "Nysus",
				"href": "http://www.nysus.net"
			}
		});

	$('#spc_table .tt').tooltip({"html":true});
}






//FROM SPC SYSTEM IN THE MANAGER:

function getAlertClassXBAR(val) {
	if (val > (sel_data_point.tolerance_target + sel_data_point.tolerance_delta)) { return 'over_tol';	}
	if (val < (sel_data_point.tolerance_target - sel_data_point.tolerance_delta)) { return 'under_tol';	}
	if (val > sel_data_point.xbar_ucl) { return 'over_xbar_ucl'; }
	if (val < sel_data_point.xbar_lcl) { return 'under_xbar_lcl'; }
	return '';
}

function getAlertClassRange(val) {
	if (val > sel_data_point.range_ucl) { return 'over_range_ucl'; }
	if (val < sel_data_point.range_lcl) { return 'under_range_lcl'; }
	return '';
}

function rotateData(data, num_to_rotate) {
	while (data.length % num_to_rotate != 0) {
		data.splice(data.length - 1, 1);
	}
	var ret = [];
	for (var i = 0; i < num_to_rotate; i++) {
		ret.push([]);
	}
	ret.push([]); //for the averages
	ret.push([]); //for the ranges
	ret.push([]); //for the mins
	ret.push([]); //for the maxs
	ret.push([]); //for the max cycle time
	ret.push([]); //for the max serial
	ret.push([]); //for the standard deviation

	for (var i = 0; i < data.length; i++) {
		var r = parseInt(data[i].Row) - 1;
		ret[r % num_to_rotate].push(data[i]);
	}

	//calculate the averages
	for (var i = 0; i < ret[0].length; i++) {
		var total = 0.0;
		var min = 99999;
		var max = -99999;
		var cycle_time = '';
		var serial = '';
		var arr = [];

		for (var j = 0; j < num_to_rotate; j++) {
			//console.log('Adding row ' + ret[j][i].Row + ' to total');
			total += parseFloat(ret[j][i].data_point_value);

			if (parseFloat(ret[j][i].data_point_value) > max) {
				max = parseFloat(ret[j][i].data_point_value);
			}
			if (parseFloat(ret[j][i].data_point_value) < min) {
				min = parseFloat(ret[j][i].data_point_value);
			}

			if (j == 0) {
				cycle_time = ret[j][i].cycle_time.date;
				serial = ret[j][i].serial_number;
			}

			arr.push(ret[j][i].data_point_value);
		}
		//console.log('Total: ' + total);
		var avg = total / num_to_rotate;
		ret[num_to_rotate].push(avg);  //add the average to the array
		ret[num_to_rotate + 1].push(max-min);  //add the range to the array
		ret[num_to_rotate + 2].push(min);  //add the min to the array
		ret[num_to_rotate + 3].push(max);  //add the max to the array
		ret[num_to_rotate + 4].push(cycle_time);  //add the first cycle_time to the array
		ret[num_to_rotate + 5].push(serial);  //add the first serial to the array
		ret[num_to_rotate + 6].push(standardDeviation(arr));  //add the standard deviation to the array
	}

	return ret;
}

function standardDeviation(values){
  var avg = average(values);

  var squareDiffs = values.map(function(value){
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });

  var avgSquareDiff = averageOfSample(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

function averageOfSample(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / (data.length - 1);
  return avg;
}