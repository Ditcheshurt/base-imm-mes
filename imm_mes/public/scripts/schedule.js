var TOOL_CHANGE_TIME = 60 * 30;

function updateSchedule() {
	var b = $('#div_schedule');

	b.html('');

	if (all_data.schedule.length == 0) {
		b.html('<i>No schedules defined.</i>');
	}

	var x = 0;
	var now = new Date();
	var total_seconds = 0;
	for (var i = all_data.schedule.length - 1; i >= 0; i--) {
		var s = all_data.schedule[i];
		var d = $('<DIV />').addClass('schedule');
		if (x == 0) {
			d.html(s.tool_description + '<span class="badge pull-right">Qty: ' + s.current_parts_in_run + ' of ' + s.cycle_qty + '</span>');
		} else {
			d.html(s.tool_description + '<span class="badge pull-right">Qty: ' + s.cycle_qty + '</span>');
		}
		d.css('height', ((s.cycle_qty/10)+20) + 'px');

		var remaining_parts = s.cycle_qty;
		if (x == 0) {
			d.addClass('current-schedule');
			var d2 = $('<DIV />').addClass('schedule-progress').appendTo(d);
			var percent_through = s.current_parts_in_run / s.cycle_qty * 100.0;
			if (percent_through > 100) {
				percent_through = 100;
			}
			d2.css('width', percent_through + '%');

			remaining_parts = s.cycle_qty - s.current_parts_in_run;
		}

		total_seconds += remaining_parts * s.target_cycle_time;
		var expected_time = Date.DateAdd('s', total_seconds, now);

		b.append(d);

		if (i > 0) {
			$('<DIV />').addClass('tool-change').html('Tool change at approx.<br>' + Date.Format(expected_time, 'C')).appendTo(b);
			total_seconds += TOOL_CHANGE_TIME;
		}
		x++;
	}
}