$(document).ready(function() {

	$('.cstat-toggle').on('click', function() {
		if ($('#cstat').hasClass('disabled')) {
			return;
		} else {
			if ($('#cstat').hasClass('open')) {
				$('#cstat').removeClass('open');
			} else {
				$('#cstat').addClass('open');
			}
		}
	});

	/**/
	$(document.body).click(function(e) {
		if (($(e.target).parents('#cstat').length || $(e.target).parents('.cstat-toggle').length)) {
			return;
		}
		if ($('#cstat').hasClass('open') === true ) {
			$('#cstat').removeClass('open');
		}
	});
	
	
	$('.sched-toggle').on('click', function() {
		if ($('#sched').hasClass('disabled')) {
			return;
		} else {
			if ($('#sched').hasClass('open')) {
				$('#sched').removeClass('open');
			} else {
				$('#sched').addClass('open');
			}
		}
	});

	/*
	$(document.body).click(function(e) {
		if ($(e.target).parents('#sched').length || $(e.target).parents('.sched-toggle').length) {
			return;
		}
		if ($('#sched').hasClass('open') === true) {
			$('#sched').removeClass('open');
		}
	});
	*/

});
