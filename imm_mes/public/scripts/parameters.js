//parameters

$(function () {


	$('.btn-parameters').on('click', function() {
		event.preventDefault();

		$('.parameter-container').remove();

		$('#div_parameter_modal').modal('show');

		$.getJSON(ajax_procs.parameters, {"action":"get_data_points"}, populateDataPoints);
	});

	$('#div_parameters').on('click', '.btn-select-data-point', function(e) {
		$(this).toggleClass('btn-info').toggleClass('btn-success');

		if ($(this).hasClass('btn-success')) {
			generateParameterChart($(this).data());
		} else {
			$('#div_chart_' + $(this).data('ID')).remove();
		}
	});
});

function populateDataPoints(jso) {
	var d = $('#div_parameters').empty();

	for (var i = 0; i < jso.length; i++) {
		var b = $('<button />').addClass('btn btn-info btn-xs btn-select-data-point').html(jso[i].data_point_name).data(jso[i]).appendTo(d);
		b.css('margin', '5px');
	}
}

function generateParameterChart(d) {
	var data = {};
	var ucl = 1000;
	var lcl = 0;
	
	var cont = $('#div_chart_' + d.ID);
	if (cont.length == 0) {
		cont = $('<div />').attr('id', 'div_chart_' + d.ID).appendTo($('#div_parameter_chart')).addClass('parameter-container');
	}
	
	$.getJSON(ajax_procs.parameters, {"action":"get_data_point_values","data_point_name":d.data_point_name},
		function(jso){
			data = jso.data_pts;
			if (d.ucl) {
				ucl = d.ucl;
			} else {
				ucl = jso.ctrl_lmts[0].ucl;
			}
			if (d.lcl) {
				lcl = d.lcl;
			} else {
				lcl = jso.ctrl_lmts[0].lcl;
			}
			
			cont.highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: d.data_point_name,
					enabled: true
				},
				credits: {
					enabled: false
				},
				xAxis: {
					categories: [],
					labels: {enabled: false},
					tickInterval: 20,
					visible: false
				},
				yAxis: {
					plotBands: [
						{
							from: ucl,
							to: 10000,
							color: 'rgba(255, 0, 0, 0.1)'
						},
						{
							from: -10000,
							to: lcl,
							color: 'rgba(255, 0, 0, 0.1)'
						}
					]
				},
				legend: {
					shadow: true,
					enabled: true
				},
				tooltip: {
					shared: true
				},
				plotOptions: {
					line: {
						grouping: false,
						shadow: true,
						borderWidth: 0
					}
				},
				series: [{
					name: 'Values',
					color: 'rgba(22,170,2,.7)',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					point: {
						events: {
							mouseOver: function() {
								console.log('x: ' + this.x + ', y: ' + this.y);
								console.log(this);
							}
						}
					}
				},{
					name: 'Upper Limit',
					color: 'red',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				},{
					name: 'Lower Limit',
					color: 'red',
					data: [],
					pointInterval: 10,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				}]
			});


			var arr = [];
			var arr2 = [];
			var arr3 = [];

			for (var i = 0; i < data.length; i++) {
				var v = data[i].data_point_value;
				var c = 'limegreen';
				if (v < lcl || v > ucl) {
					c = 'red';
				}
				arr.push({y:parseFloat(v.toFixed(3)), name:(i + ' shots ago'), color:c});
				arr2.push(ucl);
				arr3.push(lcl);
			}
			var chart = cont.highcharts();
			chart.setTitle(d.data_point_name);
			chart.series[0].setData(arr);
			chart.series[1].setData(arr2);
			chart.series[2].setData(arr3);
			chart.redraw();
		});

}