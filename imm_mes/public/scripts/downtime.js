//handles all downtime stuff
var cur_dt_codes = [];
var cur_dt_id = 0;

$(function() {

	$('#tbl_downtime TBODY').on('click', '.btn-downtime', function(e) {
		e.preventDefault();
		cur_dt_id = $(this).data('ID');
		$('#div_downtime_save').html('<span class="glyphicon glyphicon-floppy-disk"></span> Save Downtime Reason').attr('disabled', false);
		$('#div_dt_reason').html('<i>Loading reason codes...</i>');
		$('#txt_dt_reason').val('');
		$('#div_downtime_save').prop('disabled', true);
		$('#div_modal_downtime').modal('show');
	});

	$('#tbl_downtime TBODY').on('click', '.btn-end-downtime', function(e) {
		e.preventDefault();

		var d = $(this).data();
		//show modal
		$('#div_end_downtime_modal').data(d);

		//need supervisor permission to use this!
		auth_check(5, ' End Downtime', function() {
			$('#div_end_downtime_modal').modal('show');
		});
	});

	$('.btn-confirm-end-downtime').on('click', function(e) {
		e.preventDefault();

		var d = $('#div_end_downtime_modal').data();

		$.getJSON(ajax_procs.downtime, {
				"action":"end_downtime",
				"downtime_ID":d.ID
			}, function(jso) {
					$.growl("Downtime ended!", {"type":"success"});
					$('#div_end_downtime_modal').modal('hide').removeData();
					loadOpenDowntime();
			});
	});

	$('#div_modal_downtime').on('shown.bs.modal', function (e) {
		e.preventDefault();

		$.ajax({
			"url": ajax_procs.downtime,
			"type": "POST",
			"dataType": "json",
			"cache": false,
			"data": {
				"api":"entry",
				"action": "get_dt_info",
				"id":cur_dt_id,
				"machine_type_ID":all_data.machine[0].machine_type_ID
			},
			"success": function(ret) {
				//expecting code_info array and dt_info array
				if (!ret.dt_info[0].tool_description) {
					$('#div_dt_machine').html(ret.dt_info[0].machine_name);
				} else {
					$('#div_dt_machine').html(ret.dt_info[0].machine_name + ' (' + ret.dt_info[0].tool_description + ')');
				}
				$('#div_dt_start_time').html(formatTime(ret.dt_info[0].change_time.date));

				if (ret.dt_info[0].time_in_state) {
					$('#div_dt_duration').html(formatMinutes(ret.dt_info[0].time_in_state));
				} else {
					$('#div_dt_duration').html('<i>Current ongoing DT</i>');
				}

				//load codes
				cur_dt_codes = ret.code_info;
				getReasonButtons(0, 0);
			}

		});
	});

	$('#div_downtime_save').on('click', saveDowntime);


	$('#div_top5_downtime').highcharts({
		chart: {
			type: 'column',
			spacingBottom: 0,
			spacingTop: 5,
			borderWidth: 1,
			borderRadius: 5
		},
		title: {
			text: 'Top 5 Downtime Reasons',
			enabled: false,
			style: {"font-size":"10pt", "font-weight":"bold"},
			margin: 0
		},
		credits: {
			enabled: false
		},
		xAxis: {
			categories: []
		},
		yAxis: [{
			min: 0,
			endOnTick: false,
			labels: {enabled: false},
			lineWidth: 0,
			lineColor: 'transparent',
			gridLineColor: 'transparent',
			minorGridLineWidth: 0,
			minorTickLength: 0,
			tickLength: 0,
			title: {
				text: 'Top 5',
				enabled: false
			}
		}],
		legend: {
			shadow: false,
			enabled: false
		},
		tooltip: {
			enabled: false
		},
		plotOptions: {
			column: {
				grouping: false,
				shadow: false,
				borderWidth: 0,
				states: {
					hover: {
						enabled: false
					}
				}
			}
		},
		series: [{
			name: 'Minutes',
			color: 'rgba(22,170,2,.7)',
			data: [],
			pointPadding: 0.1,
			pointPlacement: 0,
			dataLabels: { enabled: true }
		}]
	});
});

function loadOpenDowntime() {
	var machine_ID = 0;
	if (all_data.machine.length > 0) {
		machine_ID = all_data.machine[0].ID;
	}
	$.ajax(ajax_procs.main, {
		"async":false,
		"data":{
			"action":"load_data",
			"data_types":"open_downtime",
			"my_IP":my_IP,
			"machine_ID":machine_ID
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			all_data.open_downtime = jso.open_downtime;
			updateOpenDowntime();
		}
	});
}

function updateDowtimeTop5() {
	if (all_data.downtime_summary.length == 0) {
		$('#div_top5_downtime').html('<h3 style="border:2px solid #2c3e50;border-radius:10px;padding:20px;"><span class="glyphicon glyphicon-thumbs-up"></span> No downtime!</h3>');
		return;
	} else {
		$('#div_top5_downtime').empty();
	}
	//update downtime top 5 chart
	var chart = $('#div_top5_downtime').highcharts();
	var labels = [];
	var data = [];
	for (var i = 0; i < all_data.downtime_summary.length; i++) {
		labels.push(all_data.downtime_summary[i].reason_description);
		data.push(parseFloat(all_data.downtime_summary[i].downtime_minutes.toFixed(2)));
	}
	chart.xAxis[0].setCategories(labels, false);
	chart.series[0].setData(data);
}

function updateOpenDowntime() {
	if (!all_data.open_downtime) {
		return;
	}

	var num_open = 0, num_closed = 0;
	for (var i = 0; i < all_data.open_downtime.length; i++) {
		if (all_data.open_downtime[i].time_in_state == null) {
			num_open++;
		} else {
			num_closed++;
		}
	}

	var b = $('#tbl_downtime TBODY');

	b.html('');

	if (all_data.open_downtime.length == 0) {
		var r = $('<TR />');
		var c = $('<TD />').html('<h2>Good job!  No open downtime!!</h2>').attr('colspan', 4);
		r.append(c);
		b.append(r);
	} else {

		// 4/27/2017 1:45:43 PM - SS - Make it show open downtime (downtime still in progress)
		if (num_open > 0) {
			var r = $('<TR />');
			var c = $('<TD />').html('<b>Current Downtime:</b>').attr('colspan', 4);
			r.append(c);
			b.append(r);

			for (var i = 0; i < all_data.open_downtime.length; i++) {
				var a = all_data.open_downtime[i];
				if (a.time_in_state == null) {
					var r = $('<TR />').addClass('open-downtime');

					var c = $('<TD />').html(a.machine_name);
					r.append(c);
					var c = $('<TD />').html(formatTime(a.change_time.date));
					r.append(c);
					var c = $('<TD />').html(formatMinutes(a.time_since_started));
					r.append(c);
					
/* 					
					var c = $('<TD />');
					var btn_group = $('<div />').addClass('btn-group');
					$('<BUTTON />').addClass('btn btn-default btn-sm btn-end-downtime').data(a).appendTo(btn_group).html('<span class="glyphicon glyphicon-step-forward"></span> End');
					var btn = $('<BUTTON />').addClass('btn btn-primary btn-sm btn-downtime').data(a).appendTo(btn_group);
					if (a.reason_description != null) {
						var d = $('<DIV />').html(a.reason_description).appendTo(c);
						btn.html('<span class="glyphicon glyphicon-pencil"></span> Change Reason');
					} else {
						btn.html('<span class="glyphicon glyphicon-record"></span> Record Reason');
					}
					c.append(btn_group);
					r.append(c);
 */
 
					b.append(r);
				}
			}
		}

		if (num_closed > 0) {
			var r = $('<TR />');
			var c = $('<TD />').html('<b>Past Downtime:</b>').attr('colspan', 4);
			r.append(c);
			b.append(r);

			for (var i = 0; i < all_data.open_downtime.length; i++) {
				if (all_data.open_downtime[i].time_in_state != null) {
					var r = $('<TR />');

					var c = $('<TD />').html(all_data.open_downtime[i].machine_name);
					r.append(c);
					var c = $('<TD />').html(formatTime(all_data.open_downtime[i].change_time.date));
					r.append(c);
					var c = $('<TD />').html(formatMinutes(all_data.open_downtime[i].time_in_state));
					r.append(c);

/*					
					var c = $('<TD />');
					var btn = $('<BUTTON />').addClass('btn btn-primary btn-sm btn-downtime').html('<span class="glyphicon glyphicon-record"></span> Record Reason');
					btn.data(all_data.open_downtime[i]);
					c.append(btn);
					r.append(c);
*/

					b.append(r);
				}
			}
		}
	}
}

function getReasonButtons(parent_ID, level) {
	if (parent_ID == 0) {
		cur_chain = '';
	}
	$('#div_dt_reason').html('');
	$('#div_cur_chain').html(cur_chain);
	for (var i = 0; i < cur_dt_codes.length; i++) {
		if (cur_dt_codes[i].parent_ID == parent_ID) {
			var b = $('<BUTTON />');
			b.addClass('btn btn-primary dt-button');
			b.data('dtrid', cur_dt_codes[i].ID);
			b.data('desc', cur_dt_codes[i].reason_description);

			var num_children = 0;
			for (var j = 0; j < cur_dt_codes.length; j++) {
				if (cur_dt_codes[j].parent_ID == cur_dt_codes[i].ID) {
					num_children++;
				}
			}

			if (num_children > 0) {
				b.html(cur_dt_codes[i].reason_description + ' (' + num_children + ')');
				b.on('click', function(e) { cur_chain += $(e.target).data('desc') + '-> ';  getReasonButtons($(e.target).data('dtrid'), level + 1); });
			} else {
				b.html(cur_dt_codes[i].reason_description);
				b.data('force_comment', cur_dt_codes[i].force_comment);
				b.on('click', function(e) { e.stopPropagation(); e.preventDefault(); selectDTReason(e.target); });

				if (cur_dt_codes[i].require_team_leader) {
					b.append('<span class="glyphicon glyphicon-lock pull-right lock-icon"></span>');
					b.addClass('dt-requires-team-leader');
				}
			}

			$('#div_dt_reason').append(b);
		}
	}

	if (level > 0) {
		var b = document.createElement('BUTTON');
		$(b).addClass('btn btn-warning dt-button').html('Start Over');
		$(b).on('click', function(e) { getReasonButtons(0, 0); $('#div_downtime_save').prop('disabled', true); });
		$('#div_dt_reason').append(b);
	}
}

function selectDTReason(el) {
	$('.dt-button').removeClass('btn-success');
	$(el).addClass('btn-success');
	$('#txt_dt_reason').focus();

	sel_dt_code = $(el).data('dtrid');

	if ($(el).hasClass('dt-requires-team-leader')) {
		$('#div_downtime_save').prop('disabled', true);
		auth_check(2, 'Planned Downtime Recording', function() {
			$('#div_downtime_save').prop('disabled', false);
		});

		return;
	}

	$('#div_downtime_save').prop('disabled', false);
}

function saveDowntime(e) {
	$('#div_downtime_save').html('Saving...').attr('disabled', true);
	$.ajax({

		"url": ajax_procs.downtime,
		"type": "POST",
		"dataType": "json",
		"cache": false,
		"data": {
			"api":"entry",
			"action": "save_downtime_reason",
			"dtid":cur_dt_id,
			"dtrid":sel_dt_code,
			"comment":$('#txt_dt_reason').val()
		},
		"success": function(ret) {
			$('#div_modal_downtime').modal('hide');
			loadOpenDowntime();
		}

	});
}