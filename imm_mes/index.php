<?php
	ini_set("display_errors", true);
	error_reporting(E_ALL);

	require('load_config.php');
	require('load_template_engine.php');

	$smarty->assign('globals', $GLOBALS['APP_CONFIG']);  //assign all variables
	$smarty->assign('strings', $GLOBALS['STRINGS']);

	$my_IP = $_SERVER['REMOTE_ADDR'];

	if (isset($_REQUEST['debug_IP'])) {
		$my_IP = $_REQUEST['debug_IP'];
	}

	$smarty->assign('my_IP', $my_IP);

	$smarty->display('./templates/main.html');
?>