<?php

	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("../init_db.php");

	$action = $_REQUEST["action"];

	if($action == "get_machines"){
		$system_ID = $_REQUEST["system_ID"];
		$sql = "SELECT *

				FROM machines

				WHERE system_ID = $system_ID";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if($action == "get_machine_overall_oee"){
		$system_ID = $_REQUEST["system_ID"];
		$machine_ID = $_REQUEST["machine_ID"];
		$sql = "DECLARE @shift_number int

			SELECT @shift_number = shift_number 

			FROM MES_COMMON.dbo.shift_time_intervals sti
			JOIN MES_COMMON.dbo.shifts s ON s.ID = sti.shift_ID
			JOIN MES_COMMON.dbo.time_intervals ti ON ti.ID = sti.time_interval_ID

			WHERE s.system_ID = $system_ID AND CAST(GETDATE() AS TIME) >= interval_start_time AND CAST(GETDATE() AS TIME) <= interval_end_time;

			SELECT TOP 1 machine_ID, machine_name, scrap_numerator, scrap_denominator, part_qty, part_target_qty, downtime_minutes, total_minutes, scheduled_minutes,
			dbo.fn_calculateOEEFactor('perf', part_qty, part_target_qty, 0) AS performance_oee,
			dbo.fn_calculateOEEFactor('qual', scrap_numerator, scrap_denominator, 0) AS quality_oee,
			dbo.fn_calculateOEEFactor('avail', downtime_minutes, total_minutes, scheduled_minutes) AS available_oee,
			dbo.fn_calculateOEE(dbo.fn_calculateOEEFactor('perf', part_qty, part_target_qty, 0), dbo.fn_calculateOEEFactor('qual', scrap_numerator, scrap_denominator, 0), dbo.fn_calculateOEEFactor('avail', downtime_minutes, total_minutes, scheduled_minutes)) AS overall_oee
			FROM shift_summary ss
			JOIN machines m ON m.ID = ss.machine_ID

			WHERE machine_ID = $machine_ID AND the_shift = @shift_number

			ORDER BY ss.ID DESC";

			$res = $db->query($sql);
		echo json_encode($res);
	}

	if($action == "get_machine_cycle_numbers"){
		$system_ID = $_REQUEST["system_ID"];
		$machine_ID = $_REQUEST["machine_ID"];
		$sql = "
    DECLARE @tool_ID int, @system_ID int
	DECLARE @last_cycle_time float, @avg_cycle_time float, @low_cycle_time float
	DECLARE @target_cycle_time float
	DECLARE @part_queue int
	DECLARE @my_IP varchar(50)

	SELECT @system_ID = m.system_ID, @tool_ID = m.current_tool_ID, @target_cycle_time = mt.target_cycle_time
	FROM machines m
		LEFT OUTER JOIN tools t ON m.current_tool_ID = t.ID
		LEFT OUTER JOIN machine_tools mt ON m.ID = mt.machine_ID AND t.ID = mt.tool_ID
	WHERE (m.ID = $machine_ID)

	SELECT TOP 1 @last_cycle_time = cycle_duration
	FROM machine_cycles 
	WHERE machine_ID = $machine_ID
		AND tool_ID = @tool_ID 
	ORDER BY ID DESC

	SELECT @avg_cycle_time = AVG(cycle_duration)
	FROM machine_cycles
	WHERE machine_ID = $machine_ID
		AND tool_ID = @tool_ID
		AND cycle_time > DATEADD(DAY, -2, GETDATE())
		AND dbo.getProdDate(cycle_time) = dbo.getProdDate(GETDATE())
		AND dbo.getShift(@system_ID, DATEADD(second, -1 * (cycle_duration + 1), cycle_time)) = dbo.getShift(@system_ID,GETDATE())

	SELECT @low_cycle_time = MIN(cycle_duration)
	FROM machine_cycles 
	WHERE machine_ID = $machine_ID
		AND tool_ID = @tool_ID
		AND cycle_time > DATEADD(DAY, -2, GETDATE())
		AND dbo.getProdDate(cycle_time) = dbo.getProdDate(GETDATE())
		AND dbo.getShift(@system_ID, DATEADD(second, -1 * (cycle_duration + 1), cycle_time)) = dbo.getShift(@system_ID, GETDATE())
		
	SELECT @my_IP = station_IP_address
	FROM machines
	WHERE ID = $machine_ID
	
	SELECT @part_queue = COUNT(*)
	FROM machine_cycle_parts mcp
		JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID 
		JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID AND tp.part_ID IN (
			SELECT parts_id 
			FROM machine_active_parts 
			WHERE end_timestamp IS NULL AND 
				machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
				AND ($machine_ID = 0 OR mc.machine_ID = $machine_ID)
			)
		JOIN parts p ON tp.part_ID = p.ID
	WHERE mc.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = @my_IP)
		AND ($machine_ID = 0 OR mc.machine_ID = $machine_ID)
		AND mcp.disposition_time IS NULL

	SELECT ISNULL(@last_cycle_time, 0) as last_cycle_time,
		   ISNULL(@avg_cycle_time, 0) as avg_cycle_time,
		   ISNULL(@target_cycle_time, 0) as target_cycle_time,
		   ISNULL(@low_cycle_time, 0) as low_cycle_time,
		   ISNULL(@part_queue, 0) as part_queue";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if($action == "get_hourly_production"){
		$system_ID = $_REQUEST["system_ID"];
		$machine_ID = $_REQUEST["machine_ID"];
		$sql = "SELECT * 
				FROM (
				SELECT TOP 8 the_date, ti.interval_desc, the_hour, part_qty, part_target_qty, part_goal
				FROM hourly_summary hs
				JOIN MES_COMMON.dbo.time_intervals ti ON the_hour = DATEPART(HH, ti.interval_start_time)
				JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.time_interval_ID = ti.ID
				JOIN MES_COMMON.dbo.shifts s ON sti.shift_ID = s.ID
				WHERE 
				hs.system_ID = $system_ID
				AND s.system_ID = $system_ID
				AND (machine_ID = $machine_ID)
				AND the_date > DATEADD(DAY, -2, GETDATE())
				ORDER BY the_date DESC, the_hour DESC) as v
				ORDER BY the_date, the_hour";


		$res = $db->query($sql);
		echo json_encode($res);
	}

?>