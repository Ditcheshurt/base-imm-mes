<?php
	error_reporting(E_ALL);

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("../init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "get_config") {
		$data_types = explode("~", $_REQUEST['data_types']);
		$res = new StdClass();

		for ($i = 0; $i < count($data_types); $i++) {
			$data_type = $data_types[$i];

			switch ($data_type) {
				case "version":
					$res->$data_type = "2.1.24";
					break;
				case "all_machines":
					$sql = "SELECT
								m.ID
								,m.machine_name
								,m.machine_number
								,m.up
								,ISNULL(m.current_tool_ID, 0) AS current_tool_ID
								,m.is_station_default
							FROM nysus_repetitive_mes.dbo.machines m
							WHERE m.ignored = 0
							AND m.system_ID = 1;";
					$res->$data_type = $db->query($sql);
					break;
				case "active_tools":
					$sql = "SELECT
								m.ID
								,ISNULL(t.ID,'') AS tool_ID
								,ISNULL(t.tool_description, '') AS tool_desc
								,ISNULL(t.short_description, '') AS short_desc
								,ISNULL(t.shot_count, 0) AS shot_count
							FROM NYSUS_REPETITIVE_MES.dbo.machines m
							LEFT JOIN NYSUS_REPETITIVE_MES.dbo.tools t ON t.ID = m.current_tool_ID
							WHERE m.system_ID = 1;";
					$res->$data_type = $db->query($sql);
					break;
				case "active_parts":
					$sql = "SELECT
								m.ID
								,ISNULL(p.ID, 0) AS part_ID
								,ISNULL(p.part_desc, '') AS part_desc
								,ISNULL(p.part_number, '') AS part_number
								,ISNULL(p.part_video, '') AS part_video
							FROM NYSUS_REPETITIVE_MES.dbo.machines m
							LEFT JOIN NYSUS_REPETITIVE_MES.dbo.machine_active_parts map ON map.machine_id = m.ID AND map.is_active > 0 AND map.end_timestamp IS NULL
							LEFT JOIN NYSUS_REPETITIVE_MES.dbo.parts p ON p.ID = map.parts_id
							WHERE m.system_ID = 1;";
					$res->$data_type = $db->query($sql);
					break;
				case "data_points":
					$sql = "SELECT
								dps.ID
								,dps.data_point_name
								,dps.data_point_desc
								,dps.units
								,dps.is_downtime_indicator
								,dps.default_ucl AS ucl
								,dps.default_lcl AS lcl
							FROM NYSUS_REPETITIVE_MES.dbo.data_point_setup dps
							WHERE dps.data_point_name <> 'COUNT'";
					$res->$data_type = $db->query($sql);
					break;
			}
		}
		echo json_encode($res);
	}
	
	if ($action == "get_last_shot_data") {
		$res = new StdClass();
		$sql = "SELECT
					mcdp.cycle_ID
					,dps.ID AS data_point_ID
					,dps.data_point_name
					,dps.data_point_desc
					,mcdp.data_point_value
					,ISNULL(mtcl.upper_ctrl_limit, dps.default_ucl) AS ucl
					,ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl) AS lcl
					,(CASE WHEN mcdp.data_point_value > ISNULL(mtcl.upper_ctrl_limit, dps.default_ucl) THEN 1 WHEN mcdp.data_point_value < ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl) THEN -1 ELSE 0 END) AS ctrl_limit_exceeded
					,dps.units
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_data_points] mcdp
					JOIN [NYSUS_REPETITIVE_MES].[dbo].[machine_cycles] mc ON mc.ID = mcdp.cycle_ID
					LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[data_point_setup] dps ON dps.ID = mcdp.data_point_ID
					LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[machine_tool_ctrl_limits] mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID
				WHERE
					mcdp.cycle_ID IN (SELECT TOP 1 ID FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_cycles] mc WHERE mc.machine_ID = ".$_REQUEST['machine_ID']." ORDER BY ID DESC)
					AND mc.tool_ID =". $_REQUEST['tool_ID'];
		$res->last_shot_data = $db->query($sql);
		
		$sql = "SELECT
					ISNULL(t1.tool_ID, 0) AS tool_ID
					,dps.ID AS data_point_ID
					,dps.data_point_name
					,dps.data_point_desc
					,ISNULL(t1.upper_ctrl_limit, dps.default_ucl) AS ucl
					,ISNULL(t1.lower_ctrl_limit, dps.default_lcl) AS lcl
				FROM
					data_point_setup dps
					LEFT JOIN (
						SELECT
							tool_ID
							,data_point_ID
							,upper_ctrl_limit
							,lower_ctrl_limit
						FROM
							machine_tool_ctrl_limits mtcl
						WHERE
							(mtcl.machine_ID = ".$_REQUEST['machine_ID']." AND mtcl.tool_ID = ".$_REQUEST['tool_ID'].")
					) t1 ON t1.data_point_ID = dps.ID
				WHERE dps.ID <> 1";
		$res->ctrl_lmts = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_data_point_values') {
		$res = new StdClass();
		$sql = "SELECT
					(ROW_NUMBER() OVER(ORDER BY mcdp.cycle_ID DESC)-1) as shots_ago
					,mcdp.cycle_ID
					,dps.data_point_name
					,dps.data_point_desc
					,mcdp.data_point_value
					,dps.units
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_data_points] mcdp
					LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[data_point_setup] dps ON dps.ID = mcdp.data_point_ID
				WHERE
					mcdp.cycle_ID IN (
						SELECT TOP 25 ID
						FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_cycles] mc
						WHERE mc.machine_ID = ".$_REQUEST['machine_ID']."
						AND mc.tool_ID = ".$_REQUEST['tool_ID']."
						ORDER BY ID DESC
					)
				AND dps.data_point_name = '". $_REQUEST['data_point_name'] ."'";
		$res->data_pts = $db->query($sql);
		
		$sql = "SELECT
					ISNULL(t1.tool_ID, 0) AS tool_ID
					,dps.ID AS data_point_ID
					,dps.data_point_name
					,dps.data_point_desc
					,dps.units
					,ISNULL(t1.upper_ctrl_limit, dps.default_ucl) AS ucl
					,ISNULL(t1.lower_ctrl_limit, dps.default_lcl) AS lcl
				FROM
					data_point_setup dps
					LEFT JOIN (
						SELECT
							tool_ID
							,data_point_ID
							,upper_ctrl_limit
							,lower_ctrl_limit
						FROM
							machine_tool_ctrl_limits mtcl
						WHERE
							(mtcl.machine_ID = ".$_REQUEST['machine_ID']." AND mtcl.tool_ID = ".$_REQUEST['tool_ID'].")
					) t1 ON t1.data_point_ID = dps.ID
				WHERE dps.ID <> 1 AND dps.data_point_name = '".$_REQUEST['data_point_name']."'";
		$res->ctrl_lmts = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_tool_chg_status') {
		$res = new StdClass();
		$machine_ID = $_REQUEST['machine_ID'];
		$tool_ID = $_REQUEST['tool_ID'];
		
		$sql = "EXECUTE dbo.sp_MACHINE_MES_getToolChangeTimes @machine_ID = $machine_ID";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}
?>