<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");
	$db->connect();

	$action = $_REQUEST['action'];
	
	if ($action === 'get_machine_group') {
		global $db;
		$res = new StdClass();		
		
		$sql = "SELECT
					m.ID,
					m.machine_name,
					m.machine_number,
					m.up,
					mg.name as group_name,
					p.part_number AS current_part_number,
					mt.target_cycle_time,
					mc.cycle_duration,
					(CASE
						WHEN DATEDIFF(SECOND, m.last_cycle_time, GETDATE()) > 2 * ISNULL(mt.target_cycle_time, 0) THEN 0  -- down
						WHEN DATEDIFF(SECOND, m.last_cycle_time, GETDATE()) < 1.05 * ISNULL(mt.target_cycle_time, 0) THEN 1  -- up
						ELSE 2 -- sort of down
					END) AS status
				FROM
					machines m
					LEFT JOIN machine_groups mg ON mg.ID = m.machine_group_ID
					LEFT JOIN tools t ON t.ID = m.current_tool_ID
					LEFT JOIN tool_parts tp ON tp.ID = t.current_part_ID
					LEFT JOIN parts p ON tp.part_ID = p.ID
					LEFT JOIN machine_tools mt ON mt.machine_ID = m.ID AND mt.tool_ID = m.current_tool_ID
					LEFT JOIN machine_cycles mc ON mc.machine_ID = m.ID AND mc.cycle_time = m.last_cycle_time
				WHERE
					m.machine_group_ID = ".$_REQUEST['machine_group_ID']."
				    AND m.ID IN (30080, 40040)
					AND m.ignored = 0
					--AND station_ip = ".$_SERVER['REMOTE_ADDR']."
				ORDER BY
					m.ID ASC;";
		//die($sql);
		$res = $db->query($sql);

		foreach($res as $i => &$machine) {

			// get hourly summary for the machine
			$sql = "SELECT TOP 24
						hs.ID,
						hs.the_date,
						hs.the_hour,
						hs.part_qty,
						hs.part_goal,
						hs.performance,
						hs.scrap_numerator,
						hs.scrap_denominator,
						hs.scrap_denominator - hs.scrap_numerator AS scrap,
						hs.quality,
						hs.downtime_minutes,
						hs.availability,
						hs.oee,
						ti.interval_desc
					FROM
						hourly_summary hs
						LEFT JOIN MES_COMMON.dbo.time_intervals ti ON ti.ID = hs.time_interval_ID
					WHERE
						machine_ID = ".$machine['ID']."
					ORDER BY
						the_date DESC, the_hour DESC;";

			$machine['hourly_summary'] = new StdClass();
			$machine['hourly_summary']->summary = $db->query($sql);

			// get shift summary for the machine
			$sql = "SELECT
						ss.ID,
						ss.the_date,
						ss.the_shift,
						ss.part_qty,
						ss.part_goal,
						ss.performance,
						ss.scrap_numerator,
						ss.scrap_denominator,
						ss.scrap_denominator - scrap_numerator AS scrap,
						ss.quality,
						ss.downtime_minutes,
						ss.availability,
						ss.oee
					FROM
						shift_summary ss
						JOIN machines m ON m.ID = machine_ID
					WHERE
						the_date >= DATEADD(DAY, -1, GETDATE())
						AND the_shift = dbo.getShift(m.system_ID, GETDATE())
						AND machine_ID = ".$machine['ID'].";";

			$shift_summary = $db->query($sql);
			if (is_array($shift_summary)) {
				$machine['shift_summary'] = $shift_summary[0];
			} else {
				$machine['shift_summary'] = null;
			}

			// machine statuses
			$sql = "SELECT
			CASE
				WHEN ps.gauge_parts_used >= ps.parts_per_gauging + ps.parts_per_gauging_maximum_offset
				THEN 'GAUGING AT ' + m.machine_name + ' IS STOPPED'
				ELSE
					CASE
					WHEN ps.gauge_parts_used >= ps.parts_per_gauging
					THEN 'GAUGING AT ' + m.machine_name + ' IS IN DANGER'
					ELSE

						CASE
							WHEN ps.gauge_parts_used >= ps.parts_per_gauging - ps.parts_per_gauging_warning_offset
							THEN 'GAUGING AT ' + m.machine_name + ' IS IN WARNING'
							ELSE null
						END
					END
			END	AS warning
		FROM
			gauge_material_types ps
			JOIN tool_parts tp
		         ON tp.ID = ps.tool_part_id
			JOIN parts p
				ON p.ID = tp.part_ID
			JOIN tools t
				ON t.ID = tp.tool_id
			JOIN machine_tools mt
				ON mt.tool_ID = t.ID
			JOIN machines m
				ON m.ID = mt.machine_ID
		WHERE
			ps.active = 1
			AND tp.active = 1
			AND m.system_ID = (SELECT system_ID FROM machines WHERE ID = " . $machine['ID'] . ")";

			$res["statuses"] = $db->query($sql);
		}

		echo json_encode($res);
	}
	
	if ($action === 'get_slides') {
		global $db;
		$res = new StdClass();

		if (isset($_REQUEST['active'])) {
			$active_str = "AND active = ".$_REQUEST['active']." ";
		} else {
			$active_str = "";
		}

		$sql = "SELECT
					ID,
					slide_order,
					image_src,
					active,
					duration,
					CASE WHEN machine_group_ID IS NULL THEN 1 ELSE 0 END AS is_common
				FROM
					production_dashboard_slides
				WHERE
					(machine_group_ID = ".$_REQUEST['machine_group_ID']." OR machine_group_ID IS NULL)
					".$active_str."
				ORDER BY
					is_common DESC,
					slide_order ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}
?>