<?php

	header("Content-Type: application/json");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	session_save_path($GLOBALS['APP_CONFIG']['session_path']);
	session_start();

	$db = new db();

	$db->server = $GLOBALS['APP_CONFIG']['server'];
	$db->catalog = $GLOBALS['APP_CONFIG']['catalog'];
	$db->username = $GLOBALS['APP_CONFIG']['username'];
	$db->password = $GLOBALS['APP_CONFIG']['password'];
	$db->testactive = $GLOBALS['APP_CONFIG']['testactive'];

	$db->connect();

	$action = $_REQUEST['action'];

	if ($action == "load_data") {
		$data_types = explode("~", $_REQUEST['data_types']);
		$res = new StdClass();

		for ($i = 0; $i < count($data_types); $i++) {
			$data_type = $data_types[$i];

			switch ($data_type) {
				case "version":
					$res->$data_type = "2.1.24";
					break;
				case "machine":
					$sql = "SELECT m.*, DATEDIFF(SECOND, m.last_cycle_time, GETDATE()) as last_cycle_time_ago, p.ip_address as PLC_IP_address,
								mast.current_operator_ID as master_operator_ID, o.name as master_operator_name, CONVERT(varchar(12), GETDATE(), 108) as master_operator_login_time
							FROM machines m
								LEFT OUTER JOIN NYSUS_PLC_POLLER.dbo.nysus_plcs p ON m.plc_ID = p.ID
								LEFT OUTER JOIN machines mast ON m.master_machine_ID = mast.ID
								LEFT OUTER JOIN MES_COMMON.dbo.operators o ON mast.current_operator_ID = o.ID
							WHERE m.station_IP_address = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "production":
					$res->$data_type = new StdClass();
					$res->$data_type->hourly_summary = array();
					$res->$data_type->shift_summary = array();

					$a = $GLOBALS['APP_CONFIG']['packout_machines'];
					for ($j = 0; $j < count($a); $j++) {
						$sql = "EXEC dbo.sp_MACHINE_MES_GetHourlyProductionSummary @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$a[$j].";";
						array_push($res->$data_type->hourly_summary, $db->query($sql));

						$sql = "EXEC dbo.sp_MACHINE_MES_GetShiftProductionSummary @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$a[$j].";";
						array_push($res->$data_type->shift_summary, $db->query($sql));
					}
					break;
				case "downtime_summary":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetDowntimeSummary @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "open_downtime":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetOpenDowntime @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "time_and_shift":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetTimeAndShift @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "defects":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetDefects @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "PLC_status":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetPLCStatus @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "tools":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetTools @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "parts":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetParts @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "cycle":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetCycleData @my_IP = '".$_REQUEST['my_IP']."';";
					$res->$data_type = $db->query($sql);
					break;
				case "work_instructions":
					$sql = "SELECT *
								FROM work_instructions
								WHERE machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."')";
					$res->$data_type = $db->query($sql);
					break;
				case "work_instruction_signoffs":
					$sql = "SELECT so.*
							FROM machine_instruction_signoffs so
							WHERE so.instr_ID IN (
								SELECT ID
								FROM machine_instructions
								WHERE machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."')
							)";
					$res->$data_type = $db->query($sql);
					break;
				case "quality_alerts":
					$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "quality";
					if (isset($_REQUEST["machine_ID"]) && $_REQUEST["machine_ID"] != "0") {
						$sql = "SELECT
									s.system_name
								FROM
									machines m
								JOIN
									MES_COMMON.dbo.systems s ON m.system_ID = s.ID
								WHERE m.ID IN (SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."')";
						$sys = $db->query($sql);
						$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "quality" . DIRECTORY_SEPARATOR . $sys[0]["system_name"];
					}
					$res->$data_type = listFiles($folder);
					break;
				case "safety_alerts":
					$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "safety";
					$res->$data_type = listFiles($folder);
					break;
				case "alert_signoffs":
					$sql = "SELECT *
							FROM alert_signoffs";
					$res->$data_type = $db->query($sql);
					break;
				case "barcode_status":
					$res->$data_type = new StdClass();
					$res->$data_type->statuses = array();

					$a = $GLOBALS['APP_CONFIG']['packout_machines'];
					for ($j = (count($a) - 1); $j >= 0; $j--) {
						$sql = "SELECT value FROM NYSUS_PLC_POLLER.dbo.nysus_plc_tags WHERE name LIKE '%PART_STATUS_TO_PLC%' AND description LIKE '".$a[$j]."%';";
						array_push($res->$data_type->statuses, $db->query($sql));
					}
					break;
				case "packout":
					$res_packout = new StdClass();

					$container_info_arr = array();

					$sql = "SELECT *
							  FROM machine_groups
							  WHERE rack_layers IS NOT NULL
							  ORDER BY ID DESC";
					$resdb = $db->query($sql);

					for ($i = 0; $i < count($resdb); $i++) {
						$container_info = $resdb[$i];

						$sql = "SELECT r.*, p.serial_number
								  FROM machine_group_racks r
								     JOIN machine_cycle_parts p ON r.ID = p.machine_group_rack_ID
								  WHERE r.end_time IS NULL
								  	  AND r.machine_group_ID = ".$resdb[$i]['ID']."
								  ORDER BY machine_group_rack_part_order";
						$container_info['parts'] = $db->query($sql);

						if ($container_info['parts'] == null) {
							$container_info['parts'] = array();
						}

						array_push($container_info_arr, $container_info);
					}

					$res_packout->container_info = $container_info_arr;


					$res->$data_type = $res_packout;
					break;
			}
		}


		echo json_encode($res);
	}

	// if ($action == "lookup_part") {
	// 	if ($_REQUEST['in_current_rack'] == "1") {
	// 		$sql = "SELECT mcp.*, r.ID as rack_ID, r.end_time
	// 				FROM machine_cycle_parts mcp
	// 					JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
	// 					JOIN machines m ON mc.machine_ID = m.ID AND m.packout_machine_group_ID = ".$_REQUEST['machine_group_ID']."
	// 					LEFT OUTER JOIN machine_group_racks r ON mcp.machine_group_rack_ID = r.ID 
	// 				WHERE mcp.full_barcode = '".$_REQUEST['scan']."'";
			
	// 	} else {
	// 		$sql = "SELECT TOP 1 mcp.* 
	// 				FROM machine_cycle_parts mcp
	// 					JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
	// 					JOIN machines m ON mc.machine_ID = m.ID AND m.packout_machine_group_ID = ".$_REQUEST['machine_group_ID']."
	// 				WHERE mcp.full_barcode = '".$_REQUEST['scan']."'
	// 				ORDER BY mcp.ID DESC";
	// 	}

	// 	echo json_encode($db->query($sql));
	// }

	if ($action == "do_manual_packout") {
		
		$sql = "EXEC dbo.sp_MACHINE_MES_ManualPackout 
				@barcode_data = '".$_REQUEST['barcode']."',
				@machine_group_ID = ".$_REQUEST['machine_group_ID'].",
				@operator_ID = ".$_REQUEST['operator_ID'];	
//echo($sql);
		echo json_encode($db->query($sql));
	}
	
	if ($action == "remove_part") {
		$res = new stdClass();
		$sql = "UPDATE machine_cycle_parts 
				SET machine_group_rack_ID = NULL, machine_group_rack_part_order = NULL, machine_group_rack_pack_time = NULL, machine_group_rack_pack_operator_ID = NULL
				WHERE ID = ".$_REQUEST['machine_cycle_part_ID'];
		$db->query($sql);
		
		$res->success = 1;
		echo json_encode($res);
	}
	
	// if ($action == "insert_part") {
	// 	$res = new stdClass();
	// 	$sql = "EXEC sp_MACHINE_MES_addPartToRack 
	// 			@machine_group_ID = ".$_REQUEST['machine_group_ID'].", 
	// 			@machine_cycle_part_ID = ".$_REQUEST['machine_cycle_part_ID'].", 
	// 			@operator_ID = ".$_REQUEST['operator_ID'];		
	// 	$db->query($sql);
		
	// 	$res->success = 1;
	// 	echo json_encode($res);
	// }
	
	if ($action == "reset_rack") {
		$res = new stdClass();
		//remove all parts from current rack for the selected group
		$sql = "UPDATE machine_cycle_parts 
				SET machine_group_rack_ID = NULL, machine_group_rack_part_order = NULL, machine_group_rack_pack_time = NULL, machine_group_rack_pack_operator_ID = NULL
				WHERE machine_group_rack_ID IN (SELECT ID FROM machine_group_racks WHERE machine_group_ID = ".$_REQUEST['machine_group_ID']." AND end_time IS NULL)";
		$db->query($sql);
		
		//delete the current rack for the selected group
		$sql = "DELETE FROM machine_group_racks 
				WHERE machine_group_ID = ".$_REQUEST['machine_group_ID']." 
					AND end_time IS NULL";
		$db->query($sql);
		
		$res->success = 1;
		echo json_encode($res);
	}

	function fixDB($s) {
		$s = str_replace("--", "", $s);
		$s = str_replace("'", "''", $s);
		return $s;
	}

	function listFiles($folder) {
		$files = scandir($folder);
		$f = array();
		for ($i = 0; $i < count($files); $i++) {
			if ($files[$i] == "." || $files[$i] == "..") {
				//do nothing
			} else {
				array_push($f, $files[$i]);
			}
		}
		return $f;
	}

	function dirToArray($dir) {

		if (!file_exists($dir)) {
			return;
		}

		$result = array();

		$cdir = scandir($dir);
		foreach ($cdir as $key => $value)
		{
			if (!in_array($value,array(".","..")))
			{
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
				{
					$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
				}
				else
				{
					$result[] = $value;
				}
			}
		}

		return $result;
	}

?>