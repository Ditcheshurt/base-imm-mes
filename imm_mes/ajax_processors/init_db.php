<?php

    header("Content-Type: application/json");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');	

	$db = new db();
	$db->server = $GLOBALS['APP_CONFIG']['server'];
	$db->catalog = $GLOBALS['APP_CONFIG']['catalog'];
	$db->username = $GLOBALS['APP_CONFIG']['username'];
	$db->password = $GLOBALS['APP_CONFIG']['password'];
	$db->testactive = $GLOBALS['APP_CONFIG']['testactive'];

    $db->connect();

    function fixDB($s) {
    	$s = str_replace("--", "", $s);
    	$s = str_replace("'", "''", $s);
    	return $s;
    }
    
    function unencrypt($s) {
    	$a = str_split($s);
    	$b = array();
    	for ($i = 0; $i < count($a); $i++) {
    		array_push($b, chr(ord($a[$i]) - 1));
    	}
    
    	return join("", $b);
    }
    
    function is_session_started() {
    	if ( php_sapi_name() !== 'cli' ) {
    		if ( version_compare(phpversion(), '5.4.0', '>=') ) {
    			return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
    		} else {
    			//echo session_id();
    			return session_id() === '' ? FALSE : TRUE;
    		}
    	}
    }
?>