<?php
	error_reporting(E_ALL);

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$res = new StdClass();
	$data_type = "part_queue";
	$misc_type = "misc_queue";

	$sql = "SELECT
					 mc.machine_ID
					,tp.part_ID
					,mcp.cycle_ID
					,mcp.serial_number
					FROM machine_cycle_parts mcp
						JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
						JOIN tool_parts tp on mcp.tool_part_ID = tp.ID
					WHERE mcp.disposition_time IS NULL
						AND tp.part_ID IN (SELECT map.parts_id FROM machine_active_parts map WHERE map.machine_id = ".$_REQUEST['machine_ID']." AND map.is_active = 1 AND map.end_timestamp IS NULL)
						AND mc.machine_ID = ".$_REQUEST['machine_ID']."
					ORDER BY mcp.tool_part_ID ASC, mcp.ID DESC";
	//$res->labeling_query = $sql;
	//$res->$data_type = $db->query($sql);
	
	$sres = $db->query($sql);
	
	for($i = 0; $i < count($sres); $i++){
		$part_id = $sres[$i]['part_ID'];
		$cycle_id = $sres[$i]['cycle_ID'];
		$serial = $sres[$i]['serial_number'];
		
		$t[$part_id][] = array('cycle_id'=>$cycle_id, 'serial'=>$serial);
	}
	$res->$misc_type = $t;
		
	//echo var_dump($res);
	echo json_encode($res);
	
	//var A = {"misc_queue":{"5107":[{"cycle_id":3930444,"serial":"AAACD"},{"cycle_id":3930443,"serial":"AAACC"},{"cycle_id":3930437,"serial":"AAACB"},{"cycle_id":3930436,"serial":"AAACA"}],"5108":[{"cycle_id":3930445,"serial":"AAAAC"},{"cycle_id":3930443,"serial":"AAAAB"}]}}
	//
	//alert("There are " + Object.keys(A["misc_queue"]).length + " part id's");
	//for(var key in A["misc_queue"]){
	// alert(key + " has " + A["misc_queue"][key].length + " parts made");
	//}
?>

