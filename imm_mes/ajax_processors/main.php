<?php
	error_reporting(E_ALL);

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);
	
	include_once("init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "load_data") {
		$data_types = explode("~", $_REQUEST['data_types']);
		$res = new StdClass();

		for ($i = 0; $i < count($data_types); $i++) {
			$data_type = $data_types[$i];

			switch ($data_type) {
				case "version":
					$res->$data_type = "2.1.24";
					break;
				case "all_machines":
					$sql = "SELECT *
							  FROM machines
							  WHERE station_IP_address = '".$_REQUEST['my_IP']."'";
					$res->$data_type = $db->query($sql);
					break;
				case "machine_last_cycle":
					$sql = "SELECT m.last_cycle_time, DATEDIFF(SECOND, m.last_cycle_time, GETDATE()) as last_cycle_time_ago
							FROM machines m
							WHERE m.station_IP_address = '".$_REQUEST['my_IP']."'";
					if ($_REQUEST['machine_ID'] != "0") {
						$sql .= " AND m.ID = ".$_REQUEST['machine_ID']." ";
					}

					$res->$data_type = $db->query($sql);
					break;
				case "machine":
					$sql = "SELECT m.*, DATEDIFF(SECOND, m.last_cycle_time, GETDATE()) as last_cycle_time_ago, p.ip_address as PLC_IP_address,
								mast.current_operator_ID as master_operator_ID, o.name as master_operator_name, CONVERT(varchar(12), GETDATE(), 108) as master_operator_login_time
							FROM machines m
								LEFT OUTER JOIN NYSUS_PLC_POLLER.dbo.nysus_plcs p ON m.plc_ID = p.ID
								LEFT OUTER JOIN machines mast ON m.master_machine_ID = mast.ID
								LEFT OUTER JOIN MES_COMMON.dbo.operators o ON mast.current_operator_ID = o.ID
							WHERE m.station_IP_address = '".$_REQUEST['my_IP']."'";
					if ($_REQUEST['machine_ID'] != "0") {
						$sql .= " AND m.ID = ".$_REQUEST['machine_ID']." ";
					}
					$res->$data_type = $db->query($sql);
					break;
				case "production":
					$res->$data_type = new StdClass();

					$sql = "EXEC dbo.sp_MACHINE_MES_GetHourlyProductionSummary @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type->hourly_summary = $db->query($sql);

					$sql = "EXEC dbo.sp_MACHINE_MES_GetShiftProductionSummary @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type->shift_summary = $db->query($sql);

					break;
				case "downtime_summary":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetDowntimeSummary @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);

//					$res->$data_type = array();
//					$arr = array("Tooling Damage", "Equipment Failures", "Material Shortage", "Over Cycle", "Component Jams");
//					$arr2 = array(15, 10, 8, 3, 2);
//					for ($j = 0; $j < 5; $j++) {
//						$x = new stdClass();
//						$x->reason_description = $arr[$j];
//						$x->downtime_minutes = $arr2[$j];
//						array_push($res->$data_type, $x);
//					}

					break;
				case "open_downtime":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetOpenDowntime @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "time_and_shift":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetTimeAndShift @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "defects":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetDefects @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "PLC_status":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetPLCStatus @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "tools":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetTools @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "parts":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetParts @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "cycle":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetCycleData @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "data_points":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetDataPoints @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "schedule":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetSchedule @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->$data_type = $db->query($sql);
					break;
				case "work_instructions":
					if (isset($GLOBALS['APP_CONFIG']['use_instruction_files']) && $GLOBALS['APP_CONFIG']['use_instruction_files'] == true) {
						if (isset($GLOBALS['APP_CONFIG']['instruction_files_path'])) {
							$res->$data_type = dirToArray($GLOBALS['APP_CONFIG']['instruction_files_path'] . DIRECTORY_SEPARATOR .$data_type);
						}
					} else {
						$sql = "SELECT i.*
							FROM machine_instructions i
							WHERE i.machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."')
								AND (".$_REQUEST['machine_ID']." = 0 OR i.machine_ID = ".$_REQUEST['machine_ID'].")";
						$res->$data_type = $db->query($sql);
					}
					break;
				case "work_instruction_signoffs":
					$sql = "SELECT so.*
							FROM machine_instruction_signoffs so
							WHERE so.instr_ID IN (
								SELECT ID
								FROM machine_instructions
								WHERE machine_ID IN (SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."')
									AND (".$_REQUEST['machine_ID']." = 0 OR machine_ID = ".$_REQUEST['machine_ID'].")
							)";
					$res->$data_type = $db->query($sql);
					break;
				case "quality_alerts":
					$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "quality";
					if (isset($_REQUEST["machine_ID"]) && $_REQUEST["machine_ID"] != "0") {
						$sql = "SELECT
									s.system_name
								FROM
									machines m
								JOIN
									MES_COMMON.dbo.systems s ON m.system_ID = s.ID
								WHERE m.ID = " . $_REQUEST["machine_ID"];
						$sys = $db->query($sql);
						$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "quality";
					}
					$res->$data_type = listFiles($folder);
					break;
				case "safety_alerts":
					$folder = $GLOBALS['APP_CONFIG']['alert_files_path'] . DIRECTORY_SEPARATOR . "safety";
					$res->$data_type = listFiles($folder);
					break;
				case "alert_signoffs":
					$sql = "SELECT *
							FROM alert_signoffs";
					$res->$data_type = $db->query($sql);
					break;
				case "labeling":
					$sql = "SELECT COUNT(mcp.ID) as queue
							  FROM machine_cycle_parts mcp
							  	  JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
							  WHERE mcp.disposition_time IS NULL
							  	  AND mc.machine_ID = ".$_REQUEST['machine_ID']." ";
					$res->labeling_query = $sql;
					$res->$data_type = $db->query($sql);
					break;
				case "tool_part_configuration":
					$sql = "EXEC dbo.sp_MACHINE_MES_GetCurrentTool @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->tool = $db->query($sql);

					$sql = "EXEC dbo.sp_MACHINE_MES_GetCurrentToolParts @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->active_parts = $db->query($sql);

					$sql = "EXEC dbo.sp_MACHINE_MES_GetCurrentToolPartQueue @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->part_queue = $db->query($sql);

					// $sql = "EXEC dbo.sp_MACHINE_MES_GetCurrentToolPartQueue2 @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					// $res->part_queue = $db->query_multiresult($sql);
					// $parts = $res->part_queue[0]['parts'];
					// $res->part_queue_count = array_slice($res->part_queue,1,$parts);
					// array_splice($res->part_queue,0,$parts+1);

					$sql = "EXEC dbo.sp_MACHINE_MES_GetCurrentToolPartContainerInfo @my_IP = '".$_REQUEST['my_IP']."', @machine_ID = ".$_REQUEST['machine_ID'].";";
					$res->container_info = $db->query($sql);

					break;
			}
		}

		echo json_encode($res);
	}

	if ($action == "record_good_part") {
		$res = new stdClass();
		$res->success = 1;

		$sql = "EXEC dbo.sp_MACHINE_MES_recordGoodPart @machine_ID = ".$_REQUEST['machine_ID'].", @part_ID = ".$_REQUEST['part_ID'].", @operator_ID = ".$_REQUEST['operator_ID'].";";
		$db->query($sql);

		echo json_encode($res);
	}

	if ($action == "record_all_good_parts") {
		$res = new stdClass();
		$res->success = 1;

		$sql = "EXEC dbo.sp_MACHINE_MES_recordAllGoodParts @machine_ID = ".$_REQUEST['machine_ID'].", @operator_ID = ".$_REQUEST['operator_ID'].";";
		$db->query($sql);

		echo json_encode($res);
	}

	if ($action == "set_current_tool") {
		$sql = "UPDATE machines
				  SET current_tool_ID = ".$_REQUEST['tool_ID']."
				  WHERE ID = ".$_REQUEST['machine_ID'];

	}

	function listFiles($folder) {
		$files = scandir($GLOBALS['APP_CONFIG']['base_dir'] . $folder);
		return $files;
	}

	function dirToArray($dir) {

		if (!file_exists($dir)) {
			return;
		}

		$result = array();

		$cdir = scandir($dir);
		foreach ($cdir as $key => $value)
		{
			if (!in_array($value,array(".","..")))
			{
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
				{
					$result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
				}
				else
				{
					$result[] = $value;
				}
			}
		}

		return $result;
	}

	if($action == "get_auth"){
		$res = new StdClass();
		$sql = "SELECT TOP 1 o.ID
				FROM [MES_COMMON].[dbo].operators as o
					JOIN [MES_COMMON].[dbo].operator_roles as opr ON o.ID = opr.operator_ID
					JOIN [MES_COMMON].[dbo].roles as r ON opr.role_ID = r.ID
				WHERE o.badge_ID = ".$_REQUEST['badgeID']."
					AND r.role_priority >= ".$_REQUEST['role']."
				ORDER BY r.role_priority DESC;";
				// FROM [MES_COMMON].[dbo].operators as o
					// JOIN [MES_COMMON].[dbo].operator_roles as r
						// ON o.ID = r.operator_ID
				// WHERE o.badgeID = ".$_REQUEST['badgeID']."
					// AND r.role_ID <= ".$_REQUEST['role']."
				// ORDER BY o.ID";
		//die($sql);
		$res=$db->query($sql);
		echo json_encode($res);
	}

?>