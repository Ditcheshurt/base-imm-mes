<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "set_machine_down_state") {
		$res = new stdClass();
		$res->success = 1;

		$sql = "EXEC dbo.sp_MACHINE_MES_setMachineDownState
						@machine_ID = ".$_REQUEST['machine_ID'].",
						@tool_ID = ".$_REQUEST['tool_ID'].",
						@state = ".$_REQUEST['state'].",
						@operator_ID = ".$_REQUEST['operator_ID'].";";
		$db->query($sql);

		echo json_encode($res);
	}

	if ($action == "get_dt_info") {
		$res = new StdClass();
		$machine_type_ID = 0;

		if (isset($_REQUEST['machine_type_ID'])) {
			$machine_type_ID = $_REQUEST['machine_type_ID'];
		}

		$sql = "SELECT dl.ID, dl.change_time, dl.time_in_state, m.machine_name, t.tool_description
					 FROM machine_downtime_log dl
					 	JOIN machines m ON dl.machine_ID = m.ID
					 	LEFT OUTER JOIN tools t ON dl.tool_ID = t.ID
					 WHERE dl.ID = ".$_REQUEST['id']."
					 ";
		$res->dt_info = $db->query($sql);

		//code_info
		$sql = "SELECT r.*, o.oee_category_desc, o.count_against_oee, o.is_scheduled, o.require_team_leader
					 FROM machine_downtime_reasons r
					 	 JOIN machine_downtime_reason_oee_categories o ON r.oee_category = o.ID
					 WHERE r.active = 1
					 	AND r.machine_type_ID = ".$machine_type_ID."
					 ORDER BY r.parent_ID, r.reason_description" ;
		$res->code_info = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "save_downtime_reason") {
		$res = new StdClass();
		$sql = "UPDATE machine_downtime_log SET reason_ID = ".$_REQUEST['dtrid'].", comment = '".str_replace("'", "''", $_REQUEST['comment'])."' WHERE id = ".$_REQUEST['dtid'];
		$db->query($sql);
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "end_downtime") {
		$res = new StdClass();
		$sql = "EXEC dbo.sp_MACHINE_MES_downtimeSplit @downtime_ID = " . $_REQUEST['downtime_ID'] . ", @split_minutes = 0";
		$db->query($sql);
		$res->success = 1;

		echo json_encode($res);
	}
?>