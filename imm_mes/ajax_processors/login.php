<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

    include_once("init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "logout") {
		
		if (isset($_REQUEST['my_IP'])) {
			// get the machine ID
			$sql = "SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."'";
			$machines = $db->query($sql);

			if ($machines) {
				foreach($machines as $machine) {
					$sql = "EXEC dbo.sp_MACHINE_MES_machineOperatorLogout @machine_ID = ".$machine['ID'];
					$db->query($sql);
				}
			}
		}

		if (isset($_SESSION['op_ID'])) {
			unset($_SESSION['op_ID']);
		}

		$action = "check_session";
		
	}
	
	if ($action == "check_OJT_requirements") {
		$sql = "EXEC dbo.sp_checkOJTRequirements @machine_ID = ".$_REQUEST['machine_ID'].", @operator_ID = ".$_REQUEST['operator_ID'];
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "check_session") {
		$res = new StdClass();

		if (is_session_started() === FALSE) {

			$res->session_matched = true;
			$res->new_session = true;
		} else {
			if (session_id() == $_COOKIE['PHPSESSID']) {
				$res->session_matched = true;
				$res->new_session = false;
			} else {
				$res->session_matched = false;
			}
		}

		$res->session = session_id();

		if (isset($_SESSION['op_ID'])) {
			//get operator info
			$sql = "SELECT ID, name, email FROM MES_COMMON.dbo.operators WHERE id = ".$_SESSION['op_ID'];
			$ops = $db->query($sql);
			$res->operator = $ops[0];

			//get roles
			$sql = "SELECT role, editor FROM MES_COMMON.dbo.operator_roles WHERE operator_ID = ".$_SESSION['op_ID'];
			$res->operator_roles = $db->query($sql);
		} else {
			$res->operator = array("name"=>"", "ID"=>0);
			$res->operator_roles = array();
		}

		echo json_encode($res);
	}

	if ($action == "verify_login") {
		$res = new StdClass();
		$r = $_REQUEST;

		$r['scan'] = fixDB($r['scan']);


		$sql = "SELECT ID, name, CONVERT(varchar(12), GETDATE(), 108) as login_time FROM MES_COMMON.dbo.operators WHERE badge_ID = '".$r['scan']."'";
		$res->operator = $db->query($sql);
		if (count($res->operator) > 0) {
			$_SESSION['op_ID'] = $res->operator[0]['ID'];
			$res->op_session_set = $_SESSION['op_ID'];

			if (isset($_REQUEST['my_IP'])) {
				// get the machine ID
				$sql = "SELECT ID FROM machines WHERE station_IP_address = '".$_REQUEST['my_IP']."'";
				$machines = $db->query($sql);

				if ($machines) {
					foreach($machines as $machine) {
						$sql = "EXEC dbo.sp_MACHINE_MES_machineOperatorLogin @machine_ID = ".$machine['ID'].", @operator_ID = ".$res->operator[0]['ID'];
						$db->query($sql);
					}
				}
			}
		}

		$sql = "SELECT role_ID, role FROM MES_COMMON.dbo.operator_roles WHERE operator_ID IN (SELECT ID FROM MES_COMMON.dbo.operators WHERE badge_ID = '".$r['scan']."')";
		$res->operator_roles = $db->query($sql);

		echo json_encode($res);
	}

?>