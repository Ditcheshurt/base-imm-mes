<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "get_current_machine_config") {
		
	}
	
	if ($action == "get_tool_chg_status") {
		$res = new StdClass();
		$machine_ID = $_REQUEST['machine_ID'];
		$tool_ID = $_REQUEST['tool_ID'];
		
		$sql = "
				SELECT TOP 1 mdl2.*
				FROM machine_downtime_log mdl
				LEFT JOIN machine_downtime_log mdl2 ON mdl2.ID = mdl.tool_change_ID
				WHERE mdl.tool_change_ID IS NOT NULL
				AND mdl.time_in_state IS NULL
				AND mdl.machine_ID = $machine_ID
				AND mdl.tool_ID = $tool_ID
				AND mdl2.ID IS NOT NULL
				ORDER BY change_time DESC
				";
		
		//die($sql);
		$res->active = $db->query($sql);
		
		$sql = "
				SELECT TOP 1
					 mdl2.*
				FROM machine_downtime_log mdl
				LEFT JOIN machine_downtime_log mdl2 ON mdl2.tool_change_ID = mdl.ID AND mdl2.ID <> mdl.ID
				WHERE mdl.tool_change_ID IS NOT NULL
				AND mdl.is_tool_change_pause = 0
				AND mdl2.ID IS NOT NULL
				AND mdl2.time_in_state IS NULL
				AND mdl2.reason_ID IS NULL
				AND mdl.machine_ID = $machine_ID
				AND mdl.tool_ID = $tool_ID
				ORDER BY change_time ASC
				";
		
		//die($sql);
		$res->paused = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == "tool_change") {
		$res = new StdClass();
		$type = $_REQUEST['type'];
		$machine_ID = $_REQUEST['machine_ID'];
		$tool_ID = $_REQUEST['tool_ID'];
		
		$sql = "EXEC dbo.sp_MACHINE_MES_".$type."ToolChange @machine_ID = $machine_ID, @tool_ID = $tool_ID";
		
		$res->sql = $sql;
		$res->type = $type;
		$res->qry = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == "set_current_tool") {
		$res = new StdClass();
		
		//ADD CHECK HERE TO MAKE SURE THE TOOL ID IS VALID FOR THIS MACHINE
		$sql = "SELECT * FROM machine_tools WHERE machine_ID = " . $_REQUEST['machine_ID'] . " AND tool_ID = " . $_REQUEST['tool_ID'];
		$dbres = $db->query($sql);
		if ($res) {
			if (count($dbres) > 0) {
				$sql = "UPDATE machines SET current_tool_ID = " . $_REQUEST['tool_ID'] ." WHERE id = ".$_REQUEST['machine_ID'];
				$db->query($sql);
				$res->success = 1;
			} else {
				//add record to loggy
				
				$res->success = 0;
			}
		} else {
			$res->success = 0;
		}		
		
		echo json_encode($res);
	}
	
	if ($action == "record_work_instruction_signoff") {
		$sql = "IF NOT EXISTS (SELECT * FROM machine_instruction_signoffs WHERE instr_ID = ".$_REQUEST['instr_ID']." AND operator_ID = ".$_REQUEST['operator_ID'].") 
					INSERT INTO machine_instruction_signoffs (instr_ID, operator_ID, signoff_time) VALUES (
						".$_REQUEST['instr_ID'].",
						".$_REQUEST['operator_ID'].",
						GETDATE())";
		$db->query($sql);
		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}
	
	if ($action == "record_alert_signoff") {
		$sql = "IF NOT EXISTS (SELECT * FROM alert_signoffs WHERE file_name = '".$_REQUEST['file_name']."' AND operator_ID = ".$_REQUEST['operator_ID']." AND alert_type = '".$_REQUEST['alert_type']."') 
					INSERT INTO alert_signoffs (alert_type, file_name, operator_ID, signoff_time) VALUES (
						'".$_REQUEST['alert_type']."',
						'".$_REQUEST['file_name']."',
						".$_REQUEST['operator_ID'].",
						GETDATE())";
		$db->query($sql);
		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}
	
	if ($action == "get_partial_racks") {
		$res = new StdClass();
		$sql = "SELECT * FROM (
						  SELECT
						   c.ID
						  ,tp.part_ID
						  ,count(cp.machine_cycle_part_ID) AS rack_part_count
						  ,p.standard_pack_qty
						  FROM containers c
						  JOIN container_parts cp ON cp.container_id = c.id AND c.machine_ID = ".$_REQUEST['machine_ID']."
						  JOIN tool_parts tp ON tp.part_ID = c.part_ID AND tp.tool_ID = ".$_REQUEST['tool_ID']." AND c.part_ID = ".$_REQUEST['part_ID']."
						  JOIN parts p on p.ID = tp.part_ID
						  GROUP BY c.ID, tp.part_ID, p.standard_pack_qty
						) AS t
						WHERE t.rack_part_count < t.standard_pack_qty;";
		$dbres = $db->query($sql);
		if (count($dbres) > 0) {
			$res->racks = $dbres;
			$res->success = 1;
		} else {
			$res->success = 0;
		}
		echo json_encode($res);
	}

	if ($action == "resume_partial_rack") {
		$res = new StdClass();
		$res->success = 0;
		$res->rack_ID = $_REQUEST['rack_ID'];
		$sql = "UPDATE containers
						SET end_time = NULL
						OUTPUT inserted.*
						WHERE ID = ".$_REQUEST['rack_ID']."
						AND machine_ID = ".$_REQUEST['machine_ID']."
						AND end_time IS NOT NULL;";
		$dbres = $db->query($sql);
		if (count($dbres) > 0) {
			$res->success = 1;
		} else {
			$res->success = 0;
		}
		echo json_encode($res);
	}

	if ($action == "end_as_partial_rack") {
		$res = new StdClass();
		$sql = "DECLARE @cont_id int;
				SELECT TOP(1) @cont_id = ID
				FROM containers
				WHERE part_ID = ".$_REQUEST['part_ID']."
				AND machine_ID = ".$_REQUEST['machine_ID']."
				AND end_time IS NULL;
				
				UPDATE TOP(1) containers
				SET end_time = GETDATE()
				OUTPUT inserted.*
				WHERE part_ID = ".$_REQUEST['part_ID']."
				AND machine_ID = ".$_REQUEST['machine_ID']."
				AND end_time IS NULL;
				
				EXEC sp_MACHINE_MES_printRackLabel @container_id = @cont_id;";
				
		$dbres = $db->query($sql);
		if (count($dbres) > 0) {
			$res->rack_ID = $dbres[0]["ID"];
			$res->success = 1;
		} else {
			$res->success = 0;
		}
		echo json_encode($res);
	}

	if ($action == "get_open_rack_parts") {
		$res = new StdClass();
		$machine = $_REQUEST['machine_ID'];
		$rack = $_REQUEST['rack_ID'];
		$part = $_REQUEST['part_ID'];
		$sql = "SELECT
							 cp.ID
							,c.machine_ID,c.part_ID
							,c.ID AS container_ID
							,cp.container_position
							,cp.machine_cycle_part_ID AS machine_cycle_part_ID
							,mcp.serial_number AS serial_number
							,mcp.full_barcode as barcode
							,p.part_desc
							,p.part_number
							,p.part_image
						FROM containers c
						JOIN container_parts cp on cp.container_ID = c.ID AND c.end_time IS NULL AND c.machine_ID = ".$machine." AND c.ID = ".$rack." AND c.part_ID = ".$part."
						LEFT JOIN machine_cycle_parts mcp on cp.machine_cycle_part_ID = mcp.ID
						LEFT JOIN parts p on p.ID = c.part_ID
						ORDER BY cp.ID ASC;";
		$dbres = $db->query($sql);
		if (count($dbres) > 0) {
			$res->parts = $dbres;
			$res->success = 1;
		} else {
			$res->success = 0;
		}
		echo json_encode($res);
	}
	
	if($action == "insert_part_change"){
		$res = new StdClass();
		$sql = "EXEC [dbo].[sp_MES_setActiveParts]
				@machine_id = ".$_REQUEST['machine_ID'].",
				@part_id_list = '".$_REQUEST['part_list']."',
				@tool_id = ".$_REQUEST['tool_id'].",
				@id_true = 1,
				@operator = ".$_REQUEST['op_id'];			
		$res = $db->query($sql);
		echo json_encode($res);
	}
?>