<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");	

	$action = $_REQUEST['action'];

	if ($action == "get_defect_types") {
		//code_info
		$sql = "SELECT *
					 FROM machine_defect_reasons
					 WHERE active = 1
					 	--AND machine_type_ID = ".$_REQUEST['machine_type_ID']."
					 ORDER BY parent_ID, series, defect_description" ;
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "get_defect_origins") {
		//code_info
		$sql = "SELECT *
					 FROM machine_defect_origins
					 WHERE active = 1
					 	--AND machine_type_ID = ".$_REQUEST['machine_type_ID']."
					 ORDER BY parent_ID, series, origin_description" ;
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "save_downtime_reason") {
		$res = new StdClass();
		$sql = "UPDATE machine_downtime_log SET reason_ID = ".$_REQUEST['dtrid'].", comment = '".str_replace("'", "''", $_REQUEST['comment'])."' WHERE id = ".$_REQUEST['dtid'];
		$db->query($sql);
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "save_defect") {
		$res = new StdClass();
		$sql = "SET NOCOUNT ON;
					INSERT INTO machine_defect_log (machine_ID, tool_ID, part_ID, defect_time, operator_ID, defect_ID, defect_origin_ID, disposition, serial_number) VALUES (
						".$_REQUEST["machine_ID"].",
						".$_REQUEST["tool_ID"].",
						".$_REQUEST["part_ID"].",
						GETDATE(),
						".$_REQUEST["operator_ID"].",
						".$_REQUEST["defect_ID"].",
						".$_REQUEST["defect_origin_ID"].",
						'".$_REQUEST["disposition"]."',
						'".$_REQUEST['serial_number']."');
					DECLARE @new_ID int;
					SET @new_ID = @@IDENTITY;
					";

		if ($_REQUEST["locations"] != "") {
			$locs = explode(",", $_REQUEST["locations"]);
			for ($i = 0; $i < count($locs); $i++) {
				$coords = explode("~", $locs[$i]);

				$sql .= "INSERT INTO machine_defect_locations (machine_defect_ID, defect_location_x, defect_location_y) VALUES (
								@new_ID,
								".$coords[0].",
								".$coords[1].");
							";
			}
		}
		
		if ($_REQUEST["disposition"] == "SCRAP") {
			$sql .= "DECLARE @mcpid table (id INT);
							UPDATE machine_cycle_parts
							SET disposition = '".$_REQUEST['disposition']."', disposition_time = GETDATE()
							OUTPUT inserted.ID INTO @mcpid
							WHERE serial_number = '".$_REQUEST['serial_number']."' AND tool_part_ID IN (
								SELECT ID FROM tool_parts WHERE tool_ID = ".$_REQUEST["tool_ID"]." AND part_ID = ".$_REQUEST["part_ID"]."
							);";
			$sql .= "UPDATE container_parts
							SET container_ID = -1, container_position = 0
							WHERE machine_cycle_part_ID IN (
								SELECT id FROM @mcpid
							); SELECT id FROM @mcpid;"; // so we get an indicator for success/fail
		} else if ($_REQUEST["disposition"] == "REWORK") {
			$sql .= "DECLARE @mcpid table (id INT);DECLARE @mcp_ID int;
							UPDATE machine_cycle_parts
							SET disposition = '".$_REQUEST['disposition']."', disposition_time = GETDATE()
							OUTPUT inserted.ID INTO @mcpid
							WHERE serial_number = '".$_REQUEST['serial_number']."' AND tool_part_ID IN (
								SELECT ID FROM tool_parts WHERE tool_ID = ".$_REQUEST["tool_ID"]." AND part_ID = ".$_REQUEST["part_ID"]."
							);";
			$sql .= "SELECT TOP 1 @mcp_ID = id FROM @mcpid;";
			$sql .= "EXEC dbo.sp_MACHINE_MES_printPartLabel @mcp_ID = @mcp_ID, @reprint_mark = 'REWORK', @select_return = 0;";
			$sql .= "EXEC dbo.sp_MACHINE_MES_packoutAddPartToRack @machine_cycle_part_ID = @mcp_ID, @operator_ID = ".$_REQUEST['operator_ID'].";";
			$sql .= "SELECT id FROM @mcpid;"; // so we get an indicator for success/fail
		}
		//die($sql);
//future print reject label
		if($db->query($sql)){
			$res->success = 1;
		} else {
			$res->success = 0;
		}
		echo json_encode($res);
	}
	
	if ($action == "lookup_serial") {
		
		$sql = "SELECT TOP 1 tp.part_ID, p.part_desc, p.part_image, mcp.serial_number, mcp.full_barcode AS barcode, mcp.ID, dl.defect_ID, dl.disposition
				  FROM machine_cycle_parts mcp
					  JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID
					  JOIN parts p ON tp.part_ID = p.ID
					  LEFT JOIN machine_defect_log dl ON dl.serial_number = mcp.serial_number
				  WHERE mcp.full_barcode = '".$_REQUEST['scan']."'
				  ORDER BY dl.ID DESC";
		echo json_encode($db->query($sql));
	}

?>