<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$action = $_REQUEST['action'];

	$r = $_REQUEST;

	if ($action == "run_report") {
		$res = new stdClass();
		$sql = "SELECT -1";
		if ($r['auto_gauge'] == 1) {
			$use_mmq = 0;			
			if (isset($r['use_mmq'])) {
				$use_mmq = $r['use_mmq'];
			}
			if ($use_mmq == 0) {
				$sql = "SELECT TOP 125 g.ID, mc.cycle_time, g.".$r['data_point_desc']." as data_point_value, ISNULL(mcp.serial_number, '') as serial_number, g.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						  FROM gauge_litchfield_autogauge g
							 JOIN machine_cycles mc ON g.cycle_ID = mc.ID
							 JOIN machines m ON mc.machine_ID = m.ID
							 LEFT OUTER JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
						  WHERE mc.tool_ID = ".$r['tool_ID']."
						  ORDER BY mc.cycle_time DESC"
						  ;
			} else {
				$sql = "SELECT TOP 125 g.ID, mc.cycle_time, g.[".$r['data_point_desc']."] as data_point_value, ISNULL(mcp.serial_number, '') as serial_number, g.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						  FROM v_spc_mmq_data g
							 JOIN machine_cycles mc ON g.cycle_ID = mc.ID
							 JOIN machines m ON mc.machine_ID = m.ID
							 LEFT OUTER JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
						  WHERE mc.tool_ID = ".$r['tool_ID']."
						  ORDER BY mc.cycle_time DESC"
						  ;
			}

		} else {
			$sql = "SELECT TOP 125 gmcp.ID, mc.cycle_time, gmcp.data_point_value, ISNULL(mcp.serial_number, '') as serial_number, gmcp.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						FROM gauge_machine_cycle_parts gmcp
							JOIN machine_cycle_parts mcp ON gmcp.machine_cycle_part_ID = mcp.ID
							JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
							JOIN gauge_material_types_instructions gmti ON gmcp.gauge_material_types_instruction_ID = gmti.ID
						WHERE mc.tool_ID = ".$r['tool_ID']."
							AND gmti.ID = ".$r['data_point_ID']."
						ORDER BY mc.cycle_time DESC";
		}

		//echo $sql;
		$res->query = $sql;
		$res->results = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "save_comment") {
		$sql = "UPDATE gauge_machine_cycle_parts SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
		if ($r['auto_gauge'] == "1") {
			if ($r['use_mmq'] == "0") {
				$sql = "UPDATE gauge_autogauge SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
			} else {
				$sql = "UPDATE gauge_mahrmmq SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
			}
		}
		$db->query($sql);

		$res = new stdClass();
		$res->query = $sql;
		$res->success = 1;

		echo json_encode($res);
	}


?>