<?php

    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	$GLOBALS['APP_CONFIG']['base_dir'] = realpath(__DIR__ . '/../../');
	require($GLOBALS['APP_CONFIG']['base_dir'] . $GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "get_data_points") {
		$res = new StdClass();
		//code_info
		$sql = "SELECT
					ID
					, data_point_name
					, extract_method
					, extract_info
					, multiplier
					, units
					, is_downtime_indicator
					, is_product_ID
					, active
					, default_ucl AS ucl
					, default_lcl AS lcl
				FROM data_point_setup WHERE ID != 1 ORDER BY ID";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "get_data_point_values") {
		$res = new StdClass();
		$sql = "SELECT
					(ROW_NUMBER() OVER(ORDER BY mcdp.cycle_ID DESC)-1) as shots_ago
					,mcdp.cycle_ID
					,dps.data_point_name
					,mcdp.data_point_value
					,dps.units
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_data_points] mcdp
					LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[data_point_setup] dps ON dps.ID = mcdp.data_point_ID
				WHERE
					mcdp.cycle_ID IN (SELECT TOP 100 ID FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_cycles] ORDER BY ID DESC)
					AND dps.data_point_name = '". $_REQUEST['data_point_name'] ."'";
		$res->data_pts = $db->query($sql);
		
		$sql = "SELECT
					ROUND((AVG(mcdp.data_point_value) + 2*STDEV(mcdp.data_point_value)),3) AS ucl
					,ROUND((AVG(mcdp.data_point_value) - 2*STDEV(mcdp.data_point_value)),3) AS lcl
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_data_points] mcdp
					LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
				WHERE
					mcdp.cycle_ID IN (SELECT TOP 100 ID FROM machine_cycles ORDER BY ID DESC)
					AND dps.data_point_name = '". $_REQUEST['data_point_name'] ."'
				GROUP BY mcdp.data_point_ID";
		$res->ctrl_lmts = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "record_work_instruction_signoff") {
		$sql = "IF NOT EXISTS (SELECT * FROM machine_instruction_signoffs WHERE instr_ID = ".$_REQUEST['instr_ID']." AND operator_ID = ".$_REQUEST['operator_ID'].")
					INSERT INTO machine_instruction_signoffs (instr_ID, operator_ID, signoff_time) VALUES (
						".$_REQUEST['instr_ID'].",
						".$_REQUEST['operator_ID'].",
						GETDATE())";
		$db->query($sql);
		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	if ($action == "record_alert_signoff") {
		$sql = "IF NOT EXISTS (SELECT * FROM alert_signoffs WHERE file_name = '".$_REQUEST['file_name']."' AND operator_ID = ".$_REQUEST['operator_ID']." AND alert_type = '".$_REQUEST['alert_type']."')
					INSERT INTO alert_signoffs (alert_type, file_name, operator_ID, signoff_time) VALUES (
						'".$_REQUEST['alert_type']."',
						'".$_REQUEST['file_name']."',
						".$_REQUEST['operator_ID'].",
						GETDATE())";
		$db->query($sql);
		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

?>