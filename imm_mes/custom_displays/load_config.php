<?php

	//load in app config
	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), FILE_USE_INCLUDE_PATH);

	//get strings, based on language in app config
	$lang = $GLOBALS['APP_CONFIG']['language'];
	$GLOBALS['STRINGS'] = json_decode(file_get_contents("../config/strings_".$lang.".json"), FILE_USE_INCLUDE_PATH);

?>