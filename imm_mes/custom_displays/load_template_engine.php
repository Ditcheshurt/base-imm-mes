<?php

	//LOAD TEMPLATING ENGINE
	//define('SMARTY_DIR', 'c:\\inetpub\\wwwroot\\common\\library\\php\\Smarty\\');
    define('SMARTY_DIR', '../../common/library/php/Smarty/');
	require(dirname(__DIR__).SMARTY_DIR.'Smarty.class.php');
	$smarty = new Smarty();
	$smarty->setTemplateDir('../templates')
				->setCompileDir('../templates/templates_c')
				->setCacheDir('../templates/cache');
?>