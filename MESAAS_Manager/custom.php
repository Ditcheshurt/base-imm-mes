<?php
require ('load_config.php');
require ('load_template_engine.php');

/*
 * spl_autoload_register ( function ($class) {
 * @require_once ('lib/' . $class . '.php');
 * } );
 *
 * $lotClient = new LotClient ();
 * $lotClient->setLotType ( new RepetitiveLot () );
 * echo $lotClient->checkLotExists ( 2, '2444' );
 */

if (! isset ( $_SESSION ['op_ID'] )) {
	switch ($_REQUEST ['type']) {
		case "metaldyne_litchfield/mahrmmq_gauge":
			$smarty->display ( './templates/manage/custom/' . $_REQUEST ['type'] . '.html' );
			break;
		default:
			$smarty->display ( './templates/bad_session.html' );
			break;
	}
	
} else {
	$smarty->display ( './templates/custom/' . $_REQUEST ['type'] . '.html' );
}

?>