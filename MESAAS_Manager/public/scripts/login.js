var LOGIN = {
	"processor":"./ajax_processors/login.php",
	"session_info":null,
	"operator":{
		"ID":0,
		"name":"",
		"email":""
	},
	"operator_roles":[]
};

LOGIN.checkSession = function(callback) {
	if (callback == null) {
		$.getJSON(LOGIN.processor, {"action":"check_session"}, LOGIN.afterCheckSession);
	} else {
		$.getJSON(LOGIN.processor, {"action":"check_session"}, callback);
	}
};

LOGIN.afterCheckSession = function(jso) {
	//response should be
	// {"session":"session_ID", "session_matched":false, "operator":{"name":"Bob", "ID":525}, "roles":["OP", "TL"]}
	LOGIN.session_info = jso;
	LOGIN.operator = jso.operator;
	LOGIN.operator_roles = (jso.operator_roles||[]);

	if (LOGIN.session_info.operator.ID == 0) {
		//show login form
		$('#div_login_info').hide();
		$('#div_login_form').show();

		$('#btn_login').click(LOGIN.verifyLogin);
	} else {
		//show operator info
		$('#div_login_info .form-control').html(LOGIN.operator.name + ' <a href="#">Logout</a>');
		$('#div_login_info').show();
		$('#div_login_form').hide();
		$('#div_login_info .form-control A').click(LOGIN.logout);
	}
	showHideMenuItems();
};

LOGIN.verifyLogin = function() {
	var e = $('#email').val();
	var p = $('#password').val();
	p = LOGIN.fakeEncrypt(p);
	$.getJSON(LOGIN.processor, {"action":"verify_login", "email":e, "password":p}, LOGIN.afterVerifyLogin);
};

LOGIN.afterVerifyLogin = function(jso) {
	jso.operator = jso.operator || [];
	jso.operator_roles = jso.operator_roles || [];

	if (jso.operator.length > 0) {
		LOGIN.operator = jso.operator[0];
		LOGIN.operator_roles = (jso.operator_roles||[]);

//		$('#div_login_info .form-control').html(LOGIN.operator.name + ' <a href="#">Logout</a>');
//		$('#div_login_info').show();
//		$('#div_login_form').hide();
//		$('#div_login_info .form-control A').click(LOGIN.logout);
		document.location = document.location;
	} else {
		$.growl(
			{"title":" Login error: ", "message":"Invalid login", "icon":"glyphicon glyphicon-warning-sign"},
			{"type":"danger", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
	}
};

LOGIN.logout = function() {
	$.getJSON(LOGIN.processor, {"action":"logout"}, function() {document.location = 'index.php';});
	return false;
};

LOGIN.fakeEncrypt = function(s) {
	var a = s.split('');
	var b = [];
	for (var i = 0; i < a.length; i++) {
		b.push(String.fromCharCode(a[i].charCodeAt(0) + 1));
	}
	return b.join('');
};

LOGIN.hasRole = function(role) {
	var has_role = false;
	for(var i=0; i<LOGIN.operator_roles.length; i++) {
		has_role = has_role || (LOGIN.operator_roles[i].role == role);
	}
	return has_role;
}

$(document).ready(
	function() {
		LOGIN.checkSession();
		$('#password').on('keypress', function(event) {
			if (event.which == 13) {
				LOGIN.verifyLogin();
			}
		});
	}
);
