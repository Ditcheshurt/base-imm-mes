function fill_table_by_cols (table_id, cols, data, double_wide) {
    if (data) {
        var t = $('#'+table_id);
        var thead = t.find('thead');
        var tbody = t.find('tbody');

        thead.empty();
        tbody.empty();

        // fill in the headers
        var thr = $('<tr></tr>');

        var repeat_cols = double_wide ? 2 : 1;

        for (var i=0; i<repeat_cols; i++) {
            $.each(cols, function (j, col) {
                var th = $('<th>'+col.title+'</th>');
                thr.append(th);
            });
        }

        if (double_wide) {
            // use two sets of the same columns
            var tr;

            for (var i=0; i<data.length; i++) {
                // create a new row if we're on an even index
                if (i % 2 == 0) {
                    tr = $('<tr></tr>');
                }

                // add item to the current row
                $.each(cols, function (j, col) {
                    if (col.type == 'datetime') {
                        var td = $('<td>'+data[i][col.data].date+'</td>');    
                        td.data(col.data, data[i][col.data].date);
                    } else {
                        var td = $('<td>'+data[i][col.data]+'</td>');
                        td.data(col.data, data[i][col.data]);
                    }
                    tr.append(td);
                });

                tbody.append(tr);
            }
        } else {
            $.each(data, function (i, item) {
                var tr = $('<tr></tr>');

                $.each(cols, function (j, col) {
                    if (col.type == 'datetime') {
                        var td = $('<td>'+item[col.data].date+'</td>');    
                        td.data(col.data, item[col.data].date);
                    } else {
                        var td = $('<td>'+item[col.data]+'</td>');
                        td.data(col.data, item[col.data]);
                    }
                    tr.append(td);
                });
                tbody.append(tr);
            });
        }

        thead.append(thr);
    }
}

function fill_table_by_rows (table_id, rows, data) {
    if (data) {
        var t = $('#'+table_id);
        var tbody = t.find('tbody');
        
        tbody.empty();

        $.each(rows, function (i, row) {
            var tr = $('<tr></tr>');
            var label_td = $('<td><label>'+row.title+'</label></td>');

            console.log(row);
            if (row.type == 'datetime') {
                var val_td = $('<td>'+data[row.data].date+'</td>');
                val_td.data(row.data, data[row.data].date);
            } else {
                var val_td = $('<td>'+data[row.data]+'</td>');
                val_td.data(row.data, data[row.data]);
            }

            tr.append(label_td);
            tr.append(val_td);
            tbody.append(tr);
        });
    }
}