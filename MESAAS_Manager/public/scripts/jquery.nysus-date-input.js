// example html
// <select id="test_select" name="test_id"></select>

// non-ajax example

// var test_data = [
//     {ID: 0, text: 'Option 0'},
//     {ID: 1, text: 'Option 1'},
//     {ID: 2, text: 'Option 2'}
// ];

// $('#test_select').fill_select({
//     name: 'test_id',
//     id: 'ID',
//     data: test_data,
//     default_value: 1,
//     render: function (item_data) {return item_data.text;}
// });


// ajax example

// $('#test_select').fill_select({
//     name: 'line_id',
//     id: 'ID',
//     data_url: 'ajax_processors/filter.php',
//     params: {action: 'get_lines'},
//     render: function (line) {return line.line_desc;}
// });


// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "nysus_date_input";
    var defaults = {
        propertyName: "value"
    };

    // The actual plugin constructor
    function Plugin (element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            //console.log(this.element);

            if (this.settings.date != undefined) {
                $(this.element).val(this.settings.date);
            } else {
                // initialize the date input to the current date
                var today = new Date();
                var offset = today.getTimezoneOffset() * 60;
                var offset_date = new Date(today.getTime() - offset * 1000);
                var today_str = offset_date.toISOString().split('T')[0];
                $(this.element).val(today_str);
            }
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

    function iso_day () {
        var date_str = $(this).val();
        return new Date(date_str).toISOString().split('T')[0];
    }

})(jQuery, window, document);