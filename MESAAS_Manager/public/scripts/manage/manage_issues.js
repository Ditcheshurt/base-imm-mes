var processor = './ajax_processors/manage/issues.php';

$(document).ready(	
	function() {
		loadAllData(populateIssues);
		
		var cols = [ 'Actions', 'Line', 'Station', 'Created By', 'Resolved By', 
			'Description', 'Start Date', 'Resolved Date' ];
		var t = $('#tbl_issues');
		
		// table header
		th = $('#tbl_issues thead');
		$.each(cols, function(k,v) {
			th.append('<th>' + v + '</th>');
		});
		
		// insert button
		$('.btn-new').on('click', function() {
			$('#div_edit_modal input').each(function (i, input) {
				$(this).val('');
			});
			$('#div_edit_modal').modal('show');			
		});
		
		// delete action
		t.on('click', '.btn-delete', function() {
			var id = $(this).closest('tr').prop('id');
			$('#div_delete_modal').data('id', id);
			$('#div_delete_modal').modal('show');
		});
		
		// delete modal buttons
		$('#div_delete_modal').on('click', '.btn-confirm', function() { 
				deleteIssue(function() {
					$('#div_delete_modal').modal('hide');
					loadAllData(populateIssues);
				});				
			}
		);
		
		// edit action
		t.on('click', '.btn-edit', function() {
			var row = $(this).closest('tr');
			var id = row.prop('id');
			$('#div_edit_modal').data('id', id);

			var cells = row.children('td');
			$('#div_edit_modal #inpLineID').val($(cells[1]).html());	
			$('#div_edit_modal #inpStationID').val($(cells[2]).html());
			$('#div_edit_modal #inpCreatedBy').val($(cells[3]).html());
			$('#div_edit_modal #inpResolvedBy').val($(cells[4]).html());
			$('#div_edit_modal #inpDescription').val($(cells[5]).html());
			$('#div_edit_modal #inpStartDate').val($(cells[6]).html().split(' ')[0]);
			$('#div_edit_modal #inpResolvedDate').val($(cells[7]).html().split(' ')[0]);

			$('#div_edit_modal').modal('show');
		});
		
		// edit modal buttons
		$('#div_edit_modal').on('click', '.btn-confirm', function() {
			$('#div_edit_modal').modal('hide');
			saveIssue(function () {
				loadAllData(populateIssues);
			});
		});				
		
	}
);

function loadAllData(cb) {
	// load data
	$.getJSON(processor, {
		'action':'select_issues'
	}, cb);
}

function populateIssues(jso) {	
	var t = $('#tbl_issues .tbody');
	t.empty();
	$.each(jso.issues, function(k,v) {
		var tr = '<tr id="'+v.id+'"><td>'+getActionButtons()+'</td>';
		tr += '<td>'+ (v.line_id ? v.line_id : '') +'</td>';
		tr += '<td>'+ (v.station_id ? v.station_id : '') +'</td>';
		tr += '<td>'+ (v.created_by ? v.created_by : '') +'</td>';
		tr += '<td>'+ (v.resolved_by ? v.resolved_by : '') +'</td>';
		tr += '<td>'+ (v.description ? v.description : '') +'</td>';
		tr += '<td>'+ (v.start_date ? v.start_date.date : '') +'</td>';
		tr += '<td>'+ (v.resolved_date ? v.resolved_date.date : '')+'</td></tr>';
		t.append(tr);
	});
}

function saveIssue(cb) {
	var action = 'update_issue';
	var id = $('#div_edit_modal').data('id');
	
	var arr = {
			'action':'update_issue',
			'id':parseInt(id),
			'line_id':parseInt($('#div_edit_modal #inpLineID').val()) || null,	
			'station_id':parseInt($('#div_edit_modal #inpStationID').val()) || null,
			'created_by':$('#div_edit_modal #inpCreatedBy').val() || null,
			'resolved_by':$('#div_edit_modal #inpResolvedBy').val() || null,
			'description':$('#div_edit_modal #inpDescription').val() || null,
			'start_date':$('#div_edit_modal #inpStartDate').val() || null,
			'resolved_date':$('#div_edit_modal #inpResolvedDate').val() || null
		};
		
	if (id === null || typeof(id) === 'undefined') {
		arr = 
		{
			'action':'insert_issue',
			'line_id':parseInt($('#div_edit_modal #inpLineID').val()) || null,				
			'station_id':parseInt($('#div_edit_modal #inpStationID').val()) || null,
			'created_by':$('#div_edit_modal #inpCreatedBy').val() || null,
			'resolved_by':$('#div_edit_modal #inpResolvedBy').val() || null,
			'description':$('#div_edit_modal #inpDescription').val() || null,
			'start_date':$('#div_edit_modal #inpStartDate').val() || null,
			'resolved_date':$('#div_edit_modal #inpResolvedDate').val() || null
		};
	}

	console.log(arr);
	$.getJSON(processor, arr, cb);
}

function deleteIssue(cb) {
	var id = $('#div_delete_modal').data('id');
	$.getJSON(processor, {
		"action":"delete_issue",
		"id":id
	}, cb);
}

function getActionButtons() {	
	var btns = '<button class="btn btn-success btn-xs btn-edit"><span class="glyphicon glyphicon-ok"></span> Edit</button>&nbsp;';
	btns += '<button class="btn btn-danger btn-xs btn-delete"><span class="glyphicon glyphicon-remove"></span> Delete</button>';
	return btns;
}