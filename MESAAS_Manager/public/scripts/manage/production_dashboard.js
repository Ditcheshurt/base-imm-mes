var processor = './ajax_processors/manage/production_dashboard.php';
$(document).ready(function () {

			var hours = [];
			for (var i = 1; i < 25; i++) {
				hours.push(i);
			}

			$('#tbl_editor').jtable({
				title : 'Hourly Required Parts',
				paging : true, //Enable paging
				pageSize : 10, //Set page size (default: 10)
				sorting : true, //Enable sorting
				defaultSorting : 'hour ASC', //Set default sorting
				jqueryuiTheme: true,  //use jquery ui theme
				actions : {
					listAction : processor + '?action=list',
					deleteAction : processor + '?action=delete',
					updateAction : processor + '?action=update',
					createAction : processor + '?action=create'
				},
				fields : {
					ID : {
						key : true,
						create : false,
						edit : false,
						list : false
					},
					cell_ID : {
						title : 'Cell',
						width : '23%',
						options :  processor + '?action=get_lines',
						list : false
					},
					cell_desc : {
						title : 'Cell Description',
						width : '23%',
						list : true,
						create: false,
						edit: false,
					},
					hour : {
						title : 'Hour',
						width : '13%',
						options : hours
					},
					required_qty : {
						title : 'Required Qty',
						width : '13%'
					}
				}
			});

			//Load list from server
			$('#tbl_editor').jtable('load');
			
		});