var processor = './ajax_processors/manage/containers.php';
var lines = [];
var stations = [];
var parts = [];
var xref = [];
var sel_line_ID = 0;

$(document).ready(
	function() {
		loadAllData(populateLines);

		$('#btn-load').on('click', function() {
			loadTaresContainers();
		});

		$('#div_current table tbody').on('click', 'BUTTON.expand_tare', function() {
			expandTare($(this), true);
		});

		$('#div_current table tbody').on('click', 'BUTTON.expand_container', function() {
			expandContainer($(this), true);
		});

		$('#div_recent table tbody').on('click', 'BUTTON.expand_tare', function() {
			expandTare($(this), false);
		});

		$('#div_recent table tbody').on('click', 'BUTTON.expand_container', function() {
			expandContainer($(this), false);
		});

		$('#div_partials table tbody').on('click', 'BUTTON.expand_partial', function() {
			expandPartial($(this));
		});

		$('DIV.main_list table tbody').on('click', 'BUTTON.history', function() {
			showPartHistory($(this));
		});

		$('DIV.main_list table tbody').on('click', 'BUTTON.reprint_part', function() {
			reprintPart($(this));
		});
	}
);

function loadAllData(cb) {
	$.getJSON(processor, {
			"action":"load_lines"
		}, cb);
}

function populateLines(jso) {
	lines = jso.lines || [];

	$('#selLine').html('');
	$.each(lines, function() {
		$('#selLine').append($("<option />").val(this.ID).text(this.cell_desc));
	});

	$('#btn-load').removeClass('disabled');
}

function loadTaresContainers() {
	$('#sp_loading').html('<img src="/images/loading.gif">').show();
	sel_line_ID = $("#selLine option:selected" ).val();
	$.getJSON(processor, {"action":"get_tares_containers", "line_ID":sel_line_ID}, showTaresContainers);
}

function showTaresContainers(jso) {
	var t = $('#div_current table tbody');
	t.html('');
	$.each(jso.tares, function() {
		if (this.in_progress == 1) {
			var r = t.append($('<tr />'));
			var c = $('<td />');
			r.append(c);
			var b = $('<button />').html('<span class="glyphicon glyphicon-plus"></span>').addClass('btn btn-xs btn-primary expand_tare').data(this);
			c.append(b);
			var c = r.append($('<td />').text(this.part_desc));
			var c = r.append($('<td />').text(this.part_number));
			var c = r.append($('<td />').text(this.start_time.date));
			var c = r.append($('<td />').text(getTotalParts(this)));
			var c = $('<td />');
			c.append($('<button />').addClass('btn btn-xs btn-primary').html('End As Partial'));
			r.append(c);

			var r2 = $('<tr />'); //hidden row
			t.append(r2);
			r2.attr('id', 'tr_hidden_' + this.ID);
			var c2 = r2.append($('<td />').attr('colspan', '10'));  //full width cell
			r2.hide();
		}
	});
	$('#div_current').show();

	var t = $('#div_recent table tbody');
	t.html('');
	$.each(jso.tares, function() {
		if (this.in_progress == 0) {
			var r = t.append($('<tr />'));
			var c = $('<td />');
			r.append(c);
			var b = $('<button />').html('<span class="glyphicon glyphicon-plus"></span>').addClass('btn btn-xs btn-primary expand_tare').data(this);
			c.append(b);
			var c = r.append($('<td />').text(this.rl_master_serialnbr));
			var c = r.append($('<td />').text(this.part_desc));
			var c = r.append($('<td />').text(this.part_number));
			var c = r.append($('<td />').text(this.start_time.date));
			var c = r.append($('<td />').text(getTotalParts(this)));
			var c = r.append($('<td />').text(((this.verified_time != null)?'YES':'NO')));
			var c = $('<td />');
			c.append($('<button />').addClass('btn btn-xs btn-primary').html('Reprint'));
			r.append(c);

			var r2 = $('<tr />');
			t.append(r2); //hidden row
			r2.attr('id', 'tr_hidden_' + this.ID);
			var c2 = r2.append($('<td />').attr('colspan', '10'));  //full width cell
			r2.hide();
		}
	});
	$('#div_recent').show();

	//partials
	var t = $('#div_partials table tbody');
	t.html('');
	$.each(jso.partials, function() {
		if (this.built_parts.length > 0) {
			var r = t.append($('<tr />'));
			var c = $('<td />');
			r.append(c);
			var b = $('<button />').html('<span class="glyphicon glyphicon-plus"></span>').addClass('btn btn-xs btn-primary expand_partial').data(this);
			c.append(b);
			var c = r.append($('<td />').text(this.ID));
			var c = r.append($('<td />').text(this.built_parts[0].part_desc));
			var c = r.append($('<td />').text(this.built_parts[0].part_number));
			var c = r.append($('<td />').text(this.create_time.date));
			var c = r.append($('<td />').text(this.built_parts.length));
			var c = $('<td />');
			c.append($('<button />').addClass('btn btn-xs btn-primary').html('Reprint'));
			r.append(c);

			var r2 = $('<tr />');
			t.append(r2); //hidden row
			r2.attr('id', 'tr_hidden_partial_' + this.ID);
			var c2 = r2.append($('<td />').attr('colspan', '10'));  //full width cell
			r2.hide();
		}
	});
	$('#div_partials').show();

	$('#sp_loading').html('').hide();
}

function expandTare(btn, allow_add) {
	var d = btn.data();
	var r = $('#tr_hidden_' + d.ID);

	if (r.css('display') == 'none') {
		//table to create in cell
		var t2 = $('<table />').addClass('table table-condensed table-striped');
		var t2h = $('<thead />');
		t2.append(t2h);
		var r2 = $('<tr />');
		t2h.append(r2);
		var c = r2.append($('<th />').text('Expand Parts'));
		var c = r2.append($('<th />').text('Start Time'));
		var c = r2.append($('<th />').text('End Time'));
		var c = r2.append($('<th />').text('Std Pack'));
		var c = r2.append($('<th />').text('Num Parts'));
		var c = r2.append($('<th />').text('Actions'));

		var tb = $('<tbody />');
		t2.append(tb);
		for (var i = 0; i < d.containers.length; i++) {
			var cnt = d.containers[i];
			var r2 = $('<tr />');
			var c = $('<td />');
			r2.append(c);
			var b = $('<button />').html('<span class="glyphicon glyphicon-plus"></span>').addClass('btn btn-xs btn-primary expand_container').data(cnt);
			c.append(b);
			var c = r2.append($('<td />').text(cnt.start_time.date));
			var c = r2.append($('<td />').text(((cnt.end_time != null)?cnt.end_time.date:'IN PROGRESS')));
			var c = r2.append($('<td />').text(cnt.std_pack_qty));
			var c = r2.append($('<td />').text(cnt.built_parts.length));
			var c = $('<td />');
			if (allow_add) {
				c.append($('<button />').addClass('btn btn-xs btn-primary').html('Add Part'));
			}
			r2.append(c);
			tb.append(r2);

			var r2 = $('<tr />');
			t2.append(r2); //hidden row
			r2.attr('id', 'tr_hidden_container_' + cnt.ID);
			var c2 = r2.append($('<td />').attr('colspan', '10'));  //full width cell
			r2.hide();
		}

		r.find('TD').html('').append(t2);
		r.css('display', 'table-row');
		btn.find('SPAN').removeClass('glyphicon-plus').addClass('glyphicon-minus');
	} else {
		r.css('display', 'none');
		btn.find('SPAN').removeClass('glyphicon-minus').addClass('glyphicon-plus');
	}
}

function expandContainer(btn, allow_remove) {
	var d = btn.data();
	var r = $('#tr_hidden_container_' + d.ID);
	r.find('TD').html('');

	if (r.css('display') == 'none') {
		//table to create in cell
		var t2 = $('<table />').addClass('table table-condensed table-striped');
		var t2h = $('<thead />');
		t2.append(t2h);
		var r2 = $('<tr />');
		t2h.append(r2);
		var c = r2.append($('<th />').text('Serial'));
		var c = r2.append($('<th />').text('Built Time'));
		var c = r2.append($('<th />').text('Actions'));

		var tb = $('<tbody />');
		t2.append(tb);
		for (var i = 0; i < d.built_parts.length; i++) {
			var bp = d.built_parts[i];
			var r2 = $('<tr />');
			var c = r2.append($('<td />').text(bp.serial));
			var c = r2.append($('<td />').text(((bp.built_time != null)?bp.built_time.date:'IN PROGRESS')));
			var c = $('<td />');
			if (allow_remove) {
				c.append($('<button />').addClass('btn btn-xs btn-primary').html('Remove Part'));
			}
			c.append($('<button />').addClass('btn btn-xs btn-primary reprint_part').html('Reprint Label'));
			c.append($('<button />').addClass('btn btn-xs btn-primary history').html('Show History'));
			r2.append(c);
			t2.find('TBODY').append(r2);
		}

		r.find('TD').html('').append(t2);
		r.css('display', 'table-row');
		btn.find('SPAN').removeClass('glyphicon-plus').addClass('glyphicon-minus');
	} else {
		r.css('display', 'none');
		btn.find('SPAN').removeClass('glyphicon-minus').addClass('glyphicon-plus');
	}
}

function expandPartial(btn) {
	var d = btn.data();
	var r = $('#tr_hidden_partial_' + d.ID);
	r.find('TD').html('');

	if (r.css('display') == 'none') {
		//table to create in cell
		var t2 = $('<table />').addClass('table table-condensed table-striped');
		var t2h = $('<thead />');
		t2.append(t2h);
		var r2 = $('<tr />');
		t2h.append(r2);
		var c = r2.append($('<th />').text('Serial'));
		var c = r2.append($('<th />').text('Built Time'));
		var c = r2.append($('<th />').text('Actions'));

		var tb = $('<tbody />');
		t2.append(tb);
		for (var i = 0; i < d.built_parts.length; i++) {
			var bp = d.built_parts[i];
			var r2 = $('<tr />');
			var c = r2.append($('<td />').text(bp.serial));
			var c = r2.append($('<td />').text(((bp.built_time != null)?bp.built_time.date:'IN PROGRESS')));
			var c = $('<td />');
			c.append($('<button />').addClass('btn btn-xs btn-primary').html('Remove Part'));
			c.append($('<button />').addClass('btn btn-xs btn-primary reprint_part').html('Reprint Label'));
			c.append($('<button />').addClass('btn btn-xs btn-primary history').html('Show History'));
			r2.append(c);
			t2.find('TBODY').append(r2);
		}

		r.find('TD').html('').append(t2);
		r.css('display', 'table-row');
		btn.find('SPAN').removeClass('glyphicon-plus').addClass('glyphicon-minus');
	} else {
		r.css('display', 'none');
		btn.find('SPAN').removeClass('glyphicon-minus').addClass('glyphicon-plus');
	}
}

function showPartHistory(btn) {
	$('#div_part_history_modal').modal('show');
}

function reprintPart(btn) {
	var d = $('#div_confirm_modal');
	d.find('.modal-title').html('Confirm Reprint Part Label');
	d.find('.modal-body').html('Are you sure you want to reprint this part label?');
	d.find('.btn-confirm').html('Ok, Reprint Label').on('click', function() {
		//fire the reprint here
	});
	d.modal('show');
}

function getTotalParts(tare) {
	var res = 0;
	for (var i = 0; i < tare.containers.length; i++) {
		res += tare.containers[i].built_parts.length;
	}
	return res;
}