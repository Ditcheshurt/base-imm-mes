$(document).ready(function () {	
	console.log('doc ready');
	
	var gauge_data;
	
	// loads the data for the page
	init_data(populate_gauges);
	
	$('#btn_add_gauge').on('click', function(e) {
		e.preventDefault();		
		$('#add_gauge_modal').modal('show');		
		return false;
		
	});
	
	$('#add_gauge_modal').on('shown.bs.modal', function(e) {
		e.preventDefault();
		populate_modal();		
		return false;
	});
	
	$('#add_gauge_modal').on('click', '.btn-confirm', function(e) {
		e.preventDefault();		
		add_gauge();	
		return false;
		
	});
	
	$('#table-gauges').on('click', '.btn-save-gauge', function(e) {
		e.preventDefault();
		var tr = $(this).closest('tr');
		save_gauge(tr);	
		return false;
	});
	
	$('#table-gauges').on('click', '.btn-cancel-gauge', function(e) {
		e.preventDefault();
		init_data(populate_gauges);
		return false;
	});
	
	$('#table-gauges').on('click', '.btn-edit-row', function(e) {
		e.preventDefault();
		var tr = $(this).closest('tr');
		edit_gauge(tr);		
		return false;
	});
	
	$('#table-gauges').on('click', '.btn-show-instr', function(e) {
		e.preventDefault();
		var tr = $(this).closest('tr');
		show_instructions(tr);
		return false;
	});

	$('#table-gauges').on('click', '.btn-cancel-instr-row', function(e) {
		e.preventDefault();
		var tr = $(this).closest('tr');
		cancel_show_instructions(tr);
		return false;
	});
	
	$(document).on('mouseenter', '.show-instr-image', function(e) {
		e.preventDefault();
		var image_url = $(this).html();
		$(this).empty();
		$(this).append('<img src="' + image_url + '" alt="no image" height="200px" width="200px" />');		
		return false;
	});
	
	$(document).on('mouseleave', '.show-instr-image', function(e) {
		e.preventDefault();
		var image_url = $(this).find('img').attr('src');
		$(this).empty();
		$(this).html(image_url);
		return false;
	});
	
	$(document).on('click', '.btn-cancel-instr-row', function(e) {
		e.preventDefault();
		var tr = $(this).closest('tr');			
		cancel_instruction(tr);	
		return false;
	});
	
	$(document).on('click', '.btn-edit-instr-row', function(e) {
		e.preventDefault();		
		var tr = $(this).closest('tr');
		edit_instruction(tr);		
		return false;
		
	});	
	
	$(document).on('click', '.btn-add-instr-row', function(e) {		
		e.preventDefault();
		var btn = $(this);
		add_instruction(btn);		
		return false;
	});	
	
	$(document).on('click', '.btn-save-instr-row', function(e) {		
		e.preventDefault();
		var tr = $(this).closest('tr');		
		save_instruction(tr);		
		return false;
	});	
		
	function init_data(callback) {
		console.log('init_data');		
		NYSUS.REPETITIVE.GAUGES.get_data(function(jso) {
			gauge_data = jso;			
			callback();
		});
		
	}	
	
	function populate_gauges() {
		console.log('populate_gauges');
		
		var table = $('#table-gauges');
		var tbody = table.find('tbody');
		
		tbody.empty();
		
		$.each(gauge_data.gauges, function(k,v) {
			var tr = $('<tr data-gauge-id="' + v.ID + '"></tr>');
			
			tr.append('<td><button class="btn btn-xs btn-info btn-edit-row"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</button>&nbsp;<button class="btn btn-xs btn-warning btn-show-instr"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Instructions</button></td>');
			tr.append('<td>' + v.description + '</td>');			
			tr.append('<td>' + v.tool_part[0].machine_number + '-' + v.tool_part[0].machine_name + '&nbsp;|&nbsp;' + v.tool_part[0].part_number + '&nbsp;|&nbsp;' + v.tool_part[0].part_desc + '</td>');			
			tr.append('<td>' + v.parts_per_gauging + '</td>');
			tr.append('<td>' + v.parts_per_gauging_warning_offset + '</td>');
			tr.append('<td>' + v.parts_per_gauging_maximum_offset + '</td>');
			tr.append('<td>' + v.station_ip_address + '</td>');
			tr.append(v.active ? '<td><span class="glyphicon glyphicon-ok"></span></td>' : '<td><span class="glyphicon glyphicon-remove"></span><td>');
			
			tbody.append(tr);
			
		});
		
		table.DataTable();
		
	}
	
	function populate_modal() {
		var sel_part = $('#inp_part_number');
		sel_part.append('<option>Select Part</option>');		
		$.each(gauge_data.tool_parts, function(k,v) {
			var opt = $('<option value="' + v.ID + '">' + v.machine_number + '-' + v.machine_name + '&nbsp;|&nbsp;' + v.part_number + '&nbsp;|&nbsp;' + v.part_desc + '</option>');				
			sel_part.append(opt);
			
		});
	}
	
	function edit_gauge(tr) {
		console.log('edit_gauge');
		
		var gauge_id = tr.data('gaugeId');
		var data;
		
		// get the data for the gaugeId
		$.each(gauge_data.gauges, function(k,v) {
			if (v.ID == gauge_id) {
				data = v;					
			}
		});
		
		tr.empty();
		tr.append('<td><button class="btn btn-xs btn-success btn-save-gauge"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save</button><button class="btn btn-xs btn-danger btn-cancel-gauge"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</button></td>');
		tr.append('<td><input class="form-control" data-field="description" type="text" value="' + data.description + '" /></td>');
		
		var sel_part = $('<select class="form_control" data-field="tool_part_ID"></select>');
		sel_part.append('<option>Select Part</option>');		
		$.each(gauge_data.tool_parts, function(k,v) {
			var opt = $('<option value="' + v.ID + '">' + v.machine_number + '-' + v.machine_name + '&nbsp;|&nbsp;' + v.part_number + '&nbsp;|&nbsp;' + v.part_desc + '</option>');
			if (v.ID == data.tool_part_ID) {				
				sel_part.append(opt);
				opt.val(v.ID);
				sel_part.val(data.tool_part_ID);
			} else {				
				sel_part.append(opt);
			}
		});
		var td = $('<td />');
		td.append(sel_part);
		tr.append(td);
		tr.append('<td><input class="form-control" data-field="parts_per_gauging" type="text" value="' + data.parts_per_gauging + '" /></td>');
		tr.append('<td><input class="form-control" data-field="parts_per_gauging_warning_offset" type="text" value="' + data.parts_per_gauging_warning_offset + '" /></td>');
		tr.append('<td><input class="form-control" data-field="parts_per_gauging_maximum_offset" type="text" value="' + data.parts_per_gauging_maximum_offset + '" /></td>');
		tr.append('<td><input class="form-control" data-field="station_ip_address" type="text" value="' + data.station_ip_address + '" /></td>');
		
		var cb = $('<input class="form-control" data-field="active" type="checkbox" />');
		if (data.active) {
			cb.prop('checked', 'checked');
		}
		var td2 = $('<td />');
		td2.append(cb);
		tr.append(td2);
				
	}
	
	function save_gauge(tr) {
		console.log('save_gauge_row');	
		
		var gauge_id = tr.data('gaugeId');
		var inputs = tr.find('[data-field]');
		var field_names = [];
		var field_values = [];		
		
		$.each(inputs, function(k,v) {
			field_names.push($(v).data('field'));
			switch($(v).data('field')) {
				case 'active':
					field_values.push($(v).prop('checked'));	
					break;
				default:
					field_values.push($(v).val());	
					break;
			}				
			
		});
		
		NYSUS.REPETITIVE.GAUGES.update_gauge(gauge_id, field_names, field_values, function(jso) {
			init_data(populate_gauges);
		});
		
	}
	
	function add_gauge() {
		console.log('add_gauge');
		
		var inputs = $('#add_gauge_modal').find('[data-field]');
		var field_names = [];
		var field_values = [];
		
		$.each(inputs, function(k,v) {
			field_names.push($(v).data('field'));
			switch($(v).data('field')) {
				case 'active':
					field_values.push($(v).prop('checked'));	
					break;
				default:
					field_values.push($(v).val());	
					break;
			}				
			
		});
		
		NYSUS.REPETITIVE.GAUGES.insert_gauge(field_names, field_values, function(jso) {
			if(jso) {
				$('#add_gauge_modal').modal('hide');
				$('#add_gauge_modal_result').modal('show');
				init_data(populate_gauges);
			}
			
		});
		
	}
	
	function show_instructions(tr) {
		console.log('show_instructions');
		
		var data;
		var gauge_id = $(tr).data('gaugeId');
		var instr_tr = $('<tr class="well"></tr>');		
		var table = $('<table class="table table-striped table-condensed table-instructions"></table>');
		var thead = $('<thead></thead>');
		var tbody = $('<tbody></tbody>');
		var td = $('<td colspan="8"></td>');
		
		thead.append('<tr><th><button class="btn btn-xs btn-success btn-add-instr-row" data-gauge-id="' + gauge_id + '"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Add Instruction</button><button class="btn btn-xs btn-warning btn-cancel-instr-row"<span class="glyphicon glyphicon-remove"></span>&nbsp;Close</button></th></tr>');
		thead.append('<tr><td colspan="8"></td></tr><tr><th>Actions</th><th>Order</th><th>Description</th><th>Instruction</th><th>Pass Fail</th><th>Base</th><th>Min</th><th>Max</th><th>Image Url</th><th>Units</th><th>Active</th></tr>');
		table.append(thead);
		table.append(tbody);
		
		$.each(gauge_data.gauges, function(k,v) {
			if (v.ID == gauge_id) {
				data = v;
			}
		});		
		
		if (data.instructions) {
			$.each(data.instructions, function(k,v) {
				var tr = $('<tr data-gauge-id="' + gauge_id + '" data-instr-id="' + v.ID + '"></tr>');
				
				tr.append('<td data-field="actions"><button class="btn btn-xs btn-info btn-edit-instr-row"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</button></td>');
				tr.append('<td data-field="instr_order">' + v.instr_order + '</td>');
				tr.append('<td data-field="description">' + v.description + '</td>');
				tr.append('<td data-field="instruction">' + v.instruction + '</td>');
				if (v.is_pass_fail == 1) {
					tr.append('<td data-field="is_pass_fail"><span class="glyphicon glyphicon-ok"></span></td>');					
				} else {
					tr.append('<td data-field="is_pass_fail"><span class="glyphicon glyphicon-remove"></span></td>');	
				}
				tr.append('<td data-field="base_value">' + v.base_value + '</td>');
				tr.append('<td data-field="min_value">' + v.min_value + '</td>');
				tr.append('<td data-field="max_value">' + v.max_value + '</td>');
				tr.append('<td data-field="image_url"><span class="show-instr-image">' + v.image_url + '</span></td>');
				tr.append('<td data-field="units"><span class="show-units">' + v.units + '</span></td>');
				tr.append(v.active ? '<td data-field="active"><span class="glyphicon glyphicon-ok"></span></td>' : '<td><span class="glyphicon glyphicon-remove"></span><td>');			
				
				tbody.append(tr);
			});
		}
		
		td.append(table);
		instr_tr.append(td);
		instr_tr.insertAfter(tr);
		
		$(tr).find('.btn-show-instr').hide();
		
	}

	function cancel_show_instructions(tr) {
		tr.closest('.well').prev().find('.btn-show-instr').show();
		tr.closest('.well').remove();
	}
	
	function edit_instruction(tr) {
		console.log('edit_instruction');
		
		var data;		
		var cols = tr.find('td');
		var gauge_id = tr.data('gaugeId');
		var instr_id = tr.data('instrId');
		
		$.each(gauge_data.gauges, function(k,v) {			
			if (v.ID == gauge_id) {				
				$.each(v.instructions, function(k,v) {
					if (v.ID == instr_id) {
						data = v;
					}					
				});				
			}			
		});	
		
		$.each(cols, function(k,v) {
			var td = $(v);
			td.empty();
			
			switch(td.data('field')) {
				case 'actions':
					td.append('<button class="btn btn-xs btn-success btn-save-instr-row"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save</button>');
					td.append('<button class="btn btn-xs btn-danger btn-cancel-instr-row"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</button>');
					break;					
				case 'instr_order':					
					td.append('<input class="form-control inp_instr_order" data-field="instr_order" type="number" value="' + data.instr_order + '" />');
					break;
				case 'description':
					td.append('<input class="form-control inp_instr_description" data-field="description" type="text" value="' + data.description + '" />');
					break;
				case 'instruction':
					td.append('<input class="form-control inp_instruction" data-field="instruction" type="text" value="' + data.instruction + '" />');
					break;
				case 'is_pass_fail':
					var cb_pass = $('<input class="form-control inp_is_pass_fail" data-field="is_pass_fail" type="checkbox" />');
					if (data.is_pass_fail) {
						cb_pass.prop('checked', 'checked');
					}
					td.append(cb_pass);
					break;
				case 'base_value':
					td.append('<input class="form-control inp_base_value" data-field="base_value" type="text" value="' + data.base_value + '" />');
					break;
				case 'min_value':
					td.append('<input class="form-control inp_min_value" data-field="min_value" type="text" value="' + data.min_value + '" />');
					break;
				case 'max_value':
					td.append('<input class="form-control inp_max_value" data-field="max_value" type="text" value="' + data.max_value + '" />');
					break;
				case 'image_url':
					td.append('<input class="form-control inp_image_url" data-field="image_url" type="text" value="' + data.image_url + '" />');
					break;
				case 'units':
					td.append('<input class="form-control inp_units" data-field="units" type="text" value="' + data.units + '" />');
					break;
				case 'active':
					var cb = $('<input class="form-control inp_active" data-field="active" type="checkbox" />');
					if (data.active) {
						cb.prop('checked', 'checked');
					}
					td.append(cb);
					break;
				default:
					td.append('<span>not configured</span>');
					break;
			}
			
		});	
		
	}
	
	function add_instruction(btn) {
		console.log('add_instruction');
		
		var gauge_id = $(btn).data('gaugeId');
		var first_row = btn.closest('.table-instructions').find('tbody>tr:first');
		var tr = $('<tr data-gauge-id="' + gauge_id + '"></tr>');
		
		tr.append('<td><button class="btn btn-xs btn-success btn-save-instr-row"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Save</button></td>');
		tr.find('td:last').append('<button class="btn btn-xs btn-danger btn-cancel-instr-row"><span class="glyphicon glyphicon-remove"></span>&nbsp;Cancel</button>');
		tr.append('<td><input class="form-control inp_instr_order" data-field="instr_order" type="number" value="" /></td>');
		tr.append('<td><input class="form-control inp_instr_description" data-field="description" type="text" value="" /></td>');
		tr.append('<td><input class="form-control inp_instruction" data-field="instruction" type="text" value="" /></td>');
		tr.append('<td><input class="form-control inp_is_pass_fail" data-field="is_pass_fail" type="checkbox" /></td>');
		tr.append('<td><input class="form-control inp_min_value" data-field="min_value" type="text" value="" /></td>');
		tr.append('<td><input class="form-control inp_max_value" data-field="max_value" type="text" value="" /></td>');
		tr.append('<td><input class="form-control inp_image_url" data-field="image_url" type="text" value="" /></td>');
		tr.append('<td><input class="form-control inp_active" data-field="active" type="checkbox" />');
		
		if (first_row.length != 0) {
			first_row.before(tr);			
		} else {
			btn.closest('.table-instructions').find('tbody').append(tr);
		}
		
	}
	
	function save_instruction(tr) {
		console.log('save_instruction');
		
		var well = $(tr).closest('.well').prev();
		var gauge_id = well.data('gaugeId');		
		var instruction_id = tr.data('instrId');		
		var inputs = tr.find('input[data-field]');
		var field_names = [];
		var field_values = [];		
		
		$.each(inputs, function(k,v) {
			field_names.push($(v).data('field'));
			switch($(v).data('field')) {
				case 'is_pass_fail':
					field_values.push($(v).prop('checked'));	
				break;
				case 'active':
					field_values.push($(v).prop('checked'));	
					break;
				default:
					field_values.push($(v).val());	
					break;
			}				
			
		});
		
		if (instruction_id) {
			NYSUS.REPETITIVE.GAUGES.update_instruction(instruction_id, field_names, field_values, function(jso) {			
				if(jso) {				
					init_data(function() {
						$(well).next('tr').remove();
						show_instructions(well);
					});
				}
			});
			
		} else {
			field_names.push('gauge_material_type_ID');
			field_values.push(gauge_id);
			
			NYSUS.REPETITIVE.GAUGES.insert_instruction(field_names, field_values, function(jso) {			
				if(jso) {				
					init_data(function() {
						$(well).next('tr').remove();
						show_instructions(well);
					});
				}
			});
		}		
		
	}
	
	function cancel_instruction(tr) {
		console.log('cancel_instruction');
		
		var data;
		var gauge_id = $(tr).data('gaugeId');
		var instr_id = $(tr).data('instrId');
		
		$.each(gauge_data.gauges, function(k,v) {
			if (v.ID == gauge_id) {
				data = v;
			}
		});		
		
		if (instr_id) {
			$.each(data.instructions, function(k,v) {
				if(v.ID == instr_id) {
					tr.empty();
					
					tr.append('<td data-field="actions"><button class="btn btn-xs btn-info btn-edit-instr-row"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</button></td>');
					tr.append('<td data-field="instr_order">' + v.instr_order + '</td>');
					tr.append('<td data-field="description">' + v.description + '</td>');
					tr.append('<td data-field="instruction">' + v.instruction + '</td>');
					if (v.is_pass_fail == 1) {
						tr.append('<td data-field="is_pass_fail"><span class="glyphicon glyphicon-ok"></span></td>');					
					} else {
						tr.append('<td data-field="is_pass_fail"><span class="glyphicon glyphicon-remove"></span></td>');	
					}
					tr.append('<td data-field="base_value">' + v.base_value + '</td>');
					tr.append('<td data-field="min_value">' + v.min_value + '</td>');
					tr.append('<td data-field="max_value">' + v.max_value + '</td>');
					tr.append('<td data-field="image_url"><span class="show-instr-image">' + v.image_url + '</span></td>');
					tr.append('<td data-field="units">' + v.units + '</td>');
					tr.append(v.active ? '<td data-field="active"><span class="glyphicon glyphicon-ok"></span></td>' : '<td><span class="glyphicon glyphicon-remove"></span><td>');	
				}			
			});
		} else {
			tr.remove();
		}		
		
	}
	
});