$(document).ready(function () {
	
	populate_lot_table();
	
	$('#btn_new_lot').on('click', function(e) {
		e.preventDefault();
		$('#edit_lot_modal').removeData();
		
		$('#inp_description').val('');
		$('#inp_alt_part_number').val('');
		$('#inp_part_number').val('');
		//$('#inp_parts_per_lot').val('');
		$('#inp_per_lot_default').val('');
		$('#inp_warning_offset').val('');
		$('#inp_maximum_offset').val('');
		$('#inp_station_ip_address').val('');
		
		$('.row-lot-machine').hide();
		$('#edit_lot_modal').modal('show');
		return false;
	});
	
	$('#table-lots').on('click', '.btn-edit-row', function(e) {
		e.preventDefault();
		var data = $(this).closest('tr').data();
		$('#edit_lot_modal').data(data);
		$('#inp_description').val(data.description);
		$('#inp_alt_part_number').val(data.alt_part_number);
		$('#inp_part_number').val(data.part_number);
		//$('#inp_parts_per_lot').val(data.parts_per_lot);
		$('#inp_per_lot_default').val(data.parts_per_lot_default);
		$('#inp_warning_offset').val(data.parts_per_lot_warning_offset);
		$('#inp_maximum_offset').val(data.parts_per_lot_maximum_offset);
		$('#inp_station_ip_address').val(data.station_ip_address);
		$('#edit_lot_modal').modal('show');		
		
		show_lot_machines();		
		return false;
	});
	
	$('#edit_lot_modal').on('click', '#btn_show_add_machine', function(e) {
		e.preventDefault();
		show_add_machine();
		return false;
	});
	
	$('#edit_lot_modal').on('click', '#btn_add_machine', function(e) {
		e.preventDefault();
		add_machine();
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_close_detail', function(e) {
		e.preventDefault();		
		$(this).closest('tr').prev().find('button').show();
		$(this).parent().hide();
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_delete_lot_tool_part', function(e) {
		e.preventDefault();
		var lot_tool_part_id = $(this).data('lotToolPartId'); 
		var row = $(this).closest('table').closest('tr').prev('tr');				
		var detail = $(this).closest('.detail'); // remove the detail view
		
		NYSUS.REPETITIVE.LOT.delete_lot_tool_part(lot_tool_part_id, function(jso) {
			detail.remove();
			show_lot_machine_detail(row);
		});
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_remove_machine', function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		NYSUS.REPETITIVE.LOT.delete_lot_machine(id, show_lot_machines);
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_show_tool_part', function(e) {
		e.preventDefault();
		var machine_id = $(this).data('machineId');
		show_add_tool_part(machine_id);
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_add_tool_part', function(e) {
		e.preventDefault();
		var lot_id = $('#edit_lot_modal').data('ID');
		var tool_part_id = $('.sel_tool_part').val();
		var row = $(this).closest('table').closest('tr').prev();
		var qty_per_part = $(this).closest('tr').find('.inp-add-qty-per-part').val();		
		var field_names = [
		                   'lot_material_type_ID'
		                   ,'tool_part_ID'
		                   ,'qty_per_part'
		                   ];
		var field_values = [
		                    lot_id,
		                    tool_part_id,
		                    qty_per_part
		                    ];
		
		$(this).closest('.jumbotron').remove();
		
		NYSUS.REPETITIVE.LOT.insert_lot_tool_part(field_names, field_values, function() {				
				show_lot_machine_detail(row);				
			});
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn_show_tool_parts', function(e) {
		e.preventDefault();
		var row = $(this).closest('tr');
		show_lot_machine_detail(row);
		return false;
	});
	
	$('#edit_lot_modal').on('click', '.btn-confirm', function(e) {
		e.preventDefault();
		save_lot();
		return false;
	});
	
	$('#edit_lot_modal').on('keyup', '.inp-qty-per-part', function(e) {
		e.preventDefault();		
		var tr = $(this).closest('tr');
		if (tr.find('.btn-save-qty-per-part').length <= 0) {
			tr.append('<td><button class="btn btn-xs btn-success btn-save-qty-per-part"><span class="glyphicon glyphicon-disk" />Save</button></td>')
		}
		return false;
		
	});
	
	$('#edit_lot_modal').on('click', '.btn-save-qty-per-part', function(e) {
		e.preventDefault();
		
		var btn = $(this);
		var tr = btn.closest('tr');
		var lot_tool_part_id = tr.find('.btn_delete_lot_tool_part').data('lotToolPartId');
		var qty_per_part = tr.find('.inp-qty-per-part').val();
		var field_names = ['qty_per_part'];
		var field_values = [qty_per_part];
		
		NYSUS.REPETITIVE.LOT.update_lot_tool_part(lot_tool_part_id, field_names, field_values, function(jso) {
			if (jso) {
				btn.remove();				
			}
		});
		
		return false;
	});
	
		                         	
 	
 	function populate_lot_table() {
    	var $table = $('#table-lots');
    	var head = $table.find('thead');
    	var body = $table.find('tbody');
    	var thead = '<tr><th>Actions</th><th>Description</th><th>Part Number</th><th>Alt Part Number</th>' +
    		'<th>Per Lot Default</th><th>Warning Offset</th><th>Maximum Offset</th><th>Station IP</th></tr>';
    	
    	head.empty();
    	head.append(thead);
    	
    	body.empty();
    	
    	var field_names = [
    	          	 		'ID',
    	          	 		'description',
    	          	 		'alt_part_number',
    	          	 		'part_number',
    	          	 		//'parts_per_lot',
    	          	 		'parts_per_lot_default',
    	          	 		'parts_per_lot_warning_offset',
    	          	 		'parts_per_lot_maximum_offset',
    	          	 		'station_ip_address'
    	          	 	];    	           		
    	
    	// get the lots
		NYSUS.REPETITIVE.LOT.get_lot_material_types(field_names, function(jso) {
			if (!jso) {
				return;
			}
			
	    	$.each(jso, function(k,v) {		
	    		var $tr = $('<tr />');
	    		$tr.append('<td><button class="btn btn-xs btn-info btn-edit-row"><span class="glyphicon glyphicon-pencil"/>&nbsp;Edit</button></td>');
	    		$tr.append('<td>' + v.description + '</td>');
	    		$tr.append('<td>' + v.part_number + '</td>');
	    		$tr.append('<td>' + v.alt_part_number + '</td>');
	    		//$tr.append('<td>' + v.parts_per_lot + '</td>');
	    		$tr.append('<td>' + v.parts_per_lot_default + '</td>');
	    		$tr.append('<td>' + v.parts_per_lot_warning_offset + '</td>');
	    		$tr.append('<td>' + v.parts_per_lot_maximum_offset + '</td>');
	    		$tr.append('<td>' + v.station_ip_address + '</td>');
	    		$tr.data(v);
	    		body.append($tr);            		
	    	});
	    	$table.DataTable({displayLength:25});
		});
    		    	
	}
	
	function show_lot_machines() {
		var lot_id = $('#edit_lot_modal').data('ID');
		var table = $('#table_lot_machines');		
		var tbody = table.find('tbody');
		tbody.empty();
		$('.row-lot-machine').show();
		
		var field_names = [
		               'lm.ID'
		               ,'lot_material_type_ID'
		               ,'machine_ID'
		               ,'m.machine_number'
		               ,'m.machine_name'
		 			];		
	
		NYSUS.REPETITIVE.LOT.get_lot_machines(lot_id, field_names, function(jso) {
			if (!jso) {
				return;
			}
			
			$.each(jso, function(k,v) {			
				var tr = $('<tr class="master"><td><button class="btn btn-xs btn-danger btn_remove_machine" data-id="' + 
						v.ID + '"><span class="glyphicon glyphicon-minus"></span>&nbsp;Remove</button>' + 
						'<button class="btn btn-xs btn-info btn_show_tool_parts"><span class="glyphicon glyphicon-search"></span>&nbsp;Tool Parts</button></td><td>' + 
						v.machine_number + '</td><td>' + v.machine_name +  
						'</td></tr>'); 
				tr.data(v);
				tbody.append(tr);
				
			});
			
			//dtLotMachines = table.DataTable();
		});	
	}
	
	function show_add_machine() {		
		var lot_id = $('#edit_lot_modal').data('ID');
		var container = $('#container_lot_machines');
		container.empty();
		
		var sel = $('<select id="sel_add_machine"><option>Select Machine</option></select>');
		
		var field_names = [
		                   'ID'
		                   ,'machine_number'
		                   ,'machine_name'
		              ];
		
		NYSUS.REPETITIVE.MACHINES.get_machines(field_names, function(jso) {
			$.each(jso, function(k,v) {
				var o = '<option value="' + v.ID + '">' + v.machine_number + ' - ' + v.machine_name + '</option>';				
				sel.append(o);
			});
			container.append(sel);
			container.append('<button id="btn_add_machine" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-plus"></span>&nbsp;Add</button>');
			
			$('#btn_show_add_machine').hide();
		});
		
	}
	
	function add_machine() {
		var lot_id = $('#edit_lot_modal').data('ID');
		var container = $('#container_lot_machines');
		// has selection insert machine
		if (container.find('#sel_add_machine option:selected').length > 0) {
			var field_names = [
			                   '[lot_material_type_ID]'
			                   ,'[machine_ID]'
			               ];
			var field_values = [
			                    lot_id
			                    , $('#sel_add_machine option:selected').val()
			               ];
			NYSUS.REPETITIVE.LOT.insert_lot_machine(field_names, field_values, function() {
				container.empty();
				$('#btn_show_add_machine').show(); 
				show_lot_machines();
			});
			
		}
		
	}
	
	function show_lot_machine_detail(row) {				
		var lot_id = $('#edit_lot_modal').data('ID');
		var machine_id = row.data('machine_ID');		
		var detail_tr = $('<tr class="detail"></tr>');
		var detail_td =  $('<td colspan="4" class="jumbotron"></td>');
		var detail_table = $('<table id="tbl_tp_detail_' + machine_id +
				'" class="table table-striped table-condensed"><thead><tr><th>Actions</th><th>Tool Name</th><th>Part Number</th><th>Part Name</th><th>Qty Per Part</th></tr></thead><tbody></tbody></table>');
		var detail_tbody = detail_table.find('tbody');		
		var field_names = [
		                   'ltp.ID AS lot_tool_part_ID'
				            ,'ltp.qty_per_part'
							,'p.part_number'
							,'p.part_desc'
							,'t.tool_description'
							];
		row.find('.btn_show_tool_parts').hide();
		
		detail_td.append('<button class="btn btn-xs btn-success btn_show_tool_part" data-machine-id="' + machine_id + '"><span class="glyphicon glyphicon-plus"></span>Add Tool Part</button>');
		detail_td.append('<button class="btn btn-xs btn-info btn_close_detail pull-right"><span class="glyphicon glyphicon-minus"></span></button>');
		detail_tbody.empty();
		
		NYSUS.REPETITIVE.LOT.get_lot_tool_parts(lot_id, machine_id, field_names, function(jso) {
			if(!jso) {
				detail_tbody.append('<tr><td colspan="5">No Lot Tool Parts Defined</td></tr>');
				return;
			}
			$.each(jso, function(k,v) {
				detail_tbody.append('<tr><td><button class="btn btn-xs btn-danger btn_delete_lot_tool_part" data-lot-tool-part-id="' + v.lot_tool_part_ID + '"><span class="glyphicon glyphicon-minus"></span>&nbsp;Remove</button></td>' +
						'<td>' + v.tool_description + '</td><td>' + v.part_number + '</td><td>' + v.part_desc + '</td><td><input class="inp-qty-per-part" type="number" value="' + v.qty_per_part + '"></input></td></tr>');				
			});
		});		
		
		detail_td.append(detail_table);
		detail_tr.append(detail_td);
		row.after(detail_tr);

	}
	
	function show_add_tool_part(machine_id) {
		var field_names = [
		           'tp.ID AS tool_part_ID'
		           ,'p.part_number'
		           ,'p.part_desc'
		           ,'t.tool_description'
		           ];
		
		NYSUS.REPETITIVE.MACHINES.get_machine_tool_parts(machine_id, field_names, function(jso) {
			if (!jso) {
				return;
			}
			var container = $('#tbl_tp_detail_' + machine_id).parent();
			var table = $('<table class="table table-condensed"><thead><tr><th>Tool Parts</th><th>Qty Per Part</th></tr></thead><tbody></tbody></table>');
			var tbody = table.find('tbody');			
			var row = $('<tr></tr>');
			var sel = $('<select class="sel_tool_part"><option>Select Tool Part</option></select>');
			var td = $('<td></td>');
			
			$.each(jso, function(k,v) {
				sel.append('<option value="' + v.tool_part_ID + '">'+ v.part_number + ' - ' + v.part_desc + '</option>');
			});
			td.append(sel);
			row.append(td);
			
			row.append('<td><input type="number" class="form-control inp-add-qty-per-part"></input></td>');
			row.append('<td><button class="btn btn-xs btn-success btn_add_tool_part">Save Tool Part</button></td>');
			table.append(row);
			container.empty().append(table);
		});
	}
	
	function save_lot() {
		var lot_id = $('#edit_lot_modal').data('ID');		
		var field_names = [
			'description',
			'alt_part_number',
			'part_number',
			'parts_per_lot',
			'parts_per_lot_default',
			'parts_per_lot_warning_offset',
			'parts_per_lot_maximum_offset',
			'station_ip_address'
		];
		var field_values = [
			$('#inp_description').val(),
			$('#inp_alt_part_number').val(),
			$('#inp_part_number').val(),
			$('#inp_parts_per_lot').val(),
			$('#inp_per_lot_default').val(),
			$('#inp_warning_offset').val(),
			$('#inp_maximum_offset').val(),
			$('#inp_station_ip_address').val()			
		];
		
		if (lot_id == null) {
			NYSUS.REPETITIVE.LOT.insert_lot_material_type(field_names, field_values, function(jso) {	
				populate_lot_table();
				$('#edit_lot_modal').modal('hide');
			});
		} else {
			// update the lot_material_types table
			NYSUS.REPETITIVE.LOT.update_lot_material_types(lot_id, field_names, field_values, function(jso) {	
				populate_lot_table();
				$('#edit_lot_modal').modal('hide');
			});
			
		}
		
	}
	
});