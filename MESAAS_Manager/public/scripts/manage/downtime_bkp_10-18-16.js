var processor = './ajax_processors/manage/downtime.php';
var downtimeTable;
var detailsTable;

String.prototype.toProperCase = function(){
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g, 
		function($1){
			return $1.toUpperCase();
		}
	);
};
	
$(function() {

	build_report_parameters();
	//build_report_results();
	build_grid_view();
	
	$('.details-table-modal').on('shown.bs.modal', function () {
	  //console.log($('.details-table-modal').data());
	  build_details_table_modal();	  
	});

	$('.btn-save').on('click', function() {
		
		// validation
		var $modal = $('.details-table-modal');
		var $modal_body = $modal.find('.modal-body');
		var val = $modal_body.find('.downtime').val();
		var data = { 
					action: 'update_downtime_log',
					machine_downtime_log_ID: $('.modal').data('ID'),
					machine_ID: $modal_body.find('.machines-combo').select2().val(),
					tool_ID: $modal_body.find('.tools-combo').select2().val(),					
					comment: $modal_body.find('.comment').val(),
					reason_ID: $modal_body.find('.reasons-combo').select2().val(),
				};

		// downtime changed
		if (val != $modal_body.find('.downtime').attr('max')) {
			if (!$.isNumeric(val)) {
				$.notify({
					// options
					message: 'Invalid downtime set' 
				},{
					// settings
					element: '.details-table-modal',
					type: 'danger'
				});
			} else if (val < 0 && val >= $modal_body.find('.downtime').attr('max')) {
				$.notify({
					// options
					message: 'New downtime must be less than original downtime' 
				},{
					// settings
					element: '.details-table-modal',
					type: 'danger'
				});
			} else {
				data.push({downtime: $('.modal').find('.downtime').val()});
			}
		}
		
		console.log('Save log data');
		console.log(data);
		$.ajax({
		  method: "POST",
		  url: processor,
		  data: data
		})
		.done(function(jso){
			$('.details-table-modal').modal('hide');
			run_report();
		});
		
	});
	
});

function build_report_parameters() {
	// panel vars
	var $panel = $('.report-parameters').nysus_panel({ 'panel_title': 'Report Parameters' });
	var $panel_body = $panel.find('.panel-body');	
	var $panel_footer = $panel.find('.panel-footer');
	//control vars
	var $run = $('<button class="btn btn-success btn-xs pull-right run-report">Run Report</button>');
	var $start_date = $('<div class="col-md-3" />');
	var $end_date = $('<div class="col-md-3" />');
	var $machines = $('<div class="col-md-4" />');
	var $tools = $('<div class="col-md-4" />');
	var $reasons = $('<div class="col-md-4" />');
	var $group_by = $('<div class="col-md-3" />');
	var $quantify_time = $('<div class="col-md-3" />');
	var $btn_group_quantify_time = $('<div class="btn-group control-buttons btn-group-quantify-time" data-toggle="buttons">');	
	var $minutes = $('<label class="btn btn-sm btn-info active"><input type="radio" id="" name="time-stuff" value="minutes" />Minutes</label>');
	var $percent = $('<label class="btn btn-sm btn-info"><input type="radio" id="" name="time-stuff" value="percent" />%</label>');
	var $hours = $('<label class="btn btn-sm btn-info"><input type="radio" id="" name="time-stuff" value="hours" />Hours</label>');	
	// control option vars
	var start_date_options = {
		label_text: 'Start Date'
		, label_tooltip: 'Select start date'
		, control_id: 'start-date'
		, control_tag: 'input'
	};
	var end_date_options = {
		label_text: 'End Date'
		, label_tooltip: 'Select end date'
		, control_id: 'end-date'
		, control_tag: 'input'
	};
	var machines_options = {
		label_text: 'Machines'
		, label_tooltip: 'Select machine'
		, control_id: 'machines-combo'
		, control_tag: 'select'
	};
	var tools_options = {
		label_text: '<span data-localize="tool.upper">Tool</span>s'
		, label_tooltip: 'Select tool'
		, control_id: 'tools-combo'
		, control_tag: 'select'
	};
	var reasons_options = {
		label_text: 'Reasons'
		, label_tooltip: 'Select reason'
		, control_id: 'reasons-combo'
		, control_tag: 'select'
	};
	var group_by_options = {
		label_text: 'Group By'
		, label_tooltip: 'Select grouping'
		, control_id: 'group-by-combo'
		, control_tag: 'select'
	};
	var button_group_quantify_time_options = {
		label_text: 'Quantify By'
		, label_tooltip: 'Select how to quantify'
		, control_id: 'removeme'
		, control_tag: 'button'
	};
	
	// add options to controls	
	$start_date.nysus_labeled_control(start_date_options);			
	$end_date.nysus_labeled_control(end_date_options);
	$machines.nysus_labeled_control(machines_options);
	$tools.nysus_labeled_control(tools_options);
	$reasons.nysus_labeled_control(reasons_options);
	$group_by.nysus_labeled_control(group_by_options);	
	$quantify_time.nysus_labeled_control(button_group_quantify_time_options);
	// btn group
	$quantify_time.find('.control-body').empty();
	$minutes.appendTo($btn_group_quantify_time);
	$percent.appendTo($btn_group_quantify_time);
	$hours.appendTo($btn_group_quantify_time);
	$btn_group_quantify_time.appendTo($quantify_time);
	// end btn group
	$start_date.find('#start-date').nysus_date_input();
	$end_date.find('#end-date').nysus_date_input();
	
	// additional classes
	$panel_footer.addClass('clearfix');	
	
	// add controls to panel
	var $row = $panel_body.append('<div class="row" />');
	$start_date.appendTo($row);
	$end_date.appendTo($row);
	$group_by.appendTo($row);
	$row.appendTo($panel_body);	
	$row = $panel_body.append('<div class="row" />');
	$machines.appendTo($row);
	$tools.appendTo($row);
	$reasons.appendTo($row);
	$row.appendTo($panel_body);	
	$row = $panel_body.append('<div class="row" />');
	$quantify_time.appendTo($row);
	$run.appendTo($panel_footer);
	
	// additional attributes
	$('#start-date').attr('type', 'date');
	$('#end-date').attr('type', 'date');	
	
	//databinding	
	build_combo($('#machines-combo'), {action: 'get_machines'}, [{key: 'multiple', value: 'multiple'}]);
	build_combo($('#tools-combo'), {action: 'get_tools'}, [{key: 'multiple', value: 'multiple'}]);
	build_combo($('#reasons-combo'), {action: 'get_reasons_description_as_id'}, [{key: 'multiple', value: 'multiple'}]);
	
	var group_options = [];
	group_options.push({id:'date', text:'Date'});
	group_options.push({id:'machine', text:'Machine'});
	group_options.push({id:'tool', text:'<span data-localize="tool.upper">Tool</span>'});
	var $combo = $('#group-by-combo');
	$combo.attr('multiple', 'multiple');
	$combo.select2({data:group_options});
	
	// events
	$run.on('click', run_report);

}

function build_combo(combo, data, attr, selection) {	
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {		
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function build_details_table_modal() {
	//console.log($('.details-table-modal').data());
	var $modal = $('.details-table-modal');
	var $modal_body = $modal.find('.modal-body');
	var $datetime = $('<div></div>');	
	var $machine = $('<div></div>');
	var $tool =  $('<div></div>');
	var $reason =  $('<div></div>');
	var $downtime = $('<div></div>');
	var $comment = $('<div></div>');
	var datetime_options = {
		label_text: 'Datetime'
		, label_tooltip: 'Downtime date'
		, control_class: 'datetime'
		, control_tag: 'input'
	};
	var machine_options = {
		label_text: 'Machine'
		, label_tooltip: 'Select machine'
		, control_class: 'machines-combo'
		, control_tag: 'select'
	};
	var tool_options = {
		label_text: '<span data-localize="tool.upper">Tool</span>'
		, label_tooltip: 'Select tool'
		, control_class: 'tools-combo'
		, control_tag: 'select'
	};
	var reason_options = {
		label_text: 'Reasons'
		, label_tooltip: 'Select reason'
		, control_class: 'reasons-combo'
		, control_tag: 'select'
	};
	var downtime_options = {
		label_text: 'Downtime <br /><sub>Editing downtime will spilt it into another downtime entry</sub>'
		, label_tooltip: 'Edit downtime, value must be smaller than current downtime, remaining downtime will spilt it into another downtime entry'
		, control_class: 'downtime'
		, control_tag: 'input'
	};
	var comment_options = {
		label_text: 'Comment'
		, label_tooltip: 'Edit comment'
		, control_class: 'comment'
		, control_tag: 'input'
	};
	$modal_body.empty();
	$datetime.nysus_labeled_control(datetime_options);
	$machine.nysus_labeled_control(machine_options);
	$tool.nysus_labeled_control(tool_options);
	$reason.nysus_labeled_control(reason_options);
	$downtime.nysus_labeled_control(downtime_options);
	$comment.nysus_labeled_control(comment_options);	
	
	$datetime.appendTo($modal_body);
	$machine.appendTo($modal_body);
	$tool.appendTo($modal_body);
	$reason.appendTo($modal_body);
	$downtime.appendTo($modal_body);
	$comment.appendTo($modal_body);
	
	//console.log($modal.data());
	$modal_body.find('.downtime').val($modal.data().downtime);
	$modal_body.find('.comments').val($modal.data().comment);
	$modal_body.find('.datetime').val($modal.data().datetime);
	build_combo($modal_body.find('.machines-combo'), {action: 'get_machines'}, {}, $modal.data().machine_ID);
	build_combo($modal_body.find('.tools-combo'), {action: 'get_tools'}, {}, $modal.data().tool_ID);
	build_combo($modal_body.find('.reasons-combo'), {action: 'get_reasons_by_machine_type_ID', machine_type_ID: $modal.data().machine_type_ID}, {}, $modal.data().reason_ID);
	
	// attr
	$modal_body.find('.datetime').attr('readonly', 'readonly');
	$modal_body.find('.downtime').attr('max', $modal.data().downtime);
	
}

/*
function build_report_results() {
	var $panel = $('.report-results').nysus_panel({ 'panel_title': 'Report Results', 'show_footer': false });
	var $body = $panel.find('.panel-body');
	var $chart = $('<div class="downtime-chart col-lg-8" />');
	var $pieChart = $('<div class="downtime-pie col-lg-4" />');
	var $row = $('<div class="row" />');
	$body.empty();
	$row.appendTo($body);
	$chart.appendTo($row);
	$pieChart.appendTo($row);
}*/

function build_grid_view() {
	var $grid_view = $('.grid-view').nysus_panel({ 'panel_title': 'Grid View', 'show_footer': false });
	var $table = $('<table class="table table-condensed table-bordered table-striped downtime-table"></table>');
	$table.appendTo($grid_view.find('.panel-body'));
}

function run_report() {
	console.log('running report');
	var sel_machines = $('#machines-combo').select2().val();
	var sel_tools = $('#tools-combo').select2().val();
	var group_by = $('#group-by-combo').select2().val();
	var sel_reasons = $('#reasons-combo').select2().val();	
	
	var quantify = $('.btn-group-quantify-time label.active input').val();
	var data = { 
		action: 'run_report', 
		from_date: $('#start-date').val(), 
		to_date: $('#end-date').val(),
		quantify: quantify
	}	
	if (sel_machines != null) {
		data.sel_machines = sel_machines;		
	}
	if (sel_tools != null) {
		data.sel_tools = sel_tools;		
	}
	if (sel_reasons != null) {
		data.sel_reasons = sel_reasons;		
	}
	if (group_by != null) {
		data.group_by = group_by;
	}

	$('.report-results').find('.panel-body').html('Loading...');		
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(after_run_report);
}

function after_run_report(jso) {	
	if (jso) {		
		populate_grid_view(jso);
	} else {
		$('.downtime-table').empty();		
	}
}

function populate_grid_view(jso) {
	console.log('populate table');
	
	if (downtimeTable != null) {
        downtimeTable.destroy();    //so the thing will update
        $('.downtime-table').empty();
    }
	
	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	$.each(objs, function(k,v) {
		cols.push({data: v, title: v.toProperCase()});
	});
	
	downtimeTable = $('.downtime-table').DataTable({
        bInfo:false,
        bAutoWidth:true,
        autoWidth:true,
        retrieve:true,     
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"
            ]
        },                        
        data: jso,        
        columns: cols

    });
	
	//show detail on cycleTimesTable click
	$('.downtime-table tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = downtimeTable.row(tr);
 
		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();			
		}
		else {
			tr.addClass('shown');
			populate_details(row);			
		}
	});	
}

function populate_details(row) {
	console.log('populating details');
	var $childContent = $('<div class="panel panel-primary"><div class="well"><table class="table-condensed table-bordered table-striped details-table"><tbody><tr><td>Loading...</td></tr></tbody></table></div></div>');
	row.child($childContent).show();
	
	var sel_machines = $('#machines-combo').select2().val();
	var sel_tools = $('#tools-combo').select2().val();
	var sel_reasons = $('#reasons-combo').select2().val();
	var group_by = $('#group-by-combo').select2().val();
	var row_data = row.data();
	//console.log(row_data);
	
	var from_date = $('#start-date').val();
	var to_date = $('#end-date').val();
	if (row_data['date'] != null) {
		from_date = row_data['date'];
		to_date = row_data['date'];
	}
	
	var data = { 
		action: 'drilldown', 
		from_date: from_date,
		to_date: to_date
	}
	if (sel_machines != null) {
		data.sel_machines = sel_machines;		
	}
	if (sel_tools != null) {
		data.sel_tools = sel_tools;		
	}
	if (sel_reasons != null) {
		data.sel_reasons = sel_reasons;		
	}
	if (group_by != null) {
		data.group_by = group_by;		
	}
	
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(after_populate_details);
	
}

function after_populate_details(jso) {
	console.log('Details jso');
	console.log(jso);
	if (detailsTable != null) {
        detailsTable.destroy();    //so the thing will update
        $('.details-table').empty();
    }
	
	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	
	$.each(objs, function(k,v) {
		var title;
		if ( v == 'ID' || v == 'machine_type_ID' || v == 'reason_ID' || v == 'machine_ID' || v == 'tool_ID') {
			title = v;
			visible = false;
			//cols.push({ data: v, title: v, visible: false});
		} else if (v == 'tool') {
			title = '<span data-localize="tool.upper">Tool</span>';
			visible = true;
		} else {
			title = v.toProperCase();
			visible = true;
			//cols.push({ data: v, title: v.toProperCase()});
		}

		cols.push({data: v, title: title, visible: visible});
	});
	
	detailsTable = $('.details-table').DataTable({
        "bInfo":false,
        "bAutoWidth":false,
        autoWidth:false,
        retrieve: true,     
        dom: 'T<"clear">lfrtip',
		sRowSelect: "os",
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"
				
            ]
        },                        
        data: jso,        
        columns: cols
	
    });	
	
	//show detail on cycleTimesTable click
	$('.details-table tbody').on('click', 'tr', function () {		
		$('.details-table-modal').data(detailsTable.row(this).data());
		$('.details-table-modal').modal('show');		
	});	
}
