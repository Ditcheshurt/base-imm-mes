
/*
 * Editor client script for DB table Operators
 * Created by http://editor.datatables.net/generator
 */

var operatorManagementConfigOptions;

(function($){    

$(document).ready(function() {
    
    // Load operator management configuration options.
    $.get('./config/app_config.json', function (data) {
	    operatorManagementConfigOptions = data.operator_management_options;
	});
	
    var editor = new $.fn.dataTable.Editor( {
		ajax: './ajax_processors/manage/table.Operators.php',
		table: '#Operators',        
        //"idSrc": "ID",
		fields: [
            {
                "name": "ID",
                "type": "hidden"    
            },
            {
                "name": "name",
                "type": "hidden"    
            },
			{
				"label": "Last Name",
				"name": "last_name",
                "attr":  {
                            "maxlength": 50,
                            "placeholder": 'Last Name'
                         }
			},
            {
				"label": "First Name",
				"name": "first_name",
                "attr":  {
                            "maxlength": 50,
                            "placeholder": 'First Name'
                         }
			},		
			
            {
				"label": "Badge ID",
				"name": "badge_ID",
                "attr":  {
                            "placeholder": 'Badge ID'
                         }
			},
			{
				"label": "Email",
				"name": "email",
                "attr":  {
                            "maxlength": 100,
                            "placeholder": 'Email'
                         }
			},
			{
				"label": "Inactive Date",
				"name": "deactive_date",
				"type": "datetime",
                "attr":  { "placeholder": 'Inactive Date' }
			},
			{
				"label": "Username",
				"name": "domain_user",
                "attr":  {
                            "maxlength"  : 50,
                            "placeholder": 'Username'
                         }
			},
			{
				"label": "Password",
				"name": "password",
				"type": "password"
			},
           
            {
                "label": "OJT Group(s):",
                "name": "ojt_groups[].ID",
                "type": "select2",                                                      
                "opts": {
                            "placeholder":     "Select Group(s)",
                            "allowClear":      true,
                            "multiple":        true//,                            
                            //"tags":            true,
                            //"tokenSeparators": [',', ' ']
                        }
            }, 
            {
                "label": "Role(s):",
                "name": "roles[].ID",
                "type": "checkbox"
            }
		]/*,
        formOptions: {
            bubble: {
                title: 'Edit',
                buttons: true
            }
        }*/
	} ); 

    editor.on('open', function() {
        console.log('restricting role selection');
        var selector = '.'+ 'DTE_Field_Name_roles[].ID'.replace( /(:|\.|\[|\]|,|=)/g, "\\$1" );
        var container = $(selector);
        var checkboxes = container.find('input[type=checkbox]');
		var order = [3,1,4,2,6];

        // disable all checkboxes
        checkboxes.prop('disabled', true);

        // enable if login has role
        $.map(checkboxes, function (sel) {
            var has_role = $.grep(LOGIN.operator_roles, function(v) {
                return v.role == $(sel).closest('div').find('label').text();
            });
            if (has_role.length > 0) {
                $(sel).prop('disabled', false);
            }
        });
		
		for(var i = order.length-1; i >= 0; i--) {
			var cb = $(selector + ' .DTE_Field_InputControl > div > div > input[value=' + order[i] + ']');
			if (cb) {
				$(cb).parent().prependTo($(selector + ' .DTE_Field_InputControl > div'));
			}
		}		

    }); 
    
    $('#Operators').on( 'click', 'tbody td:not(:first-child)', function (e) {        
        editor.bubble( this );
    } );    
    
	var table = $('#Operators').DataTable( {
		ajax: './ajax_processors/manage/table.Operators.php',
        fixedHeader: true,
        colReorder: true,
        select: "single",
        lengthChange: true,
        order: [[2, 'asc']],
		columns: [
            {
                data:           null,
                defaultContent: '',
                className:      'select-checkbox',
                orderable:      false
            },
            {
                data:           null,
                defaultContent: '',
                className:      'actions',
                orderable:      false,    
                visible:        false,            
                render: function ( data, type, full, meta ) {
                     
                     var buttons;
                     var hasActions = false;
                     
                     // Print operator badge action.
                     if (operatorManagementConfigOptions.print_operator_badge)
                     {
                        hasActions = true;
                        buttons    = '<div><button class="btn btn-xs btn-success printOperatorBadge" aria-label="Left Align" title="Print Operator Badge" data-operatorId="' + data['ID'] + '"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></button></div>'; 
                     }  
                     
                     // Toggle visibility if we don't have any actions available.
                     table.columns( '.actions' ).visible(hasActions);
                     
                     return buttons;                                     
                 }
            },            
            {
				"data": "last_name"
			},
			{
				"data": "first_name"
			}, 
            {
				"data": "badge_ID"
			},
			{
				"data":    "deactive_date",
                className: "dt-body-center",
                render: function ( data, type, row ) { 
                    var isActive = row['deactive_date'] == null;
                    
                    return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;                   
                }                
			},           
            { data: "ojt_groups", render: "[, ].group_description" },
            { data: "roles", render: "[, ].name" }
		]
	} );

	new $.fn.dataTable.Buttons( table, [
		{ extend: "create", editor: editor },
		{ extend: "edit",   editor: editor }
	] );

	table.buttons().container()
		.appendTo( $('.col-sm-6:eq(0)', table.table().container() ) );   
        
    // Print operator badge
    $('#Operators').on( 'click', 'button.printOperatorBadge', function (e) {
        e.preventDefault();
        alert($(this)[0]['dataset']['operatorid']);        
    } );    
    
} );

}(jQuery));