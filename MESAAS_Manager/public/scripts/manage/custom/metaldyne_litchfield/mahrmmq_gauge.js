$(document).ready(function() {
	var file_upload_path = "c:\\mmqupload\\";
	var file_upload_prefix;
	var files_uploaded = [];
	var passed;
	
	$('#upload_container').hide();
	
	$('#inp_serial_number').on('change', function(e) {
		e.preventDefault();
		
		var serial_number = $(this).val();
		var field_names = ['ID', 'serial_number', 'insert_time'];
		var view = $('#part_view');
		
		view.empty();		
		
		NYSUS.REPETITIVE.MACHINES.get_machine_cycle_parts_by_serial(serial_number, field_names, function(jso) {
			if(jso) {
				file_upload_prefix = serial_number + '_';
				
				var table = $('<table id="table_parts" class="table table-striped table-condensed"><thead><tr><th>Select</th><th>Machine</th><th>Tool</th><th>Serial</th><th>Created</th><tr></thead></table>');
				var tbody = $('<tbody />');
				
				$.each(jso, function(k,v) {
					var tr = $('<tr data-machine-cycle-part-id="' + v.ID + '"></tr>');
					tr.append('<td><input type="radio" name="part" /></td>');
					tr.append('<td>' + v.tool[0].machine[0].machine_name + '</td>');
					tr.append('<td>' + v.tool[0].short_description + '</td>');
					tr.append('<td>' + v.serial_number + '</td>');
					tr.append('<td>' + v.insert_time.date.slice(0,19) + '</td>');
					tr.appendTo(tbody);
				});
				tbody.appendTo(table);
				table.appendTo(view);
				
				$('input[name=part]:first').prop('checked', true);
				
			} else {
				file_upload_prefix = null;
				view.append('<h3>No Serial Number Found</h3>');
			}
			
		});
		
		if ($('#inp_serial_number').val().length > 0) {
			$('#upload_container').show();
			$('#upload-file').show();
			$('#uploader').hide();
		} else {
			$('#upload_container').hide();
			$('#uploader').hide();
		}
		
		return false;
		
	});
	
	$('#inp_operator_badge').on('change', function(e) {
		e.preventDefault();
		var inp = $(this);
		var badge_id = inp.val();
		var view = $('#operator_view');
		
		view.empty();
		
		NYSUS.COMMON.OPERATORS.get_operator_by_badge(badge_id, function(jso) {
			if(jso) {
				$.each(jso, function(k,v) {
					view.append('<span>Badge: ' + v.badge_ID + '</span><br/>');
					view.append('<span>Name: ' + v.name + '</span><br/>');
					view.append('<span>Email: <a href="mailto:' + v.email + '" tabindex=99>' + v.email + '</a></span><br/>');
					view.attr('data-operator-id', v.ID);
				});			
				
			}
			
		});
		return false;
		
	});
	
	$('#upload-file').on('click', function(e) {
		e.preventDefault();
		$('#upload-file').hide();		
		$('#uploader').show();
		init_upload();
		return false;
	});
	
	$('.btn-passed').on('click', function(e) {
		e.preventDefault();
		passed = 1;
		insert_record();
		return false;
	});
	
	$('.btn-failed').on('click', function(e) {
		e.preventDefault();
		passed = 0;
		insert_record();
		return false;
	});
	
	function insert_record() {
		var field_names = [
		                   '[machine_cycle_part_ID]'
		         	      ,'[operator_ID]'
		        	      ,'[test_type]'
		        	      ,'[file_path]'
		        	      ,'[passed]'
		        	      ];
		var field_values = [];
		
		var machine_cycle_part_ID = $('input[name=part]:checked').closest('tr').data('machineCyclePartId');
		var operator_id = $('#operator_view').data('operatorId');
		var test_type = "mahrmmq";
		
		if (validate_input()) {
			
			field_values.push(machine_cycle_part_ID, operator_id, test_type, files_uploaded, passed);
			
			NYSUS.REPETITIVE.GAUGES.insert_gauge_machine_cycle_parts_extra_data(field_names, field_values, function(jso) {
				if(jso.success == 1) {
					$('.btn-passed').attr('disabled', 'disabled');
					$('.btn-failed').attr('disabled', 'disabled');
					
					$.each(files_uploaded, function(k,v) {
						
						NYSUS.REPETITIVE.GAUGES.parse_mahrmmq(machine_cycle_part_ID, v, function(jso) {
							if(jso.success == 1) {
								$.growl(
										{"title": " SUCCESS: ","message": "RECORD SAVED!!","icon": "glyphicon glyphicon-exclamation-sign"},
										{"type": "success","allow_dismiss": false,"placement": {"from": "top","align": "right"},"offset": {"x": 10,"y": 80},delay: 10e3}
								 );	
							}
						});
					});									
					
//					setTimeout(function() {
//						location.reload();
//					}, 2000);
					
				} else {
					var message = "ERROR SAVING RECORD!!<br />" + jso.error;
					$.growl(							
							{"title": " ERROR: ","message": message,"icon": "glyphicon glyphicon-exclamation-sign"},
							{"type": "danger","allow_dismiss": false,"placement": {"from": "top","align": "right"},"offset": {"x": 10,"y": 80},delay: 10e3}
					 );	
				}
				
			});
			show_upload_detail(machine_cycle_part_ID);
			
		}	
		
	}
	
	function show_upload_detail(machine_cycle_part_ID) {
		var container = $('#upload_detail');
		var table = $('<table class="table table-condensed table-striped"><thead><tr><td>Name</td><td>Datum</td><td>Nominal</td><td>L Tol</td><td>U Tol</td><td>Value</td><td>Unit</td><td>Feature</td><td>Low Nat Border</td><td>Type</td><td>No</td><td>Date</td><td>Time</td><td>Operator</td></tr></thead></table>');
		var tbody = $('<tbody />');
		
		NYSUS.REPETITIVE.GAUGES.get_marhmmq_detail(machine_cycle_part_ID, function(jso) {
			if (jso) {
				$.each(jso, function(k,v) {
					var tr = $('<tr />');
					tr.append('<td>' + v.name + '</td>');
					tr.append('<td>' + v.datum + '</td>');
					tr.append('<td>' + v.nominal + '</td>');
					tr.append('<td>' + v.l_tol + '</td>');
					tr.append('<td>' + v.u_tol + '</td>');
					tr.append('<td>' + v.value + '</td>');
					tr.append('<td>&#' + v.unit + ';</td>');
					tr.append('<td>' + v.feature + '</td>');
					tr.append('<td>' + v.low_nat_border + '</td>');
					tr.append('<td>' + v.type + '</td>');
					tr.append('<td>' + v.no + '</td>');
					tr.append('<td>' + v.date + '</td>');
					tr.append('<td>' + v.time + '</td>');
					tr.append('<td>' + v.operator + '</td>');
					tbody.append(tr);
				});
				table.append(tbody);
				container.empty().append(table);
			} else {
				container.empty().append('<h2>THERE WAS AN ERROR UPLOADING TEST FILE! No Data Exists');
			}
			
		});		
		
	}
	
	function validate_input() {
		var machine_cycle_part_ID = $('input[name=part]:checked').closest('tr').data('machineCyclePartId');
		var operator_ID = $('#operator_view').data('operatorId');
		var growl_style = {"type": "danger","allow_dismiss": false,"placement": {"from": "top","align": "right"},"offset": {"x": 10,"y": 80},delay: 10e3};
		
		if(operator_ID == undefined || operator_ID == null) {
			$.growl(
					 {"title": " DANGER: ","message": "NO OPERATOR SELECTED","icon": "glyphicon glyphicon-exclamation-sign"},
					 growl_style
			 );
			return false;
		}
		if(machine_cycle_part_ID == undefined || machine_cycle_part_ID == null) {
			$.growl(
					 {"title": " DANGER: ","message": "NO PART SELECTED","icon": "glyphicon glyphicon-exclamation-sign"},
					 growl_style
			 );
			return false;
		}
		if(files_uploaded.length <= 0) {
			$.growl(
					 {"title": " DANGER: ","message": "NO FILE SELECTED","icon": "glyphicon glyphicon-exclamation-sign"},
					 growl_style
			 );
			return false;
		}
		
		return true;
	}
	
	function init_upload() {		
		
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'pickfiles', // you can pass in id...
			container : document.getElementById('upload_container'), // ... or DOM Element itself
			url : './ajax_processors/upload.php',
			flash_swf_url : '../js/Moxie.swf',
			silverlight_xap_url : '../js/Moxie.xap',		
			filters : {
				max_file_size : '1mb',
				mime_types: [
					{title : "Text files", extensions : "txt"}
				]
			},
			max_file_count: 1,
			multipart_params : {
		        "file_prefix" : file_upload_prefix,
		        "file_path" : file_upload_path
		    },
			init: {
				PostInit: function() {
					document.getElementById('filelist').innerHTML = '';
	
					document.getElementById('uploadfiles').onclick = function() {
						uploader.start();
						return false;
					};
				},
				
				FilesAdded: function(up, files) {
					files_uploaded = [];
					plupload.each(files, function(file) {
						document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
						files_uploaded.push(file_upload_path + file_upload_prefix + file.name);
						uploader.start();
					});
				},
	
				UploadProgress: function(up, file) {
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
				},
	
				Error: function(up, err) {
					document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
				}
			}
		});
	
		uploader.init();	
	} // end init_uploader
	
}); // end document ready