var processor = './ajax_processors/manage/batch_scheduler.php';

$(document).ready(function () {
	load_cells();
	load_parts();
	load_batches();
	$('.cell_select').on('change keyup', function () {
		populate()
	});
	$('.part_select').on('change keyup', function () {
		load_part_quantity()
	});
	$('.quantity_button').on('click', create_batch);
	$('.multi_qty_button').on('click', create_batch);
	$('.custom_qty_button').on('click', create_custom_batch);

	$('.quantity_button').hide();
	$('.multi_qty_button').hide();
	$('.multi_qty_input').hide();
	$('.custom_qty_button').hide();
	$('.custom_qty_input').hide();
	$('.batch_panel').find('.move_up').hide();
	$('.batch_panel').find('.move_down').hide();
	$('.batch_panel').find('.delete').hide();
});

function populate() {
	load_parts();
	load_batches();
	load_batch_totals();
}

function load_cells() {
	$.getJSON(
		processor, {
		'action' : 'load_cells'
	},
		after_load_cells);
}

function after_load_cells(jso) {
	var data = [];
	$.each(jso, function (key, value) {
		var obj = {
			id : value['ID'],
			text : value['cell_desc']
		};
		data.push(obj);
	});
	$('.cell_select').select2({
		placeholder : 'Select Cell...',
		data : data
	});
}

function load_parts() {
	$('.part_select').empty();
	$.getJSON(
		processor, {
		'action' : 'load_parts',
		'cell_ID' : $('.cell_select option:selected').val()
	},
		after_load_parts);
}

function after_load_parts(jso) {
	var data = [];
	$.each(jso, function (key, value) {
		var obj = {
			id : value['ID'],
			text : value['part_number'] + ': ' + value['part_desc_short']
		};
		data.push(obj);
	});
	$('.part_select').select2({
		placeholder : 'Select Part...',
		data : data
	});
	$('.part_select').select2('val', '');
}

function load_part_quantity() {
	var part_ID = $('.part_select option:selected').val();
	if (part_ID != null) {
		$.getJSON(
			processor,
			{
				'action': 'load_part_quantities',
				'part_ID': part_ID
			},
			after_load_part_quantity
		);
	}
}

function after_load_part_quantity(jso) {
	if (jso.length > 0) {
		$('.quantity_button').find('.badge').html(jso[0].pack_qty);
		$('.quantity_button').attr('pack_qty', jso[0].pack_qty);
		$('.quantity_button').attr('part_ID', jso[0].part_ID);
		$('.multi_qty_button').attr('pack_qty', jso[0].pack_qty);
		$('.multi_qty_button').attr('part_ID', jso[0].part_ID);
		$('.custom_qty_button').attr('part_ID', jso[0].part_ID);
		$('.custom_qty_button').show();
		$('.custom_qty_input').show();
		// no pack_qty so not setup for part in db
		if (jso[0].pack_qty != null) {
			$('.quantity_button').show();
			$('.multi_qty_button').show();
			$('.multi_qty_input').show();
		}
		
	} else {
		$('.quantity_button').hide();
		$('.multi_qty_button').hide();
		$('.multi_qty_input').hide();
		$('.custom_qty_button').hide();
		$('.custom_qty_input').hide();
	}
}

function load_batches() {
	$.getJSON(
		processor, {
		'action' : 'load_batches',
		'cell_ID' : $('.cell_select option:selected').val()
	},
		after_load_batches);
}

function after_load_batches(jso) {
	var elem = $('.batch_panel').first();
	$('.batches_scheduled').empty();
	
	if (jso.length == 0) {
		elem.hide();
		$('#div_no_batches').show();
	} else {
		elem.show();
		$('#div_no_batches').hide();
	}

	for (var i = 0; i < jso.length; i++) {
		if (i == 0) {
			elem.addClass('panel-success');
			elem.find('.order_rank').html('Current Batch - In Process');
			elem.find('.part_number').html(jso[i].part_number);
			elem.find('.part_qty').html(jso[i].pack_qty);
			elem.attr('batch_ID', jso[i].batch_ID);

		} else {
			var clone = elem.clone();

			clone.removeClass('panel-success');
			clone.addClass('panel-info');
			clone.find('.part_number').html(jso[i].part_number);
			clone.find('.part_qty').html(jso[i].pack_qty);

			clone.attr('pack_qty', jso[i].pack_qty);
			clone.attr('batch_ID', jso[i].batch_ID);
			clone.attr('part_ID', jso[i].part_ID);

			if (i == 1) {
				clone.find('.order_rank').html('Next Batch');
				clone.find('.move_up').hide();
				clone.addClass('panel-warning');
			} else {
				clone.find('.order_rank').html('Batch # ' + i);
				var up = clone.find('.move_up');
				up.show();
				up.on('click', move_up);
			}

			if (i + 1 == jso.length) {
				clone.find('.move_down').hide();
			} else {
				var dwn = clone.find('.move_down');
				dwn.on('click', move_down);
				dwn.show();
			}
			clone.find('.delete').show();
			clone.find('.delete').on('click', delete_batch);

			clone.appendTo('.batches_scheduled');
		}
	}	

	load_batch_totals();
}

function load_batch_totals() {
	$.getJSON(
		processor, {
		'action' : 'load_batch_totals',
		'cell_ID' : $('.cell_select option:selected').val()
	},
		after_load_batch_totals);
}

function after_load_batch_totals(jso) {
	$('.batch_totals > tbody').empty();
	$.each(jso, function (k, v) {
		$('.batch_totals > tbody:last').append('<tr><td>' + v.part_number + ': ' + v.part_desc_short + '</td><td>' + v.build_total + '</td></tr>');
	});
}

function create_custom_batch() {
	$.getJSON(
		processor, {
		'action' : 'insert_batch',
		'part_ID' : $(this).attr('part_ID'),
		'pack_qty' : $('.custom_qty_input').val(),
		'cell_ID' : $('.cell_select option:selected').val()
	}, function () {
		show_success_message();
		load_batches();
	});
}

function create_batch() {
	var qty = $(this).attr('pack_qty');

	if ($(this).hasClass('quantity_button') == false && $(this).find('multi_qty_input') != null) {
		qty = $(this).attr('pack_qty') * $('.multi_qty_input').val();
	}

	$.getJSON(
		processor, {
		'action' : 'insert_batch',
		'part_ID' : $(this).attr('part_ID'),
		'pack_qty' : qty,
		'cell_ID' : $('.cell_select option:selected').val()
	}, function () {
		show_success_message();
		load_batches();
	});
}

function move_up() {
	$.getJSON(
		processor, {
		'action' : 'move_up',
		'batch_ID' : $(this).parents('.batch_panel').attr('batch_ID')
	}, function () {
		show_success_message();
		load_batches();
	});
}

function move_down() {
	$.getJSON(
		processor, {
		'action' : 'move_down',
		'batch_ID' : $(this).parents('.batch_panel').attr('batch_ID')
	}, function () {
		show_success_message();
		load_batches();
	});
}

function delete_batch() {
	$.getJSON(
		processor, {
		'action' : 'delete_batch',
		'batch_ID' : $(this).parents('.batch_panel').attr('batch_ID')
	}, function () {
		show_success_message();
		load_batches();
	});
}

function show_success_message() {
	try {
		var m = $('#message_modal');
		var msg = m.find('.message');
		msg.html('Operation Successfull...');
		m.modal('show');
	} catch(err) {

	}
}