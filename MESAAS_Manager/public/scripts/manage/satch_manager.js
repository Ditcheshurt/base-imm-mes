var processor = './ajax_processors/manage/satch_manager.php';
var table;
var systems;
var lines;
var chosen_system;
var chosen_line;

$(document).ready(function () {

	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	$('#line_select').on('change', function (e) {
		var line_ID = $('#line_select option:selected').val();
		var res = $.grep(lines, function (l) {
			return l.line_ID == line_ID;
		});
		chosen_line = res[0];
		//load_data();
	});
	
	$('#btn_run_report').on('click', function() {
		event.preventDefault();
		load_data();
	});
	
	$('#tbl_part_setup').on('click', '.view-in-build', function() {
		$('#div_view_modal H4').html('WIP Parts In-Build');
		var d = $(this).data();
		var b = $('#div_view_modal TABLE TBODY').empty();
		for (var i = 0; i < d.parts.length; i++) {
			var r = $('<tr>').appendTo(b);
			var c = $('<td>').html(d.parts[i].internal_serial).appendTo(r);
			if (d.parts[i].module_ID == null) {
				var c = $('<td>').html('Batch').appendTo(r);
			} else {
				var c = $('<td>').html('Sequenced ').appendTo(r);
				$('<span>').addClass('glyphicon glyphicon-info-sign').attr('title', 'Module ' + d.parts[i].module_ID + ' VIN ' + d.parts[i].VIN).appendTo(c).tooltip();
			}
			var c = $('<td>').html(' ').appendTo(r);
			var c = $('<td>').html('Carrier ' + d.parts[i].carrier_ID).appendTo(r);
		}
		
		$('#div_view_modal').modal('show');
	});
	
	$('#tbl_part_setup').on('click', '.view-built', function() {
		$('#div_view_modal H4').html('WIP Parts Built');
		var d = $(this).data();
		var b = $('#div_view_modal TABLE TBODY').empty();
		for (var i = 0; i < d.parts.length; i++) {
			var r = $('<tr>').appendTo(b);
			var c = $('<td>').html(d.parts[i].internal_serial).appendTo(r);
			if (d.parts[i].module_ID == null) {
				var c = $('<td>').html('Batch').appendTo(r);
			} else {
				var c = $('<td>').html('Sequenced').appendTo(r);
			}
			var c = $('<td>').html(d.parts[i].built_time.date).appendTo(r);
			var g = $('<div>').addClass('btn-group');
			var btn = $('<button>')
				.addClass('btn btn-xs btn-warning btn-mark-wip-part-used')
				.data(d.parts[i])
				.attr('title', 'Mark as Used')
				.html('<span class="glyphicon glyphicon-ok-circle"></span> Mark as Used')
				.appendTo(g);
			var c = $('<td>').append(g).appendTo(r);
		}
		
		$('#div_view_modal').modal('show');
	});
	
	$('#div_view_modal TABLE TBODY').on('click', '.btn-mark-wip-part-used', function() {
		var d = $(this).data();
		$('#div_view_modal').modal('hide');
		$.growl("Marking WIP part as used...", {"type":"info"});
		$.getJSON(processor, {
				"action":"mark_wip_part_used",
				"wip_part_ID":d.ID,
				"line_ID":chosen_line.line_ID,
				"database":chosen_system.database_name
			}, function(data) {
				$.growl("WIP part marked as used!", {"type":"success"});
				updateSatchStatus(data);
			});
	});
	
	$('#tbl_part_setup').on('click', '.edit-buffer-level', function() {
		var d = $(this).data();
		$('#inp_buffer_level').val(d.buffer_level);
		$('#div_edit_buffer_level').modal('show').data(d);
	});
	
	$('BUTTON.btn-save-buffer-level').on('click', function() {
		var d = $('#div_edit_buffer_level').data();
		$('#div_edit_buffer_level').modal('hide');
		$.growl("Saving new buffer level...", {"type":"info"});
		$.getJSON(processor, {
				"action":"save_buffer_level",
				"bom_ID":d.bom_ID,
				"buffer_level":$('#inp_buffer_level').val(),
				"line_ID":chosen_line.line_ID,
				"database":chosen_system.database_name
			}, function(data) {
				$.growl("New buffer level saved!", {"type":"success"});
				updateSatchStatus(data);
			});
	})
});

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id, line_type: 7, only_batch_mode_lines:0}, function(data) {
		lines = data;
		show_lines(data);

		chosen_line = data[0];
		//load_data();
	});
}

function load_data() {
	$('#tbl_part_setup TBODY').html('<tr><td colspan=10>Loading...</td></tr>');
	$.getJSON(processor,	{
			action: 'get_satch_status',
			line_ID: chosen_line.line_ID,
			database: chosen_system.database_name
		},	updateSatchStatus
	);
}

function updateSatchStatus(jso) {
	$('#as_of').html('As of: ' + jso.datetime[0].the_time.date);
	jso.wip_part_buffer_levels = jso.wip_part_buffer_levels || [];
	
	var b = $('#tbl_part_setup TBODY').empty();
	for (var i = 0; i < jso.wip_part_buffer_levels.length; i++) {
		var w = jso.wip_part_buffer_levels[i];
		if (w.equiv_bom_ID == null) {
			var r = $('<tr>').appendTo(b);
			$('<td>').html(w.bom_ID).appendTo(r);
			$('<td>').html(w.bom_desc).appendTo(r);
			var c = $('<td>').html(w.buffer_level).appendTo(r);
			$('<span>').css('margin-left', '3px').css('cursor','pointer').addClass('glyphicon glyphicon-pencil edit-buffer-level').appendTo(c).data(w);
			
			//get main line BOMs
			var arr = [];
			for (var j = 0; j < jso.wip_part_buffer_levels.length; j++) {
				if (w.bom_ID == jso.wip_part_buffer_levels[j].equiv_bom_ID) {
					arr.push(jso.wip_part_buffer_levels[j].bom_ID);
				}
			}
			var c = $('<td>').html(arr.length).appendTo(r);
			if (arr.length > 0) {
				$('<span>').css('margin-left', '3px').addClass('glyphicon glyphicon-eye-open').attr('title', arr.join(', ')).tooltip({"html":true}).appendTo(c);			
			}
			
			//get num in-build
			arr = [];
			for (var j = 0; j < jso.in_progress_parts.length; j++) {
				if (w.bom_ID == jso.in_progress_parts[j].bom_ID) {
					arr.push(jso.in_progress_parts[j]);
				}
			}
			var c = $('<td>').html(arr.length).appendTo(r);
			if (arr.length > 0) {
				$('<span>').css('margin-left', '3px').css('cursor','pointer').addClass('glyphicon glyphicon-eye-open view-in-build').appendTo(c).data('parts', arr);
			}
			
			//get num built
			arr = [];
			for (var j = 0; j < jso.built_unused_parts.length; j++) {
				if (w.bom_ID == jso.built_unused_parts[j].bom_ID) {
					arr.push(jso.built_unused_parts[j]);
				}
			}
			var c = $('<td>').html(arr.length).appendTo(r);
			if (arr.length > 0) {
				$('<span>').css('margin-left', '3px').css('cursor','pointer').addClass('glyphicon glyphicon-eye-open view-built').appendTo(c).data('parts', arr);
			}
		}
	}
}