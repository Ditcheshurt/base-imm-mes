var processor = './ajax_processors/manage/machine_production_dashboard.php';

var machine_group_accordion;
var edit_slide_modal;
var remove_slide_modal;
var slide_image_form;
var current_machine_dashboard_group_ID;

$(document).ready(function () {
	machine_group_accordion = $('#machine_group_accordion');
	edit_slide_modal = $('#edit_slide_modal');
	remove_slide_modal = $('#remove_slide_modal');
	slide_image_form = $('#slide_image_form');

	// ignore enter
	$(document).on('keyup keypress', function (e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			e.preventDefault();
			return false;
		}
	});

	init_listeners();
	init_image_upload();
	load_machine_groups(show_machine_groups);
});

function init_listeners () {
	machine_group_accordion.on('click', '.btn-add-slide', function (e) {
		var panel = $(e.target).closest('.panel');
		reset_slide_modal();
		edit_slide_modal.find('.modal-title').html('Add Slide');
		edit_slide_modal.attr('data-action', 'insert_slide');
		current_machine_dashboard_group_ID = panel.attr('data-machine-dashboard-group-id');
		edit_slide_modal.modal('show').data(null);
	});

	machine_group_accordion.on('click', '.slide-table .btn-edit-slide', function (e) {
		var slide_ID = $(e.target).closest('tr').attr('data-slide-id');
		current_machine_dashboard_group_ID = $(e.target).closest('.panel').attr('data-machine-dashboard-group-id');
		load_slide(slide_ID, show_slide_modal);
	});

	edit_slide_modal.find('.btn-confirm-edit').on('click', function (e) {
		var slide_data = {
			slide_ID: edit_slide_modal.attr('data-slide-id'),
			machine_dashboard_group_ID: current_machine_dashboard_group_ID
		};

		edit_slide_modal.find('input, textarea, select').each(function (i, field) {
			var f = $(field);
			var name = f.prop('name');

			if (name != 'ID') {
				if (f.prop('type') == 'checkbox') {
					slide_data[name] = f.prop('checked') ? 1 : 0;
				} else {
					slide_data[name] = f.val();
				}
			}
		});

		delegate_slide_edit(slide_data);
	});

	machine_group_accordion.on('click', '.slide-table .btn-remove-slide', function (e) {
		var slide_ID = $(e.target).closest('tr').attr('data-slide-id');
		remove_slide_modal.attr('data-action', 'delete_slide')
								 .attr('data-slide-id', slide_ID)
								 .modal('show').data(null);
	});
 
	remove_slide_modal.find('.btn-confirm-remove').on('click', function (e) {
		var slide_ID = remove_slide_modal.attr('data-slide-id');
		delete_slide(slide_ID);
	});
}

function load_slides (machine_dashboard_group_ID, callback) {
	var params = {action: 'get_slides', machine_dashboard_group_ID: machine_dashboard_group_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_slides (machine_dashboard_group_ID, data) {
	var machine_group_panel = $('.panel[data-machine-dashboard-group-id="'+machine_dashboard_group_ID+'"]');
	var panel_body = $('.panel-body', machine_group_panel);
	panel_body.empty();

	if (data) {
		var table_container = $('<div></div>').addClass('table-responsive').appendTo(panel_body);
		var slide_table = $('<table></table>').addClass('table table-condensed table-striped table-hover slide-table')
											  .appendTo(table_container);
		var thead = $('<thead></thead>').appendTo(slide_table);
		var tbody = $('<tbody></tbody>').appendTo(slide_table);

		var trh = $('<tr></tr>').appendTo(thead);
		var cols = ['Order', 'Image', 'Duration', 'Active', 'Actions'];
		cols.forEach(function (col) {
			$('<th></th>').text(col).appendTo(trh);
		});

		data.forEach(function (slide) {
			var tr = $('<tr></tr>').prop('id', 'slide_'+slide.ID)
								   .attr('data-slide-id', slide.ID)
								   .append($('<td>'+slide.slide_order+'</td>'))
								   .append($('<td><img src="'+slide.image_src+'" class="table-image-thumbnail" width="150"></td>'))
								   .append($('<td>'+slide.duration+'</td>'))
								   .append($('<td>'+(slide.active ? 'Yes' : 'No')+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-slide"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-slide"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		});
	}
}



function load_machine_groups (callback) {
	var params = {action: 'get_machine_dashboard_groups'};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine_groups (data) {
	show_machine_group({ID: 0, name: 'Common'});

	if (data) {
		data.forEach(show_machine_group);

		var c = $('.collapse').collapse();

		c.on('shown.bs.collapse', function (e) {
			var panel = $(e.target).closest('.panel');
			var title = panel.find('.panel-title');
			
			$('.glyphicon-chevron-down', title).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
			$('.btn-add-slide', panel).show();
		});

		c.on('hidden.bs.collapse', function (e) {
			var panel = $(e.target).closest('.panel');
			var title = panel.find('.panel-title');

			$('.glyphicon-chevron-up', title).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
			$('.btn-add-slide', panel).hide();
		});
	}
}

function show_machine_group (group) {
	var panel = $('<div></div>').addClass('panel panel-default')
								.attr({'data-machine-dashboard-group-id': group.ID})
							    .appendTo(machine_group_accordion);
	var panel_heading = $('<div></div>').addClass('panel-heading clearfix')
										.prop({id: 'heading_'+group.ID})
										.attr({role: 'tab'})
										.appendTo(panel);
	var panel_title = $('<h4></h4>').addClass('panel-title pull-left')
									.appendTo(panel_heading);
	var btn = $('<a></a>').prop({
		href: '#collapse_'+group.ID
	}).attr({
		role: 'button',
		'aria-expanded': false,
		'aria-controls': 'collapse_'+group.ID,
		'data-toggle': 'collapse',
		'data-parent': '#machine_group_accordion'
	}).appendTo(panel_title);

	var icon = $('<span></span>').addClass('glyphicon glyphicon-chevron-down').appendTo(btn);
	btn.append(' '+group.name);

	var add_btn = $('<div></div>').addClass('btn btn-info btn-xs btn-add-slide pull-right')
								  .attr({'data-action': 'insert_slide'})
								  .appendTo(panel_heading)
								  .hide();
	$('<span></span>').addClass('glyphicon glyphicon-plus').appendTo(add_btn);
	add_btn.append(' Add Slide');

	var panel_collapse = $('<div></div>').addClass('panel-collapse collapse in')
										 .prop({
										 	id: 'collapse_'+group.ID
										 }).attr({
										 	role: 'tabpanel', 
										 	'aria-labelledby': 'heading_'+group.ID
										 }).appendTo(panel);
	var panel_body = $('<div></div>').addClass('panel-body')
									 .appendTo(panel_collapse);
	
	load_slides(group.ID, (function (id) {
		return function (d) {
			show_slides(id, d);
		};
	})(group.ID));
}

function load_slide (slide_ID, callback) {
	var params = {action: 'get_slide', slide_ID: slide_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_slide_modal (data) {
	reset_slide_modal();
	//current_machine_dashboard_group_ID = edit_slide_modal.closest('.panel').attr('data-machine-dashboard-group-id');
	var slide = data[0];
	edit_slide_modal.find('input, textarea, select').each(function (i, input) {
		var field = $(input);
		if (field.prop('type') == 'checkbox') {
			field.prop('checked', slide[field.prop('name')]);
		} else {
			field.val(slide[field.prop('name')]);	
		}
	});

	edit_slide_modal.find('.modal-title').html('Edit Slide');
	edit_slide_modal.attr('data-slide-id', slide.ID);
	edit_slide_modal.find('.upload-form').attr('data-slide-id', slide.ID);
	edit_slide_modal.attr('data-action', 'update_slide');

	if (slide.image_src) {
		edit_slide_modal.find('.image-preview img').attr('src', slide.image_src);
		var image_preview = edit_slide_modal.find('.image-preview');
		var img = image_preview.find('img').clone().attr('src', slide.image_src);
		image_preview.html(img);
		show_preview();
	}

	edit_slide_modal.find('image-preview').show();

	edit_slide_modal.modal('show').data(null);
}

function reset_slide_modal () {
	//current_machine_dashboard_group_ID = null;
	//edit_slide_modal.attr('data-machine-dashboard-group-id', null);
	edit_slide_modal.find('input[type="text"], textarea').val('');
	edit_slide_modal.find('select').val(-1);
	edit_slide_modal.find('input[type="checkbox"]').prop('checked', false);
	clear_upload_field();
	clear_preview();
}

function delegate_slide_edit (slide_data) {
	var action = edit_slide_modal.attr('data-action');
	if (action == 'insert_slide') {
		insert_slide(slide_data);
	} else if (action == 'update_slide') {
		update_slide(slide_data);
	}
}

function delete_slide (slide_ID) {
	var req_data = {action: 'delete_slide', slide_ID: slide_ID};
	$.post(processor, req_data).done(function (data) {
		if (data === null) {
			$.growl('Slide removed.', {type: 'success'});
			$('tr[data-slide-id="'+slide_ID+'"]', machine_group_accordion).remove();
		} else {
			$.growl('Unable to remove slide.', {type: 'danger'});
		}
	});
}


function insert_slide (slide_data) {
	var required = [
		'slide_order',
		'duration'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && slide_data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_slide', slide_data: slide_data};

		$.post(processor, req_data).done(function (data) {
			if (data) {
				$('.upload-form').attr('data-slide-id', data[0].ID);

				upload_slide_image(function (data) {
					//load_slides(show_slides);
					load_slides(slide_data.machine_dashboard_group_ID, (function (id) {
						return function (d) {
							show_slides(id, d);
						};
					})(slide_data.machine_dashboard_group_ID));

					if (data) {
						$.growl('Slide added to dashboard.', {type: 'success'});
					} else {
						$.growl('Unable to add slide.', {type: 'danger'});
					}
				});
			} else {
				$.growl('Unable to add slide.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function update_slide (slide_data) {
	var required = [
		'slide_order',
		'duration'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && slide_data[required[i]];
	}

	if (valid) {
		var image_input = slide_image_form.find(':file');
		if (image_input.val() == '') {
			$.growl('Missing required field.', {type: 'danger'});
		} else {
			upload_slide_image(function (data) {
				slide_data.image_src = data.location;
				var req_data = {action: 'update_slide', slide_data: slide_data};
				$.post(processor, req_data).done(function (data) {
					//load_slides(show_slides);
					load_slides(slide_data.machine_dashboard_group_ID, (function (id) {
						return function (d) {
							show_slides(id, d);
						};
					})(slide_data.machine_dashboard_group_ID));

					if (data === null) {
						$.growl('Updated slide.', {type: 'success'});
					} else {
						$.growl('Unable to update slide.', {type: 'danger'});
					}
				});
			});
		}
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}


// image uploads

function init_image_upload () {
	image_form = $('.image-form');
	image_form.attr('data-location', null);
	image_form.on('change', '.btn-file :file', file_changed);
	image_form.on('fileselect', '.btn-file :file',  files_selected);
}

function upload_progress (e) {
	var progress = $('.upload-form').find('.upload-group progress');

	if (e.lengthComputable) {
		progress.attr({value: e.loaded, max: e.total});
	}
}

function upload_complete (data) {
	$('.upload-form').attr('data-location', data.location);

	clear_upload_field();
}

function clear_upload_field () {
	var inputs = $('.upload-form').find('.upload-group').children(':file, :text');

	$.each(inputs, function (key, input) {
		input.replaceWith(input.clone());
		input.val('');
	});
	
	var progress = $('.upload-form').find('.upload-group progress');
	progress.attr('value', 0);
	progress.hide();
}

function show_preview () {
	var preview_img = $('.upload-form').find('.image-preview img');
	var input = $('.upload-form').find('.btn-file :file')[0];

	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			preview_img.attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
	preview_img.closest('.image-preview').show();
}

function clear_preview () {
	var preview_img = $('.upload-form').find('.image-preview img');
	preview_img.attr('src', '#');
	$('.image-preview').hide();
}

function file_changed () {
	var input = $('.upload-form').find('.btn-file :file');
	var file_count = input.get(0).files ? input.get(0).files.length : 1;
	var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [file_count, label]);
	show_preview();
}

function files_selected (e, file_count, label) {
	var input = $(this).parents('.input-group').find(':text');
	var log = file_count > 1 ? file_count + ' files selected' : label;

	if (input.length) {
		input.val(log);
	} else if (log) {
		alert(log);
	}
}

function upload_slide_image (cb) {
	var form = $('.upload-form')[0];
	var form_data = new FormData(form);
	form_data.append('slide_ID', $('.upload-form').attr('data-slide-id'));
	form_data.append('action', 'store_slide_image');
	
	var progress = $(form).find('progress');
	progress.attr('value', 0);
	progress.show();

	$.ajax({
		url: processor,
		type: 'POST',
		xhr: function() {
			var xhr = $.ajaxSettings.xhr();
			if (xhr.upload) {
				xhr.upload.addEventListener(
					'progress',
					function (e) {
						upload_progress(e);
					},
					false
				);
			}
			return xhr;
		},
		success: function (data) {
			upload_complete(data);
			if (cb != undefined) {
				cb(data);
			}
		},
		data: form_data,
		cache: false,
		contentType: false,
		processData: false
	});
}