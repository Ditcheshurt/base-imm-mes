var processor = './ajax_processors/manage/modules.php';
var table;
var systems;
var chosen_system;

var $broadcast_modal;
var $add_component_modal;
var $edit_component_modal;

$(document).ready(function () {

	$broadcast_modal = $('#broadcast_modal');
	$add_component_modal = $('#add_component_modal');
	$edit_component_modal = $('#edit_component_modal');

	// initialize the system dropdown
	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	// load filtering options based on the chosen system
	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// initialize date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	// load module list
	$('.btn-run').on('click', function () {
		load_data();
	});

	$('.btn-reloader').on('click', function () {
		load_data();
	});

	// open the module birth certificate
	$('#module_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=birth_certificate&module_ID='+row.data().ID+'&mrp_company_code='+chosen_system.mrp_company_code);
	});

	//open the module component list
	$('#module_table').on('click', '.btn-broadcast-components', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=module_component_info&module_ID='+row.data().ID);
	});

	$('#module_table').on('click', '.btn-broadcast', function (e) {
		var row = table.row($(this).closest('tr'));
		load_broadcast_data(row.data().ID, show_broadcast_modal);
	});

	// rebuild the chosen module
	$('#module_table').on('click', '.btn-retrigger', function (e) {
		var row = table.row($(this).closest('tr'));
		var m = $('#rebuild_modal');
		m.attr('data-module-id', row.data().ID);
		m.find('.vin').html(row.data().VIN);
		m.find('.sequence').html(row.data().sequence);
		m.modal('show');
	});

	// confirm the rebuild
	$('#rebuild_modal .modal-footer').on('click', '.btn-confirm', function () {
		var m = $('#rebuild_modal');
		//var remove_components = m.find('input[name="remove_components"]').prop('checked') ? 1 : 0;
		//rebuild_module(m.attr('data-module-id'), remove_components, post_rebuild_module);
		rebuild_module(m.attr('data-module-id'), post_rebuild_module);
	});

	// immediately rebuild the chosen module
	$('#module_table').on('click', '.btn-retrigger-immediate', function (e) {
		var row = table.row($(this).closest('tr'));
		var m = $('#rebuild_immediate_modal');
		m.attr('data-module-id', row.data().ID);
		m.find('.vin').html(row.data().VIN);
		m.find('.sequence').html(row.data().sequence);
		m.modal('show');
	});

	// confirm the immediate rebuild
	$('#rebuild_immediate_modal .modal-footer').on('click', '.btn-confirm', function () {
		var m = $('#rebuild_immediate_modal');
		rebuild_module_immediate(m.attr('data-module-id'), post_rebuild_module_immediate);
	});

	// mark the module as manually built
	$('#module_table').on('click', '.btn-manual-build', function (e) {
		var row = table.row($(this).closest('tr'));
		var m = $('#manual_build_modal');
		m.attr('data-module-id', row.data().ID);
		m.find('.vin').html(row.data().VIN);
		m.find('.sequence').html(row.data().sequence);
		m.modal('show');
	});

	// confirm manual build
	$('#manual_build_modal .modal-footer').on('click', '.btn-confirm', function () {
		var m = $('#manual_build_modal');
		manual_build(m.attr('data-module-id'), post_manual_build);
	});

	// edit a component
	$broadcast_modal.on('click', '.btn-edit-component', function () {
		var component_label = $(this).closest('.label-component');
		var component_ID = component_label.attr('data-component-id');
		load_component(component_ID, show_component_modal);
	});

	// confirm component edit
	$edit_component_modal.on('click', '.btn-confirm', function () {
		update_component(component_updated);
	});

	// add a component
	$broadcast_modal.on('click', '.btn-add-component', show_add_component_modal);

	// confirm component addition
	$add_component_modal.on('click', '.btn-confirm', function () {
		add_component(component_added);
	});

	// remove a component
	$broadcast_modal.on('click', '.btn-remove-component', function () {
		var module_ID = $broadcast_modal.attr('data-module-id');
		var component_label = $(this).closest('.label-component');
		var part_number = component_label.attr('data-part-number');
		remove_component(module_ID, part_number, component_removed);
	});

	// reset all component changes
	// $broadcast_modal.on('click', '.btn-reset-component-changes', function () {
	// 	reset_component_changes(component_changes_reset);
	// });
});

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id, line_type: 0}, show_lines);
	load_statuses(show_statuses);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#module_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: processor,
			data: {
				action: 'list_modules',
				start_date: function (d) {return get_date_value('start_date');},
				end_date: function (d) {return get_date_value('end_date');},
				line_id: function (d) {return $('#line_select option:selected').val();},
				status: function (d) {return $('#status_select option:selected').val();}
			},
			dataSrc: '',
		},

		order: [[4, 'desc']],

		columns: [
		{
			data: 'loaded_time',
			title: 'Loaded Time',
			render:  render_date
		},
		{
			data: 'built_time',
			title: 'Built Time',
			render:  render_date
		},
		{data: 'VIN', title: 'VIN'},
		{data: 'sequence', title: 'Sequence'},
		{data: 'build_order', title: 'Build Order'},
        {data: 'pallet_number', title: 'Pallet'},
        {data: 'pallet_position', title: 'Position'},
		{data: 'status_desc', title: 'Status'},
		{
			title: 'Actions',
			className: 'actions',
			orderable: false,
			data: null,
			defaultContent: '',
			render: function (data, type, full, meta) {
				// show action buttons for each module in the list
				var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');

				if (full.status <= 0 && LOGIN.hasRole('editor')) {
					var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-broadcast">Manage Broadcast</button>');
					btns.append(bc_btn);
				}

				if (full.status > 1) {
					var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Birth Certificate</button>');
					btns.append(bc_btn);
				}

				btns.append($('<button type="button" class="btn btn-xs btn-default btn-broadcast-components">View Broadcast</button>'));
				if (LOGIN.hasRole('editor')) {
					btns.append($('<button type="button" class="btn btn-xs btn-danger btn-retrigger">Re-Trigger</button>'));
					btns.append($('<button type="button" class="btn btn-xs btn-danger btn-retrigger-immediate">Immediate Re-Trigger</button>'));
				}
				if (full.status < 2 && LOGIN.hasRole('editor')) {
					btns.append($('<button type="button" class="btn btn-xs btn-danger btn-manual-build">Manual Build</button>'));
				}
				return btns[0].outerHTML;
			}
		},
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function load_broadcast_components(module_ID, callback) {
	$.getJSON(
	processor,
	{
		action: 'list_components',
		module_ID: module_ID
	},
	callback
	);
}

function show_broadcast_components(data) {
	var cols = [
	{data: 'part_number', title: 'Part Number'},
	{data: 'qty', title: 'Quantity'}
	];

	fill_table_by_cols('component_table', cols, data, false);
}


// Reject Module

function rebuild_module (module_ID, callback) {
	$.getJSON(
	processor,
	{
		action: 'rebuild_module',
		module_ID: module_ID
	},
	callback
	);
}

function post_rebuild_module (data) {
	// hide the confirmation modal and tell the user if the retrigger was successful
	var m = $('#rebuild_modal');
	//m.find('input[name="remove_components"]').prop('checked', false);
	m.modal('hide');

	var m = $('#message_modal');
	var msg = m.find('.message');

	if (data[0].success == 1) {
		msg.html('Module moved to hold queue with new build order: '+data[0].new_build_order+'.');
	} else {
		msg.html('Unable to rebuild module!');
	}

	m.modal('show');
}

function rebuild_module_immediate (module_ID, callback) {
	$.getJSON(
	processor,
	{
		action: 'rebuild_module_immediate',
		module_ID: module_ID
	},
	callback
	);
}

function post_rebuild_module_immediate (data) {
	// hide the confirmation modal and tell the user if the retrigger was successful
	var m = $('#rebuild_immediate_modal');
	m.modal('hide');

	var m = $('#message_modal');
	var msg = m.find('.message');

	if (data[0].success == 1) {
		msg.html('Module set to be rebuilt with new build order: '+data[0].new_build_order+'.');
	} else {
		msg.html('Unable to rebuild module!');
	}

	m.modal('show');
}


// Manual Build

function manual_build(module_ID, callback) {
	$.getJSON(
		processor,
		{
			action: 'manual_build',
			module_ID: module_ID
		}
	).done(callback);
}

function post_manual_build(data) {
	var m = $('#manual_build_modal');
	m.modal('hide');

	var m = $('#message_modal');
	var msg = m.find('.message');

	if (data[0].success == 1) {
		msg.html('Module marked as manually built.');
	} else {
		msg.html('Manual build failed!');
	}

	m.modal('show');
}



// Broadcast Editor

function load_broadcast_data (module_ID, cb) {
	$.getJSON(processor, {
		action: 'get_broadcast_data',
		module_ID: module_ID
	}).done(cb);
}

function show_broadcast_modal (data) {
	if (data) {
		populate_broadcast_modal(data);
	}

	$broadcast_modal.modal('show');
}

function populate_broadcast_modal (data) {
	if (data) {
		if (data.module) {
			$('.VIN', $broadcast_modal).text(data.module.VIN);
			$('.sequence', $broadcast_modal).text(data.module.sequence);
			$broadcast_modal.attr('data-module-id', data.module.ID);
		}

		var component_list = $('#div_incoming_broadcasted', $broadcast_modal);
		component_list.empty();

		if (data.components) {
			data.components.forEach(function (component) {
				var attributes = {
					'data-component-id': component.ID,
					'data-part-number': component.part_number,
					'data-qty': component.qty
				};

				//var component_div = $('<div></div>').addClass('col-sm-2').appendTo(component_list);
				var component_span = $('<span></span>').addClass('label label-info label-component tag').attr(attributes).appendTo(component_list);
				//var component_span = $('<span></span>').addClass('btn btn-info btn-xs').prop('disabled', true).appendTo(component_div);
				$('<span></span>').addClass('badge').text(component.qty).css({'margin-right': '5px', 'font-size': '10px'}).appendTo(component_span);
				$('<span></span>').text(component.part_number).css({'margin-right': '5px'}).appendTo(component_span);
				$('<a><i class="glyphicon glyphicon-pencil"></i></a>').addClass('btn-edit-component').css({'margin-right': '5px'}).appendTo(component_span);
				$('<a><i class="glyphicon glyphicon-trash"></i></a>').addClass('btn-remove-component').appendTo(component_span);
			});
		}

		var part_change_list = $('#div_component_changes', $broadcast_modal);
		part_change_list.empty();

		if (data.part_changes) {
			data.part_changes.forEach(function (component) {
				var label_class = 'label label-component tag';
				var icon_class = 'glyphicon';
				if (component.added) {
					label_class += ' label-success';
					icon_class += ' glyphicon-plus';
				} else if (component.deleted) {
					label_class += ' label-danger';
					icon_class += ' glyphicon-minus';
				}

				var component_span = $('<span></span>').addClass(label_class).appendTo(part_change_list);
				//$('<span></span>').addClass('badge').text(component.qty).css({'margin-right': '5px', 'font-size': '10px'}).appendTo(component_span);
				$('<a><i class="'+icon_class+'"></i></a>').css({'margin-right': '5px'}).appendTo(component_span);
				$('<span></span>').text(component.part_number).appendTo(component_span);
			});
		}

		var effective_component_list = $('#div_effective_components', $broadcast_modal);
		effective_component_list.empty();

		if (data.effective_components) {
			data.effective_components.forEach(function (component) {
				var label_class = 'label label-component tag';
				if (component.added) {
					label_class += ' label-primary';
				} else {
					label_class += ' label-info';
				}

				var component_span = $('<span></span>').addClass(label_class).appendTo(effective_component_list);
				//$('<span></span>').addClass('badge').text(component.qty).css({'margin-right': '5px', 'font-size': '10px'}).appendTo(component_span);
				//$('<a><i class="'+icon_class+'"></i></a>').css({'margin-right': '5px'}).appendTo(component_span);
				$('<span></span>').text(component.part_number).appendTo(component_span);
			});
		}
	}
}


// Edit component

function load_component (component_ID, cb) {
	$.getJSON(processor, {
		action: 'get_component',
		component_ID: component_ID
	}).done(cb);
}

function show_component_modal (data) {
	if (data) {
		$('#original_part_number_input', $edit_component_modal).val(data.part_number);
		$('#new_part_number_input', $edit_component_modal).val(data.part_number);
	}

	$edit_component_modal.modal('show');
}

function update_component (cb) {
	var params = {
		action: 'update_component',
		module_ID: $broadcast_modal.attr('data-module-id'),
		original_part_number: $('#original_part_number_input', $edit_component_modal).val(),
		new_part_number: $('#new_part_number_input', $edit_component_modal).val()
	};

	$.post(processor, params).done(cb);
}

function component_updated (data) {
	// reload data
	if (data === null) {
		var module_ID = $broadcast_modal.attr('data-module-id');
		load_broadcast_data(module_ID, populate_broadcast_modal);

		$.notify({
			message: 'Component Updated'
		}, {
			element: '#broadcast_modal',
			type: 'success'
		});
		$edit_component_modal.modal('hide');
	} else {
		$.notify({
			message: 'Component Not Updated'
		}, {
			element: '#broadcast_modal',
			type: 'danger'
		});
	}
}

function remove_component (module_ID, part_number, cb) {
	console.log(part_number);
	$.post(processor, {
		action: 'remove_component',
		module_ID: module_ID,
		part_number: part_number
	}).done(cb);
}

function component_removed (data) {
	// reload data
	if (data === null) {
		var module_ID = $broadcast_modal.attr('data-module-id');
		load_broadcast_data(module_ID, populate_broadcast_modal);

		$.notify({
			message: 'Component Removed'
		}, {
			element: '#broadcast_modal',
			type: 'success'
		});
	} else {
		$.notify({
			message: 'Component Not Removed'
		}, {
			element: '#broadcast_modal',
			type: 'danger'
		});
	}
}

function show_add_component_modal () {
	$('form input', $add_component_modal).val('');
	$add_component_modal.modal('show');
}

function add_component (cb) {
	var module_ID = $broadcast_modal.attr('data-module-id');
	var part_number = $('#part_number_input').val();

	$.post(processor, {
		action: 'add_component',
		module_ID: module_ID,
		part_number: part_number
	}).done(cb);
}

function component_added (data) {
	// reload data
	if (data === null) {
		var module_ID = $broadcast_modal.attr('data-module-id');
		load_broadcast_data(module_ID, populate_broadcast_modal);

		$.notify({
			message: 'Component Added'
		}, {
			element: '#broadcast_modal',
			type: 'success'
		});
		$add_component_modal.modal('hide');
	} else {
		$.notify({
			message: 'Component Not Added'
		}, {
			element: '#broadcast_modal',
			type: 'danger'
		});
	}
}

// function reset_component_changes (cb) {
// 	var module_ID = $broadcast_modal.attr('data-module-id');

// 	$.post(processor, {
// 		action: 'reset_component_changes',
// 		module_ID: module_ID
// 	}).done(cb);
// }

// function component_changes_reset (data) {
// 	// reload data
// 	if (data === null) {
// 		var module_ID = $broadcast_modal.attr('data-module-id');
// 		load_broadcast_data(module_ID, populate_broadcast_modal);

// 		$.notify({
// 			message: 'Component Changes Reset'
// 		}, {
// 			element: '#broadcast_modal',
// 			type: 'success'
// 		});
// 		$add_component_modal.modal('hide');
// 	} else {
// 		$.notify({
// 			message: 'Component Changes Not Reset'
// 		}, {
// 			element: '#broadcast_modal',
// 			type: 'danger'
// 		});
// 	}
// }