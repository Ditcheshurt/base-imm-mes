var processor 			  				= './ajax_processors/manage/broadcast_import.php';
var papaParseConfig 	  				= { header: true };
var insertSelectedRecordsButtonIndex 	= 3;
var supportedMessageTypes;
var table;

$(document).ready(function () {
    
	$('#broadcastText').focus();
	
	$.get('./config/app_config.json', function (data) {
	    supportedMessageTypes = data.broadcast_management_options.import_message_types_enabled;
	});
	
	$('#parseBroadcastText').on('click', function () {
		parseBroadcastText();
	});
	
	$('#insertSelectedRecords').on('click', function () {
		insertSelectedRecords(recordInsertComplete);
	});
	
	$('#isFirstRowHeader').on('click', function () {		
		papaParseConfig = { header: $('#isFirstRowHeader').prop('checked') };
	});	
	
})

function parseBroadcastText(){
	
	var dataSet = Papa.parse($('#broadcastText').val(), papaParseConfig);
	
	if (!isParsedSuccessfully(dataSet['errors'])) { return; }
	
	if (!isSupportedMessageType(dataSet['data'])) { return; }
	
	$('#broadcastTextPanel').hide();
	$('#parsedResultsPanel').show();
		
	table = $('#parsedBroadcasts').DataTable( {
				data: 		  dataSet['data'],
				bDestroy:     true,
				paging:       false,
				fixedHeader:  true,  
				select: 	  "multiple",      
				lengthChange: true,
				dom: 		  'Bfrtip',
				buttons: [
							{
								text: 'Paste Broadcast Text',
								action: function ( e, dt, node, config ) {
									$('#broadcastTextPanel').show();
									$('#parsedResultsPanel').hide();
								},
								enabled: true
							},
							'selectAll',
							'selectNone',
							{
								text: 'Insert Selected Messages',
								action: function ( e, dt, node, config ) {
									insertSelectedRecords(recordInsertComplete);
								},
								enabled: false
							}							
						 ],
				order: [[0, 'asc']],
				columns: [
					{
						"title":	"Sequence",
						"data": 	"SEQUENCE  "
					},
					{
						"title":	"Message Type",
						"data": 	"MSG   "
					},
					{
						"title":	"Plant Code",
						"data": 	"PLT"
					},
					{
						"title":	"Supplier Code",
						"data": 	"SUPPLIER"
					},
					{
						"title":	"Last 8 of VIN",
						"data": 	"VIN    "
					},
					{
						"title":	"Module Type",
						"data": 	" MOD TYPE"
					},
					{
						"title":	"Message Status",
						"data": 	"STATUS CD"
					}						
				]
			} );
			
	table.on( 'select', function () {
        toggleButtonBasedOnRowsBeingSelected(insertSelectedRecordsButtonIndex);
    } );
	
	table.on( 'deselect', function () {
        toggleButtonBasedOnRowsBeingSelected(insertSelectedRecordsButtonIndex);
    } );
}

function isParsedSuccessfully(parseErrors){
	
	if (parseErrors.length === 0){ return true; }
	
	// loop errors and notify.
	for (i = 0; i < parseErrors.length; i++) { 
		
		$.notify({
			message: 'Parsing error: ' + parseErrors[i]['message']
		}, {
			element: '#broadcastTextPanel',
			type: 'danger'
		});
	}
	
	return false;
}

function isSupportedMessageType(parsedData){
	
	var messageType = parsedData[0]['MSG   '];
	var isSupported = false;

    if (supportedMessageTypes) {
	    $.each(supportedMessageTypes, function(index, value){
	        if (messageType === value) {
	            isSupported = true; // Change the app_config broadcast_management_options.import_message_types_enabled setting.                
	        }
	    });
    }
 
    if (isSupported){ return true; }
	
	$.notify({
		message: 	'You\'re attempting to load messages of type [ ' + messageType + ' ]. Currently, the following message type(s) are supported [ ' + supportedMessageTypes.toString() + ' ].'
	}, {
		element: 	'#broadcastTextPanel',
		type: 		'danger'
	});
	
	return isSupported;
}

function toggleButtonBasedOnRowsBeingSelected(buttonIndex){
	var selectedRows = table.rows( { selected: true } ).count();
	table.button( buttonIndex ).enable( selectedRows > 0 );
	
	$('#insertSelectedRecords').prop('disabled', selectedRows < 1);
}

function insertSelectedRecords(callBack){
	
	$.post(processor, 
		   { 
				action: 'insertSelectedRecords',
				selectedRecords: table.rows('.selected').data().toArray()
		   }
	 )
     .done(callBack);
}

function recordInsertComplete(data){
	
	//alert(data.message);	
	
	if (data === null || data.type === 'success') {
        $.notify({
            message: data.message
        }, {
            element: 	'#parsedResultsPanel',
            type: 		'success'
        });
    }
    else {
        $.notify({
            message: data.message
        }, {
            element: 	'#parsedResultsPanel',
            type: 		'danger'
        });
    }
}