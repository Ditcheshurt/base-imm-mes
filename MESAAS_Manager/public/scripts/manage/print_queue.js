var processor = 'ajax_processors/manage/print_queue.php';
var table;
var printers;
var reprint_data = {};

$(document).ready(function () {

    $('#system_select').on('change', function (e) {
        reset_table();
        load_filter_options();
    });

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

	// date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	$('.btn-run').on('click', function () {
		load_data();
	});

    // reprint the chosen label
    $('body').on('click', '#print_queue_table .btn-print', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var reason = tr.find('.reason-select').val();
        console.log('poop');

        if (reason) {
            var m = $('#confirm_reprint_modal');
            var printer_option = tr.find('.printer-select option:selected');
            reprint_data = {
                id: row.data().ID,
                printer_id: parseInt(printer_option.val()),
                reason: tr.find('.reason-select').val(),
                database: chosen_system.database_name,
                process_type: chosen_system.process_type
            };

            m.find('.message').html('Label will be reprinted on printer: ' + printer_option.html());
        } else {
            var m = $('#message_modal'); 
            m.find('.message').html('Please select a reason for reprint.');
        }


        m.modal('show');
    });

    // confirm the reprint
    $('#confirm_reprint_modal .modal-footer').on('click', '.btn-confirm', function () {
        reprint_label(reprint_data, post_reprint_label);
    });
});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];
    
    //chosen_system = {process_type: 'SEQUENCED'};

    load_printers({process_type: chosen_system.process_type, database: chosen_system.database_name}, function (data) {
        printers = data;
        show_printers(data);
    });
}

function reset_table() {
    if (typeof table != 'undefined') {
        table = undefined;
        var p = $('#print_queue_table_wrapper').parent('div');
        $('#print_queue_table').remove();
        $('#print_queue_table_wrapper').remove();
        p.html('<table id="print_queue_table" class="table table-striped table-clickable table-hover"></table>');
    }
}

function load_data () {
	// table setup

	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

    if (chosen_system.process_type == 'SEQUENCED') {
        var columns = [
            {data: 'build_order', title: 'Build Order' },
            {data: 'sequence', title: 'Sequence'},
            {data: 'VIN', title: 'VIN'},
            {data: 'chute_number', title: 'Chute Number'},
            {data: 'line_code', title: 'Line'},
            {
                data: 'queue_time',
                title: 'Queue Time',
                render:  render_date
            },
            {data: 'print_text', title:'Printed Text', render: render_print_text},
            {
                title: 'Printer',
                data: 'printer_ID',
                defaultContent: '',
                render: render_printer_select
            },
            {
                title: 'Actions',
                className: 'actions',
                orderable: false,
                data: null,
                defaultContent: '',
                render: render_actions
            }
        ];

        var order = [[5, 'desc']];

    } else if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'REPETITIVE') {
        var columns = [
            {
                data: 'queue_time',
                title: 'Queue Time',
                render:  render_date
            },
            {data: 'print_text', title:'Printed Text', render: render_print_text},
            {
                title: 'Printer',
                data: 'printer_ID',
                defaultContent: '',
                render: render_printer_select
            },
            {
                title: 'Actions',
                className: 'actions',
                orderable: false,
                data: null,
                defaultContent: '',
                render: render_actions
            }
        ];

        var order = [[0, 'desc']]
    }

	table = $('#print_queue_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: processor,
            data: {
                action: 'get_print_queue',
                start_date: function (d) {return get_date_value('start_date');},
                end_date: function (d) {return get_date_value('end_date');},
                process_type: function (d) {return chosen_system.process_type;},
                database: function (d) {return chosen_system.database_name;},
                printer_id: function (d) {return $('#printer_select option:selected').val();}
            },
			dataSrc: ''
		},

		order: order,

		columns: columns
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function render_print_text (data, type, full, meta) {
	return '<span class="glyphicon glyphicon-info-sign" title="' + data.replace(/"/ig, '') + '"></span>';
}

function render_printer_select (data, type, full, meta) {
	var select = $('<select name="printer_'+full.ID+'" class="printer-select">');

	$.each(printers, function (i, printer) {
		if (printer.ID == data) {
			var option = $('<option name="printer_'+full.ID+'" value="'+printer.ID+'" selected>'+printer.printer_name+' (' + printer.printer_IP + ')</option>');
		}else {
			var option = $('<option name="printer_'+full.ID+'" value="'+printer.ID+'">'+printer.printer_name+' (' + printer.printer_IP + ')</option>');
		}
		select.append(option);
	});
	return select[0].outerHTML;
}

function render_actions (data, type, full, meta) {
	return render_reason_select(data, type, full, meta) + '<span class="glyphicon glyphicon-print btn-print" style="margin-left: 15px;"></span>';
}

function render_reason_select (data, type, full, meta) {
	var select = $('<select name="reason_'+full.ID+'" class="reason-select">');
	select.append($('<option name="reason_'+full.ID+'" value="" selected disabled>--Select Reason--</option>'));

	var reasons = [
    	'Torn Label',
    	'Printer Error',
    	'Unreadable Barcode',
    	'Other'
	];

	$.each(reasons, function (i, reason) {
		var option = $('<option name="reason_'+full.ID+'" value="'+reason+'">'+reason+'</option>');
		select.append(option);
	});
	return select[0].outerHTML;
}

function load_printers (params, callback) {
    if (params) {
        params.action = 'get_printers';
    } else {
        params = {action: 'get_printers'};
    }
    console.log(params);

    $.getJSON(
        processor,
        params,
        callback
    );
}

function show_printers (data) {
    $('#printer_select').empty();
    if (data) {
        $.each(data, function (i, printer) {
            var el = $('<option name="printer_id"></option>');
            el.prop('id', 'printer_'+printer.ID)
              .val(printer.ID)
              .html(printer.printer_name + ' (' + printer.printer_IP + ')');

            $('#printer_select').append(el);
        });
    }
}

function reprint_label (params, callback) {
    console.log('poop');
    if (params) {
        params.action = 'reprint';
    } else {
        params = {action: 'reprint'};
    }

    // test
    // console.log(params);
    // callback({});

	$.getJSON(
    	processor,
    	params,
    	callback
	);
}

function post_reprint_label(data) {
    // hide the confirmation modal and tell the user if the reprint was successful
    $('#confirm_reprint_modal').modal('hide');

    var m = $('#message_modal');
    var msg = m.find('.message');

    if (data !== false) {
        msg.html('Success!');
    } else {
        msg.html('Unable to reprint label.');
    }

    m.modal('show');
}