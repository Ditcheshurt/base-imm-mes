$(function() {
	loadSample();
});

function loadSample() {
	scheduler.locale.labels.timeline_tab = "Timeline";
	scheduler.locale.labels.section_custom="Machine";
	scheduler.config.details_on_create=true;
	scheduler.config.details_on_dblclick=true;
	scheduler.config.xml_date="%Y-%m-%d %H:%i";
	scheduler.config.mark_now = true;
	scheduler.xy.bar

	//===============
	//Configuration
	//===============
	var sections=[
		{key:1, label:"Demo Mold Machine"},
		{key:2, label:"1100 Ton - Press #4"}
	];

	scheduler.createTimelineView({
		name:	"timeline",
		x_unit:	"minute",
		x_date:	"%H:%i",
		x_step:	30,
		x_size: 24,
		x_start: 16,
		x_length:	48,
		y_unit:	sections,
		y_property:	"section_id",
		render:"bar",
		section_autoheight: false,
		dy:150,
		event_dy:"full"
	});

	//===============
	//Data loading
	//===============
	scheduler.config.lightbox.sections=[
		{name:"custom", height:23, type:"select", options:sections, map_to:"section_id" },
		{name:"description", height:130, map_to:"text", type:"textarea" , focus:true},
		{name:"time", height:72, type:"time", map_to:"auto"}
	];

	scheduler.templates.event_class=function(start, end, event){
		var css = "";

		if(event.type) // if event has subject property then special class should be assigned
			css += "event_"+event.type;

		if(event.id == scheduler.getState().select_id){
			css += " selected";
		}
		return css; // default return
	};

	scheduler.init('scheduler_here',new Date(2017,1,8),"timeline");
	scheduler.parse([
		{ start_date: "2017-02-08 09:00", end_date: "2017-02-08 12:00", text:"Sample Tool - 300", section_id:1, type:"batch"},
		{ start_date: "2017-02-08 12:00", end_date: "2017-02-08 12:30", text:"Tool Change", section_id:1, type:"tool_change"},
		{ start_date: "2017-02-08 12:30", end_date: "2017-02-08 16:00", text:"Sample Tool 2 - 600", section_id:1, type:"batch"},
		{ start_date: "2017-02-08 16:00", end_date: "2017-02-08 16:30", text:"Tool Change", section_id:1, type:"tool_change"},
		{ start_date: "2017-02-08 16:30", end_date: "2017-02-08 21:00", text:"Sample Tool - 1000", section_id:1, type:"batch"},

		{ start_date: "2017-02-08 12:00", end_date: "2017-02-08 20:00", text:"Tool ABC - 1200", section_id:2, type:"batch"},
		{ start_date: "2017-02-08 20:30", end_date: "2017-02-08 16:00", text:"Tool XYZ - 400", section_id:2, type:"batch"}
	],"json");
}

function loadSchedule() {

}