var processor = './ajax_processors/manage/production.php';
var dtKitBoxes = null;
var dtWip = null;
//var line_id = 100;
var systems;
var chosen_system;
var line_id;

$(document).ready(function () {
	//loadData();

	// initialize the system dropdown
	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	// load filtering options based on the chosen system
	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// initialize date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	// load module list
	$('.btn-run').on('click', function () {
		loadData();
	});

	//loadData();
});

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id, line_type: 0}, show_lines);
	//load_statuses(show_statuses);
}

function loadData() {
	$('.modal').modal('hide');
	$('.loading-msg').show();
	line_id = $('#line_select').val();

	$.getJSON(processor, {
		'action':'get_data',
		'line_id':line_id
	}, populate);

}

function reloadPage() {
	document.location = document.location;
}

function populate(jso) {
	console.log(jso);
	// display heading
	$('.loading-msg').hide();
	var d = new Date().toLocaleString();
	var heading =  'Line Report ' + ' Generated: '+ d;
	if (jso.production_qty != null) {
		heading = jso.production_qty[0].line_code + ' - Report ' + ' Generated: '+ d;
	}
	$('#heading').html(heading);

	t = $('#table-stations');
	// moveback action
	t.on('click', '.btn-moveback', function() {
		$('#div_move_back_modal').data('line_id', $(this).closest('tr').data('line_id'));
		$('#div_move_back_modal').data('station_id', $(this).closest('tr').data('station_id'));
		$('#div_move_back_modal').data('build_order', $(this).closest('tr').data('current_module'));
		$('#div_move_back_modal').modal('show');
	});
	// move back modal buttons
	$('#div_move_back_modal').on('click', '.btn-confirm', function() {
		$('#div_move_back_modal').modal('hide');
		$.getJSON(processor, {
			'action':'move_back',
			'station_id': $('#div_move_back_modal').data('station_id'),
			'build_order': $('#div_move_back_modal').data('build_order'),
			'line_id': $('#div_move_back_modal').data('line_id')
		}, reloadPage);
	}
	);

	$('#table-stations').on('click', '.current_module, .last_module, .next_module', function (mod) {
		var selection = $(this);
		selection.focus();
		var module_id;

		if (!selection.data('popover-shown')) {
			if (selection.hasClass('current_module')) {
				module_id = selection.closest('tr').data('current-module-id');
			} else if (selection.hasClass('last_module')) {
				module_id = selection.closest('tr').data('last-module-id');
			} else if (selection.hasClass('next_module')) {
				module_id = selection.closest('tr').data('next-module-id');
			}

			$.getJSON(processor, {
				'action':'get_module_detail',
				'module_id': module_id
			}, function (jso) {populateModuleDetail(selection, jso);});
			} else {
				selection.popover('hide');
				selection.data('popover-shown', false);
			}
		});

		$(document).on('blur', 'div[rel="popover"]', function (el) {
			$(this).popover('hide');
			$(this).data('popover-shown', false);
		});

		populateBroadcastStatus(jso);
		populateStationStatus(jso);
		populateProduction(jso);
		populateWipParts(jso);
		populateKitBoxes(jso);
		populateUpcoming(jso);

		//wip part click
		$('#table-wipparts tbody').on('click', '.btn-rebuild', function () {
			var row = dtWip.row($(this).closest('tr'));
			var m = $('#div_wip_modal');
			m.attr('data-id', row.data().ID);
			m.modal('show');
		});
		// wip modal confirm click
		$('#div_wip_modal .modal-footer').on('click', '.btn-confirm', function () {
			var m = $('#div_wip_modal');
			var wip_part_id = 	m.attr('data-id');

			$.getJSON(processor, {
				'action':'rebuild_wip_part',
				'wip_part_id': m.attr('data-id')
			}, reloadPage);
		});

		//delete kitbox click
		$('#table-kitboxes tbody').on('click', '.btn-delete', function () {
			var row = dtKitBoxes.row($(this).closest('tr'));
			var m = $('#div_kitbox_modal');
			m.attr('data-id', row.data().id);
			console.log('Kit data: ' + row.data().id);
			m.modal('show');
		});
		// kitbox modal confirm click
		$('#div_kitbox_modal .modal-footer').on('click', '.btn-confirm', function () {
			var m = $('#div_kitbox_modal');
			$.getJSON(processor, {
				'action':'delete_kitbox',
				'id': m.attr('data-id')
			}, reloadPage);
		});
	}

	function populateModuleDetail(ctrl, jso) {
		jso = jso.module_detail;
		var title = 'Module ' + jso[0].build_order;
		var markup = '<ul>';
		markup += '<li>KitBox ' + jso[0].container_id + '</li>';
		$.each(jso, function(k,v) {
			markup += '<li>' + v.internal_serial + ' - ' + v.part_desc + '</li>';
		});
		markup += '</ul>';

		ctrl.popover({
			title: title,
			content: markup,
			html: true,
			placement: 'bottom'
		}).popover('show');
		ctrl.data('popover-shown', true);
		//ctrl.html(markup);
	}

	function populateStationStatus(jso) {
		// station status table
		var cols = ['Actions', 'Station', 'IP Address',
		'Current Module', 'Next Module', 'Last Module'];
		// table header
		th = $('#table-stations .thead');
		th.empty();
		$.each(cols, function(k,v) {
			th.append('<th>' + v + '</th>');
		});
		var t = $('#table-stations .tbody');
		t.empty();
		$.each(jso.station_status, function(k,v) {
			var tr = '<tr data-current-module-id="'+v.current_module_id+'" data-last-module-id="'+v.last_module_id+'" data-next-module-id="'+v.next_module_id+'">';
			if (v.current_module != null && v.current_module != '') {
				tr += '<td><button class="btn btn-warning btn-xs btn-moveback"><span class="glyphicon glyphicon-back"></span>Move Back Current</button></td>';
			} else {
				tr += '<td></td>';
			}
			tr += '<td>'+ (v.station_order ? v.station_order : '') +'</td>';
			tr += '<td>'+ (v.ip_address ? v.ip_address : '') +'</td>';
			tr += '<td id="current_module"><div class="current_module" rel="popover">'+ (v.current_module ? v.current_module : '') +'</div></td>';
			tr += '<td><div class="next_module" rel="popover">'+ (v.next_module ? v.next_module : '') +'</div></td>';
			tr += '<td><div class="last_module" rel="popover">'+ (v.last_module ? v.last_module : '') +'</div></td></tr>';
			t.append(tr);
			var r = $('#table-stations tr:last');
			r.data('line_id', v.line_id);
			r.data('station_id', v.station_id);
			r.data('build_order', v.build_order);
			//r.find('current_module').hover($('module-hover').show());
		});
	}

	function populateUpcoming(jso) {
		var cols = [ 'Sequence', 'Build Order', 'VIN', 'Repair Status' ];
		// table header
		th = $('#table-upcoming .thead');
		th.empty();
		$.each(cols, function(k,v) {
			th.append('<th>' + v + '</th>');
		});
		var t = $('#table-upcoming .tbody');
		t.empty();
		$.each(jso.upcoming, function(k,v) {
			var tr = '<tr><td>'+ v.sequence +'</td>';
			tr += '<td>'+ v.build_order +'</td>';
			tr += '<td>'+ v.VIN +'</td>';

			var r = '';
			if (v.reject_status) {
				r = v.reject_status.replace('REJECTED_', '');
				r = '<span class="label label-danger">' + r + '</span>';
			}

			tr += '<td>'+ r +'</td></tr>';
			t.append(tr);
		}
		);
	}

	function populateBroadcastStatus(jso) {
		var cols = [ 'Broadcasts Today', 'Last Broadcast', 'Currently In-Queue',
		'Currently In-Build', 'Last Build', 'Built Today' ];
		// table header
		th = $('#table-broadcasts .thead');
		th.empty();
		$.each(cols, function(k,v) {
			th.append('<th>' + v + '</th>');
		});
		var t = $('#table-broadcasts .tbody');
		t.empty();
		$.each(jso.broadcast_status, function(k,v) {
			var tr = '<tr><td>'+ (v.broadcasts_today ? v.broadcasts_today : '0') +'</td>';
			tr += '<td>'+ (v.last_broadcast ? v.last_broadcast.date : '') +'</td>';
			tr += '<td>'+ (v.broadcasts_inqueue ? v.broadcasts_inqueue : '0') +'</td>';
			tr += '<td>'+ (v.broadcasts_inbuild ? v.broadcasts_inbuild : '0') +'</td>';
			tr += '<td>'+ (v.broadcast_lastbuild ? v.broadcast_lastbuild.date : '') +'</td>';
			tr += '<td>'+ (v.broadcasts_builttoday ? v.broadcasts_builttoday : '0') +'</td></tr>';
			t.append(tr);
		}
		);
	}

	function populateProduction(jso) {
		// table header
		thead = $('#table-production thead');
		thead.empty();
		for (i = 0; i < 25; i++) {
			var label = (i==0) ? 'Hour' : i;
			thead.append('<th>' + label + '</th>');
		}

		var t = $('#table-production .tbody');
		t.empty();

		var tr = '<tr>';
		for (i = 0; i < 25; i++) {
			if (i == 0) {
				tr +='<td>Qty</td>';
			} else {
				var qty = 0;
				if (jso.production_qty!= null) {
					$.each(jso.production_qty, function(k,v) {
						if (v.hour == i) {
							qty = v.qty_built;
						}
					})
				};

				if (i >= 7 && i < 15) {
					tr += '<td><span class="badge first-shift">'+ qty +'</span></td>';
				} else if (i >= 15 && i < 23) {
					tr += '<td><span class="badge second-shift">'+ qty +'</span></td>';
				} else if (i != 0) {
					tr += '<td><span class="badge third-shift">'+ qty +'</span></td>';
				}
			}
		}
		tr += '</tr>';
		t.append(tr);
	}

	function populateWipParts(jso) {
		if (dtWip != null) {
			dtWip.destroy(); //so the thing will update
		}
		if (jso.wip_parts) {
			dtWip = $('#table-wipparts').DataTable({
				"bInfo":false,
				"bAutoWidth":true,
				autoWidth:true,
				retrieve: true,
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
					]
				},
				data: jso.wip_parts,
				order: [[3, 'asc']],
				columns: [
				{
					title: 'Actions',
					className: 'actions',
					orderable: false,
					data: '',
					defaultContent: '',
					render: function (data, type, full, meta) {
						var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
						btns.append($('<button type="button" class="btn btn-xs btn-danger btn-rebuild">Rebuild</button>'));
						return btns[0].outerHTML;
					}
				},
				{data: 'line_code', 'title':'Line'},
				{data: 'last_completed_station_ID', 'title':'Last Station'},
				{data: 'build_order', 'title':'Build Order'},
				{data: 'sequence', 'title':'Sequence'},
				{data: 'serial_number', 'title':'Serial'},
				{data: 'VIN', 'title':'VIN'},
				{data: 'built_time', 'title':'Built', render:function(data, meta, full, meta) { if (data) {return data.date;} else { return 'NOT BUILT';} }  }

				]

			});
		} else {
			dtWip.destroy();
		}
	}

	function populateKitBoxes(jso) {
		if (dtKitBoxes != null) {
			dtKitBoxes.destroy(); //so the thing will update
		}

		if (jso.kit_boxes) {

			dtKitBoxes = $('#table-kitboxes').DataTable({
				"bInfo":false,
				"bAutoWidth":true,
				autoWidth:true,
				retrieve: true,
				dom: 'T<"clear">lfrtip',
				tableTools: {
					"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
					"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
					]
				},
				data: jso.kit_boxes,
				order: [[0, 'asc']],
				columns: [
				{
					title: "Actions",
					className: 'actions',
					orderable: false,
					data: '',
					defaultContent: '',
					render: function (data, type, full, meta) {
						var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
						btns.append($('<button type="button" class="btn btn-xs btn-danger btn-delete">Delete</button>'));
						return btns[0].outerHTML;
					}
				},
				{data: 'container_ID', title:'Container ID'},
				{data: 'build_order', title:'Build Order'},
				{
					data: 'time_queued',
					title:'Time Queued',
					type:'date',
					render: function (data, type, full, meta) {
						if (data) {
							return data.date;
						} else {
							return '';
						}
					}
				},
				{data: 'status', title:'Status'}
				]

			});

		} else {
			dtKitBoxes.destroy();
		}

	}

