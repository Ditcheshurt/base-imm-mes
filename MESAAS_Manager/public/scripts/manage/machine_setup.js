var processor = './ajax_processors/manage/machine_setup.php';

$(function() {
    load_machines();
    
    $('.tool-modal').find('.btn-save').on('click', update_tool);
    $('.parts-modal').find('.btn-save').on('click', update_tool_part);
    $('.machine-production-targets').find('.btn-save').on('click', update_machine_production_targets);
    $('.search').on('keyup', load_machines);

});

function load_machines() {
    var dat = {
        'action': 'get_machines',
        'machine_name': $('.search').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(after_load_machines);
}

function after_load_machines(jso) {
    var nav = $('.nav-list');
    nav.empty();
    var hdr = $('<li />');
    hdr.addClass('label label-default');
    hdr.html('Select Machine');
    hdr.appendTo(nav);
    $.each(jso, function(k, v) {
        var li = $('<li class="btn btn-success btn-sm navbar-btn" />');
        li.html(v['machine_number'] + ' | ' + v['machine_name']);
        li.data(v);
        li.appendTo($('.nav-list'));

        // machine click
        li.on('click', function() {
            var machine = $(this).data();            
            var machine_title = 'Configuring ' + machine['machine_name'];
            $('.machine-tools').empty();
            $('.machine').data(machine);
            $('.machine').html(machine_title);
            
            // build mach prod target button
            //$('.btn-machine-production-targets').show();
            
            var prodBtn = $('<button />');
            prodBtn.addClass('btn btn-sm pull-right btn-machine-production-targets');
            //prodBtn.attr('data-target','.machine_production_targets');
            prodBtn.html('Edit Machine Production Targets');
            prodBtn.appendTo($('.machine'));
            prodBtn.on('click', load_machine_production_targets);
            
            load_machine_tools();
        });
    });
}

function load_machine_production_targets() {
	var machine = $('.machine').data();
	var modal = $('.machine-production-targets');
	var body = modal.find('.modal-body');
	body.empty();
	modal.modal('show');
	
	for (var i = 0; i < 24; i++) {
		var id = "hour_" + i;		
		var h = i < 12 ? i : i - 12;
		var wrapper = $('<ul class="col-lg-4" />');
		var grp = $('<li class="input-group" />');
		var span = $('<span class="input-group-addon" />');
		var inp = $('<input class="form-control hour-value" id="'+id+'" type="number" min="0" max="100" placeholder="Target Percentage" aria-describedby="'+id+'" required />');
		var rng = $('<input class="range" type="range" min="0" max="100" placeholder="Target Percentage" aria-describedby="'+id+'" />');
		inp.val('0');
		rng.val();
		h = h == 0 ? 12 : h;
		h = i < 12 ? h + ' AM' : h + ' PM';	
		span.html(h);
		span.appendTo(grp);
		inp.appendTo(grp);	
		grp.appendTo(wrapper);
		rng.appendTo(wrapper);
		wrapper.appendTo(body);
		
		inp.on('keyup', function() { 
				var t = $(this).parent().parent().find('.range');
				t.val($(this).val());
			});
		rng.change(function() { 
				var t = $(this).parent().find('.hour-value');
				t.val($(this).val());
			});
	}
	
	$.ajax({
        url: processor,
        method: 'POST',
        data: {'action': 'get_machine_production_targets',
        	'machine_ID': machine['ID']
        	}
    }).done(function(jso) {
        $.each(jso, function(k,v) {
        	body.find('#hour_'+v['the_hour']).val(v['target_percent']);
        });
    });
}

function load_machine_tools() {
    var machine = $('.machine').data();
    $('.machine-tools').empty();
    $.ajax({
        url: processor,
        method: 'POST',
        data: {
            'action': 'get_machine_tool_parents'
            ,'machine_ID': machine['ID']
        }
    })
    .done(after_load_machine_tools);
}

function load_machine_sub_tools(tool_ID) {
    var subs = $('.collapse-tool-' + tool_ID).find('.panel-body-sub-tools');
    subs.empty();
    $.ajax({
        url: processor,
        method: 'POST',
        data: {
            'action': 'get_machine_tool_children'
            ,'tool_ID': tool_ID
        }
    }).done(after_load_machine_tools);
}

function after_load_machine_tools(jso) {
    if (jso == null) {
        return;
    }
    // load tool
    $.each(jso, function(k, v) {
        var trg = 'collapse-tool-' + v['ID'];
        var c = $('.clonable-tool-control').clone();
        c.removeClass('clonable-tool-control');
        c.addClass('tool-control');
        
        if ($('.' + trg).length > 0) {
            c = $('.' + trg);
        }
        c.data(v);
        c.find('.btn-tool .tool-title').html(v['tool_description']);
        c.find('.btn-tool').attr('data-target', '.' + trg);
        c.find('.btn-tool').attr('aria-controls', trg);
        c.find('.collapse').addClass(trg);
        c.find('.parts-title').html('Parts for ' + v['tool_description']);
        c.find('.subtools-title').html('Subtools for ' + v['tool_description']);
        c.find('.btn-new-part').data('tool_ID', v['ID']);

        // append to main tools or sub tools
        if (v['parent_tool_ID'] === undefined || v['parent_tool_ID'] == null) {
            c.appendTo($('.machine-tools'));
        } else {
            // parent tool panel
            var parentPanel = $('.machine-tools').find('.collapse-tool-' + v['parent_tool_ID']);
            var subs = parentPanel.find('.panel-body-sub-tools');
            c.appendTo(subs);
        }
        c.show();
        c.find('.' + trg).data(v);

        // tool collapse
        c.find('.' + trg).on('show.bs.collapse', function(e) {
            e.stopPropagation();
            var tool_ID = $(this).data('ID');
            load_tool_parts(tool_ID);
            load_machine_sub_tools(tool_ID);
        });

        // edit button click
        c.find('.btn-edit-tool').on('click', function(e) {
            e.stopPropagation();
            var tool = $(this).closest('.tool-control').data();
            var modal = $('.tool-modal');
            modal.data(tool);
            modal.find('.tool-tool-description').val(tool['tool_description']);
            modal.find('.tool-cycle-time').val(tool['target_cycle_time']);
            modal.find('.tool-active').prop('checked', tool['active']);
            modal.modal('show');
        });
    });
    
    
    $('.panel-parts').find('.btn-new-part').on('click', insert_new_part);
}

function load_tool_parts(tool_ID) {
    $.ajax({
        url: processor,
        method: 'POST',
        data: {
            'action': 'get_tool_parts'
            ,'tool_ID': tool_ID
        }
    }).done(after_load_tool_parts);
}

function after_load_tool_parts(jso) {
    if (jso == null) {
        return;
    }
    var parts = $('.collapse-tool-' + jso[0]['tool_ID']).find('.panel-body-parts').first();
    parts.empty();
    $.each(jso, function(k, v) {
        var part = $('<div />');
        part.addClass('btn btn-info');
        part.html('&nbsp;&nbsp;' + v['part_number'] + ' - ' + v['part_desc']);
        part.data(v);
        part.appendTo(parts);
        part.on('click', show_tool_part_modal);
        
        var badge = $('<div />').addClass('badge');
        badge.html(v['operation']);
        badge.prependTo(part);
    
    }); // end each
}

function show_tool_part_modal() {
    var part = $(this).data();
    var modal = $('.parts-modal');
    modal.data(part);
    modal.find('.part-number').val(part['part_number']);
    modal.find('.part-description').val(part['part_desc']);
    modal.find('.part-active').prop('checked', part['active']);
    modal.find('.part-operation').val(part['operation']);
    modal.find('.part-mrp-part-number').val(part['mrp_part_number']);
    modal.find('.part-part-image').val(part['part_image']);
    modal.find('.part-parts-per-tool').val(part['parts_per_tool']);
    modal.find('.part-scrap-cost').val(part['scrap_cost']);
    modal.find('.part-sub-tool-order').val(part['sub_tool_order']);
    modal.find('.part-cur-rack-count').val(part['cur_rack_count']);
    modal.find('.part-serialized').val(part['serialized']);
    modal.find('.part-zoned').val(part['zoned']);
    modal.find('.part-active-start').val(part['active_start']);
    modal.find('.part-active-end').val(part['active_end']);
    modal.find('.part-last-update').val(part['last_update']);
    modal.find('.part-standard-pack-qty').val(part['standard_pack_qty']);
    modal.find('.part-part-weight').val(part['part_weight']);
    modal.find('.part-box-weight').val(part['box_weight']);
    modal.find('.part-mrp-company-code').val(part['mrp_company_code']);
    
    modal.modal('show');
}

function update_tool_part() {
    var modal = $(this).closest('.parts-modal');
    var part_ID = modal.data('ID');
    var tool_ID = modal.data('tool_ID');
    var dat = {
        'part_number': modal.find('.part-number').val()
        ,'part_desc': modal.find('.part-description').val()
        ,'active': modal.find('.part-active').is(':checked')
        ,'operation': modal.find('.part-operation').val()
        ,'sub_tool_order': modal.find('.part-sub-tool-order').val()
        ,'mrp_part_number': modal.find('.part-mrp-part-number').val()
        ,'part_image': modal.find('.part-part-image').val()
        ,'parts_per_tool': modal.find('.part-parts-per-tool').val()
        ,'scrap_cost': modal.find('.part-scrap-cost').val()
        ,'sub_tool_order': modal.find('.part-sub-tool-order').val()
        ,'cur_rack_count': modal.find('.part-cur-rack-count').val()
        ,'serialized': modal.find('.part-serialized').is(':checked')
        ,'zoned': modal.find('.part-zoned').is(':checked')
        ,'active_start': modal.find('.part-active-start').val()
        ,'active_end': modal.find('.part-active-end').val()
        ,'last_update': new Date().toLocaleString().replace(/,/g, '')
        ,'standard_pack_qty': modal.find('.part-standard-pack-qty').val()
        ,'part_weight': modal.find('.part-part-weight').val()
        ,'box_weight': modal.find('.part-box-weight').val()
        ,'mrp_company_code': modal.find('.part-mrp-company-code').val()
    };

    // insert
    if (part_ID === undefined || part_ID == null) {
        dat['action'] = 'insert_tool_part';
        dat['tool_ID'] = modal.data('tool_ID');
    } else {
        dat['action'] = 'update_tool_part';
        dat['part_ID'] = part_ID;
    }
    
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).always(function() {
        load_tool_parts(tool_ID);
    });
}

function update_tool() {
    var modal = $('.tool-modal');
    var tool = modal.data();
    $(this).closest('.tool');
    
    var dat = {
        'action': 'update_tool'
        ,'tool_ID': tool['ID']
        ,'tool_description': modal.find('.tool-tool-description').val()
        ,'target_cycle_time': modal.find('.tool-cycle-time').val()
        ,'active': modal.find('.tool-active').is(':checked')
    };
    
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(function(jso) {
        load_machine_tools();
    });
}

function update_machine_production_targets() {
	var machine = $('.machine').data();
	var p = $('.machine-production-targets').find('.modal-body');
	var dat = {
			'action': 'update_machine_production_targets'
			,'machine_ID': machine['ID']
		};
	for (var i = 0; i < 24; i++) {
		dat[i] = p.find('#hour_'+i).val();
	}
	$.ajax({
        url: processor,
        method: 'POST',
        data: dat
  })
}

function insert_new_part() {
    var modal = $('.parts-modal');
    show_tool_part_modal();
    modal.data('tool_ID', $(this).data('tool_ID'));
}
