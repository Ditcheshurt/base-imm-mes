var processor = './ajax_processors/manage/operators.php';
var operators = [];
var operator_roles = [];
var roles = [];
var operator_areas = [];
var areas = [];
var sel_op_ID = 0;
var filter = '';
var operator_badge_printer_ID;
var operator_index = [];
var f_timer = null;

$(document).ready(
	function() {
		$.get( './config/app_config.json', function( data ) {
			operator_badge_printer_ID = data.operator_badge_printer_ID;
			loadAllData(populateOperators);
		});



		$('.btn-add').on('click', function() {
			$('#div_edit_modal .modal-title').text('Add Operator');
			$('#div_edit_modal INPUT').val('');
			$('.btn-save').data("edit_type", "ADD");

			var s = $('#div_edit_modal #selAreas');
			s.html('');
			for (var i = 0; i < areas.length; i++) {
				var op = $('<option />');
				op.attr('value', areas[i].ID);
				op.html(areas[i].area_name);
				s.append(op);
			}

			s.select2({"width":"style"});

			$('#div_edit_modal').modal('show');
		});

		$('#tbl_operators').on('click', '.btn-edit', function() {
			var d = $(this).data();
			$('#div_edit_modal .modal-title').text('Edit Operator');
			$('#div_edit_modal #inpFirstName').val(d.first_name);
			$('#div_edit_modal #inpLastName').val(d.last_name);
			$('#div_edit_modal #inpBadgeID').val(d.badge_ID);
			$('#div_edit_modal #inpEmail').val(d.email);
			$('#div_edit_modal #inpPassword').val(d.password);
			$('#div_edit_modal #inpShift').val(d.shift);

			var s = $('#div_edit_modal #selAreas');
			s.html('');
			var v = [];
			for (var i = 0; i < areas.length; i++) {
				if (d.dept_ID == null || d.dept_ID == areas[i].department_ID) {
					var op = $('<option />');
					op.attr('value', areas[i].ID);
					op.html(areas[i].area_name);

					for (var j = 0; j < d.areas.length; j++) {
						if (areas[i].ID == d.areas[j]) {
							op.prop('selected', true);
							v.push(areas[i].ID);
						}
					}
					s.append(op);
				}
			}

			s.select2({"width":"style"});

			$('#div_edit_modal #inpActive').attr('checked', (d.deactive_date == null));

			$('.btn-save').data("edit_type", "EDIT").data("op_ID", d.ID);
			$('#div_edit_modal').modal('show');
		});

		$('#tbl_operators').on('click', '.btn-print', function() {
			var d = $(this).data();
			$('.btn-print-confirm').data(d);
			$('#div_print_modal').modal('show');
		});

		$('#tbl_operators').on('click', '.cb-toggle', function() {
			var d = $(this);
			$.getJSON(processor, {
					"action":"save_operator_role",
					"op_ID":d.data('op_ID'),
					"role":d.data('role'),
					"on":((d.is(':checked'))?'1':'0')
				}, showToggleGrowl);
		});

		$('.btn-save').on('click', function() {
			saveOperator($(this));
		});

		$('.btn-print-confirm').on('click', function() {
			printOperatorBadge($(this));
		});

		$('#inpFilter').on('keyup', function() {
			if (f_timer) {
				clearTimeout(f_timer);
			}
			f_timer = setTimeout(function() {
				filter = $('#inpFilter').val();
				filterOperators(filter);
			}, 200);
		});

		$.ajaxSetup({
			"error":function(res) { alert("Error: " + res.responseText);  }
		});
	}
);

$(window).on('resize', function() {
	$('#tbl_operator_header').css('width', ($('#tbl_operators').width() + 1) + 'px');
});

function loadAllData(cb) {
	$.getJSON(processor, {
			"action":"load_operators"
		}, cb);
}

function populateOperators(jso) {
	$('#div_edit_modal').modal('hide');
	$('#div_filter_info').html('Loading...');

	operators = jso.operators || [];
	operator_roles = jso.operator_roles || [];
	roles = jso.roles || [];
	operator_areas = jso.operator_areas || [];
	areas = jso.areas || [];

	var t2 = $('#tbl_operators');
	var b = t2.find('TBODY');
	b.html('');

	var f = 0;
	for (var i = 0; i < operators.length; i++) {
		var o = operators[i];
		o.areas = [];
		var r = $('<tr />');

		var area_count = 0;
		for (var j = 0; j < operator_areas.length; j++) {
			if (operator_areas[j].operator_ID == o.ID) {
				area_count++;
				o.areas.push(operator_areas[j].area_ID);
			}
		}

		var c = $('<td />').html(o.name + '&nbsp;').addClass('td-name');
		var btn = $('<button />').html('<small><span class="glyphicon glyphicon-pencil"></span></small>').addClass('btn btn-xs btn-edit').data(o);
		c.append(btn);

		//var btn = $('<button />').html('<small><span class="glyphicon glyphicon-print"></span></small>').addClass('btn btn-xs btn-print').data(o);
		//c.append(btn);

		if (operator_badge_printer_ID == null || operator_badge_printer_ID == '') {
			$('.btn-print').hide();
		}

		//var area_count = $('<span />').html('<small>' + area_count + ' areas</small>').addClass('label label-info label-areas');
		//c.append(area_count);
		r.append(c);

		var c = $('<td />').text(o.badge_ID).addClass('td-ID');
		r.append(c);

		var c = $('<td />').addClass('td-active');
		var bt = $('<span />').addClass('label label-xs');
		bt.text(((o.deactive_date == null)?'yes':'no'));
		bt.addClass(((o.deactive_date == null)?'label-success':'label-danger'));
		c.append(bt);
		r.append(c);

		for (var j = 0; j < roles.length; j++) {
			var c = $('<td />').addClass('td-role');
			var cb = $('<input />', {"type":"checkbox", "class":"cb-toggle"});
			cb.data({"op_ID":o.ID, "role":roles[j]});
			var m = $.grep(operator_roles, function(or, i) {
				return or.operator_ID == o.ID && or.role == roles[j];
			});
			cb.attr('checked', (m.length > 0));
			c.append(cb);
			r.append(c);
		}

		b.append(r);
		operator_index.push({operator: o, row: r});

		if (filter && filter != '') {
			if (o.name.toUpperCase().indexOf(filter.toUpperCase()) < 0 && o.badge_ID.toString().indexOf(filter) < 0) {
				r.css('display', 'none');
				f++;
			}
		}
	}

	$('#div_loading').hide();
	t2.show();

	var t = $('#tbl_operator_header');
	t.css('width', (t2.width() + 1) + 'px');
	var h = t.find('THEAD');
	h.html('');
	var r = $('<tr />');
	r.append($('<th />').text('Name').addClass('td-name'));
	r.append($('<th />').text('Badge ID').addClass('td-ID'));
	r.append($('<th />').text('Active').addClass('td-active'));

	for (var i = 0; i < roles.length; i++) {
		r.append($('<th />').text(roles[i]).addClass('td-role'));
	}
	h.append(r);
	t.show();

	if (filter != '' && f > 0) {
		$('#div_filter_info').html('<i>' + f + ' of ' + operators.length + ' record(s) not visible due to filter.</i>');
	} else {
		$('#div_filter_info').html('<i>' + operators.length + ' operators found.</i>');
	}


}

function filterOperators(filter) {

	function partialMatch(operator, filter) {
		return operator.name.toUpperCase().indexOf(filter.toUpperCase()) > -1 || operator.badge_ID.toString().indexOf(filter) > -1;
	}

	function showFilterCount(f) {
		if (filter != '' && f > 0) {
			$('#div_filter_info').html('<i>' + f + ' of ' + operators.length + ' record(s) not visible due to filter.</i>');
		} else {
			$('#div_filter_info').html('<i>' + operators.length + ' operators found.</i>');
		}
	}

	var f = 0;
	$.each(operator_index, function (index, entry) {
		if (filter && filter != '') {
			if (partialMatch(entry.operator, filter)) {
				$(entry.row).show();
			} else {
				$(entry.row).hide();
				f++;
			}
		} else {
			$(entry.row).show();
		}
	});

	showFilterCount(f);
}

function saveOperator(btn) {
	$.getJSON(processor, {
		"action":"save_operator",
		"edit_type":btn.data('edit_type'),
		"first_name":$('#inpFirstName').val(),
		"last_name":$('#inpLastName').val(),
		"badge_ID":$('#inpBadgeID').val(),
		"active":(($('#inpActive').is(':checked'))?'1':'0'),
		"email":$('#inpEmail').val(),
		"password":$('#inpPassword').val(),
		"shift":$('#inpShift').val(),
		"op_ID":($('.btn-save').data("op_ID")||0),
		"areas":(($('#selAreas').val()||[]).join(','))
	}, populateOperators);
}

function printOperatorBadge(btn) {
	$.getJSON(processor, {
		"action":"print_operator_badge",
		"op_ID":$('.btn-print-confirm').data("ID")||0
	}, afterPrintOperatorBadge);
}

function afterPrintOperatorBadge(jso) {
	$('#div_print_modal').modal('hide');
	$.growl(
			{"title":" Success: ", "message":"Operator badge printed!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
}

function showToggleGrowl() {
	$.growl(
			{"title":" Success: ", "message":"Operator role saved!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
}