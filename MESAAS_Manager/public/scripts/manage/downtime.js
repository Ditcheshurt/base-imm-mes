var cur_dt_codes = [];
var cur_dt_id = 0;
var machine_type_ID = 0;
var processor = './ajax_processors/manage/downtime.php';

$(function() {
	onFirstLoad();
	setup_para();
	$('#tbl_downtime TBODY').on('click', '.btn-downtime', function(e) {
		e.preventDefault();
		cur_dt_id = $(this).data('ID');
		machine_type_ID = $(this).data('machine_ID');
		$('#div_downtime_save').html('<span class="glyphicon glyphicon-floppy-disk"></span> Save Downtime Reason').attr('disabled', false);
		$('#div_dt_reason').html('<i>Loading reason codes...</i>');
		$('#txt_dt_reason').val('');
		$('#div_downtime_save').prop('disabled', true);
		$('#div_modal_downtime').modal('show');
	});
	
	$('#tbl_downtime TBODY').on('click', '.btn-downtime-split', function(e) {
		e.preventDefault();
		$('#event-begin').prop('checked', true);
		$('#event-length').val(0);
		var d = $(this).data();
		$('#div_modal_downtime_split').data(d);
		$('#p_dt_machine').html(d.machine_name);
		$('#p_dt_duration').html(d.time_in_state);
		$('#p_dt_reason').html(d.reason_description);
		$('#event-length').attr("max",Math.floor(d.time_in_state));
		$('#div_modal_downtime_split').modal('show');
	});

	$('.split-downtime').click(function(){
		var d = $('#div_modal_downtime_split').data();
		cur_dt_id = d.ID;
		machine_type_ID = d.machine_ID;
		if($('#event-begin').prop('checked')){
			split($('#event-length').val(),1);
		}else{
			split($('#event-length').val(),2);
		}
	});
	$('.run-report').click(function(){
		filter_check();
	});

	$('#div_modal_downtime').on('shown.bs.modal', function (e) {
		e.preventDefault();

		$.ajax({
			"url": processor,
			"type": "POST",
			"dataType": "json",
			"cache": false,
			"data": {
				"api":"entry",
				"action": "get_dt_info",
				"id":cur_dt_id,
				"machine_type_ID":machine_type_ID
			},
			"success": function(ret) {
				//expecting code_info array and dt_info array
				if (!ret.dt_info[0].tool_description) {
					$('#div_dt_machine').html(ret.dt_info[0].machine_name);
				} else {
					$('#div_dt_machine').html(ret.dt_info[0].machine_name + ' (' + ret.dt_info[0].tool_description + ')');
				}
				$('#div_dt_start_time').html(ret.dt_info[0].change_time.date);

				if (ret.dt_info[0].time_in_state) {
					$('#div_dt_duration').html(ret.dt_info[0].time_in_state);
				} else {
					$('#div_dt_duration').html('<i>Current ongoing DT</i>');
				}

				//load codes
				cur_dt_codes = ret.code_info;
				getReasonButtons(0, 0);
			}

		});
	});

	$('#div_downtime_save').on('click', saveDowntime);

});

function onFirstLoad(){
	$.ajax(processor, {
		"data":{
			"action":"get_base_downtime"
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			//jso = jso.open_downtime;
			updateOpenDowntime(jso);
		}
	});	
	
}

function updateOpenDowntime(jso) {
	if (!jso) {
		return;
	}

	var  num_closed = jso.length;

	var b = $('#tbl_downtime TBODY');

	b.html('');

	if (jso.length == 0) {
		var r = $('<TR />');
		var c = $('<TD />').html('<h2>Good job!  No untracked downtime!!</h2>').attr('colspan', 4);
		r.append(c);
		b.append(r);
	} else {

		if (num_closed > 0) {
			var r = $('<TR />');
			b.append(r);

			for (var i = 0; i < jso.length; i++) {
				if (jso[i].time_in_state != null) {
					var r = $('<TR />');

					var c = $('<TD />').html(jso[i].machine_name);
					r.append(c);
					var c = $('<TD />').html(jso[i].reason_description);
					r.append(c);
					var c = $('<TD />').html(jso[i].change_time.date);
					r.append(c);
					var c = $('<TD />').html(jso[i].time_in_state);
					r.append(c);
					var c = $('<TD />');
					var btn = $('<BUTTON />').addClass('btn btn-primary btn-sm btn-downtime').html('<span class="glyphicon glyphicon-record"></span> Record Reason').data(jso[i]);
					var btn2 = $('<BUTTON />').addClass('btn btn-primary btn-sm btn-downtime-split').html('<span class="glyphicon glyphicon-record"></span> Split Time').data(jso[i]);
					btn.data(jso[i]);
					btn2.data(jso[i]);
					c.append(btn);
					c.append(btn2);
					r.append(c);

					b.append(r);
				}
			}
		}
	}
}

function getReasonButtons(parent_ID, level) {
	if (parent_ID == 0) {
		cur_chain = '';
	}
	$('#div_dt_reason').html('');
	$('#div_cur_chain').html(cur_chain);
	for (var i = 0; i < cur_dt_codes.length; i++) {
		if (cur_dt_codes[i].parent_ID == parent_ID) {
			var b = $('<BUTTON />');
			b.addClass('btn btn-primary dt-button');
			b.data('dtrid', cur_dt_codes[i].ID);
			b.data('desc', cur_dt_codes[i].reason_description);

			var num_children = 0;
			for (var j = 0; j < cur_dt_codes.length; j++) {
				if (cur_dt_codes[j].parent_ID == cur_dt_codes[i].ID) {
					num_children++;
				}
			}

			if (num_children > 0) {
				b.html(cur_dt_codes[i].reason_description + ' (' + num_children + ')');
				b.on('click', function(e) { cur_chain += $(e.target).data('desc') + '-> ';  getReasonButtons($(e.target).data('dtrid'), level + 1); });
			} else {
				b.html(cur_dt_codes[i].reason_description);
				b.data('force_comment', cur_dt_codes[i].force_comment);
				b.on('click', function(e) { e.stopPropagation(); e.preventDefault(); selectDTReason(e.target); });

				if (cur_dt_codes[i].require_team_leader) {
					b.append('<span class="glyphicon glyphicon-lock pull-right lock-icon"></span>');
					b.addClass('dt-requires-team-leader');
				}
			}

			$('#div_dt_reason').append(b);
		}
	}

	if (level > 0) {
		var b = document.createElement('BUTTON');
		$(b).addClass('btn btn-warning dt-button').html('Start Over');
		$(b).on('click', function(e) { getReasonButtons(0, 0); $('#div_downtime_save').prop('disabled', true); });
		$('#div_dt_reason').append(b);
	}
}

function selectDTReason(el) {
	$('.dt-button').removeClass('btn-success');
	$(el).addClass('btn-success');
	$('#txt_dt_reason').focus();

	sel_dt_code = $(el).data('dtrid');

	if ($(el).hasClass('dt-requires-team-leader')) {
		$('#div_downtime_save').prop('disabled', true);
		auth_check(2, 'Planned Downtime Recording', function() {
			$('#div_downtime_save').prop('disabled', false);
		});

		return;
	}

	$('#div_downtime_save').prop('disabled', false);
}

function saveDowntime(e) {
	$('#div_downtime_save').html('Saving...').attr('disabled', true);
	$.ajax({

		"url": processor,
		"type": "POST",
		"dataType": "json",
		"cache": false,
		"data": {
			"api":"entry",
			"action": "save_downtime_reason",
			"dtid":cur_dt_id,
			"dtrid":sel_dt_code,
			"comment":$('#txt_dt_reason').val()
		},
		"success": function(ret) {
			$('#div_modal_downtime').modal('hide');
		}

	});
	
	filter_check();
}

function setup_para(){
	var $panel = $('.report-parameters').nysus_panel({ 'panel_title': 'Search Parameters' });
	var $panel_body = $panel.find('.panel-body');	
	var $panel_footer = $panel.find('.panel-footer');
	//control vars
	var $run = $('<button class="btn btn-success btn-xs pull-right run-report">Search</button>');
	var $start_date = $('<div class="col-md-3" />');
	var $end_date = $('<div class="col-md-3" />');
	var $machines = $('<div class="col-md-4" />');
	var $tools = $('<div class="col-md-4" />');
	var $reasons = $('<div class="col-md-4" />');
	var $shifts = $('<div class="col-md-4" />');
	var start_date_options = {
		label_text: 'Start Date'
		, label_tooltip: 'Select start date'
		, control_id: 'start-date'
		, control_tag: 'input'
	};
	var end_date_options = {
		label_text: 'End Date'
		, label_tooltip: 'Select end date'
		, control_id: 'end-date'
		, control_tag: 'input'
	};
	var machines_options = {
		label_text: 'Machines'
		, label_tooltip: 'Select machine'
		, control_id: 'machines-combo'
		, control_tag: 'select'
	};
	var shift_options = {
		label_text: 'Shift'
		, label_tooltip: 'Select shift'
		, control_id: 'shifts-combo'
		, control_tag: 'select'
	};
	var tools_options = {
		label_text: 'Tools'
		, label_tooltip: 'Select tool'
		, control_id: 'tools-combo'
		, control_tag: 'select'
	};
	var reasons_options = {
		label_text: 'Reasons'
		, label_tooltip: 'Select reason'
		, control_id: 'reasons-combo'
		, control_tag: 'select'
	};
	$shifts.nysus_labeled_control(shift_options);
	$start_date.nysus_labeled_control(start_date_options);			
	$end_date.nysus_labeled_control(end_date_options);
	$machines.nysus_labeled_control(machines_options);
	$tools.nysus_labeled_control(tools_options);
	$reasons.nysus_labeled_control(reasons_options);

	// add controls to panel
	var $row = $panel_body.append('<div class="row" />');
	$start_date.appendTo($row);
	$end_date.appendTo($row);
	$row.appendTo($panel_body);
	$shifts.appendTo($row);
	$row = $panel_body.append('<div class="row" />');
	$machines.appendTo($row);
	$tools.appendTo($row);
	$reasons.appendTo($row);
	$row.appendTo($panel_body);
	$row = $panel_body.append('<div class="row" />');	
	$run.appendTo($row)
	$row.appendTo($panel_body);
	
	// additional attributes
	$('#start-date').attr('type', 'date');
	$('#end-date').attr('type', 'date');	
	
	//databinding	
	build_combo($('#machines-combo'), {action: 'get_machines'}, [{key: 'multiple', value: 'multiple'}]);
	build_combo($('#tools-combo'), {action: 'get_tools'}, [{key: 'multiple', value: 'multiple'}]);
	build_combo($('#reasons-combo'), {action: 'get_reasons_description_as_id'}, [{key: 'multiple', value: 'multiple'}]);	
	
	var shift_options = [];
	shift_options.push({id: 1, text: 'Shift 1'});
	shift_options.push({id: 2, text: 'Shift 2'});
	shift_options.push({id: 3, text: 'Shift 3'});
	$combo = $('#shifts-combo');
	$combo.attr('multiple', 'multiple');
	$combo.select2({data:shift_options});
	
}

function build_combo(combo, data, attr, selection) {	
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {		
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function filter_check(){
	var machines_list = null;
	var start_date = null;
	var end_date = null;
	var tools_list = null;
	var shifts_list = null;
	var reasons_list = null;
	if($('#start-date').val() != ''){
		start_date = $('#start-date').val();
	}
	if($('#end-date').val() != ''){
		end_date = $('#end-date').val();
	}
	if ($('#machines-combo').select2().val() != null){
		machines_list = $('#machines-combo').select2().val().join('~');
	}
	if ($('#tools-combo').select2().val() != null){
		tools_list = $('#tools-combo').select2().val().join('~');
	}
	if ($('#shifts-combo').select2().val() != null){
		shifts_list = $('#shifts-combo').select2().val().join(' OR sti.shift_ID = ');
	}
	if ($('#reasons-combo').select2().val() != null){
		reasons_list = $('#reasons-combo').select2().val().join('~');
	}
	if(machines_list == null && tools_list == null && shifts_list == null && reasons_list == null && end_date == null && start_date == null){
		onFirstLoad();
	}else{
		$.ajax(processor, {
			"data":{
				"action":"get_special_downtime",
				"machines":machines_list,
				"tools":tools_list,
				"reasons":reasons_list,
				"shifts":shifts_list,
				"start":start_date,
				"end":end_date
			},
			"dataType":"json",
			"method":"POST",
			"success":function(jso) {
				//jso = jso.open_downtime;
				updateOpenDowntime(jso);
			}
		});			
	}
}

function split(minutes, starting_point){
	$.ajax(processor, {
			"data":{
				"action":"split_old_time",
				"dt_id":cur_dt_id,
				"starting_point":starting_point,
				"length":minutes
			},
			"dataType":"json",
			"method":"POST",
			"success":function(jso){after_split(jso)}
	});
	$('#div_modal_downtime_split').modal('hide');
}

function after_split(jso){
	$('#div_downtime_save').html('<span class="glyphicon glyphicon-floppy-disk"></span> Save Downtime Reason').attr('disabled', false);
	$('#div_dt_reason').html('<i>Loading reason codes...</i>');
	$('#txt_dt_reason').val('');
	$('#div_downtime_save').prop('disabled', true);
	$('#div_modal_downtime').modal('show');
	filter_check();
}