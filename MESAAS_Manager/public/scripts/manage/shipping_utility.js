var processor = './ajax_processors/manage/shipping_utility.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

	$('#div_content').on('click', '.btn-add-shipment', function() {
		var d = $(this).data();
		addShipment(d.ship_to_code);
	});

	$('#div_content').on('click', '.btn-add-racks', function() {
		var d = $(this).data();
		$('#div_add_rack_msg').hide();
		$('#div_add_racks_modal .modal-title').html('Add Racks to Dock ' + d.ship_to_code + ' Shipment');
		$('#div_add_racks_modal').data('ship_to_code', d.ship_to_code).modal('show');
	});

	$('#div_content').on('click', '.btn-end-shipment', function() {
		var d = $(this).data();
		$('#div_end_shipment_modal .modal-title').html('End Shipment for Dock ' + d.ship_to_code);
		$('#div_end_shipment_modal .modal-body').html('<div>Are you sure you would like to end the shipment for dock ' + d.ship_to_code + '?</div>' +
				'<div class="alert alert-info">This will submit the shipment to the ERP/MRP system and print the shipping paperwork.</div>' +
				'<div class="alert alert-warning"><b>NO RACKS CAN BE ADDED AFTER THE SHIPMENT IS ENDED!</b></div>');
		$('#div_end_shipment_modal').data('ship_to_code', d.ship_to_code).modal('show');
	});

	$('#div_content').on('click', '.btn-remove-rack', function() {
		var d = $(this).parent().data();
		$('#div_remove_rack_modal .modal-title').html('Remove rack ' + d.rack_ID + ' From Shipment');
		$('#div_remove_rack_modal .modal-body').html('Are you sure you want to remove rack ' + d.rack_ID + ' from the current shipment?');
		$('#div_remove_rack_modal').data('rack_ID', d.rack_ID).modal('show');
	});

	$('.btn-confirm-remove-rack').on('click', function() {
		var d = $('#div_remove_rack_modal').data();
		removeRackFromShipment(d.rack_ID);
	});

	$('.btn-confirm-end-shipment').on('click', function() {
		var d = $('#div_end_shipment_modal').data();
		endShipment(d.ship_to_code);
	});

	$('#div_add_racks_modal').on('shown.bs.modal', function() {
		$('#inp_rack').val('');
		$('#inp_rack').focus();
	});

	$('#div_add_racks_modal').on('hide.bs.modal', function() {
		load_data();
	});

	$('#inp_rack').on('keypress', function(e) {
		if (e.keyCode == 13) {
			addRackToShipment($('#div_add_racks_modal').data('ship_to_code'), $('#inp_rack').val());
			$('#inp_rack').val('');
		}
	});

	load_data();
});

function load_data() {
	$.getJSON(processor,	{
			action: 'get_shipment_status'
		},	updateShipments
	);
}

function addShipment(ship_to_code) {
	$.growl(
			{"title":" Shipment Added ", "message":"Shipment added successfully!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);

	$.getJSON(processor,	{
			action: 'add_shipment',
			ship_to_code: ship_to_code
		},	updateShipments
	);
}

function addRackToShipment(ship_to_code, rack_scan) {
	$.getJSON(processor,	{
			action: 'add_rack_to_shipment',
			ship_to_code: ship_to_code,
			rack_scan: rack_scan.toUpperCase()
		},	afterAddRackToShipment
	);
}

function afterAddRackToShipment(jso) {
	if (jso.success == 1) {
		//good
		$('#div_add_rack_msg').show();
		$('#div_add_rack_msg .alert').removeClass('alert-danger').addClass('alert-success').html('Rack added successfully!');
		playSound(1);
	} else {
		$('#div_add_rack_msg').show();
		$('#div_add_rack_msg .alert').removeClass('alert-success').addClass('alert-danger').html(jso.err_msg);
		playSound(0);
	}
}

function removeRackFromShipment(rack_ID) {
	$.getJSON(processor,	{
			action: 'remove_rack_from_shipment',
			rack_ID: rack_ID
		},	afterRemoveRackFromShipment
	);
}

function afterRemoveRackFromShipment(jso) {
	$('#div_remove_rack_modal').data('rack_ID', '').modal('hide');
	$.growl(
			{"title":" Rack Removed ", "message":"Rack successfully removed from shipment!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);

	updateShipments(jso);
}

function endShipment(ship_to_code) {
	$.getJSON(processor,	{
			action: 'end_shipment',
			ship_to_code: ship_to_code
		},	function(data) {
			afterEndShipment(data, ship_to_code);
		}
	);
}

function afterEndShipment(jso, ship_to_code) {
	window.open('manage_shipping_utility_manifest.php?type=shipping_utility_manifest&ship_to_code=' + ship_to_code, 'manifest', 'width=800,height=600,toolbars=no');
	$('#div_end_shipment_modal').data('ship_to_code', '').modal('hide');
	$.growl(
			{"title":" Shipment Ended ", "message":"Shipment has been successfully ended!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);

	updateShipments(jso);  //hi
}

function updateShipments(jso) {
	$('#div_content').html('');
	for (var i = 0; i < jso.shipment_groups.length; i++) {
		var g = jso.shipment_groups[i];
		var d = $('<DIV />').addClass('panel panel-default');
		var h = $('<DIV />').addClass('panel-heading').html('<b style="font-size:14pt;">DOCK: ' + g.ship_to_code + '</b>').appendTo(d);
		var btn_add_shipment = $('<button />').html('<span class="glyphicon glyphicon-plus"></span> Add Shipment')
			.addClass('btn btn-success btn-add-shipment btn-sm pull-right').data('ship_to_code', g.ship_to_code);
		var btn_add_racks = $('<button />').html('<span class="glyphicon glyphicon-plus"></span> Add Racks')
			.addClass('btn btn-success btn-add-racks btn-sm pull-right').data('ship_to_code', g.ship_to_code);
		var btn_end_shipment = $('<button />').html('<span class="glyphicon glyphicon-log-out"></span> End Shipment')
			.addClass('btn btn-success btn-end-shipment btn-sm pull-right').data('ship_to_code', g.ship_to_code);

		//get shipment
		var has_shipment = false;
		for (var j = 0; j < jso.shipments.length; j++) {
			var s = jso.shipments[j];
			if (s.ship_to_code == g.ship_to_code) {

				has_shipment = true;
				break;
			}
		}

		if (!has_shipment) {
			var b = $('<DIV />').addClass('panel-body').appendTo(d);
			b.html('No shipment in progress.');
			h.append(btn_add_shipment);
		} else {
			//create table of existing racks
			var t = $('<TABLE />').addClass('table table-condensed table-bordered');
			var thead = $('<THEAD />').appendTo(t);
			var r = $('<TR />').appendTo(thead);
			var th = $('<TH />').appendTo(r).html('Line').css('width', '25%');
			var th = $('<TH />').appendTo(r).html('Racks');
			var tbody = $('<TBODY />').appendTo(t);
			var total_rack_count = 0;

			for (var j = 0; j < jso.lines.length; j++) {
				var l = jso.lines[j];
				if (l.ship_to_code == g.ship_to_code) {
					var r = $('<TR />').appendTo(tbody);
					var td = $('<TD />').appendTo(r).html(l.line_code);
					var s_line = $('<SPAN />').addClass('label label-default padded-left');
					td.append(s_line);
					var td = $('<TD />').appendTo(r);
					var rack_count = 0;
					for (var k = 0; k < jso.shipment_lanes.length; k++) {
						var sl = jso.shipment_lanes[k];
						if (sl.module_line_ID == l.ID) {
							var rack = $('<SPAN />').addClass('label label-info padded-right').html('Rack ' + sl.id).appendTo(td).data('rack_ID', sl.id);
							rack.tooltip({"title":"Min Seq: " + sl.min_sequence + "<br>Max Seq: " + sl.max_sequence, "html":true});
							rack_count++;
							total_rack_count++;
						}
					}
					s_line.html(rack_count + ' of ' + l.lanes_per_truck);
					if (rack_count == 0) {
						td.html('<i>No racks added</i>');
					} else {
						var delete_button = $('<span class="btn btn-xs btn-warning btn-circle btn-remove-rack"><i class="glyphicon glyphicon-remove"></i></span>');
						td.find('.label').last().append(delete_button);
					}
				}
			}

			d.append(t);

			h.append(btn_add_racks);
			if (total_rack_count > 0) {
				h.append(btn_end_shipment);
			}
		}

		$('#div_content').append(d);
	}
}

//sound_type: 0 = failure, 1 = success
function playSound(sound_type) {
	var s = $('#sounder_chrome');
	switch (sound_type) {
		case 0:
			s.attr('src', './public/sounds/buzzer.wav');
			break;
		case 1:
			s.attr('src', './public/sounds/dings.wav');
			break;
	}
}