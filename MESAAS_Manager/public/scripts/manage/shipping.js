var processor = './ajax_processors/manage/shipping.php';
var main_container = $('#main_container');
var confirm_modal = $('#confirm_modal');
var system_ID = 1;


$(document).ready(function () {
	load_production_snapshots(system_ID, show_production_snapshots);

	main_container.on('click', '.btn-override', function () {
		var btn = $(this);
		var panel = btn.closest('.panel');
		confirm_modal.data('line_ID', panel.data('line_ID'));
		confirm_modal.modal('show');
	});

	$('.btn-confirm', confirm_modal).on('click', function () {
		var line_ID = confirm_modal.data('line_ID');
		set_override(line_ID, 1, function (data) {
			load_production_snapshots(system_ID, show_production_snapshots);
			confirm_modal.modal('hide');
		});
	});
});

function set_override (line_ID, override, cb) {
	$.getJSON(processor, {
		action: 'set_override_status',
		line_ID: line_ID,
		override: override
	}).done(cb);
}

function load_production_snapshots (system_ID, cb) {
	$.getJSON(processor, {
		action: 'get_production_snapshots',
		system_ID: system_ID
	}).done(function (data) {
		if (data) {
			cb(data);
		}
	});
}

function show_production_snapshots (lines) {
	main_container.empty();

	if (lines) {
		lines.forEach(function (line) {
			show_line_production_snapshot(line);
		});
	}
}

function show_line_production_snapshot (line) {
	var panel = $('<div></div>').prop({id: '#line_'+line.line_ID+'_panel'}).addClass('panel panel-primary').data({line_ID: line.line_ID, override: line.override_rack_count}).appendTo(main_container);
	var panel_heading = $('<div></div>').addClass('panel-heading').appendTo(panel);
	var title = $('<h3></h3>').addClass('panel-title').appendTo(panel_heading);
	var panel_body = $('<div></div>').addClass('panel-body').appendTo(panel);

	title.html(line.line_ID +': '+line.line_code);
	$('<button></button>').addClass('btn btn-danger btn-override btn-xs pull-right').prop({type: 'button', disabled: line.override_rack_count}).html('Override').appendTo(title);

	// queue data
	var queue_div = $('<div></div>').addClass('col-sm-12 col-md-4').appendTo(panel_body);
	$('<label></label>').html('Queued: ').appendTo(queue_div);

	var queue_string = 'No Data';
	if (line.current_queue !== null && line.parts_per_rack !== null) {
		queue_string = line.current_queue + ' of ' + line.parts_per_rack;
	}
	var queue_data_badge = $('<span></span>').addClass('label label-as-badge badge-left-spaced').html(queue_string).appendTo(queue_div);
	queue_data_badge.addClass(get_queue_color(line));

	// broadcast time data
	var last_broadcast_div = $('<div></div>').addClass('col-sm-12 col-md-4').appendTo(panel_body);
	$('<label></label>').html('Last Broadcast: ').appendTo(last_broadcast_div);
	var last_broadcast_data_badge = $('<span></span>').addClass('label label-as-badge badge-left-spaced').html(line.last_broadcast_time_ago !== null ? line.last_broadcast_time_ago + ' min.' : 'No Data').appendTo(last_broadcast_div);
	last_broadcast_data_badge.addClass(get_last_broadcast_color(line));

	// current buffer data
	var current_buffer_div = $('<div></div>').addClass('col-sm-12 col-md-4').appendTo(panel_body);
	$('<label></label>').html('Current Buffer: ').appendTo(current_buffer_div);
	var current_buffer_data_badge = $('<span></span>').addClass('label label-as-badge badge-left-spaced').html(line.current_buffer !== null ? line.current_buffer : 'No Data').appendTo(current_buffer_div);
	current_buffer_data_badge.addClass(get_buffer_color(line));
}

function get_buffer_color (line) {
	var cls;

	if (line.current_buffer === null || line.current_buffer < line.buffer_critical) {
		cls = 'label-danger';
	} else if (line.current_buffer > line.buffer_critical && line.current_buffer < line.buffer_warning) {
		cls = 'label-warning';
	} else {
		cls = 'label-success';
	}

	return cls;
}

function get_last_broadcast_color (line) {
	var cls;

	if (line.last_broadcast_time_ago === null) {
		cls = 'label-danger';
	} else {
		cls = 'label-default';
	}

	return cls;
}

function get_queue_color (line) {
	var cls;

	if (line.current_queue === null || line.parts_per_rack === null) {
		cls = 'label-danger';
	} else {
		cls = 'label-default';
	}

	return cls;
}