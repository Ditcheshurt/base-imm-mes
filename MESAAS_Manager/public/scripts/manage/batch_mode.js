var processor = './ajax_processors/manage/batch_mode.php';
var table;
var systems;
var lines;
var chosen_system;
var chosen_line;

$(document).ready(function () {

//	$('#div_content').on('click', '.btn-add-shipment', function() {
//		var d = $(this).data();
//		addShipment(d.ship_to_code);
//	});
//
//	$('#div_content').on('click', '.btn-add-racks', function() {
//		var d = $(this).data();
//		$('#div_add_rack_msg').hide();
//		$('#div_add_racks_modal .modal-title').html('Add Racks to Dock ' + d.ship_to_code + ' Shipment');
//		$('#div_add_racks_modal').data('ship_to_code', d.ship_to_code).modal('show');
//	});
//
//	$('#div_content').on('click', '.btn-end-shipment', function() {
//		var d = $(this).data();
//		$('#div_end_shipment_modal .modal-title').html('End Shipment for Dock ' + d.ship_to_code);
//		$('#div_end_shipment_modal .modal-body').html('<div>Are you sure you would like to end the shipment for dock ' + d.ship_to_code + '?</div>' +
//				'<div class="alert alert-info">This will submit the shipment to the ERP/MRP system and print the shipping paperwork.</div>' +
//				'<div class="alert alert-warning"><b>NO RACKS CAN BE ADDED AFTER THE SHIPMENT IS ENDED!</b></div>');
//		$('#div_end_shipment_modal').data('ship_to_code', d.ship_to_code).modal('show');
//	});
//
//	$('#div_content').on('click', '.btn-remove-rack', function() {
//		var d = $(this).parent().data();
//		$('#div_remove_rack_modal .modal-title').html('Remove rack ' + d.rack_ID + ' From Shipment');
//		$('#div_remove_rack_modal .modal-body').html('Are you sure you want to remove rack ' + d.rack_ID + ' from the current shipment?');
//		$('#div_remove_rack_modal').data('rack_ID', d.rack_ID).modal('show');
//	});
//
//	$('.btn-confirm-remove-rack').on('click', function() {
//		var d = $('#div_remove_rack_modal').data();
//		removeRackFromShipment(d.rack_ID);
//	});
//
//	$('.btn-confirm-end-shipment').on('click', function() {
//		var d = $('#div_end_shipment_modal').data();
//		endShipment(d.ship_to_code);
//	});
//
//	$('#div_add_racks_modal').on('shown.bs.modal', function() {
//		$('#inp_rack').val('');
//		$('#inp_rack').focus();
//	});
//
//	$('#div_add_racks_modal').on('hide.bs.modal', function() {
//		load_data();
//	});
//
//	$('#inp_rack').on('keypress', function(e) {
//		if (e.keyCode == 13) {
//			addRackToShipment($('#div_add_racks_modal').data('ship_to_code'), $('#inp_rack').val());
//			$('#inp_rack').val('');
//		}
//	});
	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	$('#line_select').on('change', function (e) {
		var line_ID = $('#line_select option:selected').val();
		var res = $.grep(lines, function (l) {
			return l.line_ID == line_ID;
		});
		chosen_line = res[0];
		load_data();
	});

	$('.btn-add-batch-modules').on('click', function() {
		load_bom_IDs(populateBomIDs);
		$('#inp_qty').val('1');
		$('#div_add_batch_modules').modal('show');
	});

	$('#bom_select').on('change', function() {
		show_bom_components();
	});

	$('.btn-add-modules').on('click', function() {
		event.preventDefault();
		$.getJSON(processor, {
				action:"add_batch_modules",
				bom_ID:$('#bom_select option:selected').data().bom_ID,
				qty:$('#inp_qty').val(),
				line_ID:chosen_line.line_ID,
				database: chosen_system.database_name
			}, function(data) {
				$.growl('Batch modules added!', {type: 'success'});
				$('#div_add_batch_modules').modal('hide');
				load_data();
			}
		);
	});

	$('#tbl_queued TBODY').on('click', 'SPAN.move-module-up', function() {
		move_module($(this), 'UP');
	});

	$('#tbl_queued TBODY').on('click', 'SPAN.move-module-down', function() {
		move_module($(this), 'DOWN');
	});

	$('#tbl_queued TBODY').on('click', 'BUTTON.delete-module', function() {
		delete_module($(this));
	});

	$('#tbl_built TBODY').on('click', 'BUTTON.delete-module', function() {
		$('.btn-confirm-delete-module').data($(this).data());
		$('#div_confirm_delete_module').modal('show');  //show confirmation modal
	});

	$('.btn-confirm-delete-module').on('click', function() {
		delete_module($(this));
	});

	$('.btn-show-bom-compare').on('click', function() {
		load_bom_IDs(populateBomIDsForCompare);
		$('#div_bom_compare').modal('show');
	});

	$('#inp_hide_common').on('click', function() {
		if ($(this).is(':checked')) {
			var num_boms = $('#div_bom_compare TABLE THEAD TR TH').length;
			$('#div_bom_compare TABLE TBODY TR TD').each(function(i, el) {
				var cl = $(el).attr('class');
				if ($('.' + cl).length == num_boms) {
					$(el).css('color', '#FEFEFE');
				} else {
					$(el).css('color', '');
				}
			});

		} else {
			$('#div_bom_compare TABLE TBODY TR TD').each(function(i, el) {$(el).css('color', '');});
		}
	});
});

function move_module(btn, dir) {
	var d = btn.data();
	console.log('Moving module ' + d.sequence + ' ' + dir);
	$.getJSON(processor, {
			"action":"move_module",
			"database":chosen_system.database_name,
			"line_ID":chosen_line.line_ID,
			"module_ID":d.ID,
			"direction":dir
		}, function(data) {
			$.growl('Batch module moved!', {type: 'success'});
			updateBatchStatus(data);
		}
	);
}

function delete_module(btn) {
	var d = btn.data();
	console.log('Deleting module ' + d.sequence + '...');
	$.getJSON(processor, {
			"action":"delete_module",
			"database":chosen_system.database_name,
			"line_ID":chosen_line.line_ID,
			"module_ID":d.ID
		}, function(data) {
			$('#div_confirm_delete_module').modal('hide');
			$.growl('Batch module deleted!', {type: 'success'});
			updateBatchStatus(data);
		}
	);
}

function show_bom_components() {
	var data = $('#bom_select').find('option:selected').data();
	var d = $('#div_bom_components').html('<h6><b>Selected BOM (' + data.bom_ID + ') Components:</b></h6>');
	for (var i = 0; i < data.bom_string.length; i+=18) {
		var s = $('<span />').addClass('label label-info').html(data.bom_string.substring(i, i + 10)).css('margin','5px').css('display','inline-block');
		$('<span />').addClass('badge').html(parseInt(data.bom_string.substring(i + 11, i + 13))).appendTo(s);
		s.appendTo(d);
	}
}

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id, line_type: 0, only_batch_mode_lines:1}, function(data) {
		lines = data;
		show_lines(data);

		chosen_line = data[0];
		load_data();
	});
}

function load_data() {
	$.getJSON(processor,	{
			action: 'get_batch_status',
			line_ID: chosen_line.line_ID,
			database: chosen_system.database_name
		},	updateBatchStatus
	);
}

function updateBatchStatus(jso) {
	jso.queued = jso.queued || [];
	jso.in_progress = jso.in_progress || [];
	jso.built = jso.built || [];

	var b = $('#tbl_queued TBODY').html('');
	if (jso.queued.length == 0) {
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r).attr('colspan', 8).html('No records found');
	}
	for (var i = 0; i < jso.queued.length; i++) {
		var module = jso.queued[i];
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r);
		var x = null;
		if (i > 0) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-up mover move-module-up'); //up
			x.data(module);
			c.append(x);
		}
		if (i < (jso.queued.length - 1)) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-down mover move-module-down'); //down
			x.data(module);
			c.append(x);
		}
		var c = $('<TD />').appendTo(r).html(jso.queued[i].ID);
		var c = $('<TD />').appendTo(r).html(jso.queued[i].bom_ID);
		var c = $('<TD />').appendTo(r).html(jso.queued[i].sequence);
		var c = $('<TD />').appendTo(r);
		var s = $('<button />').addClass('btn btn-xs delete-module').data(module).attr('title', 'Delete Module').html('<span class="glyphicon glyphicon-trash"></span>');
		c.append(s);
	}

	var b = $('#tbl_in_progress TBODY').html('');
	if (jso.in_progress.length == 0) {
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r).attr('colspan', 8).html('No records found');
	}
	for (var i = 0; i < jso.in_progress.length; i++) {
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r).html(jso.in_progress[i].bom_ID);
		var c = $('<TD />').appendTo(r).html(jso.in_progress[i].sequence);
		var c = $('<TD />').appendTo(r).html(jso.in_progress[i].station_desc);
	}

	var b = $('#tbl_built TBODY').html('');
	if (jso.built.length == 0) {
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r).attr('colspan', 8).html('No records found');
	}
	for (var i = 0; i < jso.built.length; i++) {
		var module = jso.built[i];
		var r = $('<TR />').appendTo(b);
		var c = $('<TD />').appendTo(r).html(jso.built[i].ID);
		var c = $('<TD />').appendTo(r).html(jso.built[i].bom_ID);
		var c = $('<TD />').appendTo(r).html(jso.built[i].sequence);
		var c = $('<TD />').appendTo(r).html(jso.built[i].built_time);
		var c = $('<TD />').appendTo(r);
		var s = $('<button />').addClass('btn btn-xs delete-module').data(module).attr('title', 'Delete Module').html('<span class="glyphicon glyphicon-trash"></span>');
		c.append(s);
	}
}

function load_bom_IDs(cb) {
	$.getJSON(processor,	{
			action: 'get_bom_IDs',
			line_ID: chosen_line.line_ID,
			database: chosen_system.database_name
		},	cb
	);
}

function populateBomIDs(data) {
	data = data || [];
	var s = $('#bom_select').html('');
	$(data).each(function(i, el) {
		$('<OPTION />').val(el.bom_ID).html(el.bom_ID + ' (last module: ' + el.days_ago + ' days ago) (' + el.num_in_queue + ' in queue) (' + el.num_building + ' being built) (' + el.num_built + ' built)').appendTo(s).data(el);
	});
	show_bom_components();
}

function populateBomIDsForCompare(data) {
	data = data || [];
	var thead = $('#div_bom_compare TABLE THEAD TR').html('');
	var max_comp = 0;
	$(data).each(function(i, el) {
		$('<TH />').html('BOM ID: ' + el.bom_ID).appendTo(thead);
		if (el.bom_string.length > max_comp) {
			max_comp = el.bom_string.length;
		}
	});

	var tbody = $('#div_bom_compare TABLE TBODY').html('');
	max_comp = max_comp / 18;
	for (var i = 0; i < max_comp; i++) {
		var tr = $('<TR />').appendTo(tbody);

		$(data).each(function(j, el) {
			var pn = el.bom_string.substring(i*18, i*18+10);
			$('<TD />').html(pn).appendTo(tr).addClass('PN' + pn).on('mouseover', function() {
				var cl = $(this).attr('class');
				$('.' + cl).css('background-color', 'yellow');
			}).on('mouseout', function() {
				var cl = $(this).attr('class');
				$('.' + cl).css('background-color', '');
			});
		});
	}
}