var processor = './ajax_processors/manage/module_hold_queue.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

	// initialize the system dropdown
	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	// load filtering options based on the chosen system
	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// initialize date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	// load module list
	$('.btn-run').on('click', function () {
		load_data();
	});

	$('.btn-reloader').on('click', function () {
		load_data();
	});

	// open the module birth certificate
	$('#module_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=birth_certificate&module_ID='+row.data().ID+'&mrp_company_code='+chosen_system.mrp_company_code);
	});

	//open the module component list
	$('#module_table').on('click', '.btn-rebuild-module', function (e) {
		var d = $('#module_table').DataTable().row($(this).closest('tr')).data();
		$('.vin').html(d.VIN);
		$('.sequence').html(d.sequence);
		$('.queue_position').html(d.retrigger_position);
		$('#rebuild_modal').modal('show').data(d);
	});

	// confirm the rebuild
	$('#rebuild_modal .modal-footer').on('click', '.btn-confirm', function () {
		var m = $('#rebuild_modal');
		//var remove_components = m.find('input[name="remove_components"]').prop('checked') ? 1 : 0;
		rebuild_module(m.data('ID'), post_rebuild_module);
	});

	//open the module component list
	$('#module_table').on('click', '.btn-broadcast-components', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=module_component_info&module_ID='+row.data().ID);
	});
});

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id, line_type: 0}, show_lines);
	load_statuses(show_statuses);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#module_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},
		oLanguage:{sEmptyTable: "No modules found that are on hold."},
		ajax: {
			url: processor,
			data: {
				action: 'list_modules_on_hold',
				line_id: function (d) {return $('#line_select option:selected').val();}
			},
			dataSrc: '',
		},

		order: [[4, 'desc']],

		columns: [
		{
			data: 'loaded_time',
			title: 'Loaded Time',
			render:  render_date
		},
		{data: 'VIN', title: 'VIN'},
		{data: 'sequence', title: 'Sequence'},
        {data: 'pallet_number', title: 'Pallet'},
        {data: 'pallet_position', title: 'Position'},
		{
			title: 'Actions',
			className: 'actions',
			orderable: false,
			data: null,
			defaultContent: '',
			render: function (data, type, full, meta) {
				// show action buttons for each module in the list
				var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
				btns.append($('<button type="button" class="btn btn-xs btn-default btn-broadcast-components">View Broadcast</button>'));
				var btn = $('<BUTTON />').addClass('btn btn-xs btn-default btn-rebuild-module').data(data).html('Rebuild Module');
				btns.append(btn);

				return btns[0].outerHTML;
			}
		},
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function rebuild_module(module_ID, callback) {
	$.getJSON(
	processor,
	{
		action: 'rebuild_module',
		module_ID: module_ID
	},
	callback
	);
}

function post_rebuild_module(data) {
	// hide the confirmation modal and tell the user if the retrigger was successful
	var m = $('#rebuild_modal');
	m.find('input[name="remove_components"]').prop('checked', false);
	m.modal('hide');

	var m = $('#message_modal');
	var msg = m.find('.message');

	if (data[0].success == 1) {
		msg.html('Module set to be rebuilt with build order: '+data[0].new_build_order+'.');
		load_data();
	} else {
		msg.html('Unable to rebuild module!');
	}

	m.modal('show');
}

