var processor = './ajax_processors/manage/machine_monitor.php';
var tmr;
var refresh = [0,10];
var selPress = {};
var selParam = {};
var cont = $('div#param-chart');
var alrt = $('table#tbl-alerts');

$(document).ready(function () {
	loadMachines();
	loadActiveAlerts();
	loadSeverities();
	
	tmr = setInterval(doTimer, 1000);
	
	$('table#tbl-alerts tbody').on('click','button.btn-edit-alert',editAlert);
	$('table#tbl-alerts tbody').on('click','button.btn-ack-alert',ackAlert);
	
	$('button#refresh-all').on('click', function () {
		loadMachines();
		refreshData();
		loadActiveAlerts();
	});
	
	$('input.auto-refresh').on('click', function () {
		if (this.checked) {
			refresh[0] = refresh[1];
			tmr = setInterval(doTimer, 1000);
			$('div.progress').css('display','block');
			$('div.progress div.progress-bar').css('width','100%');
			$('button#refresh-all').prop('disabled',true);
		} else {
			clearInterval(tmr);
			$('div.progress').css('display','none');
			$('button#refresh-all').prop('disabled',false);
		}
	});
	
	$('button.btn-save-alert').on('click', function () {
		var d = $('form#alert-config').data(d);

		var ul = $('input#upper-limit').val();
		var ll = $('input#lower-limit').val();
		var ss = $('select#sel-severity').find(':selected').val();
		var aa = $('input#alrt-active').is(':checked')?1:0;
		
		$('button.btn-save-alert').removeClass('btn-default btn-primary btn-success btn-warning btn-danger')
		$('button.btn-save-alert').prop('disabled',true).addClass('btn-default');
		$.getJSON(processor, {
			action:'save_alert',
			machine_ID: d.machine_ID,
			tool_ID: d.tool_ID,
			dps_ID: d.dps_ID,
			mtcl_ID: d.mtcl_ID?d.mtcl_ID:null,
			upper_cl: ul,
			lower_cl: ll,
			severity: ss,
			alrt_active: aa,
			operator_ID: LOGIN.operator.ID
		})
		.done(function (jso, req_params){
			if (jso) {
				$('button.btn-save-alert').removeClass('btn-default').addClass('btn-success');
				setTimeout(function(){
					$('button.btn-save-alert').removeClass('btn-default btn-primary btn-success btn-warning btn-danger')
					$('button.btn-save-alert').prop('disabled',true).addClass('btn-default');
				},2000);
				$('label#machine-lbl').html('');
				$('label#tool-lbl').html('');
				$('label#datapt-lbl').html('');
				$('input#upper-limit').val('');
				$('input#lower-limit').val('');
				$('select#sel-severity').val('1');
				$('input#alrt-active').attr('checked',false);
				loadMachines();
				loadActiveAlerts();
			} else {
				$('button.btn-save-alert').removeClass('btn-default').addClass('btn-warning')
			}
			$('button.btn-save-alert').prop('disabled',false);
		})
		.fail(function () {
			console.error('Error saving alert');
			$('button.btn-save-alert').prop('disabled',false).removeClass('btn-default').addClass('btn-danger');
		});
	});
	
	$('.show-pgraph').on('click',function () {
		if ($('div#collapse-me').hasClass('collapse')) {
			$('a.show-pgraph').removeClass('collapsed');
			$('span.show-pgraph').addClass('open');
			$('div#collapse-me').removeClass('collapse').css('height','auto');
		} else {
			$('a.show-pgraph').addClass('collapsed');
			$('span.show-pgraph').removeClass('open');
			$('div#collapse-me').addClass('collapse').css('height','30px');
		}
	});
	
	alrt.DataTable({
		columns:[
			{title:"Shot Time",data:"cycle_time",render:renderDateTime},
			{title:"Machine",data:"machine_name"},
			{title:"Tool",data:"tool_description"},
			{title:"Parameter",data:"data_point_desc"},
			{title:"Upper Limit",data:"upper_ctrl_limit"},
			{title:"Lower Limit",data:"lower_ctrl_limit"},
			{title:"Value",data:"data_point_value"},
			{title:"Severity",data:"severity_description"},
			{title:"Edit",data:"edit",defaultContent:'<button class="btn btn-primary btn-edit-alert">Edit</button>'},
			{title:"Ack",data:"ack",defaultContent:'<button class="btn btn-success btn-ack-alert">Ack</button>'}
		],
		pageLength: 5,
		lengthMenu:[
			[5,10,15],
			[5,10,15]
		]
	});
});

function loadMachines() {
	$.getJSON(processor, {
			action: 'get_machines'
		})
		.done(function (jso,req_params) {
			var div = $('div.machine-select');
			div.empty();
			for(i = 0; i < jso.length; i++) {
				var btn = $('<a />')
					.attr('href','#')
					.addClass('btn press-select')
					.html('<span style="font-size: 1.0em">' + jso[i].machine_name + '</span><br /><span style="font-size: 0.5em;">Tool#: ' + jso[i].tool + '<br />Last Shot: '+jso[i].last_cycle_time.date.split('.')[0] + '</span>')
					.attr('id',jso[i].ID)
					.data(jso[i])
					.on('click', function(){selParam = [];machineSelected(this);});
				if (jso[i].ID == selPress.ID) {btn.addClass('selected-press');}
				btn.addClass(jso[i].up == 1?'btn-success':'btn-default');
				div.append(btn);
			}
		})
		.fail(function () {
			console.error('Unable to load machines.');
		});
}
function loadSeverities() {
	$.getJSON(processor, {
		action: 'get_severities'
	})
	.done (function (jso, req_params) {
		var sev_sel = $('select#sel-severity');
		for (i=0;i<jso.length;i++) {
			var sel_opt = $('<option />');
			sel_opt.html(jso[i].severity_description + ' ('+jso[i].severity_order+')').data(jso[i]);
			sel_opt.attr('value',jso[i].ID);
			sel_opt.appendTo(sev_sel);
		}
		console.log(jso);
	})
	.fail(function () {
		console.error('Unable to load severities');
	});
}
function machineSelected(el) {
	selPress = $(el).data();
	//selParam = {};
	$('a.press-select').removeClass('selected-press');
	$(el).addClass('selected-press');

	var params = {
		action: 'get_press_param_data',
		machine_ID: $(el).attr('id')
	};
	$.getJSON(processor, params)
		.done(afterMachineSelected)
		.fail(function () {
			console.error('Unable to load param data.');
		});
}
function afterMachineSelected(jso, req_params) {
	if (jso) {
		var ptbl = $('tbody.machine-params');
		ptbl.empty();
		for(i = 0; i < jso.length; i++){
			var tr = $('<tr />')
				.addClass('param')
				.attr('id',jso[i].ID)
				.data(jso[i])
				.on('click', parameterSelected);
			tr.addClass(Math.abs(jso[i].limit) == 1?'warning':Math.abs(jso[i].limit) > 1?'danger':'');
			tr.append($('<td />').html(jso[i].data_point_desc));
			tr.append($('<td />').html(jso[i].data_point_value + (jso[i].limit == 0?'':'  <span style="color: #2c3e50;" class="pull-right glyphicon glyphicon-chevron-' + (jso[i].limit > 0?'up':'down') + '"></span>')));
			ptbl.append(tr);
		}
		
		if (selParam.dps_ID) {
			var tr = $('tbody.machine-params tr');
			for(i=0;i<tr.length;i++){
				if ($(tr[i]).data().dps_ID == selParam.dps_ID) {
					$('tbody.machine-params tr.param').removeClass('param-select');
					$(tr[i]).addClass('param-select');
					break;
				}
			}
			getParameterData(selParam.machine_ID, selParam.tool_ID, selParam.dps_ID, 100);
		} else {
			$('span.show-pgraph').removeClass('open');
			$('div#collapse-me').addClass('collapse').css('height','30px');
			var chart = cont.highcharts();
			if (chart) {
				while (chart.series.length > 0) {
					chart.series[0].remove(true);
					chart.title.textStr = '';
				}
			}
		}
	}
}

function parameterSelected() {
	selParam = $(this).data();
	$('tbody.machine-params tr.param').removeClass('param-select');
	$(this).addClass('param-select');
	
	loadAlertForm(selParam);
	getParameterData(selParam.machine_ID, selParam.tool_ID, selParam.dps_ID, 100);
	$('a.show-pgraph').removeClass('collapsed');
	$('span.show-pgraph').addClass('open');
	$('div#collapse-me').removeClass('collapse').css('height','auto');
}
function getParameterData(machine_ID, tool_ID, data_point_ID, num_pts) {
	var n = num_pts||100;
	
	$.getJSON(processor, {
		action:'get_param_data',
		machine_ID:machine_ID,
		tool_ID: tool_ID,
		data_point_ID: data_point_ID,
		data_points: n
	})
	.done(function(jso,req_params) {
		generateParameterChart(jso);
	})
	.fail(function() {
		console.error('Failed to retrieve machine cycle data');
	});
}
function refreshData(machine_ID) {
	console.log('Refreshing...'+selPress.ID||machine_ID);
	if (machine_ID) {
		selPress = $('a.press-select#'+machine_ID).data();
		$('a.press-select').removeClass('selected-press');
		$('a.press-select#'+machine_ID).addClass('selected-press');
	}
	if (selPress.ID || machine_ID) {
		if (selPress.ID > 0 || machine_ID > 0) {
			var params = {
				action: 'get_press_param_data',
				machine_ID: selPress.ID||machine_ID
			};
			$.getJSON(processor, params)
				.done(afterMachineSelected)
				.fail(function () {
					console.error('Unable to load param data.');
				});
		}
	}
}
function doTimer() {
	$('div.progress div.progress-bar').css('width',(refresh[0]/refresh[1])*100 + '%');
	if (refresh[0] > 0) {
		refresh[0]--;
	} else {
		loadMachines();
		refreshData();
		loadActiveAlerts();
		refresh[0] = refresh[1];
	}
}

function loadActiveAlerts () {
	var params = {
		action: 'get_active_alerts'
	};
	$.getJSON(processor, params)
		.done(afterLoadActiveAlerts)
		.fail(function () {
			console.error('Unable to load active alerts.');
		});
}
function afterLoadActiveAlerts (jso, req_params) {
	var tbl = $('table#tbl-alerts').DataTable();
	var tbl_bdy = $('table#tbl-alerts tbody');
	
	tbl.clear().draw();
	if (jso) {
		tbl.rows.add(jso).draw(false);
	}
}
function loadAlertForm(d) {
	var m = $('label#machine-lbl');
	var t = $('label#tool-lbl');
	var dp = $('label#datapt-lbl');
	var ul = $('input#upper-limit');
	var ll = $('input#lower-limit');
	var ss = $('select#sel-severity');
	var aa = $('input#alrt-active');
	
	$('form#alert-config').data(d);
	
	m.text(d.machine_name);
	t.text(d.tool_short);
	dp.text(d.dps_ID?d.data_point_desc:'');
	ul.val(d.upper_ctrl_limit?d.upper_ctrl_limit:'');
	ll.val(d.lower_ctrl_limit?d.lower_ctrl_limit:'');
	
	if (d.severity) {
		$(ss.selector).val(d.severity);
	} else {
		$(ss.selector).val(1);
	}
	
	aa.prop('checked',d.active==1?true:false);
	
	$('button.btn-save-alert').prop('disabled',false).removeClass('btn-default').addClass('btn-primary');
}
function editAlert() {
	var t = $(this);
	var d = $('table#tbl-alerts').DataTable().row(t.parents('tr')).data();
	console.log(d);
	loadAlertForm(d);
}
function ackAlert() {
	var t = $(this);
	var d = $('table#tbl-alerts').DataTable().row(t.parents('tr')).data();
	
	t.removeClass('btn-success btn-warning btn-danger').addClass('btn-default');
	
	var params = {
		action: 'ack_alert',
		mc_ID: d.mc_ID,
		dps_ID: d.dps_ID,
		operator_ID: LOGIN.operator.ID
	};
	$.getJSON(processor, params)
		.done(function(jso, req_params) {
			if (jso.length > 0) {
				t.removeClass('btn-default').addClass('btn-success');
				loadActiveAlerts();
			} else {
				t.removeClass('btn-default').addClass('btn-warning');
			}
		})
		.fail(function () {
			t.removeClass('btn-default').addClass('btn-danger');
			console.error('Failed to ack alert');
		});
}
function ackAllAlerts() {
	var params = {
		action: 'ack_all_alerts',
		operator_ID: LOGIN.operator.ID
	};
	$.getJSON(processor, params)
		.done(function(jso, req_params) {
			if (jso.length > 0) {
				//t.removeClass('btn-default').addClass('btn-success');
				loadActiveAlerts();
			} else {
				//t.removeClass('btn-default').addClass('btn-warning');
				console.log('Problem tyring to acknowledge all active alerts');
			}
		})
		.fail(function () {
			//t.removeClass('btn-default').addClass('btn-danger');
			console.error('Failed to acknowledge all alerts');
		});
}

function load_param_data () {
	$('#div_parameter_chart').empty();
	$('body .btn-run').html('Loading...');
	$('body .btn-run').addClass('disabled');
	var sdate = -1;
	var edate = -1;

	try {
		sdate = get_date_time('start_date');
		edate = get_date_time('end_date');
		var params = {
			action: 'get_param_data',
			params: $('#params-combo').select2().val()?$('#params-combo').select2().val():-1,
			start_date: sdate,
			end_date: edate,
			database: chosen_system.database_name,
			machine_ID: $('#machine_select option:selected').val(),
			system_ID: function (d) {return $('#system_select option:selected').val();},
		};

		$.getJSON(processor, params)
			.done(generateParameterChart)
			.fail(function () {
				console.error('Unable to load param data.');
			});
	} catch (e) {
		if (sdate == -1) {
			$('.start_date').focus();
		}
		if (edate == -1) {
			$('.end_date').focus();
		}
		$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
		$('body .btn-run').removeClass('disabled');		
	}
}

function generateParameterChart(jso) {
	var data = [];
	var cols = [];
	var ucl = 1000;
	var lcl = 0;
	
	if(jso.length > 0) {
		for(i=0;i<jso.length;i++){
			var tmpCol = cols.findIndex(function(z){return z.data == jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')');});
			if(tmpCol == -1) {cols.push({data:jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')'),title:jso[i].data_point_desc});}
			
			tmpDataPT = jso[i].data_point_ID;
			var dptID = data.findIndex(function(a){return a.data_point_ID == tmpDataPT;});
			if(dptID > -1) {
				data[dptID].data_points.push({cycle_id:jso[i].cycle_ID,data_point_value:jso[i].data_point_value});
				if(data[dptID].ucl < jso[i].ucl) {data[dptID].ucl = jso[i].ucl;}
				if(data[dptID].lcl > jso[i].lcl) {data[dptID].lcl = jso[i].lcl;}
			} else {
				data.push({data_point_ID:jso[i].data_point_ID, data_point_desc:jso[i].data_point_desc, ucl:jso[i].ucl, lcl:jso[i].lcl, data_points:[]});
			}
		}
		
		var adj_pts = Math.floor(data[0].data_points.length/1000)+1;

		if (data[0].ucl) {
			ucl = data[0].ucl;
		}
		if (data[0].lcl) {
			lcl = data[0].lcl;
		}
/*
		cont.highcharts({
			chart: {type: 'spline'},
			title: {text: 'Parameter Data'},
			subtitle: {text: 'Other information'},
			xAxis: {
				type: 'datetime',
				dateTimeLabelFormats: {month: '%e, %b',year: '%b'},
				title: {text: 'Cycle Date'}
			},
			yAxis: {
				title: {text: 'Parameter Value (units)'},
				min: 0
			},
			tooltip: {
				headerFormat: '<b>{series.name}</b><br>',
				pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
			},
			plotOptions: {
				spline: {
					marker: {enabled: true}}
			},
			series: [{
				name: 'Parameter Data for (parameter 1)',
				data: []
			},{
				name: 'Parameter Data for (parameter 2)',
				data: []
			}]
		});
*/			
		cont.highcharts({
			chart: {type: 'line'},
			title: {text: data[0].data_point_desc,enabled: true},
			credits: {enabled: false},
			xAxis: {categories: [],labels: {enabled: false},tickInterval: adj_pts*1000,visible: false},
			yAxis: {
				plotBands: [
					{from: ucl,to: 10000,color: 'rgba(255, 0, 0, 0.1)'},
					{from: -10000,to: lcl,color: 'rgba(255, 0, 0, 0.1)'}
				]
			},
			legend: {shadow: true,enabled: true},
			tooltip: {shared: true},
			plotOptions: {
				line: {grouping: false,shadow: true,borderWidth: 0,turboThreshold:5000}
			},
			series: [{
				name: 'Values',
				color: 'rgba(22,170,2,.7)',
				data: [],
				pointInterval: 100,
				pointPlacement: 0,
				dataLabels: { enabled: false },
				point: {
					events: {
						mouseOver: function() {
							console.log('x: ' + this.x + ', y: ' + this.y);
							console.log(this);
						}
					}
				}
			},{
				name: 'Upper Limit',
				color: 'red',
				data: [],
				pointInterval: 100,
				pointPlacement: 0,
				dataLabels: { enabled: false },
				marker: {radius: 1}
			},{
				name: 'Lower Limit',
				color: 'red',
				data: [],
				pointInterval: 100,
				pointPlacement: 0,
				dataLabels: { enabled: false },
				marker: {radius: 1}
			}]
		});


		var arr = [];
		var arr2 = [];
		var arr3 = [];

		for (var k = 0; k < data[0].data_points.length; k++) {
			var x = data[0].data_points[k].cycle_id;
			var y = data[0].data_points[k].data_point_value;
			var c = 'limegreen';
			if (y < lcl || y > ucl) {
				c = 'red';
			}
			arr.push({y:parseFloat(y.toFixed(3)), name:('Cycle ID: ' + x), color:c});
			arr2.push(ucl);
			arr3.push(lcl);
		}
		var chart = cont.highcharts();
		chart.setTitle(data[0].data_point_desc);
		chart.series[0].setData(arr);
		chart.series[1].setData(arr2);
		chart.series[2].setData(arr3);
		chart.redraw();
	} else {
		$('<div />').html("No data found").appendTo($('#div_parameter_chart'));
	}
	$('#parameter_table').show();
	$('#parameter_panel').show();
}

function renderDateTime (data, type, row) {
	var d = 0;
	if (data.date) {d = new Date(data.date);}
	return d.toLocaleString();
}

function init_date_time (id,start_nEND) {
	// initialize the date input to the current date
	if (start_nEND === undefined) {start_nEND = true;}
	
	var today = new Date();
	if (start_nEND) {
		today.setHours(0,0,0,0);
	} else {
		today.setHours(23,59,0,0);
	}
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().slice(0,-1);
	$('#'+id).val(today_str);
}

function get_date_time (id) {
	var date_str = $('#'+id).val();
	var objDT = new Date(date_str);
	var offset = objDT.getTimezoneOffset() * 60;
	objDT = new Date(objDT.getTime() - offset * 1000);
	return objDT.toISOString().split('T')[0].replace(/\-/g,'/') + ' ' + objDT.toISOString().split('T')[1].slice(0,8);
}
