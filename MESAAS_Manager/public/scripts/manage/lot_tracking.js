var processor = 'ajax_processors/manage/lot_tracking.php';

var testsTable;
var testResultsTable;

var start_date_input;
var end_date_input;

$(document).ready(function () {
	start_date_input = $('#start_date');
	end_date_input = $('#end_date');
	
	    // date range filtering
    init_date_time('start_date', true, -7);
    init_date_time('end_date', true, 1);

	$('.btn-run').prop('disabled', false);
    $('.btn-run').on('click', function() {getMaterialCertTests(populateMaterialCertTests);});
	
	$('#tests_table').on('click', '.btn-set-ltnum', function (e) {
		var data = {};
		var row_data = testsTable.row($(this).closest('tr')).data();
		
		var mdl = {}; mdl.buttons = [];
		mdl.title = "Assign Lot Number (Test ID: {{ID}})";
		mdl.content = 'Please enter the lot number for test record ID {{ID}} <br><br> <input type="text" id="inpLotNum"></input>';
		mdl.data = row_data;
		mdl.buttons[0] = {};
		mdl.buttons[0].caption = "Save";
		mdl.buttons[0].cb = assignLotNumber;
		mdl.buttons[0].class = "success";
		mdl.buttons[0].data = row_data;
		mdl.buttons[0].icon = "ok";
		mdl.buttons[0].id = "btn-asn-ltnum";
		main_modal(mdl);
	});

	$('#tests_table').on('click', '.btn-set-active', function (e) {
		var data = {};
		var row_data = testsTable.row($(this).closest('tr')).data();
		
		var req_params = {};
		req_params.action = 'get_machines';
		$.getJSON(processor, req_params, function (jso) {
			var general = jso.general || [];
			var data = jso.data || [];
			var lot_machines = row_data.active_lots;
			
			if (general.return_code >= 0) {
				//build select boxes
				var cnt = $('<DIV />').html('Please select the machine(s) lot number {{lot_number}} is active on:');
				
				var mach = data[0];
				for (var i = 0; i<mach.length; i++) {
					cnt.append(
						$('<DIV />').addClass('checkbox').append(
							$('<LABEL />').addClass('checkbox-inline').html('<input type="checkbox" value="" class="fancy" id="'+mach[i].ID+'" '+(lot_machines.findIndex(x => x.machine_ID == mach[i].ID) >= 0?'checked':'')+'> '+mach[i].machine_name+' ('+mach[i].machine_number+')')
						)
					);
				}
				
				var mdl = {}; mdl.buttons = [];		
				mdl.title = "Activate Lot Number {{lot_number}}";
				mdl.content = cnt[0].outerHTML;
				mdl.data = row_data;
				mdl.buttons[0] = {};
				mdl.buttons[0].caption = "Confirm";
				mdl.buttons[0].cb = setActiveLots;
				mdl.buttons[0].class = "success";
				mdl.buttons[0].data = row_data;
				mdl.buttons[0].icon = "ok";
				mdl.buttons[0].id = "btn-activate-lot";
				main_modal(mdl);		
			} else {
				$.notify('Encountered an error when attempting to set lot number: error['+general.return_code+'] '+general.return_msg,{type:'danger',z_index:9999});
			}
		});
	});
	
	$('#tests_table').on('click', '.btn-show-res', function (e) {
		event.preventDefault();
		
		var data = {};
		var tr = $(this).closest('tr');
		var row = testsTable.row(tr);
		var row_data = testsTable.row(tr).data();
		
		var shown = $('#tests_table .shown')
		
		if (testsTable.row(shown).child.isShown()){
			testsTable.row(shown).child.hide();
			shown.removeClass('shown');
			if (shown[0] == tr[0]) {return;}
		}
		
		//show detail on click
		tr.addClass('shown');
		var $childContent = $('<div class="panel panel-primary"><div class="well"><table class="table-condensed table-bordered table-striped" id="testResults_table" style="width:100%;"><tbody><tr><td>Loading...</td></tr></tbody></table></div></div>');
		row.child($childContent).show();

		populate_testResults(row_data.test_results);
	});
});

//------------ Main Processor Functions ------------//
function assignLotNumber (e) {
	var req_params = {};
	var m = $('#main_modal');
	var d = m.data();
	
	req_params.action = 'assign_lot_number';
	req_params.test_ID = d.ID;
	req_params.lot_number = m.find('#inpLotNum').val();
	$.getJSON(processor, req_params, afterAssignLotNumber);
}

function afterAssignLotNumber (jso) {
	var general = jso.general || [];
	var data = jso.data || [];
	var m = $('#main_modal');
	
	if (general.return_code >= 0) {
		getMaterialCertTests(populateMaterialCertTests);
		m.modal('hide');
	} else {
		$.notify('Encountered an error when attempting to set lot number: error['+general.return_code+'] '+general.return_msg,{type:'danger',z_index:9999});
	}
}

function setActiveLots (e) {
	var req_params = {};
	var m = $('#main_modal');
	var d = m.data();
	
	var machine_lot_active =[];
	var cb = $('#main_modal input[type=checkbox]');
	cb.map(x => (cb[x].checked?machine_lot_active.push(cb[x].id):true));
	
	req_params.action = 'set_active_lot';
	req_params.test_ID = d.ID;
	req_params.operator_ID = LOGIN.operator.ID;
	req_params.lot_machines = machine_lot_active.join('~');
	$.getJSON(processor, req_params, afterSetActiveLot);
}

function afterSetActiveLot (jso) {
	var general = jso.general || [];
	var data = jso.data || [];
	var m = $('#main_modal');
	
	if (general.return_code >= 0) {
		getMaterialCertTests(populateMaterialCertTests);
		m.modal('hide');
	} else {
		$.notify('Encountered an error when attempting to set lot number: error['+general.return_code+'] '+general.return_msg,{type:'danger',z_index:9999});
	}
}

function getMaterialCertTests(callback) {
    if (typeof testsTable !== 'undefined') {
        testsTable.clear();
		testsTable.draw();
    }
	
	var data = getFilterOptions() || {};
	data.action = 'get_recent_tests';
    $.getJSON(processor, data, callback);
}

function populateMaterialCertTests(jso) {
	var data = jso.data || [];
	var general = jso.general || [];
	
    var tests = data[0] || [];
	var testr = data[1] || [];
	var mlots = data[2] || [];
	
	if (general.return_code >= 0) {
		var data = tests.map(
			function(x) {
				if (!x['active_lots']) {x['active_lots'] = [];}
				if (!x['test_results']) {x['test_results'] = [];}
				testr.forEach(
					function(y) {
						if (y.mct_ID === x.ID) {x.test_results.push(y);}
					}
				);
				mlots.map(
					function (lot) {
						if (lot.material_cert_test_ID == x.ID) {
							x['active_lots'].push(lot);
						}
					}
				);
				return x;
			}
		);
		loadTests(data);
	} else {
		$.notify('Encountered an error retrieving data: error[' + general.return_code + '] ' + general.return_msg, {type:"danger",z_index:9999});
	}
}


//------------ Table Building Functions ------------//
function loadTests(tbl_data) {
    

    // table setup
    if (typeof testsTable != 'undefined') {
        testsTable.clear();
		testsTable.rows.add(tbl_data);
		testsTable.draw();
		return;
    }

    var export_cols = [0, 1, 2, 3, 4, 5];

    testsTable = $('#tests_table').DataTable({
		"bAutoWidth":false,
		dom: 'T<"clear">lfBrtip',
		width: '100%',
		buttons: [
				'copyHtml5',
				'excelHtml5',
				'csvHtml5',
				'pdfHtml5',
						
				{
					text: 'Print',
					action: function (nButton, oConfig, oFlash) {
						
							var num_rows = this.rows()[0].length;
							var old_rows = this.settings()[0]._iDisplayLength;
							this.settings()[0]._iDisplayLength = num_rows;
							this.settings().draw();
							NYSUS.COMMON.REPORT.print_element($('#tests_table'), true, '');
							this.settings()[0]._iDisplayLength = old_rows;
							this.settings().draw();
							}
				}
			],
		order: [[1, 'desc']],
		columns: [
			{data: 'ID', title: 'Internal Test ID'},
			{
				data: 'timestamp',
				title: 'Test Completion Date',
				render:  render_date
			},
			{data: 'pass_fail', title: 'Test Result', render: function (d,t,f,m){return (d==1?'Pass':'Fail');}},
			{data: 'lot_number', title: 'Assigned Lot Number'},
			{
				data: null,
				defaultContent: '',
				orderable: false,
				title: 'Active Machines',
				render: function (d,t,f,m) {
					var t = [];
					for (i=0;i<d.active_lots.length;i++){t.push(d.active_lots[i].machine_number);}
					return t.join(', ');
				}
			},
			{
				title: 'Available Actions',
				className: 'action',
				orderable: false,
				data: null,
				defaultContent: '',
				render: function (data, type, full, meta) {
					var btns = $('<div class="btn-group no-export" role="group" aria-label="..."></div>');

					if (data.lot_number.length == 0) {
						btns.append($('<button type="button" class="btn btn-xs btn-default btn-set-ltnum">Assign Lot Number</button>'));
					} else {
						//btns.append($('<button type="button" class="btn btn-xs btn-disabled">Acknowledged</button>'));
						btns.append($('<button type="button" class="btn btn-xs btn-success btn-set-active">Set Active Lot</button>'));
					}
					btns.append($('<button type="button" class="btn btn-xs btn-success btn-show-res">Show Test Results</button>'));
					return btns[0].outerHTML;
				}
			}
		],
		data: tbl_data
	});
}

function populate_testResults(jso) {
	//console.log('testResults jso');
	//console.log(jso);
	if (testResultsTable != null) {
        //testResultsTable.destroy();    //so the thing will update
        $('#testResults_table').empty();
    }

	testResultsTable = $('#testResults_table').DataTable({
        dom: 'T<"clear">lfBrtip',
        bAutoWidth:false,
		bFilter: false,
		width: '100%',
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],
		order: [[4, 'asc']],
		columns: [
			{data: 'ID', visible: false},
			{data: 'mct_ID', visible: false},
			{data: 'data_point_desc', title: 'Data Point'},
			{data: 'value', title: 'Value'},
			{data: 'display_order', visible: false}
		],
		data: jso
    });
}


//------------ Helper Functions --------------------//
function init_date_time (id,start_nEND, days_offset = 0) {
	// initialize the date input to the current date
	if (start_nEND === undefined) {start_nEND = true;}
	
	var today = new Date();
	if (start_nEND) {
		today.setHours(0,0,0,0);
	} else {
		today.setHours(23,59,59,0);
	}
	var offset = (today.getTimezoneOffset() * 60) - (24*60*60*days_offset);
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().slice(0,-1);
	$('#'+id).val(today_str);
}

function getFilterOptions() {
	var data = {};
	var sdate = new Date(start_date_input.val());
	var edate = new Date(end_date_input.val());
	data.start_date = sdate.toSQLDate();
	data.end_date = edate.toSQLDate();
	return data;
}

function render_date (data, type, full, meta) {
    if (data) {
        return data.date.substr(0,data.date.length-7);
    } else {
        return '';
    }
}

function main_modal (opts) {
	var m = $('#main_modal');
	var t = $('#main_modal_title');
	var c = $('#div_modal_content');
	var b = $('#div_modal_btns');
	
	t.empty();
	c.empty();
	b.empty();
	b.off();
	
	if (opts.buttons) {
		for (i = 0; i < opts.buttons.length; i++) {
			b.append(
				$('<BUTTON />')
				.addClass('btn btn-sm btn-'+(opts.buttons[i].class?opts.buttons[i].class:'success'))
				.append(
					$('<SPAN />')
					.addClass('glyphicon glyphicon-' + opts.buttons[i].icon)
					.html(' ' + opts.buttons[i].caption)
				)
				.attr('id',opts.buttons[i].id)
				.data(opts.buttons[i].data)
			);
			b.on('click','#'+opts.buttons[i].id,opts.buttons[i].cb);
		}
		b.append('<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>');
	}
	
	if (opts.title) {
		var objs = Object.getOwnPropertyNames(opts.data);
		objs.forEach (function(e) {
			opts.title = opts.title.replace('{{'+e+'}}',opts.data[e]);
		});
		t.html(opts.title);
	} else {
		t.html("Replacement Orders");
	}

	if (opts.content && opts.data) {
		var objs = Object.getOwnPropertyNames(opts.data);
		objs.forEach (function(e) {
			opts.content = opts.content.replace('{{'+e+'}}',opts.data[e]);
		});
		c.html(opts.content);
	} else {
		c.html('<h2> No Content Specified! </h2>');
	}
	m.data(opts.data);
	m.modal('show');
}


//------------ Prototype Extensions ----------------//
String.prototype.toProperCase = function(){
	return this
		.replace("_"," ")
		.replace(/(^[a-z]| [a-z])/g,
			function ($1){
				return $1.toUpperCase();
			}
		);
};

Date.prototype.toSQLDate = function() {
	var t = this;
	
	var date = [];
	date[0] = t.getFullYear().toString();
	date[1] = (t.getMonth() + 1).toString().padStart(2,'0');
	date[2] = t.getDate().toString().padStart(2,'0');
	
	var time = [];
	time[0] = t.getHours().toString().padStart(2,'0');
	time[1] = t.getMinutes().toString().padStart(2,'0');
	time[2] = t.getSeconds().toString().padStart(2,'0');
	
	return date.join('-') + ' ' + time.join(':');
}
