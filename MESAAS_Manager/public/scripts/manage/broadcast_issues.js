var processor = './ajax_processors/manage/broadcast_issues.php';
var table;
var systems;
var chosen_system;
var part_descriptions = [];
var current_line_BOMs = [];
var cur_BOM_actions = [];
var cur_BOM_components = [];
var cur_actions_from_preview = [];

$(document).ready(function () {
	load_systems({process_type: 'SEQUENCED'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	$('.btn-refresh').on('click', function() {
		load_issues();
	});

	$('#tbl_issues TBODY').on('click', 'BUTTON.btn-ack-issue', function() {
		var data = $(this).data();
		$.getJSON(processor,	{
				action: 'ack_issue',
				database: chosen_system.database_name,
				issue_ID: data.ID
			},	updateIssues
		);
	});

	$('#tbl_issues TBODY').on('click', 'BUTTON.btn-edit-comment', function() {
		var data = $(this).data();
		$('#div_comment_modal DIV.modal-header H4').html('Issue Comment for ' + data.broadcast_type + ' broadcast, VIN: ' + data.VIN);
		$('#inp_comment').val(data.fix_comment);
		$('#div_comment_modal').modal('show').data('issue_ID', data.ID);
	});

	$('#div_comment_modal').on('shown.bs.modal', function() {
		$('#inp_comment').focus();
	});

	$('.btn-save-comment').on('click', function() {
		$.getJSON(processor,	{
				action: 'save_comment',
				database: chosen_system.database_name,
				issue_ID: $('#div_comment_modal').data('issue_ID'),
				fix_comment: $('#inp_comment').val()
			},
			function(data) {
				$('#div_comment_modal').modal('hide');
				updateIssues(data);
			}
		);
	});

	$('#tbl_issues TBODY').on('click', 'BUTTON.btn-manage-bom', function() {
		cur_BOM_actions = [];
		var data = $(this).data();
		var components = getComponentsFromRawData(data.raw_data);
		cur_BOM_components = components;
		cur_actions_from_preview = [];
		$('.btn-load-actions-from-preview').hide();
		$('#div_manage_bom_modal').data(data);
		$('#inp_new_bom').prop('checked', false);

		$('#tbl_matching_bom TBODY').html('<tr><td colspan="2"><i>Select a matching BOM</i></td></tr>');
		$('#b_matching_bom').html('Matching BOM Components');

		populateBOMComponents(components);

		$.getJSON(processor, {action:"load_all_BOMs_for_line", line_ID:data.line_ID, database:chosen_system.database_name}, function(data) {
			current_line_BOMs = data;
			findMatchingBOMs(components);
			$('#div_manage_bom_modal').modal('show');
		});

		updateBOMActions();

		if (data.broadcast_type == 'BUILD') {
			$('#div_new_bom').show();
			$.getJSON(processor,	{
					action: 'get_issue_actions',
					VIN: data.VIN,
					line_ID: data.line_ID,
					database: chosen_system.database_name
				},	function(data) {
						cur_actions_from_preview = data;
						if (data.length > 0) {
							$('.btn-load-actions-from-preview').show();
						}
				}
			);
		} else {
			$('#div_new_bom').hide();
		}
	});

	$('#tbl_issues TBODY').on('click', 'BUTTON.btn-view-bom', function() {
		var data = $(this).data();
		var components = getComponentsFromRawData(data.raw_data);

		populateBOMComponentsView(components);
		$.getJSON(processor,	{
				action: 'get_all_issue_actions',
				issue_ID: data.ID,
				database: chosen_system.database_name
			},	function(data) {
					updateBOMActionsView(data);
					$('#div_view_bom_modal').modal('show');
			}
		);

	});

	$('#div_manage_bom_modal').on('shown.bs.modal', function() {
		$('#div_manage_bom_modal DIV.alert').show();
	});

	$('DIV.alert BUTTON.close').on('click', function() {
		$(this).closest('DIV.alert').hide();
	});

	$('#tbl_matching_BOMs TBODY').on('click', '.btn-select-matching-bom', function() {
		var data = $(this).data();

		$('#b_matching_bom').html('Matching BOM Components <span class="badge">' + data.bom_ID + '</span>');
		var t = $('#tbl_matching_bom TBODY').empty();
		var components = getComponentsFromBOMString(data.bom_string);
		var components_after_compare = compareArrays(cur_BOM_components, components, 'part_number', getRootPartNumber);
		for (var i = 0; i < components_after_compare.arr2.length; i++) {
			var o = components_after_compare.arr2[i];
			var r = $('<tr />').appendTo(t);
			var c = $('<td />').appendTo(r).html('<span class="part_number" ' +
															 '   data-pn="' + o.part_number + '" ' +
															 '   data-rootpn="' + getRootPartNumber(o.part_number) + '" ' +
															 '   data-rev="' + getRevLevel(o.part_number) + '" ' +
															 '   data-qty="' + o.qty + '">' +
															 '<span class="root_part_number">' + getRootPartNumber(o.part_number) + '</span>' +
															 '<span class="rev_level rev_' + o.part_number + '">' + getRevLevel(o.part_number) + '</span>' +
															 '&nbsp;</span>');
			var c = $('<td />').appendTo(r).html('<span class="qty" data-rootpn="' + getRootPartNumber(o.part_number) + '">' + (o.qty||'') + '</span>');
		}

		populateBOMComponents(components_after_compare.arr1);

		$('#tbl_current_bom TBODY TR TD SPAN.part_number').each(function(i, el) {
			var rootpn = $(el).attr('data-rootpn');
			var rev = $(el).attr('data-rev');
			var qty = $(el).attr('data-qty');

			//find matching part number in the
			var matching_rev = $('#tbl_matching_bom TBODY TR TD SPAN.part_number[data-rootpn=' + rootpn + ']').attr('data-rev');
			var matching_qty = $('#tbl_matching_bom TBODY TR TD SPAN.part_number[data-rootpn=' + rootpn + ']').attr('data-qty');

			if (rev != matching_rev) {
				$('#tbl_matching_bom TBODY TR TD SPAN.part_number[data-rootpn=' + rootpn + '] SPAN.rev_level').addClass('highlight');
			}
			if (qty != matching_qty) {
				$('#tbl_matching_bom TBODY TR TD SPAN.qty[data-rootpn=' + rootpn + ']').addClass('highlight');
			}
		});
		updateBOMActions();
		cur_actions_from_preview = [];
		$('.btn-load-actions-from-preview').hide();
	});


	$('#tbl_current_bom TBODY').on('click', '.btn-change-rev', function() {
		var data = $(this).data();
		$('#div_old_rev').html(getRevLevel(data.part_number));
		$('#inp_new_revision').val('');
		$('#div_change_rev_modal').modal('show').data(data);
	});

	$('#tbl_current_bom TBODY').on('mouseover', '.part_number', function() {
		$('#tbl_matching_bom SPAN.part_number[data-rootpn=' + $(this).data('rootpn') + ']').closest('TR').addClass('highlight2');
	});

	$('#tbl_current_bom TBODY').on('mouseout', '.part_number', function() {
		$('#tbl_matching_bom SPAN.part_number[data-rootpn=' + $(this).data('rootpn') + ']').closest('TR').removeClass('highlight2');
	});

	$('#tbl_matching_bom TBODY').on('mouseover', '.part_number', function() {
		$('#tbl_current_bom SPAN.part_number[data-rootpn=' + $(this).data('rootpn') + ']').closest('TR').addClass('highlight2');
	});

	$('#tbl_matching_bom TBODY').on('mouseout', '.part_number', function() {
		$('#tbl_current_bom SPAN.part_number[data-rootpn=' + $(this).data('rootpn') + ']').closest('TR').removeClass('highlight2');
	});

	$('#div_change_rev_modal').on('shown.bs.modal', function() {
		$('#inp_new_revision').focus();
	});

	$('.btn-save-rev-change').on('click', function() {
		var r = new RegExp('^[A-Z][A-Z]$');
		if (!r.test($('#inp_new_revision').val().toUpperCase())) {
			$.growl({
							title:"<b>Invalid Format</b><br>",
							message:"Invalid revision format.  Must be two characters."
						},
						{
							delay:5000,
							showProgressbar:true,
							type:"danger",
							z_index:10000
						});
			return;
		}
		cur_BOM_actions.push({part_number:$('#div_change_rev_modal').data('part_number'),
									 action_desc:'CHANGE REV TO: ' + $('#inp_new_revision').val().toUpperCase(),
									 action:'CHANGE_REV',
									 new_rev:$('#inp_new_revision').val().toUpperCase()
									});
		updateBOMActions();
		$('#div_change_rev_modal').modal('hide');
	});


	$('#tbl_current_bom TBODY').on('click', '.btn-change-qty', function() {
		var data = $(this).data();
		$('#div_old_qty').html(data.qty);
		$('#inp_new_qty').val('');
		$('#div_change_qty_modal').modal('show').data(data);
	});

	$('#div_change_qty_modal').on('shown.bs.modal', function() {
		$('#inp_new_qty').focus();
	});

	$('.btn-save-qty-change').on('click', function() {
		var r = new RegExp('^[0-9]$');
		if (!r.test($('#inp_new_qty').val())) {
			$.growl({
							title:"<b>Invalid Format</b><br>",
							message:"Invalid quantity format.  Must be one numeric character."
						},
						{
							delay:5000,
							showProgressbar:true,
							type:"danger",
							z_index:10000
						});
			return;
		}
		cur_BOM_actions.push({part_number:$('#div_change_qty_modal').data('part_number'),
									 action_desc:'CHANGE QTY TO: ' + $('#inp_new_qty').val(),
									 action:'CHANGE_QTY',
									 new_qty:$('#inp_new_qty').val()
									});
		updateBOMActions();
		$('#div_change_qty_modal').modal('hide');
	});


	$('#tbl_current_bom TBODY').on('click', '.btn-delete-part', function() {
		var data = $(this).data();
		$('#div_confirm_delete_modal').modal('show').data(data);
	});

	$('.btn-confirm-delete').on('click', function() {
		cur_BOM_actions.push({part_number:$('#div_confirm_delete_modal').data('part_number'),
									 action_desc:'REMOVE PART',
									 action:'REMOVE_PART'
									});
		updateBOMActions();
		$('#div_confirm_delete_modal').modal('hide');
	});


	$('#tbl_current_bom TBODY').on('click', '.btn-change-part', function() {
		var data = $(this).data();
		$('#div_old_part').html(data.part_number);
		$('#inp_new_part').val('');
		$('#div_change_part_modal').modal('show').data(data);
	});

	$('#div_change_part_modal').on('shown.bs.modal', function() {
		$('#inp_new_part').focus();
	});

	$('.btn-save-part-change').on('click', function() {
		var r1 = new RegExp('^[A-Z,0-9]{8}$');  //8-character part number
		var r2 = new RegExp('^[A-Z,0-9]{7}[A-Z][A-Z]$');	 //7 character, with rev
		var r3 = new RegExp('^[A-Z,0-9]{8}[A-Z][A-Z]$');
		var v = $('#inp_new_part').val().toUpperCase();
		if (!r1.test(v) && !r2.test(v) && !r3.test(v) ) {
			$.growl({
							title:"<b>Invalid Format</b><br>",
							message:"Invalid part number format.  Must be a valid part number."
						},
						{
							delay:5000,
							showProgressbar:true,
							type:"danger",
							z_index:10000
						});
			return;
		}
		cur_BOM_actions.push({part_number:$('#div_change_part_modal').data('part_number'),
									 action_desc:'CHANGE PART TO: ' + v,
									 action:'CHANGE_PART',
									 new_part:v
									});
		updateBOMActions();
		$('#div_change_part_modal').modal('hide');
	});


	$('#tbl_actions TBODY').on('click', '.btn-remove-action', function() {
		var data = $(this).data();
		cur_BOM_actions.splice(data.action_index, 1);
		updateBOMActions();
		$.growl('Action removed!');
	});

	$('.btn-load-actions-from-preview').on('click', function() {
		cur_BOM_actions = cur_actions_from_preview;
		updateBOMActions();
	});

	$('BUTTON.btn-mark-fixed').on('click', function() {
		event.preventDefault();
		saveActionsAndMarkFixed();
	});

	$('.tooltipper').tooltip({"html":true});
});

function load_filter_options () {
	// load filtering options based on the selected system
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_issues();
}

function load_issues() {
	var t = $('#tbl_issues TBODY').empty();
	var r = $('<tr />').appendTo(t);
	var c = $('<td />').appendTo(r).prop('colspan', 10).html('Loading issues....').addClass('text-center');
	$.getJSON(processor,	{
			action: 'get_issues',
			database: chosen_system.database_name
		},	updateIssues
	);
}

function updateIssues(jso) {
	var issues = jso.issues || [];
	part_descriptions = jso.part_descriptions || [];
	var t = $('#tbl_issues TBODY').empty();

	if (issues.length == 0) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).prop('colspan', 10).html('No issues found!').addClass('text-center');
	}
	for (var i = 0; i < issues.length; i++) {
		var issue = issues[i];
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html(issue.alert_time.date);
		var c = $('<td />').appendTo(r).html(issue.line_code);
		var c = $('<td />').appendTo(r).html(issue.broadcast_type);
		var c = $('<td />').appendTo(r).html(issue.broadcast_issue);
		var c = $('<td />').appendTo(r).html(issue.VIN);
		var c = $('<td />').appendTo(r).html(issue.sequence);
		var c = $('<td />').appendTo(r);
		if (issue.fixed_time == null) {
			if (issue.ack_time == null) {
				c.html('<span class="label label-danger">NOT FIXED.  NOT ACKNOWLEDGED.</span>');
			} else {
				c.html('<span class="label label-warning">ACKNOWLEDGED BY: ' + issue.ack_operator + '</span>');
			}
		} else {
			c.html('<span class="label label-success">FIXED BY: ' + issue.fix_operator + '</span>');
		}
		var c = $('<td />').appendTo(r).html(issue.fix_comment || '');

		var c = $('<td />').appendTo(r); //actions
		var btn_grp = $('<div />').addClass('btn-group').appendTo(c);
		var b = $('<button />').addClass('btn btn-xs btn-edit-comment').data(issue).appendTo(btn_grp).html('<span class="glyphicon glyphicon-comment"></span>').prop('title', 'Edit Comment: ' + (issue.fix_comment||''));
		if (issue.ack_time == null) {
			var b = $('<button />').addClass('btn btn-xs btn-ack-issue').data(issue).appendTo(btn_grp).html('<span class="glyphicon glyphicon-check"></span> Ack').prop('title', 'Acknowledge Issue');
		} else {
			if (issue.broadcast_issue == 'NEW BOM') {
				if (issue.fixed_time == null) {
					var b = $('<button />').addClass('btn btn-xs btn-manage-bom').data(issue).appendTo(btn_grp).html('<span class="glyphicon glyphicon-cog"></span> Manage BOM').prop('title', 'Manage New BOM');
				} else {
					var b = $('<button />').addClass('btn btn-xs btn-view-bom').data(issue).appendTo(btn_grp).html('<span class="glyphicon glyphicon-eye-open"></span> View BOM and Actions').prop('title', 'View BOM and Actions');
				}
			}
		}
	}
}

function updateBOMActions() {
	$('#tbl_current_bom TBODY TR').removeClass('changed');
	var t = $('#tbl_actions TBODY').empty();
	if (cur_BOM_actions.length == 0) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html('<i>No actions yet for this issue.</i>').prop('colspan', 3);
	}

	$('#tbl_current_bom TBODY TR TD BUTTON.btn').show();
	for (var i = 0; i < cur_BOM_actions.length; i++) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html(cur_BOM_actions[i].part_number);
		var c = $('<td />').appendTo(r).html(cur_BOM_actions[i].action_desc);
		var c = $('<td />').appendTo(r);
		var b = $('<button />').appendTo(c).addClass('btn btn-xs btn-warning btn-remove-action').html('<span class="glyphicon glyphicon-trash"></span>').prop('title', 'Remove Action').data('action_index', i);


		switch (cur_BOM_actions[i].action) {
			case 'CHANGE_PART':
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-change-part.pn_' + cur_BOM_actions[i].part_number).hide();
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-delete-part.pn_' + cur_BOM_actions[i].part_number).hide();
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-change-rev.pn_' + cur_BOM_actions[i].part_number).hide();
				break;
			case 'CHANGE_QTY':
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-change-qty.pn_' + cur_BOM_actions[i].part_number).hide();
				break;
			case 'CHANGE_REV':
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-change-part.pn_' + cur_BOM_actions[i].part_number).hide();
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-delete-part.pn_' + cur_BOM_actions[i].part_number).hide();
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.btn-change-rev.pn_' + cur_BOM_actions[i].part_number).hide();
				break;
			case 'REMOVE_PART':
				$('#tbl_current_bom TBODY TR TD BUTTON.btn.pn_' + cur_BOM_actions[i].part_number).hide();
				break;
		}

		$('#tbl_current_bom TBODY TR[data-pn=' + cur_BOM_actions[i].part_number + ']').addClass('changed');
	}
}

function updateBOMActionsView(actions) {
	var t = $('#tbl_view_actions TBODY').empty();
	if (actions.length == 0) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html('<i>No actions for this issue.</i>').prop('colspan', 3);
	}

	for (var i = 0; i < actions.length; i++) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html(actions[i].part_number);
		var c = $('<td />').appendTo(r).html(actions[i].action_desc);
	}
}

function populateBOMComponents(components) {
	var t = $('#tbl_current_bom TBODY').empty();
	for (var i = 0; i < components.length; i++) {
		var o = components[i];
		var r = $('<tr />').appendTo(t).attr('data-pn', o.part_number);
		var c = $('<td />').appendTo(r)
			.html('<span class="part_number pn_source pn_' + o.part_number + '" ' +
					'   data-pn="' + o.part_number + '" ' +
					'   data-rootpn="' + getRootPartNumber(o.part_number) + '" ' +
					'   data-rev="' + getRevLevel(o.part_number) + '" ' +
					'   data-qty="' + o.qty + '">' +
					'<span class="root_part_number root_' + getRootPartNumber(o.part_number) + '">' + getRootPartNumber(o.part_number) + '</span>' +
					'<span class="rev_level rev_' + o.part_number + '">' + getRevLevel(o.part_number) + '</span>' +
					'</span>')
			.prop('title', findPartDesc(o.part_number));
		var c = $('<td />').appendTo(r).html(o.qty);
		var c = $('<td />').appendTo(r);
		if (o.part_number != '') {
			var btn_grp = $('<div />').addClass('btn-group').appendTo(c);
			var b = $('<button />').addClass('btn btn-xs btn-change-part pn_' + o.part_number).html('<span class="glyphicon glyphicon-pencil"></span>').prop('title', 'Edit part number').appendTo(btn_grp).data(o);
			var b = $('<button />').addClass('btn btn-xs btn-delete-part pn_' + o.part_number).html('<span class="glyphicon glyphicon-trash"></span>').prop('title', 'Delete this part').appendTo(btn_grp).data(o);
			var b = $('<button />').addClass('btn btn-xs btn-change-qty pn_' + o.part_number).html('<span class="glyphicon glyphicon-equalizer"></span>').prop('title', 'Change Quantity').appendTo(btn_grp).data(o);
			var b = $('<button />').addClass('btn btn-xs btn-change-rev pn_' + o.part_number).html('<span class="glyphicon glyphicon-transfer"></span>').prop('title', 'Change Revision').appendTo(btn_grp).data(o);
		} else {
			c.html('&nbsp;');
		}
	}
}

function populateBOMComponentsView(components) {
	var t = $('#tbl_view_current_bom TBODY').empty();
	for (var i = 0; i < components.length; i++) {
		var o = components[i];
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r)
			.html('<span>' + o.part_number + '</span>')
			.prop('title', findPartDesc(o.part_number));
		var c = $('<td />').appendTo(r).html(o.qty);
	}
}

function saveActionsAndMarkFixed() {
	var d = {"action":"save_and_mark_fixed",
				"issue_ID":$('#div_manage_bom_modal').data('ID'),
				"BOM_actions":cur_BOM_actions,
				"create_new_BOM_ID":(($('#inp_new_bom').is(':checked'))?1:0)
				};
	$.getJSON(processor, d, function(data) {
		$('#div_manage_bom_modal').modal('hide');
		if (data.success == 1) {
			if (data.create_new_BOM_ID == 1) {
				if (data.existing_bom_ID > 0) {
					$.growl({"title":"Issue Resolved!",
						"message":"You have marked the issue resolved and any actions against the broadcast have been saved.  However, an existing BOM ID (" + data.existing_bom_ID + ") was already found for your configuration.",
						"delay":0,
						"type":"warning"});
				} else {
					$.growl({"title":"Issue Resolved!",
						"message":"You have marked the issue resolved and any actions against the broadcast have been saved.  New BOM ID (" + data.new_bom_ID + ") was created.",
						"delay":0,
						"type":"success"});
				}
			} else {
				$.growl({"title":"Issue Resolved!", "message":"You have marked the issue resolved and any actions against the broadcast have been saved.", "type":"success"});
			}
		} else {
			$.growl({"title":"Issue NOT Resolved!",
						"message":data.msg,
						"delay":0,
						"type":"danger"});
		}
	});

}


function getComponentsFromRawData(raw_data) {
	var num_sales_codes = parseInt(raw_data.substring(41, 44));
	console.log('Module sales codes: ' + num_sales_codes);
	var component_count_start_pos = 44 + (num_sales_codes * 3);
	var num_components = parseInt(raw_data.substring(component_count_start_pos, component_count_start_pos + 4));
	console.log('Num components: ' + num_components);
	var components = [];
	for (var i = 0; i < num_components; i++) {
		var component_start_pos = component_count_start_pos + 4 + (i * 18);

		components.push({
			"part_number":raw_data.substring(component_start_pos, component_start_pos + 10),
			"qty":parseInt(raw_data.substring(component_start_pos + 10, component_start_pos + 18))
		});
	}
	console.log(components);
	return components;
}

function getComponentsFromBOMString(raw_data) {
	var components = [];
	for (var i = 0; i < raw_data.length; i+=18) {
		components.push({
			"part_number":raw_data.substring(i, i + 10),
			"qty":parseInt(raw_data.substring(i + 10, i + 18))
		});
	}
	return components;
}

function findPartDesc(part_number) {
	var res = $.grep(part_descriptions, function(el, i) {
		return el.part_number == part_number;
	});

	if (res.length > 0) {
		return res[0].part_desc;
	} else {
		return '';
	}
}

function findMatchingBOMs(components) {
	var matches = [];
	for (var i = 0; i < current_line_BOMs.length; i++) {
		var BOM_components = getComponentsFromBOMString(current_line_BOMs[i].bom_string);
		var this_BOM_match_count = 0;
		for (var j = 0; j < components.length; j++) {
			for (var k = 0; k < BOM_components.length; k++) {
				if (getRootPartNumber(components[j].part_number) == getRootPartNumber(BOM_components[k].part_number)) {
					this_BOM_match_count++;
				}
			}
		}

		var num_comp = BOM_components.length;
		if (components.length > num_comp) {
			num_comp = components.length;
		}

		if (this_BOM_match_count > (BOM_components.length * 0.5)) {
			matches.push({bom_string:current_line_BOMs[i].bom_string, bom_ID:current_line_BOMs[i].bom_ID, match_percentage:(this_BOM_match_count/num_comp*100)});
		}
	}

	matches.sort(function(a,b) {
		if (a.match_percentage < b.match_percentage)
			return 1;
		else if (a.match_percentage > b.match_percentage)
			return -1;
		else
			return 0;
	});

	var t = $('#tbl_matching_BOMs TBODY').empty();
	for (var i = 0; i < matches.length; i++) {
		var r = $('<tr />').appendTo(t);
		var c = $('<td />').appendTo(r).html(matches[i].bom_ID);
		var c = $('<td />').appendTo(r).html(matches[i].match_percentage.toFixed(2) + '%');
		var c = $('<td />').appendTo(r);
		var b = $('<button />').addClass('btn btn-xs btn-select-matching-bom').html('<span class="glyphicon glyphicon-eye-open"></span> View').appendTo(c).data(matches[i]);
	}
}

function findActionForPart(part_number, action) {
	for (var i = 0; i < cur_BOM_actions.length; i++) {
		if (cur_BOM_actions[i].part_number == part_number && cur_BOM_actions[i].action == action) {
			return true;
		}
	}
	return false;
}

function getRootPartNumber(part_number) {
	if (part_number.length == 8) {
		return part_number;
	} else {
		return part_number.substring(0, part_number.length - 2);
	}
}

function getRevLevel(part_number) {
	if (part_number.length == 8) {
		return '';
	} else {
		return part_number.substr(-2);
	}
}

function test() {
	var arr1 = [{"val":"A"}, {"val":"B"}, {"val":"C"}];
	var arr2 = [{"val":"A"}, {"val":"C"}, {"val":"D"}];

	var arr1a = compareArrays(arr1, arr2, 'val', null);

	console.log(arr1a);
}

//this takes two arrays (of objects), and returns an object with two arrays (of objects) that shows the differences between them
//can also optionally pass a function to apply to the values
function compareArrays(arr1, arr2, matching_field, apply_function) {
	var ret = {"arr1":[], "arr2":arr2};

	for (var i = 0; i < arr1.length; i++) {
		var curval = arr1[i][matching_field];
		if (apply_function) {
			curval = apply_function(curval);
		}
		ret.arr1.push(arr1[i]);
		if (findInArray(arr2, matching_field, curval, apply_function) < 0) {
			var o = {};
			o[matching_field] = '';
			ret.arr2.splice(i, 0, o);
		}
	}

	for (var i = 0; i < arr2.length; i++) {
		var curval = arr2[i][matching_field];
		if (apply_function) {
			curval = apply_function(curval);
		}
		if (curval != '') {
			if (findInArray(arr1, matching_field, curval, apply_function) < 0) {
				var o = {};
				o[matching_field] = '';
				ret.arr1.splice(i, 0, o);
			}
		}
	}
	return ret;
}

//finds a value in an array of objects.  Optionally applies a function to each field value
function findInArray(arr, field, val, apply_function) {
	for (var i = 0; i < arr.length; i++) {
		var curval = arr[i][field];
		if (apply_function) {
			curval = apply_function(curval);
		}
		if (curval == val) {
			return i;
		}
	}
	return -1;
}