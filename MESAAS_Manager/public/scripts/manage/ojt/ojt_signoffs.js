var processor = './ajax_processors/manage/ojt/ojt_signoffs.php';
var departments = [];
var areas = [];

$(document).ready(
	function() {		
		loadDepartmentsAndAreas();
		
		$('#selArea').on('change', loadAreaOperators);
		$('#sel_operator').on('change', function() { $('#btn_load').attr('disabled', false); });
		
		$('#btn_load').on('click', loadOJT);
		
		
		$('#div_modal_record').on('shown.bs.modal', function (e) {
			scanBadge();
		});
		

		$('#txt_badge_scan').on('keypress', function(e) {
			var el = $(e.target);
			if (e.which == 13) {
				el.prop('disabled', true);
		
				var stage = parseInt(el.data('stage'));
				switch (stage) {
					case 1:
						evalOperator(el.data(), e.target.value);
						break;
					case 2:
						evalTrainer(el.data(), e.target.value);
						break;
					case 3:
						evalSupervisor(el.data(), e.target.value);
						break;
				}
		
			}
		});

	}
);

function loadDepartmentsAndAreas() {
	$.getJSON(processor, {"action":"get_departments_and_areas"}, populateDepartmentsAndAreas);
}

function populateDepartmentsAndAreas(jso) {
	departments = jso.departments;
	areas = jso.areas;
	
	var s = $('#selArea').html('<option></option>').attr('disabled', false);
	for (var i = 0; i < departments.length; i++) {
		for (var j = 0; j < areas.length; j++) {
			if (departments[i].ID == areas[j].dept_ID) {
				var op = $('<OPTION />');
				op.val(areas[j].ID);
				op.html(departments[i].department_name + ': ' + areas[j].area_desc);
		
				s.append(op);
			}
		}
	}
}

function loadAreaOperators() {
	$('#sel_operator').attr('disabled', true);
	$.getJSON(processor, {"action":"get_operators", "area_ID":$('#selArea').val()}, function(res) {
			var s = $('#sel_operator')[0];
			s.options.length = 0;

			var o = document.createElement('OPTION');
			s.options.add(o);

			for (var i = 0; i < res.length; i++) {
				var o = document.createElement('OPTION');
				o.value = res[i].ID;
				o.text = res[i].name;

				s.options.add(o);
			}

			s.disabled = false;

			$('#btn_load').attr('disabled', true);
		});
}


function loadOJT() {
	var pb;
	$('#div_cont_pb').show();
	pb = setInterval(function() {
					var w = parseInt(document.getElementById('div_pb').style.width);
					if (w > 100) {w = -5;}
					document.getElementById('div_pb').style.width= (w + 5) +'%';
				}, 1000);

	$.getJSON(processor, {"action":"get_ojt", "area_ID":$('#selArea').val(), "operator_ID":$('#sel_operator').val()},
		function(res) {
			$('#p_quals_info').html('<i>Qualifications for: <strong>' + res.operator[0].name + '</strong> in area: <strong>' + (res.area[0].area_name||res.area[0].area_desc) + '</strong></i>');
			createOJTTable(res);
			$('#div_cont_pb').hide();
			$('#div_qual_container').show();
			$('.tooltipper').tooltip();
			$('.jobname').tooltip({"placement":"left"});
			$('.tooltipper').click(function(e){e.preventDefault();});
			$('.record_ojt').click(getSignoff);
		});
}

function createOJTTable(jso) {
	
	jso.jobs = jso.jobs || [];
	jso.quals = jso.quals || [];
	jso.levels = jso.levels || [];
	
	var t = $('#tbl_main')[0];
	$("#tbl_main tr").remove();

	//plant-wide
		var r = t.insertRow(-1);
		var c = r.insertCell(-1);
		c.innerHTML = '<h4>Plant-Wide Jobs</h4>';
		c.className = 'text-center';
		c.colSpan = 5;

		var num_jobs = 0;
		for (var i = 0; i < jso.jobs.length; i++) {
			if (jso.jobs[i].area_ID == null && jso.jobs[i].department_ID == null) {
				addJobRow(t, jso, i);
				num_jobs++;
			}
		}

		if (num_jobs == 0) {
			var r = t.insertRow(-1);
			var c = r.insertCell(-1);
			c.innerHTML = '<i>No plant-wide jobs defined.</i>';
			c.className = 'text-center';
			c.colSpan = 5;
		}

	//department-wide
		var r = t.insertRow(-1);
		var c = r.insertCell(-1);
		c.innerHTML = '<h4>Department-Wide Jobs</h4>';
		c.className = 'text-center';
		c.colSpan = 5;

		num_jobs = 0;
		for (var i = 0; i < jso.jobs.length; i++) {
			if (jso.jobs[i].area_ID == null && jso.jobs[i].department_ID != null) {
				addJobRow(t, jso, i);
				num_jobs++;
			}
		}

		if (num_jobs == 0) {
			var r = t.insertRow(-1);
			var c = r.insertCell(-1);
			c.innerHTML = '<i>No department-wide jobs defined.</i>';
			c.className = 'text-center';
			c.colSpan = 5;
		}

	// //area-specific
	// 	var r = t.insertRow(-1);
	// 	var c = r.insertCell(-1);
	// 	c.innerHTML = '<h4>Area-Specific Jobs</h4>';
	// 	c.className = 'text-center';
	// 	c.colSpan = 5;

	// 	num_jobs = 0;
	// 	for (var i = 0; i < jso.jobs.length; i++) {
	// 		if (jso.jobs[i].area_ID != null) {
	// 			addJobRow(t, jso, i);
	// 			num_jobs++;
	// 		}
	// 	}

	// 	if (num_jobs == 0) {
	// 		var r = t.insertRow(-1);
	// 		var c = r.insertCell(-1);
	// 		c.innerHTML = '<i>No area-specific jobs defined.</i>';
	// 		c.className = 'text-center';
	// 		c.colSpan = 5;
	// 	}

	//area-specific
		var r = t.insertRow(-1);
		var c = r.insertCell(-1);
		c.innerHTML = '<h4>Area-Specific Jobs</h4>';
		c.className = 'text-center';
		c.colSpan = 5;

		num_jobs = 0;
		for (var i = 0; i < jso.jobs.length; i++) {
			if (jso.jobs[i].area_ID != null && jso.jobs[i].sub_area_ID == null) {
				addJobRow(t, jso, i);
				num_jobs++;
			}
		}

		if (num_jobs == 0) {
			var r = t.insertRow(-1);
			var c = r.insertCell(-1);
			c.innerHTML = '<i>No area-specific jobs defined.</i>';
			c.className = 'text-center';
			c.colSpan = 5;
		}

	//sub-area-specific
		var r = t.insertRow(-1);
		var c = r.insertCell(-1);
		c.innerHTML = '<h4>Group-Specific Jobs</h4>';
		c.className = 'text-center';
		c.colSpan = 5;

		num_jobs = 0;
		for (var i = 0; i < jso.jobs.length; i++) {
			if (jso.jobs[i].sub_area_ID != null) {
				addJobRow(t, jso, i);
				num_jobs++;
			}
		}

		if (num_jobs == 0) {
			var r = t.insertRow(-1);
			var c = r.insertCell(-1);
			c.innerHTML = '<i>No group-specific jobs defined.</i>';
			c.className = 'text-center';
			c.colSpan = 5;
		}

	//header
		var h = t.createTHead();
		var r = h.insertRow(-1);
		var th = document.createElement('TH');
		r.appendChild(th);
		th.innerHTML = 'Job Name';
		th.rowSpan = 2;

		var th = document.createElement('TH');
		r.appendChild(th);
		th.innerHTML = 'Qualification Levels';
		th.colSpan = 4;

		var r = h.insertRow(-1);
		for (var i = 0; i < jso.levels.length; i++) {
			var th = document.createElement('TH');
			th.className = 'level-' + (i + 1);
			r.appendChild(th);
			th.innerHTML = jso.levels[i].level_desc;
		}
}

// function addJobRow(t, jso, i) {
// 	var r = t.insertRow(-1);
// 	var c = r.insertCell(-1);
// 	c.innerHTML = '<span class="jobname" title="' + jso.jobs[i].job_details + '">' + jso.jobs[i].job_name + '</span>';

// 	var qual_level = 0;
// 	for (var j = 0; j < jso.quals.length; j++) {
// 		if (jso.quals[j].ojt_job_ID == jso.jobs[i].ID) {
// 			var c = r.insertCell(-1);
// 			c.innerHTML = '<a href="#" class="btn tooltipper btn-success btn-xs" data-toggle="tooltip" title="Trained By: ' + jso.quals[j].trainer_name + ', Supervisor: ' + jso.quals[j].supervisor_name + '"><span class="glyphicon glyphicon-ok"></span> ' + phpDate(jso.quals[j].the_date) + '</a>';
// 			c.className = 'text-center';
// 			qual_level = jso.quals[j].ojt_level;
// 		}
// 	}

// 	for (var j = qual_level; j < 4; j++) {
// 		var c = r.insertCell(-1);
// 		c.className = 'text-center';
// 		if (j == qual_level) {
// 			//input
// 			c.innerHTML = '<a href="#" class="btn btn-primary btn-xs record_ojt tooltipper" title="Record OJT for this job and qualification level." ' +
// 												  '	data-jobname="' + jso.jobs[i].job_name + '" ' +
// 												  '	data-opname="' + jso.operator[0].name + '" ' +
// 												  '	data-opid="' + jso.operator[0].ID + '" ' +
// 												  '	data-level="' + (qual_level + 1) + '" ' +
// 												  '	data-opbadge="' + (jso.operator[0].badgeID||jso.operator[0].badge_ID) + '" ' +
// 												  '	data-jobid="' + jso.jobs[i].ID + '" ' +
// 												  '	data-stage=1><span class="glyphicon glyphicon-pencil"></span> Record OJT</a>';
// 		} else {
// 			c.innerHTML = '&nbsp;';
// 		}
// 	}
// }

function addJobRow(t, jso, i) {
	var r = t.insertRow(-1);
	var c = r.insertCell(-1);

	var sub_area_desc = jso.jobs[i].sub_area_desc;
	if (sub_area_desc) {
		c.innerHTML = '<span class="jobname" title="' + jso.jobs[i].job_details + '">' + jso.jobs[i].job_name + ' (' + sub_area_desc + ')</span>';
	} else {
		c.innerHTML = '<span class="jobname" title="' + jso.jobs[i].job_details + '">' + jso.jobs[i].job_name + '</span>';
	}
	

	var qual_level = 0;
	for (var j = 0; j < jso.quals.length; j++) {
		if (jso.quals[j].ojt_job_ID == jso.jobs[i].ID) {
			var c = r.insertCell(-1);
			c.innerHTML = '<a href="#" class="btn tooltipper btn-success" data-toggle="tooltip" title="Trained By: ' + jso.quals[j].trainer_name + ', Supervisor: ' + jso.quals[j].supervisor_name + '"><span class="glyphicon glyphicon-ok"></span> ' + phpDate(jso.quals[j].the_date) + '</a>';
			c.className = 'text-center';
			qual_level = jso.quals[j].ojt_level;
		}
	}

	for (var j = qual_level; j < 4; j++) {
		var c = r.insertCell(-1);
		c.className = 'text-center';
		if (j == qual_level) {
			//input
			c.innerHTML = '<a href="#" class="btn btn-primary record_ojt tooltipper" title="Record OJT for this job and qualification level." ' +
												  '	data-jobname="' + jso.jobs[i].job_name + '" ' +
												  '	data-opname="' + jso.operator[0].name + '" ' +
												  '	data-opid="' + jso.operator[0].ID + '" ' +
												  '	data-level="' + (qual_level + 1) + '" ' +
												  '	data-opbadge="' + jso.operator[0].badgeID + '" ' +
												  '	data-jobid="' + jso.jobs[i].ID + '" ' +
												  '	data-stage=1><span class="glyphicon glyphicon-pencil"></span> Record OJT</a>';
		} else {
			c.innerHTML = '&nbsp;';
		}
	}
}

function getSignoff(e) {
	var el = $(e.target);
	$('#p_instr').html('Please scan <strong>operator</strong> badge to certify training was recieved: <span class="badge">1 of 3</span>');
	$('#sp_jobname').html(el.data('jobname'));
	$('#sp_opname').html(el.data('opname'));
	$('#div_modal_record').modal();
	$('#txt_badge_scan').data(el.data());
}

function scanBadge() {
	$('#txt_badge_scan')[0].value = '';
	$('#txt_badge_scan').prop('disabled', false);
	$('#txt_badge_scan')[0].focus();
}

function evalOperator(data, scan) {
	if (data.opbadge == scan || data.opid == scan) {
		//now get trainer scan
		$('#txt_badge_scan').data('stage', 2);
		$('#txt_badge_scan').data('op_scan', scan);
		$('#p_instr').html('Please scan <strong>trainer</strong> badge to certify training was recieved: <span class="badge">2 of 3</span>');
		scanBadge();
		$('#div_recording_info').hide();
	} else {
		$('#div_recording_info').html('Operator scan failed').show().attr("class", "alert alert-danger");
		scanBadge();
	}
}

function evalTrainer(data, scan) {
	if (scan == data.op_scan) {
		$('#div_recording_info').html('Trainer scan failed.  Operator cannot train themself.').show().attr("class", "alert alert-danger");
		scanBadge();
		return;
	}

	$.getJSON(processor, {"action":"get_op_info", "scan":scan},
		function(res) {
			if (res.length > 0) {
				//if (res[0].permission_level >= 1) {
				if (res[0].roles.indexOf('trainer') > -1) {
					//now get supervisor scan
					$('#txt_badge_scan').data('stage', 3);
					$('#txt_badge_scan').data('trainerid', res[0].ID);
					$('#p_instr').html('Please scan <strong>supervisor</strong> badge to certify training was recieved: <span class="badge">3 of 3</span>');
					scanBadge();
					$('#div_recording_info').hide();
				} else {
					$('#div_recording_info').html('Trainer scan failed.  Operator doesn\'t have permissions as a trainer.').show().attr("class", "alert alert-danger");
					scanBadge();
				}
			} else {
				$('#div_recording_info').html('Trainer scan failed.  Invalid badge scan.').show().attr("class", "alert alert-danger");
				scanBadge();
			}
		});
}

function evalSupervisor(data, scan) {
	$.getJSON(processor, {"action":"get_op_info", "scan":scan},
		function(res) {
			if (res.length > 0) {
				//if (res[0].permission_level >= 2) {
				if (res[0].roles.indexOf('supervisor') > -1) {
					//now get supervisor scan
					$('#txt_badge_scan').data('stage', 3);
					$('#txt_badge_scan').data('supervisorid', res[0].ID);
					recordSignoff($('#txt_badge_scan').data());
				} else {
					$('#div_recording_info').html('Supervisor scan failed.  Operator doesn\'t have permissions as a supervisor.').show().attr("class", "alert alert-danger");
					scanBadge();
				}
			} else {
				$('#div_recording_info').html('Supervisor scan failed.  Invalid badge scan.').show().attr("class", "alert alert-danger");
				scanBadge();
			}
		});
}

function recordSignoff(data) {
	$('#div_recording_info').html('Recording OJT for job: ' + data.jobname + '...').show().attr("class", "alert alert-info");
	data['action'] = 'record_ojt';
	delete data['bs.tooltip'];
	$.getJSON(processor, data,
		function(res) {
			$('#div_recording_info').html('OJT signoff recorded!').show().attr("class", "alert alert-success");

			setTimeout(function() {
				$('#div_modal_record').modal('hide');
				loadOJT();
			},
			2000);
		});
}

function phpDate(d) {
	if (d.date) {
		d = d.date;
	}
	
	return Date.Format(d, 'mm/dd/yyyy');
}