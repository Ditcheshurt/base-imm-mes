$.extend(NYSUS.OJT, {
	selected_operator_ID: null,
	selected_operator_name: null,
	use_datatables: true,
	restrict_to_login_roles: true,
	with_alerts: false,
	operator_signoffs: null,

	init: function () {
		var container = $('.panel-signoffs .panel-body');
		var sel_operator = $('.sel_operators');
		var btn_refresh = $('.btn-refresh');
		var btn_show_alerts = $('.btn-show-alerts');

		app.render_operator_combo();

		sel_operator.on('change', function () {
			app.selected_operator_ID = $(this).val();
			app.selected_operator_name = $(this).select2('data')[0].text;
			app.render();
		});

		btn_refresh.on('click', function () {
			app.render();
		});

		container.on('change', 'table tbody input[type="checkbox"]', function (e) {
			e.preventDefault();
			var that = $(this);
			var data = that.data();

			if (data.ojt_signoff_id == undefined || data.ojt_signoff_id == null) {
				app.create_signoff(data.requirement_id, data.operator_id, LOGIN.operator.ID, data.ojt_signoff_template_levels_roles_id, function (jso) {
					//todo add growl
					that.data('ojt_signoff_id', jso[0].ID);
					that.attr("data-ojt_signoff_id", jso[0].ID);
				});

			} else {
				app.delete_signoff(data.ojt_signoff_id, LOGIN.operator.ID, function (jso) {
					that.data('ojt_signoff_id', null);
					that.attr("data-ojt_signoff_id", null);
				});
			}

		});

		btn_show_alerts.on('click', function () {
			app.with_alerts = app.with_alerts != true;
			if (app.with_alerts) {
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				$(this).addClass('btn-danger').removeClass('btn-success');
				$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
			app.render();
		});

		container.on('click', 'td[data-image_url]', function () {
			var image_url = $(this).data().image_url;

			if (image_url == '') {
				return;
			}

			if (image_url != null) {
				if (image_url.indexOf('.pdf') >= 0) {
					$('.requirement_preview').empty().append(
						'<iframe style="width:100%; height:600px" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + image_url + '"></iframe>');
				} else {
					$('.requirement_preview').empty().append('<img class="img-responsive" src="' + image_url + '" />');
				}
				$('.modal-preview').modal('show');
			}

		});
	},

	render_operator_combo: function (cb) {
		var app = NYSUS;
		var sel_operators = $('.sel_operators');

		sel_operators.empty().append('<option></option>');
		app.get_operators(false, function (jso) {
			$.map(jso, function (v) {
				sel_operators.append('<option value="' + v.ID + '">' + v.badge_ID + ': ' + v.last_name + ', ' + v.first_name + '</option>');
			});
		});

		sel_operators.select2({placeholder: 'Operators', width:'100%'});

		if (typeof cb == 'function') {
			cb();
		}

	},

	render: function () {
		var container = $('.panel-signoffs .panel-body');

		container.empty().append('<h3>Operator ' + app.selected_operator_name + '</h3>');

		app.get_operator_signoffs(app.selected_operator_ID, app.with_alerts, function (jso) {
			app.operator_signoffs = jso;

			app.get_operator_requirements(app.selected_operator_ID, app.with_alerts, function (jso) {
				var unique_templates = {};
				var templates = [];

				$.map(jso, function (template) {
					if (typeof(unique_templates[template.ojt_signoff_template_ID]) == "undefined") {
						// get template reqs
						var unique_req = {};
						var requirements = [];

						$.map(jso, function (v) {
							if (typeof(unique_req[v.ojt_requirement_ID]) == "undefined") {
								if (v.ojt_signoff_template_ID == template.ojt_signoff_template_ID) {
									requirements.push(v);
								}
							}
							unique_req[v.ojt_requirement_ID] = 0;
						});

						templates.push({
							ojt_signoff_template_ID: template.ojt_signoff_template_ID,
							template_description: template.template_description,
							requirements: requirements
						});

					}

					unique_templates[template.ojt_signoff_template_ID] = 0;
				});

				if (templates.length == 0) {
					var container = $('.panel-signoffs .panel-body');
					var table = $('<table class="table table-condensed table-responsive table-bordered table-signoffs table-hover" style="width:100%"><colgroup><col span="1"></colgroup><thead></thead><tbody></tbody></table>');
					var thead = table.find('thead');
					var tbody = table.find('tbody');

					thead.append('<tr><td>Results</td></tr>');
					tbody.append('<tr><td>Operator has no results</td></tr>');
					container.append(table);
				} else {
					$.map(templates, function (template) {
						app.build_table(template);
					});
				}

			});

		});

	},

	build_table: function (template) {
		var container = $('.panel-signoffs .panel-body');
		var table = $('<table class="table table-condensed table-responsive table-bordered table-signoffs table-hover" style="width:100%"><colgroup><col span="1"></colgroup><thead></thead><tbody></tbody></table>');
		var thead = table.find('thead');
		var tbody = table.find('tbody');
		var hr1 = $('<tr>');
		var hr2 = $('<tr>');
		var colgroup = table.find('colgroup');

		hr1.append('<th>' + template.template_description  + ' </th>');
		hr2.append('<th>Requirement</th>');

		// build the header rows
		app.get_template_roles(template.ojt_signoff_template_ID, function (jso) {
			var unique_levels = {};
			var levels = [];

			// groom the data
			$.map(jso, function (v) {

				// get unique levels
				if (typeof(unique_levels[v.level_description]) == "undefined") {
					levels.push({
						level_order: v.level_order,
						level_description: v.level_description,
						roles: $.grep(jso, function (r) {
							return r.level_description == v.level_description;
						})
					});
				}
				unique_levels[v.level_description] = 0;

			});

			template.levels = levels;

			$.map(template.levels, function (l) {
				colgroup.append('<col span="' + l.roles.length + '">');
				hr1.append('<th colspan="' + l.roles.length + '">' + l.level_description + '</th>');
				$.map(l.roles, function (r) {
					hr2.append('<th>' + r.role_description + '</th>');
				});
			});

			// build the body
			$.map(template.requirements, function (req) {
				var tr = $('<tr>');

				tr.append('<td data-requirement_id="' + req.ojt_requirement_ID + '" data-image_url="' + req.image_url + '">' + req.requirement_description + '</td>');

				$.map(template.levels, function (level) {
					$.map(level.roles, function (role) {
						var checked = '';
						var ojt_signoff_ID = '';

						// get the signoff if it exists
						var signoff = $.grep(app.operator_signoffs, function (v) {
							return req.ojt_requirement_ID == v.requirement_ID && v.signoff_level_role_ID == role.signoff_level_role_ID;
						});

						if (signoff.length > 0) {
							checked = 'checked';
							ojt_signoff_ID = 'data-ojt_signoff_ID="' + signoff[0].ojt_signoff_ID + '"';
						}

						tr.append('<td><label><input type="checkbox"' + ojt_signoff_ID +
							'data-ojt_signoff_template_levels_roles_ID="' +
							role.signoff_level_role_ID + '" data-role_name="' +
							role.role_description + '" data-operator_ID="' +
							app.selected_operator_ID + '" data-requirement_ID="' +
							req.ojt_requirement_ID + '" ' +
							checked +
							'/></label></td>');
					});
				});

				tbody.append(tr);
			});

			thead.append(hr1).append(hr2);

			container.append(table).append('<br>');

			app.restrict_checkboxes();

			if (app.use_datatables) {
				if (table.find('tbody tr').size() > app.datatables_options.lengthMenu[0][0]) {
					app.datatables_options.paging = true;
					app.datatables_options.info = true;
				} else {
					app.datatables_options.paging = false;
					app.datatables_options.info = false;
				}
				table.DataTable(app.datatables_options);

			}

		});

	},

	restrict_checkboxes: function () {
		if (app.config.ojt_restrict_to_login_roles) {
			$('.tbl-signoffs input[type="checkbox"]').prop('disabled', true);
			if (LOGIN) {
				var arrRoles = [];
				$.map(LOGIN.operator_roles, function (v) {
					arrRoles.push(v.role);
				});

				$.map($('.tbl-signoffs input[type="checkbox"]'), function (v) {
					var v = $(v);
					var name = v.data().role_name;
					if ($.inArray(name, arrRoles) != -1) {
						v.prop('disabled', false);
					}
				});
			}
		}
	},

	load_app_config: function (cb) {
		$.getJSON('./config/app_config.json', function (jso) {
			if (typeof cb == 'function') {
				cb(jso);
			}
		});
	}
});

var app = NYSUS.OJT;
$(document).ready(function () {

	// load config
	app.load_app_config(function (jso) {
		app.config = jso;

		app.init();
	});

});