$.extend(NYSUS.OJT, {
	restrict_to_login_roles: true,
	with_alerts: false,
	with_inactive_groups: false,
	selected_ojt_requirement_ID: null,
	selected_requirement_description: null,
	selected_ojt_group_ID: null,
	selected_ojt_group_description: null,
	group_requirements: null,
	use_datatables: true,
	config: null,

	init: function () {
		var container = $('.panel-signoffs .panel-body');
		var sel_req = $('.sel_requirements');
		var btn_refresh = $('.btn-refresh');
		var btn_alerts = $('.btn-show-alerts');
		var btn_preview = $('.btn-preview');
		var btn_show_active = $('.btn-groups-show-active');

		btn_refresh.on('click', function () {
			app.render();
		});

		sel_req.on('change', function () {
			app.selected_ojt_requirement_ID = $(this).val();
			app.selected_requirement_description = $(this).select2('data')[0].text;
			app.render();
		});

		container.on('change', 'table tbody input[type="checkbox"]', function (e) {
			e.preventDefault();
			var that = $(this);
			var data = that.data();

			if (data.ojt_signoff_id == undefined || data.ojt_signoff_id == null) {
				app.create_signoff(app.selected_ojt_requirement_ID, data.operator_id, LOGIN.operator.ID, data.ojt_signoff_template_levels_roles_id, function (jso) {
					//todo add growl
					that.data('ojt_signoff_id', jso[0].ID);
					that.attr("data-ojt_signoff_id", jso[0].ID);
					app.set_header_checkboxes();
				});

			} else {
				app.delete_signoff(data.ojt_signoff_id, LOGIN.operator.ID, function (jso) {
					that.data('ojt_signoff_id', null);
					that.attr("data-ojt_signoff_id", null);
					app.set_header_checkboxes();
				});
			}

		});

		btn_alerts.on('click', function () {
			container.empty();

			app.with_alerts = app.with_alerts != true;
			if (app.with_alerts) {
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				$(this).addClass('btn-danger').removeClass('btn-success');
				$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
			app.init_requirement_combo();

		});

		btn_preview.on('click', function () {
			app.get_requirement(app.selected_ojt_requirement_ID, function (jso) {
				if (jso && jso.length > 0) {

					if (jso[0].image_url.indexOf('.pdf') >= 0) {
						$('.requirement_preview').empty().append(
							'<iframe src="../common/library/pdfjs/web/viewer.html?file=../../../../' + jso[0].image_url + '"></iframe>');
					} else {
						var content;
						if (jso[0].video_url == null || jso[0].video_url.length == 0) {
							content = '<img class="img-responsive" src="' + jso[0].image_url + '" />';
						} else {
							var src = jso[0].video_url;
							if (src.indexOf('https://www.youtube.com/watch?v=') >= 0) {
								src = src.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
							}
							content = '<iframe src="' + src + '"></iframe>';
						}

						$('.requirement_preview').empty().append(content);
					}
					$('.modal-preview').modal('show');
				}
			});
		});

		btn_show_active.on('click', function () {
			app.with_inactive_groups = app.with_inactive_groups != true;
			if (app.with_inactive_groups) {
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				$(this).addClass('btn-danger').removeClass('btn-success');
				$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
			app.render_group_tree('#ojt_group_tree', app.with_inactive_groups);
		});

		container.on('click', 'table thead input[type=checkbox]', function () {
			var checkbox = $(this);
			var ojt_signoff_template_levels_roles_ID = checkbox.data('ojt_signoff_template_levels_roles_id');
			var checked = checkbox.prop('checked');

			if (checked) {
				app.create_all_group_requirement_signoffs(app.selected_ojt_group_ID, app.selected_ojt_requirement_ID, LOGIN.operator.ID, ojt_signoff_template_levels_roles_ID, app.render);
			} else {
				app.delete_all_group_requirement_signoffs(app.selected_ojt_group_ID, app.selected_ojt_requirement_ID, LOGIN.operator.ID, ojt_signoff_template_levels_roles_ID, app.render);
			}

		});

		$(document).on('nodeSelected', '#ojt_group_tree', function (event, data) {
			container.empty();
			app.selected_ojt_group_ID = data.ojt_group_ID;
			app.selected_ojt_group_description = data.ojt_group_description;

			app.init_requirement_combo();

		});

	},

	init_requirement_combo: function (cb) {
		app.get_group_requirements(app.selected_ojt_group_ID, app.with_alerts, function (jso) {
			var sel_req = $('.sel_requirements');
			app.group_requirements = jso;

			sel_req.empty().append('<option></option>');
			$.map(jso, function (v) {
				sel_req.append('<option value="' + v.ojt_requirement_ID + '">' + v.requirement_description + '</option>');
			});

			sel_req.select2({placeholder: 'OJT Requirements', width: '100%'});

			if (typeof cb == 'function') {
				cb();
			}
		});
	},

	load_data: function (cb) {
		var requirement = $.grep(app.group_requirements, function (v) {
			return v.ojt_requirement_ID == app.selected_ojt_requirement_ID;
		});

		if (requirement.length == 0) {
			return;
		}

		// gather data
		app.get_template_roles(requirement[0].ojt_signoff_template_ID, function (jso) {
			var unique_levels = {};
			var levels = [];

			// groom the data
			$.map(jso, function (v) {

				// get unique levels
				if (typeof(unique_levels[v.level_description]) == "undefined") {
					levels.push({
						level_order: v.level_order,
						level_description: v.level_description,
						roles: $.grep(jso, function (r) {
							return r.level_description == v.level_description;
						})
					});
				}
				unique_levels[v.level_description] = 0;

			});

			app.get_group_operators(app.selected_ojt_group_ID, function (jso) {
				var unique_operators = {};
				var operators = [];

				if (jso) {
					$.map(jso, function (v) {
						if (typeof(unique_operators[v.operator_ID]) == "undefined") {
							operators.push(v);
						}
						unique_operators[v.operator_ID] = 0;
					});

					app.get_signoffs(app.selected_ojt_group_ID, app.selected_ojt_requirement_ID, app.with_alerts, function (jso) {
						if (typeof cb == 'function') {
							cb(levels, operators, jso);
						} else {
							return {
								levels, operators, jso
							};
						}
					});
				} else {
					if (typeof cb == 'function') {
						cb(levels, [], []);
					} else {
						return {
							levels, operators, jso
						};
					}
				}

			});

		});

	},

	render: function (cb) {
		var container = $('.panel-signoffs .panel-body');

		if (app.selected_ojt_requirement_ID == null) {
			return;
		}

		container.empty().append('<h3 class="group-header">Group ' + app.selected_ojt_group_description + ': ' + app.selected_requirement_description + '</h3>');

		app.load_data(function(levels, operators, summary) {
			app.build_table(levels, operators, summary);

			app.set_header_checkboxes();

			if (typeof cb == 'function') {
				cb();
			}
		});
	},

	set_header_checkboxes: function() {
		// set header checkbox if all operator signoffs are complete
		var checkboxes = $("table thead tr > th > input[type=checkbox]");
		var rows = $("table tbody tr");

		$.map(checkboxes, function(c) {
			c = $(c);
			var op_checkboxes = $("table tbody tr > td input[type=checkbox][data-level_order='" + c.data('level_order') + "'][data-ojt_signoff_template_levels_roles_id='" + c.data('ojt_signoff_template_levels_roles_id') + "']");

			var signoff_count = $.grep(op_checkboxes, function(oc) {
				return $(oc).prop('checked');
			});
			if (signoff_count.length == rows.length) {
				$(c).prop('checked', true);
			} else {
				$(c).prop('checked', false);
			}
		});
	},

	build_table: function (levels, operators, summary) {
		var container = $('.panel-signoffs .panel-body');
		var table = $('<table class="table table-condensed table-responsive table-bordered table-signoffs table-hover" style="width:100%"><colgroup><col span="1"></colgroup><thead></thead><tbody></tbody></table>');
		var thead = table.find('thead');
		var tbody = table.find('tbody');
		var hr1 = $('<tr>');
		var hr2 = $('<tr>');
		var colgroup = table.find('colgroup');

		hr1.append('<th>');
		hr2.append('<th>Name</th>');

		$.map(levels, function (v) {
			colgroup.append('<col span="' + v.roles.length + '>');
			hr1.append('<th colspan="' + v.roles.length + '">' + v.level_description + '</th>');
			$.map(v.roles, function (r) {
				var h = $('<th>');
				h.append('<input type="checkbox" data-level_order="' + v.level_order + '" data-ojt_signoff_template_levels_roles_id="' + r.signoff_level_role_ID + '" style="margin-right: 5px;">');
				h.append('<span>' + r.role_description + '</span>');
				hr2.append(h);
			});
		});

		thead.append(hr1).append(hr2);

		$.map(operators, function (o) {
			var tr = $('<tr>');

			tr.append('<td>' + o.badge_ID + ': ' + o.last_name + ', ' + o.first_name + '</td>');

			$.map(levels, function (l) {
				$.map(l.roles, function (r) {
					var checked = '';
					var signoff_ID = null;
					var signoff = $.grep(summary, function (s) {
						return o.operator_ID == s.operator_ID && r.signoff_level_role_ID == s.signoff_level_role_ID;
					});

					if (signoff.length > 0) {
						signoff_ID = signoff[0].ojt_signoff_ID;
					}

					var td = $('<td>');
					var label = $('<label>');
					var checkbox = $('<input type="checkbox" data-ojt_signoff_ID="' + signoff_ID +
						'" data-level_order="' + l.level_order +
						'" data-ojt_signoff_template_levels_roles_ID="' + r.signoff_level_role_ID +
						'" data-role_name="' + r.role_description +
						'" data-operator_ID="' + o.operator_ID +
						'" data-requirement_ID="' + app.selected_ojt_requirement_ID  +
						'">');

					signoff_ID != null ? checkbox.prop('checked', true) : checkbox.prop('checked', false);

					label.append(checkbox);
					td.append(label);
					tr.append(td);

				});
			});

			tbody.append(tr);
		});

		container.append(table);

		app.after_build_table(table);
	},

	after_build_table: function (table) {
		app.restrict_checkboxes();

		if (table.find('tbody tr').size() > app.datatables_options.lengthMenu[0][0]) {
			app.datatables_options.paging = true;
			app.datatables_options.info = true;
		} else {
			app.datatables_options.paging = false;
			app.datatables_options.info = false;
		}

		if (app.use_datatables) {
			table.DataTable(app.datatables_options);
		}

		//  datatables disables header checkboxes, enable them
		table.find('th input[type=checkbox]').prop('disabled', false);

	},

	restrict_checkboxes: function () {
		if (app.config.ojt_restrict_to_login_roles) {
			$('.tbl-signoffs input[type="checkbox"]').prop('disabled', true);
			if (LOGIN) {
				var arrRoles = [];
				$.map(LOGIN.operator_roles, function (v) {
					arrRoles.push(v.role);
				});

				$.map($('.tbl-signoffs input[type="checkbox"]'), function (v) {
					var v = $(v);
					var name = v.data().role_name;
					if ($.inArray(name, arrRoles) != -1) {
						v.prop('disabled', false);
					}
				});
			}
		}
	},

	load_app_config: function (cb) {
		$.getJSON('./config/app_config.json', function (jso) {
			if (typeof cb == 'function') {
				cb(jso);
			}
		});
	}
});

var app = NYSUS.OJT;
$(document).ready(function () {

	// load config
	app.load_app_config(function (jso) {
		app.config = jso;

		app.render_group_tree('#ojt_group_tree');
		app.init();
	});

});