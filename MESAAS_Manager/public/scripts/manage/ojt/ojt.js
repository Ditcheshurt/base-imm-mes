var NYSUS = NYSUS || {};
NYSUS.OJT = NYSUS.OJT || [];

$.extend(NYSUS.OJT, {
	datatables_options: {
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"paging": true,
		"info": true,
		"width": "100%",
		"scrollX": true,
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		}
	},

	render_group_tree: function (el, with_inactive) {
		$.getJSON('../common/api/ojt/groups.php', {with_inactive: with_inactive}, function (jso) {
			var tree = [];
			$.map(jso, function (v) {
				if (v.parent_group_ID == null || v.parent_group_ID == 0) {
					var node = {
						ojt_group_ID: v.ID,
						ojt_group_description: v.group_description,
						text: build_node_text(v)
					};
					tree.push(node);
					find_children(v, node);
				}
			});
			function find_children(parent, node) {
				$.map(jso, function (v) {
					if (v.parent_group_ID == parent.ID) {
						var child_node = {
							ojt_group_ID: v.ID,
							ojt_group_description: v.group_description,
							text: build_node_text(v)
						};
						if (node.nodes == undefined) {
							node.nodes = [];
						}
						node.nodes.push(child_node);
						find_children(v, child_node);
					}
				});
			}

			function build_node_text(jso) {
				var node_text = jso.group_description;
				if (jso.active) {
					node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-success pull-right">Active</span>';
				} else {
					node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-danger pull-right">Inactive</span>';
				}
				return node_text;
			}

			$(el).treeview({data: tree});

		});

	}

});