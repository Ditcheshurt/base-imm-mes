var processor = './ajax_processors/manage/ojt/alert_signoffs.php';
var departments = [];
var areas = [];
var logged_in_ops = [];

$(document).ready(
	function() {
		loadDepartmentsAndAreas();
		$('#selArea').on('change', function(e) {
			loadAlertSignoffs();
		});

		$('#tbl_alerts').on('click', '.recordalert', function(e) {
			e.preventDefault();

			$('#div_record_alert').modal();
			$.getJSON(processor, {"action":"load_alert", "qa_item_ID":$(e.target).data('ID')},
				function(res) {
					updateRecord(res);
				}
			);
		});

		$('#inp_badge_scan').on('keypress', function(e) {
			if (e.which == 13) {
				var v = e.target.value;
				logInOperator(v);
			}
		});

		$('#btn_record').on('click', function(e) {
			var o = [];
			for (var i = 0; i < logged_in_ops.length; i++) {
				o.push(logged_in_ops[i].ID);
			}
			$.ajax({
				type: "POST",
				url: processor,
				data: {"action":"record_alert_signoffs", "qa_item_ID":$('#btn_record').data('alertid'), "ops":o.join('~')},
				success: function(res) {
					loadAlertSignoffs();
					$('#div_record_alert').modal('hide');
				},
				dataType: "json"
			});
		});

		$('.tooltipper').tooltip({"placement":"top"});
	}
);

function loadDepartmentsAndAreas() {
	$.getJSON(processor, {"action":"get_departments_and_areas"}, populateDepartmentsAndAreas);
}

function populateDepartmentsAndAreas(jso) {
	departments = jso.departments;
	areas = jso.areas;

	var s = $('#selArea').html('<option></option>').attr('disabled', false);
	for (var i = 0; i < departments.length; i++) {
		for (var j = 0; j < areas.length; j++) {
			if (departments[i].ID == areas[j].dept_ID) {
				var op = $('<OPTION />');
				op.val(areas[j].ID);
				op.html(departments[i].department_name + ': ' + areas[j].area_desc);

				s.append(op);
			}
		}
	}
}


function loadAlertSignoffs() {
	var area_ID = $('#selArea').val();
	logged_in_ops = [];
	$('#td_ops_logged_in').html(logged_in_ops.length + ' operator(s) logged in.');

	$.getJSON(processor, {"action":"load_alert_signoffs__operatrs__alerts", "area_ID":area_ID},
		function(res) {
			updateAlertSignoffs(res);
			$('#sp_load_alert_status').hide();
		}
	);
}

function updateAlertSignoffs(data) {
	data.operators = data.operators || [];
	data.alerts = data.alerts || [];
	data.signoffs = data.signoffs || [];

	$('#div_alert_signoffs').show();

	//alert headers
	var r = $('#tr_alert_headers');
	$('.dyn-headers').remove();
	var logged_in_header = $('#th_logged_in_header');
	for (var i = 0; i < data.alerts.length; i++) {
		var th = $('<TH />').html(getQALabel(data.alerts[i])).addClass('dyn-headers');
		logged_in_header.before(th);

		var btn = $('<BUTTON />').addClass('btn btn-xs btn-success recordalert disabled').html('Record Sign-off');
		btn.data(data.alerts[i]);
		var th = $('<TH />').html(btn).addClass('dyn-headers');
		r.append(th);
	}

	//operators
	var b = $('#tbl_alerts TBODY').html('');
	for (var i = 0; i < data.operators.length; i++) {
		var op = data.operators[i];
		var r = $('<TR />');
		r.append($('<TD />').html(op.name));

		for (var j = 0; j < data.alerts.length; j++) {
			var al = data.alerts[j];
			//are they signed off?
			var res = $.grep(data.signoffs, function(o, i) {return o.qa_item_ID == al.ID && o.operator_ID == op.ID;});
			if (res.length == 0) {
				r.append($('<TD />').html(' '));
			} else {
				r.append($('<TD />').html('<span class="label label-success">Yes</span>'));
			}
		}

		r.append($('<TD />').data(op).addClass('operator-login text-center').html('<label class="label label-danger">NO</label>'));
		b.append(r);
	}

//	for (var i = 0; i < data.length; i++) {
//		$("TD[data-opid='" + data[i].operator_ID + "'][data-alertid='" + data[i].qa_item_ID + "']").html('<span class="label label-success tooltipper" title="Sign-off time: ' + data[i].signoff_time + ' Team leader: ' + data[i].team_leader_name + '"><span class="glyphicon glyphicon-ok"></span> Yes</span>');
//	}

	$('.tooltipper').tooltip({"placement":"top"});
}

function logInOperator(scan) {
	$.getJSON(processor, {"action":"get_op_info", "scan":scan},
		function(res) {
			res = res || [];
			if (res.length > 0) {
				for (var i = 0; i < logged_in_ops.length; i++) {
					if (logged_in_ops[i].ID == res[0].ID) {
						//already logged in
						$('#inp_badge_scan').val('');
						return;
					}
				}

				var td = $.grep($('.operator-login'), function(o, i) {
					return $(o).data('ID') == res[0].ID;
				});

				$(td).html('<span class="label label-success"><span class="glyphicon glyphicon-ok"></span> Yes</span>');

				//$("TD[data-loginopid='" + res[0].ID + "']").html('<span class="label label-success"><span class="glyphicon glyphicon-ok"></span> Yes</span>');
				$('#inp_badge_scan').removeClass('input-error');
				logged_in_ops.push({"ID":res[0].ID, "name":res[0].name});
				$('#td_ops_logged_in').html(logged_in_ops.length + ' operator(s) logged in.');

				$('.recordalert').removeClass('disabled');
			} else {
				$('#inp_badge_scan').addClass('input-error');
			}
			$('#inp_badge_scan').val('');
		}
	);
}

function updateRecord(data) {
	var b = '';
	var l = data[0].qa_link.toUpperCase();
	var a = l.split('.');
	var ext = a[a.length-1];
	var alert_ID = data[0].item_prefix + '-';
	var s = ['', 'High', 'Medium', 'Low'];
	var c = ['', 'danger', 'warning', 'info'];

	for (var i = (data[0].ID).toString().length; i < 3; i++) {
		alert_ID += '0';
	}
	alert_ID += data[0].ID;
	$('#div_record_head').html('<h3>' + alert_ID + ': ' + data[0].qa_title + '</h3>');

	b += '<div class="alert alert-' + c[data[0].severity] + '">Severity: ' + s[data[0].severity] + '</div>';
	b += '<div>';
	switch (ext) {
		case 'GIF':
		case 'JPG':
		case 'PNG':
			b += '<a href="uploads/area_' + data[0].qa_area_ID + '/' + data[0].qa_link + '" target="_blank"><img src="uploads/area_' + data[0].qa_area_ID + '/' + data[0].qa_link + '" style="width:100%;"></a>';
			break;
		default:
			b += '<a href="uploads/area_' + data[0].qa_area_ID + '/' + data[0].qa_link + '" target="_blank" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-file"></span> Click to open: ' + data[0].qa_link + '</a>';
			break;
	}

	b += '</div>';

	$('#div_record_body').html(b);
	$('#btn_record').html('Record Sign-offs for ' + logged_in_ops.length + ' Operators').data('alertid', data[0].ID);
}

function getQALabel(al) {
	var res = al.item_prefix + '-';
	var len = al.ID.toString().length;
	if (len < 3) {
		var a = new Array(4 - len);
		res += a.join('0');
	}
	res += al.ID.toString();

	var sp = $('<SPAN />').addClass('label tooltipper').html(res);
	switch (al.severity) {
		case 1:
			sp.addClass('label-danger');
			break;
		case 2:
			sp.addClass('label-warning');
			break;
		case 3:
			sp.addClass('label-info');
			break;
	}

	var t = al.qa_title;
	sp.attr('title', t);

	return sp;
}