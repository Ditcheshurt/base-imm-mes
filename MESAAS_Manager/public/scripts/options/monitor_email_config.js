var processor = './ajax_processors/options/monitor_email_config.php';
var filter_processor = './ajax_processors/filter.php';

var machine_tool_panel;
var machine_tool_div;
var machToolTable;
var detailTable;

var edit_email_alert_modal;
var remove_email_alert_modal;
var operator_select;

var system_select;
var machine_select;
var operators;
var chosen_system;
var filter_run_btn;

String.prototype.toProperCase = function(){
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
		function($1){
			return $1.toUpperCase();
		}
	);
};

$(document).ready(function () {
	machine_tool_panel = $('#machine_tool_panel');
	machine_tool_div = $('#machine_tool_div');
	
	edit_email_alert_modal = $('#edit_email_alert_modal');
	remove_email_alert_modal = $('#remove_email_alert_modal');
	operator_select = $('#operator_select')
	
	system_select = $('#system_select');
	machine_select = $('#machine_select');
	$tools = $('#tools_combo');
	
    filter_run_btn = $('#filter_panel .btn-run');

    filter_run_btn.prop('disabled', true);

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    system_select.on('change', function (e) {
        load_filter_options();
    });

	// run report button
	$('body').on('click', '.btn-run', function(){
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			filter_run_btn.prop('disabled', true);
			// clear table
		} else {
			filter_run_btn.prop('disabled', false);
			get_machine_tool_data(machine_id); // load tools table
		}
	});
		
	//edit_email_alert_modal.on('change keyup focus blur', 'input, textarea, select', validate_alert_input);

	edit_email_alert_modal.on('click', '.btn-confirm-edit', function () {
		var o = operator_select.val() || [0];
		if (o.indexOf('all') > -1) {
			o = operators.map(function (operator) {return operator.ID;});
		}

		console.log(o);

		var email_alert_data = {
			operators: o,
		};

		update_email_alert(email_alert_data);
	});
	
	load_operators();
});

function load_filter_options () {
    filter_run_btn.prop('disabled', true);
    var system_id = system_select.find('option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    //$('#machine_select').empty();
    machine_select.nysus_select({
        name: 'machine_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_machines', database: chosen_system.database_name},
        render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		placeholder: 'Select Machine',
        callback: function (data) {
            console.log(data);
        }
    });
	
	machine_select.on('change', function(e) {
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			filter_run_btn.prop('disabled', true);
			// clear table
		} else {
			filter_run_btn.prop('disabled', false);
			get_machine_tool_data(machine_id); // load tools table
		}
	});
}

function load_operators () {
	$.getJSON(processor,{
		action: 'get_operators'
	}).done(
	function (data) {
		operators = data;
		
		if (!data) {
			data = [];
		}
		var d = data.map(function (operator) {
			return {id: operator.ID, text: operator.name+' ('+operator.email+')'};
		});
		d.unshift({id: 'all', text: 'All Employees'})

		operator_select.select2({
			placeholder: "Select Employee",
			data: d
		});
	});
}

function get_machine_tool_data (machine_id) {
	var $table = $('<table class="table table-condensed table-bordered table-striped table-display"></table>');
	//var $grid_view = $('.grid-view').nysus_panel({ 'panel_title': 'Grid View', 'show_footer': false });
	//$table.appendTo($grid_view.find('.panel-body'));
	
	machine_tool_div.empty();
	$table.appendTo(machine_tool_div);

	$.getJSON(processor, {action: 'get_tools', machine_ID: machine_id}).done(populate_machine_tool_table);
}

function populate_machine_tool_table (jso) {
	var cols = [];	
	
	if (machToolTable != null) {
        machToolTable.destroy();    //so the thing will update
        //machToolTable.clear().draw();
    }
	if (!jso) {
		jso = [{data: 'No Data'}];
		cols.push({'title':'Results','data':'data','name':'Data'});
	} else {
		cols.push({'title':'Details', 'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});	
		cols.push({'title':'Machine ID', 'data':'machine_ID', 'name':'machine_ID', 'visible':false});
		cols.push({'title':'Tool ID', 'data':'tool_ID', 'name':'tool_ID', 'visible':false});
		cols.push({'title':'Tool Short Desc', 'data':'short_description', 'name':'short_description', 'visible':true});
		cols.push({'title':'Tool Description', 'data':'tool_description', 'name':'tool_description', 'visible':true});
		cols.push({'title':'Configured Control Limits', 'data':'configured_ctrl_limits', 'name':'configured_ctrl_limits', 'visible':true});
	}
	
	machToolTable = $('.table-display').DataTable({
       "bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
		buttons: [],
        data: jso,
        columns: cols

    });
	
	//show detail on click
	$('.table-display tbody').on('click', 'td.details-control', function () {
		console.log('Details control clicked');
		event.preventDefault();
		var tr = $(this).closest('tr');
		var row = machToolTable.row(tr);

		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();
		}
		else {
			tr.addClass('shown');
			populate_details(row);
		}
	});
}

function populate_details(row) {
	console.log('Populate details');

	var data = {};
	var row_data = row.data();
	
	var $childContent = $('<div class="panel panel-primary"><div class="well"><table class="table-condensed table-bordered table-striped details-table"><tbody><tr><td>Loading...</td></tr></tbody></table></div></div>');
	row.child($childContent).show();

	data.action = 'drilldown';
	if (row_data.machine_ID != null) {
		data.machine = row_data.machine_ID;
	} else {
		data.machine = 0;
	}
	if (row_data.tool_ID != null) {
		data.tool = row_data.tool_ID;
	} else {
		data.tool = 0;
	}

	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		after_populate_details(row, jso);
	});
}

function after_populate_details(row, jso) {
	var cols = [];
	$('.details-table').empty();
	
	if (detailTable != null) {
        detailTable.destroy();    //so the thing will update
    }
	if (!jso) {
		jso = [{data: 'No Data'}];
		cols.push({'title':'Results','data':'data','name':'Data'});
	} else {
		cols.push({'title':'Machine ID', 'data':'machine_ID', 'name':'machine_ID', 'visible':false});
		cols.push({'title':'Tool ID', 'data':'tool_ID', 'name':'tool_ID', 'visible':false});
		cols.push({'title':'Data Point ID', 'data':'data_point_ID', 'name':'data_point_ID', 'visible':false});
		cols.push({'title':'Data Point', 'data':'data_point_desc', 'name':'data_point_desc', 'visible':true});
		cols.push({'title':'Upper Control Limit', 'data':'ucl', 'name':'ucl', 'visible':true});
		cols.push({'title':'Lower Control Limit', 'data':'lcl', 'name':'lcl', 'visible':true});
		cols.push({'title':'Severity Level', 'data':'severity_description', 'name':'severity_description', 'visible':true});
		cols.push({'title':'Number of Recipients', 'data':'recipients', 'name':'recipients', 'visible':true});
		cols.push({'title':'Actions', 'orderable': false, 'data': null, 'defaultContent': '<button class="btn btn-success edit_email_alert">Edit</button>'});	
	}
	
	detailTable = $('.details-table').DataTable({
       "bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
		buttons: [],
        data: jso,
        columns: cols
    });
	$('.details-table').css('width','100%');
	
	//show email alert config modal on click
	$('.details-table tbody').on('click', 'button.edit_email_alert', function () {
		console.log('Details control clicked');
		event.preventDefault();
		var tr = $(this).closest('tr');
		var row = detailTable.row(tr);
		var data = row.data();

		edit_email_alert_modal.attr('data-machine-id', data.machine_ID);
		edit_email_alert_modal.attr('data-tool-id', data.tool_ID);
		edit_email_alert_modal.attr('data-data-point-id', data.data_point_ID);

		data.action = "get_email_alerts";
		
		$.ajax({
		  method: "POST",
		  url: processor,
		  data: data
		}).done(show_email_alert_modal);
		;
	});
}

function reset_email_alert_modal () {
	edit_email_alert_modal.find('input[type="text"], input[type="number"], textarea').val('');
	edit_email_alert_modal.find('select').val(null);
	edit_email_alert_modal.find('input[type="checkbox"]').prop('checked', false);
	operator_select.select2('val', null);
	validate_alert_input();
}
function show_email_alert_modal (jso) {
	var confirm_btn = $('.btn-confirm-edit', edit_email_alert_modal);
	reset_email_alert_modal();

	if (jso) {
		var data = {};
		data.machine_id = 
		data.operators = [];

		for(i=0;i<jso.length;i++){
			data.operators.push({
				operator_ID:jso[i].ID,
				badge_ID:jso[i].badge_ID,
				name:jso[i].name,
				email:jso[i].email
			});
		}

		if (data.operators) {
			var od = data.operators.map(function (operator) {
				return operator.operator_ID;
			});
			operator_select.select2('val', od);
		}
	}
	confirm_btn.show();
	edit_email_alert_modal.modal('show');
}

function validate_alert_input () {
	var confirm_btn = $('.btn-confirm-edit', edit_email_alert_modal);

	var valid = true;
	$('form [required]', edit_email_alert_modal).each(function (i, field) {
		var f = $(field);
		var is_empty = !f.val() || f.val() < 1;
		valid = valid && !is_empty;
	});

	if (valid) {
		confirm_btn.show();
	} else {
		confirm_btn.hide();
	}
}

function update_email_alert (data) {
	data.action = 'update_alert';
	
	data.machine_id = edit_email_alert_modal.attr('data-machine-id');
	data.tool_id = edit_email_alert_modal.attr('data-tool-id');
	data.data_point_id = edit_email_alert_modal.attr('data-data-point-id');
	
	$.ajax({
		url: processor,
		type: 'POST',
		data: data,
		dataType: 'json'
	}).done(
		function (jso) {
		if (jso === null) {
			$.growl('Unable to update email alert.', {type: 'danger'});
		} else {
			$.growl('Email alert updated.', {type: 'success'});
			var machine_id = machine_select.find('option:selected').val();
			if (machine_id < 0) {
				filter_run_btn.prop('disabled', true);
				// clear table
			} else {
				filter_run_btn.prop('disabled', false);
				get_machine_tool_data(machine_id); // load tools table
			}
		}
	});
}
