var processor = './ajax_processors/options/defect_origins.php';
var current_codes = [];

$(document).ready(
	function() {
		//loadMachineTypes();
		loadData();

		//event listeners:
		//$('.machine-types').change(loadData);
		
		$('#div_codes').on('click', '.btn-edit', function() {
			event.preventDefault();
			var d = $(this).data();
			$('#inpSeries').val(d.series);
			$('#inpDesc').val(d.origin_description);
			$('#inpActive').prop('checked', (d.active == 1));

			$('.btn-save').data(d).data('edit_type', 'EDIT');
			$('.modal-title').html('Edit Downtime Reason Code');
			$('#div_add_edit_modal').modal('show');
		});

		$('#div_codes').on('click', '.btn-add-code', function() {
			event.preventDefault();
			var d = $(this).data();
			$('#inpDesc').val('');
			$('#inpActive').prop('checked', true);

			$('.btn-save').data(d).data('edit_type', 'ADD');
			$('.modal-title').html('Add Defect Origin Code <h6><span class="label label-info">Under code: ' + d.origin_description + '</span></h6>');
			$('#div_add_edit_modal').modal('show');
		});

		$('#div_codes').on('mouseover', '.btn-add-code, .btn-edit', function() {
			$(this).closest('LI').children('SPAN.highlightable').css('font-weight', 'bold').css('background-color', '#EEEEEE');
		});

		$('#div_codes').on('mouseout', '.btn-add-code, .btn-edit', function() {
			$(this).closest('LI').children('SPAN.highlightable').css('font-weight', 'normal').css('background-color', 'white');
		});

		$('#btn_add_parent').on('click', function() {
			event.preventDefault();
			$('#inpDesc').val('');
			$('#inpActive').prop('checked', true);

			$('.btn-save').data('parent_ID', '0').data('edit_type', 'ADD_Parent');
			$('.modal-title').html('Add Defect Origin Code <h6><span class="label label-info">At parent level</span></h6>');
			$('#div_add_edit_modal').modal('show');
		});

		$('.btn-save').on('click', validateSave);
		$('#btn_export').on('click', exportData);

		$('.glyphicon-question-sign').tooltip();
		
		$('#btn_clone_from').on('click', function() {
			event.preventDefault();			
			$('#div_clone_modal').modal('show');
		});
		
		$('.btn-clone').on('click', function() {
			event.preventDefault();
			clone_defect_codes();
			$('#div_clone_modal').modal('hide');
		});
	}
);

function loadMachineTypes() {
	var dat = {
        'action': 'get_machine_types'
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(function(jso) {
		if (jso == null) {
			console.log('no machine types');
			return;
		}
		var $combo = $('.machine-types');
		var opt = $('<option value="">-- Select Machine Type --</option>');
		opt.appendTo($combo);
		$.each(jso, function(k,v) {
			opt = $('<option value="' + v.ID + '">' + v.machine_type_description + '</option>');		
			opt.appendTo($combo);
		});		
		
		$combo = $('.from-machine-type');
		opt = $('<option value="">-- Select Machine Type --</option>');
		opt.appendTo($combo);
		$.each(jso, function(k,v) {
			opt = $('<option value="' + v.ID + '">' + v.machine_type_description + '</option>');		
			opt.appendTo($combo);
		});		
		
	});	
}

function exportData() {
	var x = '';
	for (var i = 0; i < current_codes.length; i++) {
		var c = current_codes[i];
		for (var j = 0; j < c.L; j++) {
			x += '  ';
		}
		x += c.defect_description + ',' + ((c.active)?'ACTIVE':'INACTIVE') + '\n';
	}
	$('#inpExport').val(x);
	$('#frmExport')[0].submit();

}

function clone_defect_codes() {
	var dat = {
        'action': 'clone_defect_codes'
        ,'from_machine_type_ID': $('.from-machine-type :selected').val()
		,'to_machine_type_ID': $('.machine-types :selected').val()
		,'machine_type_ID': $('.machine-types :selected').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(drawDowntimeTree);
	
}

function validateSave() {
	var d = $(this).data();

	$('.input-sm').parent().removeClass('has-error');

	if ($('#inpDesc').val() == '') {
		$('#inpDesc').parent().addClass('has-error');
		return;
	}

	$('div_status').html('Saving...');
	
	var params = {
		"action":"save_code",
		"edit_type":d.edit_type,
		"ID":((d.edit_type == 'ADD')?'0':d.ID),
		"parent_ID":((d.edit_type == 'ADD')?d.ID:d.parent_ID),
		"origin_description":$('#inpDesc').val(),		
		"active":(($('#inpActive').is(':checked'))?1:0)
	};
	if ($('#inpSeries').val() && $('#inpSeries').val().length > 0) {
		params.series = $('#inpSeries').val();
	}

	$.getJSON(processor, params, afterEditCode);
}

function afterEditCode(jso) {
	$('.modal').modal('hide');
	$('#div_codes').html('');

	drawDowntimeTree(jso);
	$.growl(
			{"title":" Success: ", "message":"Defect code saved!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
}

function loadData() {
	$('.modal').modal('hide');
	$.getJSON(processor, {
		'action':'get_defect_origins'
		//,'machine_type_ID': $('.machine-types :selected').val()
	}, drawDowntimeTree);
	/*
	if ($('.machine-types :selected').length > 0) {
		$('#div_status').html('Loading defect codes...');
		$('#div_codes').html('');

		$.getJSON(processor, {
			'action':'get_defect_codes'
			//,'machine_type_ID': $('.machine-types :selected').val()
		}, drawDowntimeTree);
	}*/
}

function drawDowntimeTree(jso) {
	jso = jso || [];
	current_codes = jso;
	$('#div_codes').empty();
	drawNodes($('#div_codes'), jso, null);

	$('#div_status').html('');
}

function drawNodes(target_el, arr, parent_ID) {

	var ul = $('<UL />');
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].parent_ID == parent_ID) {
			var li = $('<LI />');
			li.id = 'LI_' + arr[i].ID;
			li.data(arr[i]);
			if (arr[i].series != null && arr[i].series.length > 0) {
				li.html('<span class="highlightable">' + arr[i].series + ' - ' + arr[i].origin_description + '</span>');
			} else {
				li.html('<span class="highlightable">' + arr[i].origin_description + '</span>');
			}			
			li.css('font-weight', 'normal').css('background-color', 'white');

			if (arr[i].active != 1) {
				li.css('text-decoration', 'line-through');
			}

			//edit button
			var b = $('<BUTTON />');
			b.addClass('btn btn-xs btn-primary btn-edit pull-right');
			b.css('font-size', '75%');
			b.css('margin-left', '10px');
			b.html('<span class="glyphicon glyphicon-pencil"></span> Edit');
			b.data(arr[i]);
			li.append(b);

			//add child
			var b = $('<BUTTON />');
			b.addClass('btn btn-xs btn-success btn-add-code pull-right');
			b.css('font-size', '75%');
			b.css('margin-left', '10px');
			b.html('<span class="glyphicon glyphicon-plus"></span> Add Sub-Code');
			b.data(arr[i]);
			li.append(b);



			ul.append(li);

			drawNodes(li, arr, arr[i].ID);
		}
	}

	target_el.append(ul);
}

