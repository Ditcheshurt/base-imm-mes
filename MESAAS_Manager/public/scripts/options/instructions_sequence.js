$(function() {
		$('#div_instructions').on('click', '.btn-add-part', function() {
			event.preventDefault();

			//find max part order
			var m = 0;
			for (var i = 0; i < cur_data.instructions.length; i++) {
				if (cur_data.instructions[i].part_order > m) {
					m = cur_data.instructions[i].part_order;
				}
			}


			$('.btn-confirm-add-edit-part').data('edit_type', 'ADD');
			$('#inpAddEditPartOrder').val((m + 10).toString());
			$('#inpAddEditPartNumber').val('').prop('disabled', false);
			$('#inpPartCommon').prop('checked', false);
			$('#inpAddEditRevLevel').val('');
			$('#div_add_edit_part_modal .modal-title').html('Add Part');


			$('#div_add_edit_part_modal').modal('show');
		});

		$('.btn-confirm-add-edit-part').on('click', function() {	addEditPart($(this)); });
		$('.btn-confirm-delete-part').on('click', function() { deletePart($(this)); });

		$('.table-instructions').on('click', '.edit_part', function() {
			event.preventDefault();
			var d = $(this).data();
			$('.btn-confirm-add-edit-part').data(d).data('edit_type', 'EDIT');
			$('#inpAddEditPartOrder').val(d.part_order);
			$('#inpAddEditPartNumber').val(d.part_number);

			if (d.part_number == 'COMMON') {
				$('#inpPartCommon').prop('checked', true);
				$('#inpAddEditPartNumber').prop('disabled', true);
			} else {
				$('#inpPartCommon').prop('checked', false);
				$('#inpAddEditPartNumber').prop('disabled', false);
			}

			$('#inpAddEditRevLevel').val(d.current_rev_level);
			$('#div_add_edit_part_modal .modal-title').html('Edit Part: ' + d.part_number + d.current_rev_level);
			$('#div_add_edit_part_modal').modal('show');
		});

		$('.table-instructions').on('click', '.delete_part', function() {
			event.preventDefault();
			confirmDeletePart($(this));
		});

		$('.table-instructions').on('click', '.clone_part', function() {
			event.preventDefault();
			confirmClonePart($(this));
		});

		$('.btn-confirm-clone-part').on('click', function() {
			event.preventDefault();
			clonePart($(this));
		});

		$('#tbl_instructions_SEQUENCED').on('click', '.btn-add-step', function() {
			event.preventDefault();
			addInstruction($(this));
		});

		$('#tbl_instructions_SEQUENCED').on('click', '.btn-close-step', function() {
			event.preventDefault();
			var data = $(this).data();
			toggleInstructionRow($(this));

			var btn = $.grep($('BUTTON.select_ins'), function(el) {
				return $(el).data('part_order') == data['part_order'] && $(el).data('part_number') == data['part_number'] && $(el).data('current_rev_level') == (data['current_rev_level'] || '');
			});
			$(btn).html('<span class="glyphicon glyphicon-circle-arrow-right"></span>');
		});

		$('.btn-add-dependent').on('click', function() {
			event.preventDefault();
			var a = $('#sp_dependent_parts').data('dep');
			var v = $('#inpAddDependent').val();
			if (a.indexOf(v) < 0 && v != '') {
				a.push(v);
			}
			$('#inpAddDependent').val('');
			updateDependentAndSubtractive();
		});

		$('#sp_dependent_parts').on('click', '.btn-delete-dependent-part', function() {
			event.preventDefault();
			var a = $('#sp_dependent_parts').data('dep');
			var dependent_part = $(this).data('dependent_part');
			var i = a.indexOf(dependent_part);
			if (i > -1) {
				a.splice(i, 1);
			}
			updateDependentAndSubtractive();
		});

		$('.btn-add-subtractive').on('click', function() {
			event.preventDefault();
			var a = $('#sp_subtractive_parts').data('subtr');
			var v = $('#inpAddSubtractive').val();
			if (a.indexOf(v) < 0 && v != '') {
				a.push(v);
			}
			$('#inpAddSubtractive').val('');
			updateDependentAndSubtractive();
		});

		$('#sp_subtractive_parts').on('click', '.btn-delete-subtractive-part', function() {
			event.preventDefault();
			var a = $('#sp_subtractive_parts').data('subtr');
			var subtractive_part = $(this).data('subtractive_part');
			var i = a.indexOf(subtractive_part);
			if (i > -1) {
				a.splice(i, 1);
			}
			updateDependentAndSubtractive();
		});

		$('#inpPartCommon').on('click', function() {
			if (this.checked) {
				$('#inpAddEditPartNumber').val('COMMON').prop('disabled', true);
			} else {
				$('#inpAddEditPartNumber').val('').prop('disabled', false).focus();
			}
		});

		$('#tbl_instructions_SEQUENCED TBODY').on('click', 'SPAN.move-part-up', function() {
			confirmMovePart($(this), 'UP');
		});
		$('#tbl_instructions_SEQUENCED TBODY').on('click', 'SPAN.move-part-down', function() {
			confirmMovePart($(this), 'DOWN');
		});
		$('.btn-confirm-move-part').on('click', function() {
			movePart($(this));
		});

		$('#btn_manage_wip_parts').on('click', function() {
			event.preventDefault();
			manageWIPParts();
		});

		$('.btn-add-wip-part').on('click', function() {
			event.preventDefault();
			$('.btn-confirm-add-edit-wip-part').data('edit_type', 'ADD');
			$('#inpAddEditWIPPartID').val('');
			$('#inpAddEditWIPPartNumber').val('');
			$('#inpAddEditWIPPartDesc').val('');
			$('#div_add_edit_wip_part_modal .modal-title').html('Add WIP Part');

			$('#div_add_edit_wip_part_modal').modal('show');
		});

		$('#tbl_wip_parts').on('click', '.edit_wip_part', function() {
			event.preventDefault();
			var d = $(this).data();
			$('.btn-confirm-add-edit-wip-part').data(d).data('edit_type', 'EDIT');
			$('#inpAddEditWIPPartID').val(d.ID);
			$('#inpAddEditWIPPartNumber').val(d.part_number);
			$('#inpAddEditWIPPartDesc').val(d.part_desc);
			$('#div_add_edit_wip_part_modal .modal-title').html('Edit WIP Part: ' + d.part_number);

			$('#div_add_edit_wip_part_modal').modal('show');
		});

		$('#tbl_wip_parts').on('click', '.delete_wip_part', function() {
			event.preventDefault();
			confirmDeleteWIPPart($(this));
		});

		$('.btn-confirm-add-edit-wip-part').on('click', function() {	addEditWIPPart($(this)); });
		$('.btn-confirm-delete-wip-part').on('click', function() { deleteWIPPart($(this)); });
	}
);

function addEditPart(btn) {
	//validate
	var d = btn.data();

	var v = $('#inpAddEditPartOrder').val();
	if (isNaN(v) || v == '' || v.indexOf('.') >= 0) {
		$('#inpAddEditPartOrder').parent().addClass('has-error');
		return;
	} else {
		$('#inpAddEditPartOrder').parent().removeClass('has-error');
	}

	var v = $('#inpAddEditPartNumber').val();
	if (v == '') {
		$('#inpAddEditPartNumber').parent().addClass('has-error');
		return;
	} else {
		$('#inpAddEditPartNumber').parent().removeClass('has-error');
	}

	$.getJSON(processor, {
			"action":"save_part",
			"process_type":cur_process_type,
			"database":cur_system_database,
			"edit_type":d.edit_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"old_part_order":((d.edit_type == 'ADD')?'0':d.part_order),
			"old_part_number":((d.edit_type == 'ADD')?'':d.part_number),
			"old_current_rev_level":((d.edit_type == 'ADD')?'':d.current_rev_level),
			"new_part_order":$('#inpAddEditPartOrder').val(),
			"new_part_number":$('#inpAddEditPartNumber').val(),
			"new_current_rev_level":$('#inpAddEditRevLevel').val()
		}, populateInstrList);

}

//DELETE PART
function deletePart(btn) {
	var d = btn.data();

	$.getJSON(processor, {
			"action":"delete_part",
			"process_type":cur_process_type,
			"database":cur_system_database,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"part_order":d.part_order,
			"part_number":d.part_number,
			"current_rev_level":d.current_rev_level
		}, populateInstrList);
}

function confirmDeletePart(btn) {
	$('.btn-confirm-delete-part').data(btn.data());
	$('#div_delete_part_modal .modal-body').html('Are you ABSOLUTELY SURE you want to delete this part?  <div class="alert alert-info">Although periodic backups are made, it will take time to restore if accidentally deleted.</div>');
	$('#div_delete_part_modal').modal('show');
}

//CLONE PART
function confirmClonePart(btn) {
	var d = btn.data();
	var opt_group = '';

	$('.btn-confirm-clone-part').data(btn.data());

	$('#div_clone_part_modal .modal-title').html('Clone Part: ' + d.part_number + d.current_rev_level);

	var m = 0;
	for (var i = 0; i < cur_data.instructions.length; i++) {
		if (cur_data.instructions[i].part_order > m) {
			m = cur_data.instructions[i].part_order;
		}
	}

	m += 10;

	$('#inpClonePartOrder').val(m);

	var s = $('#selCloneStation');
	s.html('');

	var lines = $.grep(systems_lines_stations.lines, function(line, x) {
		return line.system_ID == cur_system_ID;
	});
	for (var j = 0; j < lines.length; j++) {
		opt_group += '<optgroup label="' + lines[j].line_desc + '">\n';
		var stations = $.grep(systems_lines_stations.stations, function(station, y) {
			return station.line_ID == lines[j].line_ID;
		});

		for (var k = 0; k < stations.length; k++) {
			opt_group += '<option ';
			opt_group += ((stations[k].station_ID == cur_station_ID)?' SELECTED ':'');
			opt_group += ' value="' + stations[k].line_ID + '-' + stations[k].station_ID + '-' + stations[k].station_order + '">';
			opt_group += stations[k].station_desc;
			opt_group += ' (' + stations[k].station_ID + ')</option>\n';
		}
		opt_group += '</optgroup>\n';

	}
	opt_group += '</optgroup>\n';
	$('#selCloneStation').html(opt_group).attr('disabled', false);

	$('#inpClonePartNumber').val(d.part_number);
	$('#inpCloneCurrentRevLevel').val(d.current_rev_level||'');

	$('#div_clone_part_modal').modal('show');
}

function clonePart(btn) {
	var d = btn.data();
	console.log('Cloning part: ' + d.ID);

	var v = $('#inpClonePartOrder').val();
	if (isNaN(v) || v == '' || v.indexOf('.') >= 0) {
		$('#inpClonePartOrder').parent().addClass('has-error');
		return;
	} else {
		$('#inpClonePartOrder').parent().removeClass('has-error');
	}

	var new_station_ID = $('#selCloneStation').val().split('-')[1];

	$.getJSON(processor, {
			"action":"clone_part",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"old_part_number":d.part_number,
			"old_current_rev_level":d.current_rev_level,
			"old_part_order":d.part_order,
			"new_station_ID":new_station_ID,
			"new_part_number":$('#inpClonePartNumber').val(),
			"new_current_rev_level":$('#inpCloneCurrentRevLevel').val(),
			"new_part_order":$('#inpClonePartOrder').val()
		}, populateInstrList);
}

//MOVE PART
function confirmMovePart(btn, dir) {
	var d = btn.data();

	$('#div_move_part_modal .modal-title').html('Move Part Order: ' + d.part_order);

	$('.btn-confirm-move-part').data(btn.data()).data('DIR', dir);
	$('#div_move_part_modal .modal-body').html('Are you sure you want to move ALL PARTS for part order ' + d.part_order + ' ' + dir + '?<br>To move individual parts, please edit them individually.');
	$('#div_move_part_modal').modal('show');
}

function movePart(btn) {
	var d = btn.data();
	console.log('Moving part ' + d.part_number + d.current_rev_level + ' ' + d.DIR);
	$.getJSON(processor, {
			"action":"move_part",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"part_order":d.part_order,
			"direction":d.DIR
		}, populateInstrList);
}

function populateInstrTableRowsSEQUENCED(cur_data, t) {
	//get unique part numbers and orders
	var uniq = cur_data.instructions;


	for (var i = 0; i < uniq.length; i++) {
		var part = uniq[i];
		var r = $('<tr />');
		r.addClass('part-header-row');
		t.append(r);

		//movers
		var c = $('<td />').css('text-align', 'center');
		c.data(part);
		var x = null;
		if (i > 0) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-up mover move-part-up'); //up
			x.data(part);
			c.append(x);
		}
		if (i < (cur_data.instructions.length - 1)) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-down mover move-part-down'); //down
			x.data(part);
			c.append(x);
		}
		r.append(c);

		//part_order
		var c = $('<td />').html(part.part_order).css('text-align', 'center');
		r.append(c);

		//part_number
		var c = $('<td />').html(part.part_number).css('text-align', 'center');
		r.append(c);

		//current_rev_level
		var c = $('<td />').html(part.current_rev_level).css('text-align', 'center');
		r.append(c);

		//part_desc
		var c = $('<td />').html(part.part_desc).css('text-align', 'center');
		r.append(c);

		//num_steps
		var c = $('<td />').html(part.num_steps).css('text-align', 'center');
		if ((part.num_inactive_steps || 0) > 0) {
			var s = $('<span />');
			s.addClass('label label-warning margin-left').html('<small>' + part.num_inactive_steps + ' disabled</small>');
			c.append(s);
		}
		r.append(c);

		//buttons
		var c = $('<td />').css('text-align', 'center');
		var d = $('<div />');
		d.addClass('btn-group btn-group-xs');

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('select_ins');
		s.data(part).attr('title', 'Select Part').attr('href', '#');
		s.html('<span class="glyphicon glyphicon-circle-arrow-right"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('edit_part');
		s.data(part).attr('title', 'Edit Part').attr('href', '#');
		s.html('<span class="glyphicon glyphicon-pencil"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('delete_part');
		s.data(part).attr('title', 'Delete Part');
		s.html('<span class="glyphicon glyphicon-trash"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('clone_part');
		s.data(part).attr('title', 'Clone Part');
		s.html('<span class="glyphicon glyphicon-tags"></span>');
		d.append(s);

		c.append(d);
		r.append(c);

		//part instructions row
		var r = $('<tr />');
		r.data('part_order', part.part_order);
		r.data('part_number', part.part_number);
		r.data('current_rev_level', (part.current_rev_level || ''));
		r.addClass('part-instruction-row');
		r.css('display', 'none');
		t.append(r);
	}

}

function populatePartInstrList(cur_data, part) {
	//var div = $('DIV.part-instr-table-wrapper[data-part_order=' + part.part_order + '][data-part_number=' + part.part_number + '][data-current_rev_level=' + (part.current_rev_level||'') + ']');
	var div = $.grep($('DIV.part-instr-table-wrapper'), function(el) {
		return $(el).data('part_order') == part.part_order && $(el).data('part_number') == part.part_number && $(el).data('current_rev_level') == (part.current_rev_level||'');
	});
	div = $(div);
	var tbl = $('<table />');
	tbl.addClass('table table-condensed table-striped');

	if (cur_process_type == 'SEQUENCED') {
		var c = $('<caption />');
		c.html('Instructions for part: ' + part.part_number + part.current_rev_level + ' <i>' + (part.part_desc||'') + '</i>' +
				 '<span class="label label-info margin-left"><small>order: ' + part.part_order + '</small></span>');

		var b = $('<button />');
		b.data(part);
		b.addClass('btn btn-xs btn-success pull-right btn-close-step pad-left');
		b.html('<span class="glyphicon glyphicon-remove"></span>');
		c.append(b);

		var b = $('<button />');
		b.data(part);
		b.addClass('btn btn-xs btn-success pull-right btn-add-step pad-left');
		b.html('<span class="glyphicon glyphicon-plus-sign"></span> Add New Instruction Step');
		c.append(b);


		tbl.append(c);
	}

	var th = $('<thead />');
	var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];

	th.append($('<th />').html('Move'));
	th.append($('<th />').html('Step Order'));
	th.append($('<th />').html('Title'));
	th.append($('<th />').html('Image'));
	th.append($('<th />').html('Test Type'));
	th.append($('<th />').html('Active'));
	if (cur_line_type != 1) {
		th.append($('<th />').html('Dependent<br>Subtractive'));
	} else {
		th.append($('<th />').html('WIP Part'));
	}
	th.append($('<th />').html('Actions'));

	var t = $('<tbody />');
	var max_instr_order = 0;

	cur_data.dependent = (cur_data.dependent||[]);
	cur_data.subtractive = (cur_data.subtractive||[]);

	for (var i = 0; i < cur_data.instructions.length; i++) {
		var ins = cur_data.instructions[i];

		if (ins.instr_order > max_instr_order) {
			max_instr_order = ins.instr_order;
		}

		var r = $('<tr />');
		t.append(r);

		//movers
		var c = $('<td />').css('text-align', 'center');
		c.data(ins);
		var x = null;
		if (i > 0) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-up mover move-up'); //up
			x.data(ins);
			c.append(x);
		}
		if (i < (cur_data.instructions.length - 1)) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-down mover move-down'); //down
			x.data(ins);
			c.append(x);
		}
		r.append(c);

		//instr_order
		var c = $('<td />').html(ins.instr_order).css('text-align', 'center');
		r.append(c);

		//prompt
		var c = $('<td />');
		var d = $('<span />').html(ins.title);
		d.attr('title', '<strong>Prompt:</strong> ' + ins.prompt + '<br><strong>Text:</strong> ' + ((cur_process_type=='BATCH')?ins.text:ins.instr_text));
		d.tooltip({"html":true, "placement":"bottom"});
		c.append(d);
		r.append(c);

		//image
		var c = $('<td />').css('text-align', 'center');
		if (ins.image_file != null && ins.image_file != '') {
			var img = $('<img />');
			img.attr({"height":20, "src":base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + ins.image_file});
			img.attr('title', '<img src="' + base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + ins.image_file + '" width="150">');
			img.tooltip({"html":true, "placement":"bottom"});
			c.append(img);
		} else {
			c.html('&nbsp;');
		}
		r.append(c);

		//test type
		var c = $('<td />').css('text-align', 'center');
		var d = $('<span />').html(ins.type_desc);
		var x = '';
		var ins_ops = JSON.parse(ins.instr_options || '{}');
		for (var j = 0; j < cur_data.test_type_options.length; j++) {
			if (cur_data.test_type_options[j].test_type_id == ins.test_type) {
				for (var y in ins_ops) {
					if (cur_data.test_type_options[j].option_json_member == y) {
						x += (y + ': ' + ins_ops[y] + '<br>');
					}
				}
			}
		}
		d.attr('title', x);
		d.tooltip({"html":true, "placement":"bottom"});
		c.append(d);
		r.append(c);

		var c = $('<td />').css('text-align', 'center');
		if (ins.active_flag == 1) {
			c.html('<span class="label label-success">Yes</span>');
		} else {
			c.html('<span class="label label-danger">No</span>');
		}
		if (ins_ops.move_WIP_to_FG) {
			if (ins_ops.move_WIP_to_FG == true) {
				c.append('<span class="glyphicon glyphicon-share pad-left" title="Part moves from WIP to FG at this point.  Use caution when editing this instruction."></span>');
			}
		}
		r.append(c);

		var c = $('<td />').css('text-align', 'center');
		if (cur_line_type != 1) {
			var dep = [], subtr = [];

			//dependent and subtractive
			for (var j = 0; j < cur_data.dependent.length; j++) {
				var d = cur_data.dependent[j];
				if (ins.ID == d.instr_ID) {
					dep.push(d.dependent_part);
				}
			}

			for (var j = 0; j < cur_data.subtractive.length; j++) {
				var s = cur_data.subtractive[j];
				if (ins.ID == s.instr_ID) {
					subtr.push(s.subtractive_part);
				}
			}

			c.html('<span class="label margin-left label-' + ((dep.length > 0)?'danger':'info') + '">' + dep.length + ' dep</span>');
			c.append('<span class="label margin-left label-' + ((subtr.length > 0)?'danger':'info') + '">' + subtr.length + ' sub</span>');
		} else {
			if (ins.wip_part_ID != null) {
				for (var j = 0; j < wip_parts.length; j++) {
					if (ins.wip_part_ID == wip_parts[j].ID) {
						c.html(wip_parts[j].part_desc);
					}
				}
			} else {
				c.html('All Parts');
			}
		}
		r.append(c);


		//buttons
		var c = $('<td />').css('text-align', 'center');
		var d = $('<div />');
		d.addClass('btn-group btn-group-xs');

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('edit_ins');
		s.data(ins).attr('title', 'Edit Step').attr('href', '#');
		s.html('<span class="glyphicon glyphicon-pencil"></span>');
		s.data('dependent', dep);
		s.data('subtractive', subtr);
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('delete_ins');
		s.data(ins).attr('title', 'Delete Step');
		s.html('<span class="glyphicon glyphicon-trash"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('preview_ins');
		s.data(ins).attr('title', 'Preview Step');
		s.html('<span class="glyphicon glyphicon-eye-open"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('clone_ins');
		s.data(ins).attr('title', 'Clone Step');
		s.html('<span class="glyphicon glyphicon-tags"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('view_ins_revisions');
		s.data(ins).attr('title', 'View Step Revisions');
		s.html('<span class="glyphicon glyphicon-list"></span>');
		d.append(s);

		c.append(d);
		r.append(c);
	}

	tbl.append(th);
	tbl.append(t);
	div.html(tbl).data('max_instr_order', max_instr_order);
}

function updateDependentAndSubtractive() {
	var dep = $('#sp_dependent_parts').data('dep');
	var subtr = $('#sp_subtractive_parts').data('subtr');

	var sp = $('#sp_dependent_parts');
	sp.html('');

	for (var i = 0; i < dep.length; i++) {
		var s = $('<span />').addClass('label label-info').html(dep[i] + ' ');
		var b = $('<a />').addClass('spanny-btn btn-delete-dependent-part').html('<i class="glyphicon glyphicon-trash"></i>');
		b.data('dependent_part', dep[i]);
		s.append(b);
		sp.append(s);
	}

	if (dep.length == 0) {
		var sp2 = $('<span />').addClass('label label-info').html('No dependent parts');
		sp.append(sp2);
	}


	var sp = $('#sp_subtractive_parts');
	sp.html('');

	for (var i = 0; i < subtr.length; i++) {
		var s = $('<span />').addClass('label label-info').html(subtr[i] + ' ');
		var b = $('<a />').addClass('spanny-btn btn-delete-subtractive-part').html('<i class="glyphicon glyphicon-trash"></i>');
		b.data('subtractive_part', subtr[i]);
		s.append(b);
		sp.append(s);
	}

	if (subtr.length == 0) {
		var sp2 = $('<span />').addClass('label label-info').html('No subtractive parts');
		sp.append(sp2);
	}
}

function toggleInstructionRow(btn) {
	var td = $('<td />');
	td.attr('colspan', 20);
	var d = $('<DIV />');
	d.addClass('part-instr-table-wrapper');
	d.data('part_order', btn.data('part_order'));
	d.data('part_number', btn.data('part_number'));
	d.data('current_rev_level', (btn.data('current_rev_level') || ''));
	d.html('<i>Loading instructions for ' + btn.data('part_number') + '' + (btn.data('current_rev_level') || '') + ' (order ' + btn.data('part_order') + ')...</i>');
	td.html(d);

	var r = $.grep($('TR.part-instruction-row'), function(el) {
		return $(el).data('part_order') == btn.data('part_order') && $(el).data('part_number') == btn.data('part_number') && $(el).data('current_rev_level') == (btn.data('current_rev_level') || '');
	});
	r = $(r);
	//var r = $('.part-instruction-row[data-part_order=' + btn.data('part_order') + '][data-part_number=' + btn.data('part_number') + '][data-current_rev_level=' + (btn.data('current_rev_level') || '') + ']')
	r.toggle().html(td);

	if (r.is(':visible')) {
		btn.html('<span class="glyphicon glyphicon-circle-arrow-down"></span>');
		getPartInstructions(btn.data());
	} else {
		btn.html('<span class="glyphicon glyphicon-circle-arrow-right"></span>');
	}

}

function manageWIPParts() {
	$('#div_wip_part_modal').modal('show');
	var r = Math.random() * 1000000;
		$.getJSON(processor, {
			"action":"load_wip_parts",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"r":r
		}, populateWIPParts);
}

function populateWIPParts(jso) {
	$('#div_add_edit_wip_part_modal').modal('hide');
	$('#div_delete_wip_part_modal').modal('hide');

	jso = jso||[];
	var b = $('#tbl_wip_parts TBODY');
	b.html('');

	if (jso.length == 0) {
		var r = $('<TR />');

		var c = $('<TD />');
		c.attr('colspan', 10);
		c.html('<i>No records found</i>');
		r.append(c);
		b.append(r);
	}

	for (var i = 0; i < jso.length; i++) {
		var r = $('<TR />');

		var c = $('<TD />');
		c.html(jso[i].ID);
		r.append(c);

		var c = $('<TD />');
		c.html(jso[i].part_number);
		r.append(c);

		var c = $('<TD />');
		c.html(jso[i].part_desc);
		r.append(c);

		var c = $('<TD />').css('text-align', 'center');
		var d = $('<div />');
		d.addClass('btn-group btn-group-xs');

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('edit_wip_part');
		s.data(jso[i]).attr('title', 'Edit WIP Part');
		s.html('<span class="glyphicon glyphicon-pencil"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('delete_wip_part');
		s.data(jso[i]).attr('title', 'Delete WIP Part');
		s.html('<span class="glyphicon glyphicon-trash"></span>');
		d.append(s);

		c.append(d);
		r.append(c);

		b.append(r);
	}
}

function confirmDeleteWIPPart(btn) {
	$('.btn-confirm-delete-wip-part').data(btn.data());
	$('#div_delete_wip_part_modal .modal-body').html('Are you ABSOLUTELY SURE you want to delete this WIP part?');
	$('#div_delete_wip_part_modal').modal('show');
}



function addEditWIPPart(btn) {
	//validate
	var d = btn.data();

	var v = $('#inpAddEditWIPPartID').val();
	if (isNaN(v) || v == '' || v.indexOf('.') >= 0) {
		$('#inpAddEditWIPPartID').parent().addClass('has-error');
		return;
	} else {
		$('#inpAddEditWIPPartID').parent().removeClass('has-error');
	}

	var v = $('#inpAddEditWIPPartNumber').val();
	if (v == '') {
		$('#inpAddEditWIPPartNumber').parent().addClass('has-error');
		return;
	} else {
		$('#inpAddEditWIPPartNumber').parent().removeClass('has-error');
	}

	$.getJSON(processor, {
			"action":"save_wip_part",
			"process_type":cur_process_type,
			"database":cur_system_database,
			"edit_type":d.edit_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"old_wip_part_ID":((d.edit_type == 'ADD')?'0':d.ID),
			"new_wip_part_ID":$('#inpAddEditWIPPartID').val(),
			"part_number":$('#inpAddEditWIPPartNumber').val(),
			"part_desc":$('#inpAddEditWIPPartDesc').val()
		}, populateWIPParts);

}

//DELETE PART
function deleteWIPPart(btn) {
	var d = btn.data();

	$.getJSON(processor, {
			"action":"delete_wip_part",
			"process_type":cur_process_type,
			"database":cur_system_database,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"wip_part_ID":d.ID
		}, populateWIPParts);
}




function getPartInstructions(part) {
	var r = Math.random() * 1000000;
	$.getJSON(processor, {
			"action":"load_instructions_for_part",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"station_order":cur_station_order,
			"r":r,
			"part_order":part.part_order,
			"part_number":part.part_number,
			"current_rev_level":part.current_rev_level
		}, function(jso) {
			populatePartInstrList(jso, part);
		});
}