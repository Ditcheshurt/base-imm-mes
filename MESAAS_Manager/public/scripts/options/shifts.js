var processor = './ajax_processors/options/shifts.php';

$(document).ready(
	loadData
);

function loadData() {
	$('.modal').modal('hide');

	$.getJSON(processor, {
		'action':'get_data',
		'line_id':line_id
	}, populate);

}

function populate(jso) {
	populateShiftGoals(jso);



	// shift goal commit click
	$('.btn-confirm-shift-goal').on('click', function() {
		$.getJSON(processor, {
			'action':'set_shift_goals',
			'first_shift':$('#inpFirstShiftGoal').val(),
			'second_shift':$('#inpSecondShiftGoal').val(),
			'third_shift':$('#inpThirdShiftGoal').val(),
			'line_id': line_id
		}, function(jso) {
			alert('Shift Goals Updated');
		});
	});
}

function populateShiftGoals(jso) {
	$('#inpFirstShiftGoal').val(jso.shift_goals[0].first_shift_qty);
	$('#inpSecondShiftGoal').val(jso.shift_goals[0].second_shift_qty);
	$('#inpThirdShiftGoal').val(jso.shift_goals[0].third_shift_qty);
}