var processor = './ajax_processors/options/machine_types.php';

$(function() {
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    var $panel = $('.groups').nysus_panel({panel_title: 'Machine Types', show_footer: false});
    var $head = $panel.find('.panel-heading');
    var $body = $panel.find('.panel-body');
    var $combo_label = $('<label class="control-label" for="machine-types">Select Machine Type</label>')
    var $combo = $('<select class="form-control machine-types" />');
    var $button = $('<div class="btn-add-group btn btn-info btn-xs pull-right" data-action="insert_type"><span class="glyphicon glyphicon-plus"></span>Add Type</div>');
    
    $button.appendTo($head);    
    $combo.appendTo($combo_label);
	$combo_label.appendTo($body);	
	
	// add type click
	$button.click(function() {
        $('#modalAddType').modal('show');
        $('#modalAddType input[type="text"]').val('');        
    });
	$('.btn-confirm-add-type').click(confirm_add_type);
	$('.machine-types').change(function() {
		populate_assigned_reasons();
		populate_reason_combo();
	});
	$('.btn-confirm-add-reason').click(confirm_add_reason);	
	
	populate_groups();
	
    $panel = $('.reasons').nysus_panel({panel_title: 'Assigned Downtime Reasons', show_footer: false});
    $head = $panel.find('.panel-heading');
    $body = $panel.find('.panel-body');
    $button = $('<div class="btn-add-reason btn btn-info btn-xs pull-right" data-action="insert_reason"><span class="glyphicon glyphicon-plus"></span>Add Reason</div>');
    $button.appendTo($head);
    $button.click(function() {
		$('#modalAddReason').modal('show');       
    });
	
});

function populate_reason_combo() {
	var dat = {		
        'action': 'get_downtime_reasons'
		,'machine_type_ID': $('.machine-types').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(after_populate_reason_combo);
	
}

function after_populate_reason_combo(jso) {
	if (jso == null) {
		console.log('No downtime reason codes');
		return;
	}
	
	var options = [];
	var $combo = $('#inpReasons');
	$combo.select2('data', null);
	$.each(jso, function(k,v) {
		var comboParents = [];
		if (v.parent_ID == null || v.parent_ID == 0) {
			var $kids = [];
			$.each(jso, function(k2,v2) {
				if (v.ID == v2.parent_ID) {
					$kids.push({id: v2.ID, text:v2.reason_description});					
				}
			});
			options.push({text: v.reason_description, children: $kids });
			$kids = [];
		}
	});
	
	$combo.select2({ 
		dropdownParent: $('#modalAddReason'),
		placeholder: "Downtime Reasons",
		multiple: true,
        data: options 
	});
	
}

function populate_groups() {
    var dat = {
        'action': 'get_machine_types'
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(after_populate_groups);
}

function after_populate_groups(jso) {
    var $combo = $('.machine-types');
	var opt = $('<option value="" selected>- Select Machine Type -</option>');
	
	$combo.empty();			
    opt.appendTo($combo);
    $.each(jso, function(k, v) {
        opt = $('<option value="' + v.ID + '">' + v.machine_type_description + '</option>');		
        opt.appendTo($combo);
    });    
}

function populate_assigned_reasons() {
	var dat = {
        'action': 'get_machine_types_downtime_reasons'
		,'machine_type_ID': $('.machine-types').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(after_populate_assigned_reasons);
}

function after_populate_assigned_reasons(jso) {
	var $panel = $('.reasons');
    var $head = $panel.find('.panel-heading');
    var $body = $panel.find('.panel-body');
	
	$body.empty();
	if (jso == null) {
		return;
	}
	
	var parents = [];
	// create a list for the parents
	$.each(jso, function(k,v) {		
		if (v.reason_parent_ID != null && parents.indexOf(v.reason_parent_ID) < 0) {
			var $parent = $('<div class="col-md-4 list-group parent_' + v.reason_parent_ID + '" />');
			var $label = $('<a href="#" class="list-group-item list-group-item-info">' + v.reason_parent_description + '</a>');
			$label.appendTo($parent);
			$parent.appendTo($body);
			parents.push(v.reason_parent_ID);
		}
	});
	
	// add children to parents
	$.each(jso, function(k,v) {
		var $parent = $body.find('.parent_' + v.reason_parent_ID);
		var $kid = $('<a href="#" class="list-group-item">' + v.reason_description + '</a>');
		var $delete = $('<button class="btn btn-xs btn-danger pull-right">Delete</button>');
		$delete.appendTo($kid);
		$delete.click(delete_reason);
		if (v.reason_active == 1) {
			$kid.addClass('list-group-item-success');
		} else {
			$kid.addClass('list-group-item-danger');
		}
		$kid.data(v);
		$kid.appendTo($parent);
	});

}

function confirm_add_type() {
	var modal = $('#modalAddType');
    var parent_ID = $('.machine-types :selected').val();
    var dat = {
        'action': 'insert_machine_type'
        ,'machine_type_description': modal.find('#inpDesc').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(populate_groups);
}

function confirm_add_reason() {
    var dat = {
        'action': 'insert_machine_downtime_reason'
        ,'machine_downtime_reason_ID': $('#inpReasons').val()
		,'machine_type_ID': $('.machine-types').val()
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(function() {
		populate_reason_combo();
		populate_assigned_reasons();
	});
}

function delete_reason() {
	console.log($(this).parent().data().ID);
	var dat = {
        'action': 'delete_machine_downtime_reason'
        ,'ID': $(this).parent().data().ID
    };
    $.ajax({
        url: processor,
        method: 'POST',
        data: dat
    }).done(function() {
		populate_reason_combo();
		populate_assigned_reasons();
	});
}
