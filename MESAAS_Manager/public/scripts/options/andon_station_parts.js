var processor = './ajax_processors/options/andon_station_parts.php';
var lines = [];
var stations = [];
var parts = [];
var xref = [];
var sel_line_ID = 0;

$(document).ready(
	function() {
		loadAllData(populateLinesStationsParts);

		$('#selLine').on('change', function(e) {
			sel_line_ID = $("#selLine option:selected" ).val();
			$('#btn-load').removeClass('disabled');
		});

		$('#btn-load').on('click', function() {
			loadParts();
		});

		$('#btn_add_part').click(function() {
			$('.btn-save').data({"edit_type":"ADD"});
			$('#div_edit_modal .modal-title').html('Add Part');
			$('#div_edit_modal INPUT').each(function(i, el) {el.value = '';});
			$('#div_edit_modal').modal('show');
		});

		$('#div_parts table tbody').on('click', 'input[type=checkbox]', function() {
			saveXref($(this));
		});

		$('.btn-save').click(savePart);

		$('#div_parts table tbody').on('click', '.btn-edit', editPart);
		$('#div_parts table tbody').on('click', '.btn-delete', confirmDeletePart);
		$('.btn-confirm-delete').on('click', deletePart);
	}
);

function editPart() {
	var d = $(this).data();
	$('#div_edit_modal .modal-title').html('Edit Part');
	$('#inpPartNumber').val(d.part_number);
	$('#inpPartScan').val(d.part_scan);
	$('#inpPartDesc').val(d.part_desc);
	$('#inpPickLoc').val(d.pick_location);
	$('#inpDestLoc').val(d.target_location);
	$('.btn-save').data({"part_ID":d.ID, "edit_type":"EDIT"});

	$('#div_edit_modal').modal('show');
}

function confirmDeletePart() {
	var d = $(this).data();
	$('.btn-confirm-delete').data(d);
	$('#div_delete_modal .modal-body').html('Are you SURE you want to delete this part?');
	$('#div_delete_modal').modal('show');
}

function deletePart() {
	var d = $(this).data();
	$.getJSON(processor, {"action":"delete_part", "part_ID":d.ID},  function(jso) {
		parts = jso.parts || [];
		$('.modal').modal('hide');
		loadParts();
	});
}

function savePart() {
	var d = $(this).data();
	d.action = "save_part";
	$('#frm_edit_part').find('INPUT,TEXTAREA,SELECT').each(function(i, el) {
		d[$(el).attr('id')] = $(el).val();
	});

	$.getJSON(processor, d, function(jso) {
		parts = jso.parts || [];
		$('.modal').modal('hide');
		loadParts();
	});
}

function loadAllData(cb) {
	$.getJSON(processor, {
			"action":"load_lines_stations_parts"
		}, cb);
}

function populateLinesStationsParts(jso) {
	$('.modal').modal('hide');
	lines = jso.lines || [];
	stations = jso.stations || [];
	parts = jso.parts || [];
	xref = jso.xref || [];

	$('#selLine').html('');
	$.each(lines, function() {
		$('#selLine').append($("<option />").val(this.ID).text(this.line_code));
	});
}

function populateStations(line_ID) {
	$('#selStation').html('');
	$.each(stations, function() {
		if (this.line_id == line_ID) {
			$('#selStation').append($("<option />").val(this.ID).text(this.station_desc));
		}
	});
}

function loadParts() {
	var t = $('#div_parts table thead');
	t.html('');
	var r = t.append($('<tr />'));
	var c = r.append($('<th />').text('Parts'));
	$.each(stations, function() {
		if (this.line_id == sel_line_ID) {
			var c = r.append($('<th />').html(this.station_id + '<br><span class="glyphicon glyphicon-question-sign"></span>').attr('title', this.station_desc)); //.addClass('rotate'));
		}
	});

	var t = $('#div_parts table tbody');
	t.html('');
	$.each(parts, function() {
		var r = $('<tr />');
		var c = $('<td />').text(this.part_number+ ' ');
		var s = $('<span />').addClass('glyphicon glyphicon-pencil btn btn-xs btn-edit').attr('title', 'Edit');
		s.data(this);
		c.append(s);
		var s = $('<span />').addClass('glyphicon glyphicon-trash btn btn-xs btn-delete').attr('title', 'Delete');
		s.data(this);
		c.append(s);
		r.append(c);

		var part_ID = this.ID;
		$.each(stations, function() {
			if (this.line_id == sel_line_ID) {
				var c = $('<td />');
				var cb = $('<input />').attr('type', 'checkbox').addClass('toggle_xref');
				cb.data('part_ID', part_ID);
				cb.data('line_ID', this.line_id);
				cb.data('station_ID', this.station_id);

				for (var i = 0; i < xref.length; i++) {
					if (xref[i].part_ID == part_ID && xref[i].line_ID == this.line_id && xref[i].station_ID == this.station_id) {
						cb.attr('checked', true);
					}
				}
				c.append(cb);
				r.append(c);
			}
		});
		t.append(r);
	});

	$('#div_parts').show();
}

function saveXref(cb) {
	$.getJSON(processor, {
			"action":"save_xref",
			"part_ID":cb.data('part_ID'),
			"line_ID":cb.data('line_ID'),
			"station_ID":cb.data('station_ID'),
			"is_on":((cb.is(':checked'))?1:0)
		}, afterSaveXref);
}

function afterSaveXref(jso) {
	loadAllData(afterReLoad);
}

function afterReLoad(jso) {
	lines = jso.lines || [];
	stations = jso.stations || [];
	parts = jso.parts || [];
	xref = jso.xref || [];
}