var processor = './ajax_processors/options/printers.php';

$(document).ready(
	function() {
		loadPrinters();

		$('.btn-load-tools').on('click', function(e) {
			e.preventDefault();
			loadTools();
		});

		$('.btn-add-tool').on('click', function(e) {
			e.preventDefault();

			$('#div_add_edit_tool_modal .modal-title').html('<span class="glyphicon glyphicon-plus"></span> Add Tool');
			$('#inpFullToolName').val('');
			$('#inpShortToolName').val('');
			$('#inpOJTGroup').val('');
			$('#inpAutoLabel').prop('checked', false);
			$('#inpToolActive').prop('checked', true);

			//load OJT
			$.getJSON(processor, {
				"action":"load_ojt_groups"
			}, function(jso) {
					var s = $('#inpOJTGroup').empty();
					var op = $('<OPTION />').html('- None -').val('0').appendTo(s).css('font-style', 'italic');

					$(jso).each(function(i, el) {
						var op = $('<OPTION />').html(el.group_description).val(el.ID).appendTo(s);
					});
			});

			$('#div_add_edit_tool_modal').modal('show').data('ID', 0);
		});

		$('#tbl_tools TBODY').on('click', '.btn-edit-tool', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_add_edit_tool_modal .modal-title').html('<span class="glyphicon glyphicon-pencil"></span> Edit Tool');
			$('#inpFullToolName').val(d.tool_description);
			$('#inpShortToolName').val(d.short_description);
			$('#inpOJTGroup').val(d.ojt_group_id);
			$('#inpAutoLabel').prop('checked', d.auto_label);
			$('#inpToolActive').prop('checked', d.active);

			//load OJT
			$.getJSON(processor, {
				"action":"load_ojt_groups"
			}, function(jso) {
					var s = $('#inpOJTGroup').empty();
					var op = $('<OPTION />').html('- None -').val('0').appendTo(s).css('font-style', 'italic');

					$(jso).each(function(i, el) {
						var op = $('<OPTION />').html(el.group_description).val(el.ID).appendTo(s);

						if (el.ID == d.ojt_group_id) {
							op.prop('selected', true);
						}
					});
			});

			$('#div_add_edit_tool_modal').modal('show').data(d);
		});

		$('.btn-save-tool').on('click', function(e) {
			e.preventDefault();

			//validation
			$('#div_add_edit_tool_modal DIV').removeClass('has-error');
			if ($('#inpFullToolName').val() == '') {
				$('#inpFullToolName').parent().addClass('has-error');
				return;
			}
			if ($('#inpShortToolName').val() == '') {
				$('#inpShortToolName').parent().addClass('has-error');
				return;
			}

			var d = $('#div_add_edit_tool_modal').data();
			var action = 'edit_tool';
			if (d.ID == 0) {
				action = 'add_tool';
			}

			$.getJSON(processor, {
				"action":action,
				"tool_description":$('#inpFullToolName').val(),
				"short_description":$('#inpShortToolName').val(),
				"auto_label":($('#inpAutoLabel').prop('checked'))?1:0,
				"ojt_group_id":$('#inpOJTGroup').val(),
				"active":($('#inpToolActive').prop('checked'))?1:0,
				"tool_ID":d.ID
			}, function(jso) {
					$.growl({"type":"success", "message":"Tool Saved!"});
					$('#div_add_edit_tool_modal').modal('hide');
					loadTools();
			})
		});


		$('#tbl_tools TBODY').on('click', '.btn-edit-machine-tool', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_edit_tool_machine_modal .modal-title').html('<span class="glyphicon glyphicon-pencil"></span> Edit Machine');
			$('#inpTargetCycleTime').val(d.target_cycle_time);
			$('#inpOverCycleGracePeriod').val(d.over_cycle_grace_period);
			$('#div_edit_tool_machine_modal').modal('show').data(d);
		});

		$('.btn-save-tool-machine').on('click', function(e) {
			e.preventDefault();


			//validation
			$('#div_edit_tool_machine_modal DIV').removeClass('has-error');
			if ($('#inpTargetCycleTime').val() == '') {
				$('#inpTargetCycleTime').parent().addClass('has-error');
				return;
			}
			if ($('#inpOverCycleGracePeriod').val() == '') {
				$('#inpOverCycleGracePeriod').parent().addClass('has-error');
				return;
			}

			var d = $('#div_edit_tool_machine_modal').data();
			$.getJSON(processor, {
					"action":"edit_machine_tool",
					"machine_ID":d.machine_ID,
					"tool_ID":d.tool_ID,
					"target_cycle_time":$('#inpTargetCycleTime').val(),
					"over_cycle_grace_period":$('#inpOverCycleGracePeriod').val()
				}, function(jso) {
						$.growl({"type":"success", "message":"Machine/Tool Association Saved!"});
						$('#div_edit_tool_machine_modal').modal('hide');
						loadTools();
				});

		});

		$('#tbl_tools TBODY').on('click', '.btn-associate-machine', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_associate_machine_modal .modal-title').html('<span class="glyphicon glyphicon-link"></span> Associate Machine');
			//load machines
			$.getJSON(processor, {
					"action":"load_machines_and_tools"
				}, function(jso) {
						var d2 = $('#div_associate_machine_modal .modal-body').empty();
						var t = $('<TABLE />').appendTo(d2).addClass('table table-condensed table-bordered table-striped');
						var h = $('<THEAD />').appendTo(t);
						var r = $('<TR />').appendTo(h);
						var th = $('<TH />').appendTo(r).html('Machine Name');
						var th = $('<TH />').appendTo(r).html('Target Cycle Time (sec)');
						var th = $('<TH />').appendTo(r).html('Over Cycle Grace Period (sec)');
						var th = $('<TH />').appendTo(r).html('Enabled');
						var b = $('<TBODY />').appendTo(t);

						$(jso.machines).each(function(i, el) {
							var tct = '';
							var ocgp = '';
							var on = false;

							$(jso.machine_tools).each(function(j, el2) {
								if (el.ID == el2.machine_ID && el2.tool_ID == d.tool_ID) {
									tct = el2.target_cycle_time;
									ocgp = el2.over_cycle_grace_period;
									on = true;
								}
							});

							var r = $('<TR />').appendTo(b);
							var c = $('<TD />').html(el.machine_name).appendTo(r);

							var c = $('<TD />').appendTo(r);
							var inp = $('<INPUT type="text" />').appendTo(c).addClass('input-sm input-tct').val(tct).attr('id', 'inp_tct_' + el.ID);
							var c = $('<TD />').appendTo(r);
							var inp = $('<INPUT type="text" />').appendTo(c).addClass('input-sm input-ocgp').val(ocgp).attr('id', 'inp_ocgp_' + el.ID);;

							var c = $('<TD />').appendTo(r).addClass('text-center');
							var l = $('<LABEL />').addClass('switch').appendTo(c);
							var cb = $('<INPUT />').attr('type', 'checkbox').appendTo(l).prop('checked', on).attr('id', 'inp_on_off_' + el.ID);
							var sl = $('<DIV />').addClass('slider round').appendTo(l);
						});
				});

			$('#div_associate_machine_modal').modal('show').data(d);
		});

		$('#tbl_tools TBODY').on('click', '.btn-disassociate-machine', function(e) {
			e.preventDefault();

			var d = $(this).data();
			d.action = 'DISASSOCIATE_MACHINE';
			$('#div_confirmation_modal .modal-title').html('<span class="glyphicon glyphicon-minus"></span> Disassociate Machine?');
			$('#div_confirmation_modal .modal-body').html('Are you sure you want to disassociate the machine from this tool?');
			$('#div_confirmation_modal').modal('show').data(d);
		});

		$('.btn-save-tool-machine-association').on('click', function(e) {
			e.preventDefault();

			var d = $('#div_associate_machine_modal').data();
			var post_data = [];
			var found_error = false;

			$('#div_associate_machine_modal INPUT.input-tct').each(function(i, el) {
				var id = $(el).attr('id');
				var id_arr = id.split('_');
				var machine_ID = id_arr[2];

				if ($('#inp_on_off_' + machine_ID).prop('checked') && $(el).val() == '') {
					$(el).focus();
					$.growl({"type":"danger", "message":"No target cycle time entered!"});
					found_error = true;
					return;
				} else {
					$(el).parent().removeClass('has-error');
				}

				if ($('#inp_on_off_' + machine_ID).prop('checked') && $('inp_ocgp_' + machine_ID).val() == '') {
					$('inp_ocgp_' + machine_ID).focus();
					$.growl({"type":"danger", "message":"No over cycle grace period entered!"});
					found_error = true;
					return;
				}else {
					$('inp_ocgp_' + machine_ID).parent().removeClass('has-error');
				}

				post_data.push(machine_ID + '~' + $(el).val() + '~' + $('#inp_ocgp_' + machine_ID).val() + '~' + (($('#inp_on_off_' + machine_ID).prop('checked'))?'1':'0') );
			});

			if (found_error) {
				return;
			}

			$.getJSON(processor, {
					"action":"save_tool_machines",
					"tool_ID":d.tool_ID,
					"post_data":post_data
				}, function(jso) {
						$.growl({"type":"success", "message":"Machine Associations Saved!"});
						$('#div_associate_machine_modal').modal('hide');
						$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
				});

		});



		//PARTS:
		$('#tbl_tools TBODY').on('click', '.btn-add-part', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_add_edit_part_modal .modal-title').html('<span class="glyphicon glyphicon-plus"></span> Add Part');
			$('#inpPartDesc').val('');
			$('#inpPartNumber').val('');
			$('#inpPartsPerTool').val('');
			$('#inpStdPackQty').val('');
			$('#inpImage').val('');

			$.getJSON(processor, {
				"action":"load_part_groups"
			}, function(jso) {
					var s = $('#inpPartGroup').empty();
					var op = $('<OPTION />').html('- None -').val('0').appendTo(s).css('font-style', 'italic');

					$(jso).each(function(i, el) {
						var op = $('<OPTION />').html(el.part_group_description).val(el.ID).appendTo(s);
					});
			});

			$('#div_add_edit_part_modal').modal('show').data(d).data('action','add_part').data('ID', 0);
		});

		$('#tbl_tools TBODY').on('click', '.a-move-part', function(e) {
			e.preventDefault();

			var d = $(this).data();

			$.getJSON(processor, {
				"action":"move_part",
				"direction":d.direction,
				"tool_ID":d.tool_ID,
				"part_ID":d.part_ID
			}, function(jso) {
					$.growl({"type":"success", "message":"Part moved!"});
						$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
			});
		});

		$('#tbl_tools TBODY').on('click', '.btn-edit-part', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_add_edit_part_modal .modal-title').html('<span class="glyphicon glyphicon-pencil"></span> Edit Part');
			$('#inpPartDesc').val(d.part_desc);
			$('#inpPartNumber').val(d.part_number);
			$('#inpPartsPerTool').val(d.parts_per_tool);
			$('#inpStdPackQty').val(d.standard_pack_qty);
			$('#inpImage').val('');

			$.getJSON(processor, {
				"action":"load_part_groups"
			}, function(jso) {
					var s = $('#inpPartGroup').empty();
					var op = $('<OPTION />').html('- None -').val('0').appendTo(s).css('font-style', 'italic');

					$(jso).each(function(i, el) {
						var op = $('<OPTION />').html(el.part_group_description).val(el.ID).appendTo(s);
						if (el.ID == d.part_group_id) {
							op.prop('selected', true);
						}
					});
			});
			$('#div_add_edit_part_modal').modal('show').data(d).data('action','edit_part');
		});

		$('.btn-save-part').on('click', function(e) {
			e.preventDefault();

			$('#frm_edit_part').submit();
		});

		$('#frm_edit_part').on('submit', function(e) {
			e.preventDefault();

			//validation
			$('#div_add_edit_part_modal DIV').removeClass('has-error');
			if ($('#inpPartDesc').val() == '') {
				$('#inpPartDesc').parent().addClass('has-error');
				return;
			}
			if ($('#inpPartNumber').val() == '') {
				$('#inpPartNumber').parent().addClass('has-error');
				return;
			}

			var d = $('#div_add_edit_part_modal').data();
			var action = 'edit_part';
			if (d.ID == 0) {
				action = 'add_part';
			}

			var fd = new FormData($('#frm_edit_part')[0]);
			fd.append('action', action);
			fd.append('tool_ID', d.tool_ID);
			fd.append('part_ID', d.ID);
			fd.append('part_group_ID', $('#inpPartGroup').val());
			fd.append('part_desc', $('#inpPartDesc').val());
			fd.append('part_number', $('#inpPartNumber').val());
			fd.append('std_pack_qty', $('#inpStdPackQty').val());
			fd.append('parts_per_tool', $('#inpPartsPerTool').val());
			fd.append('part_image', $('#inpImage')[0].files[0]);

			$.ajax(processor, {
				data:fd,
				method:"POST",
				processData: false,
				contentType: false,
				success: function(jso) {
					$.growl({"type":"success", "message":"Part Saved!"});
					$('#div_add_edit_part_modal').modal('hide');

					//load tool info
					$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
				}
			})
		});

		$('#tbl_tools TBODY').on('click', '.btn-associate-part', function(e) {
			e.preventDefault();

			var d = $(this).data();
			$('#div_associate_part_modal .modal-title').html('<span class="glyphicon glyphicon-link"></span> Associate Parts');
			//load parts
			$.getJSON(processor, {
					"action":"load_part_and_tool_parts"
				}, function(jso) {
						var d2 = $('#div_associate_part_modal .modal-body').empty();
						var t = $('<TABLE />').appendTo(d2).addClass('table table-condensed table-bordered table-striped');
						var h = $('<THEAD />').appendTo(t);
						var r = $('<TR />').appendTo(h);
						var th = $('<TH />').appendTo(r).html('Part Description');
						var th = $('<TH />').appendTo(r).html('Parts Per Tool');
						var th = $('<TH />').appendTo(r).html('Off/On <span class="glyphicon glyphicon-question-sign helper" title="Off means part is not associated to the current tool."></span>');
						var b = $('<TBODY />').appendTo(t);

						$(jso.parts).each(function(i, el) {
							var ppt = '';
							var on = false;

							$(jso.tool_parts).each(function(j, el2) {
								if (el.ID == el2.part_ID && el2.tool_ID == d.tool_ID) {
									ppt = el2.parts_per_tool;
									on = true;
								}
							});

							var r = $('<TR />').appendTo(b);
							var c = $('<TD />').html(el.part_desc).appendTo(r);

							var c = $('<TD />').appendTo(r);
							var inp = $('<INPUT type="text" />').appendTo(c).addClass('input-sm input-ppt').val(ppt).attr('id', 'inp_ppt_' + el.ID);

							var c = $('<TD />').appendTo(r).addClass('text-center');
							var l = $('<LABEL />').addClass('switch').appendTo(c);
							var cb = $('<INPUT />').attr('type', 'checkbox').appendTo(l).prop('checked', on).attr('id', 'inp_on_off_' + el.ID);
							var sl = $('<DIV />').addClass('slider round').appendTo(l);
						});
						$('SPAN.helper').tooltip({container:'body'});
				});

			$('#div_associate_part_modal').modal('show').data(d);
		});

		$('#tbl_tools TBODY').on('click', '.btn-disassociate-part', function(e) {
			e.preventDefault();

			var d = $(this).data();
			d.action = 'DISASSOCIATE_PART';
			$('#div_confirmation_modal .modal-title').html('<span class="glyphicon glyphicon-minus"></span> Disassociate Part?');
			$('#div_confirmation_modal .modal-body').html('Are you sure you want to disassociate the part from this tool?');
			$('#div_confirmation_modal').modal('show').data(d);
		});

		$('.btn-save-confirmation').on('click', function(e) {
			e.preventDefault();

			var d = $('#div_confirmation_modal').data();

			switch (d.action) {
				case 'DISASSOCIATE_MACHINE':
					$.getJSON(processor, {
						"action":"DISASSOCIATE_MACHINE",
						"machine_ID":d.machine_ID,
						"tool_ID":d.tool_ID
					}, function(jso) {
							$.growl({"type":"success", "message":"Machine Association Removed!"});
							$('#div_confirmation_modal').modal('hide');

							$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
					});
					break;
				case 'DISASSOCIATE_PART':
					$.getJSON(processor, {
						"action":"DISASSOCIATE_PART",
						"part_ID":d.part_ID,
						"tool_ID":d.tool_ID
					}, function(jso) {
							$.growl({"type":"success", "message":"Part Association Removed!"});
							$('#div_confirmation_modal').modal('hide');

							$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
					});
					break;
			}
		});

		$('.btn-save-tool-part-association').on('click', function(e) {
			e.preventDefault();

			var d = $('#div_associate_part_modal').data();
			var post_data = [];
			var found_error = false;

			$('#div_associate_part_modal INPUT.input-ppt').each(function(i, el) {
				var id = $(el).attr('id');
				var id_arr = id.split('_');
				var part_ID = id_arr[2];

				if ($('#inp_on_off_' + part_ID).prop('checked') && $(el).val() == '') {
					$(el).focus();
					$.growl({"type":"danger", "message":"No parts per tool entered!"});
					found_error = true;
					return;
				} else {
					$(el).parent().removeClass('has-error');
				}

				post_data.push(part_ID + '~' + $(el).val() + '~' + (($('#inp_on_off_' + part_ID).prop('checked'))?'1':'0') );
			});

			if (found_error) {
				return;
			}

			$.getJSON(processor, {
					"action":"save_tool_parts",
					"tool_ID":d.tool_ID,
					"post_data":post_data
				}, function(jso) {
						$.growl({"type":"success", "message":"Part Associations Saved!"});
						$('#div_associate_part_modal').modal('hide');
						$.getJSON(processor, {
								"action":"load_tool_parts_and_machines",
								"tool_ID":d.tool_ID
							}, function(jso) {
									populateToolPartsAndMachines(d.tool_ID, jso);
								}
							);
				});

		});

		$('.btn-export-tools').on('click', function(e) {
			e.preventDefault();

			$.getJSON(processor, {
					"action":"load_tools"
				}, function(jso) {
						var d = '';

						$(jso).each(function(i, el) {
							if (i == 0) {
								for (x in el) {
									d += x + ',';
								}
								d += '\n';
							}

							for (x in el) {
								d += el[x] + ',';
							}
							d += '\n';
						});

						$('#inpExport').val(d);
						$('#frm_export').submit();
				});
		});

		$('SPAN.glyphicon-question-sign').tooltip();
	}
);


function loadTools() {
	$.getJSON(processor, {
			"action":"load_tools"
		}, populateTools);
}

function populateTools(jso) {
	var b = $('#tbl_tools TBODY').empty();

	$(jso).each(function (i, el) {
			var r = $('<TR />').appendTo(b);
			if (i % 2 == 1) {
				r.css('background-color', '#f9f9f9');
			}
			var c = $('<TD />').appendTo(r);
			var btn = $('<BUTTON />').addClass('btn btn-xs btn-default btn-expand-tool')
				.html('<span class="glyphicon glyphicon-plus"></span> Expand')
				.appendTo(c)
				.data(el);

			var c = $('<TD />').appendTo(r).html(el.tool_description);
			var c = $('<TD />').appendTo(r).html(el.short_description);
			var c = $('<TD />').appendTo(r).html(el.auto_label ? 'Yes' : 'No');
			var c = $('<TD />').appendTo(r).html(el.group_description || '<i>None</i>');
			var c = $('<TD />').appendTo(r).html(el.shot_count);
			var c = $('<TD />').appendTo(r).html(el.active ? 'Yes' : 'No');
			var c = $('<TD />').appendTo(r).html(el.num_part_groups);
			var c = $('<TD />').appendTo(r);

			var btn_group = $('<DIV />').addClass('btn-group').appendTo(c);

			var btn = $('<BUTTON />').addClass('btn btn-xs btn-default btn-edit-tool')
					.html('<span class="glyphicon glyphicon-pencil"></span> Edit')
					.appendTo(btn_group)
					.data(el);

			var r = $('<TR />').appendTo(b).css('display', 'none').attr('id', 'tr_tool_' + el.ID);
		});
}

function populateToolPartsAndMachines(tool_ID, jso) {
	var tool_TD = $('#td_tool_' + tool_ID).empty();

	//machines:
	var d_machines = $('<DIV />').addClass('col-md-3').appendTo(tool_TD);
	var d_machines_panel = $('<DIV />').addClass('panel panel-primary').appendTo(d_machines);
	var d_machines_panel_heading = $('<DIV />').addClass('panel-heading').appendTo(d_machines_panel).html('<span class="glyphicon glyphicon-compressed"></span> Machines');
	var btn = $('<BUTTON />').addClass('btn btn-xs btn-success btn-associate-machine pull-right')
				.html('<span class="glyphicon glyphicon-link"></span> Associate Machines')
				.appendTo(d_machines_panel_heading)
				.data('tool_ID', tool_ID);

	var d_machines_panel_body = $('<DIV />').addClass('panel-body').appendTo(d_machines_panel);

	var t = $('<TABLE />').addClass('table table-condensed table-bordered table-striped').appendTo(d_machines_panel_body);
	var thead = $('<THEAD />').appendTo(t);
	var r = $('<TR />').appendTo(thead);
	var h = $('<TH />').appendTo(r).html('Machine');
	var h = $('<TH />').appendTo(r).html('Target Cycle');
	var h = $('<TH />').appendTo(r).html('Edit');

	var tbody = $('<TBODY />').appendTo(t);
	$(jso.machines).each(function(i, el) {
		var r = $('<TR />').appendTo(tbody);
		var c = $('<TD />').appendTo(r).html(el.machine_name);
		var c = $('<TD />').appendTo(r).html(el.target_cycle_time);
		//add over cycle grace period as a label
		if (el.over_cycle_grace_period) {
			$('<span class="label label-warning left-margin" />').html(el.over_cycle_grace_period + ' grace').appendTo(c);
		}

		var c = $('<TD />').appendTo(r);
		var btn_group = $('<DIV />').addClass('btn-group').appendTo(c);
		var btn = $('<BUTTON />').addClass('btn btn-xs btn-default btn-edit-machine-tool')
				.html('<span class="glyphicon glyphicon-pencil"></span> Edit')
				.appendTo(btn_group)
				.data(el)
				.data('tool_ID', tool_ID)
				.data('machine_ID', el.ID);
		var btn = $('<BUTTON />').addClass('btn btn-xs btn-danger btn-disassociate-machine')
				.html('<span class="glyphicon glyphicon-minus"></span> Remove')
				.appendTo(btn_group)
				.data('tool_ID', tool_ID)
				.data('machine_ID', el.ID);
	});

	//parts
	var d_parts = $('<DIV />').addClass('col-md-9').appendTo(tool_TD);
	var d_parts_panel = $('<DIV />').addClass('panel panel-primary').appendTo(d_parts);
	var d_parts_panel_heading = $('<DIV />').addClass('panel-heading').appendTo(d_parts_panel).html('Parts');
	var btn_group = $('<DIV />').addClass('btn-group pull-right').appendTo(d_parts_panel_heading);
	var btn = $('<BUTTON />').addClass('btn btn-xs btn-success btn-add-part')
				.html('<span class="glyphicon glyphicon-plus"></span> Add New Part')
				.appendTo(btn_group)
				.data('tool_ID', tool_ID);
	var btn = $('<BUTTON />').addClass('btn btn-xs btn-success btn-associate-part pull-right')
				.html('<span class="glyphicon glyphicon-link"></span> Associate Existing Parts')
				.appendTo(btn_group)
				.data('tool_ID', tool_ID);

	var d_parts_panel_body = $('<DIV />').addClass('panel-body').appendTo(d_parts_panel);

	var t = $('<TABLE />').addClass('table table-condensed table-bordered table-striped').appendTo(d_parts_panel_body);
	var thead = $('<THEAD />').appendTo(t);
	var r = $('<TR />').appendTo(thead);
	var h = $('<TH />').appendTo(r).html('Part Group');
	var h = $('<TH />').appendTo(r).html('Description');
	var h = $('<TH />').appendTo(r).html('Part Number');
	var h = $('<TH />').appendTo(r).html('Image');
	var h = $('<TH />').appendTo(r).html('Parts Per Tool');
	var h = $('<TH />').appendTo(r).html('Std Pack Qty');
	var h = $('<TH />').appendTo(r).html('Part Order');
	var h = $('<TH />').appendTo(r).html('Edit');

	var tbody = $('<TBODY />').appendTo(t);
	$(jso.parts).each(function(i, el) {
		var r = $('<TR />').appendTo(tbody);
		var c = $('<TD />').appendTo(r).html(el.part_group_description);
		var c = $('<TD />').appendTo(r).html(el.part_desc);
		var c = $('<TD />').appendTo(r).html(el.part_number);
		var c = $('<TD />').appendTo(r);
		if (el.part_image) {
			var img = $('<IMG />').appendTo(c).attr('src', el.part_image).attr('height', 40);
		} else {
			c.html('<i>None</i>');
		}
		var c = $('<TD />').appendTo(r).html(el.parts_per_tool);
		var c = $('<TD />').appendTo(r).html(el.standard_pack_qty || '<i>NOT DEFINED!</i>');
		var c = $('<TD />').appendTo(r).html('').addClass('text-center')
		if ((i + 1) < jso.parts.length) {
			//move down
			$('<a />').attr('href', '#').addClass('a-move-part').html('<span class="glyphicon glyphicon-menu-down"></span>').appendTo(c)
				.data(el)
				.data('direction', 'DOWN')
				.data('part_ID', el.ID)
				.data('tool_ID', tool_ID);
		}
		if (i > 0) {
			//move up
			$('<a />').attr('href', '#').addClass('a-move-part').html('<span class="glyphicon glyphicon-menu-up"></span>').appendTo(c)
				.data(el)
				.data('direction', 'UP')
				.data('part_ID', el.ID)
				.data('tool_ID', tool_ID);
		}
		var c = $('<TD />').appendTo(r);

		var btn_group = $('<DIV />').addClass('btn-group').appendTo(c);
		var btn = $('<BUTTON />').addClass('btn btn-xs btn-default btn-edit-part')
				.html('<span class="glyphicon glyphicon-pencil"></span> Edit Part')
				.appendTo(btn_group)
				.data(el)
				.data('tool_ID', tool_ID)
				.data('part_ID', el.ID);
		var btn = $('<BUTTON />').addClass('btn btn-xs btn-danger btn-disassociate-part')
				.html('<span class="glyphicon glyphicon-minus"></span> Remove')
				.appendTo(btn_group)
				.data('tool_ID', tool_ID)
				.data('part_ID', el.ID);
	});
}
