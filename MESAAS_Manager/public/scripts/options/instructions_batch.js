$(function () {

	$('#batch-parts-modal').on('show.bs.modal', function (e) {
		$('#batch-parts-modal .modal-body .tbl-batch-parts tbody').empty();
		loadPartsTable();
	});

	$('#batch-parts-modal').on('hide.bs.modal', function (e) {
		$('#batch-parts-modal .modal-body .tbl-batch-parts tbody').empty();
	});

	$('.tbl-batch-parts').on('click', '.btn-edit-part', function () {
		var d = $(this).closest('tr').data('part_ID');
		$(this).data('part_ID', d);
	});

	$('.tbl-batch-parts').on('click', '.btn-delete-part', function () {
		var d = $(this).closest('tr').data('part_ID');
		var pd = $(this).closest('tr').data('part_desc');
		$(this).data('part_ID', d);
		$(this).data('part_desc', pd);
	});

	$('.btn-confirm-delete-batch-part').on('click', function () {
		var d = $(this).data('part_ID');

		$.ajax({
			url : './ajax_processors/options/batch_parts.php',
			method : 'POST',
			data : {
				action : 'delete',
				part_ID : d
			}
		});

	});

	$('.btn-confirm-edit-batch-part').on('click', function () {
		var d = $(this).data('part_ID');
		var postData = {
			action : d == '' || d == undefined ? 'insert' : 'update',
			cell_ID : cur_line_ID,
			part_ID : d,
			part_number : $('#inp_part_num').val(),
			part_desc : $('#inp_part_desc').val(),
			part_rev_level : $('#inp_part_rev').val(),
			shipping_part_number : $('#inp_ship_part_num').val(),
			blind_cats_1 : $('#inp_bc_1').val(),
			blind_cats_2 : $('#inp_bc_2').val(),
			blind_cats_3 : $('#inp_bc_3').val(),
			blind_cats_4 : $('#inp_bc_4').val(),
			blind_cats_5 : $('#inp_bc_5').val(),
			blind_cats_6 : $('#inp_bc_6').val()
		};

		$.ajax({
			url : './ajax_processors/options/batch_parts.php',
			method : 'POST',
			data : postData
		}).done(function (jso) {
			$('#edit-batch-parts-modal modal-body').html('Saved ' + jso['Result']);
		}).fail(function (jqXHR, textStatus) {
			$('#edit-batch-parts-modal modal-body').html('Save Failed: ' + textStatus);
		});
	});

	$('#edit-batch-parts-modal').on('show.bs.modal', function (e) {
		var d = $(e.relatedTarget).data('part_ID');
		$('.btn-confirm-edit-batch-part').data('part_ID', d == '' || d == undefined ? '' : d);

		$("#edit-batch-parts-modal modal-body :input").each(function () {
			$(this).val('');
		});

		if (d != null) {
			$.ajax({
				url : './ajax_processors/options/batch_parts.php',
				data : {
					action : 'select',
					method : 'POST',
					part_ID : d
				}
			}).done(function (jso) {
				if (jso.length > 0) {
					$('#inp_part_num').val(jso[0]['part_number']);
					$('#inp_part_desc').val(jso[0]['part_desc']);
					$('#inp_part_rev').val(jso[0]['part_rev_level']);
					$('#inp_ship_part_num').val(jso[0]['shipping_part_number']);
					$('#inp_bc_1').val(jso[0]['blind_cats_1']);
					$('#inp_bc_2').val(jso[0]['blind_cats_2']);
					$('#inp_bc_3').val(jso[0]['blind_cats_3']);
					$('#inp_bc_4').val(jso[0]['blind_cats_4']);
					$('#inp_bc_5').val(jso[0]['blind_cats_5']);
					$('#inp_bc_6').val(jso[0]['blind_cats_6']);
				}
			});
		}
	});

	$('#edit-batch-parts-modal').on('hide.bs.modal', function (e) {
		loadPartsTable();
	});

	$('#delete-batch-parts-modal').on('show.bs.modal', function (e) {
		var d = $(e.relatedTarget).data('part_ID');
		var pd = $(e.relatedTarget).data('part_desc');
		$(e.currentTarget).find('.modal-body').html('Confirm Deletion of Part: ' + pd);
		$('.btn-confirm-delete-batch-part').data('part_ID', d);
	});

	$('#delete-batch-parts-modal').on('hide.bs.modal', function (e) {
		loadPartsTable();
	});

});

function loadPartsTable() {
	$('.tbl-batch-parts tbody').empty();
	$.ajax({
		url : './ajax_processors/options/batch_parts.php',
		data : {
			action : 'load_batch_parts',
			method : 'POST',
			cell_ID : cur_line_ID
		}
	}).done(function (jso) {
		var dataCols = ['part_number', 'part_desc', 'part_rev_level', 'shipping_part_number'];
		for (var i = 0; i < jso.length; i++) {
			var d = jso[i];
			var r = $('<tr />');
			r.data('part_ID', d['ID']);
			r.data('part_desc', d['part_desc']);
			for (var ii = 0; ii < dataCols.length; ii++) {
				var c = $('<td />').html(d[dataCols[ii]]);
				r.append(c);
			};
			var ac = $('<td />');
			var btn_edit = $('<button />').addClass('btn btn-info btn-xs btn-edit-part').html('Edit');
			// add a stackable confirm modal
			btn_edit.attr('data-toggle', 'modal');
			btn_edit.attr('href', '#edit-batch-parts-modal');
			var btn_delete = $('<button />').addClass('btn btn-danger btn-xs btn-delete-part').html('Delete');
			// add a stackable confirm modal
			btn_delete.attr('data-toggle', 'modal');
			btn_delete.attr('href', '#delete-batch-parts-modal');
			ac.append(btn_edit);
			ac.append(btn_delete);
			r.append(ac);
			$('.tbl-batch-parts tbody').append(r);
		}

		//$('#batch-parts-modal .modal-body .tbl-batch-parts').table_scroll({columnsInScrollableArea :2});

	});
}


function populateInstrTableRowsBATCH(cur_data, t) {
	var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];

	for (var i = 0; i < cur_data.instructions.length; i++) {
		var ins = cur_data.instructions[i];

		var r = $('<tr />');
		t.append(r);

		//movers
		var c = $('<td />').css('text-align', 'center');
		c.data(ins);
		var x = null;
		if (i > 0) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-up mover move-up'); //up
			x.data(ins);
			c.append(x);
		}
		if (i < (cur_data.instructions.length - 1)) {
			x = $('<span />').addClass('glyphicon glyphicon-circle-arrow-down mover move-down'); //down
			x.data(ins);
			c.append(x);
		}
		r.append(c);

		//instr_order
		var c = $('<td />').html(ins.instr_order).css('text-align', 'center');
		r.append(c);

		//applicable part
		var c = $('<td />').css('text-align', 'center');
		if (ins.part_id == null) {
			c.html('All Parts');
		} else {
			if (ins.part_id > 0) {
				c.html(ins.part_desc);
			} else {
				for (var j = 0; j < systems_lines_stations.part_groups.length; j++) {
					if (systems_lines_stations.part_groups[j].part_group_ID == ins.part_id) {
						c.html(systems_lines_stations.part_groups[j].part_group_desc);
						break;
					}
				}
			}
		}
		r.append(c);

		//prompt
		var c = $('<td />');
		var d = $('<span />').html(ins.title);
		d.attr('title', '<strong>Prompt:</strong> ' + ins.prompt + '<br><strong>Text:</strong> ' + ins.text);
		d.tooltip({"html":true, "placement":"bottom"});
		c.append(d);
		r.append(c);

		//image
		var c = $('<td />').css('text-align', 'center');
		if (ins.image_file != null && ins.image_file != '') {
			var img = $('<img />');
			img.attr({"height":20, "src":base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_order) + ins.image_file});
			img.attr('title', '<img src="' + base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_order) + ins.image_file + '" width="150">');
			img.tooltip({"html":true, "placement":"bottom"});
			c.append(img);
		} else {
			c.html('&nbsp;');
		}
		r.append(c);

		//test type
		var c = $('<td />').css('text-align', 'center');
		var d = $('<span />').html(ins.type_desc);
		var x = '';
		var ins_ops = JSON.parse(ins.instr_options || '{}');
		for (var j = 0; j < cur_data.test_type_options.length; j++) {
			if (cur_data.test_type_options[j].test_type_id == ins.test_type) {
				for (var y in ins_ops) {
					if (cur_data.test_type_options[j].option_json_member == y) {
						x += (y + ': ' + ins_ops[y] + '<br>');
					}
				}
			}
		}
		d.attr('title', x);
		d.tooltip({"html":true, "placement":"bottom"});
		c.append(d);
		r.append(c);

		var c = $('<td />').css('text-align', 'center');
		if (ins.active_flag) {
			c.html('<span class="label label-success">Yes</span>');
		} else {
			c.html('<span class="label label-danger">No</span>');
		}
		if (ins_ops.move_WIP_to_FG) {
			if (ins_ops.move_WIP_to_FG == true) {
				c.append('<span class="glyphicon glyphicon-share pad-left" title="Part moves from WIP to FG at this point.  Use caution when editing this instruction."></span>');
			}
		}
		r.append(c);

		//buttons
		var c = $('<td />').css('text-align', 'center');
		var d = $('<div />');
		d.addClass('btn-group btn-group-xs');

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('edit_ins');
		s.data(ins).attr('title', 'Edit').attr('href', '#');
		s.html('<span class="glyphicon glyphicon-pencil"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('delete_ins');
		s.data(ins).attr('title', 'Delete');
		s.html('<span class="glyphicon glyphicon-trash"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('preview_ins');
		s.data(ins).attr('title', 'Preview');
		s.html('<span class="glyphicon glyphicon-eye-open"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('clone_ins');
		s.data(ins).attr('title', 'Clone');
		s.html('<span class="glyphicon glyphicon-tags"></span>');
		d.append(s);

		var s = $('<button />');
		s.addClass('btn btn-xs');
		s.addClass('view_ins_revisions');
		s.data(ins).attr('title', 'View Revisions');
		s.html('<span class="glyphicon glyphicon-list"></span>');
		d.append(s);

		c.append(d);
		r.append(c);
	}
}

