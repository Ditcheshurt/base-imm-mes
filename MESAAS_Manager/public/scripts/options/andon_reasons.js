var processor = './ajax_processors/options/andon_reasons.php';
var categories = [];
var reasons = [];
var sel_category_ID = 0;

$(document).ready(
	function() {
		loadAllData(populateCategories);

		$('#btn-load').on('click', function() {
			sel_category_ID = $("#selCategory option:selected" ).val();
			loadReasons();
		});

		$('.btn-save').click(saveReason);

		$('#div_reasons').on('click', '.btn-edit', editReason);
		$('#div_reasons').on('click', '.btn-delete', confirmDeleteReason);
		$('#div_reasons').on('click', '.btn-add', addReason);
		$('.btn-confirm-delete').on('click', deleteReason);

		$('FORM').on('submit', function() {
			return false;
		});
	}
);

function addReason() {
	var d = $(this).data();
	var parent_ID = 0;
	if (d.parent == '0') {
		$('#div_edit_modal .modal-title').html('Add Reason');
	} else {
		$('#div_edit_modal .modal-title').html('Add Sub-Reason to ' + d.reason_desc);
		parent_ID = d.ID;
	}
	$('#inpDesc').val('');
	$('.btn-save').data({"part_ID":0, "parent_ID":parent_ID, "edit_type":"ADD"});

	$('#div_edit_modal').modal('show');
}

function editReason() {
	var d = $(this).data();
	$('#div_edit_modal .modal-title').html('Edit Reason');
	$('#inpDesc').val(d.reason_desc);
	$('.btn-save').data({"part_ID":d.ID, "edit_type":"EDIT"});

	$('#div_edit_modal').modal('show');
}

function confirmDeleteReason() {
	var d = $(this).data();
	$('.btn-confirm-delete').data(d);
	$('#div_delete_modal .modal-body').html('Are you SURE you want to delete this reason?');
	$('#div_delete_modal').modal('show');
}

function deleteReason() {
	var d = $(this).data();
	$.getJSON(processor, {"action":"delete_reason", "reason_ID":d.ID, "category_ID":sel_category_ID},  function(jso) {
		reasons = jso.reasons || [];
		$('.modal').modal('hide');
		loadReasons();
	});
}

function saveReason() {
	var d = $(this).data();
	d.action = "save_reason";
	d.reason_desc = $('#inpDesc').val();
	d.category_ID = sel_category_ID;

	$.getJSON(processor, d, function(jso) {
		reasons = jso.reasons || [];
		$('.modal').modal('hide');
		loadReasons();
	});
}

function loadAllData(cb) {
	$.getJSON(processor, {
			"action":"load_categories"
		}, cb);
}

function populateCategories(jso) {
	$('.modal').modal('hide');
	categories = jso.categories || [];

	$('#selCategories').html('');
	$.each(categories, function() {
		$('#selCategory').append($("<option />").val(this.ID).text(this.category_name));
	});
}

function populateStations(line_ID) {
	$('#selStation').html('');
	$.each(stations, function() {
		if (this.line_id == line_ID) {
			$('#selStation').append($("<option />").val(this.ID).text(this.station_desc));
		}
	});
}

function loadReasons() {
	$.getJSON(processor, {"action":"load_reasons", "category_ID":sel_category_ID},  function(jso) {
		reasons = jso.reasons || [];
		$('.modal').modal('hide');

		populateReasons();
	});
}

function populateReasons() {
	var u = $('#ul_reasons').html('');
	for (var i = 0; i < reasons.length; i++) {
		var li = $('<LI />').html(reasons[i].reason_desc);
		var b = $('<BUTTON />').html('<span class="glyphicon glyphicon-plus"></span>').attr('title', 'Add Sub-Reason').data(reasons[i]);
		b.addClass('btn btn-primary btn-xs margin-left btn-add');
		li.append(b);
		var b = $('<BUTTON />').html('<span class="glyphicon glyphicon-trash"></span>').attr('title', 'Delete').data(reasons[i]);
		b.addClass('btn btn-primary btn-xs btn-delete');
		li.append(b);
		var b = $('<BUTTON />').html('<span class="glyphicon glyphicon-pencil"></span>').attr('title', 'Edit').data(reasons[i]);
		b.addClass('btn btn-primary btn-xs btn-edit');
		li.append(b);

		if ((reasons[i].reasons||[]).length > 0) {
			var ul2 = $('<UL />');
			for (var j = 0; j < reasons[i].reasons.length; j++) {
				var li2 = $('<LI />').html('<img src="/images/elbow.png"> ' + reasons[i].reasons[j].reason_desc);
				var b = $('<BUTTON />').html('<span class="glyphicon glyphicon-trash"></span>').attr('title', 'Delete').data(reasons[i].reasons[j]);
				b.addClass('btn btn-primary btn-xs margin-left btn-delete');
				li2.append(b);
				var b = $('<BUTTON />').html('<span class="glyphicon glyphicon-pencil"></span>').attr('title', 'Edit').data(reasons[i].reasons[j]);
				b.addClass('btn btn-primary btn-xs btn-edit');
				li2.append(b);

				ul2.append(li2);
			}
			li.append(ul2);
		}
		u.append(li);
	}

	$('#div_reasons').fadeIn();
}