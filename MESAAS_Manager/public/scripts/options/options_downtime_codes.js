var processor = './ajax_processors/options/downtime_codes.php';
var current_codes = [];

$(document).ready(
	function() {
		loadData();

		//event listeners:
		$('#div_codes').on('click', '.btn-edit', function() {
			event.preventDefault();
			var d = $(this).data();
			$('#inpDesc').val(d.reason_description);
			$('#inpCountAgainst').prop('checked', (d.count_against == 1));
			$('#inpActive').prop('checked', (d.active == 1));

			$('.btn-save').data(d).data('edit_type', 'EDIT');
			$('.modal-title').html('Edit Downtime Reason Code');
			$('#div_add_edit_modal').modal('show');
		});

		$('#div_codes').on('click', '.btn-add-code', function() {
			event.preventDefault();
			var d = $(this).data();
			$('#inpDesc').val('');
			$('#inpCountAgainst').prop('checked', true);
			$('#inpActive').prop('checked', true);

			$('.btn-save').data(d).data('edit_type', 'ADD');
			$('.modal-title').html('Add Downtime Reason Code <h6><span class="label label-info">Under code: ' + d.reason_description + '</span></h6>');
			$('#div_add_edit_modal').modal('show');
		});

		$('#div_codes').on('mouseover', '.btn-add-code, .btn-edit', function() {
			$(this).closest('LI').children('SPAN.highlightable').css('font-weight', 'bold').css('background-color', '#EEEEEE');
		});

		$('#div_codes').on('mouseout', '.btn-add-code, .btn-edit', function() {
			$(this).closest('LI').children('SPAN.highlightable').css('font-weight', 'normal').css('background-color', 'white');
		});

		$('#btn_add_parent').on('click', function() {
			event.preventDefault();
			$('#inpDesc').val('');
			$('#inpCountAgainst').prop('checked', false);
			$('#inpActive').prop('checked', true);

			$('.btn-save').data('parent_ID', '0').data('edit_type', 'ADD');
			$('.modal-title').html('Add Downtime Reason Code <h6><span class="label label-info">At parent level</span></h6>');
			$('#div_add_edit_modal').modal('show');
		});

		$('.btn-save').on('click', validateSave);
		$('#btn_export').on('click', exportData);


		$('.glyphicon-question-sign').tooltip();
	}
);

function exportData() {
	var x = '';
	for (var i = 0; i < current_codes.length; i++) {
		var c = current_codes[i];
		for (var j = 0; j < c.L; j++) {
			x += '  ';
		}
		x += c.reason_description + ',' + ((c.active)?'ACTIVE':'INACTIVE') + ',' +  ((c.count_against)?'NON-SCHEDULED':'SCHEDULED') + '\n';
	}
	$('#inpExport').val(x);
	$('#frmExport')[0].submit();

}

function validateSave() {
	var d = $(this).data();

	$('.input-sm').parent().removeClass('has-error');

	if ($('#inpDesc').val() == '') {
		$('#inpDesc').parent().addClass('has-error');
		return;
	}

	$('div_status').html('Saving...');

	$.getJSON(processor, {
			"action":"save_code",
			"edit_type":d.edit_type,
			"ID":((d.edit_type == 'ADD')?'0':d.ID),
			"parent_ID":((d.edit_type == 'ADD')?d.ID:d.parent_ID),
			"reason_description":$('#inpDesc').val(),
			"count_against":(($('#inpCountAgainst').is(':checked'))?1:0),
			"active":(($('#inpActive').is(':checked'))?1:0)
		}, afterEditCode);
}

function afterEditCode(jso) {
	$('.modal').modal('hide');
	$('#div_codes').html('');

	drawDowntimeTree(jso);
	$.growl(
			{"title":" Success: ", "message":"Downtime code saved!", "icon":"glyphicon glyphicon-ok"},
			{"type":"success", "allow_dismiss":false, "placement":{"from":"top", "align":"right"}, "offset":{"x":10, "y":80}}
		);
}

function loadData() {
	$('.modal').modal('hide');

	$('#div_status').html('Loading downtime codes...');
	$('#div_codes').html('');

	$.getJSON(processor, {
		'action':'get_downtime_codes'
	}, drawDowntimeTree);

}

function drawDowntimeTree(jso) {
	jso = jso || [];
	current_codes = jso;

	drawNodes($('#div_codes'), jso, null);

	$('#div_status').html('');
}

function drawNodes(target_el, arr, parent_ID) {

	var ul = $('<UL />');
	for (var i = 0; i < arr.length; i++) {
		if (arr[i].parent_ID == parent_ID) {
			var li = $('<LI />');
			li.id = 'LI_' + arr[i].ID;
			li.data(arr[i]);
			li.html('<span class="highlightable">' + arr[i].reason_description + '</span>');
			li.css('font-weight', 'normal').css('background-color', 'white');

			if (arr[i].active != 1) {
				li.css('text-decoration', 'line-through');
			}

			if (arr[i].count_against != 1) {
				var sp = $('<SPAN />');
				sp.addClass('label label-xs label-info');
				sp.css('margin-left', '20px');
				sp.html('Scheduled');
				li.append(sp);
			}

			//edit button
			var b = $('<BUTTON />');
			b.addClass('btn btn-xs btn-primary btn-edit pull-right');
			b.css('font-size', '75%');
			b.css('margin-left', '10px');
			b.html('<span class="glyphicon glyphicon-pencil"></span> Edit');
			b.data(arr[i]);
			li.append(b);

			//add child
			var b = $('<BUTTON />');
			b.addClass('btn btn-xs btn-success btn-add-code pull-right');
			b.css('font-size', '75%');
			b.css('margin-left', '10px');
			b.html('<span class="glyphicon glyphicon-plus"></span> Add Sub-Code');
			b.data(arr[i]);
			li.append(b);



			ul.append(li);

			drawNodes(li, arr, arr[i].ID);
		}
	}

	target_el.append(ul);
}