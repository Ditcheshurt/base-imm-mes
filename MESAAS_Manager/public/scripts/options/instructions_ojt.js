var processor_ojt = "./ajax_processors/options_instructions_ojt.php";

$(function() {
	$('#div_ojt_modal').on('click', '.btn-add-ojt', function() {
		$('#div_ojt_add_requirement_modal').modal('show');
		$.getJSON(processor_ojt, {"action":"load_OJT_jobs", "line_ID":cur_line_ID, "station_ID":cur_station_ID}, populateOJTJobSelection);
	});

	$('#div_ojt_modal').on('click', '.btn-add-qa', function() {
		$('#div_QA_add_requirement_modal').modal('show');
		$.getJSON(processor_ojt, {"action":"load_quality_alerts", "line_ID":cur_line_ID, "station_ID":cur_station_ID}, populateQASelection);
	});

	$('.btn-confirm-add-ojt-requirement').click(addOJTRequirement);
	$('.btn-confirm-add-qa-requirement').click(addQARequirement);

});

function showOJT() {
	var line_desc = $.grep(systems_lines_stations.lines, function(l, i) {return l.line_ID == cur_line_ID;})[0]['line_desc'];
	var station_desc = $.grep(systems_lines_stations.stations, function(s, i) {return s.line_ID == cur_line_ID && s.station_order == cur_station_order;})[0]['station_desc'];
	var station_ID = $.grep(systems_lines_stations.stations, function(s, i) {return s.line_ID == cur_line_ID && s.station_order == cur_station_order;})[0]['station_ID'];
	var station_IPs = $.map($.grep(systems_lines_stations.stations, function(s, i) {return s.station_ID == station_ID;}), function(n) { return n.ip_address; });
	$('#sp_ojt_station_desc').html('Line: ' + station_desc);
	$('#sp_ojt_line_desc').html('Station: ' + line_desc);

	$('#div_ojt_modal').modal('show');

	loadOJTRequirements();
	loadQualityAlerts();
}


function loadOJTRequirements() {
	$.getJSON(processor_ojt, {"action":"load_OJT_requirements", "line_ID":cur_line_ID, "station_ID":cur_station_ID}, populateOJTTable);
}

function populateOJTTable(jso) {
	var tb = $('#tblOJT TBODY');
	tb.html('');

	if (jso.length == 0) {
		var r = $('<tr />');
		var c = $('<td />').attr('colspan', 20).html('No OJT requirements found.');
		r.append(c);
		tb.append(r);
	} else {
		for (var i = 0; i < jso.length; i++) {
			var b = $('<button class="btn btn-danger btn-sm" onclick="deleteOJTRequirement('+jso[i].ID+')"><span class="glyphicon glyphicon-remove"></span> Delete</button>');
			b.attr('value', jso[i].ID);
			//b.click(deleteOJTRequirement(jso[i].ID));

			var r = $('<tr />');
			r.append($('<td />').html(jso[i].job_name));
			r.append($('<td />').html(jso[i].num_signoffs));
			r.append($('<td />').html(''));
			r.append($('<td align="center" />').html(b));
			tb.append(r);
		}
	}
}

function loadQualityAlerts() {
	$.getJSON(processor_ojt, {"action":"load_quality_alert_requirements", "line_ID":cur_line_ID, "station_ID":cur_station_ID}, populateQATable);
}

function populateQATable(jso) {
	var tb = $('#tblQA TBODY');
	tb.html('');

	if (jso.length == 0) {
		var r = $('<tr />');
		var c = $('<td />').attr('colspan', 20).html('No quality alerts found.');
		r.append(c);
		tb.append(r);
	} else {
		for (var i = 0; i < jso.length; i++) {
			var b = $('<button class="btn btn-danger btn-sm" onclick="deleteQARequirement('+jso[i].ID+')"><span class="glyphicon glyphicon-remove"></span> Delete</button>');
			b.attr('value', jso[i].ID);

			var r = $('<tr />');
			r.append($('<td />').html(jso[i].job_name));
			r.append($('<td />').html(jso[i].num_signoffs));
			r.append($('<td />').html(jso[i].allow_MES_signoff));
			r.append($('<td />').html(''));
			r.append($('<td align="center" />').html(b));
			tb.append(r);
		}
	}
}

function populateOJTJobSelection(jso) {
	var s = $('#selOJTJobAdd').html('');
	jso = jso||[];

	if (jso.length == 0) {
		var op = $('<option />');
		op.attr('disabled', true);
		op.html('No OJT jobs available to add');
		s.append(op);
		$('.btn-confirm-add-ojt-requirement').prop('disabled', true);
	} else {
		$('.btn-confirm-add-ojt-requirement').prop('disabled', false);
		for (var i = 0; i < jso.length; i++) {
			var op = $('<option />');
			op.attr('value', jso[i].ID);
			op.html(jso[i].job_name);

			s.append(op);
		}
	}
}

function populateQASelection(jso) {
	var s = $('#selQAAdd').html('');
	jso = jso||[];

	if (jso.length == 0) {
		var op = $('<option />');
		op.attr('disabled', true);
		op.html('No quality alerts available to add');
		s.append(op);
		$('.btn-confirm-add-qa-requirement').prop('disabled', true);
	} else {
		$('.btn-confirm-add-qa-requirement').prop('disabled', false);
		for (var i = 0; i < jso.length; i++) {
			var op = $('<option />');
			op.attr('value', jso[i].ID);
			op.html(jso[i].qa_title);

			s.append(op);
		}
	}
}

function addOJTRequirement() {
	var ojt_job_ID = $("#selOJTJobAdd option:selected").attr('value');

	if(cur_station_ID != null && ojt_job_ID != null)
	{
		$.getJSON(processor_ojt, {"action":"add_OJT_requirements", "station_ID":cur_station_ID, "ojt_job_ID":ojt_job_ID}, populateOJTTable);
		$('#div_ojt_add_requirement_modal').hide(1000).modal('hide');
	}

}

function deleteOJTRequirement(id) {
	if(cur_station_ID != null && id != null)
	{
		$.getJSON(processor_ojt, {"action":"delete_OJT_requirements", "station_ID":cur_station_ID, "ojt_job_ID":id}, populateOJTTable);
	}

}

function addQARequirement() {
	var qa_item_ID = $("#selQAAdd option:selected").attr('value');
	var allow_MES_signoff = $("#inpAllowMESSignoff").is(':checked');

	if(cur_station_ID != null && qa_item_ID != null)
	{
		$.getJSON(processor_ojt, {"action":"add_QA_requirements", "station_ID":cur_station_ID, "qa_item_ID":qa_item_ID, "allow_MES_signoff":allow_MES_signoff}, populateQATable);
		$('#div_QA_add_requirement_modal').hide(1000).modal('hide');
	}

}

function deleteQARequirement(id) {
	if(cur_station_ID != null && id != null)
	{
		$.getJSON(processor_ojt, {"action":"delete_QA_requirements", "station_ID":cur_station_ID, "qa_item_ID":id}, populateQATable);
	}

}