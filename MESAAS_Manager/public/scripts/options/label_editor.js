var processor = './ajax_processors/options/label_editor.php';
var templates;
var template_select;
var selected_template;
var edit_template_modal;
var edit_template_form;
var edit_template_modal;
var archive_template_modal;

$(document).ready(function () {
	edit_template_modal = $('#edit_template_modal');
	edit_template_form = $('#edit_template_form');
	archive_template_modal = $('#archive_template_modal');

	build_template_select();

	$('.btn-add-template').on('click', function(e) {
		edit_template_form[0].reset();
		edit_template_modal.find('.modal-title').html('Add Template');
		edit_template_modal.attr('data-action', 'add_template');
		edit_template_modal.modal('show').data(null);
	});
	
	$('.btn-edit-template').on('click', function (e) {
		//reset_template_modal();
		edit_template_modal.find('form')[0].reset();

		var template_type_select = $('#template_type_select').nysus_select({
			name: 'template_type_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_template_types'},
			render: function (template_type) {return template_type.template_type_description;},
			placeholder: 'Select Template Type',
			default_value: selected_template.template_type_ID
		});

		edit_template_modal.find('input, textarea, checkbox').each(function (i, input) {
			var field = $(input);
			if (field.prop('type') == 'checkbox') {
				var val = selected_template[field.prop('name')]?true:false;
				field.prop('checked', val );
			} else if (field.prop('type') != 'radio') {
				field.val(selected_template[field.prop('name')]);
			}

		});

		edit_template_modal.find('.modal-title').html('Edit Template');
		edit_template_modal.attr('data-action', 'update_template');
		edit_template_modal.modal('show').data(null);
	});
	
	$('.btn-archive-template').on('click', function(e) {
		archive_template_modal.find('.btn-confirm-archive').on('click', function (e) {
			archive_template(selected_template);
		});
		archive_template_modal.modal('show').data(null);
	});
	
	$('#checkbox1').change(function() {
		var rev_hist = $('#template_rev_panel');
		if(this.checked){
			rev_hist.show(null,show_history(selected_template));
		} else {
			rev_hist.hide();
		}
	});

	edit_template_modal.find('.btn-confirm-edit').on('click', function (e) {
		var data = {
			template_name: edit_template_modal.find('input[name="template_name"]').val(),
			template_text: edit_template_modal.find('textarea[name="template_text"]').val(),
			template_active: edit_template_modal.find('input[name="template_active"]').prop("checked"),
			operator_ID: LOGIN.operator.ID
		};
		switch(edit_template_modal.attr('data-action')) {
			case 'add_template':
				add_template(data);
				break;
			case 'update_template':
				update_template(data);
				break;
			default:
				break;
		}
	});

	// ignore enter
	// $(document).on('keyup keypress', function (e) {
		// var code = e.keyCode || e.which;
		// if (code == 13) {
			// e.preventDefault();
			// return false;
		// }
	// });
});

function build_template_select () {
	template_select = $('#template_select').nysus_select({
		name: 'template_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_templates'},
		render: function (template) {return template.template_name;},
		callback: function (data) {
			templates = data;
		},
		placeholder: 'Select Template'
	});

	template_select.on('change', function (e) {
		var detail = $('#template_detail_panel');
		var rev_hist = $('#template_rev_panel');
		var template_ID = $('#template_select option:selected').val();

		var res = $.grep(templates, function (m) {
			return m.ID == template_ID;
		});
		selected_template = res[0];

		if (template_ID > 0) {
			load_template(template_ID, show_template);
			$('#checkbox1').prop("disabled",false);
		} else {
			detail.hide();
			rev_hist.hide();
			$('#checkbox1').prop("disabled",true);
		}
	});
}

function load_template (template_ID, callback) {
	var params = {action: 'get_template', template_ID: template_ID};
	$.getJSON(processor, params).done(function (data) {
		if (data) {
			selected_template = data[0];
		}

		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_template (data) {
	var detail = $('#template_detail_panel');

	if (data) {
		detail.find('.template-name').html(data[0].template_name);
		detail.find('.template-text').html(data[0].template_text.replace(/</g, "&lt").replace(/>/g, "&gt"));
		detail.find('.template-active').html(data[0].template_active?'Yes':'No');
		detail.find('.template-revision').html(data[0].template_revision);
		
		var rev_hist = $('#template_rev_panel');
		if($('#checkbox1').is(":checked")){
			rev_hist.show(null,show_history(selected_template));
		} else {
			rev_hist.hide();
		}

		detail.show();
	} else {
		detail.hide();
	}
}

function validate_fields (req_fields, data) {
	var valid = true;
	
	for (var i=0; i<req_fields.length; i++) {
		valid = valid && data[req_fields[i]];
	}
	
	return valid;
}

function add_template (data) {
	var required = [
		'template_name',
		'template_text'
	];
	
	if (validate_fields(required, data)) {
		var req_data = {
			action: 'add_template',
			data: data
		};
		$.post(processor, req_data).done(function (data) {
			if (data === null) {
				console.log(data);
				$.growl('Template archived', {type: 'success'});
				build_template_select();
			} else {
				$.growl('Unable to save the template.', {type: 'danger'});
			}
		});
	}
}

function update_template (data) {
	var required = [
		'template_name',
		'template_text'
	];


	if (validate_fields(required, data)) {
		data.template_ID = selected_template.ID;
		var req_data = {
			action: 'update_template',
			data: data
		};

		$.post(processor, req_data).done(function (data) {
			load_template(data[0].template_ID, show_template);
			build_template_select();

			if (data[0].template_ID > 0) {
				$.growl('Template updated.', {type: 'success'});
			} else {
				$.growl('Unable to update template.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function archive_template (data) {
	data.operator_ID = LOGIN.operator.ID;
	data.template_ID = selected_template.ID;
	var req_data = {
			action: 'archive_template',
			data: data
	};
	$.post(processor, req_data).done(function (data) {
		if (data === null) {
			$.growl('Template archived', {type: 'success'});
			$('#template_detail_panel').hide();
			build_template_select();
		} else {
			$.growl('Unable to archive the template.', {type: 'danger'});
		}		
	});
}

function show_history (data) {
	var req_data = {
		action: 'get_archive_hist',
		data: data
	};
	$.post(processor, req_data).done(function (data) {
		var tbl = $('.hist_table tbody');
		tbl.empty();
		for(i = 1; i < data.length; i++){
			var tr = $('<tr></tr>');
			tr
				.append($('<td></td>').html(data[i].template_name))
				.append($('<pre style="max-height:125px;"><code></code></pre>').html(data[i].template_text.replace(/</g, "&lt").replace(/>/g, "&gt")))
				.append($('<td></td>').html(data[i].template_active?'Yes':'No'))
				.append($('<td></td>').html(data[i].revision))
				.append($('<td></td>').html(data[i].archived_timestamp.date.slice(0,19)))
				.append($('<td></td>').html(data[i].name));
			tbl.append(tr);
		}
	});
	
}

function afterMoveInstruction() {
	$.growl('Instruction moved!', {type: 'success'});
	load_template(selected_template.ID, show_template);
}

//END OF INSTRUCTIONS