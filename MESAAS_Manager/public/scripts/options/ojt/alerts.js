var processor = './ajax_processors/options/ojt/alerts.php';
var departments = [];
var areas = [];

$(document).ready(
	function() {
		loadDepartmentsAndAreas();

		$('#selArea').on('change', function() {
			if ($(this).val() != '') {
				$('#btn_add_alert').removeClass('disabled');
				$('#tbl_alerts').show();
				$('#tbl_alerts TBODY').html('<tr><td colspan="6" class="text-center">Loading alerts...</td></tr>');
				
				$('#area_ID').val($(this).val());
				loadAlerts($(this).val());
			} else {
				$('#btn_add_alert').addClass('disabled');
				$('#tbl_area_specific_jobs').hide();
			}
		});
		
		$('#btn_add_alert').on('click', function() {
			displayAddEditAlert();
		});
		
		$('#txt_start_date,#txt_end_date').on('changeDate', function(ev){
			$(this).datepicker('hide');
		});
		
		
		$('#tbl_alerts').on('click', '.editalert', function() {
			var data = $(this).data();
			displayAddEditAlert(data.qa_area_ID, data.ID, data);
		});
		
		
		$('#tbl_alerts').on('click', '.deletealert', function() {
			$('.btn-confirm-delete-alert').data($(this).data());
			$('#div_delete_alert_modal').modal('show');
		});
		
		$('.btn-confirm-delete-alert').on('click', function() {
			var data = $(this).data();
			
			$.getJSON(processor, {"action":"delete_alert", "alert_ID":data.ID, "area_ID":$('#selArea').val()}, populateAlerts);
		});

		$('#a_save_alert').on('click', function(e) {
			e.preventDefault();
			$('#frm_alert').submit();
		});

		$('.glyphicon-question-sign').tooltip();
		$('#txt_start_date,#txt_end_date').datepicker();
	}
);

function loadDepartmentsAndAreas() {
	$.getJSON(processor, {"action":"get_departments_and_areas"}, populateDepartmentsAndAreas);
}

function populateDepartmentsAndAreas(jso) {
	departments = jso.departments;
	areas = jso.areas;
	
	var s = $('#selArea').html('<option></option>');
	for (var i = 0; i < departments.length; i++) {
		for (var j = 0; j < areas.length; j++) {
			if (departments[i].ID == areas[j].dept_ID) {
				var op = $('<OPTION />');
				op.val(areas[j].ID);
				op.html(departments[i].department_name + ': ' + areas[j].area_desc);
		
				s.append(op);
			}
		}
	}
}

function loadAlerts(area_ID) {
	$.getJSON(processor, {"action":"get_alerts", "area_ID":area_ID}, populateAlerts);
}

function populateAlerts(jso) {
	$('.modal').modal('hide');
	
	jso = jso||[];
	var tb = $('#tbl_alerts TBODY').html('');
	if (jso.length == 0) {
		tb.html('<tr><td colspan="6" class="text-center">No alerts defined.</td></tr>');
	} else {
		for (var i = 0; i < jso.length; i++) {
			var r = $('<tr />');
			r.append($('<td />').html(jso[i].qa_title));
			r.append($('<td />').html(phpDate(jso[i].start_date)));
			r.append($('<td />').html(phpDate(jso[i].end_date)));
			
			var btn = $('<button />').html('Edit');
			btn.addClass('btn btn-info btn-xs editalert');
			btn.data(jso[i]);
			btn.data('job_ID', jso[i].ID);
			
			r.append($('<td />').append(btn));
			
			var btn = $('<button />').html('Delete');
			btn.addClass('btn btn-info btn-xs deletealert');
			btn.data(jso[i]);
			btn.data('alert_ID', jso[i].ID);
			
			r.append($('<td />').append(btn));
			
			tb.append(r);
		}
	}
}


function displayAddEditAlert(area_ID, alert_ID, data) {
	$('#a_save_alert').data('area_ID', area_ID);
	$('#a_save_alert').data('alert_ID', alert_ID);
	$('#div_manage_body').show();
	$('#div_manage_info').hide();
	
	if (alert_ID == null) {
		$('#frm_alert')[0].reset();
		$('#p_leave_msg').hide();
		$('#alert_ID').val('');
		$('#grp_reset_signoff').hide();
		var area_desc = $.grep(areas, function(o, i) {return o.ID == $('#selArea').val();})[0].area_desc;
		$('#h_manage_alert').html('<span class="glyphicon glyphicon-plus"></span> Add Alert To: ' + area_desc);
	} else {
		$('#p_leave_msg').show().html('');
		$('#grp_reset_signoff').show();
		$('#div_manage_alert').modal();
		$('#alert_ID').val(data.ID);
		var alert_ID = data.item_prefix + '-';
		for (var i = (data.ID).toString().length; i < 3; i++) {
			alert_ID += '0';
		}
		alert_ID += data.ID;
		$('#h_manage_alert').html('<span class="glyphicon glyphicon-edit"></span> Edit Alert: ' + alert_ID);
		$('#sel_item_prefix').val(data.item_prefix);
		$('#txt_title').val(data.qa_title);
		$('#txt_start_date').val(phpDate(data.start_date));
		$('#txt_end_date').val(phpDate(data.end_date.date));
		$('#sel_severity').val(data.severity);
	
		$('#p_leave_msg').html('<i>Leave blank to keep existing file: ' + data.qa_link + '</i>');
	}
	
	$('#div_manage_alert').modal();
}

function validateAlert() {
	if ($('#sel_item_prefix').val() == '') {
		$('#grp_item_prefix').addClass('has-error');
		return false;
	} else {
		$('#grp_item_prefix').removeClass('has-error');
	}

	if ($('#txt_title').val() == '') {
		$('#grp_title').addClass('has-error');
		return false;
	} else {
		$('#grp_title').removeClass('has-error');
	}

	if ($('#alert_ID').val() == '') {
		if ($('#txt_file').val() == '') {
			$('#grp_file').addClass('has-error');
			return false;
		} else {
			$('#grp_file').removeClass('has-error');
		}
	}

	if ($('#txt_start_date').val() == '') {
		$('#grp_start_date').addClass('has-error');
		return false;
	} else {
		$('#grp_start_date').removeClass('has-error');
	}

	if ($('#txt_end_date').val() == '') {
		$('#grp_end_date').addClass('has-error');
		return false;
	} else {
		$('#grp_end_date').removeClass('has-error');
	}
	if ($('#sel_severity').val() == '') {
		$('#grp_severity').addClass('has-error');
		return false;
	} else {
		$('#grp_severity').removeClass('has-error');
	}

	$('#div_manage_body').hide();
	return true;
}

function phpDate(d) {
	if (d.date) {
		d = d.date;
	}
	
	return Date.Format(d, 'mm/dd/yyyy');
}