$.extend(NYSUS.OJT, {
	processor_datable_editor: './ajax_processors/options/ojt/datatables/requirements.php',
	selected_ojt_group_ID: null,
	selected_ojt_group_description: null,
	with_alerts: false,
	with_inactive_groups: false,
	ojt_requirements_editor: {
		ajax: './ajax_processors/options/ojt/datatables/requirements.php',
		table: '#table_ojt_requirements',
		fields: [
			{
				"label": "Requirement Type",
				"name": "ojt_requirements.ojt_requirement_type_ID",
				"type": "select",
				"placeholderDisabled": false,
				"placeholder": "Select Type"
			},
			{
				"label": "Signoff Template",
				"name": "ojt_requirements.ojt_signoff_template_ID",
				"type": "select",
				"placeholderDisabled": false,
				"placeholder": "Select Template"
			},
			{
				"label": "Requirement Description",
				"name": "ojt_requirements.requirement_description"
			},
			{
				"label": "Expire in Days",
				"name": "ojt_requirements.expire_days"
			},
			{
				"label": "Video Url",
				"name": "ojt_requirements.video_url"
			},
			{
				"label": "Image",
				"name": "ojt_requirements.image_url",
				"type": "upload",
				"display": function (data) {
					if (data.endsWith('.pdf')) {
						//return `<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../${data}"></iframe>`;
						return '<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + data + '"></iframe>';
					}
					return '<img src="' + data + '" width="100px" height="100px" />';
				},
				"clearText": "Clear",
				"noImageText": 'No image'
			},
			{
				"label": "Requirement Text",
				"name": "ojt_requirements.requirement_text"
			},
			{
				"label": "Allow MES Signoff",
				"name": "ojt_requirements.allow_mes_signoff",
				"type": "checkbox",
				"default": 0,
				"separator": ",",
				"options": [
					{label: "", value: 1}
				]
			},
			{
				"label": "Active",
				"name": "ojt_requirements.active",
				"type": "checkbox",
				"default": 0,
				"separator": ",",
				"options": [
					{label: "", value: 1}
				]
			}
		]
	},

	init: function () {
		var modal_add_group = $('.modal-add-group');
		var modal_add_group_operators = $('.modal_add_group_operators');
		var modal_add_requirement = $('.modal-add-requirement');
		var modal_edit_group = $('.modal-edit-group');
		var modal_edit_requirement = $('.modal-edit-requirement');
		var ojt_group_information = $('#ojt_group_information');
		var ojt_group_operators = $('#ojt_group_operators');
		var ojt_group_reqs = $('#ojt_group_requirements');
		var btn_edit_group = $('.btn-edit-group');
		var btn_add_requirement = $('.btn-add-requirement');
		var btn_show_requirement_alerts = $('.btn-show-requirement-alerts');
		var btn_add_operator = $('.btn-add-operator');
		var btn_show_active = $('.btn-groups-show-active');
		var ojt_group_tree = $('#ojt_group_tree');

		btn_edit_group.prop('disabled', app.selected_ojt_group_ID == null);
		btn_add_requirement.prop('disabled', app.selected_ojt_group_ID == null);
		btn_add_operator.prop('disabled', app.selected_ojt_group_ID == null);
		btn_show_requirement_alerts.prop('disabled', app.selected_ojt_group_ID == null);

		app.render_ojt_requirements();

		$(document).on('nodeSelected', '#ojt_group_tree', function (event, data) {
			app.selected_ojt_group_ID = data.ojt_group_ID;
			app.selected_ojt_group_description = data.ojt_group_description;
			app.render();
		});

		btn_show_active.on('click', function () {
			app.with_inactive_groups = app.with_inactive_groups != true;
			if (app.with_inactive_groups) {
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				$(this).addClass('btn-danger').removeClass('btn-success');
				$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
			app.render_group_tree('#ojt_group_tree', app.with_inactive_groups);
		});

		// group
		ojt_group_information.on('click', '.btn-add-group', app.show_add_group_modal);

		ojt_group_information.on('click', '.btn-edit-group', app.show_edit_group_modal);

		modal_add_group.on('click', '.btn-confirm-add-group', function () {
			var ojt_parent_group_ID = modal_add_group.find('.sel_group_parent').val();
			var ojt_group_description = modal_add_group.find('.inp_group_description').val();

			app.create_group(ojt_parent_group_ID, ojt_group_description, function (jso) {
				//TODO add GROWL
				app.render_group_tree('#ojt_group_tree', app.with_inactive_groups)
			});

		});

		modal_edit_group.on('click', '.btn-confirm-edit-group', function () {
			var ojt_parent_group_ID = modal_edit_group.find('.sel_group_parent').val();
			var ojt_group_description = modal_edit_group.find('.inp_group_description').val();
			var active = modal_edit_group.find('.inp_group_active').prop('checked');

			app.update_group(app.selected_ojt_group_ID, ojt_parent_group_ID, ojt_group_description, active, function (jso) {
				//TODO add GROWL
				app.render_group_tree('#ojt_group_tree', app.with_inactive_groups);
				app.render_group_information();
			});
		});

		ojt_group_information.on('click', '.btn-group-active', function () {
			var data = ojt_group_information.data();
			var active = data.active == 1 ? 0 : 1;

			app.update_group(app.selected_ojt_group_ID, data.parent_group_ID, data.group_description, active, function (jso) {
				// TODO add growl;
				app.render_group_information();
				app.render_group_tree('#ojt_group_tree', app.with_inactive_groups);
			});

		});

		// requirements
		ojt_group_reqs.on('click', '.btn-edit-requirement', function () {
			var modal = $('.modal-edit-requirement');
			var data = $(this).closest('tr').data();

			modal.data(data);
			modal.find('.inp_requirement').html(data.requirement_description);
			modal.find('.inp_min_operators').val(data.min_operators);
			modal.modal('show');
		});

		modal_edit_requirement.on('click', '.btn-confirm-edit-requirement', function () {
			var modal = $('.modal-edit-requirement');
			var data = modal.data();
			var min_operators = isNaN(parseInt(modal.find('.inp_min_operators').val())) ? null : parseInt(modal.find('.inp_min_operators').val());

			app.update_group_requirement(data.ojt_group_ID, data.ojt_requirement_ID, min_operators, function (jso) {
				// TODO add growl
				app.render_group_requirements();
			});

		});

		ojt_group_reqs.on('click', '.btn-delete-requirement', function () {
			var data = $(this).closest('tr').data();
			var id = data.ojt_group_requirement_ID;

			$.confirm({
				title: 'Remove Group Requirement',
				text: 'Are you sure you want remove requirement from group?',
				confirm: function () {
					app.delete_group_requirement(id, function (jso) {
						// TODO add growl
						app.render_group_requirements();
					});
				},
				cancel: function () {
					// nothing to do
				}
			});

		});

		ojt_group_reqs.on('click', '.btn-add-requirement', function () {
			var sel = $('#sel_requirements');

			sel.empty();
			app.get_requirements(app.with_alerts, function (requirements) {
				// filter out requirements already in group
				app.get_group_requirements(app.selected_ojt_group_ID, app.with_alerts, function (group_requirements) {
					$.map(requirements, function (r) {
						var group_reqs = $.grep(group_requirements, function (gr) {
							return gr.ojt_requirement_ID == r.ID;
						});

						if (group_reqs.length == 0) {
							//sel.append(`<option value="${r.ID}">${r.requirement_description}</option>`);
							sel.append('<option value="' + r.ID + '">' + r.requirement_description + '</option>');
						}
					});
				});
			});

			sel.select2({
				placeholder: 'Requirements',
				width: '100%',
				allowClear: true,
				multiple: true
			}).val(null).trigger('change');
			modal_add_requirement.modal('show');
		});

		modal_add_requirement.on('click', '.btn-confirm-add-requirement', function () {
			var requirements = modal_add_requirement.find('#sel_requirements').val();
			var min_operators = modal_add_requirement.find('.inp_min_operators').val()

			$.map(requirements, function (v) {
				app.create_group_requirement(app.selected_ojt_group_ID, v, min_operators, function (jso) {
					// TODO add growl
					app.render_group_requirements();
				});
			});
		});

		// operators
		btn_add_operator.on('click', app.show_add_group_operator);

		modal_add_group_operators.on('click', '.btn-confirm-add-operators', function () {
			var sel = modal_add_group_operators.find('.sel_operators');
			var operators = sel.val();

			var count = 0;
			$.map(operators, function (v) {
				app.create_group_operator(app.selected_ojt_group_ID, v, function (jso) {
					count++;

					if (count == operators.length) {
						// TODO add growl
						app.render_group_operators();
					}
				});
			});

		});

		ojt_group_operators.on('click', '.btn-delete-operator', function () {
			var data = $(this).closest('tr').data();
			var id = data.ojt_group_operator_ID;

			$.confirm({
				title: 'Remove Group Operator',
				text: 'Are you sure you want remove operator from group?',
				confirm: function () {
					app.delete_group_operator(id, function (jso) {
						// TODO add growl
						app.render_group_operators();
					});
				},
				cancel: function () {
					// nothing to do
				}
			});

		});

		// show alerts event
		$('.btn-show-requirement-alerts').on('click', function () {
			app.with_alerts = app.with_alerts != true;
			if (app.with_alerts) {
				$(this).removeClass('btn-danger').addClass('btn-success');
				$(this).find('span').removeClass('glyphicon-remove').addClass('glyphicon-ok');
			} else {
				$(this).addClass('btn-danger').removeClass('btn-success');
				$(this).find('span').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			}
			app.render_group_requirements();
		});

	},

	render: function () {
		app.render_group_information();
		app.render_group_operators();
		app.render_group_requirements();
	},

	render_group_information: function () {
		var container = $('#ojt_group_information');
		var btn_edit_group = $('.btn-edit-group');

		app.get_group(app.selected_ojt_group_ID, function (jso) {
			if (jso && jso.length == 1) {
				var obj = jso[0];
				var active = $('<div class="btn btn-xs btn-group-active">Yes</div>').data(obj);

				obj.active == 1 ? active.addClass('btn-success').html('YES') : active.addClass('btn-danger').html('NO');
				container.find('.ojt_group_active').empty().append(active);
				container.find('.ojt_group_description').html(obj.group_description);
				container.find('.ojt_parent_group_description').html(obj.parent_group_description);
				container.find('.ojt_group_create_date').html(obj.created_date.date);
				container.data(obj);
			} else {
				container.find('.ojt_group_active').empty();
				container.find('.ojt_group_description').empty();
				container.find('.ojt_parent_group_description').empty();
				container.find('.ojt_group_create_date').empty();
				container.data(null);
			}
			btn_edit_group.prop('disabled', app.selected_ojt_group_ID == null);

		});
	},

	render_group_operators: function () {
		var container = $('#ojt_group_operators').find('.panel-body');
		var btn_add_operator = $('.btn-add-operator');

		app.get_group_operators(app.selected_ojt_group_ID, function (jso) {

			if (jso && jso.length > 0) {
				var table = $('<table class="table table-condensed table-responsive table-bordered table-hover table-group-operators" style="width:100%;">');
				var thead = $('<thead><tr><th>Actions</th><th>Owner</th><th>Badge</th><th>First Name</th><th>Last Name</th></tr></thead>');
				var tbody = $('<tbody>');

				table.append(thead);

				$.map(jso, function (v) {
					var row = $('<tr>');
					var actions = '<button class="btn btn-danger btn-xs btn-delete-operator" style="margin-left: 10px"><span class="glyphicon glyphicon-remove"></span> Delete</button>';

					row.data(v);
					row.append('<td>' + actions + '</td>');
					row.append('<td>' + v.group_description + '</td>');
					row.append('<td>' + v.badge_ID + '</td>');
					row.append('<td>' + v.first_name + '</td>');
					row.append('<td>' + v.last_name + '</td>');

					tbody.append(row);
				});

				table.append(tbody);

				container.empty().append(table);

				$('.ojt_operator_count').html(table.find('tbody').find('tr').length);

				app.use_datatables($('.table-group-operators'));
			} else {
				$('.ojt_operator_count').html('0');
				container.empty().append('No Assigned Operators');
			}
			btn_add_operator.prop('disabled', app.selected_ojt_group_ID == null);

		});
	},

	render_group_requirements: function () {
		var container = $('#ojt_group_requirements').find('.panel-body');
		var btn_add_requirement = $('.btn-add-requirement');
		var btn_show_requirement_alerts = $('.btn-show-requirement-alerts');

		app.get_group_requirements(app.selected_ojt_group_ID, app.with_alerts, function (jso) {

			if (jso && jso.length > 0) {
				var table = $('<table class="table table-condensed table-responsive table-bordered table-hover table-group-requirements" style="width:100%;">');
				var thead = $('<thead><tr><th>Actions</th><th>Owner</th><th>Type</th><th>Description</th><th>Min Req.</th></tr></thead>');
				var tbody = $('<tbody>');

				table.append(thead);

				$.map(jso, function (v) {
					var row = $('<tr>');
					var btn_active = "";
					var min_operators = v.min_operators == null ? 'no min' : v.min_operators;
					var actions = '<div class="btn-group" role="group" aria-label="..."><button class="btn btn-info btn-xs btn-edit-requirement"><span class="glyphicon glyphicon-pencil"></span> Edit</button>';

					actions += '<button class="btn btn-danger btn-xs btn-delete-requirement"><span class="glyphicon glyphicon-remove"></span> Delete</button></div>';

					row.data(v);
					row.append('<td>' + actions + '</td>');
					row.append('<td>' + v.group_description + '</td>');
					row.append('<td>' + v.type_description + '</td>');
					row.append('<td>' + v.requirement_description + '</td>');
					row.append('<td>' + min_operators + '</td>');

					tbody.append(row);
				});

				table.append(tbody);

				container.empty().append(table);

				$('.ojt_requirement_count').html(table.find('tbody').find('tr').length);

				app.use_datatables($('.table-group-requirements'));
			} else {
				$('.ojt_requirement_count').html('0');
				container.empty().append('No Assigned Requirements');
			}
			btn_add_requirement.prop('disabled', app.selected_ojt_group_ID == null);
			btn_show_requirement_alerts.prop('disabled', app.selected_ojt_group_ID == null);

		});

	},

	render_ojt_requirements: function () {
		var options = jQuery.extend({}, app.datatables_options);

		options.ajax = app.processor_datable_editor;
		options.fixedHeader = true;
		options.select = "single";
		options.columns = [
			{
				data: null,
				defaultContent: '',
				className: 'select-checkbox',
				orderable: false
			},
			{
				"data": "ojt_requirement_types.type_description"
			},
			{
				"data": "ojt_requirements.requirement_description"
			},
			{
				"data": "ojt_requirements.active",
				className: "dt-body-center",
				render: function (data, type, row) {
					var isActive = data == 1 ? true : false;
					return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
				}
			}
		];

		var table = $('#table_ojt_requirements').DataTable(options);
		var editor = new $.fn.dataTable.Editor(app.ojt_requirements_editor);
		new $.fn.dataTable.Buttons(table, [
			{
				text: 'Reload',
				action: function (e, dt, node, config) {
					dt.ajax.reload();
				}
			},
			{extend: "create", editor: editor},
			{extend: "edit", editor: editor}
			//{extend: "remove", editor: editor}
		]);

		table.buttons().container().prependTo($('#table_ojt_requirements_wrapper'));

	},

	show_add_group_modal: function () {
		var modal = $('.modal-add-group');
		var sel = modal.find('.sel_group_parent');

		modal.find('.inp_group_description').val('');

		app.get_groups(app.with_inactive_groups, function (jso) {
			sel.empty().append('<option></option>');

			$.map(jso, function (v) {
				//sel.append(`<option value="${v.ID}">${v.group_description}</option>`);
				sel.append('<option value="' + v.ID + '">' + v.group_description + '</option>');
			});

			sel.select2({placeholder: 'OJT Groups', width: '100%', allowClear: true}).val(null).trigger('change');

		});

		modal.modal('show');

	},

	show_edit_group_modal: function () {
		var modal = $('.modal-edit-group');
		var sel = modal.find('.sel_group_parent');

		app.get_group(app.selected_ojt_group_ID, function (jso) {
			var checked = jso[0].active == 1 ? 'checked' : '';

			modal.find('.inp_group_description').val(jso[0].group_description);
			modal.find('.inp_group_active').prop('checked', checked);

			app.get_groups(app.with_inactive_groups, function (jso) {
				var desc = '';
				var parent_ID = -1;

				// build the combo
				sel.empty().append('<option></option>');

				$.map(jso, function (v) {
					if (v.ID == app.selected_ojt_group_ID) {
						desc = v.group_description;
						parent_ID = v.parent_group_ID;
					}
					//sel.append(`<option value="${v.ID}">${v.group_description}</option>`);
					sel.append('<option value="' + v.ID + '">' + v.group_description + '</option>');
				});

				sel.select2({
					placeholder: 'OJT Groups',
					width: '100%',
					allowClear: true
				}).val(parent_ID).trigger("change");

			});

		});

		modal.modal('show');
	},

	show_add_group_operator: function () {
		var modal = $('.modal_add_group_operators');
		var sel = modal.find('.sel_operators');

		sel.empty();
		NYSUS.get_operators(false, function (jso) {
			$.map(jso, function (v) {
				//sel.append(`<option value="${v.ID}">${v.badge_ID + ': ' + v.last_name + ', ' + v.first_name}</option>`);
				sel.append('<option value="' + v.ID + '">' + v.badge_ID + ': ' + v.last_name + ', ' + v.first_name + '</option>');
			});
		});

		sel.select2({
			placeholder: 'Operators',
			width: '100%',
			allowClear: true,
			multiple: true
		}).val(null).trigger('change');

		modal.modal('show');
	},

	use_datatables: function (table) {

		if ($(table).find('tbody tr').size() > app.datatables_options.lengthMenu[0][0]) {
			app.datatables_options.paging = true;
			app.datatables_options.info = true;
		} else {
			app.datatables_options.paging = false;
			app.datatables_options.info = false;
		}
		$(table).DataTable(app.datatables_options);
	}

});

var app = NYSUS.OJT;
$(document).ready(function () {
	app.render_group_tree('#ojt_group_tree');
	app.init();
});