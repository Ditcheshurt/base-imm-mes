var processor = './ajax_processors/options/ojt/ojt_jobs.php';
var departments = [];
var areas = [];

$(document).ready(
	function() {
		loadDepartmentsAndAreasAndSubAreas();
		loadJobs(null, null, null);

		$('#selDept').on('change', function() {
			if ($(this).val() != '') {
				$('#btn_add_dept_job').removeClass('disabled');
				$('#tbl_dept_specific_jobs').show();
				$('#tbl_dept_specific_jobs TBODY').html('<tr><td colspan="6" class="text-center">Loading jobs...</td></tr>');
				loadJobs($(this).val(), null, null);
			} else {
				$('#btn_add_dept_job').addClass('disabled');
				$('#tbl_dept_specific_jobs').hide();
			}
		});

		$('#selArea').on('change', function() {
			if ($(this).val() != '') {
				$('#btn_add_area_job').removeClass('disabled');
				$('#tbl_area_specific_jobs').show();
				$('#tbl_area_specific_jobs TBODY').html('<tr><td colspan="6" class="text-center">Loading jobs...</td></tr>');
				loadJobs(null, $(this).val(), null);
			} else {
				$('#btn_add_area_job').addClass('disabled');
				$('#tbl_area_specific_jobs').hide();
			}
		});

		$('#selSubArea').on('change', function() {
			if ($(this).val() != '') {
				$('#btn_add_sub_area_job').removeClass('disabled');
				$('#tbl_sub_area_specific_jobs').show();
				$('#tbl_sub_area_specific_jobs TBODY').html('<tr><td colspan="6" class="text-center">Loading jobs...</td></tr>');
				loadJobs(null, null, $(this).val());
			} else {
				$('#btn_add_sub_area_job').addClass('disabled');
				$('#tbl_sub_area_specific_jobs').hide();
			}
		});

		
		$('#btn_add_plant_job').on('click', function() {
			displayAddEditJob(null, null, null, null);
		});
		$('#btn_add_dept_job').on('click', function() {
			displayAddEditJob($('#selDept').val(), null, null);
		});
		$('#btn_add_area_job').on('click', function() {
			displayAddEditJob(null, $('#selArea').val(), null, null);
		});
		$('#btn_add_sub_area_job').on('click', function() {
			displayAddEditJob(null, null, $('#selSubArea').val(), null);
		});
		
		$('#txt_job_name, #txt_min_operators_qualified').on('keyup', function(e) {
			var el = $(e.target);
			if (el.val() == '') {
				$('#a_save_job').addClass('disabled');
			} else {
				$('#a_save_job').removeClass('disabled');
			}
		});
		
		$('#a_save_job').on('click', function(e) {
			e.preventDefault();
			$('#txt_job_name').addClass('disabled');
			$('#txt_job_details').addClass('disabled');
			$('#txt_min_operators_qualified').addClass('disabled');
			$('#a_save_job').addClass('disabled');
			$('#div_manage_body').hide();
		
			var params = {
							"action":"save_job",
							"dept_ID":$('#a_save_job').data('dept_ID'),
							"area_ID":$('#a_save_job').data('area_ID'),
							"sub_area_ID":$('#a_save_job').data('sub_area_ID'),
							"job_ID":$('#a_save_job').data('job_ID'),
							"job_name":$('#txt_job_name').val(),
							"job_details":$('#txt_job_details').val(),
							"min_operators_qualified":$('#txt_min_operators_qualified').val(),
							"active":(($('#btn_active').html() == 'Yes')?1:0)
						};
			$.getJSON(processor, params,
				function(res) {
					loadJobs(params['dept_ID'], params['area_ID'], params['sub_area_ID']);
					$('#div_manage_info').html('OJT job saved!').show().attr("class", "alert alert-success");
					setTimeout(function() {
							$('#div_add_job').modal('hide');
						}, 2000);
				}
			);
		});
		
		$(document.body).on('click', '.editjob', function(e) {
			e.preventDefault();
			var el = $(e.target);
			displayAddEditJob(el.data('department_ID'), el.data('area_ID'), el.data('sub_area_ID'), el.data('job_ID'), el.data());
		});
		
		$(document.body).on('click', '#btn_active', function(e) {
			e.preventDefault();
			if (e.target.innerHTML == 'Yes') {
				$('#btn_active').html('No').attr("class", "label label-danger");
			} else {
				$('#btn_active').html('Yes').attr("class", "label label-success");
			}
		});

		$('.glyphicon-question-sign').tooltip();
	}
);

function loadDepartmentsAndAreasAndSubAreas() {
	$.getJSON(processor, {"action":"get_departments_and_areas_and_sub_areas"}, populateDepartmentsAndAreasAndSubAreas);
}

function populateDepartmentsAndAreasAndSubAreas(jso) {
	departments = jso.departments;
	areas = jso.areas;
	sub_areas = jso.sub_areas;
	
	var s = $('#selDept').html('<option></option>');
	for (var i = 0; i < departments.length; i++) {
		var op = $('<option></option>');
		op.val(departments[i].ID);
		op.html(departments[i].department_name);
		
		s.append(op);
	}
	
	var s = $('#selArea').html('<option></option>');
	for (var i = 0; i < departments.length; i++) {
		for (var j = 0; j < areas.length; j++) {
			if (departments[i].ID == areas[j].dept_ID) {
				var op = $('<option></option>');
				op.val(areas[j].ID);
				op.html(departments[i].department_name + ': ' + areas[j].area_desc);
		
				s.append(op);
			}
		}
	}

	var s = $('#selSubArea').html('<option></option>');
	for (var i = 0; i < departments.length; i++) {
		for (var j = 0; j < areas.length; j++) {
			for (var k = 0; k < sub_areas.length; k++) {
				if (departments[i].ID == areas[j].dept_ID && areas[j].ID == sub_areas[k].area_ID) {
					var op = $('<option></option>');
					op.val(sub_areas[k].ID);
					op.html(departments[i].department_name + ': ' + areas[j].area_desc + ' - ' + sub_areas[k].sub_area_desc);
					s.append(op);
				}
			}
		}
	}
}

function loadJobs(dept_ID, area_ID, sub_area_ID) {
	if (dept_ID == null && area_ID == null && sub_area_ID == null) {
		$.getJSON(processor, {"action":"get_jobs", "dept_ID":dept_ID, "area_ID":area_ID, "sub_area_ID":sub_area_ID}, populatePlantJobs);
	}
	if (dept_ID != null && area_ID == null && sub_area_ID == null) {
		$.getJSON(processor, {"action":"get_jobs", "dept_ID":dept_ID, "area_ID":area_ID, "sub_area_ID":sub_area_ID}, populateDeptJobs);
	}
	if (dept_ID == null && area_ID != null && sub_area_ID == null) {
		$.getJSON(processor, {"action":"get_jobs", "dept_ID":dept_ID, "area_ID":area_ID, "sub_area_ID":sub_area_ID}, populateAreaJobs);
	}
	if (dept_ID == null && area_ID == null && sub_area_ID != null) {
		$.getJSON(processor, {"action":"get_jobs", "dept_ID":dept_ID, "area_ID":area_ID, "sub_area_ID":sub_area_ID}, populateSubAreaJobs);
	}
}

function populatePlantJobs(jso) {
	populateJobs(jso, 'tbl_plant_wide_jobs');
}

function populateDeptJobs(jso) {
	populateJobs(jso, 'tbl_dept_specific_jobs');
}

function populateAreaJobs(jso) {
	populateJobs(jso, 'tbl_area_specific_jobs');
}

function populateSubAreaJobs(jso) {
	populateJobs(jso, 'tbl_sub_area_specific_jobs');
}

function populateJobs(jso, target_el) {
	jso = jso||[];
	var tb = $('#' + target_el + ' TBODY').html('');
	if (jso.length == 0) {
		tb.html('<tr><td colspan="6" class="text-center">No jobs defined.</td></tr>');
	} else {
		for (var i = 0; i < jso.length; i++) {
			var r = $('<tr />');
			r.append($('<td />').html(jso[i].job_name));
			r.append($('<td />').html(jso[i].job_details));
			r.append($('<td />').html(jso[i].num_signoffs));
			r.append($('<td />').html( (jso[i].active)?'<span class="label label-success">Yes</span>':'<span class="label label-danger">No</span>'  ));
			
			var btn = $('<button />').html('Edit');
			btn.addClass('btn btn-info btn-xs editjob');
			btn.data(jso[i]);
			btn.data('job_ID', jso[i].ID);
			
			r.append($('<td />').append(btn));
			
			tb.append(r);
		}
	}
}


function displayAddEditJob(dept_ID, area_ID, sub_area_ID, job_ID, data) {
	$('#a_save_job').data('dept_ID', dept_ID);
	$('#a_save_job').data('area_ID', area_ID);
	$('#a_save_job').data('sub_area_ID', sub_area_ID);
	$('#a_save_job').data('job_ID', job_ID);
	$('#div_manage_body').show();
	$('#div_manage_info').hide();
	
	if (job_ID == null) {
		//add mode
		$('#txt_job_name').val('');
		$('#txt_job_details').val('');
		$('#txt_min_operators_qualified').val(0);
		
		if (dept_ID == null && area_ID == null && sub_area_ID == null) {
			$('#h_add_job').html('Add Plant-Wide Job');
		}
		if (dept_ID != null && area_ID == null && sub_area_ID == null) {
			var department_name = $.grep(departments, function(o, i) {return o.ID == $('#selDept').val();})[0].department_name;
			$('#h_add_job').html('Add Department-Specific Job To: ' + department_name);
		}
		if (dept_ID == null && area_ID != null && sub_area_ID == null) {
			var area_desc = $.grep(areas, function(o, i) {return o.ID == $('#selArea').val();})[0].area_desc;
			$('#h_add_job').html('Add Area-Specific Job To: ' + area_desc);
		}
		if (dept_ID == null && area_ID == null && sub_area_ID != null) {
			var sub_area_desc = $.grep(sub_areas, function(o, i) {return o.ID == $('#selSubArea').val();})[0].sub_area_desc;
			$('#h_add_job').html('Add Group-Specific Job To: ' + sub_area_desc);
		}
		// $('#div_active_buttons').hide();
	} else {
		// $('#div_active_buttons').show();
		// if (data.active) {
		// 	$('#btn_active').html('Yes').attr("class", "label label-success");
		// } else {
		// 	$('#btn_active').html('No').attr("class", "label label-danger");
		// }

		$('#h_add_job').html('Edit OJT Job');
		$('#txt_job_name').val(data.job_name);
		$('#txt_job_details').val(data.job_details);
		$('#txt_min_operators_qualified').val(data.min_operators_qualified);
		$('#a_save_job').removeClass('disabled');
	}

	if (data && data.active) {
		$('#btn_active').html('Yes').attr("class", "label label-success");
	} else {
		$('#btn_active').html('No').attr("class", "label label-danger");
	}
	
	$('#txt_job_name').removeClass('disabled');
	$('#txt_job_details').removeClass('disabled');
	$('#txt_min_operators_qualified').removeClass('disabled');
	
	$('#div_add_job').modal('show');
}