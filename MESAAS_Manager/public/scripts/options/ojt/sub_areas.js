var processor = './ajax_processors/options/ojt/sub_areas.php';
var filter_processor = './ajax_processors/filter.php';
var lang_strings;

var area_select;

var edit_sub_area_modal;
var sub_areas_table;


$(document).ready(function () {
	area_select = $('#area_select');
	sub_areas_table = $('#sub_areas_table');
	edit_sub_area_modal = $('#edit_sub_area_modal');

	area_select.on('change', function (e) {
		var area_ID = $(this).val();
		load_sub_areas(area_ID, show_sub_areas);

		if (area_ID > 0) {
			$('.btn-add-sub-area').show();
		} else {
			$('.btn-add-sub-area').hide();
		}
	});

	edit_sub_area_modal.on('hidden.bs.modal', reset_sub_area_modal);

	$('.btn-add-sub-area').on('click', function (e) {
		reset_sub_area_modal();
		edit_sub_area_modal.find('.modal-title').html('Add Group');
		edit_sub_area_modal.attr('data-action', 'insert_sub_area');
		edit_sub_area_modal.modal('show').data(null);
	});

	$('#sub_areas_table tbody').on('click', '.btn-edit-sub-area', function (e) {
		var sub_area_ID = $(this).closest('tr').attr('data-sub-area-id');
		load_sub_area(sub_area_ID, show_sub_area_modal);
	});

	edit_sub_area_modal.find('.btn-confirm-edit').on('click', commit_sub_area);

	load_areas(show_areas);
});

function load_areas (callback) {
    var params = {
        action: 'get_departments_areas',
    };

    $.getJSON(filter_processor, params).done(callback);
}

function show_areas (data) {
    area_select.empty();
    if (data) {
        $('<option></option>').prop({id: 'area_select_placeholder', name: 'area_ID'})
                              .text('Select an Area')
                              .val(-1)
                              .appendTo(area_select);

        data.forEach(function (department, i) {
            if (department.areas && department.areas.length > 0) {
                var group = $('<optgroup></optgroup>').prop({label: department.department_name})
                                                      .val(department.ID)
                                                      .appendTo(area_select);
                department.areas.forEach(function (area, j) {
                    $('<option></option>').prop({id: 'area_select_'+area.ID, name: 'area_ID'})
                                          .text(area.area_desc)
                                          .val(area.ID)
                                          .appendTo(group);
                });
            }
        });
        area_select.prop({disabled: false});
    }
}

function load_sub_areas (area_ID, callback) {
	$.getJSON(processor, {action: 'get_sub_areas', area_ID: area_ID}).done(callback);
}

function show_sub_areas (data) {
	var tbody = sub_areas_table.find('tbody');
	tbody.empty();

	if (data) {
		data.forEach(function (sub_area, i) {
			var mrp_part_number = sub_area.mrp_part_number;
			var tr = $('<tr></tr>').prop('id', 'sub_area_'+sub_area.ID)
								   .attr('data-sub-area-id', sub_area.ID)
								   .append($('<td>'+sub_area.sub_area_desc+'</td>'))
								   .append($('<td>'+sub_area.job_count+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-sub-area"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		});
	}
}

function reset_sub_area_modal () {
	edit_sub_area_modal.attr('data-sub-area-id', null);
	var f = edit_sub_area_modal.find('form')[0].reset();
}

function load_sub_area (sub_area_ID, callback) {
	$.getJSON(processor, {action: 'get_sub_area', sub_area_ID: sub_area_ID}).done(callback);
}

function show_sub_area_modal (data) {
	reset_sub_area_modal();
	var sub_area = data[0];
	$('#sub_area_desc_input').val(sub_area.sub_area_desc);

	edit_sub_area_modal.find('.modal-title').html('Edit Group');
	edit_sub_area_modal.attr('data-sub-area-id', sub_area.ID);
	edit_sub_area_modal.attr('data-action', 'update_sub_area');
	edit_sub_area_modal.modal('show').data(null);
}

function commit_sub_area (sub_area_data) {
	var required = [
		'sub_area_desc'
	];

	var req_data = {
		action: edit_sub_area_modal.attr('data-action'),
		area_ID: area_select.val(),
		sub_area_ID: edit_sub_area_modal.attr('data-sub-area-id'),
		sub_area_desc: $('#sub_area_desc_input').val()
	};

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && req_data[required[i]];
	}

	if (valid) {
		$.post(processor, req_data).done(function (data) {
			load_sub_areas(area_select.val(), show_sub_areas);
			if (data === null) {
				$.growl('Group '+(req_data.action == 'insert_sub_area' ? 'created' : 'updated')+'.', {type: 'success'});
			} else {
				$.growl('Unable to '+(req_data.action == 'insert_sub_area' ? 'create' : 'update')+' group.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}