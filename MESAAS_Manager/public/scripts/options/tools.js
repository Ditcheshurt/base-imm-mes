var processor = './ajax_processors/options/tools.php';
var lang_strings;

var tools;
var tool_select;
var selected_tool;
var edit_tool_modal;

var edit_tool_part_modal;
var tool_parts_table;
var remove_tool_part_modal;

var edit_tool_defect_reason_modal;
var remove_tool_defect_reason_modal;
var tool_defect_reasons_table;
var parent_defect_select;
var tool_defect_select;


$(document).ready(function () {
	edit_tool_modal = $('#edit_tool_modal');

	edit_tool_part_modal = $('#edit_tool_part_modal');
	tool_parts_table = $('#tool_parts_table');
	remove_tool_part_modal = $('#remove_tool_part_modal');

	edit_tool_defect_reason_modal = $('#edit_tool_defect_reason_modal');
	remove_tool_defect_reason_modal = $('#remove_tool_defect_reason_modal');
	tool_defect_reasons_table = $('#tool_defect_reasons_table');
	parent_defect_select = $('#parent_defect_select');
	tool_defect_select = $('#tool_defect_select');
	tool_part_image_form = $('#tool_part_image_form');

	$.getJSON('./config/language/app-en.json').done(function (data) {
		lang_strings = data;
		build_tool_select();
		init_image_upload();
	});

	edit_tool_part_modal.on('hidden.bs.modal', function (e) {
		reset_tool_part_modal();
	});

	$('.btn-add-tool').on('click', function (e) {
		edit_tool_modal.find('form')[0].reset();
		
		var parent_tool_select = $('#parent_tool_select').nysus_select({
			name: 'parent_tool_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_tools'},
			render: function (tool) {return tool.tool_description;},
			placeholder: 'Select Parent <span data-localize="tool.upper">Tool</span>'
		});

		edit_tool_modal.find('.modal-title').html('New <span data-localize="tool.upper">Tool</span>');
		edit_tool_modal.attr('data-action', 'insert_tool');
		edit_tool_modal.modal('show').data(null);
	});

	$('.btn-edit-tool').on('click', function (e) {
		edit_tool_modal.find('form')[0].reset();

		var parent_tool_select = $('#parent_tool_select').nysus_select({
			name: 'parent_tool_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_tools'},
			render: function (tool) {return tool.tool_description;},
			placeholder: 'Select Parent <span data-localize="tool.upper">Tool</span>',
			default_value: selected_tool.parent_ID
		});

		edit_tool_modal.find('input, textarea, select').each(function (i, input) {
			var field = $(input);
			if (field.prop('type') == 'checkbox') {
				field.prop('checked', selected_tool[field.prop('name')]);
			} else {
				field.val(selected_tool[field.prop('name')]);	
			}
		});

		edit_tool_modal.find('.modal-title').html('Edit <span data-localize="tool.upper">Tool</span>');
		edit_tool_modal.attr('data-action', 'update_tool');
		edit_tool_modal.modal('show').data(null);
	});

	edit_tool_modal.find('.btn-confirm-edit').on('click', function (e) {
		var tool_data = {
			short_description: edit_tool_modal.find('input[name="short_description"]').val(),
			tool_description: edit_tool_modal.find('textarea[name="tool_description"]').val(),
			parent_tool_ID: edit_tool_modal.find('select[name="parent_tool_ID"]').val(),
			active: edit_tool_modal.find('input[name="active"]').prop('checked') ? 1 : 0
		};

		delegate_tool_edit(tool_data);
	});

	$('.btn-add-tool-part').on('click', function (e) {
		reset_tool_part_modal();
		edit_tool_part_modal.find('.modal-title').html('Add Part');
		edit_tool_part_modal.attr('data-action', 'insert_tool_part');
		edit_tool_part_modal.modal('show').data(null);				
	});

	$('#tool_parts_table tbody').on('click', '.btn-edit-tool-part', function (e) {
		var tool_part_ID = $(this).closest('tr').attr('data-tool-part-id');
		load_tool_part(tool_part_ID, show_tool_part_modal);
	});

	edit_tool_part_modal.find('.btn-confirm-edit').on('click', function (e) {
		var tool_part_data = {
			tool_ID: selected_tool.ID,
			tool_part_ID: edit_tool_part_modal.attr('data-tool-part-id')
		};

		edit_tool_part_modal.find('input, textarea, select').each(function (i, field) {
			var f = $(field);
			var name = f.prop('name');

			if (name != 'ID') {
				if (f.prop('type') == 'checkbox') {
					tool_part_data[name] = f.prop('checked') ? 1 : 0;
				} else {
					tool_part_data[name] = f.val();
				}
			}
		});

		delegate_tool_part_edit(tool_part_data);
	});

	$('#tool_parts_table tbody').on('click', '.btn-remove-tool-part', function (e) {
 		var tool_part_ID = $(this).closest('tr').attr('data-tool-part-id');
 		remove_tool_part_modal.attr('data-action', 'delete_tool_part')
 								 .attr('data-tool-part-id', tool_part_ID)
 								 .modal('show').data(null);
 	});
 
 	remove_tool_part_modal.find('.btn-confirm-remove').on('click', function (e) {
 		var tool_part_ID = remove_tool_part_modal.attr('data-tool-part-id');
 		delete_tool_part(tool_part_ID);
 	});

	$('.btn-add-tool-defect-reason').on('click', function (e) {
		edit_tool_defect_reason_modal.find('input[type="text"], textarea').val('');
		edit_tool_defect_reason_modal.find('select').val(-1);
		edit_tool_defect_reason_modal.find('input[type="checkbox"]').prop('checked', false);

		parent_defect_select = $('#parent_defect_select').nysus_select({
			name: 'parent_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_parent_defect_reasons'},
			render: function (defect) {return defect.defect_description;},
			placeholder: 'Select Defect Type'
		});

		edit_tool_defect_reason_modal.find('.modal-title').html('Add Defect Code');
		edit_tool_defect_reason_modal.attr('data-action', 'insert_tool_defect_reason');
		edit_tool_defect_reason_modal.modal('show').data(null);
	});

	$('#tool_defect_reasons_table tbody').on('click', '.btn-edit-tool-defect-reason', function (e) {
		var tool_defect_reason_ID = $(this).closest('tr').attr('data-tool-defect-reason-id');
		load_tool_defect_reason(tool_defect_reason_ID, show_tool_defect_reason_modal);
	});

	edit_tool_defect_reason_modal.find('.btn-confirm-edit').on('click', function (e) {
		var tool_defect_reason_data = {
			tool_ID: selected_tool.ID,
			tool_defect_reason_ID: edit_tool_defect_reason_modal.attr('data-tool-defect-reason-id')
		};

		edit_tool_defect_reason_modal.find('input, textarea, select').each(function (i, field) {
			var f = $(field);
			var name = f.prop('name');

			if (name != 'ID') {
				if (f.prop('type') == 'checkbox') {
					tool_defect_reason_data[name] = f.prop('checked') ? 1 : 0;
				} else {
					tool_defect_reason_data[name] = f.val();
				}
			}
		});

		delegate_tool_defect_reason_edit(tool_defect_reason_data);
	});

	$('#tool_defect_reasons_table tbody').on('click', '.btn-remove-tool-defect-reason', function (e) {
		var tool_defect_reason_ID = $(this).closest('tr').attr('data-tool-defect-reason-id');
		remove_tool_defect_reason_modal.attr('data-action', 'delete_tool_defect_reason')
								 .attr('data-tool-defect-reason-id', tool_defect_reason_ID)
								 .modal('show').data(null);
	});

	remove_tool_defect_reason_modal.find('.btn-confirm-remove').on('click', function (e) {
		var tool_defect_reason_ID = remove_tool_defect_reason_modal.attr('data-tool-defect-reason-id');
		delete_tool_defect_reason(tool_defect_reason_ID);
	});

	parent_defect_select = $('#parent_defect_select');
	parent_defect_select.on('change', function (e) {
		var parent_ID = $(this).find('option:selected').val();

		tool_defect_select.nysus_select({
			name: 'ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_machine_defect_reasons', parent_ID: parent_ID, tool_ID: selected_tool.ID},
			render: function (defect) {return defect.defect_description;},
			placeholder: 'Select Defect Code'
		});
	});

	// ignore enter
	$(document).on('keyup keypress', function (e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			e.preventDefault();
			return false;
		}
	});
});

function reset_tool_modal () {
	edit_tool_modal.find('input[type="text"], textarea').val('');
	edit_tool_modal.find('select').val(-1);
	edit_tool_modal.find('input[type="checkbox"]').prop('checked', false);

	var parent_tool_select = $('#parent_tool_select').nysus_select({
		name: 'parent_tool_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_tools'},
		render: function (tool) {return tool.tool_description;},
		placeholder: 'Select Parent <span data-localize="tool.upper">Tool</span>'
	});
}

function delegate_tool_edit (tool_data) {
	var action = edit_tool_modal.attr('data-action');
	if (action == 'insert_tool') {
		insert_tool(tool_data);
	} else if (action == 'update_tool') {
		update_tool(tool_data);
	}
}

function delegate_tool_part_edit (tool_part_data) {
	var action = edit_tool_part_modal.attr('data-action');
	if (action == 'insert_tool_part') {
		insert_tool_part(tool_part_data);
	} else if (action == 'update_tool_part') {
		update_tool_part(tool_part_data);
	}
}

function delegate_tool_defect_reason_edit (tool_defect_reason_data) {
	var action = edit_tool_defect_reason_modal.attr('data-action');
	if (action == 'insert_tool_defect_reason') {
		insert_tool_defect_reason(tool_defect_reason_data);
	} else if (action == 'update_tool_defect_reason') {
		update_tool_defect_reason(tool_defect_reason_data);
	}
}

function build_tool_select () {
	tool_select = $('#tool_select').nysus_select({
		name: 'tool_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_tools'},
		render: function (tool) {return tool.tool_description;},
		callback: function (data) {
			tools = data;
		},
		placeholder: 'Select <span data-localize="tool.upper">Tool</span>'
	});

	tool_select.on('change', function (e) {
		var tool_ID = $('#tool_select option:selected').val();
		
		var res = $.grep(tools, function (m) {
			return m.ID == tool_ID;
		});
		selected_tool = res[0];

		load_tool(tool_ID, show_tool);
	});
}

function load_tool (tool_ID, callback) {
	var params = {action: 'get_tool', tool_ID: tool_ID};
	$.getJSON(processor, params).done(function (data) {
		if (data) {
			selected_tool = data[0];
		}

		load_tool_parts(tool_ID, show_tool_parts);
		load_tool_defect_reasons(tool_ID, show_tool_defect_reasons);
		load_ojt_groups(show_ojt_groups);

		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_tool (data) {
	var detail = $('#tool_detail_panel');
	var parts = $('#tool_parts_panel');
	var defect_reasons = $('#tool_defect_reasons_panel');
	var ojt = $('#tool_ojt_panel');

	if (data) {
		var short_description = data[0].short_description ? data[0].short_description : 'None';
		var tool_description = data[0].tool_description ? data[0].tool_description : 'None';
		var parent_tool_description = data[0].parent_tool_description ? data[0].parent_tool_description : 'None';
		var tool_active = data[0].active ? 'Yes' : 'No';

		detail.find('.short-description').html(short_description);
		detail.find('.description').html(tool_description);
		detail.find('.parent-tool-description').html(parent_tool_description);
		detail.find('.tool-active').html(tool_active);

		detail.show();
		parts.show();
		defect_reasons.show();
		ojt.show();
	} else {
		detail.hide();
		parts.hide();
		defect_reasons.hide();
		ojt.show();
	}
}

function load_tool_parts (tool_ID, callback) {
	var params = {action: 'get_tool_parts', tool_ID: tool_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_tool_parts (data) {
	var tbody = tool_parts_table.find('tbody');
	tbody.empty();

	if (data) {
		for (var i=0; i<data.length; i++) {
			var mrp_part_number = data[i].mrp_part_number;
			var tr = $('<tr></tr>').prop('id', 'tool_part_'+data[i].ID)
								   .attr('data-tool-part-id', data[i].ID)
								   //.append($('<td>'+data[i].operation+'</td>'))
								   //.append($('<td>'+data[i].sub_tool_order+'</td>'))
								   .append($('<td>'+data[i].part_number+'</td>'))
								   .append($('<td>'+(mrp_part_number ? mrp_part_number : 'None')+'</td>'))
								   .append($('<td>'+data[i].part_desc+'</td>'))
								   .append($('<td>'+data[i].parts_per_tool+'</td>'))
								   .append($('<td>'+(data[i].active ? 'Yes' : 'No')+'</td>'))
								   .append($('<td>'+data[i].last_update.date+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-tool-part"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-tool-part"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			//var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-tool-part"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		}
	}
}

function load_ojt_groups (callback) {
	var params = {action: 'get', as_nested: 1};
	$.getJSON('/common/api/ojt/groups.php', params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_ojt_groups (groups) {

}

function load_tool_part (tool_part_ID, callback) {
	var params = {action: 'get_tool_part', tool_part_ID: tool_part_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_tool_part_modal (data) {
	reset_tool_part_modal();

	part_select = $('#part_select').nysus_select({
		name: 'part_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_parts'},
		render: function (jso) {return jso.part_number + ' | ' + jso.part_desc + ' | ' + jso.mrp_part_number},
		callback: function (jso) { 

			var tool_part = data[0];
			edit_tool_part_modal.find('input, textarea, select').each(function (i, input) {
				var field = $(input);
				if (field.prop('type') == 'checkbox') {
					field.prop('checked', tool_part[field.prop('name')]);
				} else {
					field.val(tool_part[field.prop('name')]);	
				}
			});

			edit_tool_part_modal.find('.modal-title').html('Edit Part');
			edit_tool_part_modal.attr('data-tool-part-id', tool_part.ID);
			edit_tool_part_modal.find('.upload-form').attr('data-tool-part-id', tool_part.ID);
			edit_tool_part_modal.attr('data-action', 'update_tool_part');

			/*if (tool_part.part_image) {
				edit_tool_part_modal.find('.image-preview img').attr('src', tool_part.part_image);
				var image_preview = edit_tool_part_modal.find('.image-preview');
				var img = image_preview.find('img').clone().attr('src', tool_part.part_image);
				image_preview.html(img);
				show_preview();
			}*/

			edit_tool_part_modal.find('image-preview').show();

			edit_tool_part_modal.modal('show').data(null);

		},
		placeholder: 'Select Part'
	});
	
}

function reset_tool_part_modal () {
	edit_tool_part_modal.find('input[type="text"], textarea').val('');
	edit_tool_part_modal.find('select').val(-1);
	edit_tool_part_modal.find('input[type="checkbox"]').prop('checked', false);
	clear_upload_field();
	clear_preview();
	tool_part_image_form.attr('data-image-id', null);

	// set defaults
	edit_tool_part_modal.find('input[name="operation"]').val(10);
	edit_tool_part_modal.find('input[name="sub_tool_order"]').val(10);
	
}

function insert_tool (tool_data) {
	var required = [
		'short_description',
		'tool_description'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && tool_data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_tool', tool_data: tool_data};

		$.post(processor, req_data).done(function (data) {
			build_tool_select();
			if (data === null) {
				$.growl('New <span data-localize="tool.lower">tool</span> created.', {type: 'success'});
			} else {
				$.growl('Unable to create <span data-localize="tool.lower">tool</span>.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function update_tool (tool_data) {
	var required = [
		'short_description',
		'tool_description'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && tool_data[required[i]];
	}

	if (valid) {
		var req_data = {
			action: 'update_tool',
			tool_ID: selected_tool.ID,
			tool_data: tool_data
		};

		$.post(processor, req_data).done(function (data) {
			load_tool(selected_tool.ID, show_tool);
			if (data === null) {
				$.growl('<span data-localize="tool.upper">Tool</span> updated.', {type: 'success'});
			} else {
				$.growl('Unable to update <span data-localize="tool.lower">tool</span>.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function delete_tool_part (tool_part_ID) {
 	var req_data = {action: 'delete_tool_part', tool_part_ID: tool_part_ID};
 	$.post(processor, req_data).done(function (data) {
 		load_tool(selected_tool.ID, show_tool);
 		if (data === null) {
 			$.growl('Part removed.', {type: 'success'});
 		} else {
 			$.growl('Unable to remove part.', {type: 'danger'});
 		}
 	});
}


function insert_tool_part (tool_part_data) {
	var required = [
		'part_ID',
		'operation',
		'sub_tool_order',		
		'parts_per_tool'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && tool_part_data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_tool_part', tool_part_data: tool_part_data};

		$.post(processor, req_data).done(function (data) {
			if (data) {
				//$('.upload-form').attr('data-tool-part-id', data[0].ID);

				//upload_tool_part_image(function (data) {
					load_tool(selected_tool.ID, show_tool);
					if (data) {
						$.growl('Part added to <span data-localize="tool.lower">tool</span>.', {type: 'success'});
					} else {
						$.growl('Unable to add part.', {type: 'danger'});
					}
				//});
			} else {
				$.growl('Unable to add part.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function update_tool_part (tool_part_data) {
	var required = [
		'part_ID',
		'operation',
		'sub_tool_order',		
		'parts_per_tool'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && tool_part_data[required[i]];
	}

	if (valid) {
		//var image_input = tool_part_image_form.find(':file');
		//console.log(image_input.val());
		//if (image_input.val() == '') {
			var req_data = {action: 'update_tool_part', tool_part_data: tool_part_data};
			$.post(processor, req_data).done(function (data) {
				load_tool(selected_tool.ID, show_tool);
				if (data === null) {
					$.growl('Updated <span data-localize="tool.lower">tool</span>.', {type: 'success'});
				} else {
					$.growl('Unable to update <span data-localize="tool.lower">tool</span>.', {type: 'danger'});
				}
			});
		/*} else {
			upload_tool_part_image(function (data) {
				tool_part_data.part_image = data.location;
				var req_data = {action: 'update_tool_part', tool_part_data: tool_part_data};
				$.post(processor, req_data).done(function (data) {
					load_tool(selected_tool.ID, show_tool);
					if (data === null) {
						$.growl('Updated <span data-localize="tool.lower">tool</span>.', {type: 'success'});
					} else {
						$.growl('Unable to update <span data-localize="tool.lower">tool</span>.', {type: 'danger'});
					}
				});
			});
		}*/
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function load_tool_defect_reasons (tool_ID, callback) {
	var params = {action: 'get_tool_defect_reasons', tool_ID: tool_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_tool_defect_reasons (data) {
	var tbody = tool_defect_reasons_table.find('tbody');
	tbody.empty();

	if (data) {
		for (var i=0; i<data.length; i++) {
			var tr = $('<tr></tr>').prop('id', 'tool_defect_reason_'+data[i].ID)
								   .attr('data-tool-defect-reason-id', data[i].ID)
								   .append($('<td>'+data[i].parent_defect_description+'</td>'))
								   .append($('<td>'+data[i].defect_description+'</td>'))
								   .append($('<td>'+(data[i].active ? 'Yes' : 'No')+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			//var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-tool-part"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-tool-defect-reason"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		}
	}
}

function load_tool_defect_reason (tool_defect_reason_ID, callback) {
	var params = {action: 'get_tool_defect_reason', tool_defect_reason_ID: tool_defect_reason_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_tool_defect_reason_modal (data) {
	var tool_defect_reason = data[0];


	parent_defect_select = $('#parent_defect_select').nysus_select({
		name: 'parent_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_parent_defect_reasons'},
		render: function (defect) {return defect.defect_description;},
		placeholder: 'Select Defect Type',
		default_value: tool_defect_reason.parent_ID
	});

	edit_tool_defect_reason_modal.find('input, textarea, select').each(function (i, input) {
		var field = $(input);
		if (field.prop('type') == 'checkbox') {
			field.prop('checked', tool_defect_reason[field.prop('name')]);
		} else {
			field.val(tool_defect_reason[field.prop('name')]);	
		}
	});

	edit_tool_defect_reason_modal.find('.modal-title').html('Edit Defect Code');
	edit_tool_defect_reason_modal.attr('data-tool-defect-reason-id', tool_defect_reason.ID);
	edit_tool_defect_reason_modal.attr('data-action', 'update_tool_defect_reason');
	edit_tool_defect_reason_modal.modal('show').data(null);
}

function insert_tool_defect_reason (data) {
	var required = [
		'parent_ID',
		'defect_reason_ID'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_tool_defect_reason', tool_defect_reason_data: data};

		$.post(processor, req_data).done(function (data) {
			load_tool(selected_tool.ID, show_tool);
			if (data === null) {
				$.growl('Defect code added to <span data-localize="tool.lower">tool</span>.', {type: 'success'});
			} else {
				$.growl('Unable to add defect code.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function delete_tool_defect_reason (tool_defect_reason_ID) {
	var req_data = {action: 'delete_tool_defect_reason', tool_defect_reason_ID: tool_defect_reason_ID};

	$.post(processor, req_data).done(function (data) {
		load_tool(selected_tool.ID, show_tool);
		if (data === null) {
			$.growl('Defect code removed from <span data-localize="tool.lower">tool</span>.', {type: 'success'});
		} else {
			$.growl('Unable to remove defect code.', {type: 'danger'});
		}
	});
}


// image uploads

function init_image_upload () {
	tool_part_image_form = $('#tool_part_image_form');
    tool_part_image_form.attr('data-location', null);

    tool_part_image_form.on('change', '.btn-file :file', file_changed);

    tool_part_image_form.on('fileselect', '.btn-file :file',  files_selected);
}

function upload_progress (e) {
    var progress = $('.upload-form').find('.upload-group progress');

    if (e.lengthComputable) {
        progress.attr({value: e.loaded, max: e.total});
    }
}

function upload_complete (data) {
    $('.upload-form').attr('data-location', data.location);

    clear_upload_field();
}

function clear_upload_field () {
    var inputs = $('.upload-form').find('.upload-group').children(':file, :text');

    $.each(inputs, function (key, input) {
        input.replaceWith(input.clone());
        input.val('');
    });
    
    var progress = $('.upload-form').find('.upload-group progress');
    progress.attr('value', 0);
    progress.hide();
}

function show_preview () {
    var preview_img = $('.upload-form').find('.image-preview img');
    var input = $('.upload-form').find('.btn-file :file')[0];

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            preview_img.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    preview_img.closest('.image-preview').show();
}

function clear_preview () {
	var preview_img = $('.upload-form').find('.image-preview img');
	preview_img.attr('src', '#');
	$('.image-preview').hide();
}

function file_changed () {
    var input = $('.upload-form').find('.btn-file :file');
    var file_count = input.get(0).files ? input.get(0).files.length : 1;
    var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [file_count, label]);
    show_preview();
}

function files_selected (e, file_count, label) {
    var input = $(this).parents('.input-group').find(':text');
    var log = file_count > 1 ? file_count + ' files selected' : label;

    if (input.length) {
        input.val(log);
    } else if (log) {
        alert(log);
    }
}

function upload_tool_part_image (cb) {
    var form = $('.upload-form')[0];
    var form_data = new FormData(form);
    form_data.append('part_ID', $('.upload-form').attr('data-tool-part-id'));
    form_data.append('action', 'store_tool_part_image');
    
    var progress = $(form).find('progress');
    progress.attr('value', 0);
    progress.show();

    $.ajax({
        url: processor,
        type: 'POST',
        xhr: function() {
            var xhr = $.ajaxSettings.xhr();
            if (xhr.upload) {
                xhr.upload.addEventListener(
                    'progress',
                    function (e) {
                        upload_progress(e);
                    },
                    false
                );
            }
            return xhr;
        },
        success: function (data) {
            upload_complete(data);
            if (cb != undefined) {
            	cb(data);
            }
        },
        data: form_data,
        cache: false,
        contentType: false,
        processData: false
    });
}