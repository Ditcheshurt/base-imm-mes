var processor = './ajax_processors/options/machines.php';
var machines;
var machine_select;
var selected_machine;
var edit_machine_modal;
var edit_machine_form;
var edit_machine_tool_modal;
var edit_machine_ojt_job_modal;
var remove_machine_tool_modal;
var remove_machine_ojt_job_modal;
var machine_tools_table;
var machine_instructions_table;
//var machine_image_upload_form;

$(document).ready(function () {
	edit_machine_modal = $('#edit_machine_modal');
	edit_machine_form = $('#edit_machine_form');
	edit_machine_tool_modal = $('#edit_machine_tool_modal');
	remove_machine_tool_modal = $('#remove_machine_tool_modal');
	edit_machine_ojt_job_modal = $('#edit_machine_ojt_job_modal');
	remove_machine_ojt_job_modal = $('#remove_machine_ojt_job_modal');
	machine_tools_table = $('#machine_tools_table');
	machine_ojt_jobs_table = $('#machine_ojt_jobs_table');

	machine_instructions_table = $('#machine_instructions_table');
	edit_machine_instruction_modal = $('#edit_machine_instruction_modal');
	remove_machine_instruction_modal = $('#remove_machine_instruction_modal');

	build_machine_select();

	$('.btn-edit-machine').on('click', function (e) {
		//reset_machine_modal();
		edit_machine_modal.find('form')[0].reset();

		var machine_type_select = $('#machine_type_select').nysus_select({
			name: 'machine_type_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_machine_types'},
			render: function (machine_type) {return machine_type.machine_type_description;},
			placeholder: 'Select Machine Type',
			default_value: selected_machine.machine_type_ID
		});

		var machine_group_select = $('#machine_group_select').nysus_select({
			name: 'machine_group_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_machine_groups'},
			render: function (machine_group) {return machine_group.name;},
			placeholder: 'Select Machine Group',
			default_value: selected_machine.machine_group_ID
		});

		// load and show plc tags
		var plc_tag_group = edit_machine_modal.find('.primary-plc-tag-group');
		plc_tag_group.empty();

		var plc_tags = selected_machine.plc_tags;
		if (plc_tags) {
			$.each(plc_tags, function (i, tag) {
				var plc_tag_label = $('<label></label>').appendTo(plc_tag_group);
				console.log(tag);
				var is_primary = tag.is_primary == 1 ? true : false;
				console.log(is_primary);
				var plc_tag_radio = $('<input />').prop({type: 'radio', name: 'primary_plc_tag_ID'})
												  .attr('checked', is_primary)
												  .val(tag.ID)
												  .appendTo(plc_tag_label);
				plc_tag_label.append(' '+tag.name);
			});
		}

		edit_machine_modal.find('input, textarea, select').each(function (i, input) {
			var field = $(input);
			if (field.prop('type') == 'checkbox') {
				field.prop('checked', selected_machine[field.prop('name')]);
			} else if (field.prop('type') != 'radio') {
				field.val(selected_machine[field.prop('name')]);
			}

		});

		edit_machine_modal.find('.modal-title').html('Edit Machine');
		edit_machine_modal.attr('data-action', 'update_machine');
		edit_machine_modal.modal('show').data(null);
	});

	edit_machine_modal.find('.btn-confirm-edit').on('click', function (e) {
		var data = {
			machine_number: edit_machine_modal.find('input[name="machine_number"]').val(),
			machine_name: edit_machine_modal.find('input[name="machine_name"]').val(),
			machine_type_ID: edit_machine_modal.find('select[name="machine_type_ID"]').val(),
			machine_group_ID: edit_machine_modal.find('select[name="machine_group_ID"]').val(),
			station_IP_address: edit_machine_modal.find('input[name="station_IP_address"]').val(),
			//primary_plc_ip: edit_machine_modal.find('input[name="primary_plc_IP_address"]').val(),
			//primary_plc_tag_ID: edit_machine_modal.find('input[name="primary_plc_tag_ID"]:checked').val(),
			op_order: edit_machine_modal.find('input[name="op_order"]').val(),
			//over_cycle_grace_period: edit_machine_modal.find('input[name="over_cycle_grace_period"]').val()
		};

		update_machine(data);
	});
	
	edit_machine_modal.find('.btn-confirm-printer').on('click', function (e) {
		var data = {
			machine_number: edit_machine_modal.find('input[name="machine_number"]').val(),
			machine_name: edit_machine_modal.find('input[name="machine_name"]').val(),
			machine_type_ID: edit_machine_modal.find('select[name="machine_type_ID"]').val(),
			machine_group_ID: edit_machine_modal.find('select[name="machine_group_ID"]').val(),
			station_IP_address: edit_machine_modal.find('input[name="station_IP_address"]').val(),
			//primary_plc_tag_ID: edit_machine_modal.find('input[name="primary_plc_tag_ID"]:checked').val(),
			op_order: edit_machine_modal.find('input[name="op_order"]').val(),
			//over_cycle_grace_period: edit_machine_modal.find('input[name="over_cycle_grace_period"]').val()
		};

		update_machine(data);
	});
	
	$(".btn-add-printer").on('click',function(e){
		/*$.getJSON(processor, {"action":"add_printer", 
		"direction":"UP", 
		"machine_ID":selected_machine.ID, 
		"printer_name":$("#printer_name").val(),
		"ip":$("#printer_IP").val(),
		"part":$("#part_order").val(),
		"type":$("#label_type").val()},
		after_printer_add);	*/	
		
	});
	
	function after_printer_add(){
		edit_machine_tool_modal.modal('hide');
		load_machine(machine_ID, show_machine);
	}

	$('.btn-add-machine-tool').on('click', function (e) {
		edit_machine_tool_modal.find('form')[0].reset();

		var tool_select = $('#tool_select').nysus_select({
			name: 'tool_ID',
			key: 'ID',
			data_url: processor,
			params: {action: 'get_tools'},
			render: function (tool) {return tool.tool_description;},
			placeholder: 'Select <span data-localize="tool.upper">Tool</span>'
		});

		edit_machine_tool_modal.find('.modal-title').html('Add Machine <span data-localize="tool.upper">Tool</span>');
		edit_machine_tool_modal.attr('data-action', 'insert_machine_tool');
		edit_machine_tool_modal.modal('show').data(null);
	});

	$('#machine_tools_table tbody').on('click', '.btn-edit-machine-tool', function (e) {
		var machine_tool_ID = $(this).closest('tr').attr('data-machine-tool-id');
		load_machine_tool(machine_tool_ID, show_machine_tool_modal);
	});

	edit_machine_tool_modal.find('.btn-confirm-edit').on('click', function (e) {
		var machine_tool_data = {
			tool_ID: edit_machine_tool_modal.find('select[name="tool_ID"]').val(),
			target_cycle_time: edit_machine_tool_modal.find('input[name="target_cycle_time"]').val(),
			machine_ID: selected_machine.ID,
			machine_tool_ID: edit_machine_tool_modal.attr('data-machine-tool-id')
		};

		delegate_machine_tool_edit(machine_tool_data);

	});

	$('#machine_tools_table tbody').on('click', '.btn-remove-machine-tool', function (e) {
		var machine_tool_ID = $(this).closest('tr').attr('data-machine-tool-id');
		remove_machine_tool_modal.attr('data-action', 'delete_machine_tool')
								 .attr('data-machine-tool-id', machine_tool_ID)
								 .modal('show').data(null);
	});

	edit_machine_tool_modal.find('.btn-confirm-printer').on('click', function (e) {
		var machine_tool_data = {
			tool_ID: edit_machine_tool_modal.find('select[name="tool_ID"]').val(),
			target_cycle_time: edit_machine_tool_modal.find('input[name="target_cycle_time"]').val(),
			machine_ID: selected_machine.ID,
			machine_tool_ID: edit_machine_tool_modal.attr('data-machine-tool-id')
		};

		delegate_machine_tool_edit(machine_tool_data);

	});

	$('#machine_tools_table tbody').on('click', '.btn-remove-machine-tool', function (e) {
		var machine_tool_ID = $(this).closest('tr').attr('data-machine-tool-id');
		remove_machine_tool_modal.attr('data-action', 'delete_machine_tool')
								 .attr('data-machine-tool-id', machine_tool_ID)
								 .modal('show').data(null);
	});
	
	remove_machine_tool_modal.find('.btn-confirm-remove').on('click', function (e) {
		var machine_tool_ID = remove_machine_tool_modal.attr('data-machine-tool-id');
		delete_machine_tool(machine_tool_ID);
	});

	//OJT
	$('.btn-add-machine-ojt-job').on('click', function (e) {
		edit_machine_ojt_job_modal.find('form')[0].reset();

	    load_ojt_jobs(show_ojt_job_select);

		edit_machine_ojt_job_modal.find('.modal-title').html('Add OJT Job');
		edit_machine_ojt_job_modal.attr('data-action', 'insert_machine_ojt_job');
		edit_machine_ojt_job_modal.modal('show').data(null);
	});

	// $('#machine_ojt_jobs_table tbody').on('click', '.btn-edit-machine-ojt-job', function (e) {
	// 	var machine_ojt_job_ID = $(this).closest('tr').attr('data-machine-ojt-job-id');
	// 	load_machine_ojt_job(machine_ojt_job_ID, show_machine_ojt_job_modal);
	// });

	edit_machine_ojt_job_modal.find('.btn-confirm-edit').on('click', function (e) {
		var machine_ojt_job_data = {
			ojt_job_ID: edit_machine_ojt_job_modal.find('select[name="ojt_job_ID"]').val(),
			machine_ID: selected_machine.ID,
			//machine_ojt_job_ID: edit_machine_ojt_job_modal.attr('data-machine-ojt-job-id')
		};

		//delegate_machine_ojt_job_edit(machine_ojt_job_data);
		insert_machine_ojt_job(machine_ojt_job_data);
	});

	$('#machine_ojt_jobs_table tbody').on('click', '.btn-remove-machine-ojt-job', function (e) {
		var machine_ojt_job_ID = $(this).closest('tr').attr('data-machine-ojt-job-id');
		remove_machine_ojt_job_modal.attr('data-action', 'delete_machine_ojt_job')
								 .attr('data-machine-ojt-job-id', machine_ojt_job_ID)
								 .modal('show').data(null);
	});

	edit_machine_ojt_job_modal.find('.btn-confirm-printer').on('click', function (e) {
		var machine_ojt_job_data = {
			ojt_job_ID: edit_machine_ojt_job_modal.find('select[name="ojt_job_ID"]').val(),
			machine_ID: selected_machine.ID,
			//machine_ojt_job_ID: edit_machine_ojt_job_modal.attr('data-machine-ojt-job-id')
		};

		//delegate_machine_ojt_job_edit(machine_ojt_job_data);
		insert_machine_ojt_job(machine_ojt_job_data);
	});

	$('#machine_ojt_jobs_table tbody').on('click', '.btn-remove-machine-ojt-job', function (e) {
		var machine_ojt_job_ID = $(this).closest('tr').attr('data-machine-ojt-job-id');
		remove_machine_ojt_job_modal.attr('data-action', 'delete_machine_ojt_job')
								 .attr('data-machine-ojt-job-id', machine_ojt_job_ID)
								 .modal('show').data(null);
	});

	
	remove_machine_ojt_job_modal.find('.btn-confirm-remove').on('click', function (e) {
		var machine_ojt_job_ID = remove_machine_ojt_job_modal.attr('data-machine-ojt-job-id');
		delete_machine_ojt_job(machine_ojt_job_ID);
	});
	//END OF ojt

	//INSTRUCTIONS
	$('.btn-add-machine-instruction').on('click', function (e) {
		edit_machine_instruction_modal.find('form')[0].reset();
		edit_machine_instruction_modal.find('.modal-title').html('Add Instruction');
		edit_machine_instruction_modal.attr('data-action', 'insert_machine_instruction');
		$('#img_instr_image').attr('src', '');
		$('#instr_order').val(next_instr_order);
		edit_machine_instruction_modal.modal('show').data(null);
	});

	$('#machine_instructions_table tbody').on('click', '.btn-edit-machine-instruction', function (e) {
		var machine_instruction_ID = $(this).closest('tr').attr('data-machine-instruction-id');
		edit_machine_instruction_modal.attr('data-action', 'update_machine_instruction');
		edit_machine_instruction_modal.attr('data-machine-instruction-id', machine_instruction_ID);
		load_machine_instruction(machine_instruction_ID, show_machine_instruction_modal);
	});

	$('#machine_instructions_table tbody').on('click', '.btn-move-up-machine-instruction', function() {
		var machine_instruction_ID = $(this).closest('tr').attr('data-machine-instruction-id');
		$.getJSON(processor, {"action":"move_machine_instruction", "direction":"UP", "machine_ID":selected_machine.ID, "machine_instruction_ID":machine_instruction_ID}, afterMoveInstruction);
	});

	$('#machine_instructions_table tbody').on('click', '.btn-move-down-machine-instruction', function() {
		var machine_instruction_ID = $(this).closest('tr').attr('data-machine-instruction-id');
		$.getJSON(processor, {"action":"move_machine_instruction", "direction":"DOWN", "machine_ID":selected_machine.ID, "machine_instruction_ID":machine_instruction_ID}, afterMoveInstruction);
	});

	edit_machine_instruction_modal.find('.btn-confirm-edit').on('click', function (e) {
		var machine_instruction_data = {
			machine_instruction_ID: edit_machine_instruction_modal.attr('data-machine-instruction-id'),
			machine_ID: selected_machine.ID,
			instr_order: $('#instr_order').val(),
			title: $('#title').val(),
			instr_text: $('#instr_text').val(),
			instr_image: $('#instr_image').val()
		};

		if (isNaN(machine_instruction_data.instr_order)) {
			$.growl('Use only numbers for instruction order', {type: 'danger'});
			return;
		}

		delegate_machine_instruction_edit(machine_instruction_data);
	});
	edit_machine_instruction_modal.find('.btn-confirm-printer').on('click', function (e) {
		var machine_instruction_data = {
			machine_instruction_ID: edit_machine_instruction_modal.attr('data-machine-instruction-id'),
			machine_ID: selected_machine.ID,
			instr_order: $('#instr_order').val(),
			title: $('#title').val(),
			instr_text: $('#instr_text').val(),
			instr_image: $('#instr_image').val()
		};

		if (isNaN(machine_instruction_data.instr_order)) {
			$.growl('Use only numbers for instruction order', {type: 'danger'});
			return;
		}

		delegate_machine_instruction_edit(machine_instruction_data);
	});

	$('#machine_instructions_table tbody').on('click', '.btn-remove-machine-instruction', function (e) {
		var machine_instruction_ID = $(this).closest('tr').attr('data-machine-instruction-id');
		remove_machine_instruction_modal.attr('data-action', 'delete_machine_instruction')
								 .attr('data-machine-instruction-id', machine_instruction_ID)
								 .modal('show').data(null);
	});

	remove_machine_instruction_modal.find('.btn-confirm-remove').on('click', function (e) {
		var machine_instruction_ID = remove_machine_instruction_modal.attr('data-machine-instruction-id');
		delete_machine_instruction(machine_instruction_ID);
	});

	$('.btn-preview-machine-instructions').on('click', function (e) {
		load_machine_instructions(selected_machine.ID, preview_machine_instructions);
	});

	$('#instr_image').on('change', function() {
		//upload that mutha
		$('#action').val('upload_instruction_image');
		$('#image_upload_machine_ID').val(selected_machine.ID);
		$('#frm_image_upload')[0].submit();
	});

	//END OF INSTRUCTIONS


	// ignore enter
	$(document).on('keyup keypress', function (e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			e.preventDefault();
			return false;
		}
	});
});

function delegate_machine_tool_edit (machine_tool_data) {
	var action = edit_machine_tool_modal.attr('data-action');
	if (action == 'insert_machine_tool') {
		insert_machine_tool(machine_tool_data);
	} else if (action == 'update_machine_tool') {
		update_machine_tool(machine_tool_data);
	}
}

function build_machine_select () {
	machine_select = $('#machine_select').nysus_select({
		name: 'machine_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_machines'},
		render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		callback: function (data) {
			machines = data;
		},
		placeholder: 'Select Machine'
	});

	machine_select.on('change', function (e) {
		var machine_ID = $('#machine_select option:selected').val();

		var res = $.grep(machines, function (m) {
			return m.ID == machine_ID;
		});
		selected_machine = res[0];

		load_machine(machine_ID, show_machine);
	});
}

function load_machine (machine_ID, callback) {
	var params = {action: 'get_machine', machine_ID: machine_ID};
	$.getJSON(processor, params).done(function (data) {
		if (data) {
			selected_machine = data[0];
		}

		//load_machine_tools(machine_ID, show_machine_tools);
		load_machine_ojt_jobs(machine_ID, show_machine_ojt_jobs);
		load_machine_instructions(machine_ID, show_machine_instructions);
		load_machine_printers(machine_ID, show_machine_printers);

		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine (data) {
	var detail = $('#machine_detail_panel');
	var tools = $('#machine_tools_panel');
	var ojt_jobs = $('#machine_ojt_jobs_panel');
	var instructions = $('#machine_instructions_panel');
	var printers = $('#machine_printers_panel');

	if (data) {
		detail.find('.machine-number').html(data[0].machine_number);
		detail.find('.machine-name').html(data[0].machine_name);
		detail.find('.machine-type').html(data[0].machine_type);
		detail.find('.machine-group').html(data[0].machine_group);
		detail.find('.station-ip').html(data[0].station_IP_address);
		detail.find('.primary-plc-tag').html(data[0].primary_plc_tag_name);
		detail.find('.primary-plc-ip').html(data[0].primary_plc_ip);
		detail.find('.operation-order').html(data[0].op_order);
		detail.find('.over-cycle-grace-period').html(data[0].over_cycle_grace_period);

		detail.show();
		tools.show();
		printers.show();
		// ojt_jobs.show();
		instructions.show();
	} else {
		detail.hide();
		tools.hide();
		ojt_jobs.hide();
		instructions.hide();
	}
}

function load_machine_tools (machine_ID, callback) {
	var params = {action: 'get_machine_tools', machine_ID: machine_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine_tools (data) {
	var tbody = machine_tools_table.find('tbody');
	tbody.empty();

	if (data) {
		for (var i=0; i<data.length; i++) {
			var tr = $('<tr></tr>').prop('id', 'machine_tool_'+data[i].machine_tool_ID)
								   .attr('data-machine-tool-id', data[i].machine_tool_ID)
								   .append($('<td>'+data[i].tool_description+'</td>'))
								   .append($('<td>'+data[i].target_cycle_time+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-machine-tool"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-machine-tool"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		}
	}
}

function load_machine_tool (machine_tool_ID, callback) {
	var params = {action: 'get_machine_tool', machine_tool_ID: machine_tool_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine_tool_modal (data) {
	var machine_tool = data[0];
	edit_machine_tool_modal.find('form')[0].reset();

	var tool_select = $('#tool_select').nysus_select({
		name: 'tool_ID',
		key: 'ID',
		data_url: processor,
		params: {action: 'get_tools'},
		render: function (tool) {return tool.tool_description;},
		placeholder: 'Select Tool',
		default_value: machine_tool.tool_ID
	});


	edit_machine_tool_modal.find('input, textarea, select').each(function (i, input) {
		var field = $(input);
		if (field.prop('type') == 'checkbox') {
			field.prop('checked', machine_tool[field.prop('name')]);
		} else {
			field.val(machine_tool[field.prop('name')]);
		}
	});

	edit_machine_tool_modal.find('.modal-title').html('Edit Machine <span data-localize="tool.upper">Tool</span>');
	edit_machine_tool_modal.attr('data-machine-tool-id', machine_tool.machine_tool_ID);
	edit_machine_tool_modal.attr('data-action', 'update_machine_tool');
	edit_machine_tool_modal.modal('show').data(null);
}

function update_machine (data) {
	// var required = [
		// 'machine_number',
		// 'machine_name',
		// 'machine_type_ID',
		// 'machine_group_ID',
		// 'primary_plc_ip',
		// 'station_IP_address'
	// ];
	var required = [
		'machine_number',
		'machine_name',
		'machine_type_ID',
		'machine_group_ID',
		'station_IP_address'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {
			action: 'update_machine',
			machine_ID: selected_machine.ID,
			data: data
		};

		$.post(processor, req_data).done(function (data) {
			load_machine(selected_machine.ID, show_machine);
			if (data === null) {
				$.growl('Machine updated.', {type: 'success'});
			} else {
				$.growl('Unable to update machine.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function insert_machine_tool (data) {
	var required = [
		'tool_ID',
		'target_cycle_time'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_machine_tool', machine_tool_data: data};

		$.post(processor, req_data).done(function (data) {
			load_machine(selected_machine.ID, show_machine);
			if (data === null) {
				$.growl('Tool added to machine.', {type: 'success'});
			} else {
				$.growl('Unable to add tool.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function update_machine_tool (data) {
	var required = [
		'tool_ID',
		'target_cycle_time'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'update_machine_tool', machine_tool_data: data};

		$.post(processor, req_data).done(function (data) {
			load_machine(selected_machine.ID, show_machine);
			if (data === null) {
				$.growl('Updated tool.', {type: 'success'});
			} else {
				$.growl('Unable to update tool.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function delete_machine_tool (machine_tool_ID) {
	var req_data = {action: 'delete_machine_tool', machine_tool_ID: machine_tool_ID};

	$.post(processor, req_data).done(function (data) {
		load_machine(selected_machine.ID, show_machine);
		if (data === null) {
			$.growl('Tool removed from machine.', {type: 'success'});
		} else {
			$.growl('Unable to remove tool.', {type: 'danger'});
		}
	});
}


function load_machine_ojt_jobs (machine_ID, callback) {
	var params = {action: 'get_machine_ojt_jobs', machine_ID: machine_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine_ojt_jobs (data) {
	var tbody = machine_ojt_jobs_table.find('tbody');
	tbody.empty();

	if (data) {
		for (var i=0; i<data.length; i++) {
			var tr = $('<tr></tr>').prop('id', 'machine_ojt_job_'+data[i].ID)
								   .attr('data-machine-ojt-job-id', data[i].ID)
								   .append($('<td>'+data[i].job_name+'</td>'))
								   .append($('<td>'+data[i].qualified+'</td>'))
								   .append($('<td>'+data[i].min_operators_qualified+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			//var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-machine-ojt-job"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-machine-ojt-job"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		}
	}
}

function load_ojt_jobs (cb) {
	$.getJSON(processor, {action: 'get_ojt_jobs'}).done(cb);
}

function show_ojt_job_select (data) {
	console.log(data);

	if (data) {
		var ojt_job_select = $('#ojt_job_select');
		ojt_job_select.empty();

		// plant-wide jobs
		if (data.jobs) {
			var plant_jobs_group = $('<optgroup></optgroup>').prop({id: 'plant_jobs_group', label: 'Plant-Wide'}).appendTo(ojt_job_select);
			data.jobs.forEach(function (job) {
				var job_option = $('<option></option>').prop({id: 'plant_job_'+job.ID, name: 'ojt_job_ID'})
													   .text(job.job_name)
													   .val(job.ID)
													   .appendTo(plant_jobs_group);
			});
		}

		data.departments.forEach(function (department) {
			var department_group = $('<optgroup></optgroup>').prop({id: 'department_group_'+department.ID, label: department.department_name})
															 .val(department.ID)
															 .appendTo(ojt_job_select);

			// department-wide jobs
			if (department.jobs) {
				var department_jobs_group = $('<optgroup></optgroup>').prop({id: 'department_jobs_group_'+department.ID, label: 'Department-Wide'})
																 .val(department.ID)
																 .appendTo(department_group);

				department.jobs.forEach(function (job) {
					var job_option = $('<option></option>').prop({id: 'department_job_'+job.ID, name: 'ojt_job_ID'})
														   .text(job.job_name)
														   .val(job.ID)
														   .appendTo(department_jobs_group);
				});
			}

			department.areas.forEach(function (area) {
				// area-specific jobs
				if (area.jobs) {
					var area_jobs_group = $('<optgroup></optgroup>').prop({id: 'area_group_'+area.ID, label: area.area_desc})
																	 .val(area.ID)
																	 .appendTo(department_group);

					area.jobs.forEach(function (job) {
						var job_option = $('<option></option>').prop({id: 'area_job_'+job.ID, name: 'ojt_job_ID'})
															   .text(job.job_name)
															   .val(job.ID)
															   .appendTo(area_jobs_group);
					});
				}
			});

		});
	}
}

function insert_machine_ojt_job (data) {
	data.action = 'insert_machine_ojt_job';

	$.post(processor, data).done(function (res_data) {
		load_machine(selected_machine.ID, show_machine);
		if (res_data === null) {
			$.growl('OJT job added to machine.', {type: 'success'});
		} else {
			$.growl('Unable to add OJT job.', {type: 'danger'});
		}
	});
}

function delete_machine_ojt_job (machine_ojt_job_ID) {
	$.post(processor, {action: 'delete_machine_ojt_job', machine_ojt_job_ID: machine_ojt_job_ID}).done(function (res_data) {
		load_machine(selected_machine.ID, show_machine);
		if (res_data === null) {
			$.growl('OJT job added to machine.', {type: 'success'});
		} else {
			$.growl('Unable to add OJT job.', {type: 'danger'});
		}
	});
}

//PRINTERS
function load_machine_printers (machine_ID, callback) {
	var params = {action: 'get_machine_printers', machine_ID: machine_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function show_machine_printers(jso) {
	var b = $('#machine_printers_table TBODY').empty();

	$(jso).each(function(i, el) {
		var r = $('<TR />').appendTo(b);

		var c = $('<TD />').appendTo(r).html(el.printer_name);
		var c = $('<TD />').appendTo(r).html(el.printer_IP);
		var c = $('<TD />').appendTo(r).html(el.part_order);
		var c = $('<TD />').appendTo(r).html(el.label_type);
		var c = $('<TD />').appendTo(r);
		var btn_group = $('<div />').addClass('btn-group').appendTo(c);
		var btn = $('<BUTTON />').addClass('btn btn-xs btn-danger btn-remove-printer').html('<span class="glyphicon glyphicon-remove"></span> Remove').appendTo(btn_group);
	});
}

//INSTRUCTIONS
function load_machine_instructions (machine_ID, callback) {
	var params = {action: 'get_machine_instructions', machine_ID: machine_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

function load_machine_instruction (machine_instruction_ID, callback) {
	var params = {action: 'get_machine_instruction', machine_instruction_ID: machine_instruction_ID};
	$.getJSON(processor, params).done(function (data) {
		if (callback != undefined) {
			callback(data);
		}
	});
}

var next_instr_order = 10;
function show_machine_instructions (data) {
	var tbody = machine_instructions_table.find('tbody');
	tbody.empty();

	if (data) {
		for (var i=0; i<data.length; i++) {

			var img = '';
			if (data[i].instr_image) {
				img = '<img src="/machine_mes2/public/images/work_instructions/machine_' + selected_machine.ID + '/' + data[i].instr_image + '" style="height: 50px;width:50px;">';
			}

			var txt = data[i].instr_text;
			if (txt.length > 30) {
				txt = txt.substring(0, 30) + '...';
			}

			var tr = $('<tr></tr>').prop('id', 'machine_instruction_'+data[i].ID)
								   .attr('data-machine-instruction-id', data[i].ID)
								   .append($('<td>'+data[i].instr_order +'</td>'))
								   .append($('<td>'+data[i].title +'</td>'))
								   .append($('<td>'+txt+'</td>'))
								   .append($('<td>'+img+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			//var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-machine-ojt-job"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			if (i > 0) {
				var move_up_btn = $('<button type="button" class="btn btn-info btn-xs btn-move-up-machine-instruction"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true" data-toggle="tooltip" title="Move Up"></span> Move Up</button>').appendTo(btn_group);
			}
			if (i < data.length - 1) {
				var move_down_btn = $('<button type="button" class="btn btn-info btn-xs btn-move-down-machine-instruction"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true" data-toggle="tooltip" title="Move Down"></span> Move Down</button>').appendTo(btn_group);
			}
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-machine-instruction"><span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" title="Edit"></span> Edit</button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-machine-instruction"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span> Remove</button>').appendTo(btn_group);
			tbody.append(tr);

			next_instr_order = data[i].instr_order + 10;
		}
	}
}

function show_machine_instruction_modal (data) {
	var machine_instruction = data[0];
	edit_machine_instruction_modal.find('form')[0].reset();

	edit_machine_instruction_modal.find('input, textarea, select').each(function (i, input) {
		var field = $(input);
		if (field.prop('type') == 'checkbox') {
			field.prop('checked', machine_instruction[field.prop('name')]);
		} else if (field.prop('type') == 'file') {
			field.val('');
		} else {
			field.val(machine_instruction[field.prop('name')]);
		}
	});

	if ((machine_instruction.instr_image || '') == '') {
		$('#img_instr_image').attr('src', '');
	} else {
		$('#img_instr_image').attr('src', '/machine_mes2/public/images/work_instructions/MACHINE_' + selected_machine.ID + '/' + machine_instruction.instr_image);
	}

	edit_machine_instruction_modal.find('.modal-title').html('Edit Machine <span data-localize="tool.upper">Instruction</span>');
	edit_machine_instruction_modal.attr('data-machine-instruction-id', machine_instruction.machine_instruction_ID);
	edit_machine_instruction_modal.attr('data-action', 'update_machine_instruction');
	edit_machine_instruction_modal.modal('show').data(null);
}



function delegate_machine_instruction_edit (machine_instruction_data) {
	var action = edit_machine_instruction_modal.attr('data-action');
	if (action == 'insert_machine_instruction') {
		insert_machine_instruction(machine_instruction_data);
	} else if (action == 'update_machine_instruction') {
		update_machine_instruction(machine_instruction_data);
	}
}

function insert_machine_instruction (data) {
	var required = [
		'instr_order',
		'title',
		'instr_text'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'insert_machine_instruction', machine_instruction_data: data};

		$.post(processor, req_data).done(function (data) {
			load_machine(selected_machine.ID, show_machine);
			if (data === null) {
				$.growl('Instruction added to machine.', {type: 'success'});
			} else {
				$.growl('Unable to add instruction.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function update_machine_instruction (data) {
	var required = [
		'instr_order',
		'title',
		'instr_text'
	];

	var valid = true;
	for (var i=0; i<required.length; i++) {
		valid = valid && data[required[i]];
	}

	if (valid) {
		var req_data = {action: 'update_machine_instruction', machine_instruction_data: data};

		$.post(processor, req_data).done(function (data) {
			load_machine(selected_machine.ID, show_machine);
			if (data === null) {
				$.growl('Updated instruction.', {type: 'success'});
			} else {
				$.growl('Unable to update instruction.', {type: 'danger'});
			}
		});
	} else {
		$.growl('Missing required field.', {type: 'danger'});
	}
}

function delete_machine_instruction (machine_instruction_ID) {
	var req_data = {action: 'delete_machine_instruction', machine_instruction_ID: machine_instruction_ID};

	$.post(processor, req_data).done(function (data) {
		load_machine(selected_machine.ID, show_machine);
		if (data === null) {
			$.growl('Instruction removed from machine.', {type: 'success'});
		} else {
			$.growl('Unable to remove instruction.', {type: 'danger'});
		}
	});
}

function afterMoveInstruction() {
	$.growl('Instruction moved!', {type: 'success'});
	load_machine(selected_machine.ID, show_machine);
}

function preview_machine_instructions(jso) {
	var ol = $('#div_preview_carousel OL.carousel-indicators').html('');
	var slides = $('#div_preview_carousel DIV.carousel-inner').html('');

	for (var i = 0; i < jso.length; i++) {
		var li = $('<LI />').data('target', '#div_preview_carousel').data('slide-to', i);
		var item = $('<DIV />').addClass('item');

		var tbl = $('<TABLE />').appendTo(item).addClass('table table-bordered').css('width','90%').attr('align', 'center');
		var r = $('<TR />').appendTo(tbl);
		var top_title = $('<TD />').attr('colspan', 2).css('text-align', 'center').appendTo(r).html(jso[i].title);

		var r = $('<TR />').appendTo(tbl);
		var lh_side = $('<TD />').css('width','50%').css('font-size','24pt').css('text-align', 'center').appendTo(r).html(jso[i].instr_text);

		var rh_side = $('<TD />').css('width','50%').appendTo(r);
		var img = $('<IMG />').attr('src', '/machine_mes2/public/images/work_instructions/machine_' + selected_machine.ID + '/' + jso[i].instr_image);
		img.css('min-height', '300px');
		img.appendTo(rh_side);

		if (i == 0) {
			li.addClass('active');
			item.addClass('active');
		}

		li.appendTo(ol);
		item.appendTo(slides);
	}

	if (jso.length == 1) {
		$('.carousel-control').hide();
		$('.carousel-indicators').hide();
	} else {
		$('.carousel-control').show();
		$('.carousel-indicators').show();
	}


	$('#div_preview_carousel').carousel({"interval":false});
	$('#preview_machine_instruction_modal').modal('show');
}

function afterImageUpload(fileName) {
	$('#img_instr_image').attr('src', '/machine_mes2/public/images/work_instructions/MACHINE_' + selected_machine.ID + '/' + fileName);
}


//END OF INSTRUCTIONS