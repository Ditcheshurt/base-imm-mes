
var cur_system_ID = null;
var cur_system_database = null;
var cur_line_ID = null;
var cur_line_type = null;
var cur_station_ID = null;
var cur_process_type = null;
var cur_station_order = null;
var processor = "./ajax_processors/options/instructions.php";
var cur_data = null;
var cur_instr = null;
var systems_lines_stations = null;
var wip_parts = null;
var station_data;

var base_image_url = "/MESAAS/content/instruction_images/%SYSTEM_ID%/CELL_%LINE_ID%/STATION_%STATION_ORDER%/";
var uploader = null;

//init everything
$(function() {
	$.ajaxSetup({
		"error":function(res) {
			alert('MAJOR AJAX ERROR' + res.responseText)
		}
	});

	$.getJSON = function(url, params, callback) {
		$.post(url, params, callback, 'json');
	};

	loadSystemsLinesStations();

	$("#selSystem").change(function() {
		cur_system_ID = $(this).find("option:selected").val();
		cur_process_type = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID})[0]['process_type'];
		cur_system_database = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID})[0]['database_name'];
		populateLinesStations();
	});

	$("#selStation").change(function() {
		//$('#btn_load').attr('disabled', false);
		loadInstructions();
		loadStationOptions();
	});

	//$('#btn_load').on('click', loadInstructions);

	$('.table-instructions').on('click', '.edit_ins', function() {
		event.preventDefault();
		editInstruction($(this));
	});

	$('.table-instructions').on('click', '.select_ins', function() {
		event.preventDefault();
		toggleInstructionRow($(this));
	});

	$('.table-instructions').on('click', '.delete_ins', function() {
		event.preventDefault();
		confirmDeleteInstruction($(this));
	});

	$('.btn-confirm-delete').on('click', function() {
		deleteInstruction($(this));
	});

	$('.table-instructions').on('click', '.preview_ins', function() {
		event.preventDefault();
		previewInstruction($(this));
	});

	$('.table-instructions').on('click', '.clone_ins', function() {
		event.preventDefault();
		confirmCloneInstruction($(this));
	});

	$('.btn-confirm-clone').on('click', function() {
		cloneInstruction($(this));
	});

	$('.table-instructions').on('click', '.view_ins_revisions', function() {
		event.preventDefault();
		viewInstructionRevisions($(this));
	});

	$('#div_edit_step').on('click', '.save-instr', function() {
		$('#div_edit_step').modal('hide');
	});

	$('#selTestType').on('change', function() {
		loadTestTypeOptions();
	});

	$('#tbl_test_type_options').on('click', '.btn-remove-row', function() {
		event.preventDefault();
		var r = $(this).closest('TR');
		r.remove();
	});

	$('#tbl_test_type_options').on('click', '.btn-add-row', function() {
		event.preventDefault();
		var t_ID = $(this).data('option_json_member');
		var t = $('#' + t_ID);
		addRowToTable(t_ID, t.data('option_description'));

	});

	$('#div_instr_heading').on('click', '.btn-add-step', function() {
		event.preventDefault();
		addInstruction($(this));
	});

	$('.btn-validate-save').on('click', function() {
		validateSave($(this));
	});

	$('.table-instructions').on('click', 'SPAN.move-up', function() {
		confirmMoveInstruction($(this), 'UP');
	});
	$('.table-instructions').on('click', 'SPAN.move-down', function() {
		confirmMoveInstruction($(this), 'DOWN');
	});

	$('.btn-confirm-move').on('click', function() {
		moveInstruction($(this));
	});

	$('#div_instructions').on('click', '.btn-refresh', loadInstructions);

	//$('#div_target_cyce_time').on('click', '.btn-refresh', loadTargetCycleTime);

	//init tinyMCE
	tinymce.init({
		"selector":"#inpText",
		"menubar":false,
		"statusbar":false,
		"toolbar":"bold italic underline | fontsizeselect | forecolor | backcolor",
		"plugins":"textcolor",
		"theme":"modern",
		"skin":"light",
		"fontsize_formats":"8pt 10pt 12pt 14pt 18pt 24pt 36pt",
		"convert_fonts_to_spans":true,
		"font_size_style_values":"8pt,10pt,12pt,14pt,18pt,24pt,36pt"
	});

	$('#btn_clear_image').on('click', function(event) {
		event.stopPropagation();
		$('#div_no_image').show();
		$('#aInstrImage').hide();
		$('#inpImageFile').val('');
		$('#sp_file_selected').html('');
		return false;
	});

	$('#btn_manage_ojt').on('click', function() {
		showOJT();
	});

	$('.btn-station-options').on('click', function () {
		$('#station_options_modal').modal('show');
	});

	$('#station_options_modal .btn-save').on('click', function () {
		updateStationOptions();
	});

	$('#station_options_modal form input').on('keyup', function () {
		var save_btn = $('#station_options_modal .btn-save');

		if (!$(this).val()) {
			save_btn.hide();
		} else {
			save_btn.show();
		}
	});

	$('#station_options_modal').on('hidden.bs.modal', function () {
		populateStationOptions(station_data);
	});

	//init plupload
	uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		browse_button : 'btn_select_image', // you can pass in id...
		container: document.getElementById('div_upload_container'), // ... or DOM Element itself
		url : './ajax_processors/options/instructions_image_upload.php',
		flash_swf_url : '../js/Moxie.swf',
		silverlight_xap_url : '../js/Moxie.xap',
		multi_selecttion: false,

		filters : {
			max_file_size : '400kb',
			mime_types: [
				{title : "Image files", extensions : "jpg,gif,png"}
			]
		},

		init: {
			PostInit: function() {
				document.getElementById('sp_file_selected').innerHTML = '';
			},

			FilesAdded: function(up, files) {
				document.getElementById('sp_file_selected').innerHTML = '';
				$('.btn-validate-save').attr('disabled', true); //disable saving while file is uploading
				plupload.each(files, function(file) {
					document.getElementById('sp_file_selected').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
				});
				up.start();
			},

			UploadProgress: function(up, file) {
				var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];
				document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="' + file.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + file.percent + '%;">' + file.percent + '%</div></div>';

				if (file.percent >= 100) {
					document.getElementById('inpImageFile').value = file.name;
					$('#div_no_image').hide();
					$('#imgInstrImage').attr('src', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + file.name);
					$('#aInstrImage').show().attr('href', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + file.name);
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '';
					$('.btn-validate-save').attr('disabled', false);
				}
			},

			Error: function(up, err) {
				//document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
				alert('Error uploading image: ' + err.message);
			}
		}
	});

	uploader.init();

	$('SPAN.glyphicon-question-sign').tooltip();
});



function loadSystemsLinesStations() {
	$.getJSON(processor, {"action":"load_systems_lines_stations"}, populateSystemsLinesStations);
}

function populateSystemsLinesStations(jso) {
	systems_lines_stations = jso;
	$('#selStation').html('').attr('disabled', true);
	$('#selSystem').html('');

	var sys = systems_lines_stations.systems;
	var ops = '';
	for (var k = 0; k < sys.length; k++) {
		ops += '<option value="' + sys[k].ID + '">' + sys[k].system_name + '</option>\n';
		if (k == 0) {
			cur_system_ID = sys[k].ID;
			cur_process_type = sys[k].process_type;
			cur_system_database = sys[k].database_name;
		}
	}
	$('#selSystem').html(ops);
	populateLinesStations();
}

function populateLinesStations() {
	var opt_group = '';
	$('#selStation').html('');

	var lines = $.grep(systems_lines_stations.lines, function(line, x) {
		return line.system_ID == cur_system_ID;
	});
	for (var j = 0; j < lines.length; j++) {
		opt_group += '<optgroup label="' + lines[j].line_desc + '">\n';
		var stations = $.grep(systems_lines_stations.stations, function(station, y) {
			return station.line_ID == lines[j].line_ID;
		});

		for (var k = 0; k < stations.length; k++) {
			opt_group += '<option value="' + stations[k].line_ID + '-' + stations[k].station_ID + '-' + stations[k].station_order + '-' + lines[j].line_type + '" data-id="'+stations[k].ID+'">' + stations[k].station_desc + ' (' + stations[k].ID + ')</option>\n';
		}
		opt_group += '</optgroup>\n';

	}
	opt_group += '</optgroup>\n';
	$('#selStation').html(opt_group).attr('disabled', false);
	//$('#btn_load').attr('disabled', false).removeClass('disabled').trigger('click');
	loadInstructions();
	loadStationOptions();
}

function loadInstructions() {
	$('#div_loading_instructions').show();
	var x = $('#selStation').find("option:selected").val().split('-');
	cur_line_ID = x[0];
	cur_station_ID = x[1];
	cur_station_order = x[2];
	cur_line_type = x[3];

	if (cur_process_type == 'BATCH') {
		var r = Math.random() * 1000000;
		$.getJSON(processor, {
			"action":"load_part_groups",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"r":r
		}, setPartGroups);
	}

	if (cur_process_type == 'SEQUENCED' && cur_line_type != 0) {
		var r = Math.random() * 1000000;
		$.getJSON(processor, {
			"action":"load_wip_parts",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"r":r
		}, setWIPParts);
	}

	var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];
	uploader.setOption("multipart_params", {"system":company_code, "line_ID":cur_line_ID, "station_order":cur_station_order, "station_ID":cur_station_ID, "process_type":cur_process_type});

	var r = Math.random() * 1000000;
	$.getJSON(processor, {
			"action":"load_instructions",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_ID":cur_station_ID,
			"station_order":cur_station_order,
			"r":r
		}, populateInstrList);

	$('.cmdAddInstruction').removeClass('disabled').attr('disabled', false);
}

function loadStationOptions() {
	var x = $('#selStation').find("option:selected").val().split('-');
	cur_line_ID = x[0];
	cur_station_ID = x[1];
	cur_station_order = x[2];
	cur_line_type = x[3];
	var real_station_ID = $('#selStation').find('option:selected').attr('data-id');

	$.getJSON(processor, {
			"action":"load_station_options",
			"system_ID":cur_system_ID,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_ID":real_station_ID,
			"station_order":cur_station_order,
		}, function (data) {
			if (data) {
				station_data = data[0];
				populateStationOptions(station_data);
			}
		});

	//$('#div_target_cycle_time .btn-save').removeClass('disabled').attr('disabled', false);
}

function setPartGroups(jso) {
	systems_lines_stations.part_groups = jso || [];
}

function setWIPParts(jso) {
	wip_parts = jso||[];
}

function populateInstrList(jso) {
	$('#div_loading_instructions').hide();
	$('.modal').modal('hide');
	var line_desc = $.grep(systems_lines_stations.lines, function(l, i) {return l.line_ID == cur_line_ID;})[0]['line_desc'];
	var station_desc = $.grep(systems_lines_stations.stations, function(s, i) {return s.line_ID == cur_line_ID && s.station_order == cur_station_order;})[0]['station_desc'];
	var station_ID = $.grep(systems_lines_stations.stations, function(s, i) {return s.line_ID == cur_line_ID && s.station_order == cur_station_order;})[0]['station_ID'];
	var station_IPs = $.map($.grep(systems_lines_stations.stations, function(s, i) {return s.station_ID == station_ID;}), function(n) { return n.ip_address; });

	$('#ul_station_IPs').html('');
	$.each(station_IPs, function(i, el) {
		$('#ul_station_IPs').append($('<li />').html(el).addClass('list-group-item'));
	});

	var station_options = $.grep(systems_lines_stations.stations, function(s, i) {return s.line_ID == cur_line_ID && s.station_order == cur_station_order;})[0]['station_options'];
	//$('#div_station_options').html(station_options);
	$('#div_station_options').html('');
	var op = {};
	try {
		JSON.parse( (station_options || '{}') );
	} catch (e)  {

	}
	var a = Object.keys(op);
	if (a.length > 0) {
		for (var i = 0; i < a.length; i++) {
			$('#div_station_options').append($('<div />').html(a[i] + ': ' + op[a[i]]).addClass('tiny'));
		};
	} else {
		$('#div_station_options').html('<i>none</i>');
	}

	$('#div_wip_parts_button').hide();

	if (cur_process_type == 'BATCH') {
		$('#div_instr_heading').html('Instructions for Line: ' + line_desc + ', Station: ' + station_desc +
											  '<button class="btn btn-xs btn-success pull-right btn-add-step margin-left"><span class="glyphicon glyphicon-plus-sign"></span> Add New Instruction Step</button>' +
											  '<button class="btn btn-xs btn-success pull-right btn-add-batch-part margin-left" data-toggle="modal" data-target="#batch-parts-modal"><span class="glyphicon glyphicon-wrench"></span> Part Setup</button>' +
											  '<button class="btn btn-xs btn-info pull-right btn-refresh" title="Refresh"><span class="glyphicon glyphicon-repeat"></span>');
	}
	if (cur_process_type == 'SEQUENCED') {
		if (cur_line_type == 1) {
			$('#div_wip_parts_button').show();
		}
		$('#div_instr_heading').html('Instructions for Line: ' + line_desc + ', Station: ' + station_desc +
											  '<button class="btn btn-xs btn-success pull-right btn-add-part margin-left"><span class="glyphicon glyphicon-plus-sign"></span> Add New Part</button>' +
											  '<button class="btn btn-xs btn-info pull-right btn-refresh" title="Refresh"><span class="glyphicon glyphicon-repeat"></span>');
	}

	cur_data = jso;
	$('.table-instructions').hide();
	$('#tbl_instructions_' + cur_process_type).show();
	var t = $('#tbl_instructions_' + cur_process_type + ' TBODY');
	t.html('');

	cur_data.instructions = cur_data.instructions || [];

	if (cur_data.instructions.length == 0) {
		var r = $('<tr />');
		t.append(r);
		var c = $('<td />').attr('colspan', 10).html('<div class="alert alert-warning">No records found for selected station.</div>');
		r.append(c);
	} else {

		switch (cur_process_type) {
			case 'BATCH':
				populateInstrTableRowsBATCH(cur_data, t);
				break;
			case 'SEQUENCED':
				populateInstrTableRowsSEQUENCED(cur_data, t);
				break;
		}

	}


	$('#div_instructions').fadeIn();

	$('#div_station_info').fadeIn();

	$('#div_instructions .btn-group BUTTON').tooltip();
}

function populateStationOptions (data) {
	if (data) {
		$('#target_cycle_time_input').val(data.target_cycle_time ? data.target_cycle_time : 0);
		$('#transit_time_input').val(data.transit_time ? data.transit_time : 0);
	}
	//$('#target_cycle_time_modal').fadeIn();
}


function confirmMoveInstruction(btn, dir) {
	$('.btn-confirm-move').data(btn.data()).data('DIR', dir);
	$('#div_move_modal .modal-body').html('Are you sure you want to move this instruction ' + dir + '?');
	$('#div_move_modal').modal('show');
}

function moveInstruction(btn) {
	var d = btn.data();
	console.log('Moving instruction ' + d.ID + ' ' + d.DIR);
	$.getJSON(processor, {
			"action":"move_instr",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"instr_ID":d.ID,
			"direction":d.DIR,
			"line_ID":cur_line_ID,
			"station_order":cur_station_order,
			"station_ID":cur_station_ID,
			"part_number":((cur_process_type=='SEQUENCED')?d.part_number:''),
			"current_rev_level":((cur_process_type=='SEQUENCED')?d.current_rev_level:''),
			"part_order":((cur_process_type=='SEQUENCED')?d.part_order:''),
			"reason":("MOVED "+d.DIR)
		}, function(jso) { afterEditInstruction(d, jso); });
}

function confirmDeleteInstruction(btn) {
	$('.btn-confirm-delete').data(btn.data());
	$('#div_delete_modal .modal-body').html('Are you SURE you want to delete this instruction?  <div class="alert alert-info">Although a backup will be made, it will take time to restore if accidentally deleted.</div>');
	$('#div_delete_modal').modal('show');
}

function deleteInstruction(btn) {
	var d = btn.data();
	console.log('Deleting instruction: ' + d.ID);
	$.getJSON(processor, {
			"action":"delete_instr",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"instr_ID":d.ID,
			"line_ID":cur_line_ID,
			"station_order":cur_station_order,
			"station_ID":cur_station_ID,
			"part_order":((cur_process_type == 'SEQUENCED')?d.part_order:0),
			"part_number":((cur_process_type == 'SEQUENCED')?d.part_number:''),
			"current_rev_level":((cur_process_type == 'SEQUENCED')?d.current_rev_level:'')
		}, function(jso) { afterEditInstruction(d, jso); }
	);
}

function editInstruction(btn) {
	var d = btn.data();
	var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];
	$('.btn-validate-save').data(d).data('edit_type', 'EDIT');
	cur_instr = d;
	$('#div_modal_title').html('Edit Instruction For Part: ' + d.part_number + d.current_rev_level);
	$('#inpOrder').val(d.instr_order);

	$('#selPart').html('');
	var o = $('<option />').text('All Parts').val(0);
	$('#selPart').append(o);

	if (cur_process_type == 'BATCH') {
		for (var i = 0; i < systems_lines_stations.part_groups.length; i++) {
			if (systems_lines_stations.part_groups[i].line_ID == cur_line_ID) {
				var o = $('<option />').text(systems_lines_stations.part_groups[i].part_group_desc).val(systems_lines_stations.part_groups[i].part_group_ID);
				if (systems_lines_stations.part_groups[i].part_group_ID == d.part_id) {
					o.prop('selected', true);
				}
				$('#selPart').append(o);
			}
		}

		for (var i = 0; i < cur_data.parts.length; i++) {
			var p = cur_data.parts[i];
			var o = $('<option />').text(p.part_desc).val(p.ID);

			if (p.ID == d.part_id) {
				o.prop('selected', true);
			}

			$('#selPart').append(o);
		}

		$('.applicable-part').show();
		$('.dependent-parts').hide();
		$('.subtractive-parts').hide();
	}

	if (cur_process_type == "SEQUENCED") {
		if (cur_line_type != 1) {
			$('.applicable-part').hide();

			$('.dependent-parts').show();
			$('.subtractive-parts').show();

			$('#sp_dependent_parts').data('dep', d.dependent);
			$('#sp_subtractive_parts').data('subtr', d.subtractive);

			updateDependentAndSubtractive();
		} else {

			$('#sp_dependent_parts').data('dep', []);
			$('#sp_subtractive_parts').data('subtr', []);
			$('.applicable-part').show();
			$('.dependent-parts').hide();
			$('.subtractive-parts').hide();
			$('#selPart').html('');
			var o = $('<option />').text('All Parts').val(0);
			$('#selPart').append(o);

			for (var i = 0; i < wip_parts.length; i++) {
				var p = wip_parts[i];
				var o = $('<option />').text(p.part_desc).val(p.ID);

				if (cur_instr.wip_part_ID == p.ID) {
					o.prop('selected', true);
				}

				$('#selPart').append(o);
			}
		}
	}

	if (d.image_file == null || d.image_file == '') {
		$('#div_no_image').show();
		$('#aInstrImage').hide();
		$('#inpImageFile').val('');
	} else {
		$('#div_no_image').hide();
		$('#inpImageFile').val(d.image_file);

		var s_order = cur_station_order;
		if (cur_process_type == 'SEQUENCED') {
			s_order = cur_station_ID;
		}
		$('#imgInstrImage').attr('src', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', s_order) + d.image_file);
		$('#aInstrImage').show().attr('href', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', s_order) + d.image_file);
	}
	$('#sp_file_selected').html('');

	if (d.active_flag) {
		$('#inpActive').prop('checked', true);
	} else {
		$('#inpActive').prop('checked', false);
	}

	if (d.title == 'NEW_ADDED_PART') {
		d.title = '';
	}
	$('#inpTitle').val(d.title);
	$('#inpPrompt').val(d.prompt);

	switch (cur_process_type) {
		case 'BATCH':
			tinymce.activeEditor.setContent(d.text);
			break;
		case 'SEQUENCED':
			tinymce.activeEditor.setContent(d.instr_text);
			break;
	}

	$('#selTestType').html('');
	for (var i = 0; i < cur_data.test_types.length; i++) {
		var t = cur_data.test_types[i];
		var o = $('<option />').text(t.type_desc).val(t.ID);

		if (t.ID == d.test_type) {
			o.prop('selected', true);
		}

		$('#selTestType').append(o);
	}
	loadTestTypeOptions();

	if (Object.keys(d).indexOf('max_seconds') >= 0) {
		$('#sp_max_seconds').show();
		$('#inpJumpBacksteps').val(d.jump_back_steps);
		$('#inpMaxSeconds').val(d.max_seconds);
	} else {
		$('#sp_max_seconds').hide();
	}

	$('#inpReason').val('');

	$('#div_instr_modal').modal('show');
}

function confirmCloneInstruction(btn) {
	var d = btn.data();
	$('.btn-confirm-clone').data(btn.data());

	var m = 0;

	if (cur_process_type == 'BATCH') {
		for (var i = 0; i < cur_data.instructions.length; i++) {
			if (cur_data.instructions[i].instr_order > m) {
				m = cur_data.instructions[i].instr_order;
			}
		}
	}

	if (cur_process_type == 'SEQUENCED') {
		var div = $.grep($('DIV.part-instr-table-wrapper'), function(el) {
			return $(el).data('part_order') == d.part_order && $(el).data('part_number') == d.part_number && $(el).data('current_rev_level') == (d.current_rev_level||'');
		});
		m = $(div).data('max_instr_order');
	}

	m += 10;

	$('#inpCloneOrder').val(m);


	if (cur_process_type == 'BATCH') {
		$('#selClonePart').html('');
		var o = $('<option />').text('All Parts').val(0);
		$('#selClonePart').append(o);

		for (var i = 0; i < systems_lines_stations.part_groups.length; i++) {
			if (systems_lines_stations.part_groups[i].line_ID == cur_line_ID) {
				var o = $('<option />').text(systems_lines_stations.part_groups[i].part_group_desc).val(systems_lines_stations.part_groups[i].part_group_ID);
				$('#selClonePart').append(o);
			}
		}

		for (var i = 0; i < cur_data.parts.length; i++) {
			var p = cur_data.parts[i];
			var o = $('<option />').text(p.part_desc).val(p.ID);

			if (p.ID == d.part_ID) {
				o.prop('selected', true);
			}

			$('#selClonePart').append(o);
		}
	} else {
		$('.applicable-part').hide();
	}

	$('#div_clone_modal').modal('show');
}

function cloneInstruction(btn) {
	var d = btn.data();
	console.log('Cloning instruction: ' + d.ID);
	$.getJSON(processor, {
			"action":"clone_instr",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"instr_ID":d.ID,
			"line_ID":cur_line_ID,
			"station_order":cur_station_order,
			"station_ID":cur_station_ID,
			"part_order":((cur_process_type == 'SEQUENCED')?d.part_order:0),
			"part_number":((cur_process_type == 'SEQUENCED')?d.part_number:''),
			"current_rev_level":((cur_process_type == 'SEQUENCED')?d.current_rev_level:''),
			"instr_order":$('#inpCloneOrder').val(),
			"part_ID":$('#selClonePart').val()
		}, function(jso) { afterEditInstruction(d, jso); });
}

function afterEditInstruction(part, jso) {
	$('.modal').modal('hide');
	if (cur_process_type == 'BATCH') {
		populateInstrList(jso);
	} else {
		getPartInstructions(part);
	}
}

function addInstruction(btn) {
	var d = btn.data();
	$('.btn-validate-save').data(d).data('edit_type', 'ADD');
	$('#div_modal_title').html('Add Instruction For Part: ' + d.part_number + d.current_rev_level);

	//get max step order
	var m = 0;

	if (cur_process_type == 'BATCH') {
		for (var i = 0; i < cur_data.instructions.length; i++) {
			if (cur_data.instructions[i].instr_order > m) {
				m = cur_data.instructions[i].instr_order;
			}
		}
	}

	if (cur_process_type == 'SEQUENCED') {
		var div = $.grep($('DIV.part-instr-table-wrapper'), function(el) {
			return $(el).data('part_order') == d.part_order && $(el).data('part_number') == d.part_number && $(el).data('current_rev_level') == (d.current_rev_level||'');
		});
		m = $(div).data('max_instr_order');
	}

	m += 10;

	$('#inpOrder').val(m);

	if (cur_process_type == 'BATCH') {
		$('.applicable-part').show();
		$('#selPart').html('');
		var o = $('<option />').text('All Parts').val(0);
		$('#selPart').append(o);

		for (var i = 0; i < systems_lines_stations.part_groups.length; i++) {
			if (systems_lines_stations.part_groups[i].line_ID == cur_line_ID) {
				var o = $('<option />').text(systems_lines_stations.part_groups[i].part_group_desc).val(systems_lines_stations.part_groups[i].part_group_ID);
				$('#selPart').append(o);
			}
		}

		for (var i = 0; i < cur_data.parts.length; i++) {
			var p = cur_data.parts[i];
			var o = $('<option />').text(p.part_desc).val(p.ID);

			$('#selPart').append(o);
		}
		$('.dependent-parts').hide();
		$('.subtractive-parts').hide();
	}

	if (cur_process_type == "SEQUENCED") {
		$('#sp_dependent_parts').data('dep', []);
		$('#sp_subtractive_parts').data('subtr', []);

		if (cur_line_type != 1) {
			$('.applicable-part').hide();
			$('.dependent-parts').show();
			$('.subtractive-parts').show();
		} else {
			$('.applicable-part').show();
			$('.dependent-parts').hide();
			$('.subtractive-parts').hide();
			$('#selPart').html('');
			var o = $('<option />').text('All Parts').val(0);
			$('#selPart').append(o);

			for (var i = 0; i < wip_parts.length; i++) {
				var p = wip_parts[i];
				var o = $('<option />').text(p.part_desc).val(p.ID);

				$('#selPart').append(o);
			}
		}
	}

	$('#div_no_image').show();
	$('#aInstrImage').hide();
	$('#inpImageFile').val('');
	$('#sp_file_selected').html('');

	$('#inpActive').prop('checked', true);

	$('#inpTitle').val('');
	$('#inpPrompt').val('');

	tinymce.activeEditor.setContent('');

	$('#selTestType').html('');
	for (var i = 0; i < cur_data.test_types.length; i++) {
		var t = cur_data.test_types[i];
		var o = $('<option />').text(t.type_desc).val(t.ID);
		if (t.type_desc == 'Badge Scan') {
			o.prop('selected', true);
		}
		$('#selTestType').append(o);
	}

	cur_instr = null;

	loadTestTypeOptions();


	if (Object.keys(d).indexOf('max_seconds') >= 0) {
		$('#sp_max_seconds').show();
		$('#inpJumpBacksteps').val('');
		$('#inpMaxSeconds').val('');
	} else {
		$('#sp_max_seconds').hide();
	}

	$('#inpReason').val('');

	$('#div_instr_modal').modal('show');
}


function loadTestTypeOptions() {
	var d = cur_instr || {"instr_options":"{}"};

	var t = $('#tbl_test_type_options');
	t.html('');
	var num_ops = 0;
	var instr_options = JSON.parse(d.instr_options);
	var cur_sel_type = $('#selTestType').find('option:selected').val();

	for (var i = 0; i < cur_data.test_type_options.length; i++) {
		var tto = cur_data.test_type_options[i];
		if (tto.test_type_id == cur_sel_type) {
			var r = $('<tr />');
			var c = $('<td />').html(tto.option_prompt);
			r.append(c);

			var c = $('<td />');
			var inp = $('<input />').addClass('form-control input-sm instr-option');
			switch (tto.option_data_type) {
				case 'text':
					inp.attr('type', 'text');
					if (instr_options && instr_options[tto.option_json_member]) {
						inp.val(instr_options[tto.option_json_member]);
					}
					break;
				case 'number':
					inp.attr('type', 'number');
					if (instr_options && instr_options[tto.option_json_member]) {
						inp.val(instr_options[tto.option_json_member]);
					}
					break;
				case 'checkbox':
					inp.attr('type', 'checkbox');
					if (instr_options && instr_options[tto.option_json_member]) {
						inp.prop('checked', (instr_options[tto.option_json_member]));
					}
					break;
				case 'list':
					inp = $('<select />').addClass('form-control instr-option input-sm');
					var l = tto.option_regex_validation.split(',');
					for (var j = 0; j < l.length; j++) {
						var o = $('<option />').text(l[j]).val(l[j]);
						if (instr_options && instr_options[tto.option_json_member]) {
							if (instr_options[tto.option_json_member] == l[j]) {
								o.prop('selected', true);
							}
						}
						inp.append(o);
					}
					break;
				case 'plc_button':
					inp = $('<BUTTON />').addClass('btn btn-sm');
					inp.html('Read PLC Tag');
					inp.on('click', validatePLCTag);
					break;
				case 'table':
					inp = $('<TABLE />').addClass('table table-condensed table-striped table-option-type');
					inp.removeClass('instr-option');
					var r2 = $('<TR />');
					r2.addClass('header');
					var arr = eval('(' + tto.option_description + ')');
					for (var j = 0; j < arr.length; j++) {
						var c2 = $('<TD />');
						c2.html(arr[j].desc);
						if (arr[j].help) {
							var sp_help = $('<SPAN />');
							sp_help.addClass('glyphicon glyphicon-question-sign');
							sp_help.attr('data-toggle', 'tooltip');
							sp_help.attr('title', arr[j].help);
							sp_help.tooltip();
							c2.append(' ');
							c2.append(sp_help);
						}
						r2.append(c2);
					}
					inp.append(r2);

					if (instr_options && instr_options[tto.option_json_member]) {
						var arr2 = instr_options[tto.option_json_member];
						for (var j = 0; j < arr2.length; j++) {
							var r2 = $('<TR />');
							for (var k = 0; k < arr.length; k++) {
								var c2 = $('<TD />');
								var inp2 = $('<INPUT />');
								inp2.attr('type', 'text');
								inp2.addClass('table_type form-control input-sm');
								inp2.data('key', arr[k].key);
								inp2.attr('value', arr2[j][arr[k].key]);
								c2.append(inp2);
								r2.append(c2);
							}

							var c2 = $('<TD />');
							c2.html('<button class="btn btn-xs btn-danger btn-remove-row" title="Delete Row"><span class="glyphicon glyphicon-remove"></span></button>');
							r2.append(c2);

							inp.append(r2);
						}
					}

					break;
				case 'link':
					inp = $('<DIV></DIV>').addClass('btn btn-sm');
					inp.html('Click to Set Up');

					var link = tto.option_regex_validation.replace('INSTRUCTION_ID', d.ID);
					inp.on('click', function() { window.open(link, 'multi_tag', 'height=800,width=800'); } );
					break;
			}
			inp.attr('id', tto.option_json_member);
			inp.data(tto);
			c.append(inp);
			r.append(c);

			if (tto.option_data_type != 'table') {
				var c = $('<td />').html(tto.option_description).attr('id', 'td_' + tto.option_json_member + '_desc');
				r.append(c);
			} else {
				var c = $('<TD />');
				var b = $('<BUTTON />');
				b.addClass('btn btn-primary btn-xs btn-add-row btn-success');
				b.data('option_json_member', tto.option_json_member);
				b.html('<span class="glyphicon glyphicon-plus"></span> Add Row');
				c.append(b);
				r.append(c);
			}

			t.append(r);
			num_ops++;
		}
	}

	if (num_ops == 0) {
		var r = $('<tr />');
		var c = $('<td />').html('No options for this test type');
		r.append(c);
		t.append(r);
	}
}

function addRowToTable(tbl_ID, option_description) {
	var arr = eval('(' + option_description + ')');
	var t = $('#' + tbl_ID);
	var r = $('<TR />');
	for (var j = 0; j < arr.length; j++) {
		var c = $('<TD />');
		var inp2 = $('<INPUT />');
		inp2.attr('type', 'text');
		inp2.addClass('table_type form-control input-sm');
		inp2.data('key', arr[j].key);
		c.append(inp2);
		r.append(c);
	}
	var c = $('<TD />');
	c.html('<button class="btn btn-xs btn-danger btn-remove-row"><span class="glyphicon glyphicon-remove" title="Delete Row"></span></button>');
	r.append(c);
	t.append(r);
}

function validatePLCTag() {
	//expects
	$.getJSON('/PLCService/PLCWebReadWrite.asmx/getValueAsJSON', {
			"plc_ip":$('#plc_ip').val(),
			"file_addr":$('#plc_addr').val(),
			"plc_path":"0"
		}, showValidatePLCTagResult);

	return false;
}

function showValidatePLCTagResult(jso) {
	if (jso.ErrorCode == '0') {
		$('#td_validate_plc_desc').html('Got PLC value: ' + jso.value).addClass('success').removeClass('danger');
	} else {
		$('#td_validate_plc_desc').html('PLC ERROR: ' + jso.ErrorCode).addClass('danger').removeClass('success');
	}
}

function validateSave(btn) {
	var d = btn.data();
	//order must be present and numeric
	var v = $('#inpOrder').val();
	if (isNaN(v) || v == '' || v.indexOf('.') >= 0) {
		$('#inpOrder').parent().addClass('has-error');
		return;
	} else {
		$('#inpOrder').parent().removeClass('has-error');
	}

	var v = $('#inpTitle').val();
	if (v == '') {
		$('#inpTitle').parent().addClass('has-error');
		return;
	} else {
		$('#inpTitle').parent().removeClass('has-error');
	}

	var max_seconds = 0, jump_back_steps = 0;
	if ($('#sp_max_seconds').is(':visible')) {
		max_seconds = $('#inpMaxSeconds').val();
		if (isNaN(max_seconds) || max_seconds < 0) {
			$('#inpMaxSeconds').parent().addClass('has-error');
			return;
		} else {
			$('#inpMaxSeconds').parent().removeClass('has-error');
		}

		jump_back_steps = $('#inpJumpBacksteps').val();
		if (isNaN(jump_back_steps) || jump_back_steps < 0) {
			$('#inpJumpBacksteps').parent().addClass('has-error');
			return;
		} else {
			$('#inpJumpBacksteps').parent().removeClass('has-error');
		}

		if (max_seconds == '') {
			max_seconds = 'NULL';
		}
		if (jump_back_steps == '') {
			jump_back_steps = 'NULL';
		}
	}

	var v = $('#inpReason').val();
	if (v == '') {
		$('#inpReason').parent().addClass('has-error');
		return;
	} else {
		$('#inpReason').parent().removeClass('has-error');
	}

	var instr_options = {};
	var instr_option_errors = false;
	$('.instr-option').each(function(i, o) {
		var d2 = $(o).data();
		if (d2.option_data_type == 'checkbox') {
			instr_options[o.id] = $(o).is(':checked');
		} else {
			var v = $(o).val();
			if (d2.option_data_type != 'list') {
				if (d2.option_regex_validation != null) {
					var r = new RegExp(d2.option_regex_validation);
					if (!r.test(v)) {
						$(o).parent().addClass('has-error');
						instr_option_errors = true;
					} else {
						$(o).parent().removeClass('has-error');
						instr_options[o.id] = v;
					}
				} else {
					if (d2.option_data_type == 'number' && isNaN(v)) {
						$(o).parent().addClass('has-error');
						instr_option_errors = true;
					} else {
						$(o).parent().removeClass('has-error');
						instr_options[o.id] = v;
					}
				}
			} else {
				instr_options[o.id] = v;
			}

		}
	});

	$('.table-option-type').each(function(i, o) {
		var d2 = $(o).data();
		var x = [];
		var rows = $(o).find('TR').not('.header');
		rows.each(function(i2, o2) {
			var row_obj = {};

			var inputs = $(o2).find('INPUT.table_type');
			inputs.each(function(i3, o3) {
				var d3 = $(o3).data();
				row_obj[d3.key] = $(o3).val();
			});

			x.push(row_obj);
		});
		instr_options[o.id] = x;
	});

	if (instr_option_errors) {
		return;
	}

	$.getJSON(processor, {
			"action":"save_instr",
			"edit_type":d.edit_type,
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"line_type":cur_line_type,
			"station_order":cur_station_order,
			"station_ID":cur_station_ID,
			"instr_ID":((d.edit_type == 'ADD')?'0':d.ID),
			"instr_order":$('#inpOrder').val(),
			"part_order":((cur_process_type=='SEQUENCED')?d.part_order:0),
			"part_number":((cur_process_type=='SEQUENCED')?d.part_number:''),
			"current_rev_level":((cur_process_type=='SEQUENCED')?d.current_rev_level:''),
			"part_ID":((cur_process_type=='BATCH')?$('#selPart').val():0),
			"wip_part_ID":((cur_process_type=='SEQUENCED' && cur_line_type == 1)?$('#selPart').val():0),
			"title":$('#inpTitle').val(),
			"image_file":$('#inpImageFile').val(),
			"active_flag":(($('#inpActive').is(':checked'))?1:0),
			"prompt":$('#inpPrompt').val(),
			"instr_text":tinyMCE.activeEditor.getContent(),
			"test_type":$('#selTestType').val(),
			"instr_options":JSON.stringify(instr_options),
			"reason":$('#inpReason').val(),
			"dependent":((cur_process_type=='SEQUENCED')?$('#sp_dependent_parts').data('dep').join('~'):''),
			"subtractive":((cur_process_type=='SEQUENCED')?$('#sp_subtractive_parts').data('subtr').join('~'):''),
			"max_seconds":max_seconds,
			"jump_back_steps":jump_back_steps
		}, function(jso) { afterEditInstruction(d, jso); });
}

function previewInstruction(btn) {
	var d = btn.data();
	var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];
	//window.open('./preview.php?instrID='+d.ID, 'preview', 'height=800,width=1024,toolbar=no');
	$('#div_preview_modal .modal-body').load('/mesaas/content/batch_build.html', '', function() {
		$('#div_middle_content').css('height', '95%');

		var stepDiv = document.createElement("div");
		$(stepDiv).attr("id", "div_interaction_0");
		$(stepDiv).addClass("INTERACTION_step");
		$(stepDiv).addClass("INTERACTION_curstep");
		$(stepDiv).text(d.title);
		$("#div_interaction_steps").empty();
		$("#div_interaction_steps").append(stepDiv);

		$("#div_interaction_info").text(d.prompt);

		if (d.image_file != null && d.image_file != '') {
			var img = document.createElement("img");
			$(img).css("height", "99%");
			$(img).css("width", "99%");

			if (cur_process_type == 'BATCH') {
				$(img).attr("src", base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_order) + d.image_file);
			}
			if (cur_process_type == 'SEQUENCED') {
				$(img).attr("src", base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + d.image_file);
			}


			$("#div_interaction_image").css("visibility", "visible");
			$("#div_interaction_image").append(img);
		}

		$("#div_interaction_instructions").html(d.text);

		$("#div_interaction_test_type").text(d.type_desc);

		$('#div_preview_modal').modal('show');
	});
}

function viewInstructionRevisions(btn) {
	var d = btn.data();
	$('#div_revision_modal').modal('show');
	$.getJSON(processor, {
			"action":"load_instruction_revisions",
			"instr_ID":d.ID,
			"database":cur_system_database,
			"process_type":cur_process_type
		}, populateInstructionRevisions);
}

function populateInstructionRevisions(jso) {
	$('#tblRevisions_' + cur_process_type).show();
	var t = $('#tblRevisions_' + cur_process_type + ' TBODY');
	t.html('');
	if (jso.length == 0) {
		t.append($('<tr />').append($('<td />').html('No revisions found.').attr('colspan', 12)));
	}
	for (var i = 0; i < jso.length; i++) {
		var r = $('<tr />');
		var c = $('<td />').html(jso[i].revised_time.date||jso[i].revised_time);
		r.append(c);

		var c = $('<td />').html(jso[i].revisor);
		r.append(c);

		var c = $('<td />').html(jso[i].revised_action);
		r.append(c);

		if (cur_process_type == 'SEQUENCED') {
			var c = $('<td />').html(jso[i].part_order);
			r.append(c);
		}

		var c = $('<td />').html(jso[i].instr_order);
		r.append(c);

		if (cur_process_type == 'BATCH') {
			var c = $('<td />').html(jso[i].part_ID);
		}
		if (cur_process_type == 'SEQUENCED') {
			var c = $('<td />').html((jso[i].part_number||'') + (jso[i].current_rev_level||''));
		}
		r.append(c);

		var c = $('<td />').html(jso[i].title);
		r.append(c);

		var c = $('<td />').html(jso[i].image_file);
		r.append(c);

		var c = $('<td />').html(jso[i].type_desc);
		r.append(c);

		var c = $('<td />').html(jso[i].active_flag);
		r.append(c);

		var c = $('<td />').html(prettyJSON(jso[i].instr_options));
		r.append(c);

		var c = $('<td />').html(jso[i].revision_reason);
		r.append(c);

		t.append(r);
	}
}

function prettyJSON(s) {
	var obj = JSON.parse(s||'{}');
	var r = [];
	for (x in obj) {
		r.push(x + '=' + obj[x]);
	}
	return r.join(', ');
}

function updateStationOptions () {
	var target_cycle_time = $('#target_cycle_time_input').val();
	var transit_time = $('#transit_time_input').val();
	var real_station_ID = $('#selStation').find('option:selected').attr('data-id');

	$.getJSON(processor, {
			"action":"update_station_options",
			"database":cur_system_database,
			"process_type":cur_process_type,
			"line_ID":cur_line_ID,
			"station_order":cur_station_order,
			"station_ID":real_station_ID,
			"target_cycle_time": target_cycle_time,
			"transit_time": transit_time
		}, function (data) {
			$('#station_options_modal').modal('hide');
		}
	);
}