var processor = './ajax_processors/options/group_manager.php';
var list = [];
var hide = false;

$(document).ready(function () {
	loadGroupList();
	
	$(".btn-add-group").click(function(){
		$('#p_group_name').val('');
		$('#p_group_name').removeClass('danger');
		$('#div_modal_add_group').modal('show');
	});
	
	$("#hide_group").change(function(){
		if($("#hide_group").prop('checked')){
			hide = true;
		}else{
			hide = false;
		}
		loadGroupList();
	});
	
	$("#add_group").on('click', function(e){
		if(list[$('#p_group_name').val()]){
			$('#p_group_name').addClass('danger');
		}else{
			$('#p_group_name').removeClass('danger');
			addGroup();
		}
		
	});
	
	$('#tbl_groups TBODY').on('click', '.btn-update', function(e) {
		var d = $(this).data();
		updateGroup(d.is_active,d.ID);
	});
});

function loadGroupList(){
	$.ajax(processor, {
		"data":{
			"action":"load_group_list"
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			updateGroupList(jso);
		}
	});	
	
}

function updateGroupList(jso){
	if (!jso) {
		return;
	}
	
	var b = $('#tbl_groups TBODY');

	b.html('');

	if (jso.length > 0) {
		var r = $('<TR />');
		b.append(r);

		for (var i = 0; i < jso.length; i++) {
			list[jso[i].part_group_description]= jso[i].is_active;
			if ( jso[i].is_active == 1 || hide == false ) {
					var r = $('<TR />');

					var c = $('<TD />').html(jso[i].part_group_description);
					r.append(c);
					var active = 'Inactive';
					if(jso[i].is_active == 1){
						active = 'Active';
					}
					var c = $('<TD />').html(active);
					r.append(c);
					var c = $('<TD />');
					if(jso[i].is_active == 1){
						var btn = $('<BUTTON />').addClass('btn btn-success btn-sm btn-update').html('<span class="glyphicon glyphicon-check"></span> Deactivate').data(jso[i]);
					}else{
						var btn = $('<BUTTON />').addClass('btn btn-danger btn-sm btn-update').html('<span class="glyphicon glyphicon-unchecked"></span> Activate').data(jso[i]);
					}
					btn.data(jso[i]);
					c.append(btn);
					r.append(c);

					b.append(r);
			}
		}
	}
}

function updateGroup(active,id){
	$.ajax(processor, {
		"data":{
			"action":"update_group",
			"active":active,
			"group_id":id
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			loadGroupList();
		}
	});	
	
}


function addGroup(){
	var active = 0;
	if($('#p_group_active').prop('checked')){
		active = 1;
	}
	$.ajax(processor, {
		"data":{
			"action":"add_group",
			"active":active,
			"group_name":$('#p_group_name').val()
		},
		"dataType":"json",
		"method":"POST",
		"success":function(jso) {
			loadGroupList();
			$('#div_modal_add_group').modal('hide');
		}
	});	
}