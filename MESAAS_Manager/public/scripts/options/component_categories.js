var processor = './ajax_processors/options/component_categories.php';
var component_categories = [];
var component_category_parts = [];
var chosen_system = {};
var chosen_line = {};

$(document).ready(
	function() {
		load_systems({}, function(data) {
			show_systems(data);
			chosen_system = data[0];
			load_lines({system_id:chosen_system.ID, line_type:0}, show_lines);
		});

		$('#btn-load').on('click', function() {
			loadAllData();
		});

		$('#div_parts TABLE TBODY').on('click', '.btn-add-part', function() {
			var d = $(this).data();
			d.edit_mode = 'ADD_PART';
			$('#div_add_edit_modal .modal-title').html('Add Part');
			$('#div_add_edit_modal .modal-body LABEL').html('New Part Number');

			$('#inpField').val('');

			$('#div_add_edit_modal').modal('show').data(d);
		});

		$('.btn-add-category').on('click', function() {
			$('#div_add_edit_modal .modal-title').html('Add Category');
			$('#div_add_edit_modal .modal-body LABEL').html('New Category Name');

			$('#inpField').val('');

			$('#div_add_edit_modal').modal('show').data('edit_mode', 'ADD_CATEGORY');
		});


		$('#div_parts TABLE TBODY').on('click', '.btn-edit-category', function() {
			var d = $(this).data();
			d.edit_mode = 'EDIT_CATEGORY';
			$('#div_add_edit_modal .modal-title').html('Edit Category');
			$('#div_add_edit_modal .modal-body LABEL').html('Category Name');

			$('#inpField').val(d.component_category_name);

			$('#div_add_edit_modal').modal('show').data(d);
		});

		$('#div_parts TABLE TBODY').on('click', '.btn-edit-part', function() {
			var d = $(this).data();
			d.edit_mode = 'EDIT_PART';
			$('#div_add_edit_modal .modal-title').html('Edit Part');
			$('#div_add_edit_modal .modal-body LABEL').html('Part Number');

			$('#inpField').val(d.part_number);

			$('#div_add_edit_modal').modal('show').data(d);
		});

		$('#div_parts TABLE TBODY').on('click', '.btn-edit-min', function() {
			var d = $(this).data();
			d.edit_mode = 'EDIT_MIN';
			$('#div_add_edit_modal .modal-title').html('Edit Minimum Qty');
			$('#div_add_edit_modal .modal-body LABEL').html('New Minimum');

			$('#inpField').val(d.min_qty);

			$('#div_add_edit_modal').modal('show').data(d);
		});

		$('#div_parts TABLE TBODY').on('click', '.btn-edit-max', function() {
			var d = $(this).data();
			d.edit_mode = 'EDIT_MAX';
			$('#div_add_edit_modal .modal-title').html('Edit Maximum Qty');
			$('#div_add_edit_modal .modal-body LABEL').html('New Maximum');

			$('#inpField').val(d.max_qty);

			$('#div_add_edit_modal').modal('show').data(d);
		});


		$('#div_parts TABLE TBODY').on('click', '.btn-delete-category', function() {
			var d = $(this).data();
			d.edit_mode = 'DELETE_CATEGORY';
			$('#div_delete_modal .modal-title').html('Delete Category');
			$('#div_delete_modal .modal-body').html('Are you sure you want to delete this category?');

			$('#div_delete_modal').modal('show').data(d);
		});

		$('#div_parts TABLE TBODY').on('click', '.btn-delete-part', function() {
			var d = $(this).data();
			d.edit_mode = 'DELETE_PART';
			$('#div_delete_modal .modal-title').html('Delete Part');
			$('#div_delete_modal .modal-body').html('Are you sure you want to delete this part number?');

			$('#div_delete_modal').modal('show').data(d);
		});


		$('BUTTON.btn-save').on('click', function() {
			var d = $.extend({}, $('#div_add_edit_modal').data());
			d.action = 'add_edit';
			d['bs.modal'] = '';
			d.new_value = $('#inpField').val();
			d.database = chosen_system.database_name;
			d.line_ID = chosen_line.line_ID;
			$.getJSON(processor, d, function(data) {
				$('#div_add_edit_modal').modal('hide');
				$.growl('Saved!', {"type":"success"});
				populateCategories(data);
			});
		});

		$('BUTTON.btn-confirm-delete').on('click', function() {
			var d = $.extend({}, $('#div_delete_modal').data());
			d.action = 'delete';
			d['bs.modal'] = '';
			d.database = chosen_system.database_name;
			d.line_ID = chosen_line.line_ID;
			$.getJSON(processor, d, function(data) {
				$('#div_delete_modal').modal('hide');
				$.growl('Record Deleted!', {"type":"success"});
				populateCategories(data);
			});
		});

		$('SPAN.glyphicon-question-sign').tooltip();
	}
);

function loadAllData() {
	chosen_line = $('#line_select OPTION:selected').data();
	console.log(chosen_line);
	$.getJSON(processor, {
			"action":"load_data",
			"database":chosen_system.database_name,
			"line_ID":chosen_line.line_ID
		}, populateCategories);
}

function populateCategories(jso) {
	$('.modal').modal('hide');
	component_categories = jso.component_categories || [];
	component_category_parts = jso.component_category_parts || [];

	$('#div_parts').show();
	var b = $('#div_parts TABLE TBODY').empty();
	for(var i = 0; i < component_categories.length; i++) {
		var r = $('<tr>').appendTo(b);
		var c = $('<td>').html(component_categories[i].component_category_name).appendTo(r);
		$('<span>').addClass('glyphicon glyphicon-pencil pointer btn-edit-category pad-both').data(component_categories[i]).appendTo(c);

		var c = $('<td>').appendTo(r);
		for (var j = 0; j < component_category_parts.length; j++) {
			if (component_categories[i].ID == component_category_parts[j].component_category_ID) {
				var sp = $('<span>').appendTo(c).addClass('label label-primary pad-right').html(component_category_parts[j].part_number + ' ');
				$('<span>').addClass('glyphicon glyphicon-pencil pointer btn-edit-part pad-both').data(component_category_parts[j]).appendTo(sp);
				$('<span>').addClass('glyphicon glyphicon-trash pointer btn-delete-part pad-both').data(component_category_parts[j]).appendTo(sp);
			}
		}

		var c = $('<td>').html(component_categories[i].min_qty).appendTo(r);
		$('<span>').addClass('glyphicon glyphicon-pencil pointer btn-edit-min pad-both').data(component_categories[i]).appendTo(c);
		var c = $('<td>').html(component_categories[i].max_qty).appendTo(r);
		$('<span>').addClass('glyphicon glyphicon-pencil pointer btn-edit-max pad-both').data(component_categories[i]).appendTo(c);

		var btngroup = $('<div>').addClass('btn-group');
		$('<button>').addClass('btn btn-xs btn-success btn-add-part').html('<span class="glyphicon glyphicon-plus"></span> Add Part').data(component_categories[i]).appendTo(btngroup);
		$('<button>').addClass('btn btn-xs btn-danger btn-delete-category').html('<span class="glyphicon glyphicon-trash"></span> Delete Category').data(component_categories[i]).appendTo(btngroup);
		var c = $('<td>').append(btngroup).appendTo(r);
	}
}