
var sound_uploaders = {};
var sound_uploader_names = ['station_success', 'station_failure'];

$(function() {
	for (var i = 0; i < sound_uploader_names.length; i++) {
		var sound_uploader_name = sound_uploader_names[i];
		sound_uploaders[sound_uploader_name] = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'btn_select_' + sound_uploader_name + '_sound',
			container: document.getElementById('div_' + sound_uploader_name + '_upload_container'),
			url : './ajax_processors/options/instructions_sound_upload.php',
			flash_swf_url : '../js/Moxie.swf',
			silverlight_xap_url : '../js/Moxie.xap',
			multi_selecttion: false,
			filters: {max_file_size : '2000kb',	mime_types: [{title : "Sound files", extensions : "wav,mp3"}]},
			init: {
				PostInit: function() { document.getElementById('sp_' + sound_uploader_name + '_file_selected').innerHTML = '';	},
				FilesAdded: function(up, files) {
					document.getElementById('sp_station_success_file_selected').innerHTML = '';
					//$('.btn-validate-save').attr('disabled', true); //disable saving while file is uploading
					plupload.each(files, function(file) {
						document.getElementById('sp_' + sound_uploader_name + '_file_selected').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
					});
					up.start();
				},
				UploadProgress: function(up, file) {
					var company_code = $.grep(systems_lines_stations.systems, function(s, i) {return s.ID == cur_system_ID;})[0]['mrp_company_code'];
					document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="' + file.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + file.percent + '%;">' + file.percent + '%</div></div>';

					if (file.percent >= 100) {
						//document.getElementById('inpImageFile').value = file.name;
						//$('#div_no_image').hide();
						//$('#imgInstrImage').attr('src', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + file.name);
						//$('#aInstrImage').show().attr('href', base_image_url.replace('%SYSTEM_ID%', company_code).replace('%LINE_ID%', cur_line_ID).replace('%STATION_ORDER%', cur_station_ID) + file.name);
						document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '';
						//$('.btn-validate-save').attr('disabled', false);
					}
				},
				Error: function(up, err) {	alert('Error uploading sound: ' + err.message);	}
			}
		});

		sound_uploaders[sound_uploader_name].init();
	}
});