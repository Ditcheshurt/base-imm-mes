var processor = './ajax_processors/options/email_alerts.php';
var filter_processor = './ajax_processors/filter.php';

var email_alert_panel;
var email_alert_table;
var edit_email_alert_modal;
var remove_email_alert_modal;
var alert_type_select;
var machine_select;
var operator_select;

var machines;
var operators;


$(document).ready(function () {
	email_alert_panel = $('#email_alert_panel');
	email_alert_table = $('#email_alert_table');
	edit_email_alert_modal = $('#edit_email_alert_modal');
	remove_email_alert_modal = $('#remove_email_alert_modal');
	alert_type_select = $('#alert_type_select');
	machine_select = $('#machine_select');
	operator_select = $('#operator_select');

	init_selects();
	init_listeners();
	load_email_alerts(show_email_alerts);
});

function init_selects () {
	load_alert_types(show_alert_types);
	load_machines(show_machines);
	load_operators(show_operators);
}

function init_listeners () {
	email_alert_panel.on('click', '.btn-add-email-alert', function (e) {
		edit_email_alert_modal.find('.modal-title').html('Add Email Alert');
		edit_email_alert_modal.attr('data-action', 'insert_email_alert');
		show_email_alert_modal(null);
	});

	email_alert_table.on('click', '.btn-edit-email-alert', function (e) {
		edit_email_alert_modal.find('.modal-title').html('Edit Email Alert');
		edit_email_alert_modal.attr('data-action', 'update_email_alert');
		var email_alert_ID = $(e.target).closest('tr').attr('data-email-alert-id');
		load_email_alert(email_alert_ID, show_email_alert_modal);
	});

	$('#select_all_days_checkbox').on('change', function () {
		var all = $(this).prop('checked') ? 1 : 0;
		$('.day-checkbox', edit_email_alert_modal).prop('checked', all);
	});

	$('.day-checkbox', edit_email_alert_modal).on('change', function () {
		var select_all_days_checkbox = $('#select_all_days_checkbox');
		if (select_all_days_checkbox.prop('checked')) {
			var all_checked = true;
			$('input.day-checkbox', edit_email_alert_modal).each(function (i, input) {
				var this_checked = $(this).prop('checked') ? 1 : 0;
				all_checked = all_checked && this_checked;
			});

			select_all_days_checkbox.prop('checked', all_checked);
		}
	});

	$('#select_all_shifts_checkbox').on('change', function () {
		var all = $(this).prop('checked') ? 1 : 0;
		$('.shift-checkbox', edit_email_alert_modal).prop('checked', all);
	});

	$('.shift-checkbox', edit_email_alert_modal).on('change', function () {
		var select_all_shifts_checkbox = $('#select_all_shifts_checkbox');
		if (select_all_shifts_checkbox.prop('checked')) {
			var all_checked = true;
			$('input.shift-checkbox', edit_email_alert_modal).each(function (i, input) {
				var this_checked = $(this).prop('checked') ? 1 : 0;
				all_checked = all_checked && this_checked;
			});

			select_all_shifts_checkbox.prop('checked', all_checked);
		}
	});

	edit_email_alert_modal.on('change keyup focus blur', 'input, textarea, select', validate_alert_input);

	edit_email_alert_modal.on('click', '.btn-confirm-edit', function () {
		var schedule = [];
		$('input.day-checkbox', edit_email_alert_modal).each(function (i, input) {
			var active = $(this).prop('checked') ? 1 : 0;
			var the_day = {the_day: $(this).val(), active: active};
			schedule.push(the_day);
		});

		var shifts = [];
		$('input.shift-checkbox', edit_email_alert_modal).each(function (i, input) {
			var active = $(this).prop('checked') ? 1 : 0;
			var the_shift = {the_shift: $(this).val(), active: active};
			shifts.push(the_shift);
		});

		var m = machine_select.val();
		if (m.indexOf('all') > -1) {
			m = machines.map(function (machine) {return machine.ID;});
		}

		var o = operator_select.val();
		if (o.indexOf('all') > -1) {
			o = operators.map(function (operator) {return operator.ID;});
		}

		console.log(machines);
		console.log(operators);

		var email_alert_data = {
			alert_ID: edit_email_alert_modal.attr('data-email-alert-id'),
			description: $('#description_input').val(),
			alert_type_ID: $('#alert_type_select').val(),
			max_threshold: $('#max_threshold_input').val(),
			machines: m,
			operators: o,
			active: $('#active_checkbox').prop('checked') ? 1 : 0,
			schedule: schedule,
			shifts: shifts
		};

		delegate_email_alert_edit(email_alert_data);
	});

	email_alert_table.on('click', '.btn-remove-email-alert', function (e) {
		var email_alert_ID = $(e.target).closest('tr').attr('data-email-alert-id');
		remove_email_alert_modal.attr('data-action', 'delete_email_alert')
								 .attr('data-email-alert-id', email_alert_ID)
								 .modal('show').data(null);
	});

	remove_email_alert_modal.on('click', '.btn-confirm-remove', function (e) {
		var email_alert_ID = remove_email_alert_modal.attr('data-email-alert-id');
		delete_email_alert(email_alert_ID, email_alert_deleted);
	});
}

function load_email_alerts (cb) {
	$.getJSON(processor, {action: 'get_email_alerts'}).done(cb);
}

function show_email_alerts (data) {
	var tbody = $('tbody', email_alert_table);
	tbody.empty();

	if (data) {
		data.forEach(function (email_alert) {
			var tr = $('<tr></tr>').prop('id', 'email_alert_'+email_alert.ID)
								   .attr('data-email-alert-id', email_alert.ID)
								   .append($('<td>'+email_alert.type_description+'</td>'))
								   .append($('<td>'+email_alert.description+'</td>'))
								   .append($('<td>'+email_alert.max_threshold+'</td>'))
								   .append($('<td>'+(email_alert.active ? 'Yes' : 'No')+'</td>'));
			var actions = $('<td></td>').appendTo(tr);
			var btn_group = $('<div class="btn-group" role="group"></div>').appendTo(actions);
			var edit_btn = $('<button type="button" class="btn btn-info btn-xs btn-edit-email-alert"><span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" title="Edit"></span></button>').appendTo(btn_group);
			var delete_btn = $('<button type="button" class="btn btn-danger btn-xs btn-remove-email-alert"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" data-toggle="tooltip" title="Remove"></span></button>').appendTo(btn_group);
			tbody.append(tr);
		});
	}
}

function reset_email_alert_modal () {
	edit_email_alert_modal.find('input[type="text"], input[type="number"], textarea').val('');
	edit_email_alert_modal.find('select').val(null);
	edit_email_alert_modal.find('input[type="checkbox"]').prop('checked', false);
	operator_select.select2('val', null);
	machine_select.select2('val', null);
	validate_alert_input();
}

function load_email_alert (email_alert_ID, cb) {
	$.getJSON(processor, {action: 'get_email_alert', alert_ID: email_alert_ID}).done(cb);
}

function show_email_alert_modal (data) {
	reset_email_alert_modal();
	if (data) {
		edit_email_alert_modal.attr('data-email-alert-id', data.ID);
		$('#description_input').val(data.description);
		$('#alert_type_select').val(data.alert_type_ID);
		$('#max_threshold_input').val(data.max_threshold);
		$('#active_checkbox').prop('checked', data.active);

		if (data.machines) {
			var md = data.machines.map(function (machine) {
				return machine.machine_ID;
			});
			machine_select.select2('val', md);
		}

		if (data.operators) {
			var od = data.operators.map(function (operator) {
				return operator.operator_ID;
			});
			operator_select.select2('val', od);
		}

		if (data.schedule) {
			data.schedule.forEach(function (day) {
				$('input.day-checkbox[value="'+day.the_day+'"]', edit_email_alert_modal).prop('checked', day.active);
			});
		}

		if (data.shifts) {
			data.shifts.forEach(function (shift) {
				$('input.shift-checkbox[value="'+shift.the_shift+'"]', edit_email_alert_modal).prop('checked', shift.active);
			});
		}
	}
	edit_email_alert_modal.modal('show');
}

function load_alert_types (cb) {
    $.getJSON(processor, {action: 'get_alert_types'}).done(cb);
}

function show_alert_types (data) {
	if (!data) {
		data = [];
	}

	alert_type_select.empty();
	var placeholder_option = $('<option></option>').prop({
		id: 'alert_type_ID_placeholder',
		name: 'alert_type_ID',
	}).text('Select Alert Type').appendTo(alert_type_select);

	data.forEach(function (alert_type) {
		var option = $('<option></option>').prop({
			id: 'alert_type_ID_'+alert_type.ID,
			name: 'alert_type_ID',
		}).val(alert_type.ID)
		  .text(alert_type.description)
		  .appendTo(alert_type_select);
	});
}

function load_machines (cb) {
	$.getJSON(filter_processor, {action: 'get_machines'}).done(function (data) {
		machines = data;
		cb(data);
	});
}

function show_machines (data) {
	if (!data) {
		data = [];
	}
	var d = data.map(function (machine) {
		return {id: machine.ID, text: machine.machine_number+' - '+machine.machine_name};
	});
	d.unshift({id: 'all', text: 'All Machines'})

	machine_select.select2({
		placeholder: "Select Machine",
		data: d
	});
}

function load_operators (cb) {
	$.getJSON(processor, {action: 'get_operators'}).done(function (data) {
		operators = data;
		cb(data);
	});
}

function show_operators (data) {
	if (!data) {
		data = [];
	}
	var d = data.map(function (operator) {
		return {id: operator.ID, text: operator.name+' ('+operator.email+')'};
	});
	d.unshift({id: 'all', text: 'All Employees'})

	operator_select.select2({
		placeholder: "Select Employee",
		data: d
	});
}

function insert_email_alert (data, cb) {
	data.action = 'insert_alert';
	$.ajax({
		url: processor,
		type: 'POST',
		data: data,
		dataType: 'json'
	}).done(cb);
}

function email_alert_inserted (data) {
	if (data === null) {
		$.growl('Email alert created.', {type: 'success'});
		load_email_alerts(show_email_alerts);
	} else {
		$.growl('Unable to create email alert.', {type: 'danger'});
	}
}

function update_email_alert (data, cb) {
	data.action = 'update_alert';
	$.ajax({
		url: processor,
		type: 'POST',
		data: data,
		dataType: 'json'
	}).done(cb);
}

function email_alert_updated (data) {
	if (data === null) {
		$.growl('Email alert updated.', {type: 'success'});
		load_email_alerts(show_email_alerts);
	} else {
		$.growl('Unable to update email alert.', {type: 'danger'});
	}
}

function delete_email_alert (ID, cb) {
	$.ajax({
		url: processor,
		type: 'POST',
		data: {action: 'delete_alert', alert_ID: ID},
		dataType: 'json'
	}).done(cb);
}

function email_alert_deleted (data) {
	if (data === null) {
		$.growl('Email alert deleted.', {type: 'success'});
		load_email_alerts(show_email_alerts);
	} else {
		$.growl('Unable to delete email alert.', {type: 'danger'});
	}
}

function validate_alert_input () {
	var confirm_btn = $('.btn-confirm-edit', edit_email_alert_modal);

	var valid = true;
	$('form [required]', edit_email_alert_modal).each(function (i, field) {
		var f = $(field);
		var is_empty = !f.val() || f.val() < 1;
		valid = valid && !is_empty;
	});

	if (valid) {
		confirm_btn.show();
	} else {
		confirm_btn.hide();
	}
}

function delegate_email_alert_edit (email_alert_data) {
	var action = edit_email_alert_modal.attr('data-action');
	if (action === 'insert_email_alert') {
		insert_email_alert(email_alert_data, email_alert_inserted);
	} else if (action === 'update_email_alert') {
		update_email_alert(email_alert_data, email_alert_updated);
	}
}