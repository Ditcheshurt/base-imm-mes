(function ($) {
    var processor = 'ajax_processors/options/table.parts.php';
    $(document).ready(function () {

        var editor = new $.fn.dataTable.Editor({
            colReorder: true,
            ajax: processor,
            table: '#parts',
            fields: [
                {
                    "label": "Part Description",
                    "name": "part_desc"
                },
                {
                    "label": "Part Number",
                    "name": "part_number"
                },
                {
                    "label": "MRP Part Num",
                    "name": "mrp_part_number"
                },
                {
                    "label": "Part Image",
                    "name": "part_image",
                    type: "upload",
                    display: function (data) {
                        return '<img src="' + data + '" width="100px" height="100" />';
                    },
                    clearText: "Clear",
                    noImageText: 'No image'
                },
                {
                    "label": "Std Pack Qty",
                    "name": "standard_pack_qty"
                },
                {
                    "label": "Part Wgt",
                    "name": "part_weight"
                },
                {
                    "label": "Box Wgt",
                    "name": "box_weight"
                },
                {
                    "label": "MRP Co. Code",
                    "name": "mrp_company_code"
                },
                {
                    "label": "FG Part ID",
                    "name": "fg_part_id"
                },
                {
                    "label": "Active",
                    "name": "active",
                    "type": "checkbox",
                    default:0,
                    "separator": ",",
                    "options": [
                        { label: "", value: 1 }
                    ]
                }
            ]
        });

        var table = $('#parts').DataTable({
            colReorder: true,
            ajax: processor,
            fixedHeader: true,
            colReorder: true,
            select: "single",
            lengthChange: true,
            columns: [
                {
                    data: null,
                    defaultContent: '',
                    className: 'select-checkbox',
                    orderable: false
                },
                {
                    "data": "part_desc"
                },
                {
                    "data": "part_number"
                },
                {
                    "data": "mrp_part_number"
                },
                {
                    "data": "part_image",
                    type: "upload",
                    render: function (data) {
                        return data ?
                        '<img src="' + data + '" width="50px" height="50px" />' :
                            'No image';
                    }

                },
                {
                    "data": "standard_pack_qty"
                },
                {
                    "data": "part_weight"
                },
                {
                    "data": "box_weight"
                },
                {
                    "data": "mrp_company_code"
                },
                {
                    "data": "fg_part_id"
                },
                {
                    "data": "active",
                    className: "dt-body-center",
                    render: function (data, type, row) {
                        var isActive = row['active'] == null || row['active'] == false ? false : true;

                        return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;

                    }
                }
            ],
            select: true,
            lengthChange: false
        });

        new $.fn.dataTable.Buttons(table, [
            {
                text: 'Reload',
                action: function (e, dt, node, config) {
                    dt.ajax.reload();
                }
            },
            {extend: "create", editor: editor},
            {extend: "edit", editor: editor},
            {extend: "remove", editor: editor}
        ]);

        table.buttons().container()
            .appendTo($('.col-sm-6:eq(0)', table.table().container()));

    });

}(jQuery));