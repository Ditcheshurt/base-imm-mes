
var menu = null;
$(document).ready(
	function() {
		$.getJSON('./config/menu.json', {}, buildMenu);
	}
);

function buildMenu(jso) {
	menu = jso;
	var n = $('.navbar-nav');
	for (section in jso) {
		var section_obj = jso[section];
		var li = $('<li />').addClass('dropdown');
		var a = $('<a />').addClass('dropdown-toggle').attr('href', '#').attr('data-toggle', 'dropdown').html(section_obj.name).append($('<span />').addClass('caret'));
		li.append(a);

		var ul = $('<ul />').addClass('dropdown-menu').attr('role', 'menu');
		for (var i = 0; i < section_obj.items.length; i++) {
			var item = section_obj.items[i];
			var li2 = $('<li />');
			li2.data(item);
			if (item.is_divider) {
				li2.addClass('divider');
			} else {
				if (item.is_header) {
					li2.addClass('dropdown-header').attr("role", "presentation").html(item.name);
				} else {
					if (item.is_external) {
						var a = $('<a />').attr("href", item.url);
						a.attr("target", item.target);
						a.append(item.name);
						li2.append(a);
					} else {
						var a = $('<a />').attr("href", section_obj.page + '?type=' + item.type);
						if (item.icon) {
							a.append($('<span />').addClass('glyphicon glyphicon-' + item.icon));
							a.append(' ');
						}
						a.append(item.name);
						li2.append(a);
					}
				}
			}

			ul.append(li2);
		}

		li.append(ul);
		n.append(li);
	}

	showHideMenuItems();
}

function showHideMenuItems() {
	if (!menu) {
		return;
	}

	$('.navbar-nav LI UL LI').each(function(i, obj) {
		var li2 = $(obj);
		var item = li2.data();
		//hide it?
		if (item.hidden) {
			li2.hide();
		} else {
			if (item.viewer_roles) {
				if (item.viewer_roles.indexOf('all') < 0) {
					if (LOGIN.operator.ID != 0) {
						var has_access = false;
						for (var x = 0; x < item.viewer_roles.length; x++) {
							for (var y = 0; y < LOGIN.operator_roles.length; y++) {
								if (item.viewer_roles[x] == LOGIN.operator_roles[y].role) {
									has_access = true;
									break;
								}
							}
						}
						if (!has_access) {
							li2.attr('disabled', true);
							li2.addClass('disabled');
						} else {
							li2.attr('disabled', false);
							li2.removeClass('disabled');
						}
					} else {
						li2.attr('disabled', true);
						li2.addClass('disabled');
					}
				}
			}
		}
	});
}