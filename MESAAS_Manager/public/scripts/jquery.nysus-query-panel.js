//var defaults = [{type:'select', label:'System', id:'sel_system', loader: get_systems()}, {type:'select', label:'Line', id:'sel_line', loader: get_lines()}];

var criteria_controls = [
	{label_text: 'System'
		, label_tooltip: 'Select System'
		, control_class: ''
		, control_id: 'sel_system'
		, control_tag: 'select'},
	{label_text: 'Line'
		, label_tooltip: 'Select Line'
		, control_class: ''
		, control_id: 'sel_line'
		, control_tag: 'select'},
	{label_text: 'Start Date'
		, label_tooltip: 'Select start date'
		, control_class: ''
		, control_id: 'sel_start_date'
		, control_tag: 'input type="date"'},
	{label_text: 'End Date'
		, label_tooltip: 'Select end date'
		, control_class: ''
		, control_id: 'sel_end_date'
		, control_tag: 'input type="date"'},
	{label_text: 'Station'
		, label_tooltip: 'Select station'
		, control_class: ''
		, control_id: 'sel_station'
		, control_tag: 'select'},
	{label_text: 'Shift'
		, label_tooltip: 'Select shift'
		, control_class: ''
		, control_id: 'sel_shift'
		, control_tag: 'select'},
	{label_text: 'Operator'
		, label_tooltip: 'Select operator'
		, control_class: ''
		, control_id: 'sel_operator'
		, control_tag: 'select'},
	{label_text: 'Group By'
		, label_tooltip: 'Select group by'
		, control_class: ''
		, control_id: 'sel_group_by'
		, control_tag: 'select'}
];


$(function () {
	
	$.each(criteria_controls, function(k,v) {
		var ctl = $('<div />').nysus_labeled_control(v);
		$('#test .panel-body').append(ctl);
	});
	
	//populate_query_panel($('#test'));
});

function populate_query_panel(elem, options) {
	$.extend(defaults, options);
	
	$(elem).append('<div class="panel panel-primary"><div class="panel-heading">Criteria Panel</div><div class="panel-body"></div></div>');
	
	$.each(defaults, function(k,v) {
		var ctl = '';
		switch(v.type) {
			case 'input':
				ctl = '<input type="' + v.type + '" id="' + v.id + '" value="' + v.value + '" />';
				if (v.loader && $.isFunction(v.loader)) {
					v.loader;
				}				
				break;
			default:
				ctl = '';
				break;
		}
		
		$(elem).find('.panel-body').append(ctl);
	});
}

function get_systems() {
	alert('poop');
}

function get_lines() {
	
}