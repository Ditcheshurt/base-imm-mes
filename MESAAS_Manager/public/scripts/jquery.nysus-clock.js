 // the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;
(function($, window, document, undefined) {
    
    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = 'nysus_clock', 
    defaults = {
        propertyName: 'value'
        ,interval: 1000
		,clock_hr: 0
		,clock_min: 0
		,clock_sec: 0
    };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function() {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            
            getServerTime(this.settings);
            setInterval($.proxy(tick, this), this.settings.interval);
        },
        refresh: function() {
            var n = this.settings.clock_hr + ':' + leftPad(this.settings.clock_min, 2) + ":" + leftPad(this.settings.clock_sec, 2) + " ";
            $(this.element).html(n);
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };
    
    function getServerTime(settings) {
        $.ajax({
            method: "POST",
            url: "./ajax_processors/common.php",
            data: {action: 'get_datetime'}
        }).done(function(jso) {
            if (jso != null && jso.length > 0) {
				settings.clock_hr = jso[0].hours;
				settings.clock_min = jso[0].minutes;
                settings.clock_sec = jso[0].seconds;
            }
        });
    }
    
    function tick() {
        var sec = this.settings.interval / 1000;
        this.settings.clock_sec += sec;
        if (this.settings.clock_sec > 59) {
            this.settings.clock_sec = 0;
            this.settings.clock_min += 1;
        }
        if (this.settings.clock_min > 59) {
            this.settings.clock_min = 0;
            this.settings.clock_hr += 1;
        }
        if (this.settings.clock_hr > 23) {
            this.settings.clock_hr = 0;
        }
        this.refresh();
    }
    
    function leftPad(number, targetLength) {
        var output = number + '';
        while (output.length < targetLength) {
            output = '0' + output;
        }
        return output;
    }


})(jQuery, window, document);
