// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

	"use strict";

		// undefined is used here as the undefined global variable in ECMAScript 3 is
		// mutable (ie. it can be changed by someone else). undefined isn't really being
		// passed in so we can ensure the value of it is truly undefined. In ES5, undefined
		// can no longer be modified.

		// window and document are passed through as local variable rather than global
		// as this (slightly) quickens the resolution process and can be more efficiently
		// minified (especially when both are regularly referenced in your plugin).

		// Create the defaults once
		var pluginName = 'nysus_labeled_control',
				defaults = {
				propertyName: 'value'
				, label_text: 'Control Label'
				, label_tooltip: 'Control label tooltip'
				, control_class: 'nysus_labeled_control'
				, control_id: ''
				, control_tag: 'input'
				, control_type: ''
		};

		// The actual plugin constructor
		function Plugin ( element, options ) {
				this.element = element;
				// jQuery has an extend method which merges the contents of two or
				// more objects, storing the result in the first object. The first object
				// is generally empty as we don't want to alter the default options for
				// future instances of the plugin
				this.settings = $.extend( {}, defaults, options );
				this._defaults = defaults;
				this._name = pluginName;
				this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
				init: function () {
						// Place initialization logic here
						// You already have access to the DOM element and
						// the options via the instance, e.g. this.element
						// and this.settings
						// you can add more functions like the one below and
						// call them like so: this.yourOtherFunction(this.element, this.settings).
						//console.log(this.element);
						
						buildControl(this.element, this.settings);
				}
		});

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function ( options ) {
				return this.each(function() {
						if ( !$.data( this, "plugin_" + pluginName ) ) {
								$.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
						}
				});
		};
		
		function buildControl(elem, settings) {			
			elem = $(elem);
			elem.addClass('form-group');
			var l = $('<label class="control-label" />');
			l.attr('for', settings.control_id);
			l.html(settings.label_text + '&nbsp;&nbsp;');
			var s = $('<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" />');
			s.attr('title', settings.label_tooltip);
			s.appendTo(l);
			var b = $('<div class="control-body" />');
			var c = $('<' + settings.control_tag + ' type="' + settings.control_type + '" class="form-control ' + settings.control_class + '" id="' + settings.control_id + '" />');
			c.appendTo(b);
			l.appendTo(elem);
			b.appendTo(elem);
		}

})( jQuery, window, document );