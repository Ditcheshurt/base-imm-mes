var NYSUS = NYSUS || {};
NYSUS.OJT = NYSUS.OJT || [];

NYSUS.OJT = {
	datatables_options: {
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"paging": true,
		"info": true,
		"width": "100%",
		"scrollX": true,
		"dom": 'T<"clear">lfrtip',
		"tableTools": {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		}
	},

	render_group_tree: function (el, with_inactive) {
		$.getJSON('./ajax_processors/api/ojt/groups.php', {with_inactive: with_inactive}, function (jso) {
			var tree = [];
			$.map(jso, function (v) {
				if (v.parent_group_ID == null || v.parent_group_ID == 0) {
					var node = {
						ojt_group_ID: v.ID,
						ojt_group_description: v.group_description,
						text: build_node_text(v)
					};
					tree.push(node);
					find_children(v, node);
				}
			});
			function find_children(parent, node) {
				$.map(jso, function (v) {
					if (v.parent_group_ID == parent.ID) {
						var child_node = {
							ojt_group_ID: v.ID,
							ojt_group_description: v.group_description,
							text: build_node_text(v)
						};
						if (node.nodes == undefined) {
							node.nodes = [];
						}
						node.nodes.push(child_node);
						find_children(v, child_node);
					}
				});
			}

			function build_node_text(jso) {
				var node_text = jso.group_description;
				if (jso.active) {
					node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-success pull-right">Active</span>';
				} else {
					node_text = '<span>' + jso.group_description + '</span><span class="btn btn-xs btn-danger pull-right">Inactive</span>';
				}
				return node_text;
			}

			$(el).treeview({data: tree});

		});

	},

	get_operator_requirements: function (operator_ID, with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				operator_ID: operator_ID,
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_groups: function (with_inactive, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/groups.php',
			type: "POST",
			dataType: 'json',
			data: {
				with_inactive: with_inactive
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_group: function (ojt_group_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/groups.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_group_ID: ojt_group_ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	create_group: function (ojt_parent_group_ID, ojt_group_description, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/groups.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'create',
				ojt_parent_group_ID: ojt_parent_group_ID,
				ojt_group_description: ojt_group_description
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	update_group: function (ojt_group_ID, ojt_parent_group_ID, ojt_group_description, active, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/groups.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'update',
				ojt_group_ID: ojt_group_ID,
				ojt_parent_group_ID: ojt_parent_group_ID,
				ojt_group_description: ojt_group_description,
				active: active
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	get_group_requirements: function (ojt_group_ID, with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_group_ID: ojt_group_ID,
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	create_group_requirement: function (ojt_group_ID, ojt_requirement_ID, min_operators, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'create',
				ojt_group_ID: ojt_group_ID,
				ojt_requirement_ID: ojt_requirement_ID,
				min_operators: min_operators
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	update_group_requirement: function (ojt_group_ID, ojt_requirement_ID, min_operators, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'update',
				ojt_group_ID: ojt_group_ID,
				ojt_requirement_ID: ojt_requirement_ID,
				min_operators: min_operators
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	delete_group_requirement: function (id, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'delete',
				ID: id
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_group_summary: function (ojt_group_ID, with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/summary.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_group_ID: ojt_group_ID,
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_group_operators: function (ojt_group_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/operators.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_group_ID: ojt_group_ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	create_group_operator: function (ojt_group_ID, operator_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/operators.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'create',
				ojt_group_ID: ojt_group_ID,
				operator_ID: operator_ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	delete_group_operator: function (ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/operators.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'delete',
				ID: ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_requirements: function (with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/requirements.php',
			type: "POST",
			dataType: 'json',
			data: {
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	get_template_roles: function (ojt_signoff_template_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/roles.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_signoff_template_ID: ojt_signoff_template_ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	get_template_levels: function (signoff_template_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/levels.php',
			type: "POST",
			dataType: 'json',
			async: false,
			data: {
				ojt_signoff_template_ID: signoff_template_ID
			},
			success: function (jso) {
				cb(jso);
			}
		});

	},

	get_signoffs: function (ojt_group_ID, ojt_requirement_ID, with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/signoffs.php',
			type: "POST",
			dataType: 'json',
			data: {
				ojt_group_ID: ojt_group_ID,
				ojt_requirement_ID: ojt_requirement_ID,
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	get_operator_signoffs: function (operator_ID, with_alerts, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/signoffs.php',
			type: "POST",
			dataType: 'json',
			data: {
				operator_ID: operator_ID,
				with_alerts: with_alerts
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	get_operator_requirement_signoffs: function (operator_ID, ojt_requirement_ID, with_expired, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/signoffs.php',
			type: "POST",
			dataType: 'json',
			data: {
				operator_ID: operator_ID,
				ojt_requirement_ID: ojt_requirement_ID,
				with_expired: with_expired
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});

	},

	create_signoff: function (ojt_requirement_ID, operator_ID, ojt_signoff_template_levels_roles_ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/signoffs.php',
			type: "POST",
			dataType: 'json',
			data: {
				action: 'create',
				ojt_requirement_ID: ojt_requirement_ID,
				operator_ID: operator_ID,
				ojt_signoff_template_levels_roles_ID: ojt_signoff_template_levels_roles_ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	},

	delete_signoff: function (ID, cb) {
		$.ajax({
			url: './ajax_processors/api/ojt/signoffs.php',
			method: "POST",
			dataType: 'json',
			data: {
				action: 'delete',
				ID: ID
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	}

};