var NYSUS = NYSUS || {};

$.extend(NYSUS, {

	load_app_config: function (cb) {
		$.getJSON('./config/app_config.json', function (jso) {
			if (typeof cb == 'function') {
				cb(jso);
			}
		});
	}

});