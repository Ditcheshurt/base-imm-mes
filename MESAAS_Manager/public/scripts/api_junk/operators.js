var NYSUS = NYSUS || {};

$.extend(NYSUS, {

	get_operators: function(with_inactive, cb) {
		$.ajax({
			url: './ajax_processors/api/operators.php',
			type: "POST",
			dataType: 'json',
			data: {
				with_inactive: with_inactive
			},
			success: function (jso) {
				if (typeof cb == 'function') {
					cb(jso);
				}
			}
		});
	}

});