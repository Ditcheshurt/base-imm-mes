var processor = './ajax_processors/report_wire_harness_kitting.php?action=get_wire_harness_info&line_id=110';

$(document).ready(function () {  
	self.table = $('#rack_table').DataTable({             
        ajax: {
            url: processor,
            dataSrc: ''
        },
        
        order: [[4, 'desc']],

        columns: [
            {data: 'ID'},
            {data: 'start_time'},
            {data: 'status'},
            {data: 'start_time'},
            {
                data: 'built_time',
                render:  function (data, type, full, meta) {
                    if (data) {
                        return data.date;
                    } else {
                        return '';
                    }
                }
            }
        ]
    });

    $('#rack_table tbody').on('click', 'tr', function () {
        var row = self.table.row(this);
        window.location.assign('reports.php?type=report_wire_harness_kitting.php&rack_ID='+row.data().ID);
    });
});