//*
var processor = './ajax_processors/reports/custom/meb/alarm_display.php';

//$(document).ready(
//	loadData
//);

$(function () {
	loadData();
	
	// reload data every 10 seconds
	setInterval(loadData, 10000);

	// reload the page after 30 minutes
	setTimeout(function () {
		window.location.reload(true);
	}, 1200000);
});

function loadData() {
	
	var ph = document.getElementById("ph");
	var ph = ph.innerText
	var ph = ph.substring(6,7)
	
	$.getJSON(processor, {
		'action':'get_alarms',
		'machine_id': ph
	}, populate);	
}

function populate(jso) {
	
	//hide nav bar
	$('[role="navigation"]').remove();
	
	// display heading
	var d = new Date().toLocaleString();
	//var heading =  jso.alarms[0].machine_name + ' Alarm Status Report';
	$('#panel-heading').html(d);
	
	// alarm status table
	
	var cols = ['Label Serial', 'Minutes Ago', 'Data Point', 'Value', 'Range', 'Disposition'];
	// table header
	th = $('#table-alarms .thead');
	th.empty()
	$.each(cols, function(k,v) {
		th.append('<th>' + v + '</th>');
	});	
	var t = $('#table-alarms .tbody');
	t.empty()
	$.each(jso.alarms, function(k,v) {
		var tr = '<tr>';		
		tr += '<td>'+ (v.serial_calc ? v.serial_calc : '') +'</td>';
		tr += '<td>'+ (v.cycle_time ? v.cycle_time : '') +'</td>';
		tr += '<td>'+ (v.data_point_desc ? v.data_point_desc : '') +'</td>';
		tr += '<td>'+ (v.value ? v.value : '') +'</td>';
		tr += '<td>'+ (v.range ? v.range : '') +'</td>';
		tr += '<td>'+ (v.disposition ? v.disposition : '') +'</td>';
		tr += '</tr>';
		t.append(tr);

	});
	
//	$('#table-carriers').on('click', '.current_module', function(el) {
//        var selection = $(this);
//        selection.focus();
//
//        if (!selection.data('popover-shown')) {
//            var module_id = selection.closest('tr').data('module_id');
//
//            $.getJSON(processor, {
//                'action':'get_module_detail',
//                'module_id': module_id
//            }, function (jso) {populateModuleDetail(selection, jso);}); 
//        } else {
//            selection.popover('hide');
//            selection.data('popover-shown', false);
//        }
//	});
}

//function populateModuleDetail(ctrl, jso) {
//    jso = jso.module_detail;
//    var title = 'Module ' + jso[0].build_order;
//    var markup = '<ul>';
//    markup += '<li>KitBox ' + jso[0].container_id + '</li>';
//    $.each(jso, function(k,v) {     
//        markup += '<li>' + v.internal_serial + ' - ' + v.part_desc + '</li>';
//    });
//    markup += '</ul>';
//
//    ctrl.popover({
//        title: title,
//        content: markup,
//        html: true,
//        placement: 'bottom'
//    }).popover('show');
//    ctrl.data('popover-shown', true);
//    //ctrl.html(markup);
//}