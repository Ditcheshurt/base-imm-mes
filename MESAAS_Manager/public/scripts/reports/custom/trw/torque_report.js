var processor = 'ajax_processors/reports/custom/trw/torque_report.php';
var dtResults;

$(document).ready(function () {
	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.controls.lines_select.multiple = false;
		NYSUS.REPORTS.REPORT_FILTER.controls.line_stations_select.multiple = false;

		NYSUS.REPORTS.REPORT_FILTER.hide_elements(['#operators-group']);
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(['#shifts-group']);
		$('.btn-run-report').on('click', function (e) {
			$('.btn-run-report').prop( "disabled", true );
			e.preventDefault();
			load_data();
		});
		$(document).on('report.complete', function() {
			$('.btn-run-report').prop( "disabled", false );
		});
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	function load_data() {
		var data = NYSUS.REPORTS.REPORT_FILTER.get_selections();
		data.action = 'get_torque_data';
		// destroy the table
		if (dtResults != null) {
			dtResults.destroy();
			$('#panel-data .panel-body').empty();
			$('#panel-data .panel-body').append('<table id="result-table" class="table table-striped table-condensed table-responsive"></table>');
		}
		$.getJSON(processor, data, function(jso) {
			$(document).trigger('report.complete');
			show_data(jso);
		});
	}

	function show_data(jso) {		

		if (jso.length == 0) {
			jso = [{'Result': 'No Data'}];
		}
		var cols = [];
		var objs = Object.getOwnPropertyNames(jso[0]);
		$.each(objs, function (k, v) {
			if (jso[0][v].date) {
				cols.push({data: v, title: v.toProperCase(), render: renderDate});
			} else {
				cols.push({data: v, title: v.toProperCase()});
			}
		});

		dtResults = $('#result-table').DataTable({
			destroy: true,
			responsive: true,
			dom: 'T<"clear">lfrtip',
			lengthMenu: [[25, 50, 100, -1], [25, 50, 100, "All"]],
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},
			data: jso,
			columns: cols
		});

	}

	String.prototype.toProperCase = function () {
		return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
			function ($1) {
				return $1.toUpperCase();
			}
		);
	};

	function renderDate(data, type, full) {
		if (data && data.date) {
			return moment(data.date).format('MM/DD/YYYY HH:mm');
		}
		return 'No Date';
	}

});