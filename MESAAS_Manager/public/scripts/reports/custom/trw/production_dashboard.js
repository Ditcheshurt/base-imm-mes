var processor = './ajax_processors/reports/custom/trw/production_dashboard.php';
var system_ID = 1;
var production_snapshots;
var content = $('#content');

$(function () {
	load_data();

	// reload data every 30 seconds
	setInterval(load_data, 30000);

	// reload the page after 30 minutes
	setTimeout(function () {
		window.location.reload(true);
	}, 1200000);
});

function load_data () {
	var clock = $('.clock-text').nysus_clock();
	load_production_snapshots(system_ID, show_production_snapshots);
	load_time_since_last_broadcast(system_ID, show_time_since_last_broadcast);
}

function load_production_snapshots (system_ID, cb) {
	$.getJSON(processor, {
		action: 'get_production_snapshots',
		system_ID: system_ID
	}).done(function (data) {
		if (data) {
			production_snapshots = data;
			cb(data);
		}
	});
}

function show_production_snapshots (data) {
	var table_container = $('#table_container', content);
	table_container.empty();

	var rows = [
		{name: 'module_code', title: 'Line', title_class: 'col-sm-2 col-title row-line cell-title', data_class: 'col-sm-1 cell-data row-line cell-title'},
		{name: 'prod_total', title: 'Today', title_class: 'col-sm-2 col-title group-prod-today cell-title', data_class: 'col-sm-1 cell-data group-prod-today'},
		{name: 'cust_prod_total', title: 'Customer', title_class: 'col-sm-2 col-title group-prod-today prod-customer cell-title', data_class: 'col-sm-1 cell-data group-prod-today prod-customer'},
		{name: 'diff_total', title: 'Difference', title_class: 'col-sm-2 col-title group-prod-today cell-title', data_class: 'col-sm-1 cell-data group-prod-today'},
		{name: 'yest_prod_total', title: 'Yesterday', title_class: 'col-sm-2 col-title cell-title', data_class: 'col-sm-1 cell-data'},
		{name: 'built_unshipped', title: 'Un-Shipped', title_class: 'col-sm-2 col-title cell-title', data_class: 'col-sm-1 cell-data'},
		{name: 'current_buffer', title: 'Buffer', title_class: 'col-sm-2 col-title cell-title', data_class: 'col-sm-1 cell-data'},
		{name: 'pre_queue', title: 'Pre-Queue', title_class: 'col-sm-2 col-title cell-title', data_class: 'col-sm-1 cell-data'},
		{name: 'current_queue', title: 'Queue', title_class: 'col-sm-2 col-title cell-title', data_class: 'col-sm-1 cell-data'}
	];

	//var exclude_lines = [210];
	var exclude_lines = [];

	var table = $('<table></table>').addClass('table').appendTo(table_container);
	var thead = $('<thead></thead>').appendTo(table);
	var tbody = $('<tbody></tbody>').appendTo(table);

	rows.forEach(function (row) {
		var tr = $('<tr></tr>').appendTo(tbody);
		var title_cell = $('<td></td>').addClass(row.title_class).text(row.title).appendTo(tr);
		
		production_snapshots.forEach(function (line, i) {
			if (exclude_lines.indexOf(line.line_ID) === -1) {
				var value = line[row.name];
				var cls = row.data_class + ' ' + get_data_cell_class(line, row, value);

				var cell = $('<td></td>').addClass(cls).html(value).appendTo(tr);
			}
		});
	});
}

function get_data_cell_class (line, row, data) {
	var cls = '';

	if (row.name === 'diff_total') {
		if (data >= 0) {
			cls = 'text-success';
		} else {
			cls = 'text-danger';
		}
	} else if (row.name === 'current_buffer') {
		if (data > line.buffer_warning) {
			cls = 'buffer-success';
		} else if (data > line.buffer_critical) {
			cls = 'buffer-warning';
		} else {
			cls = 'buffer-danger';
		}
	}

	return cls;
}

function load_time_since_last_broadcast (system_ID, cb) {
	$.getJSON(processor, {
		action: 'get_time_since_last_broadcast',
		system_ID: system_ID
	}).done(function (data) {
		if (data) {
			cb(data);
		}
	});
}

function show_time_since_last_broadcast (data) {
	var last_broadcast_container = $('#last_broadcast_container', content);
	last_broadcast_container.empty();

	var cls = '';
	var t = data.last_broadcast_time_ago;
	
	if (t !== null) {
		if (t < 6) {
			cls = 'broadcast-success';
		} else if (t < 20) {
			cls = 'broadcast-warning';
		} else {
			cls = 'broadcast-danger';
		}
	}
	
	var broadcast_str = (data.last_broadcast_time_ago !== null) ? ('Last Broadcast MSG: '+data.last_broadcast_time_ago+' min. ago') : 'No Data';
	$('<div></div>').addClass('col-sm-12 well last-broadcast-time-ago '+cls).text(broadcast_str).appendTo(last_broadcast_container);
}