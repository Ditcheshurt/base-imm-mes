var MBC = {

	init: function () {
		MBC.processor = './ajax_processors/reports/custom/trw/module_birth_certificate.php';
		MBC.main_div = $('#main_div');
	    MBC.module_ID = MBC.main_div.data('module-id');
	    MBC.mrp_company_code = MBC.main_div.data('mrp-company-code');
	    MBC.sub_assemblies = [];
	    MBC.batch_assemblies = [];
	    MBC.build_sections();
	    MBC.load_data();
	},

	build_sections: function () {
		$('<div>Module Birth Certificate (VIN: <span class="vin"></span> Sequence: <span class="sequence"></span>)</div>').addClass('main-header').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'module_overview'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'components'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'station_history'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'station_instruction_history'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'rework_station_instruction_history'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'module_station_torque_values'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'module_stored_plc_values'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'eol_test_data'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'main_line_images'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'chute_images'}).addClass('section').appendTo(MBC.main_div);
		$('<div></div>').prop({id:'sub_assemblies'}).appendTo(MBC.main_div);
	},

	load_data: function () {
		MBC.load_module(function (data) {
			MBC.show_module(data);
			MBC.load_components(MBC.show_components);
			MBC.load_station_history(function (data) {
				MBC.show_station_history(data);
				MBC.show_station_instruction_history(data);
			});
			MBC.load_rework_station_history(MBC.show_rework_staion_instruction_history);
			MBC.load_eol_test_data(MBC.show_eol_test_data);
			MBC.load_sub_assembly_history(function (data) {
				MBC.show_sub_assemblies(data);
				MBC.load_batch_assemblies(data);
			});
			MBC.load_images(function (data) {
				if (data) {
					if (data.main_line_images) {
						MBC.show_main_line_images(data.main_line_images);
					}
					if (data.chute_images) {
						MBC.show_chute_images(data.chute_images);
					}
				}
			});
			MBC.load_module_station_torque_values(MBC.show_module_station_torque_values);
			MBC.load_module_stored_plc_values(MBC.show_module_stored_plc_values);
		});
	},

	load_module: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_module',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.module = data;
	    		cb(MBC.module);
	    	}
	    });
	},

	show_module: function (module) {
	    //var section = $('<div id="module_overview" class="section"></div>').appendTo(MBC.main_div);
	    var section = $('#module_overview', MBC.main_div);
	    var title = $('<div class="section-header">Module Overview:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="module_overview_table" class="section-table"></table>').appendTo(content);

	    var tr = $('<tr></tr>').appendTo(table);
	    tr.append('<td><label>Broadcast Time:</label> <span class="broadcast-time"></span></td>');
	    tr.append('<td><label>Built Time:</label> <span class="built-time"></span></td>');

	    tr = $('<tr></tr>').appendTo(table);
	    tr.append('<td><label>VIN:</label> <span class="vin"></span></td>');
	    tr.append('<td><label>Sequence:</label> <span class="sequence"></span></td>');

	    if (module.reject_status) {
	        tr = $('<tr></tr>').appendTo(table);
	        tr.append('<td><label>Reject Status:</label> <span class="reject-status"></span></td>');
	    }

	    $('.vin').html(module.VIN);
	    $('.sequence').html(module.sequence);
	    $('.broadcast-time').html(module.recvd_time.date);
	    $('.built-time').html(module.built_time ? module.built_time.date : '');
	    $('.reject-status').html(module.reject_status);
	},

	load_components: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_components',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.components = data;
	    		cb(MBC.components);
	    	}
	    });
	},

	show_components: function (components) {
	    // var section = $('<div id="components" class="section"></div>').appendTo($('#info_div'));
	    var section = $('#components', MBC.main_div);
	    var title = $('<div class="section-header">Components:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="component_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);
	    
	    var cols = [
	        {data: 'part_number', title: 'Part Number'},
	        {data: 'qty', title: 'Quantity'}
	    ];

	    for (var i=0; i<2; i++) {
	        for (var j=0; j<cols.length; j++) {
	            htr.append('<th>'+cols[j].title+'</th>');
	        }
	    }

	    var tbody = $('<tbody></tbody>').appendTo(table);
	    var tr;

	    for (var i=0; i<components.length; i++) {
	        var c = components[i];

	        // create a new row if we're on an even index
	        if (i % 2 == 0) {
	            tr = $('<tr></tr>').appendTo(tbody);
	        }

	        for (var j=0; j<cols.length; j++) {
	            var column_name = cols[j].data;
	            if (cols[j].type != undefined && cols[j].type === 'datetime') {
	                var value = c[column_name].date;
	            } else {
	                var value = c[column_name];
	            }
	            tr.append('<td>'+value+'</td>');
	        }
	    }
	},

	load_station_history: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_station_history',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.station_history = data;
	    		cb(MBC.station_history);
	    	}
	    });
	},

	show_station_history: function (station_history) {
	    var section = $('#station_history', MBC.main_div);
	    var title = $('<div class="section-header">Station History:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="station_history_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);
	    
	    var cols = [
	        {data: 'station_desc', title: 'Station'},
	        {data: 'operator_name', title: 'Operator'},
	        {data: 'entry_time', title: 'Start Time', type: 'datetime'},
	        {data: 'exit_time', title: 'End Time', type: 'datetime'},
	        {data: 'cycle_time', title: 'Cycle Time'},
	    ];

	    for (var i=0; i<cols.length; i++) {
	        htr.append('<th>'+cols[i].title+'</th>');
	    }

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    for (var i=0; i<station_history.length; i++) {
	        var station = station_history[i];
	        var tr = $('<tr></tr>').appendTo(tbody);
	        for (var j=0; j<cols.length; j++) {
	            var column_name = cols[j].data;
	            if (cols[j].type != undefined && cols[j].type === 'datetime') {
	                var value = station[column_name] ? station[column_name].date : '';
	            } else {
	                var value = station[column_name] ? station[column_name] : '';
	            }
	            tr.append('<td>'+value+'</td>');
	        }
	    }
	},

	show_station_instruction_history: function (station_history) {
	    var section = $('#station_instruction_history', MBC.main_div);
	    var title = $('<div class="section-header">Station Instruction History:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    var table = $('<table id="station_instruction_history_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    
	    var cols = [
	        {data: 'type_desc', title: 'Test Type'},
	        {data: 'start_time', title: 'Start Time', type: 'datetime'},
	        {data: 'complete_time', title: 'End Time', type: 'datetime'},
	        {data: 'completion_details', title: 'Completion Details'},
	    ];

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    for (var i=0; i<station_history.length; i++) {
	        var station = station_history[i];
	        var tb = $('<tbody class="table-section"></tbody>').appendTo(table);
	        tb.append('<tr><th colspan="'+cols.length+'" class="sub-table-title">'+station.station_desc+'</th></tr>');

	        var htr = $('<tr></tr>').appendTo(tb);
	        for (var j=0; j<cols.length; j++) {
	            htr.append('<th>'+cols[j].title+'</th>');
	        }

	        var instruction_history = station_history[i].station_instruction_history;
	        for (var j=0; j<instruction_history.length; j++) {
	            var instruction = instruction_history[j];
	            var tr = $('<tr></tr>').appendTo(tb);
	            for (var k=0; k<cols.length; k++) {
	                var column_name = cols[k].data;
	                if (cols[k].type != undefined && cols[k].type === 'datetime') {
	                    var value = instruction[column_name] ? instruction[column_name].date : '';
	                } else {
	                    var value = instruction[column_name] ? instruction[column_name] : '';
	                }
	                tr.append('<td>'+value+'</td>');
	            }
	        }        
	    }
	},

	load_rework_station_history: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_rework_station_history',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.rework_station_history = data;
	    		cb(MBC.rework_station_history);
	    	}
	    });
	},

	show_rework_staion_instruction_history: function (rework_station_history) {
	    var section = $('#rework_station_instruction_history', MBC.main_div);
	    var title = $('<div class="section-header">Rework Instruction History:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    var table = $('<table id="rework_instruction_history_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    
	    var cols = [
	        {data: 'type_desc', title: 'Test Type'},
	        //{data: 'start_time', title: 'Start Time', type: 'datetime'},
	        {data: 'complete_time', title: 'End Time', type: 'datetime'},
	        {data: 'completion_details', title: 'Completion Details'},
	    ];

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    for (var i=0; i<rework_station_history.length; i++) {
	        var station = rework_station_history[i];
	        var tb = $('<tbody class="table-section"></tbody>').appendTo(table);
	        tb.append('<tr><th colspan="'+cols.length+'" class="sub-table-title">'+station.station_desc+'</th></tr>');

	        var htr = $('<tr></tr>').appendTo(tb);
	        for (var j=0; j<cols.length; j++) {
	            htr.append('<th>'+cols[j].title+'</th>');
	        }

	        var instruction_history = rework_station_history[i].rework_instruction_history;
	        for (var j=0; j<instruction_history.length; j++) {
	            var instruction = instruction_history[j];
	            var tr = $('<tr></tr>').appendTo(tb);
	            for (var k=0; k<cols.length; k++) {
	                var column_name = cols[k].data;
	                if (cols[k].type != undefined && cols[k].type === 'datetime') {
	                    var value = instruction[column_name] ? instruction[column_name].date : '';
	                } else {
	                    var value = instruction[column_name] ? instruction[column_name] : '';
	                }
	                tr.append('<td>'+value+'</td>');
	            }
	        }        
	    }
	},

	load_sub_assembly_history: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_sub_assembly_history',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.sub_assemblies = data;
	    		cb(MBC.sub_assemblies);
	    	}
	    });
	},

	load_batch_assembly_history: function (wip_part_ID, cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_batch_assembly_history',
	        wip_part_ID: wip_part_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.sub_assemblies.push(data);
	    		cb(data);
	    	}
	    });
	},

	load_batch_assemblies: function (sub_assemblies) {
		var container = $('#sub_assemblies', MBC.main_div);
		$.each(sub_assemblies, function (i, sub_assembly) {
			if (sub_assembly.line_ID == 160) {
				$.each(sub_assembly.batch_parts, function (i, batch_assembly) {
					MBC.load_batch_assembly_history(batch_assembly.ID, function (data) {
						MBC.batch_assemblies.push(data);
						MBC.show_sub_assembly_history(data, container);
					});
				});
			}
		});
	},

	show_sub_assemblies: function (sub_assemblies) {
		var container = $('#sub_assemblies', MBC.main_div);
		$.each(sub_assemblies, function (i, sub_assembly) {
			MBC.show_sub_assembly_history(sub_assembly, container);
		});
	},

	show_sub_assembly_history: function (sub_assembly, container) {
	    var section = $('<div></div>').prop({id:'sub_assembly_'+sub_assembly.line_ID}).addClass('section').appendTo(container);
	    var title = $('<div></div>').text('Sub Assembly: '+(sub_assembly.line_code ? sub_assembly.line_code : '')).addClass('section-header').appendTo(section);
	    var content = $('<div></div>').addClass('section-content').appendTo(section);
	    
	    var table = $('<table></table>').prop({id:'sub_assembly_table_'+sub_assembly.line_ID}).addClass('section-table').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var tbody = $('<tbody></tbody>').appendTo(table);

        var tb = $('<tbody></tbody>').addClass('table-section').appendTo(table);
        tb.append('<tr><th class="sub-table-title" colspan="3">' + (sub_assembly.part_desc ? sub_assembly.part_desc : '') + ' (' + sub_assembly.internal_serial + ') - ' + (sub_assembly.built_time ? sub_assembly.built_time.date : '') + '</th></tr>');

        var stations_row = $('<tr><td colspan="2"></td></tr>').appendTo(tb);

        var wip_station_table = $('<table></table>').addClass('sub-table').appendTo(stations_row);
        var wip_station_thead = $(
            '<thead>' +
              '<tr>' +
                '<th>Station</th>' +
                '<th>Start Time</th>' +
                '<th>End Time</th>' +
                '<th>Operator</th>' +
              '</tr>' +
            '</thead>'
        ).appendTo(wip_station_table);
        var wip_station_tbody = $('<tbody></tbody>').appendTo(wip_station_table);

        $.each(sub_assembly.station_history, function (j, station) {
            var station_row = $('<tr></tr>');
            station_row.append('<td>'+station.station_desc+'</td>');
            station_row.append('<td>'+(station.entry_time ? station.entry_time.date : '')+'</td>');
            station_row.append('<td>'+(station.exit_time ? station.exit_time.date : '')+'</td>');
            station_row.append('<td>'+station.operator_name+'</td>');
            wip_station_tbody.append(station_row);

            var instructions_row = $('<tr><td colspan="4"></td></tr>');

            var instruction_table = $('<table class="sub-table"></table>');
            var instruction_thead = $(
                '<thead>' +
                  '<tr>' +
                    '<th>Test Type</th>' +
                    '<th>Start Time</th>' +
                    '<th>End Time</th>' +
                    '<th>Completion Details</th>' +
                  '</tr>' +
                '</thead>'
            );
            var instruction_tbody = $('<tbody></tbody>');

            if (station.instruction_history) {
                $.each(station.instruction_history, function (k, instruction) {
                    var instruction_row = $('<tr></tr>');
                    instruction_row.append('<td>'+instruction.type_desc+'</td>');
                    instruction_row.append('<td>'+(instruction.entry_time ? instruction.entry_time.date : '')+'</td>');
                    instruction_row.append('<td>'+(instruction.exit_time ? instruction.exit_time.date : '')+'</td>');
                    instruction_row.append('<td>'+instruction.completion_details+'</td>');
                    instruction_tbody.append(instruction_row);
                });
            }

            instruction_table.append(instruction_thead)
                             .append(instruction_tbody);

            instructions_row.append(instruction_table);
            wip_station_tbody.append(instructions_row);
        });

		if (sub_assembly.process_data) {
			MBC.show_process_data(sub_assembly, container);
		}

	    if (sub_assembly.line_ID == 160) {
	    	MBC.show_poly_iso_scans(sub_assembly, container);
	    	//MBC.show_batch_station_history(sub_assembly.airbag, container);
	    }
	},

	show_process_data: function (sub_assembly, container) {
	    var section = $('<div></div>').prop({id:'process_data_'+sub_assembly.line_ID}).addClass('section').appendTo(container);
	    var title = $('<div></div>').text('Process Data: '+(sub_assembly.part_desc ? sub_assembly.part_desc : '')+' ('+sub_assembly.internal_serial+')').addClass('section-header').appendTo(section);
	    var content = $('<div></div>').addClass('section-content').appendTo(section);
	    
	    var table = $('<table></table>').prop({id:'process_data_table_'+sub_assembly.line_ID}).addClass('section-table').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var headers = ['Station Instance', 'Name', 'Value', 'Time'];
	    for (var i=0; i<headers.length; i++) {
	        htr.append('<th>'+headers[i]+'</th>');
	    }
	    var tbody = $('<tbody></tbody>').appendTo(table);

	    $.each(sub_assembly.process_data, function (j, data) {
	        var row = $('<tr></tr>');
	        row.append('<td>'+data.instance_desc+'</td>');
	        row.append('<td>'+data.process_desc+'</td>');
	        row.append('<td>'+data.process_value+'</td>');
	        row.append('<td>'+(data.process_timestamp ? data.process_timestamp.date : '')+'</td>');
	        tbody.append(row);
	    });

	    // $('#process_data_table').show(true);
	    // $('#process_data').show(true);
	},

	show_poly_iso_scans: function (sub_assembly, container) {
	    var section = $('<div></div>').prop({id:'poly_iso_scan_'+sub_assembly.line_ID}).addClass('section').appendTo(container);
	    var title = $('<div></div>').text('POLY/ISO: ').addClass('section-header').appendTo(section);
	    var content = $('<div></div>').addClass('section-content').appendTo(section);
	    
	    var table = $('<table></table>').prop({id:'poly_iso_scan_table_'+sub_assembly.line_ID}).addClass('section-table').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var headers = ['POLY', 'ISO'];
	    for (var i=0; i<headers.length; i++) {
	        htr.append('<th>'+headers[i]+'</th>');
	    }
	    var tbody = $('<tbody></tbody>').appendTo(table);

        var row = $('<tr></tr>');
        row.append('<td>'+sub_assembly.poly_scan+'</td>');
        row.append('<td>'+sub_assembly.iso_scan+'</td>');
        tbody.append(row);

	    // $('#process_data_table').show(true);
	    // $('#process_data').show(true);
	},

	show_batch_station_history: function (sub_assembly, container) {
	    //var tbody = $('#station_instruction_history_table tbody');
	    var section = $('<div></div>').prop({id:'batch_station_history'+sub_assembly.line_ID}).addClass('section').appendTo(container);
	    var title = $('<div></div>').text('Batch Assembly: '+(sub_assembly.line_code ? sub_assembly.line_code : '')).addClass('section-header').appendTo(section);
	    var content = $('<div></div>').addClass('section-content').appendTo(section);
	    
	    var table = $('<table></table>').prop({id:'batch_station_history_table_'+sub_assembly.line_ID}).addClass('section-table').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var headers = ['Station', 'Operator'];
	    for (var i=0; i<headers.length; i++) {
	        htr.append('<th>'+headers[i]+'</th>');
	    }
	    //var tbody = $('<tbody></tbody>').appendTo(table);

        var tbody = $('<tbody></tbody>').addClass('table-section').appendTo(table);
        tbody.append('<tr><th class="sub-table-title" colspan="3">' + (sub_assembly.part_desc ? sub_assembly.part_desc : '') + ' (' + sub_assembly.internal_serial + ') - ' + (sub_assembly.built_time ? sub_assembly.built_time.date : '') + '</th></tr>');

	    $.each(sub_assembly.station_history, function (i, station) {
	        if (station.station_desc) {
	            var station_row = $('<tr></tr>');
	            station_row.append('<td>'+station.station_desc+'</td>');
	            //station_row.append('<td>'+(station.entry_time ? station.entry_time.date : '')+'</td>');
	            //station_row.append('<td>'+(station.exit_time ? station.exit_time.date : '')+'</td>');
	            station_row.append('<td>'+station.operator_name+'</td>');
	            tbody.append(station_row);

	            if (station.instruction_history) {
	                var instructions_row = $('<tr><td colspan="4"></td></tr>');

	                var instruction_table = $('<table class="sub-table"></table>');
	                var instruction_thead = $(
	                    '<thead>' +
	                      '<tr>' +
	                        '<th>Test Type</th>' +
	                        '<th>Start Time</th>' +
	                        '<th>End Time</th>' +
	                        '<th>Completion Details</th>' +
	                      '</tr>' +
	                    '</thead>'
	                );
	                var instruction_tbody = $('<tbody></tbody>');
	            
	                $.each(station.instruction_history, function (j, instruction) {
	                    var instruction_row = $('<tr></tr>');
	                    instruction_row.append('<td>'+(instruction.type_desc ? instruction.type_desc : '')+'</td>');
	                    instruction_row.append('<td>'+(instruction.entry_time ? instruction.entry_time.date : '')+'</td>');
	                    instruction_row.append('<td>'+(instruction.exit_time ? instruction.exit_time.date : '')+'</td>');
	                    instruction_row.append('<td>'+instruction.completion_details+'</td>');
	                    instruction_tbody.append(instruction_row);
	                });

	                instruction_table.append(instruction_thead)
	                                 .append(instruction_tbody);

	                instructions_row.append(instruction_table);
	                tbody.append(instructions_row);
	            }
	        }
	    });
	},

	load_eol_test_data: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_eol_test_data',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.eol_test_data = data;
	    		cb(MBC.eol_test_data);
	    	}
	    });
	},

	show_eol_test_data: function (eol_test_data) {
		var section = $('#eol_test_data', MBC.main_div);
	    var title = $('<div class="section-header">EOL Test Detail:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="eol_test_info_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);
	    var headers = ['Result', 'Test Time', 'Details'];
	    for (var i=0; i<headers.length; i++) {
	        htr.append('<th>'+headers[i]+'</th>');
	    }

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    $.each(eol_test_data, function (i, test) {
	        var test_row = $('<tr></tr>');
	        test_row.append($('<td>'+(test.result ? 'PASSED' : 'FAILED')+'</td>'));
	        test_row.append($('<td>'+(test.start_time ? test.start_time.date : '')+'</td>'));
	        tbody.append(test_row);

	        var details_row = $('<tr><td colspan="3"></td></tr>');
	        var detail_table = $('<table class="sub-table"></table>');
	        var detail_thead = $(
	            '<thead>' +
	              '<tr>' +
	                '<th>Test Description</th>' +
	                '<th>Channel Min</th>' +
	                '<th>Channel Max</th>' +
	                '<th>Channel Actual</th>' +
	                '<th>Result</th>' +
	              '</tr>' +
	            '</thead>'
	        );
	        var detail_tbody = $('<tbody></tbody>');

	        if (test.detail) {
	            $.each(test.detail, function (j, detail) {
	                var detail_row = $('<tr></tr>');
	                detail_row.append('<td>'+detail.test_item+'</td>');
	                detail_row.append('<td>'+detail.min_float_val.toFixed(3)+'</td>');
	                detail_row.append('<td>'+detail.max_float_val.toFixed(3)+'</td>');
	                detail_row.append('<td>'+detail.float_val.toFixed(3)+'</td>');
	                detail_row.append('<td>'+(detail.passed == 1 || detail.passed == 'PASSED' ? 'PASSED' : 'FAILED')+'</td>');
	                detail_tbody.append(detail_row);
	            });            
	        } else {
	            var detail_row = $('<tr></tr>');
	            detail_row.append('<td>No Tests Completed</td><td></td><td></td><td></td><td></td>');
	            detail_tbody.append(detail_row);
	        }


	        detail_table.append(detail_thead);
	        detail_table.append(detail_tbody);
	        details_row.append(detail_table);       
	        tbody.append(details_row);
	    });
	},

	load_images: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_images',
	        module_ID: MBC.module_ID,
	        mrp_company_code: MBC.mrp_company_code
	    }).done(function (data) {
	    	if (data) {
	    		MBC.main_line_images = data.main_line_images;
	    		MBC.chute_images = data.chute_images;
	    		cb(data);
	    	}
	    });
	},

	show_main_line_images: function (main_line_images) {
	    var section = $('#main_line_images', MBC.main_div);
	    var title = $('<div class="section-header">Main Line Images:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="main_line_image_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var tbody = $('<tbody></tbody>').appendTo(table);
	    var r = $('<tr></tr>').appendTo(tbody);

	    $.each(main_line_images, function (i, image) {
	        htr.append('<th>Camera '+(i+1)+'</th>');
	        r.append('<td><a href="'+image+'" target="_blank"><img src="'+image+'" width="200px" /></a></td>');
	    });
	},

	show_chute_images: function (chute_images) {
	    var section = $('#chute_images', MBC.main_div);
	    var title = $('<div class="section-header">Chute Images:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="chute_image_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);

	    var tbody = $('<tbody></tbody>').appendTo(table);
	    var r = $('<tr></tr>').appendTo(tbody);

	    $.each(chute_images, function (i, image) {
	        htr.append('<th>Camera '+(i+1)+'</th>');
	        r.append('<td><a href="'+image+'" target="_blank"><img src="'+image+'" width="200px" /></a></td>');
	    });
	},
	
	load_module_station_torque_values: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_module_station_torque_values',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.module_station_torque_values = data;
	    		cb(MBC.module_station_torque_values);
	    	}
	    });
	},

	show_module_station_torque_values: function (module_station_torque_values) {
	    var section = $('#module_station_torque_values', MBC.main_div);
	    var title = $('<div class="section-header">Torque Values:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="module_station_torque_values_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);
	    
	    var cols = [
	        {data: 'station_desc', title: 'Station'},
			{data: 'part_number', title: 'Part'},
			{data: 'part_order', title: 'Part Order'},
			{data: 'instr_order', title: 'Instruction Order'},
			{data: 'torque_time', title: 'Torque Time', type: 'datetime'},
			{data: 'gun_number', title: 'Gun'},
			{data: 'torque_value', title: 'Torque Value'},
			{data: 'angle_value', title: 'Angle Value'},
			{data: 'passed', title: 'Passed', type: 'bool'},
			{data: 'cleared', title: 'Cleared'},
			{data: 'attempt_number', title: 'Attempts'}
	    ];

	    for (var i=0; i<cols.length; i++) {
	        htr.append('<th>'+cols[i].title+'</th>');
	    }

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    for (var i=0; i<module_station_torque_values.length; i++) {
	        var v = module_station_torque_values[i];
	        var tr = $('<tr></tr>').appendTo(tbody);
	        for (var j=0; j<cols.length; j++) {
	            var column_name = cols[j].data;
	            if (cols[j].type != undefined && cols[j].type === 'datetime') {
	                var value = v[column_name] ? v[column_name].date : '';
	            } else if (cols[j].type != undefined && cols[j].type === 'bool') {
					var value = v[column_name] ? 'TRUE' : 'FALSE';
				} else {
	                var value = v[column_name] !== null || v[column_name] !== undefined ? v[column_name] : '';
	            }
	            tr.append('<td>'+value+'</td>');
	        }
	    }
	},

	load_module_stored_plc_values: function (cb) {
	    $.getJSON(MBC.processor, {
	        action: 'get_module_stored_plc_values',
	        module_ID: MBC.module_ID
	    }).done(function (data) {
	    	if (data) {
	    		MBC.module_stored_plc_values = data;
	    		cb(MBC.module_stored_plc_values);
	    	}
	    });
	},

	show_module_stored_plc_values: function (module_stored_plc_values) {
	    var section = $('#module_stored_plc_values', MBC.main_div);
	    var title = $('<div class="section-header">Stored PLC Values:</div>').appendTo(section);
	    var content = $('<div class="section-content"></div>').appendTo(section);
	    
	    var table = $('<table id="module_stored_plc_values_table" class="section-table"></table>').appendTo(content);
	    var thead = $('<thead></thead>').appendTo(table);
	    var htr = $('<tr></tr>').appendTo(thead);
	    
	    var cols = [
	        {data: 'station_desc', title: 'Station'},
			{data: 'the_time', title: ' Time', type: 'datetime'},
			{data: 'tag_desc', title: 'Description'},
			{data: 'test_status', title: 'Test Status'},
			{data: 'attempt_number', title: 'Attempts'},
			{data: 'tag_value', title: 'Tag Value'}
	    ];

	    for (var i=0; i<cols.length; i++) {
	        htr.append('<th>'+cols[i].title+'</th>');
	    }

	    var tbody = $('<tbody></tbody>').appendTo(table);

	    for (var i=0; i<module_stored_plc_values.length; i++) {
	        var v = module_stored_plc_values[i];
	        var tr = $('<tr></tr>').appendTo(tbody);
	        for (var j=0; j<cols.length; j++) {
	            var column_name = cols[j].data;
	            if (cols[j].type != undefined && cols[j].type === 'datetime') {
	                var value = v[column_name] ? v[column_name].date : '';
	            } else {
	                var value = v[column_name] !== null || v[column_name] !== undefined ? v[column_name] : '';
	            }
	            tr.append('<td>'+value+'</td>');
	        }
	    }
	},
	
};

$(document).ready(MBC.init);