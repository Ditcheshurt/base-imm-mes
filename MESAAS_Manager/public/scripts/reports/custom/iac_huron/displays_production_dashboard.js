$(function () {
	show_clock();
	update_display();
	setInterval('show_clock();', 1000);
	setInterval('update_display();', 30000);
});

function show_clock() {
	var time = new Date();
	var mins = time.getMinutes() < 10 ? '0' + time.getMinutes() : time.getMinutes();
	var secs = time.getSeconds() < 10 ? '0' + time.getSeconds() : time.getSeconds();
	$('.display_clock').text(time.getHours() + ':' + mins + ':' + secs);
}

function update_display() {
	var panel = $('div[cell_id]');
	$.each(panel, function (k, v) {
		$.getJSON(
			'../ajax_processors/displays_production_dashboard.php', {
			'action' : 'hourly_production_summary',
			'cell_id' : v.getAttribute('cell_id')
		},
			after_update_display);
	});
}

function after_update_display(jso) {
	if (jso == null) {
		return;
	}

	$('.line_panel').each(function (idx, itm) {

		if ($(itm).attr('cell_id') == jso.description[0].cell_ID) {
			if (jso.description[0].is_active == 0) {
				$(itm).find('.panel').removeClass('panel-success');
				$(itm).find('.panel').addClass('panel-danger');
			} else {
				$(itm).find('.panel').removeClass('panel-danger');
				$(itm).find('.panel').addClass('panel-success');
			}

			var total_built = 0;
			$.each(jso.built, function (k, v) {
				total_built += v.qty_built;
			});

			$(itm).find('.panel-title').text(jso.description[0].cell_desc);
			$(itm).find('.qty_built').text(total_built);
			build_chart(jso, $(itm).find('.production_chart'));
		}

	});

}

function build_chart(jso, ele) {
	var per_hour = [];
	var per_hour_req = [];
	for (var i = 1; i < 25; i++) {
		per_hour.push(0);
	}
	for (var i = 1; i < 25; i++) {
		per_hour_req.push(0);
	}

	if (jso.built !== null) {
		for (var i = 0; i < jso.built.length; i++) {
			per_hour[jso.built[i].hour] = jso.built[i].qty_built;
		}
	}
	if (jso.req !== null) {
		for (var i = 0; i < jso.req.length; i++) {
			per_hour_req[jso.req[i].hour] = jso.req[i].required_qty;
		}
	}

	var data = [{
			name : 'Required',
			color : 'rgba(255,215,0,.4)',
			data : per_hour_req,
			pointPadding : -0.2,
			pointPlacement : -0.2
		}, {
			name : 'Built',
			color : 'rgba(0,0,215,1)',
			data : per_hour,
			pointPadding : 0.1,
			pointPlacement : -0.2
		}
	];

	//hourly production report
	var options = {
		credits : {
			enabled : false
		},
		chart : {
			type : 'column'
		},
		title : {
			text : null
		},
		xAxis : {
			title : {
				text : null
			},
			categories : ['1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am',
				'11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm',
				'11pm', '12pm']
		},
		yAxis : {
			min : 0,
			title : {
				text : null
			}
		},
		series : data,
		subtitle : {
			text : null
		},
		legend : {
			enabled : false
		},
		plotOptions : {
			column : {
				grouping : false,
				shadow : false,
				borderWidth : 0
			}
		},
	}

	ele.highcharts(options);

}
