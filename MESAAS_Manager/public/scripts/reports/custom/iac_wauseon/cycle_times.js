var processor = './ajax_processors/reports/custom/iac_wauseon/cycle_times.php';
var dtCycleTimes;
var dtStationDetail;
var systems;
var chosen_system;
var over_target_cycle_time;
var custom_target_cycle_time;

$(document).ready(function() {	
    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    $('#system_select').on('change', function (e) {
        load_filter_options();
    });

    $('#line_select').on('change', line_changed);

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	// run report button
	$('body').on('click', '.btn-run', loadData);
		
	//show detail on cycleTimesTable click
	$('.table-cycle-report tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = dtCycleTimes.row(tr);
 
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			populateStationDetailRow(row);
			tr.addClass('shown');
		}
	});

	$('#over_target_cycle_time_checkbox').on('change', function () {
		over_target_cycle_time = $(this).prop('checked');
		if (!over_target_cycle_time) {
			$('#custom_target_cycle_time_input').val('');
		}
		$('#custom_target_cycle_time_input').prop('disabled', !over_target_cycle_time);
	});
		
});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];
	
	// populate operators
	load_operators(null, function (operators) {
    	show_operators(operators);
		var d = $('<option />').prop('id', 'operator_default')
							   .prop('name', 'soperator_id')
							   .val('%')
							   .text('- Operator -');
		$('#operator_select').prepend(d);
    });
	
	//populate the group by
	$('#inpGroupBy').empty();	
	$('#inpGroupBy').append($('<option />').val('Date').text('Date'));
	$('#inpGroupBy').append($('<option />').val('Operator').text('Operator'));
	$('#inpGroupBy').append($('<option />').val('Model').text('Model'));
	
	//populate shifts
	$('#inpShift').empty();
	$('#inpShift').append($('<option />').val('%').text('- Shift -'));
	$('#inpShift').append($('<option />').val('1').text('First'));
	$('#inpShift').append($('<option />').val('2').text('Second'));
	$('#inpShift').append($('<option />').val('3').text('Third'));

    if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
		$('#inpGroupBy').prepend($('<option />').val('Line').text('Line'));
        $('#line_select').empty();
		$('#line_select').nysus_select({
		    name: 'line_id',
		    key: 'line_ID',
		    data_url: 'ajax_processors/filter.php',
		    params: {action: 'get_lines', system_id: chosen_system.ID},
		    render: function (line) {return line.line_desc;},
            callback: function(data) {line_changed();}
		}).prop('disabled', false);

        $('#line_select').empty().prop('disabled', false);
        $('#line_model_select').empty().prop('disabled', false);
        $('#machine_select').empty().prop('disabled', true);
    } else if (chosen_system.process_type === 'REPETITIVE') {
        $('#machine_select').empty();
        $('#machine_select').nysus_select({
            name: 'machine_id',
            key: 'ID',
            data_url: 'ajax_processors/filter.php',
            params: {action: 'get_machines', database: chosen_system.database_name},
            render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;}
        }).prop('disabled', false);

        $('#machine_select').empty().prop('disabled', false);
        $('#line_select').empty().prop('disabled', true);
        $('#line_model_select').empty().prop('disabled', true);
		$('#station_select').empty().prop('disabled', true);
		
    }
}

function line_changed() {
	// load_stations({
	// 	system_id: chosen_system.ID,
	// 	line_id: $('#line_select').val()
	// }, function (stations) {
	// 	show_stations(stations);
	// 	var default_op = $('<option />').prop('id', 'station_default')
	// 								    .prop('name', 'station_id')
	// 								    .val('%')
	// 								    .text('- Station -');
	// 	$('#station_select').prepend(default_op);
	// });

    $('#line_model_select').empty();
	$('#line_model_select').nysus_select({
	    name: 'line_model',
	    key: 'model',
	    data_url: 'ajax_processors/filter.php',
	    params: {action: 'get_line_models', line_id: $('#line_select').val()},
	    render: function (line_model) {return line_model.model;},
	    placeholder: '- Model -'
	}).prop('disabled', false);

    $('#station_select').empty();
	$('#station_select').nysus_select({
	    name: 'station_id',
	    key: 'ID',
	    data_url: 'ajax_processors/filter.php',
	    params: {action: 'get_stations', system_id: chosen_system.ID, line_id: $('#line_select').val()},
	    render: function (station) {return station.name;},
	    placeholder: '- Station -'
	}).prop('disabled', false);
}

function loadData() {
	
	// set dates on panels
	var dateTitle = ($('#start_date').val() != $('#end_date').val()) 
		? $('#start_date').val() + ' -> ' + $('#end_date').val() 
		: $('#start_date').val();
	$('.panel .panel-report-date').html(dateTitle);
	//$('.table-cycle-summary .loading').show();
	 
	$.getJSON(processor, {
		'action':'cycle_times',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
		'line_model':$('#line_model_select').val(),
		'operator_id':$('#operator_select').val(),
		'station_id':$('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
        'over_target_cycle_time': over_target_cycle_time ? 1 : 0,
        'custom_target_cycle_time': $('#custom_target_cycle_time_input').val(),
		'machine_ID': $('#machine_select :selected').val()
	}, function(jso) {

		//respond no data
		if (!jso) {
			$('.panel .panel-report-date').html('No Data to Report');
			$('.panel .panel-cycle-chart').empty();
			$('.table-cycle-report').hide();
			return; 
		} else {
			$('.table-cycle-report').show();
			populateCycleTimesReport(jso);
			populateCycleTimesChart(jso);
		}
		//$('.table-cycle-summary .loading').hide();
	});
	
}

function populateCycleTimesChart(jso) {
	// populate the cycle times chart	
	var categories = [];
	var data = {target: [], average: [], full_average: []};

	custom_target_cycle_time = parseInt($('#custom_target_cycle_time_input').val());

	$.each(jso, function(k,v) {
		if ($('#inpGroupBy').val() === 'Operator') {
			categories.push(v.operator_name);
		} else if ($('#inpGroupBy').val() === 'Date') {			
			categories.push(v.id != undefined ? v.id + ': ' + v.entry_date : v.entry_date);			
		// } else if ($('#inpGroupBy').val() === 'Program') {
		// 	var prog = v.program === '2L' ? '540' : '539';
		// 	categories.push(v.id != undefined ? v.id + ': ' + prog : prog);	
		// }
		} else if ($('#inpGroupBy').val() === 'Model') {
			categories.push(v.id != undefined ? v.id + ': ' + v.model : v.model);	
		} else {
			//categories.push(v.id);
			categories.push(v.station);
		}

		data.average.push(v.avg_cycle_time);
		data.full_average.push(Math.round(v.avg_full_cycle_time));

		if (custom_target_cycle_time) {
			data.target.push(custom_target_cycle_time);
		} else {
			data.target.push(v.target_cycle_time);
		}
		
	});
		
	var series = [
		{
			 name: 'Target Cycle Time',
			 data: data.target,
			 color: 'rgba(0, 0, 0, 0.7)'
		},
		{
			 name: 'Average Cycle Time',
			 data: data.average,
			 color : 'rgba(0, 100, 200, 0.7)'
		},
		{
			 name: 'Average Full Cycle Time',
			 data: data.full_average,
			 color : 'rgba(200, 100, 0, 0.7)'
		}
	];

	// populate the chart
	$('.panel-cycle-chart').highcharts({
        chart: {
            type: 'column',
            height: 600
        },
		tooltip: {
            formatter: function () {
				var label = '';
				
				if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
					label = '<b>Station: ' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>';
				} else {
					label = this.series.name + ': ' + this.y + '<br/>';
				}
                return label;
            }
        },
        title: {
            text: 'Cycle Times'
        },
        xAxis: {
            categories: categories,
            labels: {
            	rotation: -40
            }
        },
        yAxis: {
	        title: {
	            text: 'Cycle Time'
	        }
	    },
        series: series,
  //       series: [{
		// 	name: 'Avg Cycle',
		// 	data: data
		// }],
		credits: false,
		plotOptions: {
            column: {
                dataLabels: {
                    enabled: false
                }
            }
        }
	});

}

function populateCycleTimesReport(jso) {
	if (!jso) {return;}

	if (dtCycleTimes != null) { 
		dtCycleTimes.destroy();	//so the thing will update
	}

	custom_target_cycle_time = parseInt($('#custom_target_cycle_time_input').val());
	
	var cols = [];
	cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	cols.push({data: 'operator_name', "title":"Operator", "defaultContent":""});
	cols.push({data: 'entry_date', "title":"Date", "defaultContent":""});
	if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
		cols.push({data: 'station', "title":"Station"});
	} else if (chosen_system.process_type == 'REPETITIVE') {
		cols.push({data: 'tool_description', "title":"Tool"});
	}
	// cols.push({
	// 	data: 'program',
	// 	title: 'Program',
	// 	render: function (data, type, full, meta) {
	// 		if (data == '2L') {
	// 			return '540';
	// 		} else if (data == '2F') {
	// 			return '539';
	// 		} else {
	// 			return ''
	// 		}
	// 	}
	// });
	cols.push({
		data: 'model',
		title: 'Model',
		defaultContent: ''
	});
	cols.push({
		data: 'target_cycle_time',
		title:'<span title="The target cycle time for the station.">Target Cycle</span>',
			render: function (data, type, full, meta) {
			if (custom_target_cycle_time) {
				return custom_target_cycle_time;
			} else {
				return data;
			}
		}
	});
	cols.push({
		data: 'avg_cycle_time',
		title:'<span title="The station exit time minus the entry time.">Avg Cycle</span>'
	});
	cols.push({
		data: 'over_cycle_time',
		title:'<span title="The cycle time minus the target cycle time.">Avg Over Cycle</span>',
		render: function (data, type, full, meta) {
			if (custom_target_cycle_time) {
				return full.avg_cycle_time - custom_target_cycle_time;
			} else {
				return data;
			}
		}
	});
	cols.push({
		data: 'avg_full_cycle_time',
		title:'<span title="The average cycle time plus the transit time.">Avg Full Cycle</span>',
		render: function (data, type, full, meta) {
			return full.avg_full_cycle_time !== null ? Math.round(full.avg_full_cycle_time) : null;
		}
	});
	cols.push({
		data: 'full_over_cycle_time',
		title:'<span title="The full cycle time minus the target cycle time.">Avg Full Over Cycle</span>',
		render: function (data, type, full, meta) {
			if (custom_target_cycle_time) {
				return full.avg_full_cycle_time  !== null ? Math.round(full.avg_full_cycle_time - custom_target_cycle_time) : null;
			} else {
				return data === null ? null : Math.round(data);
			}
		}
	});
	cols.push({
		data: 'num_cycles',
		title:'<span title="The total number of cycles during the chosen time period.">Total Cycles</span>',
	});
	
	dtCycleTimes = $('.table-cycle-report').DataTable({
		"bInfo":false,
		"bAutoWidth":true,
		autoWidth:true,
		retrieve: true,		
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		},                        
		data: jso,        
		order: [[3, 'asc']],
		columns: cols
	});
	
	dtCycleTimes.column(1).visible(false);
	dtCycleTimes.column(2).visible(false);
	dtCycleTimes.column(4).visible(false);
	// show hide cols based on groupBy
	if ($('#inpGroupBy').val() === "Operator") {
		dtCycleTimes.column(1).visible(true);
	} else if ($('#inpGroupBy').val() === "Date") {
		dtCycleTimes.column(2).visible(true);
	} else if ($('#inpGroupBy').val() === "Model") {
		dtCycleTimes.column(4).visible(true);
	}

	$(dtCycleTimes).find('th[title]').tooltip({"delay":0, "track":true, "fade":250});
}

/* Formatting function for row details */
function populateStationDetailRow(r) {	
	childContent = '<div class="col-sm-12 well">'+
				'<div class="col-sm-8"><table class="station-detail table table-striped table-condensed table-bordered"></table></div>'+
				'<div class="col-sm-4"><div class="station-cycle-chart-'+r.data().station_id+'"></div></div>'+
				'</div>';
	r.child($(childContent)).show();

	//console.log(r.data());
	
	//build Operator table in left table
	populateDetailTable(r);
	//build line graph in right table
	if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
		populateDetailChart(r);
	}
	
}

function populateDetailTable(r) {
	var line_model = r.data().model;
	if (!line_model) {
		if ($('#line_model_select').val() != -1) {
			line_model = $('#line_model_select').val();
		} else {
			line_model = '%';
		}
	}

	var params = {
		'action':'cycle_times_detail',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':r.data().line_id == null ? "%" : r.data().line_id,
		'station_id':r.data().id == null ? "%" : r.data().id,
		//'line_model':$('#line_model_select').val(),
		'line_model':line_model,
		'operator_id':r.data().operator_id == null ? "%" : r.data().operator_id,
		'program':r.data().program == null ? "%" : r.data().program,
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		'machine_ID': $('#machine_select :selected').val(),
        'over_target_cycle_time': over_target_cycle_time ? 1 : 0,
        'custom_target_cycle_time': $('#custom_target_cycle_time_input').val()
	};

	$.getJSON(processor, params, function(jso) {
		if (!jso) {return;}
		if (dtStationDetail != null) {
			dtStationDetail.destroy();
		}

		var cols = [];		
		if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
			cols.push({data: 'station', "title":"Station"});
		} else if (chosen_system.process_type == 'REPETITIVE') {
			cols.push({data: 'tool_description', "title":"Tool"});
		}



		cols.push({data: 'name', "title":"Operator"});
		cols.push({data: 'entry_time.date', "title":"Entry Time"});
		cols.push({data: 'exit_time.date', "title":"Complete Time"});

		cols.push({
			data: 'cycle_time',
			title:'<span title="The station exit time minus the entry time.">Cycle</span>'
		});

		cols.push({
			data: 'over_cycle_time',
			title:'<span title="The cycle time minus the target cycle time.">Over Cycle</span>',
			render: function (data, type, full, meta) {
				if (custom_target_cycle_time) {
					return full.cycle_time - custom_target_cycle_time;
				} else {
					return data;
				}
			}
		});

		cols.push({
			data: 'full_cycle_time',
			title:'<span title="The cycle time plus the station transit time.">Full Cycle</span>',
			render: function (data, type, full, meta) {
				return full.full_cycle_time  !== null ? Math.round(full.full_cycle_time) : null;
			}
		});

		cols.push({
			data: 'full_over_cycle_time',
			title:'<span title="The full cycle time minus the target cycle time.">Full Over Cycle</span>',
			render: function (data, type, full, meta) {
				if (custom_target_cycle_time) {
					return full.full_cycle_time  !== null ? Math.round(full.full_cycle_time - custom_target_cycle_time) : null;
				} else {
					return data === null ? null : Math.round(data);
				}
			}
		});
		
		dtStationDetail = $('.station-detail').DataTable({
			"sScrollY": "100%",
			"bScrollCollapse": true,
			"bAutoWidth":true,
			autoWidth:true,
			retrieve: true,
			
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},                        
			data: jso,        
			order: [[2, 'desc']], 
			columns: cols
		});
	});
	
}

function populateDetailChart(r) {
	console.log(r.data());

	var line_model = r.data().model;
	if (!line_model) {
		if ($('#line_model_select').val() != -1) {
			line_model = $('#line_model_select').val();
		} else {
			line_model = '%';
		}
	}

	$.getJSON(processor, {
		'action':'station_operator_cycle_times',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
		'line_model':line_model,
		'station_id':r.data().id == null ? "%" : r.data().id,
		'shift':$('#inpShift').val(),
		'program':r.data().program == null ? "%" : r.data().program,
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		'machine_ID': $('#machine_select :selected').val(),
        'over_target_cycle_time': over_target_cycle_time ? 1 : 0,
        'custom_target_cycle_time': $('#custom_target_cycle_time_input').val(),
        'operator_id': $('#operator_select :selected').val()
	}, function(jso) {
		if (!jso) {return;}

		custom_target_cycle_time = parseInt($('#custom_target_cycle_time_input').val());

		// populate the cycle times chart	
		var categories = [];
		$.each(jso, function(k,v) {
			categories.push(v.name);
		});
		var data = {cycle: [], full_cycle: []};
		$.each(jso, function(k,v) {
			data.cycle.push(v.avg_cycle_time);
			data.full_cycle.push(Math.round(v.avg_full_cycle_time));
		});	

	    if (custom_target_cycle_time) {
	    	target = custom_target_cycle_time;
	    } else {
	    	target = jso[0].target_cycle_time;
	    }
		
		// populate the chart
		$('.station-cycle-chart-'+r.data().station_id).highcharts({
			chart: {
				type: 'bar'
			},
			title: {
				text: 'Operator Cycle Times'
			},
			xAxis: {
				categories: categories
			},
			yAxis: {
		        title: {
		            text: 'Cycle Time'
		        },
		        plotLines: [{
					color: 'rgba(0, 0, 0, 0.7)',
					value: target,
					width: 2   
				}],
				min: 0,
				minRange: target + 0.03*target
		    },
			series: [{
				name: 'Avg Cycle Time',
				data: data.cycle,
				color : 'rgba(0, 100, 200, 0.7)'
			},
			{
				name: 'Avg Full Cycle Time',
				data: data.full_cycle,
				color : 'rgba(200, 100, 0, 0.7)'
			}],
			credits: false		
		});

		//$('.table-cycle-chart .loading').hide();
	});

}

