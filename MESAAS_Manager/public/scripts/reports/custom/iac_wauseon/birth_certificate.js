var processor = './ajax_processors/report_birth_certificate.php';

var module_ID;

$(document).ready(function () {
    module_ID = $('#info_div').data('module-id');
    mrp_company_code = $('#info_div').data('mrp-company-code');
    load_info(module_ID, show_info);
});

function load_info (module_ID, callback) {
    $.getJSON(processor, {
        action: 'load_data',
        module_ID: module_ID,
        mrp_company_code: mrp_company_code
    }, callback);
}

function show_info (data) {
    show_module_info(data.module_info[0]);
    show_component_info(data.component_info);
    show_station_history_info(data.station_history_info);
    show_station_instruction_history_info(data.station_history_info);

    if (data.rework_info) {show_rework_info(data.rework_info);}
    if (data.rework_station_history) {show_rework_instruction_history(data.rework_station_history);}
    if (data.sub_assembly_info) {show_sub_assembly_info(data.sub_assembly_info);}
    if (data.eol_test_info) {show_eol_test_info(data.eol_test_info);}
    if (data.main_line_images) {show_main_line_images(data.main_line_images);}
    if (data.chute_images) {show_chute_images(data.chute_images);}
}

function show_module_info (module) {
    var section = $('<div id="module_overview" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Module Overview:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="module_overview_table" class="section-table"></table>').appendTo(content);

    var tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Broadcast Time:</label> <span class="broadcast-time"></span></td>');
    tr.append('<td><label>Built Time:</label> <span class="built-time"></span></td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>VIN:</label> <span class="vin"></span></td>');
    tr.append('<td><label>Sequence:</label> <span class="sequence"></span></td>');

    if (module.reject_status) {
        tr = $('<tr></tr>').appendTo(table);
        tr.append('<td><label>Reject Status:</label> <span class="reject-status"></span></td>');
    }

    $('.vin').html(module.VIN);
    $('.sequence').html(module.sequence);
    $('.broadcast-time').html(module.recvd_time.date);
    $('.built-time').html(module.built_time ? module.built_time.date : '');
    $('.reject-status').html(module.reject_status);
}

function show_rework_info (data) {
    var section = $('<div id="rework_info" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Rework Data:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="rework_info_table" class="section-table"></table>').appendTo(content);

    var tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Defect Station:</label> '+data.defect_station_desc+'</td>');
    tr.append('<td><label>Defect Operator:</label> '+data.defect_operator+'</td>');
    tr.append('<td><label>Defect Time:</label> '+data.defect_time.date+'</td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Status:</label> '+data.reject_status+'</td>');
    tr.append('<td><label>Rework Operator:</label> '+data.rework_operator+'</td>');
    tr.append('<td><label>Rework Time:</label> '+(data.rework_time ? data.rework_time.date : '')+'</td>');

    tr = $('<tr></tr>').appendTo(table);
    //var topper_count = data.toppers ? data.toppers.length : 0;
    tr.append('<td><label>New Topper:</label> '+(data.new_topper ? 'Yes' : 'No')+'</td>');
}

function show_component_info (components) {
    var section = $('<div id="components" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Components:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="component_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);
    
    var cols = [
        {data: 'part_number', title: 'Part Number'},
        {data: 'qty', title: 'Quantity'}
    ];

    for (var i=0; i<2; i++) {
        for (var j=0; j<cols.length; j++) {
            htr.append('<th>'+cols[j].title+'</th>');
        }
    }

    var tbody = $('<tbody></tbody>').appendTo(table);
    var tr;

    for (var i=0; i<components.length; i++) {
        var c = components[i];

        // create a new row if we're on an even index
        if (i % 2 == 0) {
            tr = $('<tr></tr>').appendTo(tbody);
        }

        for (var j=0; j<cols.length; j++) {
            var column_name = cols[j].data;
            if (cols[j].type != undefined && cols[j].type === 'datetime') {
                var value = c[column_name].date;
            } else {
                var value = c[column_name];
            }
            tr.append('<td>'+value+'</td>');
        }
    }
}

function show_station_history_info (station_history) {
    var section = $('<div id="station_history" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Station History:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="station_history_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);
    
    var cols = [
        {data: 'station_desc', title: 'Station'},
        {data: 'operator_name', title: 'Operator'},
        {data: 'entry_time', title: 'Start Time', type: 'datetime'},
        {data: 'exit_time', title: 'End Time', type: 'datetime'},
        {data: 'cycle_time', title: 'Cycle Time'},
    ];

    for (var i=0; i<cols.length; i++) {
        htr.append('<th>'+cols[i].title+'</th>');
    }

    var tbody = $('<tbody></tbody>').appendTo(table);

    for (var i=0; i<station_history.length; i++) {
        var station = station_history[i];
        var tr = $('<tr></tr>').appendTo(tbody);
        for (var j=0; j<cols.length; j++) {
            var column_name = cols[j].data;
            if (cols[j].type != undefined && cols[j].type === 'datetime') {
                var value = station[column_name] ? station[column_name].date : '';
            } else {
                var value = station[column_name] ? station[column_name] : '';
            }
            tr.append('<td>'+value+'</td>');
        }
    }
}

function show_station_instruction_history_info (station_history) {
    var section = $('<div id="station_instruction_history" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Station Instruction History:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    var table = $('<table id="station_instruction_history_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    
    var cols = [
        {data: 'type_desc', title: 'Test Type'},
        {data: 'start_time', title: 'Start Time', type: 'datetime'},
        {data: 'complete_time', title: 'End Time', type: 'datetime'},
        {data: 'completion_details', title: 'Completion Details'},
    ];

    var tbody = $('<tbody></tbody>').appendTo(table);

    for (var i=0; i<station_history.length; i++) {
        var station = station_history[i];
        var tb = $('<tbody class="table-section"></tbody>').appendTo(table);
        tb.append('<tr><th colspan="'+cols.length+'" class="sub-table-title">'+station.station_desc+'</th></tr>');

        var htr = $('<tr></tr>').appendTo(tb);
        for (var j=0; j<cols.length; j++) {
            htr.append('<th>'+cols[j].title+'</th>');
        }

        var instruction_history = station_history[i].station_instruction_history;
        for (var j=0; j<instruction_history.length; j++) {
            var instruction = instruction_history[j];
            var tr = $('<tr></tr>').appendTo(tb);
            for (var k=0; k<cols.length; k++) {
                var column_name = cols[k].data;
                if (cols[k].type != undefined && cols[k].type === 'datetime') {
                    var value = instruction[column_name] ? instruction[column_name].date : '';
                } else {
                    var value = instruction[column_name] ? instruction[column_name] : '';
                }
                tr.append('<td>'+value+'</td>');
            }
        }        
    }
}

function show_rework_instruction_history (rework_station_history) {
    var section = $('<div id="rework_instruction_history" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Rework Instruction History:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    var table = $('<table id="rework_instruction_history_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    
    var cols = [
        {data: 'type_desc', title: 'Test Type'},
        //{data: 'start_time', title: 'Start Time', type: 'datetime'},
        {data: 'complete_time', title: 'End Time', type: 'datetime'},
        {data: 'completion_details', title: 'Completion Details'},
    ];

    var tbody = $('<tbody></tbody>').appendTo(table);

    for (var i=0; i<rework_station_history.length; i++) {
        var station = rework_station_history[i];
        var tb = $('<tbody class="table-section"></tbody>').appendTo(table);
        tb.append('<tr><th colspan="'+cols.length+'" class="sub-table-title">'+station.station_desc+'</th></tr>');

        var htr = $('<tr></tr>').appendTo(tb);
        for (var j=0; j<cols.length; j++) {
            htr.append('<th>'+cols[j].title+'</th>');
        }

        var instruction_history = rework_station_history[i].rework_instruction_history;
        for (var j=0; j<instruction_history.length; j++) {
            var instruction = instruction_history[j];
            var tr = $('<tr></tr>').appendTo(tb);
            for (var k=0; k<cols.length; k++) {
                var column_name = cols[k].data;
                if (cols[k].type != undefined && cols[k].type === 'datetime') {
                    var value = instruction[column_name] ? instruction[column_name].date : '';
                } else {
                    var value = instruction[column_name] ? instruction[column_name] : '';
                }
                tr.append('<td>'+value+'</td>');
            }
        }        
    }
}

function show_sub_assembly_info (sub_assembly) {
    var section = $('<div id="sub_assemblies" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Sub Assemblies:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="sub_assembly_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);

    var tbody = $('<tbody></tbody>').appendTo(table);

    $.each(sub_assembly, function (i, part) {
        var tb = $('<tbody class="table-section"></tbody>').appendTo(table);
        tb.append('<tr><th class="sub-table-title" colspan="3">' + part.internal_serial + ' - ' + (part.built_time ? part.built_time.date : '') + '</th></tr>');

        var stations_row = $('<tr><td colspan="2"></td></tr>').appendTo(tb);

        var wip_station_table = $('<table class="sub-table"></table>').appendTo(stations_row);
        var wip_station_thead = $(
            '<thead>' +
              '<tr>' +
                '<th>Station</th>' +
                '<th>Start Time</th>' +
                '<th>End Time</th>' +
                '<th>Operator</th>' +
              '</tr>' +
            '</thead>'
        ).appendTo(wip_station_table);
        var wip_station_tbody = $('<tbody></tbody>').appendTo(wip_station_table);

        $.each(part.station_history, function (j, station) {
            var station_row = $('<tr></tr>');
            station_row.append('<td>'+station.station_desc+'</td>');
            station_row.append('<td>'+station.entry_time.date+'</td>');
            station_row.append('<td>'+station.exit_time.date+'</td>');
            station_row.append('<td>'+station.operator_name+'</td>');
            wip_station_tbody.append(station_row);

            var instructions_row = $('<tr><td colspan="4"></td></tr>');

            var instruction_table = $('<table class="sub-table"></table>');
            var instruction_thead = $(
                '<thead>' +
                  '<tr>' +
                    '<th>Test Type</th>' +
                    '<th>Start Time</th>' +
                    '<th>End Time</th>' +
                    '<th>Completion Details</th>' +
                  '</tr>' +
                '</thead>'
            );
            var instruction_tbody = $('<tbody></tbody>');

            if (station.instruction_history) {
                $.each(station.instruction_history, function (k, instruction) {
                    var instruction_row = $('<tr></tr>');
                    instruction_row.append('<td>'+instruction.type_desc+'</td>');
                    instruction_row.append('<td>'+instruction.entry_time.date+'</td>');
                    instruction_row.append('<td>'+instruction.exit_time.date+'</td>');
                    instruction_row.append('<td>'+instruction.completion_details+'</td>');
                    instruction_tbody.append(instruction_row);
                });
            }

            instruction_table.append(instruction_thead)
                             .append(instruction_tbody);

            instructions_row.append(instruction_table);
            wip_station_tbody.append(instructions_row);
        });
        
    });
}

function show_eol_test_info (eol_test_info) {
    var section = $('<div id="eol_test_info" class="section eol_test_info"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">EOL Test Detail:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="eol_test_info_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);
    var headers = ['Result', 'Test Time', 'Details'];
    for (var i=0; i<headers.length; i++) {
        htr.append('<th>'+headers[i]+'</th>');
    }

    var tbody = $('<tbody></tbody>').appendTo(table);

    $.each(eol_test_info, function (i, test) {
        var test_row = $('<tr></tr>');
        test_row.append($('<td>'+(test.result ? 'PASSED' : 'FAILED')+'</td>'));
        test_row.append($('<td>'+(test.start_time ? test.start_time.date : '')+'</td>'));
        //test_row.append($('<td>'+(test.end_time ? test.end_time.date : '')+'</td>'));        
        
        tbody.append(test_row);

        var details_row = $('<tr><td colspan="3"></td></tr>');
        var detail_table = $('<table class="sub-table"></table>');
        var detail_thead = $(
            '<thead>' +
              '<tr>' +
                //'<th>Test Channel</th>' +
                '<th>Test Description</th>' +
                '<th>Channel Min</th>' +
                '<th>Channel Max</th>' +
                '<th>Channel Actual</th>' +
                '<th>Result</th>' +
              '</tr>' +
            '</thead>'
        );
        var detail_tbody = $('<tbody></tbody>');

        $.each(test.detail, function (j, detail) {
            var detail_row = $('<tr></tr>');
            //detail_row.append('<td>'+detail.channel_num+'</td>');
            // detail_row.append('<td>'+detail.test_description+'</td>');
            // detail_row.append('<td>'+detail.resistance_min.toFixed(3)+'</td>');
            // detail_row.append('<td>'+detail.resistance_max.toFixed(3)+'</td>');
            // detail_row.append('<td>'+detail.m_resistance.toFixed(3)+'</td>');
            // detail_row.append('<td>'+detail.test_result+'</td>');
            detail_row.append('<td>'+detail.test_item+'</td>');
            detail_row.append('<td>'+detail.min_float_val.toFixed(3)+'</td>');
            detail_row.append('<td>'+detail.max_float_val.toFixed(3)+'</td>');
            detail_row.append('<td>'+detail.float_val.toFixed(3)+'</td>');
            detail_row.append('<td>'+(detail.passed == 1 || detail.passed == 'PASSED' ? 'PASSED' : 'FAILED')+'</td>');
            detail_tbody.append(detail_row);
        });

        detail_table.append(detail_thead);
        detail_table.append(detail_tbody);
        details_row.append(detail_table);       
        tbody.append(details_row);
    });
}

function show_main_line_images (main_line_images) {
    var section = $('<div id="main_line_images" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Main Line Images:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="main_line_image_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);

    var tbody = $('<tbody></tbody>').appendTo(table);
    var r = $('<tr></tr>').appendTo(tbody);

    $.each(main_line_images, function (i, image) {
        htr.append('<th>Camera '+(i+1)+'</th>');
        r.append('<td><a href="'+image+'" target="_blank"><img src="'+image+'" width="200px" /></a></td>');
    });
}

function show_chute_images (chute_images) {
    var section = $('<div id="chute_images" class="section"></div>').appendTo($('#info_div'));
    var title = $('<div class="section-header">Chute Images:</div>').appendTo(section);
    var content = $('<div class="section-content"></div>').appendTo(section);
    
    var table = $('<table id="chute_image_table" class="section-table"></table>').appendTo(content);
    var thead = $('<thead></thead>').appendTo(table);
    var htr = $('<tr></tr>').appendTo(thead);

    var tbody = $('<tbody></tbody>').appendTo(table);
    var r = $('<tr></tr>').appendTo(tbody);

    $.each(chute_images, function (i, image) {
        htr.append('<th>Camera '+(i+1)+'</th>');
        r.append('<td><a href="'+image+'" target="_blank"><img src="'+image+'" width="200px" /></a></td>');
    });
}
