var processor = './ajax_processors/reports/custom/iac_wauseon/downtime.php';
var filter_processor = 'ajax_processors/filter.php';
var dt_downtimes;
var dtStationDetail;
var systems;
var chosen_system;
var over_target_cycle_time;
//var custom_target_cycle_time;

$(document).ready(function() {	
    load_systems(null, function (data) {
        systems = data;
        show_systems(data);

		// hard-code for main line
		$('#system_select').val(4);

        load_filter_options();
    });

    $('#system_select').on('change', function (e) {
        load_filter_options();
    });

    $('#line_select').on('change', line_changed);

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	// run report button
	$('body').on('click', '.btn-run', loadData);
		
	//show detail on cycleTimesTable click
	$('.table-report tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = dt_downtimes.row(tr);
 
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			populate_station_detail_row(row);
			tr.addClass('shown');
		}
	});


});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];
	
	// populate operators
	load_operators(null, function (operators) {
    	show_operators(operators);
		var d = $('<option />').prop('id', 'operator_default')
							   .prop('name', 'operator_id')
							   .val('%')
							   .text('- Operator -');
		$('#operator_select').prepend(d);
    });
	
	//populate the group by
	$('#inpGroupBy').empty();	
	$('#inpGroupBy').append($('<option />').val('Date').text('Date'));
	$('#inpGroupBy').append($('<option />').val('Operator').text('Operator'));
	//$('#inpGroupBy').append($('<option />').val('Program').text('Program'));
	
	//populate shifts
	$('#inpShift').empty();
	$('#inpShift').append($('<option />').val('%').text('- Shift -'));
	$('#inpShift').append($('<option />').val('1').text('First'));
	$('#inpShift').append($('<option />').val('2').text('Second'));
	$('#inpShift').append($('<option />').val('3').text('Third'));

    if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
		$('#inpGroupBy').prepend($('<option />').val('Line').text('Line'));
        $('#line_select').empty();
		$('#line_select').nysus_select({
		    name: 'line_id',
		    key: 'line_ID',
		    data_url: filter_processor,
		    params: {action: 'get_lines', system_id: chosen_system.ID},
		    render: function (line) {return line.line_desc;},
            callback: function(data) {line_changed();}
		}).prop('disabled', false);

        $('#line_select').empty().prop('disabled', false);
        $('#machine_select').empty().prop('disabled', true);
    } else if (chosen_system.process_type === 'REPETITIVE') {
        $('#machine_select').empty();
        $('#machine_select').nysus_select({
            name: 'machine_id',
            key: 'ID',
            data_url: filter_processor,
            params: {action: 'get_machines', database: chosen_system.database_name},
            render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;}
        }).prop('disabled', false);

        $('#machine_select').empty().prop('disabled', false);
        $('#line_select').empty().prop('disabled', true);
		$('#station_select').empty().prop('disabled', true);
		
    }
}

function line_changed() {
	// load_stations({
	// 	system_ID: chosen_system.ID,
	// 	line_ID: $('#line_select').val()
	// }, function (stations) {
	// 	show_stations(stations);
	// 	var default_op = $('<option />').prop('id', 'station_default')
	// 								    .prop('name', 'station_ID')
	// 								    .val('%')
	// 								    .text('- Station -');
	// 	$('#station_select').prepend(default_op);
	// });

    $('#station_select').empty();
	$('#station_select').nysus_select({
	    name: 'station_id',
	    key: 'ID',
	    data_url: filter_processor,
	    params: {action: 'get_stations', system_id: chosen_system.ID, line_id: $('#line_select').val()},
	    render: function (station) {return station.name;},
	    placeholder: '- Station -'
	}).prop('disabled', false);
}

function loadData() {
	


	// set dates on panels
	var dateTitle = ($('#start_date').val() != $('#end_date').val()) 
		? $('#start_date').val() + ' -> ' + $('#end_date').val() 
		: $('#start_date').val();
	$('.panel .panel-report-date').html(dateTitle);
	//$('.table-cycle-summary .loading').show();
	 
	$.getJSON(processor, {
		'action':'get_downtimes',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val() == '-1' ? '%' : $('#line_select').val(),
		'operator_id':$('#operator_select').val() == '-1' ? '%' : $('#operator_select').val(),
		'station_id':$('#station_select').val() == '-1' ? '%' : $('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	}, function(jso) {

		//respond no data
		if (!jso) {
			$('.panel .panel-report-date').html('No Data to Report');
			$('.downtime-chart').empty();
			$('.table-report').hide();
			return; 
		} else {
			$('.table-report').show();
			populate_chart(jso);
			populate_report(jso);
			//populate_report(jso);
		}
		//$('.table-cycle-summary .loading').hide();
	});

	$.getJSON(processor, {
		'action':'get_top_downtimes',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val() == '-1' ? '%' : $('#line_select').val(),
		'operator_id':$('#operator_select').val() == '-1' ? '%' : $('#operator_select').val(),
		'station_id':$('#station_select').val() == '-1' ? '%' : $('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	}, function(jso) {

		//respond no data
		if (!jso) {
			$('.panel .panel-report-date').html('No Data to Report');
			$('.downtime-pie').empty();
			return; 
		} else {
			populate_top_downtime(jso);
			//populate_report(jso);
		}
		//$('.table-cycle-summary .loading').hide();
	});
	
}

function populate_chart (jso) {
	// populate the cycle times chart	
	var categories = [];
	var data = {target: [], average: [], total: []};

	$.each(jso, function(k,v) {
		if ($('#inpGroupBy').val() === 'Operator') {
			categories.push(v.operator_name);
		} else if ($('#inpGroupBy').val() === 'Date') {			
			categories.push(v.id != undefined ? v.id + ': ' + v.entry_date : v.entry_date);			
		} else if ($('#inpGroupBy').val() === 'Program') {
			var prog = v.program === '2L' ? '540' : '539';
			categories.push(v.id != undefined ? v.id + ': ' + prog : prog);	
		} else {
			//categories.push(v.id);
			categories.push(v.station);
		}

		data.average.push(v.average_downtime / 60);
		data.total.push(v.total_downtime / 60);
		
	});
		
	var series = [
		{
			 name: 'Total Downtime (Minutes)',
			 data: data.total,
			 color : 'rgba(0, 100, 200, 0.7)'
		}
	];

	console.log(series);

	// populate the chart
	$('.downtime-chart').highcharts({
        chart: {
            type: 'column',
            height: 600
        },
		tooltip: {
            formatter: function () {
				var label = '';
				
				if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
					label = '<b>Station: ' + this.x + '</b><br/>' + this.series.name + ': ' + this.y.toFixed(2) + '<br/>';
				} else {
					label = this.series.name + ': ' + this.y.toFixed(2) + '<br/>';
				}
				console.log(label);
                return label;
            }
        },
        title: {
            text: 'Downtime'
        },
        xAxis: {
            categories: categories,
            labels: {
            	rotation: -40
            }
        },
        yAxis: {
	        title: {
	            text: 'Downtime'
	        }
	    },
        series: series,
  //       series: [{
		// 	name: 'Avg Cycle',
		// 	data: data
		// }],
		credits: false,
		plotOptions: {
            column: {
                dataLabels: {
                    enabled: false
                }
            }
        }
	});

}


function populate_top_downtime(jso) {
	console.log('populate pie chart');
	var i = 0;
	data = [];

	$.each(jso, function(k,v) {
		if ($('#inpGroupBy').val() === 'Operator') {
			name = v.operator_name;
		} else if ($('#inpGroupBy').val() === 'Date') {
			categories.push(v.id != undefined ? v.id + ': ' + v.entry_date : v.entry_date);			
		} else if ($('#inpGroupBy').val() === 'Program') {
			var prog = v.program === '2L' ? '540' : '539';
			name = v.id != undefined ? v.id + ': ' + prog : prog;
		} else {
			name = v.station;
		}

		data.push({ name: name, y: v.total_downtime / 60, color: Highcharts.getOptions().colors[i] });
		i++;
	});
	
	var s = [
		{
			 name: 'Total Downtime (Minutes)',
			 data: data,
			 color : 'rgba(0, 100, 200, 0.7)'
		}
	];
	console.log(s);

	$('.downtime-pie').highcharts({
		chart: {
			type: 'pie'
		},
		title: {
			text: 'Top 5'
		},
		series: s,
		tooltip: {
            formatter: function () {
				var label = '';
				
				if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
					label = '<b>Station: ' + this.point.name + '</b><br/>' + this.series.name + ': ' + this.y.toFixed(2) + '<br/>';
				} else {
					label = this.series.name + ': ' + this.y.toFixed(2) + '<br/>';
				}
                return label;
            }
        },
		//center: [50, 10],
		//size: 200,
		plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },					
		credits: false
	});
}


function populate_report(jso) {
	if (!jso) {return;}

	if (dt_downtimes != null) { 
		dt_downtimes.destroy();	//so the thing will update
	}

	//custom_target_cycle_time = parseInt($('#custom_target_cycle_time_input').val());
	
	var cols = [];
	cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	cols.push({data: 'operator_name', "title":"Operator", "defaultContent":""});
	cols.push({data: 'entry_date', "title":"Date", "defaultContent":""});
	if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
		cols.push({data: 'station', "title":"Station"});
	} else if (chosen_system.process_type == 'REPETITIVE') {
		cols.push({data: 'tool_description', "title":"Tool"});
	}
	cols.push({
		data: 'program',
		title: 'Program',
		render: function (data, type, full, meta) {
			if (data == '2L') {
				return '540';
			} else if (data == '2F') {
				return '539';
			} else {
				return ''
			}
		}
	});

	cols.push({
		data: 'average_downtime',
		title:'<span title="The cycle time minus the target cycle time.">Avg Downtime (Minutes)</span>',
		render: function (data, type, full, meta) {
			return (data / 60).toFixed(2);
		}
	});
	cols.push({
		data: 'total_downtime',
		title:'<span title="The cycle time minus the target cycle time.">Total Downtime (Minutes)</span>',
		render: function (data, type, full, meta) {
			return (data / 60).toFixed(2);
		}
	});
	cols.push({
		data: 'instance_count',
		title:'<span title="The total number of downtime instances during the chosen time period.">Total Downtime (Instances)</span>',
	});
	
	dt_downtimes = $('.table-report').DataTable({
		"bInfo":false,
		"bAutoWidth":true,
		autoWidth:true,
		retrieve: true,		
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		},                        
		data: jso,        
		order: [[3, 'asc']],
		columns: cols
	});
	
	dt_downtimes.column(1).visible(false);
	dt_downtimes.column(2).visible(false);
	dt_downtimes.column(4).visible(false);
	// show hide cols based on groupBy
	if ($('#inpGroupBy').val() === "Operator") {
		dt_downtimes.column(1).visible(true);
	} else if ($('#inpGroupBy').val() === "Date") {
		dt_downtimes.column(2).visible(true);
	} else if ($('#inpGroupBy').val() === "Program") {
		dt_downtimes.column(4).visible(true);
	}

	$(dt_downtimes).find('th[title]').tooltip({"delay":0, "track":true, "fade":250});
}

/* Formatting function for row details */
function populate_station_detail_row(r) {	
	childContent = '<div class="col-sm-12 well">'+
				'<div class="col-sm-12"><table class="station-detail table table-striped table-condensed table-bordered"></table></div>'+
				//'<div class="col-sm-4"><div class="station-chart-'+r.data().station_ID+'"></div></div>'+
				'</div>';
	r.child($(childContent)).show();
	
	//build Operator table in left table
	populate_detail_table(r);

	//build line graph in right table
	// if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
	// 	populateDetailChart(r);
	// }
	
}

function populate_detail_table(r) {
	var params = {
		'action':'downtimes_detail',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':r.data().line_id == null ? "%" : r.data().line_id,
		'station_id':r.data().id == null ? "%" : r.data().id,
		'operator_id':r.data().operator_ID == null ? "%" : r.data().operator_ID,
		'program':r.data().program == null ? "%" : r.data().program,
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		//'machine_ID': $('#machine_select :selected').val(),
       // 'over_target_cycle_time': over_target_cycle_time ? 1 : 0,
        //'custom_target_cycle_time': $('#custom_target_cycle_time_input').val()
	};

	console.log(params);

	$.getJSON(processor, params, function(jso) {
		if (!jso) {return;}
		if (dtStationDetail != null) {
			dtStationDetail.destroy();
		}

		var cols = [];		
		if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
			cols.push({data: 'station', "title":"Station"});
		} else if (chosen_system.process_type == 'REPETITIVE') {
			cols.push({data: 'tool_description', "title":"Tool"});
		}



		cols.push({data: 'name', "title":"Operator"});
		cols.push({data: 'entry_time.date', "title":"Entry Time"});

		cols.push({
			data: 'downtime',
			title:'<span title="The cycle time minus the target cycle time.">Downtime</span>',
			render: function (data, type, full, meta) {
				return (data / 60).toFixed(2);
			}
		});
		
		dtStationDetail = $('.station-detail').DataTable({
			"sScrollY": "100%",
			"bScrollCollapse": true,
			"bAutoWidth":true,
			autoWidth:true,
			retrieve: true,
			
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},                        
			data: jso,        
			order: [[2, 'desc']], 
			columns: cols
		});
	});
	
}
