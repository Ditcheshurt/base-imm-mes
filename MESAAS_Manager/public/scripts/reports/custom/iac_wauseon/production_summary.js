var processor = './ajax_processors/reports/production_summary.php';
var selDate;
var systems;
var chosen_system;
var dtProductionTable;

$(document).ready(function() {

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    $('#system_select').on('change', load_filter_options);

    $('#line_select').on('change', line_changed);

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

    $('.btn-run').on('click', function () {
        populateReport();
    });

	// populateCriteria(function () {
	// 	populateReport();
	// });
});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    load_lines({system_id: system_id}, function (lines) {
        show_lines(lines);
        line_changed();
    });
}

function line_changed () {
    $('#line_model_select').empty();
    $('#line_model_select').nysus_select({
        name: 'line_model',
        key: 'model',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_line_models', line_id: $('#line_select').val()},
        render: function (line_model) {return line_model.model;},
        placeholder: '- Model -'
    }).prop('disabled', false);
}


// function populateCriteria(callback) {	
// 	//populate lines	
// 	$.getJSON(processor, {
// 		'action':'lines'
// 	}, function(jso) { 
// 		$.each(jso, function() {
// 			$('#line_select').append($("<option />").val(this.id).text(this.line_code));
// 		});
// 		// set the default to the first option
// 		$("#line_select option:first").attr('selected', true);

// 		// initialize the date controls
// 		var start_date = $('#start_date').val() || new Date().toJSON().slice(0,10);
// 		var end_date = $('#end_date').val() ||  new Date().toJSON().slice(0,10);
// 		$('#start_date').val(start_date);
// 		$('#end_date').val(end_date);
		
// 		//run report button
// 		$('body').on('click', '.btn-run', function() {
// 			populateReport();
// 		});
		
// 		if (callback) {
// 			callback();
// 		}
// 	});
// }

function populateReport() {
	console.log({
        system_id: $('#system_select option:selected').val(),
		line_id: $('#line_select option:selected').val(),
		start_date: get_date_value('start_date'),
		end_date: get_date_value('end_date')
	});

	//populateProductionChart
	$.getJSON(processor, {
		'action':'daily_production_summary',
		'line_id':$('#line_select').val(),
        'line_model':$('#line_model_select').val(),
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	},	populateDailyProductionChart);
	
	// //populateProductionChart
	// $.getJSON(processor, {
	// 	'action':'hourly_production_summary',
	// 	'line_id':$('#line_select').val(),
	// 	'start_date':$('#start_date').val(),
	// 	'end_date':$('#end_date').val(),
 //        'database': chosen_system.database_name,
 //        'process_type': chosen_system.process_type
	// }, populateHourlyProductionChart);
	
	// //populateProductionTable
	// $.getJSON(processor, {
	// 	'action':'production_summary',
	// 	'line_id':$('#line_select').val(),
	// 	'start_date':$('#start_date').val(),
	// 	'end_date':$('#end_date').val(),
 //        'database': chosen_system.database_name,
 //        'process_type': chosen_system.process_type
	// }, populateDailyProductionTable);

    //populateProductionChart
    $.getJSON(processor, {
        'action':'hourly_production_summary',
        'line_id':$('#line_select').val(),
        'line_model':$('#line_model_select').val(),
        'start_date':$('#start_date').val(),
        'end_date':$('#end_date').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
    }, function (data) {
        populateHourlyProductionChart(data);
        populateHourlyProductionTable(data);
    });
}

function populateDailyProductionChart(jso) {
    if (!jso) {return;}

	// populate the cycle times chart	
	var categories = [];
	var data = [];	
	var fs = [];
	var ss= [];
	var ts = [];
	
	$.each(jso, function(k,v) {		
		categories.push(v.built_date);		
		fs.push(v.first_shift);
		ss.push(v.second_shift);
		ts.push(v.third_shift);		
	});
	data.push({                        
		name: 'First Shift',
		data: fs
	});
	data.push({                        
		name: 'Second Shift',
		data: ss
	});
	data.push({                        
		name: 'Third Shift',
		data: ts
	});
	console.log(data);
	
	// test data
	/*
	categories = ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'];
	data = [{
            name: 'John',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Jane',
            data: [2, 2, 3, 2, 1]
        }, {
            name: 'Joe',
            data: [3, 4, 4, 2, 5]
        }];
	*/
	
	var title = 'Daily Production Summary :';
	title += $('#start_date').val() != $('#end_date').val() 
			? $('#start_date').val() + ' - ' + $('#end_date').val()
			:$('#start_date').val();
	
	//daily prod summary chart
	var options = {
		series: data,		
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        xAxis: {
			title: {
                text: 'Date'
            }, 
			categories: categories         
        },
        yAxis: {
			min: 0,
            title: {
                text: 'Built Quantity'
            },
			stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
		legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
			},
			series: {
				cursor: 'pointer',
				point: {
					events: {
						click: function() {
							//console.log({this.category, this.y});
							selDate = this.category;
							
							//populateProductionChart
							$.getJSON(processor, {
								'action':'hourly_production_summary',
								'line_id':$('#line_select').val(),
                                'line_model':$('#line_model_select').val(),
								'start_date':this.category,
								'end_date':this.category
							}, populateHourlyProductionChart);
						}
					}
				}
			}
		}		
	}
	
	// populate the chart
	$('.panel-daily-chart').highcharts(options);
	
}

function populateHourlyProductionChart(jso) {
    if (!jso) {return;}

	// populate the cycle times chart	
	var categories = [];
	var data = [];
	var title;
	var totalBuilt = 0;
	var avgPerHour = 0;
	var startDate = new Date($('#start_date').val() + ' 00:00:00');
	var endDate = new Date();
	var hours = Math.abs(startDate - endDate) / 36e5;
	
	for (var i = 1; i <= 24; i++) {		
		data.push(0);		
	}
	$.each(jso, function(k,v) {	
		data[v.hour-1] = v.qty_built;
		totalBuilt += v.qty_built;
	});	
	
	if (totalBuilt > 0) {
		avgPerHour = totalBuilt / hours;
	}
	
	title = 'Hourly Production Summary : '; 
	if (selDate === undefined) {
		title += $('#start_date').val() != $('#end_date').val() 
				? $('#start_date').val() +' - ' + $('#end_date').val()
				:$('#start_date').val();
	} else {
		title += selDate;
	}
	
	subTitle = 'Total Built:<b>' 
		+ totalBuilt 
		+ '</b><br>Avg Per Hour:<b>'
		+ avgPerHour.toFixed(2) + '</b>';
	
	//hourly production report
	var options = {
        chart: {
            type: 'line'
        },
        title: {
            text: title
        },
        xAxis: {
			title: {
                text: 'Hour'
            },
			categories: ['1am','2am','3am','4am','5am','6am','7am','8am','9am','10am',
			'11am','12pm','1pm','2pm','3pm','4pm','5pm','6pm','7pm','8pm','9pm','10pm',
			'11pm','12pm']            
        },
        yAxis: {
            title: {
                text: 'Built Quantity'
            }
        },
		 tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },
        series: [{
					name: 'Built Quantity',
					data: data
				}],
		subtitle: {
				text: subTitle
			}
	};
	
	
	// populate the chart
	$('.panel-hourly-chart').highcharts(options);

}

function populateHourlyProductionTable(jso) {
    if (!jso) {return;}

    if (dtProductionTable != null) { 
        dtProductionTable.destroy();    //so the thing will update      
    } 
    dtProductionTable = $('.table-prod-report').DataTable({
        "bInfo":false,
        "bAutoWidth":true,
        autoWidth:true,
        retrieve: true,     
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"
            ]
        },                        
        data: jso,        
        order: [[0, 'asc']],
        columns: [
            {
                data: 'hour',
                visible: false,
                searchable: false
            },
            {
                data: 'hour',
                title: 'Hour',
                defaultContent: '',
                render: function (data, type, full, meta) {
                    if (data === 0) {
                        return '12am';
                    } else if (data === 12) {
                        return '12pm';
                    } else if (data < 12 && data > 0) {
                        return data + 'am';
                    } else {
                        return (data - 12) + 'pm';
                    }
                }
            },
            {data: 'qty_built', "title":"Built", "defaultContent":""}
        ]

    });
    
}

// function populateDailyProductionTable(jso) {
// 	if (!jso) {return;}

// 	if (dtProductionTable != null) { 
// 		dtProductionTable.destroy();	//so the thing will update		
// 	} 
// 	dtProductionTable = $('.table-prod-report').DataTable({
// 		"bInfo":false,
// 		"bAutoWidth":true,
// 		autoWidth:true,
// 		retrieve: true,		
// 		dom: 'T<"clear">lfrtip',
// 		tableTools: {
// 			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
// 			"aButtons": [
// 				"copy",
// 				"csv",
// 				"xls",
// 				{
// 					"sExtends": "pdf",
// 					"sPdfOrientation": "portrait"
// 				},
// 				"print"
// 			]
// 		},                        
// 		data: jso,        
// 		order: [[3, 'asc']],
// 		columns: [
// 			{
// 				"className": 'details-control',
// 				"orderable": false,
// 				"data": null,
// 				"defaultContent": ''
// 			},
// 			{data: 'build_order', "title":"Build Order", "defaultContent":""},
// 			{data: 'sequence', "title":"Sequence", "defaultContent":""},
// 			{data: 'VIN', "title":"VIN"},
// 			{data: 'built_time_formatted', "title":"Built"},
// 			{data: 'pallet_number', "title":"Pallet"},
// 			{data: 'pallet_position', "title":"Pallet Position"}
// 		]

// 	});
	
// }