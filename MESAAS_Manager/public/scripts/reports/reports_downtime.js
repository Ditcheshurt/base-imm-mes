var processor = './ajax_processors/reports_downtime.php';

$(function() {

	function getDateTime(end) {
		var time = "00:00";
		if (typeof end != "undefined")
			time = "23:59";

		var d = new Date();
		return d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + time;
	};

	$("#nys").nysReports({
		"filters": {
			"options": {
				"cols_per_row": 2
			},
			"fields": [{
				"id": "from_date",
				"input": "datetimepicker",
				"title": "From",
				"options": { "default": getDateTime() }
			}, {
				"id": "to_date",
				"input": "datetimepicker",
				"title": "To",
				"options": { "default": getDateTime(true) }
			}, {
				"id": "sel_shifts",
				"input": "select2",
				"title": "Shifts",
				"selectOptions": {
					"json_key": "shifts",
					"multiple": true
				}
			}, {
				"id": "sel_machines",
				"input": "select2",
				"title": "Machines",
				"selectOptions": {
					"json_key": "machines",
					"multiple": true
				}
			}, {
				"id": "sel_sub_machines",
				"input": "select2",
				"title": "Sub Machines",
				"selectOptions": {
					"json_key": "sub_machines",
					"multiple": true
				}
			}, {
				"id": "sel_reasons",
				"input": "select2",
				"title": "Reason Codes",
				"selectOptions": {
					"json_key": "reasons",
					"override_options": function(data) {

						var content = '<option value=""> - Choose - </option>';
						var last_group;

						for (var i = 0; i < data.length; i++) {

							var option = data[i];
							//split select into categories based upon line
							var cat = option.text.split(":");
							var group = cat[0];
							var reason = cat[1];

							if (last_group != group) {

								//special case for first one
								if (i == 0)
									content += '<optgroup label="' + group + '">';
								else
									content += '</optgroup><optgroup label="' + group + '">';

								last_group = group;

							}

							content += '<option value="' + option.value + '">' + reason + '</option>';

						}

						return content;

					}
				}
			}, {
				"id": "sel_uptime",
				"input": "checkbox",
				"title": "% Uptime"
			}, {
				"id": "sel_hide_scheduled",
				"input": "checkbox",
				"title": "Hide Scheduled"
			}, {
				"id": "group_by",
				"input": "select2",
				"title": "Group By",
				"selectOptions": {
					"multiple": true,
					"group": [{
						"text": "Date",
						"value": "date"
					}, {
						"text": "Shift",
						"value": "shift"
					}, {
						"text": "Machine",
						"value": "machine"
					}, {
						"text": "Sub-Machine",
						"value": "sub_machine"
					}, {
						"text": "Reason Code",
						"value": "reason_code"
					}, {
						"text": "Hour",
						"value": "hour"
					}, {
						"text": "Week",
						"value": "week"
					}]
				}
			}]
		},
		"selectContent": {
			"url": processor,
			"data": {
				"action":"get_select_content",
				"get": ['shifts', 'machines', 'sub_machines', 'reasons']
			}
		},
		"reportContent": {
			"url": processor + "?action=run_report"
		},
		"drilldownContent": [{
			"url": processor + "?action=drilldown"
		}],
		"chartContent": {
			"id": "chart",
			"type": "ColumnChart",
			"cols": [{
				"type": "group_by",
				"name": "date",
				"axis": "x"
			}, {
				"type": "number",
				"name": "Downtime (min)",
				"alt_name": "Uptime (%)",
				"axis": "y"
			}]
		}
	});

});