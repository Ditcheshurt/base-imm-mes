var processor = './ajax_processors/reports/press_params.php';

var system_select;
var machine_select;
var start_date_input;
var end_date_input;
var shift_select;
var chosen_system;
var table;
var production_panel;
var filter_run_btn;
var defectTable;

var tmpDataPT = 0;

$(document).ready(function () {
	system_select = $('#system_select');
	machine_select = $('#machine_select');
	start_date_input = $('#start_date');
	end_data_input = $('#end_date');
	$params = $('#params_combo');
	
	production_panel = $('#production_panel');
    filter_run_btn = $('#filter_panel .btn-run');

    filter_run_btn.prop('disabled', true);

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    system_select.on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_time('start_date', true);
    init_date_time('end_date', false);

	// run report button
	$('body').on('click', '.btn-run', load_param_data);
	
	var params_options = {
		label_text: 'Parameters'
		, label_tooltip: 'Select parameters'
		, control_id: 'params-combo'
		, control_tag: 'select'
	};
	$params.nysus_labeled_control(params_options);
	build_combo($('#params-combo'), {action: 'get_params'}, [{key: 'multiple', value: 'multiple'}]);

});

function load_filter_options () {
    filter_run_btn.prop('disabled', true);
    var system_id = system_select.find('option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    //$('#machine_select').empty();
    machine_select.nysus_select({
        name: 'machine_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_machines', database: chosen_system.database_name},
        render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		placeholder: 'Select Machine',
        callback: function (data) {
            
        }
    });
	
	machine_select.on('change', function(e) {
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			filter_run_btn.prop('disabled', true);
		} else {
			filter_run_btn.prop('disabled', false);
		}
	});
}

function load_param_data () {
	$('#div_parameter_chart').empty();
	$('body .btn-run').html('Loading...');
	$('body .btn-run').addClass('disabled');
	var sdate = -1;
	var edate = -1;

	try {
		sdate = get_date_time('start_date');
		edate = get_date_time('end_date');
		var params = {
			action: 'get_param_data',
			params: $('#params-combo').select2().val()?$('#params-combo').select2().val():-1,
			start_date: sdate,
			end_date: edate,
			database: chosen_system.database_name,
			machine_ID: $('#machine_select option:selected').val(),
			system_ID: function (d) {return $('#system_select option:selected').val();},
		};

		$.getJSON(processor, params)
			.done(generateParameterChart)
			.fail(function () {
				console.error('Unable to load param data.');
			});
	} catch (e) {
		if (sdate == -1) {
			$('.start_date').focus();
		}
		if (edate == -1) {
			$('.end_date').focus();
		}
		$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
		$('body .btn-run').removeClass('disabled');		
	}
}

function build_combo(combo, data, attr, selection) {
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function generateParameterChart(jso) {
	var data = [];
	var tbl = [];
	var cols = [];
	var ucl = 1000;
	var lcl = 0;
	
	$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
	$('body .btn-run').removeClass('disabled');
	cols.push({data:'cycle_ID',title:'Cycle ID'});
	cols.push({data:'cycle_ts',title:'Cycle Timestamp'});
	cols.push({data:'tool_name',title:'Tool Name'});
	
	if (defectTable != null) {
		//defectTable.clear().draw();
        //defectTable.destroy();    //so the thing will update
        $('#parameter_table').find('.panel-body').empty();
    }
	var $grid_view = $('#parameter_table');
	var $table = $('<table class="table table-condensed table-bordered table-striped table-display"></table>');
	$table.appendTo($grid_view.find('.panel-body'));
	
	if(jso.length > 0) {
		for(i=0;i<jso.length;i++){
			var tmpCol = cols.findIndex(function(z){return z.data == jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')');});
			if(tmpCol == -1) {cols.push({data:jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')'),title:jso[i].data_point_desc});}
			
			tmpDataPT = jso[i].data_point_ID;
			tmpCyclID = jso[i].cycle_ID;
			tmpCyclTM = jso[i].cycle_time.date;
			tmpToolID = jso[i].tool;
			var dptID = data.findIndex(function(a){return a.data_point_ID == tmpDataPT;});
			if(dptID > -1) {
				data[dptID].data_points.push({cycle_id:jso[i].cycle_ID,data_point_value:jso[i].data_point_value});
				if(data[dptID].ucl < jso[i].ucl) {data[dptID].ucl = jso[i].ucl;}
				if(data[dptID].lcl > jso[i].lcl) {data[dptID].lcl = jso[i].lcl;}
			} else {
				data.push({data_point_ID:jso[i].data_point_ID, data_point_desc:jso[i].data_point_desc, ucl:jso[i].ucl, lcl:jso[i].lcl, data_points:[]});
			}
			var tblID = tbl.findIndex(function(b){return b.cycle_ID == tmpCyclID;});
			if(tblID > -1) {
				tbl[tblID][jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')')] = jso[i].data_point_value;
			} else {
				var t = tbl.push({cycle_ID:tmpCyclID, cycle_ts:tmpCyclTM.substr(0,tmpCyclTM.indexOf('.')).replace(/-/g,'/'), tool_name:tmpToolID});
				var new_cyclID = tbl.findIndex(function(c){return c.cycle_ID == tmpCyclID;});
				if(new_cyclID > -1) {
					tbl[new_cyclID][jso[i].data_point_desc.toLowerCase().replace(/\ /g,'_').replace(/\[/g,'(').replace(/\]/g,')')] = jso[i].data_point_value;
				} else {
					console.log('couldn\'t find the inserted tbl ID for tmpCyclID: ' + tmpCyclID);
				}
			}
		}
		
		for(j=0;j<data.length; j++){
			var adj_pts = Math.floor(data[j].data_points.length/1000)+1;
			var cont = $('#div_chart_' + data[j].data_point_ID);
			if (cont.length == 0) {
				cont = $('<div />').attr('id', 'div_chart_' + data[j].data_point_ID).appendTo($('#div_parameter_chart')).addClass('parameter-container');
			}
			
			if (data[j].ucl) {
				ucl = data[j].ucl;
			} else {
				ucl = 100
			}
			if (data[j].lcl) {
				lcl = data[j].lcl;
			} else {
				lcl = 0;
			}
			
			cont.highcharts({
				chart: {
					type: 'line'
				},
				title: {
					text: data[j].data_point_desc,
					enabled: true
				},
				credits: {
					enabled: false
				},
				xAxis: {
					categories: [],
					labels: {enabled: false},
					tickInterval: adj_pts*1000,
					visible: false
				},
				yAxis: {
					plotBands: [
						{
							from: ucl,
							to: 10000,
							color: 'rgba(255, 0, 0, 0.1)'
						},
						{
							from: -10000,
							to: lcl,
							color: 'rgba(255, 0, 0, 0.1)'
						}
					]
				},
				legend: {
					shadow: true,
					enabled: true
				},
				tooltip: {
					shared: true
				},
				plotOptions: {
					line: {
						grouping: false,
						shadow: true,
						borderWidth: 0,
						turboThreshold:5000
					}
				},
				series: [{
					name: 'Values',
					color: 'rgba(22,170,2,.7)',
					data: [],
					pointInterval: 100,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					point: {
						events: {
							mouseOver: function() {
								console.log('x: ' + this.x + ', y: ' + this.y);
								console.log(this);
							}
						}
					}
				},{
					name: 'Upper Limit',
					color: 'red',
					data: [],
					pointInterval: 100,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				},{
					name: 'Lower Limit',
					color: 'red',
					data: [],
					pointInterval: 100,
					pointPlacement: 0,
					dataLabels: { enabled: false },
					marker: {radius: 1}
				}]
			});


			var arr = [];
			var arr2 = [];
			var arr3 = [];

			for (var k = 0; k < data[j].data_points.length; k++) {
				var x = data[j].data_points[k].cycle_id;
				var y = data[j].data_points[k].data_point_value;
				var c = 'limegreen';
				if (y < lcl || y > ucl) {
					c = 'red';
				}
				arr.push({y:parseFloat(y.toFixed(3)), name:('Cycle ID: ' + x), color:c});
				arr2.push(ucl);
				arr3.push(lcl);
			}
			var chart = cont.highcharts();
			chart.setTitle(data[j].data_point_desc);
			chart.series[0].setData(arr);
			chart.series[1].setData(arr2);
			chart.series[2].setData(arr3);
			chart.redraw();
		}
		
		defectTable = $('.table-display').DataTable({
			"bAutoWidth":false,
			dom: 'T<"clear">lfBrtip',
			width: '100%',
			buttons: [
				'copyHtml5',
				{
					extend: 'excelHtml5',
					title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
				},
				{
					extend: 'csvHtml5',
					title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
				},
				'pdfHtml5'
			],
			data: tbl,
			columns: cols
		});

	} else {
		$('<div />').html("No data found").appendTo($('#div_parameter_chart'));
	}
	$('#parameter_table').show();
	$('#parameter_panel').show();
}

function init_date_time (id,start_nEND) {
	// initialize the date input to the current date
	if (start_nEND === undefined) {start_nEND = true;}
	
	var today = new Date();
	if (start_nEND) {
		today.setHours(0,0,0,0);
	} else {
		today.setHours(23,59,0,0);
	}
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().slice(0,-1);
	$('#'+id).val(today_str);
}

function get_date_time (id) {
	var date_str = $('#'+id).val();
	var objDT = new Date(date_str);
	var offset = objDT.getTimezoneOffset() * 60;
	objDT = new Date(objDT.getTime() - offset * 1000);
	return objDT.toISOString().split('T')[0].replace(/\-/g,'/') + ' ' + objDT.toISOString().split('T')[1].slice(0,8);
}
