ABC = NYSUS.COMMON.BC;

$(document).ready(function () {
	ABC.sections = [
		{el_id: 'assembly_overview', title: 'Assembly Overview', loader: ABC.load_assembly, callback: ABC.show_assembly},
		{el_id: 'rework_history', title: 'Rework History', loader: ABC.load_rework_history, callback: ABC.show_rework_history},
		{el_id: 'component_lots', title: 'Component Lots', loader: ABC.load_component_lots, callback: ABC.show_component_lots},
		{el_id: 'machine_history', title: 'Machine History', loader: ABC.load_machine_history, callback: ABC.show_machine_history},
		{el_id: 'machine_cycle_history', title: 'Machine Cycle History', loader: ABC.load_machine_cycle_history, callback: ABC.show_machine_cycle_history},
		// {el_id: 'machine_process_data', title: 'Machine Process Data', loader: ABC.load_machine_process_data, callback: ABC.show_machine_process_data},
		// {el_id: 'rework_history', title: 'Rework History', loader: ABC.load_rework_history, callback: ABC.show_rework_history},
		{el_id: 'plc_values', title: 'PLC Values', loader: ABC.load_plc_values, callback: ABC.show_plc_values},
		{el_id: 'part_gauge_details', title: 'Part Gauge Details', loader: ABC.load_part_gauge_details, callback: ABC.show_part_gauge_details},
		{el_id: 'part_gauge_history', title: 'Part Gauge History', loader: ABC.load_part_gauge_history, callback: ABC.show_part_gauge_history},
		{el_id: 'auto_gauge_data', title: 'Auto Gauge Data', loader: ABC.load_auto_gauge_data, callback: ABC.show_auto_gauge_data},
		{el_id: 'mahr_mmq_data', title: 'Mahr MMQ Data', loader: ABC.load_mahr_mmq_data, callback: ABC.show_mahr_mmq_data},
		{el_id: 'mahr_mmq_history', title: 'Mahr MMQ History', loader: ABC.load_mahr_mmq_history, callback: ABC.show_mahr_mmq_history},

	];

	ABC.init('main_div', './ajax_processors/reports/machine_cycle_part_info.php');
});

ABC.init = function (element_ID, processor) {
	ABC.processor = processor;
	ABC.main_div = $('#'+element_ID);
	ABC.serial_number = ABC.main_div.data('serial-number');

	ABC.set_title();
	ABC.build_sections();
	ABC.load_data();
};

ABC.set_title = function () {
	$('<div>Assembly Birth Certificate (Barcode: <span class="full-barcode"></span>)</div>').addClass('main-header').appendTo(ABC.main_div);
};

ABC.load_assembly = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_part',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.assembly = data;
			cb(ABC.assembly);
		}
	});
};

ABC.show_assembly = function (assembly) {
	var section = $('#assembly_overview', ABC.main_div);
	var content = $('.section-content', section);

	var table = $('<table></table>').prop({id: 'assembly_overview_table'}).addClass('section-table').appendTo(content);

	var tr = $('<tr></tr>').appendTo(table);
	tr.append('<td><label>Serial Number:</label> <span class="serial-number"></span></td>');
	tr.append('<td><label>Part Description:</label> <span class="part-description"></span></td>');

	tr = $('<tr></tr>').appendTo(table);
	tr.append('<td><label>Chrysler Part Number:</label> <span class="chrysler-part-number"></span></td>');
	tr.append('<td><label>Packout Time:</label> <span class="packout-time"></span></td>');

	tr = $('<tr></tr>').appendTo(table);
	tr.append('<td><label>FCA-E&T Part Number:</label> <span class="fcaet-part-number"></span></td>');
	tr.append('<td><label>Supplier Code:</label> <span class="supplier-code"></span></td>');

	tr = $('<tr></tr>').appendTo(table);
	tr.append('<td><label>Metaldyne Part Number:</label> <span class="metaldyne-part-number"></span></td>');

	$('.full-barcode').html(assembly.full_barcode);
	$('.serial-number').html(assembly.serial_number);
	$('.part-description').html(assembly.part_desc);
	$('.chrysler-part-number').html(assembly.chrysler_part_number);
	$('.fcaet-part-number').html(assembly.fcaet_part_number);
	$('.packout-time').html(assembly.complete_time ? assembly.complete_time.date : 'IN PROGRESS');
	$('.supplier-code').html(assembly.supplier_code);
	$('.metaldyne-part-number').html(assembly.part_number);
	section.show();
};

ABC.load_component_lots = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_component_lots',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.component_lots = data;
			cb(ABC.component_lots);
		}
	});
};

ABC.show_component_lots = function (component_lots) {
	var columns = [
		{name: 'part_number', title: 'Part Number'},
		{name: 'description', title: 'Part Description'},
		{name: 'lot_number', title: 'Lot Serial Number'}
	];

	var section = $('#component_lots', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, component_lots);
	section.show();
};

ABC.load_machine_history = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_machine_history',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.machine_history = data;
			cb(ABC.machine_history);
		}
	});
};

ABC.show_machine_history = function (machine_history) {
	var columns = [
		{name: 'machine_name', title: 'Machine'},
		{name: 'operator_name', title: 'Operator'},
		{name: 'check_time', title: 'Barcode Scan Time', render: ABC.render_datetime},
		{name: 'start_time', title: 'Machine Start Time', render: ABC.render_datetime},
		{name: 'complete_time', title: 'Machine Complete Time', render: ABC.render_datetime},
		{name: 'cycle_time', title: 'Machine Cycle Time (seconds)'},
		{name: 'full_cycle_time', title: 'Total Cycle Time (seconds)'}
	];

	var section = $('#machine_history', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, machine_history);
	section.show();
};

ABC.load_machine_cycle_history = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_machine_cycle_history',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.machine_cycle_history = data;
			cb(ABC.machine_cycle_history);
		}
	});
};

ABC.show_machine_cycle_history = function (machine_cycle_history) {
	var columns = [
		{name: 'check_time', title: 'Barcode Scan Time', render: ABC.render_datetime},
		{name: 'check_result', title: 'Check Result'},
		{name: 'cycle_start_time', title: 'Machine Start Time', render: ABC.render_datetime},
		{name: 'cycle_complete_time', title: 'Machine Complete Time', render: ABC.render_datetime},
		{name: 'cycle_complete_result', title: 'Cycle Complete Result', render: ABC.render_cycle_complete_result}
	];

	var section = $('#machine_cycle_history', ABC.main_div);
	var content = $('.section-content', section);

	machine_cycle_history.forEach(function (machine) {
		var machine_div = $('<div></div>').prop({id:'machine_history_'+machine.machine_ID}).appendTo(content);
		$('<div></div>').html(machine.machine_name).addClass('sub-table-title').appendTo(machine_div);
		var history_div = $('<div></div>').appendTo(machine_div);
		ABC.build_table(history_div, columns, machine.machine_cycle_part_history);
	});

	section.show();
};

ABC.load_part_gauge_history = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_part_gauge_history',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.part_gauge_history = data;
			cb(ABC.part_gauge_history);
		}
	});
};

ABC.show_part_gauge_history = function (part_gauge_history) {
	var columns = [
		{name: 'machine_name', title: 'Machine'},
		{name: 'serial_number', title: 'Last Part Checked', render: ABC.render_part_link},
		{name: 'recorded_date', title: 'Time', render: ABC.render_datetime},
		{name: 'operator_name', title: 'Operator'}
	];

	var section = $('#part_gauge_history', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, part_gauge_history);
	section.show();
};

ABC.load_part_gauge_details = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_part_gauge_details',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.part_gauge_details = data;
			cb(ABC.part_gauge_details);
		}
	});
};

ABC.show_part_gauge_details = function (part_gauge_details) {
	var show = false;

	var columns = [
		{name: 'description', title: 'Description'},
		{name: 'data_point_value', title: 'Value'},
		{name: 'min_value', title: 'Min Value'},
		{name: 'max_value', title: 'Max Value'}
	];

	var section = $('#part_gauge_details', ABC.main_div);
	var content = $('.section-content', section);

	part_gauge_details.forEach(function (machine) {
		var machine_div = $('<div></div>').prop({id:'machine_part_gauge_detail_'+machine.machine_ID}).appendTo(content);
		$('<div></div>').html(machine.machine_name).addClass('sub-table-title').appendTo(machine_div);
		var history_div = $('<div></div>').appendTo(machine_div);
		ABC.build_table(history_div, columns, machine.machine_part_gauge_detail);

		show = show || machine.machine_part_gauge_detail;
	});

	if (show) {section.show();}
};

ABC.load_plc_values = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_plc_values',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.plc_values = data;
			cb(ABC.plc_values);
		}
	});
};

ABC.show_plc_values = function (plc_values) {
	var columns = [
		{name: 'machine_ID', title: 'Machine'},
		{name: 'data_point_name', title: 'Data Point'},
		{name: 'tag_value', title: 'Value', render: ABC.render_plc_value},
	];

	var section = $('#plc_values', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, plc_values);
	section.show();
};

ABC.load_auto_gauge_data = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_auto_gauge_data',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.auto_gauge_data = data;
			cb(ABC.auto_gauge_data);
		}
	});
};

ABC.show_auto_gauge_data = function (auto_gauge_data) {
	var columns = [
		{name: 'm1', title: 'm1'},
		{name: 'm2', title: 'm2'},
		{name: 'm3', title: 'm3'},
		{name: 'm4', title: 'm4'},
		{name: 'taper', title: 'taper'},
		{name: 'sp1_low', title: 'sp1_low'},
		{name: 'sp1_high', title: 'sp1_high'},
		{name: 'sp2_low', title: 'sp2_low'},
		{name: 'sp2_high', title: 'sp2_high'},
		{name: 'sp3_low', title: 'sp3_low'},
		{name: 'sp3_high', title: 'sp3_high'},
		{name: 'sp4_low', title: 'sp4_low'},
		{name: 'sp4_high', title: 'sp4_high'},
		{name: 'spt_low', title: 'spt_low'},
		{name: 'spt_high', title: 'spt_high'},
		{name: 'good_part', title: 'good_part', render: ABC.render_good_part_data},
		{name: 'recorded_date', title: 'Time', render: ABC.render_datetime}
	];

	var section = $('#auto_gauge_data', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, auto_gauge_data);
	section.show();
};

ABC.load_mahr_mmq_data = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_mahr_mmq_data',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.mahr_mmq_data = data;
			cb(ABC.mahr_mmq_data);
		}
	});
};

ABC.show_mahr_mmq_data = function (mahr_mmq_data) {
	var columns = [
		{name: 'name', title: 'name'},
		{name: 'datum', title: 'datum'},
		{name: 'nominal', title: 'nominal'},
		{name: 'l_tol', title: 'l_tol'},
		{name: 'u_tol', title: 'u_tol'},
		{name: 'value', title: 'value'},
		{name: 'unit', title: 'unit'},
		{name: 'feature', title: 'feature'},
		{name: 'low_nat_border', title: 'low_nat_border'},
		{name: 'type', title: 'type'},
		{name: 'no', title: 'no'},
		{name: 'date', title: 'date'},
		{name: 'time', title: 'time'},
		{name: 'created_date', title: 'Check Time', render: ABC.render_datetime},
		//{name: 'operator', title: 'operator'},
		{name: 'operator_name', title: 'Operator'},
	];

	var section = $('#mahr_mmq_data', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, mahr_mmq_data);
	section.show();
};

ABC.load_mahr_mmq_history = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_mahr_mmq_history',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.mahr_mmq_history = data;
			cb(ABC.mahr_mmq_history);
		}
	});
};

ABC.show_mahr_mmq_history = function (mahr_mmq_history) {
	var columns = [
		{name: 'serial_number', title: 'Last Part Checked', render: ABC.render_part_link},
		//{name: 'recorded_date', title: 'Time', render: ABC.render_datetime},
		{name: 'created_date', title: 'Time', render: ABC.render_datetime},
		{name: 'operator_name', title: 'Operator'}
	];

	var section = $('#mahr_mmq_history', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, mahr_mmq_history);
	section.show();
};

ABC.load_rework_history = function (cb) {
	$.getJSON(ABC.processor, {
		action: 'get_rework_history',
		serial_number: ABC.serial_number
	}).done(function (data) {
		if (data) {
			ABC.rework_history = data;
			cb(ABC.rework_history);
		}
	});
};

ABC.show_rework_history = function (rework_history) {
	var columns = [
		{name: 'rejected_station_date', title: 'Reject Time', render: ABC.render_datetime},
		{name: 'rejected_station_desc', title: 'Reject Station'},
		{name: 'operator_name', title: 'Operator'},
		{name: 'approval_date', title: 'Approval Time', render: ABC.render_datetime},
		{name: 'approval_operator_name', title: 'Approved By'},
		{name: 'injected_station_date', title: 'Reintroduced Time', render: ABC.render_datetime},
		{name: 'injected_station_desc', title: 'Reintroduced Station'},
		{name: 'rework_date', title: 'Rework Time', render: ABC.render_datetime}
	];

	var section = $('#rework_history', ABC.main_div);
	var content = $('.section-content', section);
	ABC.build_table(content, columns, rework_history);
	section.show();
};

ABC.render_good_part_data = function (row, value) {
	var res;

	if (value == 1) {
		res = 'TRUE';
	} else if (value == 0) {
		res = 'FALSE';
	} else {
		res = '';
	}

	return res;
};


ABC.render_cycle_complete_result = function (row, value) {
	var res;

	if (value == 1) {
		res = 'PASSED';
	} else if (value == 2) {
		res = 'FAILED';
	} else {
		res = 'INCOMPLETE CYCLE';
	}

	return res;
};

ABC.render_part_link = function (row, serial_number) {
	var s;

	if (serial_number) {
		s = '<a href="./reports.php?type=machine_cycle_part_info&process_type=REPETITIVE&database=NYSUS_REPETITIVE_MES&serial_number='+serial_number+'" target="_blank">'+serial_number+'</a>';
	} else {
		s = 'NONE';
	}

	return s;
};

ABC.render_plc_value = function (row, value) {
	return (value || value === 0) ? (value + ' ' + row.units) : 'NONE';
};



