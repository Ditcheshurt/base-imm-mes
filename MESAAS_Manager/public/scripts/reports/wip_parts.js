var processor = './ajax_processors/report_wip_parts.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

	load_systems(null, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	$('.btn-run').on('click', function () {
		load_data();
	});

	$('#wip_part_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=wip_part_info&process_type='+chosen_system.process_type+'&database='+chosen_system.database_name+'&wip_part_id='+row.data().ID);
	});
});

function load_filter_options () {
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	//load_lines({system_id: system_id, line_type: 2}, show_lines);
	load_lines({system_id: system_id}, show_lines);
	//load_lines({system_id: system_id, main_line_id: chosen_system.}, show_lines);
	show_statuses([
		{ID: 0, status_desc: 'In Progress'},
		{ID: 1, status_desc: 'Built'}
	]);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#wip_part_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: processor,
			data: {
				action: 'list_wip_parts',
				start_date: function (d) {return get_date_value('start_date');},
				end_date: function (d) {return get_date_value('end_date');},
				line_id: function (d) {return $('#line_select option:selected').val();},
				status: function (d) {return $('#status_select option:selected').val();},
        		database: function (d) {return chosen_system.database_name;},
        		process_type: function (d) {return chosen_system.process_type;},
			},
			dataSrc: '',
		},

		order: [[0, 'desc']],

		columns: [
		//{data: 'ID'},
		{
			data: 'built_time',
			title: 'Built Time',
			render:  render_date
		},
		{data: 'line_code', title:'Line'},
		{data: 'VIN', title: 'VIN'},
		{data: 'sequence', title: 'Sequence'},

		{data: 'build_order', title: 'MES Build Order'},
		{data: 'serial_number', title: 'Serial'},
		{data: 'last_completed_station_desc', title: 'Last Station'},
		{
			title: 'Actions',
			className: 'actions',
			orderable: false,
			data: null,
			defaultContent: '',
			render: function (data, type, full, meta) {
				var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
				var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Part Info</button>');
				btns.append(bc_btn);
				return btns[0].outerHTML;
			}
		},
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}
