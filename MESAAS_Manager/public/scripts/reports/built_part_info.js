var processor = './ajax_processors/reports/built_part_info.php';
var built_part_id;

$(document).ready(function () {
    built_part_id = $('#info_table').data('built-part-id');
    load_info(built_part_id, show_info);
});

function load_info (built_part_id, callback) {
    $.getJSON(processor, {
        action: 'load_data',
        built_part_id: built_part_id
    }, callback);
}

function show_info (data) {
    show_built_part_info(data.built_part_info[0]);
    //show_module_info(data.module_info[0]);
    show_station_history_info(data.station_history);
    if (data.process_data != undefined && data.process_data) {
        show_process_data(data.process_data);
    }
}

function show_built_part_info (built_part) {
    $('.serial').html(built_part.serial_number);
    // $('.internal-serial').html(built_part.serial);
    // $('.external-serial').html(built_part.external_serial);
    // $('.part-number').html(built_part.part_number);
    // $('.internal-part-number').html(built_part.internal_part_number);
    // $('.shipping-part-number').html(built_part.shipping_part_number);
    $('.part-desc').html(built_part.part_desc);
    $('.built-time').html(built_part.built_time ? built_part.built_time.date : '');
    $('.line').html(built_part.cell_desc);
    $('.last-station').html(built_part.last_completed_station_ID + ' - ' + built_part.last_completed_station_desc);
}

// function show_module_info (module) {
//     $('.vin').html(module.VIN);
//     $('.sequence').html(module.sequence);
//     $('.build-order').html(module.build_order);
//     $('.module-line').html(module.line_code);
// }

function show_station_history_info (station_history) {
    var tbody = $('#station_instruction_history_table tbody');

    $.each(station_history, function (i, station) {
        var station_row = $('<tr></tr>');
        station_row.append('<td>' + station.station_order + ' - ' + station.station_desc + '</td>');
        //station_row.append('<td>'+(station.entry_time ? station.entry_time.date : '')+'</td>');
        //station_row.append('<td>'+(station.exit_time ? station.exit_time.date : '')+'</td>');
        //station_row.append('<td>'+station.operator_name+'</td>');
        tbody.append(station_row);

        var instructions_row = $('<tr><td colspan="4"></td></tr>');

        var instruction_table = $('<table class="sub-table"></table>');
        var instruction_thead = $(
            '<thead>' +
              '<tr>' +
                '<th>Test Type</th>' +
                '<th>Operator</th>' +
                '<th>Start Time</th>' +
                '<th>End Time</th>' +
                '<th>Completion Details</th>' +
              '</tr>' +
            '</thead>'
        );
        var instruction_tbody = $('<tbody></tbody>');

        $.each(station.instruction_history, function (j, instruction) {
            var instruction_row = $('<tr></tr>');
            instruction_row.append('<td>'+(instruction.type_desc ? instruction.type_desc : '')+'</td>');
            instruction_row.append('<td>'+instruction.operator_name+'</td>');
            instruction_row.append('<td>'+(instruction.entry_time ? instruction.entry_time.date : '')+'</td>');
            instruction_row.append('<td>'+(instruction.exit_time ? instruction.exit_time.date : '')+'</td>');
            instruction_row.append('<td>'+instruction.completion_details+'</td>');
            instruction_tbody.append(instruction_row);
        });

        instruction_table.append(instruction_thead)
                         .append(instruction_tbody);

        instructions_row.append(instruction_table);
        tbody.append(instructions_row);
    });
}

function show_process_data(process_data) {
    var tbody = $('#process_data_table tbody');

    $.each(process_data, function (j, data) {
        var row = $('<tr></tr>');
        row.append('<td>'+data.process_location+'</td>');
        row.append('<td>'+data.process_desc+'</td>');
        row.append('<td>'+data.process_value+'</td>');
        row.append('<td>'+(data.process_timestamp ? data.process_timestamp.date : '')+'</td>');
        tbody.append(row);
    });

    $('#process_data_table').show(true);
    $('#process_data').show(true);
}