var processor = './ajax_processors/reports/modules.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

    load_systems({process_type: 'SEQUENCED'}, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    $('#system_select').on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

    $('.btn-run').on('click', function () {
        load_data();
    });

    $('.btn-reloader').on('click', function () {
        load_data();
    });

    $('#module_table').on('click', '.btn-bcert', function (e) {
        var row = table.row($(this).closest('tr'));
        window.open('reports.php?type=birth_certificate&module_ID='+row.data().ID+'&mrp_company_code='+chosen_system.mrp_company_code);
    });

    $('#module_table').on('click', '.btn-broadcast-components', function (e) {
        var row = table.row($(this).closest('tr'));
        window.open('reports.php?type=module_component_info&module_ID='+row.data().ID);
    });

});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    load_lines({system_id: system_id, line_type: 0}, show_lines);
    load_statuses(show_statuses);
}

function load_data() {
    

    // table setup
    if (typeof table != 'undefined') {
        table.ajax.reload();
        return;
    }

    var export_cols = [0, 1, 2, 3, 4, 5];

    table = $('#module_table').DataTable({
        dom: 'T<"clear">lfrtip',
        tableTools: {
            sSwfPath: "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            aButtons: [
                {
                    sExtends: "copy",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "csv",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "xls",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "pdf",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "print",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
            ]
        },

        ajax: {
            url: processor,
            data: {
                action: 'list_modules',
                start_date: function (d) {return get_date_value('start_date');},
                end_date: function (d) {return get_date_value('end_date');},
                line_id: function (d) {return $('#line_select option:selected').val();},
                status: function (d) {return $('#status_select option:selected').val();}
            },
            dataSrc: '',
        },

        order: [[4, 'desc']],

        columns: [
        //{data: 'ID'},
        {
            data: 'loaded_time',
            title: 'Loaded Time',
            render:  render_date
        },
        {
            data: 'built_time',
            title: 'Built Time',
            render:  render_date
        },
        {data: 'VIN', title: 'VIN'},
        {data: 'sequence', title: 'Sequence'},
        {data: 'build_order', title: 'MES Build Order'},
        {data: 'pallet_number', title: 'Pallet'},
        {data: 'pallet_position', title: 'Position'},
        {data: 'status_desc', title: 'Status'},
        {
            title: 'Actions',
            className: 'actions',
            orderable: false,
            data: null,
            defaultContent: '',
            render: function (data, type, full, meta) {
                var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');

                if (full.status > 1) {
                    var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Birth Certificate</button>');
                    btns.append(bc_btn);
                }

                btns.append($('<button type="button" class="btn btn-xs btn-default btn-broadcast-components">View Broadcast</button>'));
                //btns.append($('<button type="button" class="btn btn-xs btn-danger btn-retrigger">Re-Trigger</button>'));
                return btns[0].outerHTML;
            }
        },
        ]
    });
}

function render_date (data, type, full, meta) {
    if (data) {
        return data.date;
    } else {
        return '';
    }
}

function load_broadcast_components(module_ID, callback) {
    $.getJSON(
    processor,
    {
        action: 'list_components',
        module_ID: module_ID
    },
    callback
    );
}

function show_broadcast_components(data) {
    var cols = [
        {data: 'part_number', title: 'Part Number'},
        {data: 'qty', title: 'Quantity'}
    ];

    fill_table_by_cols('component_table', cols, data, false);
}

