var processor_path = './ajax_processors/reports/daily_oee.php';
var shifts;
var current_shift;
var shift_panel_template;


Handlebars.registerHelper('get', function(obj, prop) {
    return obj[prop];
});


$(document).ready(function () {
    var charts;

    load_template(
        './templates/reports/daily_oee/shift_panel.handlebars',
        function (t) {
            shift_panel_template = t;
            load_oee_data();
        }
    );

    $('#print_btn').click(function() {

        $('.gauge').each(function (i, c) {
            var chart = $(this).highcharts();
            chart.setSize(140,70, false);
            
            setTimeout(function() {
                chart.setSize(225,150, false);
            }, 1200);
        });

        setTimeout(window.print, 500);

    });

});

function load_template(path, cb) {
    $.get(path).done(function (src) {
        var template = Handlebars.compile(src);
        cb(template);
    }).fail(function () {
        console.log('Unable to load template file.');
    });
}

function load_oee_data () {
    $.getJSON(processor_path, {action: 'get_oee_data'})
        .done(show_oee_data)
        .fail(function () {
            console.log('Unable to load oee data for shift.');
        });
}

function get_cell_color (val) {
    if (val <= 0.85) {
        return 'danger';
    } else if (val <= 0.95) {
        return 'warning';
    } else {
        return 'success';
    }
}

function show_oee_data(data) {
    var shifts = data.shifts;

    var columns = [
        {name: 'machine_number', title: 'Machine Number'},
        {name: 'machine_name', title: 'Machine Name'},
		{name: 'tool_desc', title: 'Tool'},
        {name: 'oee', title: 'OEE', type: 'oee_param'},
        {name: 'availability', title: 'Availability', type: 'oee_param'},
        {name: 'quality', title: 'Quality', type: 'oee_param'},
        {name: 'performance', title: 'Performance', type: 'oee_param'},
        {name: 'part_goal', title: 'Goal', type: 'part_goal'},
        {name: 'part_qty', title: 'Produced'},
        {name: 'scrap', title: 'Scrap'},
        {name: 'total_minutes', title: 'Hours', type: 'hours'},
        {name: 'downtime_minutes', title: 'Downtime <span class="text-muted">(min.)</span>', type: 'minutes'}
    ];

    var weekdays = [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    ];

    var start_day = new Date(shifts[shifts.length-1].the_date);
    var start_weekday = weekdays[start_day.getDay()];

    var end_day = new Date(shifts[0].the_date);
    var end_weekday = weekdays[end_day.getDay()];

    var title = 'Production Totals: ' + start_weekday + ', Shift ' + shifts[shifts.length-1].the_shift + ' -- ' + end_weekday + ', Shift ' + shifts[0].the_shift
    build_panel(data.total, columns, title);

    for (var i=0; i<shifts.length; i++) {
        var d = new Date(shifts[i].the_date);
        var weekday = weekdays[d.getDay()];
        var title = 'Production: ' + weekday + ', Shift ' + shifts[i].the_shift
        build_panel(shifts[i], columns, title);
    }
}

function build_panel (data, columns, title) {
    var panel_data = {
        title: title,
        columns: columns,
        data: data
    };

    var panel = shift_panel_template(panel_data);
    var panel_element = $(panel).appendTo('#oee_container');

    // format and style the cells
    panel_element.find('.oee-table tr td').each(function () {
        var td = $(this);
        var type = td.attr('data-type');

        if (type == 'oee_param') {
            var value = parseFloat(td.attr('data-value'));
            td.addClass('oee-param');
            td.addClass(get_cell_color(value));
            td.html(Math.round(value * 100) + '%');
        }

        if (type == 'part_goal') {
            var value = parseFloat(td.attr('data-value'));
            td.html(Math.ceil(value));
        }

        if (type == 'hours') {
            var value = parseFloat(td.attr('data-value'));
            td.html(Math.round(value / 60).toFixed(1));
        }

        if (type == 'minutes') {
            var value = parseFloat(td.attr('data-value'));
            td.html(Math.round(value).toFixed(1));
        }
    });

    // populate gauges
    if (data.summary != undefined) {
        var summary = data.summary;
    } else {
        var summary = null;
    }        

    var gauge_data = [
        {name: 'oee', title: 'OEE', value: summary ? Math.round(summary.oee * 100) : 0},
        {name: 'availability', title: 'Availability', value: summary ? Math.round(summary.availability * 100) : 0},
        {name: 'quality', title: 'Quality', value: summary ? Math.round(summary.quality * 100) : 0},
        {name: 'performance', title: 'Performance', value: summary ? Math.round(summary.performance * 100) : 0}
    ];

    for(var j=0; j<gauge_data.length; j++) {
        var gauge_panel = panel_element.find('.'+gauge_data[j]['name']+'-gauge');
        build_gauge(gauge_panel, gauge_data[j]);
    }
}

function build_gauge (el, data) {
    var series = [{
        name: data.title,
        data: [data.value],
        dataLabels: {
            format: '<div style="text-align:center"><span class="gauge-value-text">{y}</span>' +
                   '<span class="gauge-value-sign">%</span></div>'
        }
    }];

    var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            events: {
                beforePrint: function () {
                    this._hasUserSize = this.hasUserSize;
                    this._reset = [this.chartWidth, this.chartHeight, false];
                    this.setSize(100, 60, false);
                },
                afterPrint: function () {
                    this.setSize.apply(this, this._reset);
                    this.hasUserSize = this._hasUserSize;
                }
            }
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.85, '#DF5353'], // red
                [0.95, '#DDDF0D'], // yellow
                [1,'#55BF3B'] // green
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    $(el).highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: null
        },

        credits: {
            enabled: false
        },

        exporting: {
            enabled: false
        },

        series: series

    }));
}