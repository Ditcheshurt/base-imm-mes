var processor = './ajax_processors/reports/machine_cycle_parts.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

	load_systems(null, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	$('.btn-run').on('click', function () {
		load_data();
	});

	$('#machine_cycle_part_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=machine_cycle_part_info&process_type=REPETITIVE&database=NYSUS_REPETITIVE_MES&serial_number='+row.data().serial_number);
	});
});

function load_filter_options () {
	var system_ID = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_ID;
	});
	chosen_system = res[0];

	//load_lines({system_ID: system_ID, line_type: 2}, show_lines);
	load_lines({system_ID: system_ID}, show_lines);
	//load_lines({system_ID: system_ID, main_line_ID: chosen_system.}, show_lines);
	
	load_parts(show_parts);

	//{ID: 0, status_desc: 'In Queue'},
	show_statuses([
		{ID: 1, status_desc: 'In Progress'},
		{ID: 2, status_desc: 'Built'}
	]);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#machine_cycle_part_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: processor,
			data: {
				action: 'get_machine_cycle_parts',
				start_date: function (d) {return get_date_value('start_date');},
				end_date: function (d) {return get_date_value('end_date');},
				system_ID: function (d) {return $('#system_select option:selected').val();},
				status: function (d) {return $('#status_select option:selected').val();},
				part_ID: function (d) {return $('#part_select option:selected').val();},
        		database: 'NYSUS_REPETITIVE_MES',
        		process_type: 'REPETITIVE',
			},
			dataSrc: '',
		},

		order: [[3, 'desc']],

		columns: [
		//{data: 'ID'},
		{data: 'serial_number', title: 'Serial'},
		{data: 'part_number', title: 'Part #'},
		{data: 'part_desc', title: 'Description'},
		//{data: 'last_completed_station_desc', title: 'Last Station'},
		{
			data: 'start_time',
			title: 'Start Time',
			render:  render_date
		},
		{
			data: 'complete_time',
			title: 'Built Time',
			render:  render_date
		},
		{
			title: 'Actions',
			className: 'actions',
			orderable: false,
			data: null,
			defaultContent: '',
			render: function (data, type, full, meta) {
				var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
				var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Birth Certificate</button>');
				btns.append(bc_btn);
				return btns[0].outerHTML;
			}
		},
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function load_parts (cb) {
    $.getJSON(processor, {
        action: 'get_parts'
    }).done(function (data) {
    	cb(data);
    });
}

function show_parts (data) {
    var part_select = $('#part_select');
    part_select.empty();

    $('<option></option>').val(-1).html('All Parts').appendTo(part_select);

    if (!data) {return;}

    $.each(data, function (i, part) {
        $('<option></option>').prop({id: 'part_'+part.ID, name: 'part_ID'})
        					  .val(part.ID)
        					  .html(part.part_number + ' - ' + part.part_desc)
        					  .appendTo(part_select);
    });
}