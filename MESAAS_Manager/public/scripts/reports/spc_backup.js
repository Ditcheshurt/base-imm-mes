
var ajax_processor = './ajax_processors/reports/spc.php';
//Selected Tool ID
var sel_tool_ID = 0;
//Selected Cavity ID
var sel_cavity_ID = 0;
//All data concerning the instances selected
var cur_instances_info = [];
//Current number of instances grabbed
var cur_num_instances = 0;

var recent_data = [];
var recent_data_aggr = [];

//All data concerning the data point selected
var cur_data_point_info = {};
//Total number of parts for this data point
var cur_num_parts = 0;
//Total number of subgroups pre-defined in spc_data_points table
var cur_num_subgroups = 0;

//Part origin information
var sel_part_origin = 0;
var sel_part_origin_filter = 0;
var cur_part_origins = [];

//Variables used in calculating control limits
var cur_xbar = 0.0;
var cur_range_avg = 0.0;
var cur_std_dev_avg = 0;
var cur_avgs = [];
var cur_ranges = [];

var range_avg = 0.0;
var xbar = 0.0;
var ucl_xbar = 0.0;
var ucl_range = 0.0;
var lcl_xbar = 0.0;
var lcl_range = 0.0;
var con_range_avg = 0.0;
var con_xbar = 0.0;

//Temporary container for date
var tmp_date = 0;

//Flags to determine if a further information is required after entering data for a new instance
var xbar_comment_required = 0;
var range_comment_required = 0;

var report_instance_id = [];

//Current date
var current_date = ''; //dateFormat("mm/dd/yyyy");

//Date range for picking range of data selected
var start_date = current_date;
var end_date = current_date;

//Calculated upper and lower limits for tolerance
var tolerance_upper_limit = 0;
var tolerance_lower_limit = 0;


var systems = [];
var sel_system = {};
var sel_machine = {};
var sel_tool = {};
var sel_subtool = {};
var sel_data_point = {};
var starting_instance = 0, starting_index = 0;
var ending_instance = 0, ending_index = 0;
var current_process_data = [];


var NUM_DATA_POINTS = 5;  //THIS COULD COME FROM DATA POINT SETUP

$(document).on('ready', function() {

	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.closest('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$this.closest('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});

	$('BUTTON.btn-run').on('click', function() {
		event.preventDefault();
		run_report();
	});

	$('.btn-set-limits').on('click', function() {
		starting_instance = 0;
		ending_instance = 0;
		starting_index = 0;
		ending_index = 0;
		load_all_instances();
		$('#div_modal_set_limits').modal('show');
	});

	$('#div_sel_instances').on('click', 'BUTTON.btn-set-instance', function() {
		if (starting_instance == 0 || ending_instance != 0) {
			$('#div_sel_instances BUTTON').removeClass('starting-instance').removeClass('middle-instance').removeClass('ending-instance');
			ending_instance = 0;
			ending_index = 0;
			$('#div_calculated_values').empty();
			$('#div_process_capability').empty();

			starting_instance = $(this).data('ID');
			starting_index = $(this).data('index');
			$(this).addClass('starting-instance');
			$('#div_instance_instructions').html('Click on ending instance....');
			$('BUTTON.btn-save-limits').prop('disabled', true);
		} else {
			ending_instance = $(this).data('ID');
			ending_index = $(this).data('index');
			$(this).addClass('ending-instance');
			$('#div_instance_instructions').html('Click on Save to Store Control Limits, or click new Starting Instance.');

			//do calculations
			var ret = calclateProcessCapability();
			$('BUTTON.btn-save-limits').data(ret);
			$('#div_calculated_values').html('<b title="Estimates what the process is capable of producing if the process mean were to be centered between the specification limits. Assumes process output is approximately normally distributed.">Cp:</b> ' + ret.cp.toFixed(5) + '&nbsp;&nbsp;' +
														'<b title="Estimates what the process is capable of producing, considering that the process mean may not be centered between the specification limits. (If the process mean is not centered, Cp overestimates process capability.) Cpk < 0 if the process mean falls outside of the specification limits. Assumes process output is approximately normally distributed.">Cpk:</b> ' + ret.cpk.toFixed(5) + '&nbsp;&nbsp;' +
														'<b>Pp:</b> ' + ret.pp.toFixed(5) + '&nbsp;&nbsp;' +
														'<b>Ppk:</b> ' + ret.ppk.toFixed(5));

			var t = $('<TABLE />').css('width', '100%');
			var r = $('<TR />').appendTo(t);
			$('<td />').appendTo(r).html('<b>Ra:</b> ' + ret.final_avg_range.toFixed(6));
			$('<td />').appendTo(r).html('<b>Mean:</b> ' + ret.x_dbl_bar.toFixed(6));
			$('<td />').appendTo(r).html('<b>UCL-Xbar: </b>' + ret.tmp_ucl_xbar.toFixed(6));
			$('<td />').appendTo(r).html('<b>LCL-Xbar:</b> ' + ret.tmp_lcl_xbar.toFixed(6));
			$('<td />').appendTo(r).html('<b>UCL-Range:</b> ' + ret.tmp_ucl_range.toFixed(6));
			$('<td />').appendTo(r).html('<b>LCL-Range:</b> ' + ret.tmp_lcl_range.toFixed(6));
			$('#div_process_capability').empty().append(t);

			var found_start = false;
			$('#div_sel_instances BUTTON').each(function(i, el) {
				if ($(el).hasClass('ending-instance')) {
					return false;
				}
				if (found_start) {
					$(el).addClass('middle-instance');
				}
				if ($(el).hasClass('starting-instance')) {
					found_start = true;
				}
			});

			$('BUTTON.btn-save-limits').prop('disabled', false);
		}
	});

	$('BUTTON.btn-save-limits').on('click', function() {
		var d = $(this).data();
		d.action = "save_control_limits";
		d.auto_gauge = sel_data_point.auto_gauge;
		d.data_point_ID = sel_data_point.ID;
		d.database = sel_system.database_name;

		//set option data, in case they run the report again
		$('#datapoint_select OPTION:selected').first()
			.data('xbar_ucl', d.tmp_ucl_xbar)
			.data('xbar_lcl', d.tmp_lcl_xbar)
			.data('range_ucl', d.tmp_ucl_range)
			.data('range_lcl', d.tmp_lcl_range);

		$.getJSON(ajax_processor, d, function() {
			$.growl("Control limits saved!", {"type":"success"});
			$('#div_modal_set_limits').modal('hide');
			/*var t = $('<TABLE />').css('width', '100%');
			var r = $('<tr />').appendTo(t);
			$('<td />').appendTo(r).html('<b>UCL-Xbar: </b>' + d.tmp_ucl_xbar.toFixed(6));
			$('<td />').appendTo(r).html('<b>LCL-Xbar:</b> ' + d.tmp_lcl_xbar.toFixed(6));
			$('<td />').appendTo(r).html('<b>UCL-Range:</b> ' + d.tmp_ucl_range.toFixed(6));
			$('<td />').appendTo(r).html('<b>LCL-Range:</b> ' + d.tmp_lcl_range.toFixed(6));
			$('#td_process_capability').empty().html(t);*/
			run_report();
		});

	});

	$('#system_select').on('change', function() {
		sel_system = $('#system_select OPTION:selected').data();
		load_machines({"system_ID":sel_system.ID, "database":sel_system.database_name}, function(data) {
			show_machines(data);
			$('#machine_select').prop('disabled', false);
			if ($('#machine_select OPTION').length > 0) {
				sel_machine = $('#machine_select OPTION').first().data();
				load_tools({"machine_ID":sel_machine.ID}, function(data) {
					show_tools(data);
					if ($('#tool_select OPTION').length > 0) {
						sel_tool = $('#tool_select OPTION').first().data();
						load_subtools({"tool_ID":sel_tool.ID}, function(data) {
							show_subtools(data);
							if ($('#subtool_select OPTION').length > 0) {
								sel_subtool = $('#subtool_select OPTION').first().data();
								load_data_points({"tool_ID":sel_tool.ID, "subtool_ID":sel_subtool.ID}, function(data) {
									show_data_points(data);
								});
							}
						});
					}
				});
			}
		});
	});

	$('#machine_select').on('change', function() {
		sel_machine = $('#machine_select OPTION:selected').data();
		load_tools({"machine_ID":sel_machine.ID}, function(data) {
			show_tools(data);
			if ($('#tool_select OPTION').length > 0) {
				sel_tool = $('#tool_select OPTION').first().data();
				load_subtools({"tool_ID":sel_tool.ID}, function(data) {
					show_subtools(data);
					if ($('#subtool_select OPTION').length > 0) {
						sel_subtool = $('#subtool_select OPTION').first().data();
						load_data_points({"tool_ID":sel_tool.ID, "subtool_ID":sel_subtool.ID}, function(data) {
							show_data_points(data);
						});
					}
				});
			}
		});
	});

	$('#tool_select').on('change', function() {
		sel_tool = $('#tool_select OPTION:selected').data();
		load_subtools({"tool_ID":sel_tool.ID}, function(data) {
			show_subtools(data);
			if ($('#subtool_select OPTION').length > 0) {
				sel_subtool = $('#subtool_select OPTION').first().data();
				load_data_points({"tool_ID":sel_tool.ID, "subtool_ID":sel_subtool.ID}, function(data) {
					show_data_points(data);
				});
			}
		});
	});

	$('#subtool_select').on('change', function() {
		sel_subtool = $('#subtool_select OPTION:selected').data();
		load_data_points({"tool_ID":sel_tool.ID, "subtool_ID":sel_subtool.ID}, function(data) {
			show_data_points(data);
		});
	});

	$('#parts_body').on('click', '.add-comment', function() {
		$('#inp_comment').val('');
		if ($(this).data('comment')) {
			$('#inp_comment').val($(this).data('comment'));
		}
		$('#div_modal_comment').modal('show').data($(this).data());
	});

	$('#div_modal_comment').on('shown.bs.modal', function() { $('#inp_comment').focus(); });

	$('BUTTON.btn-save-comment').on('click', function() {
		$('#comment_' + $('#div_modal_comment').data('ID')).attr('data-original-title', $('#inp_comment').val()).attr('title', $('#inp_comment').val()).tooltip().removeClass('no-comment').addClass('has-comment').data('comment', $('#inp_comment').val());
		if ($('#inp_comment').val() == '') {
			$('#comment_' + $('#div_modal_comment').data('ID')).removeClass('has-comment').addClass('no-comment');
		}
		$.getJSON(ajax_processor, {
				"action":"save_comment",
				"database":sel_system.database_name,
				"auto_gauge":sel_data_point.auto_gauge,
				"record_ID":$('#div_modal_comment').data('ID'),
				"comment":$('#inp_comment').val()
			}, function() {
				$.growl('Comment saved!', {type: 'success'});
				$('#div_modal_comment').modal('hide');
			}
		);
	});

	$('BUTTON.btn-export-data').on('click', function() {
		exportData();
	});

	$('BUTTON.btn-set-target').on('click', function() {
		$('#inp_target').val(sel_data_point.tolerance_target);
		$('#inp_tolerance').val(sel_data_point.tolerance_delta);
		$('#div_modal_target').modal('show');
	});

	$('BUTTON.btn-save-target').on('click', function() {

		if (isNaN($('#inp_target').val())) {
			$('#inp_target').closest('DIV.form-group').addClass('has-error');
			return;
		} else {
			$('#inp_target').closest('DIV.form-group').removeClass('has-error');
		}

		if (isNaN($('#inp_tolerance').val())) {
			$('#inp_tolerance').closest('DIV.form-group').addClass('has-error');
			return;
		} else {
			$('#inp_tolerance').closest('DIV.form-group').removeClass('has-error');
		}

		sel_data_point.tolerance_target = parseFloat($('#inp_target').val());
		sel_data_point.tolerance_delta  = parseFloat($('#inp_tolerance').val());

		$('#datapoint_select OPTION:selected').first()
			.data('tolerance_target', sel_data_point.tolerance_target)
			.data('tolerance_delta', sel_data_point.tolerance_delta);

		$.getJSON(ajax_processor, {
				"action":"save_target",
				"database":sel_system.database_name,
				"auto_gauge":sel_data_point.auto_gauge,
				"tolerance_target":$('#inp_target').val(),
				"tolerance_delta":$('#inp_tolerance').val(),
				"data_point_ID":sel_data_point.ID
			}, function() {

					$.growl("Target saved!", {"type":"success"});
					$('#div_modal_target').modal('hide');

					run_report();
			}
		);
	});

	//kick it off
	load_systems({"process_type":"REPETITIVE"}, function(data) {
		systems = data;
		show_systems(data);

		if (data.length > 0) {
			sel_system = data[0];
			load_machines({"system_ID":sel_system.ID, "database":sel_system.database_name}, function(data) {
				show_machines(data);
				$('#machine_select').prop('disabled', false);
				if ($('#machine_select OPTION').length > 0) {
					sel_machine = $('#machine_select OPTION').first().data();
					load_tools({"machine_ID":sel_machine.ID}, function(data) {
						show_tools(data);
						if ($('#tool_select OPTION').length > 0) {
							sel_tool = $('#tool_select OPTION').first().data();
							load_subtools({"tool_ID":sel_tool.ID}, function(data) {
								show_subtools(data);
								if ($('#subtool_select OPTION').length > 0) {
									sel_subtool = $('#subtool_select OPTION').first().data();
									load_data_points({"tool_ID":sel_tool.ID, "subtool_ID":sel_subtool.ID}, function(data) {
										show_data_points(data);
									});
								}
							});
						}
					});
				}
			});

		}
	});
});

function load_tools(params, callback) {
	if (params) {
		params.action = 'get_tools';
	} else {
		params = {action: 'get_tools'};
	}
	params.database = sel_system.database_name;

	$.getJSON(ajax_processor, params, callback);
}

function show_tools(data) {
	$('#tool_select').empty().prop('disabled', true);
	if (!data) {return;}

	$.each(data, function (i, tool) {
		var el = $('<option name="tool_id"></option>');
		el.prop('id', 'tool_'+tool.ID).val(tool.ID).html(tool.tool_description).data(tool);
		$('#tool_select').append(el).prop('disabled', false);
	});
}

function load_subtools(params, callback) {
	if (params) {
		params.action = 'get_subtools';
	} else {
		params = {action: 'get_subtools'};
	}
	params.database = sel_system.database_name;

	$.getJSON(ajax_processor, params, callback);
}

function show_subtools(data) {
	$('#subtool_select').empty().prop('disabled', true);
	if (!data) {return;}

	$.each(data, function (i, subtool) {
		var el = $('<option name="sub_tool_id"></option>');
		el.prop('id', 'subtool_'+subtool.ID).val(subtool.ID).html(subtool.part_desc).data(subtool);
		$('#subtool_select').append(el).prop('disabled', false);
	});
}

function load_data_points(params, callback) {
	if (params) {
		params.action = 'get_data_points';
	} else {
		params = {action: 'get_data_points'};
	}
	params.database = sel_system.database_name;

	$.getJSON(ajax_processor, params, callback);
}

function show_data_points(data) {
	$('#datapoint_select').empty().prop('disabled', true);
	if (!data) {return;}

	$.each(data, function (i, data_point) {
		if (sel_machine.ID.toString().substr(-2) == '45') {
			if (data_point.auto_gauge) {
				var el = $('<option name="data_point_id"></option>');
				el.prop('id', 'data_point_'+data_point.ID).val(data_point.ID).html(data_point.description + ': ' + data_point.instruction_description).data(data_point);
				$('#datapoint_select').append(el).prop('disabled', false);
			}
		} else {
			if (!data_point.auto_gauge) {
				var el = $('<option name="data_point_id"></option>');
				el.prop('id', 'data_point_'+data_point.ID).val(data_point.ID).html(data_point.description + ': ' + data_point.instruction_description).data(data_point);
				$('#datapoint_select').append(el).prop('disabled', false);
			}
		}
	});
}

function run_report() {
	var params = {"action":"run_report",
					  "database":sel_system.database_name,
					  "start_date":$('#start_date').val(),
					  "end_date":$('#end_date').val(),
					  "tool_ID":$('#tool_select').val(),
					  "subtool_ID":$('#subtool_select').val(),
					  "data_point_ID":$('#datapoint_select').val(),
					  "auto_gauge":$('#datapoint_select OPTION:selected').data('auto_gauge'),
					  "data_point_desc":$('#datapoint_select OPTION:selected').data('instruction_description')
					};
	$.getJSON(ajax_processor, params, show_report);

	$('#div_report_data').show('blind');
	$('#div_report_chart').show();
}

function show_report(data) {
	$('#div_chart_xbar').empty();
	$('#div_chart_range').empty();
	data = data.results || [];

	data = rotateData(data, NUM_DATA_POINTS);  //THIS CHANGES THE DATA FROM A 125-ELEMENT ARRAY TO 5 25-ELEMENT ARRAYS, PLUS A 25-ELEMENT ARRAY FOR THE AVERAGES, PLUS A 25-ELEMENT ARRAY FOR THE RANGES
	//console.log(data);


	$('BUTTON.btn-export-data').data('current_data', data);

	//console.log(data);
	sel_data_point = $('#datapoint_select OPTION:selected').first().data();

	$('#sp_target').empty().html(sel_data_point.tolerance_target);
	$('#sp_delta').empty().html(sel_data_point.tolerance_delta);
	$('#sp_target_range').empty().html('(' + (sel_data_point.tolerance_target - sel_data_point.tolerance_delta) + ' to ' + (sel_data_point.tolerance_target + sel_data_point.tolerance_delta) + ')');

	$('#td_process_capability').empty();
	if (sel_data_point.xbar_ucl != null) {
		var t = $('<TABLE />').css('width', '100%');
		var r = $('<tr />').appendTo(t);
		$('<td />').appendTo(r).html('<b>UCL-Xbar: </b>' + sel_data_point.xbar_ucl.toFixed(6));
		$('<td />').appendTo(r).html('<b>LCL-Xbar:</b> ' + sel_data_point.xbar_lcl.toFixed(6));
		$('<td />').appendTo(r).html('<b>UCL-Range:</b> ' + sel_data_point.range_ucl.toFixed(6));
		$('<td />').appendTo(r).html('<b>LCL-Range:</b> ' + sel_data_point.range_lcl.toFixed(6));
		$('#td_process_capability').html(t);
	}

	var tr = $('#tr_instance_numbers').empty();
	var values = [];
	var chart_series = {
		"name":"Avg",
		"data":[]
	};
	var chart_ucl = {
		"name":"UCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};
	var chart_lcl = {
		"name":"LCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};

	var chart_mean = {
		"name":"Mean",
		"data":[],
		"dashStyle": "Dash",
		"color": "black",
		"marker": {
			"enabled":false
		}
	};

	var chart_range_series = {
		"name":"Range",
		"data":[]
	};

	var chart_range_ucl = {
		"name":"UCL",
		"data":[],
		"dashStyle": "Dash",
		"color": "orange",
		"marker": {
			"enabled":false
		}
	};

	var chart_range_mean = {
		"name":"Mean",
		"data":[],
		"dashStyle": "Dash",
		"color": "black",
		"marker": {
			"enabled":false
		}
	};

	var ranges = [];
	var xBars  = [];
	var dp_names = [];
	var split_date = '';
	var time_split = '';
	var total = 0.0;

	$('.span_all').attr('colSpan', data[0].length);
//
//	$(data).each(function(i, el) {
//		if (i == 0) {
//			for (var j = 0; j < el.length; j++) {
//				var th =
//		/*		var tip = 'Cycle time: ' + el.cycle_time.date;
//				dp_names.push(el.cycle_time.date);
//				if (el.serial_number != '') {
//					tip += '<br>Serial: ' + el.serial_number;
//				}
//				var sp = $('<span />').addClass('glyphicon glyphicon-info-sign tt').attr('title', tip).appendTo(th);*/
//				tr.append(th);
//			}
//		}
//	});


	for (var i = 0; i < data[NUM_DATA_POINTS + 4].length; i++) {

		$('<th />').html(i+1).appendTo(tr);

		dp_names.push(data[NUM_DATA_POINTS + 4][i]);
	}

	var tb = $('#parts_body').empty();
	//LOOP THROUGH EACH SET OF DATA POINTS
	for (var i = 0; i < NUM_DATA_POINTS; i++) {
		var tr = $('<tr />').appendTo(tb);
		var td = $('<td />').appendTo(tr).html(i + 1);
		var data_row = data[i];

		for (var j = 0; j < data_row.length; j++) {
			var dp = data_row[j];  //the data point record
			var comment_btn = $('<div />').css('font-size', '7pt').addClass('btn btn-xs btn-default glyphicon glyphicon-comment add-comment no-comment').data(dp).prop('id', 'comment_' + dp.ID);
			if (dp.comment) {
				if (dp.comment != null && dp.comment != '') {
					comment_btn.addClass('has-comment').removeClass('no-comment').attr('title', dp.comment).tooltip();
				}
			}
			$('<td />').html('<div class="tt" title="' + dp.cycle_time.date + ' Serial: ' + dp.serial_number + '">' + dp.data_point_value + '</div>').append(comment_btn).appendTo(tr);
		}
	}

	//MIN
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('MIN').css('border-top', '2px solid black');
	for (var i = 0; i < data[NUM_DATA_POINTS+2].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+2][i]).appendTo(tr).css('border-top', '2px solid black');
	}

	//MAX
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('MAX');
	for (var i = 0; i < data[NUM_DATA_POINTS+3].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+3][i]).appendTo(tr);
	}

	//RANGE
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('Range');
	var total_of_ranges = 0.0;
	for (var i = 0; i < data[NUM_DATA_POINTS+1].length; i++) {
		var range_val = data[NUM_DATA_POINTS+1][i];
		$('<td />').html(range_val.toFixed(6)).appendTo(tr).addClass(getAlertClassRange(range_val));
		total_of_ranges += range_val;

		var point = {"y":range_val};
		if (parseFloat(sel_data_point.range_ucl) < range_val) {
			point.marker = {"fillColor": "red"};
			point.color = "red";
			//if (cur_instances_info[j].instance_comment != null)
			point.name = ''; //cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name;
				}
			};
		} else if (parseFloat(sel_data_point.range_lcl) > range_val) {
			point.marker = {"fillColor": "red"};
			point.color = "red";
			//if (cur_instances_info[j].instance_comment != null)
			point.name = ''; //cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name;
				}
			};
		}

		chart_range_series.data.push(point);
		chart_range_ucl.data.push(sel_data_point.range_ucl);
	}
	var average_of_ranges = total_of_ranges / data[NUM_DATA_POINTS+1].length;
	$('<span />').addClass('glyphicon glyphicon-info-sign tt').attr('title', 'Average of ranges: ' + average_of_ranges).appendTo(td);


	//AVERAGES
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('xBar');
	var total_of_averages = 0.0;
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		var avg_val = data[NUM_DATA_POINTS][i];
		$('<td />').html(avg_val.toFixed(6)).appendTo(tr).addClass(getAlertClassXBAR(avg_val));
		total_of_averages += avg_val;
		var point = {
			"y":avg_val
		};

		if (parseFloat(avg_val) > sel_data_point.xbar_ucl) {
			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = '';
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};

		} else if (parseFloat(avg_val) < sel_data_point.xbar_lcl) {
			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = '';
			point.dataLabels = {
				"align": "left",
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};
		}
		chart_series.data.push(point);
		chart_ucl.data.push(sel_data_point.xbar_ucl);
		chart_lcl.data.push(sel_data_point.xbar_lcl);
	}
	var average_of_averages = total_of_averages / data[NUM_DATA_POINTS].length;
	$('<span />').addClass('glyphicon glyphicon-info-sign tt').attr('title', 'Average of averages: ' + average_of_averages).appendTo(td);


	//Standard Deviation
	var tr = $('<tr />').appendTo(tb);
	var td = $('<td />').appendTo(tr).html('&sigma;');
	for (var i = 0; i < data[0].length; i++) {
		$('<td />').html(data[NUM_DATA_POINTS+6][i].toFixed(6)).appendTo(tr);
	}


	//charting starts here!
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		chart_mean.data.push(average_of_averages);
	}
	for (var i = 0; i < data[NUM_DATA_POINTS].length; i++) {
		chart_range_mean.data.push(average_of_ranges);
	}


	var label = '<strong>Mean xBar</strong>: ' + average_of_averages.toFixed(6) + ' <strong>UCL-xBar</strong>: ' + sel_data_point.xbar_ucl.toFixed(6) +
					' <strong>LCL-xBar</strong>: ' + sel_data_point.xbar_lcl.toFixed(6);

	console.log('dp_names:');
	console.log(dp_names);
	console.log('chart_series.data:');
	console.log(chart_series.data);

	var chart_xbar = new Highcharts.Chart({
			"chart": {
				"renderTo": "div_chart_xbar",
				"type": "line",
				"borderWidth":1,
				"marginBottom": 130,
				"borderRadius":5
			},
			"title": {
				"margin": 25,
				"text":"SPC Xbar Chart: " + sel_data_point.instruction_description
			},
			"subtitle": {
				"text":label,
				"useHTML":true
			},
			"xAxis": {
				"categories": dp_names,
				"labels": {
					"rotation": 45,
					"y": 10
				},
				"max": data[0].length - 1,
				"maxPadding": 0.2,
				"minPadding": 0.2
			},
			"yAxis": {
				"title": {
					"text": 'mm'
				}
			},
			"tooltip": {
				"crosshairs": [true, true]
			},
			"plotOptions":{
				"line":{
					"marker":{
						"symbol":"circle"
					}
				},
				"series":{
					"lineWidth":3,
					"color":"rgb(69, 114, 167)"
				}
			},
			"legend": {
				"align": 'right',
				"verticalAlign": 'top',
				"backgroundColor":"white",
				"x": -10,
				"y": 40,
				"shadow": true,
			},
			"series": [chart_ucl, chart_lcl, chart_mean, chart_series],
			"credits": {
				"text": "Nysus",
				"href": "http://www.nysus.net"
			}
		});
	label = '<strong>Mean Range</strong>: ' + average_of_ranges.toFixed(6) + ' <strong>Range UCL</strong>: ' + sel_data_point.xbar_ucl.toFixed(6);

	console.log(chart_range_series);

	var chart_range = new Highcharts.Chart({
			"chart": {
				"renderTo": "div_chart_range",
				"type": "line",
				"borderWidth":1,
				"marginBottom": 130,
				"borderRadius":5
			},
			"title": {
				"margin": 25,
				"text":"SPC Range Chart: " + sel_data_point.instruction_description
			},
			"subtitle": {
				"text":label,
				"useHTML":true
			},
			"xAxis": {
				"categories": dp_names,
				"labels": {
					"rotation": 45,
					"y": 10
				},
				"max": data[0].length - 1,
				"maxPadding": 0.2,
				"minPadding": 0.2
			},
			"yAxis": {
				"title": {
					"text": 'mm'
				}
			},
			"tooltip": {
				"crosshairs": [true, true]
			},
			"plotOptions":{
				"line":{
					"marker":{
						"symbol":"circle"
					}
				},
				"series":{
					"lineWidth":3,
					"color":"rgb(69, 114, 167)"
				}
			},
			"legend": {
				"align": 'right',
				"verticalAlign": 'top',
				"backgroundColor":"white",
				"x": -10,
				"y": 40,
				"shadow": true,
			},
			"series": [chart_range_ucl, chart_range_series, chart_range_mean],
			"credits": {
				"text": "Nysus",
				"href": "http://www.nysus.net"
			}
		});

	$('#spc_table .tt').tooltip({"html":true});
}

function getAlertClassXBAR(val) {
	if (val > (sel_data_point.tolerance_target + sel_data_point.tolerance_delta)) { return 'over_tol';	}
	if (val < (sel_data_point.tolerance_target - sel_data_point.tolerance_delta)) { return 'under_tol';	}
	if (val > sel_data_point.xbar_ucl) { return 'over_xbar_ucl'; }
	if (val < sel_data_point.xbar_lcl) { return 'under_xbar_lcl'; }
	return '';
}

function getAlertClassRange(val) {
	if (val > sel_data_point.range_ucl) { return 'over_range_ucl'; }
	if (val < sel_data_point.range_lcl) { return 'under_range_lcl'; }
	return '';
}

function load_all_instances() {
	$('#div_sel_instances').html('Loading instances...');
	$.getJSON(ajax_processor, {
			"action":"run_report",
			"database":sel_system.database_name,
			"tool_ID":sel_tool.ID,
			"data_point_ID":sel_data_point.ID,
			"data_point_desc":sel_data_point.instruction_description,
			"auto_gauge":sel_data_point.auto_gauge,
			"start_date":"1-1-2001",
			"end_date":"1-1-2050"
		}, show_all_instances);
}

function show_all_instances(data) {
	data = data.results || [];
	data = rotateData(data, NUM_DATA_POINTS);
	current_process_data = data;
	var d = $('#div_sel_instances').empty();
	for (var i = 0; i < data[0].length; i++) {
		$('<button />').addClass('btn btn-xs btn-primary btn-set-instance')
			.css('padding', '5px 10px').css('margin', '10px')
			.html(i + 1)
			.prop('title', 'From: ' + data[NUM_DATA_POINTS + 4][i])
			.data('index', i)
			.appendTo(d);
	}

	$('#div_sel_instances SPAN.btn-set-instance').tooltip();
	$('#div_instance_instructions').html('Click on starting instance....');
}

function rotateData(data, num_to_rotate) {
	while (data.length % num_to_rotate != 0) {
		data.splice(data.length - 1, 1);
	}	
	
	var ret = [];
	for (var i = 0; i < num_to_rotate; i++) {
		ret.push([]);
	}
	ret.push([]); //for the averages
	ret.push([]); //for the ranges
	ret.push([]); //for the mins
	ret.push([]); //for the maxs
	ret.push([]); //for the max cycle time
	ret.push([]); //for the max serial
	ret.push([]); //for the standard deviation

	for (var i = 0; i < data.length; i++) {
		var r = parseInt(data[i].Row) - 1;
		ret[r % num_to_rotate].push(data[i]);
	}

	//calculate the averages
	for (var i = 0; i < ret[0].length; i++) {
		var total = 0.0;
		var min = 99999;
		var max = -99999;
		var cycle_time = '';
		var serial = '';
		var arr = [];

		for (var j = 0; j < num_to_rotate; j++) {
			//console.log('Adding row ' + ret[j][i].Row + ' to total');
			total += parseFloat(ret[j][i].data_point_value);

			if (parseFloat(ret[j][i].data_point_value) > max) {
				max = parseFloat(ret[j][i].data_point_value);
			}
			if (parseFloat(ret[j][i].data_point_value) < min) {
				min = parseFloat(ret[j][i].data_point_value);
			}

			if (j == 0) {
				cycle_time = ret[j][i].cycle_time.date;
				serial = ret[j][i].serial_number;
			}

			arr.push(ret[j][i].data_point_value);
		}
		//console.log('Total: ' + total);
		var avg = total / num_to_rotate;
		ret[num_to_rotate].push(avg);  //add the average to the array
		ret[num_to_rotate + 1].push(max-min);  //add the range to the array
		ret[num_to_rotate + 2].push(min);  //add the min to the array
		ret[num_to_rotate + 3].push(max);  //add the max to the array
		ret[num_to_rotate + 4].push(cycle_time);  //add the first cycle_time to the array
		ret[num_to_rotate + 5].push(serial);  //add the first serial to the array
		ret[num_to_rotate + 6].push(standardDeviation(arr));  //add the standard deviation to the array
	}

	return ret;
}

function standardDeviation(values){
  var avg = average(values);

  var squareDiffs = values.map(function(value){
    var diff = value - avg;
    var sqrDiff = diff * diff;
    return sqrDiff;
  });

  var avgSquareDiff = average(squareDiffs);

  var stdDev = Math.sqrt(avgSquareDiff);
  return stdDev;
}

function average(data){
  var sum = data.reduce(function(sum, value){
    return sum + value;
  }, 0);

  var avg = sum / data.length;
  return avg;
}

function calclateProcessCapability() {
	var ret = {
		"cp":0.0,
		"cpk":0.0,
		"pp":0.0,
		"ppk":0.0
	};

	var std = 0;
	var num_parts = NUM_DATA_POINTS;
	var num_selected = ending_index - starting_index + 1;
	var instances = num_selected; // / num_parts;
	var range_sum = 0.0;
	var inst_range = [];
	var x_dbl_bar = 0.0;
	var final_avg_range = 0.0;
	var long_sum = 0;
	var counter = 0;
	var total_x = 0;
	var total_sum_avg = 0.0;
	//var last_range = current_process_data[0].data_point_value;
	var sample_size = null;

	if (NUM_DATA_POINTS > 1) {
		//get x_dbl_bar
		var total_of_averages = 0.0;
		for (var i = starting_index; i <= ending_index; i++) {
			total_of_averages += current_process_data[NUM_DATA_POINTS][i];
		}
		ret.x_dbl_bar = (total_of_averages / instances);

		for (var i = starting_index; i <= ending_index; i++) {
			range_sum += current_process_data[NUM_DATA_POINTS+1][i];

			for (var j = 0; j < NUM_DATA_POINTS; j++) {
				std = parseFloat(current_process_data[j][i].data_point_value) - parseFloat(ret.x_dbl_bar);
				std = Math.pow(std, 2);

				long_sum = long_sum + parseFloat(std);
			}
		}

		sample_size = NUM_DATA_POINTS;
	} else {
		for (var i = starting_index; i <= ending_index; i++) {
			if (i == 0) {
				range_sum = 0;
			} else {
				range_sum = range_sum + Math.abs(current_process_data[i].data_point_value - last_range);
			}

			last_range = current_process_data[i].data_point_value;
			total_sum_avg = total_sum_avg + current_process_data[i].data_point_value;
		}

		ret.x_dbl_bar = total_sum_avg / num_selected; //current_process_data.length;

		console.log('x_dbl_bar: ' + ret.x_dbl_bar);

		for (var j = starting_index; j <= ending_index; j++) {
			std = current_process_data[j].data_point_value - ret.x_dbl_bar;
			std = Math.pow(std, 2);
			long_sum = long_sum + std;
		}

		sample_size = 2;
	}

	ret.final_avg_range = (range_sum / instances);
	console.log('final_avg_range: ' + ret.final_avg_range);

	ret.tmpstd = Math.sqrt(long_sum / num_selected);
	ret.sigma = ret.final_avg_range / getD2(sample_size);

	//Cp -- Process capability; a simple and straightforward indicator of process capability
	ret.tolerance_upper_limit = sel_data_point.tolerance_target + sel_data_point.tolerance_delta;
	ret.tolerance_lower_limit = sel_data_point.tolerance_target - sel_data_point.tolerance_delta;
	ret.cp = ((parseFloat(ret.tolerance_upper_limit) - parseFloat(tolerance_lower_limit))/(6*parseFloat(ret.sigma)));
	ret.cp_u = ((parseFloat(ret.tolerance_upper_limit) - parseFloat(ret.x_dbl_bar)) / (3*parseFloat(ret.sigma)));
	ret.cp_l = ((parseFloat(ret.x_dbl_bar) - parseFloat(tolerance_lower_limit)) / (3*parseFloat(ret.sigma)));

	//Cpk -- Process Capability Index; adjustment of Cp for the effect of non-centered distribution
	if (ret.cp_u < ret.cp_l) {
		ret.cpk = ret.cp_u;
	} else {
		ret.cpk = ret.cp_l;
	}

	//Capability Ratio (Cr) indicated what proportion of tolerance is occupied by the data
	var cr = (1/ret.cp);

	//Pp -- Process Performance; a simple indivator of process performance
	ret.pp = (ret.tolerance_upper_limit - ret.tolerance_lower_limit) / (6*ret.tmpstd);
	ret.pp_u = (ret.tolerance_upper_limit - x_dbl_bar) / (3*ret.tmpstd);
	ret.pp_l = (ret.x_dbl_bar - ret.tolerance_lower_limit) / (3*ret.tmpstd);

	if (ret.pp_u < ret.pp_l) {
		ret.ppk = ret.pp_u;
	} else {
		ret.ppk = ret.pp_l;
	}

	ret.tmp_ucl_xbar = ret.x_dbl_bar + (ret.final_avg_range*getA2(sample_size));
	ret.tmp_ucl_range = ret.final_avg_range*getD4(sample_size);
	ret.tmp_lcl_xbar = ret.x_dbl_bar - (ret.final_avg_range*getA2(sample_size));
	ret.tmp_lcl_range = ret.final_avg_range*getD3(sample_size);

	console.log(ret);

	return ret;
}

function exportData() {
	var d = $('BUTTON.btn-export-data').data('current_data');
	var v = '';
//	var header = 'Instance,';
//	var dates = 'Date,';
//	var serials = 'Serial,';
//	var vals = 'Value,';
//	var comments = 'Comments,';
//	for (var i = 0; i < d.length; i++) {
//		header += (i + 1).toString();
//		dates += d[i].cycle_time.date;
//		serials += d[i].serial_number;
//		vals += d[i].data_point_value;
//		comments += (d[i].comment || '');
//		if (i < d.length - 1) {
//			header += ',';
//			dates += ',';
//			serials += ',';
//			vals += ',';
//			comments += ',';
//		}
//	}
//
//	$('#inpExport').val(header + '\n' + dates + '\n' + serials + '\n' + vals + '\n' + comments);

	var arr = ['xBar', 'Range', 'Min', 'Max', 'Max Cycle Time', 'Max Serial', 'Standard Deviation'];

	for (var i = 0; i < d.length; i++) {
		//add row info
		if (i < NUM_DATA_POINTS) {
			v += 'Part ' + (i + 1) + ':,';
		} else {
			v += arr[i - NUM_DATA_POINTS] + ',';
		}

		for (var j = 0; j < d[i].length; j++) {
			if (i < NUM_DATA_POINTS) {
				v += d[i][j].data_point_value;
			} else {
				v += d[i][j];
			}

			if (j < (d[i].length - 1)) {
				v += ',';
			}
		}

		if (i < (d.length - 1)) {
			v += '\n';
		}
	}
	$('#inpExport').val(v);
	$('#frm_export').submit();
}






//OLD STUFF FROM NYSUS.NET SPC SYSTEM:
//Kicks off the SPC application
function init() {
	loadTools();
}

//Grabs all available tools
function loadTools() {
	$('div_tools').update('Loading tools...');
	$('div_cavities').hide();
	$('div_right_side').update('<div class="instr">To start, please select a tool from the left.</div>');

	new Ajax.Request(ajax_processor, {
		parameters: {"action":"load_tools", "site_ID":site_ID},
		onSuccess: function(res) {
			showTools(res.responseJSON);
		}
	});
}

//Displays all available tools as buttons
function showTools(arr) {
	$('div_tools').update();
	$('div_tools').update(':: Tools ::');
	if (arr.length == 0) {
		$('div_tools').insert('<div class="error">There are no tools defined for this site.</div>');
		return;
	}
	for (var i = 0; i < arr.length; i++) {
		var a = new Element('A').update(arr[i].text).addClassName('tool_button').writeAttribute('tool_ID', arr[i]['tool_ID']);
		a.observe('click', function(e) {
			$$('A.tool_button').each(function(el) {el.removeClassName('selected');});
			this.addClassName('selected');
			selectTool(this.readAttribute('tool_ID'));
		});
		$('div_tools').insert(a);
	}
}

//After a tool has been selected, load cavities for that tool
function selectTool(tool_ID) {
	sel_tool_ID = tool_ID;
	$('div_cavities').show().update('Loading cavities...');
	$('div_dp').hide();

	new Ajax.Request(ajax_processor, {
		parameters: {"action":"load_cavities", "tool_ID":tool_ID},
		onSuccess: function(res) {
			showCavities(res.responseJSON);
		}
	});
}

//Displays all available cavities as buttons
function showCavities(arr) {
	$('div_cavities').update();
	$('div_right_side').update('<div class="instr">Now, select a cavity from the bottom left.</div>').removeClassName('error');
	$('div_cavities').update(':: Cavities ::');
	if (arr.length == 0) {
		$('div_cavities').insert('<div class="error">There are no cavities defined for this tool.</div>');
	}
	for (var i = 0; i < arr.length; i++) {
		var a = new Element('A').update(arr[i].text + '<br>' + arr[i].part_number).addClassName('cavity_button').writeAttribute({"cavity_ID":arr[i]['cavity_ID'], "image_file":arr[i]['image_file']});
		a.observe('click', function(e) {
			$$('A.cavity_button').each(function(el) {el.removeClassName('selected');});
			this.addClassName('selected');
			selectCavity(this.readAttribute('cavity_ID'), this.readAttribute('image_file'));
		});
		$('div_cavities').insert(a);
	}
}

//After a cavity has been selected, load all available data points
function selectCavity(cavity_ID, image_file) {
	sel_cavity_ID = cavity_ID;
	$('div_dp').show().update('Loading data points...');

	new Ajax.Request(ajax_processor, {
		parameters: {"action":"load_dp", "cavity_id":cavity_ID, "image_file":image_file},
		onSuccess: function(res) {
			showDP(res.responseJSON, res.request.parameters.image_file);
		}
	});
}

var img_new_width = 0.0;
var img_new_height = 0.0;
var img_width = 0.0;
var img_height = 0.0;
var width_ratio = 0.0;
var height_ratio = 0.0;

//Displays all available data points and shows the image of that cavity
function showDP(arr, image_file) {
	$('div_dp').update();
	$('div_right_side').update('<div class="instr">Now, select a data point from the bottom left.</div>').removeClassName('error');
	$('div_dp').update(':: Data Points ::');
	if (arr.length == 0) {
		$('div_dp').insert('<div class="error">There are no data points defined for this cavity.</div>');
		return;
	}

	if (image_file != null) {
		if (image_file != '') {

			try {

				var d_img = new Element('DIV');
				d_img.style.position = 'relative';
				d_img.style.width = '100%';
				var img_el = new Element('IMG');
				d_img.update(img_el);
				$('div_dp').insert(d_img);
				img_el.src = '/UPLOADS/SPC/' + company_ID + '/' + sel_cavity_ID + '~~' + image_file;
				img_el.style.width = '100%';

				img_el.onload = function() {

					img_new_width = $(img_el).getDimensions().width;
					img_new_height = $(img_el).getDimensions().height;

				}

				var img = new Element("img", { id: "big_img", width: "300px" });
				$(img).setStyle({ display: "none" });
				img.src = $(img_el).readAttribute("src");

				d_img.insert(img);

				img.onload = function() {

					img_width = $(img).getDimensions().width;
					img_height = $(img).getDimensions().height;

				}

				var d_plot = new Element('DIV', {"id":"div_plot"}).setStyle({"position":"absolute", "display":"none", "backgroundColor":"limegreen", "width":"5px", "height":"5px"});
				d_img.insert(d_plot);
				var d_plot2 = new Element('DIV', {"id":"div_plot2"}).setStyle({"position":"absolute", "display":"none", "backgroundColor":"#CC0066", "width":"5px", "height":"5px"});
				d_img.insert(d_plot2);

			} catch (e) { alert(e.message); }
		}
	}

	var width = 0.0;
	var height = 0.0;

	for (var i = 0; i < arr.length; i++) {
		var a = new Element('A')
					.update(arr[i].data_point_name + '<br>' + arr[i].num_subgroups + ' Max Subgroups ' + arr[i].num_parts + ' Parts')
					.addClassName('dp_button')
					.writeAttribute({"dp_id":arr[i]['id'], "num_parts": arr[i].num_parts, "plot_location":arr[i]['plot_location']});

		a.observe('click', function(e) {

			$$('A.dp_button').each(function(el) {el.removeClassName('selected');});

			this.addClassName('selected');
			selectDP(this.readAttribute('dp_ID'), this.readAttribute('num_parts'));

			var a = this.readAttribute('plot_location');

			if (a != null) {
				if (a != '') {

					a = a.split(',');

					width_ratio = (parseInt(img_width) / parseInt(img_new_width));
					height_ratio = (parseInt(img_height) / parseInt(img_new_height));

					width = parseInt(a[0]) / width_ratio;
					height = parseInt(a[1]) / height_ratio;

					$('div_plot2').setStyle({"left":(width + 'px'), "top":(height + 'px')}).show();

				}
			}
		});

		a.observe('mouseover', function (e) {
			var a = this.readAttribute('plot_location');
			if (a != null) {
				if (a != '') {

					a = a.split(',');

					width_ratio = (parseInt(img_width) / parseInt(img_new_width));
					height_ratio = (parseInt(img_height) / parseInt(img_new_height));

					width = parseInt(a[0]) / width_ratio;
					height = parseInt(a[1]) / height_ratio;

					$('div_plot').setStyle({"left":(width + 'px'), "top":(height + 'px')}).show();
				}
			}
		});

		$('div_dp').insert(a);
	}
}

//After a data point has been chosen, load the data
function selectDP(dp_ID, num_parts) {
	sel_dp_id = dp_ID;
	start_date = current_date;
	end_date = current_date;

	load_data(dp_ID, num_parts, [], null);

}

//optional values_entered array will be all values of new data points
function load_data(dp_ID, num_parts, values_entered, new_save_data) {
	xbar_comment_required = 0;
	range_comment_required = 0;

	var el = $('sel_last_top');
	if (el) {
		last_top = $F('sel_last_top');
	}

	$('div_right_side').update('Loading data points...');

	if (values_entered.length == 0) {
		values_entered = '';
	}

	var control_limit_update_instance_ID = 0;
	if (new_save_data != null) {
		if (new_save_data.do_control_limit_calc) {
			control_limit_update_instance_ID = new_save_data.new_instance_ID;
		}
	}

	new Ajax.Request(ajax_processor, {
		parameters: {
			"action":"load_data",
			"site_ID":site_ID,
			"dp_ID":dp_ID,
			"num_parts": num_parts,
			"last_top":last_top,
			"start_date": start_date,
			"end_date":end_date,
			"sel_part_origin":sel_part_origin_filter,
			"control_limit_update_instance_ID":control_limit_update_instance_ID,
			"values_entered":values_entered
		},
		onSuccess: function(res) {
			showDataNew(res.responseJSON, res.request.parameters.values_entered, res.request.parameters.control_limit_update_instance_ID);
		}
	});
}

function showDataNew(arr, values_entered, control_limit_update_instance_ID) {
	try {
	cur_instances_info = arr.instances_info;  //last (# subgroups instances)
	cur_num_instances = cur_instances_info.length; //entered number of instances

	cur_data_point_info = arr.data_point_info[0];

	cur_num_parts = cur_data_point_info.num_parts;
	cur_num_subgroups = cur_data_point_info.num_subgroups; //predefined from table

	recent_data = arr.recent_crosstab;

	if (arr.recent_aggr != null)
		recent_data_aggr = arr.recent_aggr[0];

	cur_part_origins = arr.part_origin_info;

	//If date contains '/' instead of '-' it converts it to '-' for the date input field
	if (start_date.match("/")) {

		tmp_start = start_date.split("/");
		tmp_end = end_date.split("/");

		start_date = tmp_start[2] + '-' + tmp_start[0] + '-' + tmp_start[1];
		end_date = tmp_end[2] + '-' + tmp_end[0] + '-' + tmp_end[1];

	}

	//Blue header at top of SPC application
	$('div_right_side').update('<div id="div_top_info"><strong>Cavity:</strong> ' + cur_data_point_info.cavity_description + ' <strong>Data Point:</strong> ' + cur_data_point_info.data_point_name + ' <strong>Target:</strong> ' +
										cur_data_point_info.tolerance_target + ' +/-' + cur_data_point_info.tolerance_delta + '</div>');

	//Error checking for # of parts/subgroups
	if (cur_num_parts == 0 || cur_num_subgroups == 0) {
		$('div_right_side').update('Number of parts or number of subgroups defined for selected data point is 0.  Please correct in setup.').addClassName('error');
		return;
	} else {
		$('div_right_side').removeClassName('error');
	}

	//Div that contains most data
	var d = new Element('DIV');
	$('div_right_side').insert(d);

	//Start and end date forms
	d.update('Show data from: ');

	var inp_start = new Element('INPUT', {"id":"inp_start_date", "type":"date", "size":10});
	inp_start.value = start_date;
	d.insert(inp_start);

	d.insert(' to ');

	var inp_end = new Element('INPUT', {"id":"inp_end_date", "type":"date", "size":10});
	inp_end.value = end_date;
	d.insert(inp_end);

	//Selecting data from a part origin ex: data from press 1, press 2, etc..
	d.insert('&nbsp;Filter part origin: ');

	var sel_origin = new Element('SELECT', {"id":"sel_part_origin_filter"});
	var op = new Option('No Filter', 0);
	sel_origin.options.add(op);
	for (var i = 0; i < cur_part_origins.length; i++) {
		var op = new Option(cur_part_origins[i].origin_description, cur_part_origins[i].ID);
		if (cur_part_origins[i].ID == sel_part_origin_filter) {
			op.selected = true;
		}
		sel_origin.options.add(op);
	}
	d.insert(sel_origin);

	//GO button for press filter
	var b = new Element('BUTTON');
	b.update('Go');
	b.onclick = function() {
		if (Date.IsDate($F('inp_start_date')) && Date.IsDate($F('inp_end_date'))) {
			start_date = $F('inp_start_date');
			end_date = $F('inp_end_date');
			sel_part_origin_filter = $F('sel_part_origin_filter');
			load_data(cur_data_point_info.ID, cur_num_parts, [], null);
		} else {
			if (!Date.IsDate($F('inp_start_date'))) $('inp_start_date').setStyle({"border":"1px solid red"});
				else $('inp_end_date').setStyle({"border":"1px solid red"})
		}
	};
	d.insert(b);

	//Full Instance View link
	d.insert('<a href="#" class="save_button" id="instance_popup_link" onclick="showInstancesPopup(this, \'' + cur_data_point_info.ID + '\');">Select Instances for Control Limit Calucation</a>');

	//Error Report spreadsheet link
	d.insert('<a href="#" class="save_button" id="export_report" onclick="showErrorReport();">Error Report</a> ');

	var part_origin = $F('sel_part_origin_filter');
	var ignore_control_alerts = -1;

	//Table containing raw SPC data
	var tbl = new Element('TABLE');
	tbl.id = 'tbl_data';
	tbl.width = '100%';

	//AJAX call to grab saved constant control limit calculations or the first 25 instances if no calculations are saved
	//Also prints the data into a row
	new Ajax.Request(ajax_processor, {
		parameters: {
			"action":"calc_control_limits",
			"data_point_ID": cur_data_point_info.ID
		},
		onSuccess: function(res) {

			var row = tbl.insertRow(-1);
			row.style.fontWeight = 'bold';

			var column = row.insertCell(-1);
			column.innerHTML = '&nbsp;';

			var column = row.insertCell(-1);
			column.innerHTML = '<i>Add New</i>';

			//Subgroup header
			var column = row.insertCell(-1);
			column.innerHTML = 'Subgroups ';
			column.colSpan = cur_num_subgroups;

			//Table header
			var row = tbl.insertRow(-1);
			row.style.fontWeight = 'bold';
			row.id = 'subgroup_row';
			var column = row.insertCell(-1);
			column.innerHTML = 'Part';

			var newCol = Element.extend(row.insertCell(-1));
			newCol.update('Part Origin:<br>');
			var sel_new_origin = new Element('SELECT', {"id":"new_part_origin"});

			for (var i = 0; i < cur_part_origins.length; i++) {
				var op = new Option(cur_part_origins[i].origin_description, cur_part_origins[i].ID);
				if (cur_part_origins[i].ID == sel_part_origin) {
					op.selected = true;
				}
				sel_new_origin.options.add(op);
			}

			//Sets global variable for press origin
			sel_new_origin.observe('change', function(event) {
				sel_part_origin = this.options[this.selectedIndex].value;
			});

			newCol.insert(sel_new_origin);

			//instance numbers at top of data table
			for (var i = 0; i < cur_num_instances; i++) {
				var column = row.insertCell(2);
				column.writeAttribute({"instance_id":cur_instances_info[i].ID});
				column.innerHTML = cur_num_instances - i;
				column.title = cur_instances_info[i].instance_time + '\nFrom: ' + cur_instances_info[i].origin_description + '\nSequence: ' + cur_instances_info[i].instance_sequence;

					if (cur_instances_info[i].instance_comment != null) {
						column.title += '\nComment: ' + cur_instances_info[i].instance_comment;
					}
			}

			//subgroups 1->25
			for (var x = cur_num_instances; x < cur_num_subgroups; x++) {
				var col = row.insertCell(-1);
				col.innerHTML = (x+1);
				col.title = 'Not yet entered';
			}

			//Data input for SPC
			for (var j = 0; j < cur_num_parts; j++) {

				var row = tbl.insertRow(-1);
				var column = row.insertCell(-1);
				column.innerHTML = 'Part #' + (j + 1);

				var addCol = row.insertCell(-1);
				addCol.innerHTML = '<input type="text" id="new_' + (j+1) + '" style="width:30px;">';

				//recent_crosstab data
				for (var x = 0; x < cur_num_instances; x++) {
					var col = Element.extend(row.insertCell(2));
					if (j < recent_data.length) {
						col.innerHTML = recent_data[j]['VALUE_' + (x + 1)];
						col.id = 'IP_ID_' + recent_data[j]['IP_ID_' + (x + 1)];
						col.writeAttribute({"part_sequence":(j+1), "instance_ID":cur_instances_info[x].ID});

						col.className = 'edit_cell';
						col.title = 'Click to Edit.\nPress Enter to save';
					}
				}

				for (var x = cur_num_instances; x < cur_num_subgroups; x++) {
					var col = row.insertCell(-1);
					col.innerHTML = '&nbsp;';
					col.title = 'Not yet entered';
				}

			}

			//Insert row for date
			var place_date = '';

			var ro = tbl.insertRow(-1);
			var co = ro.insertCell(-1);
			co.innerHTML = 'Date';
			co.colsSpan = 2;

			if (tmp_date == 0) {
				tmp_date = dateFormat("mm/dd/yyyy h:MM:ss TT");
			}

			co = ro.insertCell(-1);
			co.innerHTML = '<input type="text" id="new_instance_date" value="' + tmp_date + '" />';

			//save row
			var botRow = tbl.insertRow(-1);
			var botCol = botRow.insertCell(-1);
			botCol.innerHTML = '&nbsp;';

			var botCol = botRow.insertCell(-1);
			var a = new Element('A');
			a.update('SAVE');
			a.addClassName('save_button');
			a.observe('click', function(e) {
				saveNewInstance($F('new_instance_date'));
			});
			Element.extend(botCol).update(a);

			var botCol = botRow.insertCell(-1);

			 botCol.innerHTML = '<span style="background-color:#FF9966;padding:1px 4px;margin:0px 10px;">Over Tolerance</span>' +
			                    '<span style="background-color:#6666FF;padding:1px 4px;margin:0px 10px;">Under Tolerance</span>' +
			                    '<span style="background-color:orange;padding:1px 4px;margin:0px 10px;">Over xBar UCL</span>' +
								'<span style="background-color:yellow;padding:1px 4px;margin:0px 10px;">Under xBar LCL</span>' +
								'<span style="background-color:red;padding:1px 4px;margin:0px 10px;">Over Range UCL</span>' +
								'<span style="background-color:#CC00FF;padding:1px 4px;margin:0px 10px;">Under Range LCL</span>';
			botCol.colSpan = cur_num_subgroups;

			if (cur_num_parts == 1) {
				//insert row for range
				var r = tbl.insertRow(-1);
				r.id = 'row_range';
				var c = r.insertCell(-1);
				var range_total = 0;
				cur_ranges = [];
				c.innerHTML = 'Range';
				c.colSpan = 2;
				var range = 0.0;
				var last_range = 0.0;

				var i = cur_num_instances;
				while (i--) {

					var c = r.insertCell(-1);

					if (i == cur_num_instances-1) {
						range = 0.0;
					} else {
						range = Math.abs(parseFloat(recent_data[0]['VALUE_' + (i+1)]) - last_range).toFixed(2);
					}

					last_range = parseFloat(recent_data[0]['VALUE_' + (i+1)]);

					c.id = 'range_' + i;
					c.innerHTML = range;
					c.className = 'edit_cell';
					c.title = 'Click to comment';
					c.writeAttribute({"instance_ID":cur_instances_info[i].ID});

					range_total += range;
					cur_ranges.push(range);

				}

				cur_range_avg = range_total/cur_num_instances;

				//insert row for xBar
				var xbarow = tbl.insertRow(-1);
				xbarow.id = 'xbar_row';

				var c = xbarow.insertCell(-1);
				var sum_total = 0.0;
				var avg = 0.0;
				cur_avgs = [];
				c.innerHTML = 'xBar';
				c.colSpan = 2;

				var cur_dir = '';
				var last_val = -1;
				var num_trending = 0;

				//var tmp_count = 1;
				i = cur_num_instances;
				while (i--) {
					var c = xbarow.insertCell(-1);

					/*if (i == cur_num_instances-1) {
						sum_total = parseFloat(recent_data[0]['VALUE_' + (i+1)]);
						avg = sum_total;
					} else {
						sum_total = sum_total + parseFloat(recent_data[0]['VALUE_' + (i+1)]);
						avg = sum_total / parseFloat(tmp_count);
					}*/

					//tmp_count++;

					avg = parseFloat(recent_data[0]['VALUE_' + (i+1)]);

					c.id = 'xbar_' + i;
					c.innerHTML = avg;
					c.className = 'edit_cell';
					c.title =  'Click to comment';
					c.writeAttribute({"instance_ID":cur_instances_info[i].ID});

					avg_total += avg;
					cur_avgs.push(avg);

					if (avg > last_val) {
						if (cur_dir == 'incr') {
							num_trending++;
						} else {
							num_trending = 0;
						}
						cur_dir = 'incr';
					}

					if (avg == last_val) {
						cur_dir = 'same';
						num_trending = 0;
					}

					if (avg < last_val) {
						if (cur_dir == 'decr') {
							num_trending++;
						} else {
							num_trending = 0;
						}
						cur_dir = 'decr';
					}

					if (num_trending >= 7) {
						c.style.border = '4px solid red';
						for (var j = 0; j < 7; j++) {
							xbarow.cells[j + 2].style.border = '4px solid red';
						}
					}

					last_val = avg;
				}

				cur_xbar = avg_total/cur_num_instances;

			} else {

				//insert row for max
				var r = tbl.insertRow(-1);
				var c = r.insertCell(-1);
				c.innerHTML = 'MAX';
				c.colSpan = 2;

				for (var i = 0; i < cur_num_instances; i++) {
					var c = r.insertCell(1);
					c.style.borderTop = '1px solid black';
					c.innerHTML = recent_data_aggr['max_' + cur_instances_info[i].ID].toFixed(2);
				}

				//insert row for min
				var r = tbl.insertRow(-1);
				var c = r.insertCell(-1);
				c.innerHTML = 'MIN';
				c.colSpan = 2;

				for (var i = 0; i < cur_num_instances; i++) {
					var c = r.insertCell(1);
					c.innerHTML = recent_data_aggr['min_' + cur_instances_info[i].ID].toFixed(2);
				}

				//insert row for range
				var r = tbl.insertRow(-1);
				r.id = 'row_range';
				var c = r.insertCell(-1);
				var range_total = 0;
				cur_ranges = [];
				c.innerHTML = 'Range';
				c.colSpan = 2;

				for (var i = 0; i < cur_num_instances; i++) {
					var c = r.insertCell(1);
					var range = recent_data_aggr['max_' + cur_instances_info[i].ID] - recent_data_aggr['min_' + cur_instances_info[i].ID];

					c.id = 'range_' + i;
					c.innerHTML = range.toFixed(2);
					c.className = 'edit_cell';
					c.title = 'Click to comment';
					c.writeAttribute({"instance_ID":cur_instances_info[i].ID});

					range_total += range;
					cur_ranges.push(range);
				}

				cur_range_avg = range_total/cur_num_instances;

				//insert row for xBar
				var xbarow = tbl.insertRow(-1);
				xbarow.id = 'xbar_row';

				var c = xbarow.insertCell(-1);
				var avg_total = 0;
				cur_avgs = [];
				c.innerHTML = 'xBar';
				c.colSpan = 2;

				var cur_dir = '';
				var last_val = -1;
				var num_trending = 0;

				for (var i = 0; i < cur_num_instances; i++) {
					var c = xbarow.insertCell(1);
					var avg = parseFloat(recent_data_aggr['avg_' + cur_instances_info[i].ID]);
					c.id = 'xbar_' + i;
					c.innerHTML = roundIt(avg);
					c.className = 'edit_cell';
					c.title =  'Click to comment';
					c.writeAttribute({"instance_ID":cur_instances_info[i].ID});

					avg_total += avg;
					cur_avgs.push(avg);

					if (avg > last_val) {
						if (cur_dir == 'incr') {
							num_trending++;
						} else {
							num_trending = 0;
						}
						cur_dir = 'incr';
					}

					if (avg == last_val) {
						cur_dir = 'same';
						num_trending = 0;
					}

					if (avg < last_val) {
						if (cur_dir == 'decr') {
							num_trending++;
						} else {
							num_trending = 0;
						}
						cur_dir = 'decr';
					}

					if (num_trending >= 7) {
						c.style.border = '4px solid red';
						for (var j = 0; j < 7; j++) {
							xbarow.cells[j + 2].style.border = '4px solid red';
						}
					}

					last_val = avg;
				}
				cur_xbar = avg_total/cur_num_instances;

				//insert row for std dev
				var r = tbl.insertRow(-1);
				var c = r.insertCell(-1);
				c.innerHTML = '&sigma;';
				var std_dev_total = 0;
				c.colSpan = 2;

				for (var i = 0; i < cur_num_instances; i++) {
					var c = r.insertCell(1);
					var std_dev = parseFloat(recent_data_aggr['stdev_' + cur_instances_info[i].ID]);
					c.innerHTML = roundIt(std_dev);
					std_dev_total += std_dev;
				}

				cur_std_dev_avg = std_dev_total/cur_num_instances;

			}


		//END SPLIT HERE

			var jso = res.responseJSON;

			if (jso[0].instance_id_range == 0) {
				ignore_control_alerts = 1;
			} else {
				ignore_control_alerts = 0;
			}

			/*
			//control limit calculations
			cur_ucl_xbar = cur_xbar + (cur_range_avg*getA2(cur_num_parts));
			cur_ucl_range = cur_range_avg*getD4(cur_num_parts);

			cur_lcl_xbar = cur_xbar - (cur_range_avg*getA2(cur_num_parts));
			cur_lcl_range = cur_range_avg*getD3(cur_num_parts);

			if (control_limit_update_instance_ID != 0) {
				//do ajax call to update instance with new control limits
				new Ajax.Request(ajax_processor, {
					parameters: {
						"action":"update_instance_control_limits",
						"xbar_UCL":cur_ucl_xbar,
						"xbar_LCL":cur_lcl_xbar,
						"range_UCL":cur_ucl_range,
						"range_LCL":cur_lcl_range,
						"instance_ID":control_limit_update_instance_ID
					},
					onSuccess: function(res) {

					}
				});
			}
			*/

			var total_xbar = 0;
			var total_range = 0;
			var title = '';

			if (jso[0].xbar_ucl != null && jso[0].xbar_ucl != '' && jso[0].xbar_ucl != 'undefined') {

				ucl_xbar = parseFloat(jso[0].xbar_ucl);
				lcl_xbar = parseFloat(jso[0].xbar_lcl);
				ucl_range = parseFloat(jso[0].range_ucl);
				lcl_range = parseFloat(jso[0].range_lcl);
				var e2 = 1.8859107142857056;

				range_avg = jso[0].range_ucl / (getD4(jso[0].num_parts));

				xbar = jso[0].xbar_ucl - (getA2(jso[0].num_parts) * range_avg);

				if (jso[0].num_parts == 1) {
					xbar = ucl_xbar - e2 * range_avg;
				}

				con_range_avg = range_avg;
				con_xbar = xbar;

				title = '<b style="font-size:7pt;">Saved <br />Control <br /> Limits (' + jso[0].instance_id_range + ')</b>';

			} //else {

				/*range_avg = jso[0].cur_range_UCL / (getD4(jso[0].num_parts));
				xbar = jso[0].cur_xbar_UCL - (getA2(jso[0].num_parts) * range_avg);

				ucl_xbar = parseFloat(jso[0].cur_xbar_UCL);
				ucl_range = parseFloat(jso[0].cur_range_UCL);
				lcl_xbar = parseFloat(jso[0].cur_xbar_LCL);

				if (jso[0].cur_range_LCL != null) lcl_range = jso[0].cur_range_LCL;
					else lcl_range = 0;

				title = '<b style="font-size:7pt;">First 25<br />Instances:</b>';*/

			//}

			var xbar_cols = $('xbar_row');

			for (var i = 0; i < xbar_cols.cells.length; i++) {
				var xbar_c = xbar_cols.cells[i];
				var xbar_value = xbar_c.innerHTML;
				var back = xbar_cols.cells.length - 1;

				if (xbar_value != '&nbsp;') {
					if (!isNaN(xbar_value)) {
						if (ignore_control_alerts == 0) {
							if (parseFloat(xbar_value) > ucl_xbar) {
								xbar_c.style.backgroundColor = 'orange';
								report_instance_id.push($('xbar_' + (back-i)).readAttribute('instance_id'));

								if (i == cur_num_instances) {
									xbar_comment_required = 1;
								}
							}

							if (parseFloat(xbar_value) < lcl_xbar) {
								xbar_c.style.backgroundColor = 'yellow';
								report_instance_id.push($('xbar_' + (back-i)).readAttribute('instance_id'));
								if (i == cur_num_instances) {
									xbar_comment_required = 1;

								}
							}
						}
					}
				}
			}

			var r = $('row_range');
			for (var i = 1; i < r.cells.length; i++) {
				var c = r.cells[i];
				var x = c.innerHTML;
				var back = r.cells.length - 1;

				if (x != '&nbsp;') {
					if (!isNaN(x)) {
						if (ignore_control_alerts == 0) {
							//got a value, so check it
							x = parseFloat(x);
							if (x > ucl_range) {
								c.style.backgroundColor = 'red';
								report_instance_id.push($('xbar_' + (back-i)).readAttribute('instance_id'));

								if (i == cur_num_instances) {
									range_comment_required = 1;
								}
							}

							if (x < lcl_range) {
								c.style.backgroundColor = '#CC00FF';
								report_instance_id.push($('xbar_' + (back-i)).readAttribute('instance_id'));
								if (i == cur_num_instances) {
									range_comment_required = 1;
								}
							}
						}
					}
				}
			}


			var r = tbl.insertRow(-1);
			var c2 = r.insertCell(-1);
			c2.colSpan = 2 + cur_num_subgroups;

			if (ignore_control_alerts == 0) {
				var tbl2 = new Element('TABLE');
				tbl2.width = '100%';
				var r2 = tbl2.insertRow(-1);
				var c = r2.insertCell(-1);

				c.innerHTML = title;

				var c = r2.insertCell(-1);
				c.innerHTML = '<b>Ra:</b> ' + range_avg.toFixed(3);
				var c = r2.insertCell(-1);
				c.innerHTML = '<b>Mean:</b> ' + xbar.toFixed(3);

				var c = r2.insertCell(-1);
				c.innerHTML = '<b>UCL-Xbar: </b>' + ucl_xbar.toFixed(3);
				var c = r2.insertCell(-1);
				c.innerHTML = '<b>LCL-Xbar:</b> ' + lcl_xbar.toFixed(3);
				var c = r2.insertCell(-1);
				c.innerHTML = '<b>UCL-Range:</b> ' + ucl_range.toFixed(3);
				var c = r2.insertCell(-1);
				c.innerHTML = '<b>LCL-Range:</b> ' + lcl_range.toFixed(3);

				if (jso[0].instance_id_range != null) {
					var tmp = jso[0].instance_id_range.split("-");
					var start = tmp[0];
					var end = tmp[1];
				}

				var c = r2.insertCell(-1);

				c.innerHTML = '<a class="save_button" href="#" onclick="doExcelExport(' + cur_data_point_info.ID + ', ' + cur_num_instances + ', ' + start + ', ' + end + ');return false;"><img src="/images/icons/silk/page_excel.png"> Export to Excel</a>';

				Element.extend(c2).update(tbl2);
			}

			var r = tbl.insertRow(-1);
			r.id = 'tr_selected_summary';
			var c2 = r.insertCell(-1);
			c2.id = 'td_selected_summary';
			c2.colSpan = 2 + cur_num_subgroups;
			r.style.display = 'none';

			//Sets global variables for tolerance limits
			tolerance_upper_limit = cur_data_point_info.tolerance_target + cur_data_point_info.tolerance_delta;
			tolerance_lower_limit = cur_data_point_info.tolerance_target - cur_data_point_info.tolerance_delta;

			//Inserts data and highlights out of tolerance point values
			for (var i = 2; i < (2 + cur_num_parts); i++) {
				var r = tbl.rows[i];
				for (var j = 2; j < r.cells.length; j++) {
					var c = r.cells[j];
					var x = c.innerHTML;
					if (x != '&nbsp;') {
						if (!isNaN(x)) {
							//got a value, so check it
							x = parseFloat(x);
							if (x > tolerance_upper_limit) {
								c.style.backgroundColor = '#FF9966';
							}
							if (x < tolerance_lower_limit) {
								c.style.backgroundColor = '#6666FF';
							}
						}
					}
				}
			}

			//store all alerts to be sent as one package
			var send_alert = [];
			var alert_content = '';

			if (values_entered != 'undefined') {
				if (values_entered != null) {
					if (values_entered.length > 0) {

						var tool_cavity = cur_data_point_info.cavity_description;
						var dp_info = cur_data_point_info.data_point_name;
						if (values_entered.length > 1) var array_asc = values_entered.sort( function(a,b){return a - b} );
							else var array_asc = values_entered;
						var average = 0;
						var range = parseFloat(array_asc[array_asc.length-1]) - parseFloat(array_asc[0]);
						var sum = 0;
						var v = 0.0;

						for (var i = 0; i < array_asc.length; i++) {
							sum = sum + parseFloat(array_asc[i]);
							v = parseFloat(array_asc[i]);

							//SINGLE PART is over upper tolerance
							if (v > tolerance_upper_limit) {
								alert_content = 'upper_tolerance~' + tolerance_upper_limit + '~' + v;
								send_alert.push(alert_content);
							}

							//SINGLE PART is under lower tolerance
							if (v < tolerance_lower_limit) {
								alert_content = 'lower_tolerance~' + tolerance_lower_limit + '~' + v;
								send_alert.push(alert_content);
							}
						}

						average = (parseFloat(sum) / parseFloat(array_asc.length));

						if (ignore_control_alerts == 0) {
							var force_title = 'Addition Information Required!';
							var help = 'Error(s): <br />';
							var content = '';
							var flag_xbar = 0;
							var flag_range = 0;
							var force_xbar_comment = '';
							var force_range_comment = '';

							//Check AVERAGE of parts and UCL xbar
							if (average > ucl_xbar) {
								help = help + 'Calculated average is over upper control limit xbar <br />';
								alert_content = 'ucl_xbar~' + roundIt(ucl_xbar) + '~' + average;
								send_alert.push(alert_content);

								if (control_limit_update_instance_ID != null) {
									flag_xbar++;
									content = '<div>UCL XBAR: ' + roundIt(ucl_xbar) + '</div>' +
											  '<div>Calculated Average: ' + average + '</div>';
								}
							} else if (average < lcl_xbar) {
								help = help + 'Calculated average is under lower control limit xbar <br />';
								alert_content = 'lcl_xbar~' + roundIt(lcl_xbar) + '~' + average;
								send_alert.push(alert_content);

								if (control_limit_update_instance_ID != null) {
									flag_xbar++;
									content = '<div>LCL XBAR: ' + roundIt(lcl_xbar) + '</div>' +
											  '<div>Calculated Average: ' + average + '</div>';
								}
							}

							//Check RANGE of parts and UCL range
							if (range > ucl_range) {
								help = help + 'Calculated range is over upper control limit range';
								alert_content = 'ucl_range~' + roundIt(ucl_range) + '~' + range;
								send_alert.push(alert_content);

								if (control_limit_update_instance_ID != null) {
									flag_range++;
									content = content + '<div>UCL Range: ' + roundIt(ucl_range) + '</div>' +
											  			'<div>Calculated Range: ' + range + '</div>';
								}
							} else if (range < lcl_range) {
								help = help + 'Calculated range is under lower control limit range';
								alert_content = 'lcl_range~' + roundIt(lcl_range) + '~' + range;
								send_alert.push(alert_content);

								if (control_limit_update_instance_ID != null) {
									flag_range++;
									content = content + '<div>LCL Range: ' + roundIt(lcl_range) + '</div>' +
											  			'<div>Calculated Range: ' + range + '</div>';
								}
							}

							content = '<div style="color: red;">' + help + '</div><br />' + content + 'Reason: <select id="force_comment_reason"></select> <br />';

							if (flag_xbar == 1 && flag_range == 1) {
								content = content + 'Comments for Out-Of-Control Xbar: <input type="text" id="force_reason_xbar"> <br />' +
													'Comments for Out-Of-Control Range: <input type="text" id="force_reason_range"> <br />' +
													'<button id="force_submit">Save</button><div id="force_error"></div>';

								showPopup(force_title, content);

							} else if (flag_xbar == 1) {
								content = content + 'Reason for Out-Of-Control Xbar: <input type="text" id="force_reason_xbar"> <br />' +
													'<button id="force_submit">Save</button><div id="force_error"></div>';

								showPopup(force_title, content);
							} else if (flag_range == 1) {
								content = content + 'Reason for Out-Of-Control Range: <input type="text" id="force_reason_range"> <br />' +
													'<button id="force_submit">Save</button><div id="force_error"></div>';
								showPopup(force_title, content);
							}

							if (flag_xbar == 1 || flag_range == 1) {

								new Ajax.Request(ajax_processor, {
										parameters: {"action":"get_tolerance_comments", "site_ID": site_ID},
										onSuccess: function(res) {
											var select = $('force_comment_reason');
											for (var i = 0; i < res.responseJSON.length; i++) {
												select.options[select.options.length] = new Option(res.responseJSON[i].reason, res.responseJSON[i].id);
											}
										}
								});

								$('force_submit').observe('click', function() {
									var error = 0;

									if (flag_xbar == 1) {
										force_xbar_comment = $F('force_reason_xbar');
										if (force_xbar_comment == '' || force_xbar_comment == null) error++;
									}
									if (flag_range == 1) {
										force_range_comment = $F('force_reason_range');
										if (force_range_comment == '' || force_range_comment == null) error++;
									}

									if (error == 0) {
										new Ajax.Request(ajax_processor, {
												parameters: {
													"action":"force_save_data",
													"operator": operator,
													"instance_id": control_limit_update_instance_ID,
													"tolerance_comment": $F('force_comment_reason'),
													"xbar_comment": force_xbar_comment,
													"range_comment": force_range_comment
												},
												onSuccess: function(res) {
													hidePopup();
												}
										});
									} else {
										$('force_error').addClassName('error').update('All fields required for submission!');
									}

								});
							}
						}
					}
				}
			}

			if (ignore_control_alerts == 0) {
				//send package
				if (alert_content != '') sendAlert(sel_tool_ID, tool_cavity, dp_info, send_alert);

				doCharting();
			}

		registerEditHandlers();

		}
	});

	var d_top = new Element('DIV');
	d_top.id = 'div_top';
	d_top.update(tbl);
	$('div_right_side').insert(d_top);

	var d_bot = new Element('DIV');
	d_bot.id = 'div_bottom';
	$('div_right_side').insert(d_bot);

	/*
	new Ajax.Request(ajax_processor, {
		parameters: {
			//"action":"calc_control_limits",
			//"data_point_ID": cur_data_point_info.ID
			"action": "get_tool_log",
			"tool_ID": sel_tool_ID
		},
		onSuccess: function(res) {
			doCharting(res);
		}
	});
    */
	} catch (e) { alert(e.message); }
}

function showErrorReport() {

	//exports data to csv spreadsheet
	window.open("processors/entry.asp?action=error_report&report_instance_id=" + report_instance_id + "");

}

//Deletes instance from database
function delete_instance(i_id, del) {
	$('instance_delete_container').update('Are you sure? <a href="#" id="d_inst" class="save_button" onclick="delete_instance(' + i_id + ', \'del\')">yes</a><a href="#" id="c_inst" class="save_button" onclick="cancel_delete_instance(' + i_id + ')">no</a>');
	$('del_warn').update('<strong>WARNING: Historical data will be lost <br /> and control limit calculations could <br /> be miscalculated as a result!</strong>');

	if (del == "del") {
		//yes
		var params = {"action":"delete_instance", "instance_id":i_id};

		new Ajax.Request(ajax_processor, {
			parameters: params,
			onSuccess: function(res) {
				$('instance_delete_container').update('Instance Deleted!');
				load_data(cur_data_point_info.ID, cur_num_parts, [], null);
			}
		});

	} else {
		//no
		$('c_inst').observe('click', function() {
			$('instance_delete_container').update('<a href="#" id="delete_instance" class="save_button">Delete Instance</a>');
		});
	}

}

function cancel_delete_instance(i_id) {
	$('instance_delete_container').update('<a href="#" class="save_button" onclick="delete_instance(' + i_id + ')">Delete Instance</a>');
	$('del_warn').update('');
}

function save_instance_comment(i_id) {
	new Ajax.Request(ajax_processor, {
		parameters: { "action":"save_instance_comment", "instance_ID":i_id, "operator":operator, "instance_comment":$F('save_comment') },
		onSuccess: function(res) {
			$('instance_delete_container').update('Instance Comment Saved!');
		}
	});
}


var popup_timer = null;
var last_popup_source = null;

function registerEditHandlers() {
	//add event listeners for edit cells
	$$('TD.edit_cell').each(
		function(el) {
			el.stopObserving();  //clear all handlers

			el.observe('mouseover',
				function(e) {
					this.addClassName('hovered');
				}
			);
			el.observe('mouseout',
				function(e) {
					this.removeClassName('hovered');
				}
			);
			el.observe('click',
				function(e) {
					var popup_el = $('div_edit_popup');

					var el = Event.element(e);
					var ip_id = el.readAttribute('id');

					var id_detect = ip_id.split("_");
					if (id_detect[0] == 'xbar' || id_detect[0] == 'range'){

						var instance_ID = el.readAttribute('instance_ID');

						var comment = el.readAttribute('comment');
						var ip_val = el.innerHTML;

						popup_el.writeAttribute({"ip_id":ip_id, "instance_ID":instance_ID});

						var update_content = '<b>Editing Instance ' + id_detect[0].charAt(0).toUpperCase() + id_detect[0].slice(1) + ' #' + (el.cellIndex) + '</b><br />' +
											'Reason: <select id="comment_reason"></select><br />' +
											'Edit comment:<input type="text" id="inp_edit_comment" value="Loading comment..." DISABLED> <br />' +
											'<span style="float: right;" class="linker" onclick="saveEdit(this, \'comment\', \'' + id_detect[0] + '\');">Save</span>' +
											'<span style="float: left;" class="linker" onclick="$(\'div_edit_popup\').hide();">Close</span><br />' +
											'<span id="save_note" style="margin-top: 0.2em;"></span>';

						new Ajax.Request(ajax_processor, {
							parameters: {"action":"get_tolerance_comments", "site_ID": site_ID},
							onSuccess: function(res) {
								var select = $('comment_reason');
								for (var i = 0; i < res.responseJSON.length; i++) {
									select.options[select.options.length] = new Option(res.responseJSON[i].reason, res.responseJSON[i].id);
								}
							}
						});

						var params = {"action":"get_comment", "instance_ID":instance_ID, "cell_type":id_detect[0]};
					} else if (id_detect[0] == 'IP') {

						var part_sequence = el.readAttribute('part_sequence');
						var instance_ID = el.readAttribute('instance_ID');
						var comment = el.readAttribute('comment');
						var ip_val = el.innerHTML;

						popup_el.writeAttribute({"ip_id":ip_id, "orig_value":ip_val, "part_sequence":part_sequence, "instance_ID":instance_ID});

						var update_content = '<b>Editing Instance ' + (el.cellIndex - 1) + ' for Part #' + part_sequence + ':</b><br />' +
											'Edit value:<input type="text" id="inp_edit_value" size="5" value="' + el.innerHTML + '"> ' +
											'<span class="linker" onclick="saveEdit(this, \'value\', \'' + id_detect[0] + '\');">Save</span><br />' +
											'Reason: <select id="comment_reason_ip"></select><br />' +
											'Edit comment:<input type="text" id="inp_edit_comment" value="Loading comment..." DISABLED> ' +
											'<span class="linker" onclick="saveEdit(this, \'comment\', \'' + id_detect[0] + '\');">Save</span><br />' +
											'<span class="linker" style="margin-top: 0.2em;" onclick="$(\'div_edit_popup\').hide();">Close</span><br />' +
											'<span id="save_note" style="margin-top: 0.2em;"></></div>';

						new Ajax.Request(ajax_processor, {
							parameters: {"action":"get_tolerance_comments", "site_ID": site_ID},
							onSuccess: function(res) {
								var select = $('comment_reason_ip');
								for (var i = 0; i < res.responseJSON.length; i++) {
									select.options[select.options.length] = new Option(res.responseJSON[i].reason, res.responseJSON[i].id);
								}
							}
						});

						var params = {"action":"get_comment", "instance_ID":instance_ID, "part_sequence":part_sequence, "cell_type":id_detect[0]};
					}

					popup_el.setStyle({"backgroundColor":this.style.backgroundColor});
					popup_el.clonePosition(this, {"setWidth":false, "setHeight":false, "offsetTop":20, "offsetLeft":-200});
					popup_el.update(update_content);

					new Ajax.Request(ajax_processor, {
						parameters: params,
						onSuccess: function(res) {
							if (res.responseJSON[0].comment != null) {

								$('inp_edit_comment').value = res.responseJSON[0]['comment'];

								if (res.responseJSON[0]['tolerance_comment_id'] != null)
									$('comment_reason_ip').value = res.responseJSON[0]['tolerance_comment_id']
								else
									$('comment_reason_ip').value = 1;


							} else if (res.responseJSON[0].xbar_comment != null) {

								$('inp_edit_comment').value = res.responseJSON[0]['xbar_comment'];

								if (res.responseJSON[0]['tolerance_xbar_comment_id'] != null)
									$('comment_reason').value = res.responseJSON[0]['tolerance_xbar_comment_id'];
								else
									$('comment_reason').value = 1;

							} else if (res.responseJSON[0].range_comment != null) {

								$('inp_edit_comment').value = res.responseJSON[0]['range_comment'];

								if (res.responseJSON[0]['tolerance_range_comment_id'] != null)
									$('comment_reason').value = res.responseJSON[0]['tolerance_range_comment_id'];
								else
									$('comment_reason').value = 1;

							} else {
								$('inp_edit_comment').value = '';
							}

							$('inp_edit_comment').disabled = false;
						}
					});
					popup_el.show();
				}
			);
		}
	);

	//setupRangeSelect('subgroup_row', 2, 1);
	$$('#subgroup_row td').each(function(el) {
		if (el.cellIndex >= 2) {
			el.addClassName('selectable');
			el.unselectable = true;
				$(el).observe('click', function(e) {

					var popup_el = $('div_edit_popup');
					var instance_ID = el.readAttribute('instance_ID');

					var update_content = '<b>Instance View (ID: ' + instance_ID + ')</b><br>' +
										'<div id="instance_comment_container">Loading ...</div><br />' +
										'<span style="float:right;" id="instance_delete_container"><a href="#" class="save_button" onclick="delete_instance(' + instance_ID + ')">Delete Instance</a></span>' +
										'<span style="float:left;"><a href="#" class="save_button" onclick="$(\'div_edit_popup\').hide();">Close</a></span><div id="div_edit_status"></div>' +
										'<span id="del_warn"></span>';

					var params = {"action":"get_instance_comment", "instance_id":instance_ID};

					popup_el.setStyle({"backgroundColor":this.style.backgroundColor});
					popup_el.clonePosition(this, {"setWidth":false, "setHeight":false, "offsetTop":20, "offsetLeft":-200});
					popup_el.update(update_content);

					new Ajax.Request(ajax_processor, {
						parameters: params,
						onSuccess: function(res) {
							var comment = res.responseJSON[0].instance_comment;

							if (comment != null) {
								$('instance_comment_container').update('Comment: <input type="text" id="save_comment" value="' + comment + '" /><a href="#" class="save_button" onclick="save_instance_comment(' + instance_ID + ')">Save</a>');
							} else {
								$('instance_comment_container').update('Comment: <input type="text" id="save_comment" value="" /><a href="#" class="save_button" onclick="save_instance_comment(' + instance_ID + ')">Save</a>');
							}
						}
					});

					popup_el.show();

				});

		}
	});

}

function saveEdit(src_el, edit_type, cell_type) {
	$('save_note').update('<i>Saving...</i>');
	var el = src_el.up();

	var required = '';

	var new_value = '';
	if (edit_type == 'value') {
		new_value = $F('inp_edit_value');
	}
	if (edit_type == 'comment') {
		new_value = $F('inp_edit_comment');
	}

	switch (cell_type) {
		case "IP":
			var ip_id = el.readAttribute('ip_id').replace('IP_ID_', '');
			var params = {
				"action":"save_data_edit",
				"site_ID":site_ID,
				"edit_type":edit_type,
				"ip_id":ip_id,
				"part_sequence":el.readAttribute('part_sequence'),
				"instance_ID":el.readAttribute('instance_ID'),
				"dp_ID":cur_data_point_info.ID,
				"sel_part_origin":sel_part_origin_filter,
				"new_value":new_value,
				"cell_type":cell_type,
				"tolerance_comment": $F('comment_reason_ip')
			};
			break;

		case "range":
			var params = {
				"action":"save_data_edit",
				"edit_type":edit_type,
				"instance_ID":el.readAttribute('instance_ID'),
				"new_value":new_value,
				"cell_type":cell_type,
				"operator":operator,
				"tolerance_comment": $F('comment_reason')
			};
			range_comment_required = 0;
			break;

		case "xbar":
			ip_id = el.readAttribute('ip_id').replace('xbar_', '');
			var params = {
				"action":"save_data_edit",
				"edit_type":edit_type,
				"instance_ID":el.readAttribute('instance_ID'),
				"new_value":new_value,
				"cell_type":cell_type,
				"operator":operator,
				"tolerance_comment": $F('comment_reason')
			};
			xbar_comment_required = 0;
			break;
	}

	new Ajax.Request(ajax_processor, {
		parameters: params,
		onSuccess: function(res) {

			$('save_note').update('<b>Data saved!</b>');

			if (res.request.parameters.edit_type == 'value') {
				$('div_edit_popup').hide();
				var v = res.request.parameters.new_value;
				var tool_cavity = cur_data_point_info.cavity_description;
				var dp_info = cur_data_point_info.data_point_name;

				//SINGLE PART is over upper tolerance
				if (v > tolerance_upper_limit) {
					alert_content = 'upper_tolerance~' + tolerance_upper_limit + '~' + v;
					var arr = [alert_content];
					sendAlert(sel_tool_ID, tool_cavity, dp_info, arr);
				}

				//SINGLE PART is under lower tolerance
				if (v < tolerance_lower_limit) {
					alert_content = 'lower_tolerance~' + tolerance_lower_limit + '~' + v;
					var arr = [alert_content];
					sendAlert(sel_tool_ID, tool_cavity, dp_info, arr);
				}

				load_data(cur_data_point_info.ID, cur_num_parts, [], null);  //pass saved data point for alert analysis

			}
		}
	});

}


function saveNewInstance(date){

	//mm/dd/yyyy hh:mm:ss am
	var validate = date.split(" ");
	var v_date = validate[0].split("/");

	//Validate date and time
	if ((date.length == 21 || date.length == 22) && v_date.length == 3 && v_date[0].match("^[0-9]{2}$") != null && v_date[1].match("^[0-9]{2}$") != null && v_date[2].match("^[0-9]{4}$") != null) {

		var params = {
			"action":"check_comments",
			"data_point_ID": cur_data_point_info.ID,
			"new_part_origin":$F('new_part_origin')
		}

		new Ajax.Request(ajax_processor, {
			parameters: params,
			onSuccess: function(res) {

				var check = res.responseJSON[0];

					var params = {
						"action":"save_new_instance",
						"date":date,
						"data_point_ID":cur_data_point_info.ID,
						"num_parts":cur_num_parts,
						"new_part_origin":$F('new_part_origin'),
						"num_subgroups":cur_num_subgroups
					};

					var values_entered = [];

					for (var i = 1; i <= cur_num_parts; i++) {
						var newVal = $('new_' + i);
						if (newVal.value == '' || isNaN(newVal.value)) {
							newVal.addClassName('error');
							newVal.focus();
							newVal.select();
							return;
						} else {
							newVal.removeClassName('error');
							params['part_' + i] = newVal.value;
							values_entered.push(newVal.value);
						}
					}

				if ((xbar_comment_required == 0 && range_comment_required == 0) || (range_comment_required == 1 && check.range_comment != null && xbar_comment_required == 0) ||  (xbar_comment_required == 1 && check.xbar_comment != null && range_comment_required == 0) || (xbar_comment_required == 1 && check.xbar_comment != null && range_comment_required == 1 && check.range_comment != null)) {
					new Ajax.Request(ajax_processor, {
						parameters: params,
						onSuccess: function(res) {
							// do_control_limit_calc will be true if this new instance triggers a recalc of the control limits
							load_data(cur_data_point_info.ID, cur_num_parts, values_entered, res.responseJSON);

							var new_date = dateFormat("mm/dd/yyyy h:MM:ss TT");
							var split_date = new_date.split(" ", 1);
							var split_res = res.responseJSON.instance_time.split(" ", 1);

							if (split_date == split_res) {
								tmp_date = 0;
							} else {
								tmp_date = res.responseJSON.instance_time;
							}
						}
					});
				} else {
					if (check.xbar_comment == null && xbar_comment_required == 1) { alert("Xbar was outside of the control limit threshold for the previous instance and still requires a comment"); }
					if (check.range_comment == null && range_comment_required == 1) { alert("Range was outside of the control limit threshold for the previous instance and still requires a comment"); }
				}

			}
		});

	} else {
		alert("Date must be in this format to save: mm/dd/yyyy hh:mm:ss pm");
	}
}

//var alert_timer = null;

//control_limit_type and control_limit_value is misleading because this function now applies to tolerances as well
function sendAlert(tool_info, cavity_info, dp_info, alert_json) {

	//do ajax call to send email
	new Ajax.Request(ajax_processor, {
		parameters: {
			"action": "send_alert",
			"operator": operator,
			"tool": tool_info,
			"cavity": cavity_info,
			"dp": dp_info,
			"alert_json": alert_json
		},
		onSuccess: function(res) {
			/*var alerts_triggered = res.responseJSON;
			if (alerts_triggered.length > 0) {
				$('div_alert').update('A value entered was ' + control_limit_type + ' and triggered an alert').show();
			} else {
				$('div_alert').update('A value entered was ' + control_limit_type + ', but not enough to trigger an alert email.').show();
			}

			alert_timer = setTimeout(function() {$('div_alert').hide();}, 6000);*/
		}
	});
}

var start_of_range_id = null;
var all_in_select_mode = false;
var start_of_range = null;
var end_of_range = null;
var start_range_number = null;
var end_range_number = null;

var tmp_ucl_xbar = 0;
var tmp_ucl_range = 0;
var tmp_lcl_xbar = 0;
var tmp_lcl_range = 0;

//Full Instance View selection function
function selectRangeAll() {

		$$('#instance_row td').each(function(el) {

				el.addClassName('selectable');
				el.unselectable = true;

					$(el).observe('click', function(f) {

						if (!all_in_select_mode) {

							$$('#instance_row td').each(function(el) {
								el.className = 'selectable';
							});

							this.addClassName('selected');

							start_of_range = parseInt(this.readAttribute('id'));
							end_of_range = start_of_range_id;
							start_range_number = parseInt(this.innerText);
							end_range_number = start_range_number;

							all_in_select_mode = true;

							$('tr_selected_summary').hide();

						} else {

							end_of_range = parseInt(this.readAttribute('id'));

							end_range_number = parseInt(this.innerText);

							all_in_select_mode = false;

							this.addClassName('selected');

							if (start_of_range > end_of_range) {

									//swapping variable values only using two variables
									start_of_range = start_of_range + end_of_range;
									end_of_range = start_of_range - end_of_range;
									start_of_range = start_of_range - end_of_range;

									start_range_number = start_range_number + end_range_number;
									end_range_number = start_range_number - end_range_number;
									start_range_number = start_range_number - end_range_number;

							}

							$$('#instance_row td').each(function (elem) {
								if (elem.id > start_of_range && elem.id < end_of_range) {
									elem.addClassName('selected');
								}
							});

							$('control_limit_save').show();

								new Ajax.Request(ajax_processor, {
									parameters: {
										"action":"get_ip_values",
										"start_of_range_id":start_of_range,
										"end_of_range_id":end_of_range,
										"data_point_ID":cur_data_point_info.ID
									},
									onSuccess: function(re) {
									try {
										//getting avg range and xbar for selected data
										$('instance_range').update('Current Range: ' + parseInt(start_range_number) + ' -> ' + parseInt(end_range_number));

										//Calculating process capability
										var jso = re.responseJSON;
										var std = 0;
										var instances = jso.length / jso[0].num_parts;
										var range_sum = 0.0;
										var inst_range = [];
										var x_dbl_bar = 0.0;
										var final_avg_range = 0.0;
										var long_sum = 0;
										var counter = 0;
										var total_x = 0;
										var total_sum_avg = 0.0;
										var last_range = jso[0].point_value;
										var sample_size = null;

										if (cur_num_parts > 1) {

											//get x_dbl_bar
											for (var i = 1; i < (instances+1); i++) {

												for (var j = ((i-1)*jso[0].num_parts); j < (i*jso[0].num_parts); j++) {
													total_x = total_x + jso[j].point_value;
												}

												total_sum_avg = total_sum_avg + (total_x / jso[0].num_parts);
												total_x = 0;
											}

											x_dbl_bar = (total_sum_avg / instances);

											for (var i = 1; i < (instances+1); i++) {

													inst_range = [];

													for (var j = ((i-1)*jso[0].num_parts); j < (i*jso[0].num_parts); j++) {
														//get avg for each instance
														inst_range.push(jso[j].point_value);

														std = parseFloat(jso[j].point_value) - parseFloat(x_dbl_bar);
														std = Math.pow(std, 2);

														long_sum = long_sum + parseFloat(std);

														counter++;
													}

													//sort range ascending
													inst_range.sort(function(a,b){return a - b});
													range_sum = range_sum + (inst_range[inst_range.length-1] - inst_range[0]);
											}

											sample_size = cur_num_parts;

										} else {

											for (var i = 0; i < jso.length; i++) {

												if (i == 0) {
													range_sum = 0;
												} else {
													range_sum = range_sum + Math.abs(jso[i].point_value - last_range);
												}

												last_range = jso[i].point_value;
												total_sum_avg = total_sum_avg + jso[i].point_value;

											}

											x_dbl_bar = total_sum_avg / jso.length;

											for (var j = 0; j < jso.length; j++) {
												std = jso[j].point_value - x_dbl_bar;
												std = Math.pow(std, 2);

												long_sum = long_sum + std;
											}

											sample_size = 2;

										}

										//console.log('range_sum: ' + range_sum);

										final_avg_range = (range_sum / instances);
										//console.log('avg_range = ' + 'sum (' + range_sum + ') / instances (' + instances + ')');
										//console.log('avg_range: ' + final_avg_range);

										//console.log('x-dbl-bar: ' + x_dbl_bar);
										//console.log('long sum: ' + long_sum);

										var tmpstd = Math.sqrt(long_sum / jso.length);
										//console.log('overal std = sqrt(sum (' + long_sum + ') / n (' + jso.length + '))');
										//console.log('overall std: ' + tmpstd);

										//if (cur_num_parts <= 4) {
										var sigma = final_avg_range / getD2(sample_size);
											//console.log('sigma = range_avg (' + final_avg_range + ') / getD2(' + cur_num_parts + ') (' + getD2(cur_num_parts) + ')');
										//} else {
										//	var sigma = st_dev_bar / getC4(cur_num_parts);
										//	console.log('sigma = standard_deviation (' + st_dev_bar + ') / getC4(' + cur_num_parts + ') (' + getC4(cur_num_parts) + ')');
										//	console.log('other sigma = ' + (final_avg_range / getD2(cur_num_parts)));
										//}

										//console.log('sigma: ' + sigma);

										//Cp -- Process capability; a simple and straightforward indicator of process capability
										var cp = ((parseFloat(tolerance_upper_limit) - parseFloat(tolerance_lower_limit))/(6*parseFloat(sigma)));

										//console.log('Cp = (tolerance_upper_limit (' + tolerance_upper_limit + ') - tolerance_lower_limit (' + tolerance_lower_limit + ')) / (6 * sigma (' + sigma + '))');
										//console.log('Cp = ' + cp);

										var cp_u = ((parseFloat(tolerance_upper_limit) - parseFloat(x_dbl_bar)) / (3*parseFloat(sigma)));
										//console.log('CpU = (tolerance_upper_limit (' + tolerance_upper_limit + ') - xbar (' + x_dbl_bar + ')) / (3 * sigma (' + sigma + '))');
										//console.log('CpU: ' + cp_u);

										var cp_l = ((parseFloat(x_dbl_bar) - parseFloat(tolerance_lower_limit)) / (3*parseFloat(sigma)));
										//console.log('CpL = (xbar (' + x_dbl_bar + ') - tolerance_lower_limit (' + tolerance_lower_limit + ')) / (3 * sigma (' + sigma + '))');
										//console.log('CpL: ' + cp_l);

										//Cpk -- Process Capability Index; adjustment of Cp for the effect of non-centered distribution
										if (cp_u < cp_l) { var cpk = cp_u; }
											else { var cpk = cp_l; }

										//console.log('Cpk = Min (CpU (' + cp_u + '), CpL (' + cp_l + '))');
										//console.log('Cpk: ' + cpk);

										//Capability Ratio (Cr) indicated what proportion of tolerance is occupied by the data
										var cr = (1/cp);
										//console.log('Cr: ' + cr);

										//Pp -- Process Performance; a simple indivator of process performance
										var pp = (tolerance_upper_limit - tolerance_lower_limit) / (6*tmpstd);
										//console.log('Pp = (tolerance_upper_limit (' + tolerance_upper_limit + ') - tolerance_lower_limit (' + tolerance_lower_limit + ')) / (6 * standard_deviation (' + tmpstd + ')) ');
										//console.log('Pp: ' + pp);

										var pp_u = (tolerance_upper_limit - x_dbl_bar) / (3*tmpstd);
										//console.log('PpU = (tolerance_upper_limit (' + tolerance_upper_limit + ') - xbar (' + x_dbl_bar + ')) / (3 * standard_deviation (' + tmpstd + '))');
										//console.log('PpU: ' + pp_u);

										var pp_l = (x_dbl_bar - tolerance_lower_limit) / (3*tmpstd);
										//console.log('PpL = (xbar (' + x_dbl_bar + ') - tolerance_lower_limit (' + tolerance_lower_limit + ')) / (3 * standard_deviation (' + tmpstd + ')');

										if (pp_u < pp_l) { var ppk = pp_u; }
											else { var ppk = pp_l; }

										//Ppk -- Process Performance Index; adjustment of Pp for the effect of non-centered distribution
										//console.log('PpK = Min (PpU (' + pp_u + '), PpL (' + pp_l + '))');
										//console.log('PpK: ' + ppk);

										$('process_capability').update('<strong>Cp: </strong>' + cp.toFixed(5) + ' <strong>Cpk: </strong>' + cpk.toFixed(5) + '<br /><strong>Pp: </strong>' + pp.toFixed(5) + ' <strong>Ppk: </strong>' + ppk.toFixed(5));

										tmp_ucl_xbar = x_dbl_bar + (final_avg_range*getA2(sample_size));
										//console.log('ucl_xbar = xbar (' + x_dbl_bar + ') + (avg_range (' + final_avg_range + ') * A2(' + cur_num_parts + ')) = ' + tmp_ucl_xbar);

										tmp_ucl_range = final_avg_range*getD4(sample_size);
										//console.log('ucl_range = avg_range (' + final_avg_range + ') * D4(' + cur_num_parts + ') = ' + tmp_ucl_range);

										tmp_lcl_xbar = x_dbl_bar - (final_avg_range*getA2(sample_size));
										//console.log('lcl_xbar = xbar (' + x_dbl_bar + ') - (avg_range (' + final_avg_range + ') * A2(' + cur_num_parts + ')) = ' + tmp_lcl_xbar);

										tmp_lcl_range = final_avg_range*getD3(sample_size);
										//console.log('lcl_range = avg_range (' + final_avg_range + ') * D3(' + cur_num_parts + ') = ' + tmp_lcl_range);

										var tbl2 = new Element('TABLE');
											tbl2.width = '100%';
											var r2 = tbl2.insertRow(-1);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b style="font-size:7pt;">Selected <br />Data (' + start_of_range + '-' + end_of_range + '):</b>';
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>Ra:</b> ' + final_avg_range.toFixed(3);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>Mean:</b> ' + x_dbl_bar.toFixed(3);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>UCL-Xbar: </b>' + tmp_ucl_xbar.toFixed(3);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>LCL-Xbar:</b> ' + tmp_lcl_xbar.toFixed(3);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>UCL-Range:</b> ' + tmp_ucl_range.toFixed(3);
											var c = r2.insertCell(-1);
											c.innerHTML = '<b>LCL-Range:</b> ' + tmp_lcl_range.toFixed(3);
											var c = r2.insertCell(-1);

											c.innerHTML = '<a class="save_button" href="#" onclick="doExcelExport(' + cur_data_point_info.ID + ', ' + cur_num_instances + ', ' + start_of_range + ', ' + end_of_range + ');return false;"><img src="/images/icons/silk/page_excel.png"> Export to Excel</a>';

											$('td_selected_summary').update(tbl2);
											$('tr_selected_summary').show();
									} catch (e) { alert(e.message); }
									}
								});

						}

					});

		});
}

//Popup instance window
function showInstancesPopup(e, dp_id) {

	var popup_el = $('div_edit_popup');

	var update_content = '<b>Full Instance View</b><br>' +
						'<div id="instance_range"></div>' +
						'<div id="process_capability"></div>' +
						'Select Instances <br />(Click once to start the range and again to finish):<div id="instance_container">Loading ...</div><br />' +
						'<span style="display:none; float:right" id="control_limit_save"><a href="#" class="save_button" onclick="saveControlLimits(this, \'' + dp_id + '\');">Set Control Limits</a></span>' +
						'<span style="float:left;"><a href="#" class="save_button" onclick="$(\'div_edit_popup\').hide();">Close</a></span><div id="div_edit_status"></div>';

	var params = {"action":"get_instances", "dp_id":dp_id};

	popup_el.setStyle({"backgroundColor":e.style.backgroundColor});
	popup_el.clonePosition(e, {"setWidth":false, "setHeight":false, "offsetTop":20, "offsetLeft":5});
	popup_el.update(update_content);

	new Ajax.Request(ajax_processor, {
		parameters: params,
		onSuccess: function(res) {

			var length = res.responseJSON.length;
			var jso = res.responseJSON;
			//default # of columns in table
			var cols = 10;
			var rows = Math.round(length / cols)+1;
			var content = '<table style="border: 1px solid black;">';
			var count = 1;
			var title = '';

				for (var i = 0; i < rows; i++) {
					content = content + '<tr id="instance_row">';

					for (var j = 0; j < cols; j++) {
						if (count <= length) {
							title = jso[count-1].instance_time + '\n From: Press ' + jso[count-1].part_origin_ID;

							if (jso[count-1].xbar_comment != null) {
								title = title + '\n XBAR Comment: ' + jso[count-1].xbar_comment;
							}


							if (jso[count-1].range_comment != null) {
								title = title + '\n RANGE Comment: ' + jso[count-1].xbar_comment;
							}
							content = content + '<td class="instance_td" id="' + res.responseJSON[count-1].ID + '" title="' + title + '">' + count + '</td>';

							count++;
						} else {
							count++
						}

					}
					content = content + '</tr>';
				}

			content = content + '</table>';
			$('instance_container').update(content);

			selectRangeAll();

		}
	});

	popup_el.show();
}

//Cancel control limit save
function cancelSaveControlLimits(e, dp_id)
{
	 $('control_limit_save').update('<a href="#" class="save_button" onclick="saveControlLimits(\'' + e + '\', \'' + dp_id + '\');">Set Control Limits</a>');
}

//Confirm control limit save
function saveControlLimits(e, dp_id, save) {

	if (save != "save") {
		$('control_limit_save').update('Are you sure? <a href="#" class="save_button" onclick="saveControlLimits(\'' + e + '\', \'' + dp_id + '\', \'save\')">yes</a><a href="#" class="save_button" onclick="cancelSaveControlLimits(this, \'' + dp_id + '\')">no</a>');
	} else {

	var tmp_instance_id_range = start_of_range + '-' + end_of_range;
	var params = { "action": "save_control_limits", "dp_id": dp_id, "instance_id_range": tmp_instance_id_range, "xbar_ucl": tmp_ucl_xbar, "xbar_lcl": tmp_lcl_xbar, "range_ucl": tmp_ucl_range, "range_lcl": tmp_lcl_range };

		new Ajax.Request(ajax_processor, {
			parameters: params,
			onSuccess: function(res) {

				$('control_limit_save').update('Control Limits Saved!');
				load_data(cur_data_point_info.ID, cur_num_parts, [], null);

			}
		});

	}
}

//Export function to CSV file
function doExcelExport(dp_id, instances, start_range, end_range) {
	//check for undefined values
	if (typeof start_range === 'undefined') {
		start_range = '';
		end_range = '';
	}

	window.open("processors/entry.asp?action=export_excel&dp_id=" + dp_id + "&num_instances=" + instances + "&start_range_id=" + start_range + "&end_range_id=" + end_range + "");

}