var NYSUS = NYSUS || {};
NYSUS.REPORTS = NYSUS.REPORTS || [];
NYSUS.REPORTS.ELEMENTS = NYSUS.REPORTS.ELEMENTS || {
		start_date: true,
		end_date: true,
		operators: true,
		shifts: true,
		machines: true,
		tools: true,
		parts: true
	};

$(document).ready(function () {
	$('.btn-run-report').prop("disabled", true);

	$(document).on('select2:select', '#sel_systems', function () {
		var sel_systems = $('#sel_systems');

		$('#sel_shifts').select2(NYSUS.REPORTS.REPORT_FILTER.controls.shifts_select);
		NYSUS.REPORTS.REPORT_FILTER.selected_system_ID = sel_systems.select2('data')[0].id;
		NYSUS.REPORTS.REPORT_FILTER.selected_process_type = sel_systems.select2('data')[0].process_type;
		NYSUS.REPORTS.REPORT_FILTER.show_process_type_filter();
		$('.btn-run-report').prop("disabled", false);
	});

});

NYSUS.REPORTS.REPORT_FILTER = {
	display_element: 'body',
	common_filter_html: 'templates/reports/report_filter.html #filter-common',
	sequenced_filter_html: 'templates/reports/report_filter.html #filter-sequenced',
	batch_filter_html: 'templates/reports/report_filter.html #filter-batch',
	repetitive_filter_html: 'templates/reports/report_filter.html #filter-repetitive',
	misc_filter_html: 'templates/reports/report_filter.html #filter-misc',
	processor: 'ajax_processors/reports/report_filter.php',

	get_selections: function () {
		return {
			process_type: $('#sel_systems').select2('data').length > 0 ? $('#sel_systems').select2('data')[0].process_type : null,
			system_ID: $('#sel_systems').val(),
			start_date: $('#inp_start_date').val(),
			end_date: $('#inp_end_date').val(),
			machines: $('#sel_machines').val(),
			tools: $('#sel_tools').val(),
			parts: $('#sel_parts').val(),
			plcs: $('#sel_plcs').val(),
			operators: $('#sel_operators').val(),
			shifts: $('#sel_shifts').val(),
			group_by: $('#sel_group_bys').val(),
			top: $('#sel_top').val(),
			lines: $('#sel_lines').val(),
			stations: $('#sel_line_stations').val(),
		};
	},

	before_render: function () {
	},
	after_render: function () {
		var n = NYSUS.REPORTS.ELEMENTS;
		!n.start_date?$('#inp_start_date').closest('[id^=start-date]').remove():false;
		!n.end_date?$('#inp_end_date').closest('[id^=end-date]').remove():false;
		!n.machines?$('#sel_machines').closest('[id^=machines]').remove():false;
		!n.tools?$('#sel_tools').closest('[id^=tools]').remove():false;
		!n.parts?$('#sel_parts').closest('[id^=parts]').remove():false;
		!n.operators?$('#sel_operators').closest('[id^=operators]').remove():false;
		!n.shifts?$('#sel_shifts').closest('[id^=shifts]').remove():false;
	},
	render: function () {
		NYSUS.REPORTS.REPORT_FILTER.load_html(NYSUS.REPORTS.REPORT_FILTER.common_filter_html, function () {
			NYSUS.REPORTS.REPORT_FILTER.before_render();
			var sel_systems = $('#sel_systems');

			$('#inp_start_date').set_date_value();
			$('#inp_end_date').set_date_value();

			sel_systems.select2(NYSUS.REPORTS.REPORT_FILTER.controls.systems_select);
			$('#sel_operators').select2(NYSUS.REPORTS.REPORT_FILTER.controls.operators_select);
			$('#sel_shifts').select2();
			//sel_systems.val(1).trigger('change.select2');

			NYSUS.REPORTS.REPORT_FILTER.after_render();

			if (typeof callback === "function") {
				callback();
			}
		});
	},

	load_html: function (html, callback) {
		$('<div>').load(html, function () {
			var el = NYSUS.REPORTS.REPORT_FILTER.display_element;

			$(el).append($(this).html());
			if (typeof callback === 'function') {
				callback();
			}
		});
	},

	before_show_process_type_filter: function () {
	},
	after_show_process_type_filter: function () {
	},
	show_process_type_filter: function () {
		var el = NYSUS.REPORTS.REPORT_FILTER.display_element;
		var process_type = NYSUS.REPORTS.REPORT_FILTER.selected_process_type;

		if ($(el).find("div[id*='filter-" + process_type + "']").length == 0) {
			$(el).find('.row:gt(0)').remove(); // remove any other filters

		}

		switch (NYSUS.REPORTS.REPORT_FILTER.selected_process_type.toLowerCase()) {
			case 'repetitive':
				NYSUS.REPORTS.REPORT_FILTER.load_html(NYSUS.REPORTS.REPORT_FILTER.repetitive_filter_html, function () {
					NYSUS.REPORTS.REPORT_FILTER.before_show_process_type_filter();

					$('#sel_machines').select2(NYSUS.REPORTS.REPORT_FILTER.controls.machines_select);
					$('#sel_tools').select2(NYSUS.REPORTS.REPORT_FILTER.controls.tools_select);
					$('#sel_parts').select2(NYSUS.REPORTS.REPORT_FILTER.controls.parts_select);
					$('#sel_plcs').select2(NYSUS.REPORTS.REPORT_FILTER.controls.plcs_select);
					$('#sel_fault_codes').select2(NYSUS.REPORTS.REPORT_FILTER.controls.fault_codes_select);
					NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter();
				});
				break;
			case 'sequenced':
				NYSUS.REPORTS.REPORT_FILTER.load_html(NYSUS.REPORTS.REPORT_FILTER.sequenced_filter_html, function () {
					NYSUS.REPORTS.REPORT_FILTER.before_show_process_type_filter();
					$('#sel_lines').select2(NYSUS.REPORTS.REPORT_FILTER.controls.lines_select);
					$('#sel_line_stations').select2(NYSUS.REPORTS.REPORT_FILTER.controls.line_stations_select);
					NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter();
				});
				break;
			case 'batch':
				break;
			default:
				break;
		}
	},

	before_show_misc_filter: function () {
	},
	after_show_misc_filter: function () {
	},
	show_misc_filter: function () {
		NYSUS.REPORTS.REPORT_FILTER.load_html(NYSUS.REPORTS.REPORT_FILTER.misc_filter_html, function () {
			NYSUS.REPORTS.REPORT_FILTER.before_show_misc_filter();
			$('#sel_group_bys').select2(NYSUS.REPORTS.REPORT_FILTER.controls.group_bys_select);
			$('#sel_top').select2(NYSUS.REPORTS.REPORT_FILTER.controls.top_select);
			NYSUS.REPORTS.REPORT_FILTER.after_show_misc_filter();
		});
	},

	show_elements: function (arr) {
		$.map(arr, function (v) {
			$(v).show();
		});
	},

	hide_elements: function (arr) {
		$.map(arr, function (v) {
			$(v).hide();
		});
	}
};

NYSUS.REPORTS.REPORT_FILTER.controls = {
	systems_select: {
		placeholder: 'Select System',
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			data: function (params) {
				return {
					q: params.term,
					action: 'get_systems',
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.system_name,
							process_type: item.process_type,
							database_name: item.database_name
						}
					})
				}
			}
		}
	},

	operators_select: {
		placeholder: 'Select Operators',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_operators',
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.badge_ID + ': ' + item.name
						}
					})
				}
			}
		}
	},

	shifts_select: {
		placeholder: 'Select Shifts',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_shifts',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.shift_desc
						}
					})
				}
			}
		}
	},

	lines_select: {
		placeholder: 'Select Lines',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_lines',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.ID + ': ' + item.line_code
						}
					})
				}
			}
		}
	},

	line_stations_select: {
		placeholder: 'Select Stations',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_line_stations',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					lines: $('#sel_lines').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.ID + ': ' + item.station_desc
						}
					})
				}
			}
		}
	},

	machines_select: {
		placeholder: 'Select Machines',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_machines',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					tools: $('#sel_tools').val(),
					parts: $('#sel_parts').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.machine_number + ': ' + item.machine_name
						}
					})
				}
			}
		}
	},

	tools_select: {
		placeholder: 'Select Tools',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_tools',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					machines: $('#sel_machines').val(),
					parts: $('#sel_parts').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.short_description
						}
					})
				}
			}
		}
	},

	parts_select: {
		placeholder: 'Select Parts',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_parts',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					machines: $('#sel_machines').val(),
					tools: $('#sel_tools').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.part_number + ': ' + item.part_desc
						}
					})
				}
			}
		}
	},

	plcs_select: {
		placeholder: 'Select PLC',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_plcs',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					machines: $('#sel_machines').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.description
						}
					})
				}
			}
		}
	},

	fault_codes_select: {
		placeholder: 'Select Fault Code',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_fault_codes',
					system_ID: NYSUS.REPORTS.REPORT_FILTER.selected_system_ID,
					machines: $('#sel_machines').val(),
					plcs: $('#sel_plcs').val(),
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item.ID,
							text: item.description
						}
					})
				}
			}
		}
	},

	group_bys_select: {
		placeholder: 'Select Grouping',
		//dropdownParent: $('#sel_systems').parent(),
		multiple: true,
		allowClear: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_group_by',
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item,
							text: item
						}
					})
				}
			}
		}
	},

	top_select: {
		placeholder: 'Select Top',
		//dropdownParent: $('#sel_systems').parent(),
		allowClear: true,
		ajax: {
			cache: true,
			url: NYSUS.REPORTS.REPORT_FILTER.processor,
			dataType: 'json',
			type: "GET",
			quietMillis: 250,
			theme: "bootstrap",
			data: function (params) {
				return {
					q: params.term,
					action: 'get_top',
					page: params.page
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							id: item,
							text: item
						}
					})
				}
			}
		}
	}

};

$.fn.get_date_value = function () {
	var date_str = this.val();
	return new Date(date_str).toISOString().split('T')[0];
};

$.fn.set_date_value = function () {
	// initialize the date input to the current date
	var today = new Date();
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().split('T')[0];
	this.val(today_str);
	return this;
};