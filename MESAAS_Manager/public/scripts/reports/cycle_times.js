var processor = './ajax_processors/reports/cycle_times.php';
var dtCycleTimes;
var systems;
var chosen_system;

$(document).ready(function() {	
    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    $('#system_select').on('change', function (e) {
        load_filter_options();
    });

    $('#line_select').on('change', line_changed);

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	// run report button
	$('body').on('click', '.btn-run', loadData);
		
	//show detail on cycleTimesTable click
	$('.table-cycle-report tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = dtCycleTimes.row(tr);
 
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			populateStationDetailRow(row);
			tr.addClass('shown');
		}
	});	
		
});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];
	
	// populate operators
	load_operators(null, function (operators) {
    	show_operators(operators);
		var d = $('<option />').prop('id', 'operator_default')
							   .prop('name', 'soperator_id')
							   .val('%')
							   .text('- Operator -');
		$('#operator_select').prepend(d);
    });
	
	//populate the group by
	$('#inpGroupBy').empty();	
	$('#inpGroupBy').append($('<option />').val('Date').text('Date'));
	$('#inpGroupBy').append($('<option />').val('Operator').text('Operator'));	
	
	//populate shifts
	$('#inpShift').empty();
	$('#inpShift').append($('<option />').val('%').text('- Shift -'));
	$('#inpShift').append($('<option />').val('1').text('First'));
	$('#inpShift').append($('<option />').val('2').text('Second'));
	$('#inpShift').append($('<option />').val('3').text('Third'));

    if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
        console.log(chosen_system);
		$('#inpGroupBy').prepend($('<option />').val('Line').text('Line'));
        $('#line_select').empty();
		$('#line_select').nysus_select({
		    name: 'line_id',
		    key: 'line_ID',
		    data_url: 'ajax_processors/filter.php',
		    params: {action: 'get_lines', system_id: chosen_system.ID},
		    render: function (line) {return line.line_desc;},
            callback: function(data) {line_changed();}
		}).prop('disabled', false);

        $('#line_select').empty().prop('disabled', false);
        $('#machine_select').empty().prop('disabled', true);
    } else if (chosen_system.process_type === 'REPETITIVE') {
        $('#machine_select').empty();
        $('#machine_select').nysus_select({
            name: 'machine_id',
            key: 'ID',
            data_url: 'ajax_processors/filter.php',
            params: {action: 'get_machines', database: chosen_system.database_name},
            render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;}
            //callback: function(data) {console.log();}
        }).prop('disabled', false);

        $('#machine_select').empty().prop('disabled', false);
        $('#line_select').empty().prop('disabled', true);
		$('#station_select').empty().prop('disabled', true);
		
    }
}

function line_changed() {
	// load_stations({
	// 	system_id: chosen_system.ID,
	// 	line_id: $('#line_select').val()
	// }, function (stations) {
	// 	show_stations(stations);
	// 	var default_op = $('<option />').prop('id', 'station_default')
	// 								    .prop('name', 'station_id')
	// 								    .val('%')
	// 								    .text('- Station -');
	// 	$('#station_select').prepend(default_op);
	// });

    $('#station_select').empty();
	$('#station_select').nysus_select({
	    name: 'station_id',
	    key: 'station_ID',
	    data_url: 'ajax_processors/filter.php',
	    params: {action: 'get_stations', system_id: chosen_system.ID, line_id: $('#line_select').val()},
	    render: function (station) {return station.name;},
	    placeholder: '- Station -'
	}).prop('disabled', false);
}

function loadData() {
	
	// set dates on panels
	var dateTitle = ($('#start_date').val() != $('#end_date').val()) 
		? $('#start_date').val() + ' -> ' + $('#end_date').val() 
		: $('#start_date').val();
	$('.panel .panel-report-date').html(dateTitle);
	//$('.table-cycle-summary .loading').show();
	 
	$.getJSON(processor, {
		'action':'cycle_times',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
		'operator_id':$('#operator_select').val(),
		'station_id':$('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		'machine_ID': $('#machine_select :selected').val()
	}, function(jso) {

		//respond no data
		if (!jso) {
			$('.panel .panel-report-date').html('No Data to Report');
			$('.panel .panel-cycle-chart').empty();
			$('.table-cycle-report').hide();
			return; 
		} else {
			$('.table-cycle-report').show();
			populateCycleTimesReport(jso);
			populateCycleTimesChart(jso);
		}
		//$('.table-cycle-summary .loading').hide();
	});
	
}

function populateCycleTimesChart(jso) {
	// populate the cycle times chart	
	var categories = [];
	var data = [];
	$.each(jso, function(k,v) {
		if ($('#inpGroupBy').val() === 'Operator') {
			categories.push(v.operator_name);
		} else if ($('#inpGroupBy').val() === 'Date') {			
			categories.push(v.id != undefined ? v.id + ': ' + v.entry_date : v.entry_date);			
		} else {
			categories.push(v.id);
		}
		data.push(v.avg_cycle_time);
	});
		
	// populate the chart
	$('.panel-cycle-chart').highcharts({
        chart: {
            type: 'bar'
        },
		tooltip: {
            formatter: function () {
				var label = '';
				
				if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
					label = '<b>Station: ' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '<br/>';
				} else {
					label = this.series.name + ': ' + this.y + '<br/>';
				}
                return label;
            }
        },
        title: {
            text: 'Cycle Times'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Cycle Time'
            }
        },
        series: [{
			name: 'Avg Cycle',
			data: data
		}],
		credits: false,
		plotOptions: {
            column: {
                dataLabels: {
                    enabled: true
                }
            }
        }
	});

}

function populateCycleTimesReport(jso) {
	if (!jso) {return;}

	if (dtCycleTimes != null) { 
		dtCycleTimes.destroy();	//so the thing will update		
	} 
	
	var cols = [];
	cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	cols.push({data: 'operator_name', "title":"Operator", "defaultContent":""});
	cols.push({data: 'entry_date', "title":"Date", "defaultContent":""});
	if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
		cols.push({data: 'station', "title":"Station"});
	} else if (chosen_system.process_type == 'REPETITIVE') {
		cols.push({data: 'tool_description', "title":'<span data-localize="tool.upper">Tool</span>'});
	}
	cols.push({data: 'avg_cycle_time', "title":"Avg Cycle"});
	cols.push({data: 'num_cycles', "title":"Total Cycles"});
	
	dtCycleTimes = $('.table-cycle-report').DataTable({
		"bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],                        
		data: jso,        
		order: [[3, 'asc']],
		columns: cols	
	});
	
	dtCycleTimes.column(1).visible(false);
	dtCycleTimes.column(2).visible(false);
	// show hide cols based on groupBy
	if ($('#inpGroupBy').val() === "Operator") {
		dtCycleTimes.column(1).visible(true);
	} else if ($('#inpGroupBy').val() === "Date") {
		dtCycleTimes.column(2).visible(true);
	}
}

/* Formatting function for row details */
function populateStationDetailRow(r) {	
	childContent = '<div class="row">'+
				'<div class="col-sm-12"><table class="table table-striped station-detail"></table></div>'+
				'<div class="col-sm-4"><div class="station-cycle-chart-'+r.data().station_id+'"></div></div>'+
				'</div>';
	r.child($(childContent)).show();
	
	//build Operator table in left table
	populateDetailTable(r);
	//build line graph in right table
	if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
		populateDetailChart(r);
	}
	
}

function populateDetailTable(r) {
	var params = {
		'action':'cycle_times_detail',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':r.data().line_id == null ? "%" : r.data().line_id,
		'station_id':r.data().id == null ? "%" : r.data().id,
		'operator_id': $('#operator_select :selected').val(),//r.data().operator_id == null ? "%" : r.data().operator_id,
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		'machine_ID': $('#machine_select :selected').val(),
		'tool_ID': r.data().tool_ID == null ? "%" : r.data().tool_ID
	};
	console.log(params);
	$.getJSON(processor, params, function(jso) {
		if (!jso) {return;}

		var cols = [];		
		if (chosen_system.process_type == 'BATCH' || chosen_system.process_type == 'SEQUENCED') {
			cols.push({data: 'station', "title":"Station"});
		} else if (chosen_system.process_type == 'REPETITIVE') {
			cols.push({data: 'tool_description', "title":'<span data-localize="tool.upper">Tool</span>'});
		}
		cols.push({data: 'name', "title":"Operator"});
		cols.push({data: 'entry_time.date', "title":"Entry Time"});
		cols.push({data: 'cycle_time', "title":"Cycle Time"});
		
		r.child().find('.station-detail').DataTable({
			"bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],                      
			data: jso,        
			order: [[2, 'desc']], 
			columns: cols
		});
	});
	
}

function populateDetailChart(r) {
	$.getJSON(processor, {
		'action':'station_operator_cycle_times',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
		'station_id':r.data().id == null ? "%" : r.data().id,
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type,
		'machine_ID': $('#machine_select :selected').val()
	}, function(jso) {
		if (!jso) {return;}

		// populate the cycle times chart	
		var categories = [];
		$.each(jso, function(k,v) {
			categories.push(v.name);
		});
		var data = [];
		$.each(jso, function(k,v) {
			data.push(v.avg_cycle_time);
		});	
		
		// populate the chart
		r.child().find('.station-cycle-chart-'+r.data().station_id).highcharts({
			chart: {
				type: 'bar'
			},
			title: {
				text: 'Operator Cycle Times'
			},
			xAxis: {
				categories: categories
			},
			yAxis: {
				title: {
					text: 'Cycle Time'
				}
			},
			series: [{
				name: 'Avg Cycle',
				data: data
			}],
			credits: false		
		});

		//$('.table-cycle-chart .loading').hide();
	});

}