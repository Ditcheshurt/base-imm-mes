var utility = NYSUS.COMMON.UTILITY;
var input = NYSUS.UI.INPUT;

var processor = './ajax_processors/reports/operator_report_card_selection.php';

var operator_select;
var start_date;
var end_date;

$(document).ready(function() {
	operator_select = $('#operator_select');
	start_date = $('#start_date');
	end_date = $('#end_date');

    // date range filtering
    input.init_date_input('#start_date');
    input.init_date_input('#end_date');

    load_operators(show_operators);

	// run report button
	$('body').on('click', '.btn-run', function () {
		var params = {
			type: 'operator_report_card',
			operator_ID: operator_select.val(),
			start_date: start_date.val(),
			end_date: end_date.val()
		};

		utility.open_location('./reports.php', params, '_blank');
	});
		
});

function load_operators (cb) {
	var params = {action: 'get_operators'};
	$.getJSON(processor, params).done(function (data) {
		if (cb) {
			cb(data);
		}
	});
}

function show_operators (data) {
    operator_select.empty();
    if (!data) {return;}

    data.forEach(function (operator) {
    	var name = '(' + operator.badge_ID + ') ' + operator.first_name + ' ' + operator.last_name;
        $('<option></option>').prop({id: 'operator_' + operator.ID, name: 'operator_ID'}).val(operator.ID).html(name).appendTo(operator_select);
    });
}