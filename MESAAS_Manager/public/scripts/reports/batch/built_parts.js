var processor = './ajax_processors/reports/batch/built_parts.php';
var table;
var systems;
var chosen_system;

$(document).ready(function () {

	load_systems({process_type: 'BATCH'}, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	$('.btn-run').on('click', function () {
		load_data();
	});

	$('#built_part_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=batch/built_part_info&built_part_ID='+row.data().ID);
	});
});

function load_filter_options () {
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id}, show_lines);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#built_part_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: processor,
			data: {
				action: 'get_built_parts',
				start_date: function (d) {return get_date_value('start_date');},
				end_date: function (d) {return get_date_value('end_date');},
				line_ID: function (d) {return $('#line_select option:selected').val();},
        		database: function (d) {return chosen_system.database_name;},
        		process_type: function (d) {return chosen_system.process_type;}
			},
			dataSrc: '',
		},

		order: [[0, 'desc']],

		columns: [
			{data: 'external_serial', title: 'Serial #'},
			{data: 'part_number', title: 'Part #'},
			{data: 'part_desc', title: 'Description'},
			{data: 'line_desc', title:'Line'},
			{
				data: 'built_time',
				title: 'Built Time',
				render:  render_date
			},
			{
				title: 'Actions',
				className: 'actions',
				orderable: false,
				data: null,
				defaultContent: '',
				render: function (data, type, full, meta) {
					var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
					var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Birth Certificate</button>');
					btns.append(bc_btn);
					return btns[0].outerHTML;
				}
			}
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function load_lines (params, callback) {
    if (params) {
        params.action = 'get_lines';
    } else {
        params = {action: 'get_lines'};
    }

    $.getJSON(
        'ajax_processors/filter.php',
        params,
        callback
    );
}

function show_lines (data) {
    $('#line_select').empty();
    if (!data) {return;}

    var el = $('<option name="line_ID"></option>');
        el.prop('id', 'line_default')
          .val(-1)
          .html('All Lines');
        $('#line_select').append(el);

    $.each(data, function (i, line) {
        var el = $('<option name="line_ID"></option>');
        el.prop('id', 'line_'+line.line_ID)
          .val(line.line_ID)
          .html(line.line_desc);

        $('#line_select').append(el);
    });
}