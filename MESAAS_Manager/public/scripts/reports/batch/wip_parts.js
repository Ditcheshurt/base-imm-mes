var table;
var systems;
var chosen_system;

$(document).ready(function () {

	load_systems(null, function (data) {
		systems = data;
		show_systems(data);
		load_filter_options();
	});

	$('#system_select').on('change', function (e) {
		load_filter_options();
	});

	// date range filtering
	init_date_input('start_date');
	init_date_input('end_date');

	$('.btn-run').on('click', function () {
		load_data();
	});

	$('#wip_part_table').on('click', '.btn-bcert', function (e) {
		var row = table.row($(this).closest('tr'));
		window.open('reports.php?type=batch/wip_part_info&process_type='+chosen_system.process_type+'&database='+chosen_system.database_name+'&wip_part_ID='+row.data().ID);
	});
});

function load_filter_options () {
	var system_id = $('#system_select option:selected').val();
	var res = $.grep(systems, function (s) {
		return s.ID == system_id;
	});
	chosen_system = res[0];

	load_lines({system_id: system_id}, show_lines);
	show_statuses([
		{ID: 1, status_desc: 'In Progress'},
		{ID: 9, status_desc: 'Rejected'}
	]);
}

function load_data() {
	// table setup
	if (typeof table != 'undefined') {
		table.ajax.reload();
		return;
	}

	table = $('#wip_part_table').DataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
			"copy",
			"csv",
			"xls",
			{
				"sExtends": "pdf",
				"sPdfOrientation": "portrait"
			},
			"print"
			]
		},

		ajax: {
			url: './ajax_processors/reports/batch/wip_parts.php',
			data: {
				action: 'get_wip_parts',
				start_date: function (d) {return get_date_value('start_date');},
				end_date: function (d) {return get_date_value('end_date');},
				line_ID: function (d) {return $('#line_select option:selected').val();},
				status: function (d) {return $('#status_select option:selected').val();},
        		database: function (d) {return chosen_system.database_name;},
        		process_type: function (d) {return chosen_system.process_type;},
			},
			dataSrc: '',
		},

		order: [[4, 'desc']],

		columns: [
		//{data: 'ID'},
		
		{data: 'external_serial', title: 'Serial #'},
		{data: 'part_number', title: 'Part #'},
		{data: 'part_desc', title: 'Description'},
		{data: 'line_desc', title:'Line'},
		{data: 'last_station_completed_desc', title: 'Last Station'},
		{
			data: 'created_at',
			title: 'Start Time',
			render: render_date
		},
		{
			title: 'Actions',
			className: 'actions',
			orderable: false,
			data: null,
			defaultContent: '',
			render: function (data, type, full, meta) {
				var btns = $('<div class="btn-group" role="group" aria-label="..."></div>');
				var bc_btn = $('<button type="button" class="btn btn-xs btn-default btn-bcert">Birth Certificate</button>');
				btns.append(bc_btn);
				return btns[0].outerHTML;
			}
		},
		]
	});
}

function render_date (data, type, full, meta) {
	if (data) {
		return data.date;
	} else {
		return '';
	}
}

function load_lines (params, callback) {
    if (params) {
        params.action = 'get_lines';
    } else {
        params = {action: 'get_lines'};
    }

    $.getJSON(
        'ajax_processors/filter.php',
        params,
        callback
    );
}

function show_lines (data) {
    $('#line_select').empty();
    if (!data) {return;}

    var el = $('<option name="line_ID"></option>');
        el.prop('id', 'line_default')
          .val(-1)
          .html('All Lines');
        $('#line_select').append(el);

    $.each(data, function (i, line) {
        var el = $('<option name="line_ID"></option>');
        el.prop('id', 'line_'+line.line_ID)
          .val(line.line_ID)
          .html(line.line_desc);

        $('#line_select').append(el);
    });
}