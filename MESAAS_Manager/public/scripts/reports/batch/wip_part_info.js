BWPBC = NYSUS.COMMON.BC;

$(document).ready(function () {
    BWPBC.init('main_div', './ajax_processors/reports/batch/wip_part_info.php');
});

BWPBC.init = function (element_ID, processor) {
    BWPBC.processor = processor;
    BWPBC.main_div = $('#'+element_ID);
    BWPBC.wip_part_ID = BWPBC.main_div.data('wip-part-id');
    BWPBC.process_type = BWPBC.main_div.data('process-type');
    BWPBC.database = BWPBC.main_div.data('database');

    BWPBC.sections = [
        {el_id: 'part_info', title: 'Part Overview', loader: BWPBC.load_wip_part, callback: BWPBC.show_wip_part},
        {el_id: 'station_history', title: 'Station History', loader: BWPBC.load_station_history, callback: BWPBC.show_station_history},
        {el_id: 'process_data', title: 'Process Data', loader: BWPBC.load_process_data, callback: BWPBC.show_process_data}
    ];

    BWPBC.set_title();
    BWPBC.build_sections();
    BWPBC.load_data();
};

BWPBC.set_title = function () {
    $('<div>Part Birth Certificate (Serial: <span class="serial"></span>, Type: <span class="part-desc"></span>)</div>').addClass('main-header').appendTo(BWPBC.main_div);
};

BWPBC.load_wip_part = function (cb) {
    $.getJSON(BWPBC.processor, {
        action: 'get_wip_part',
        wip_part_ID: BWPBC.wip_part_ID
    }).done(function (data) {
        if (data) {
            BWPBC.wip_part = data;
            cb(BWPBC.wip_part);
        }
    });
};

BWPBC.show_wip_part = function (wip_part) {
    var section = $('#part_info', BWPBC.main_div);
    var content = $('.section-content', section);

    var table = $('<table></table>').prop({id: 'part_info_table'}).addClass('section-table').appendTo(content);

    var tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Serial:</label> <span class="serial"></span></td>');
    tr.append('<td><label>Line:</label> <span class="line"></span></td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Last Station:</label> <span class="last-station"></span></td>');
    tr.append('<td><label>Built Time:</label> <span class="built-time"></span></td>');

    $('.serial').html(wip_part.serial_number);
    $('.part-desc').html(wip_part.part_desc);
    $('.built-time').html(wip_part.built_time ? wip_part.built_time.date : '');
    $('.line').html(wip_part.line_code);
    $('.last-station').html(wip_part.last_completed_station_desc);
    section.show();
};

BWPBC.load_station_history = function (cb) {
    $.getJSON(BWPBC.processor, {
        action: 'get_station_history',
        wip_part_ID: BWPBC.wip_part_ID
    }).done(function (data) {
        if (data) {
            BWPBC.station_history = data;
            cb(BWPBC.station_history);
        }
    });
};

BWPBC.show_station_history = function (station_history) {
    var columns = [
        {name: 'type_desc', title: 'Test Type'},
        {name: 'entry_time', title: 'Start Time', render: BWPBC.render_datetime},
        {name: 'exit_time', title: 'End Time', render: BWPBC.render_datetime},
        {name: 'completion_details', title: 'Completion_details'}
    ];

    var section = $('#station_history', BWPBC.main_div);
    var content = $('.section-content', section);

    station_history.forEach(function (station) {
        var station_div = $('<div></div>').prop({id:'station_history_'+station.ID}).appendTo(content);
        $('<div></div>').html(station.station_desc + ' (' + station.operator_name + ')').addClass('sub-table-title').appendTo(station_div);
        var history_div = $('<div></div>').appendTo(station_div);
        BWPBC.build_table(history_div, columns, station.instruction_history);
    });

    section.show();
};

BWPBC.load_process_data = function (cb) {
    $.getJSON(BWPBC.processor, {
        action: 'get_process_data',
        wip_part_ID: BWPBC.wip_part_ID
    }).done(function (data) {
        if (data) {
            BWPBC.process_data = data;
            cb(BWPBC.process_data);
        }
    });
};

BWPBC.show_process_data = function (process_data) {
    var columns = [
        {name: 'process_location', title: 'Location'},
        {name: 'process_desc', title: 'Name'},
        {name: 'process_value', title: 'Value'},
        {name: 'process_timestamp', title: 'Time', render: BWPBC.render_datetime}
    ];

    var section = $('#process_data', BWPBC.main_div);
    var content = $('.section-content', section);
    BWPBC.build_table(content, columns, process_data);
    section.show();
};
