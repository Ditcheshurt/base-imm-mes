BBPBC = NYSUS.COMMON.BC;

$(document).ready(function () {
    BBPBC.init('main_div', './ajax_processors/reports/batch/built_part_info.php');
});

BBPBC.init = function (element_ID, processor) {
    BBPBC.processor = processor;
    BBPBC.main_div = $('#'+element_ID);
    BBPBC.built_part_ID = BBPBC.main_div.data('built-part-id');

    BBPBC.sections = [
        {el_id: 'part_info', title: 'Part Overview', loader: BBPBC.load_built_part, callback: BBPBC.show_built_part},
        {el_id: 'station_history', title: 'Station History', loader: BBPBC.load_station_history, callback: BBPBC.show_station_history},
        {el_id: 'process_data', title: 'Process Data', loader: BBPBC.load_process_data, callback: BBPBC.show_process_data}
    ];

    BBPBC.set_title();
    BBPBC.build_sections();
    BBPBC.load_data();
};

BBPBC.set_title = function () {
    $('<div>Part Birth Certificate (Serial: <span class="serial"></span>, Type: <span class="part-desc"></span>)</div>').addClass('main-header').appendTo(BBPBC.main_div);
};

BBPBC.load_built_part = function (cb) {
    $.getJSON(BBPBC.processor, {
        action: 'get_built_part',
        built_part_ID: BBPBC.built_part_ID
    }).done(function (data) {
        if (data) {
            BBPBC.built_part = data;
            cb(BBPBC.built_part);
        }
    });
};

BBPBC.show_built_part = function (built_part) {
    var section = $('#part_info', BBPBC.main_div);
    var content = $('.section-content', section);

    var table = $('<table></table>').prop({id: 'part_info_table'}).addClass('section-table').appendTo(content);

    var tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Serial:</label> <span class="serial"></span></td>');
    tr.append('<td><label>Line:</label> <span class="line"></span></td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Last Station:</label> <span class="last-station"></span></td>');
    tr.append('<td><label>Built Time:</label> <span class="built-time"></span></td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Color:</label> <span class="part-color"></span></td>');
    tr.append('<td><label>Revision Level:</label> <span class="part-rev-level"></span></td>');

    $('.serial').html(built_part.serial);
    $('.part-desc').html(built_part.part_desc);
    $('.built-time').html(built_part.built_time ? built_part.built_time.date : '');
    $('.line').html(built_part.cell_desc);
    $('.last-station').html(built_part.last_completed_station_ID + ' - ' + built_part.last_completed_station_desc);
    $('.part-color').html(built_part.part_color);
    $('.part-rev-level').html(built_part.part_rev_level);
    section.show();
};

BBPBC.load_station_history = function (cb) {
    $.getJSON(BBPBC.processor, {
        action: 'get_station_history',
        built_part_ID: BBPBC.built_part_ID
    }).done(function (data) {
        if (data) {
            BBPBC.station_history = data;
            cb(BBPBC.station_history);
        }
    });
};

BBPBC.show_station_history = function (station_history) {
    var columns = [
        {name: 'type_desc', title: 'Test Type'},
        {name: 'operator_name', title: 'Operator'},
        {name: 'entry_time', title: 'Start Time', render: BBPBC.render_datetime},
        {name: 'exit_time', title: 'End Time', render: BBPBC.render_datetime},
        {name: 'completion_details', title: 'Completion_details'}
    ];

    var section = $('#station_history', BBPBC.main_div);
    var content = $('.section-content', section);

    station_history.forEach(function (station) {
        var station_div = $('<div></div>').prop({id:'station_history_'+station.ID}).appendTo(content);
        $('<div></div>').html(station.station_desc).addClass('sub-table-title').appendTo(station_div);
        var history_div = $('<div></div>').appendTo(station_div);
        BBPBC.build_table(history_div, columns, station.instruction_history);
    });

    section.show();
};

BBPBC.load_process_data = function (cb) {
    $.getJSON(BBPBC.processor, {
        action: 'get_process_data',
        built_part_ID: BBPBC.built_part_ID
    }).done(function (data) {
        if (data) {
            BBPBC.process_data = data;
            cb(BBPBC.process_data);
        }
    });
};

BBPBC.show_process_data = function (process_data) {
    var columns = [
        {name: 'process_location', title: 'Location'},
        {name: 'process_desc', title: 'Name'},
        {name: 'process_value', title: 'Value'},
        {name: 'process_timestamp', title: 'Time', render: BBPBC.render_datetime}
    ];

    var section = $('#process_data', BBPBC.main_div);
    var content = $('.section-content', section);
    BBPBC.build_table(content, columns, process_data);
    section.show();
};

