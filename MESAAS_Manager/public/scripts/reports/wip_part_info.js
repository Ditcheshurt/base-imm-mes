var processor = './ajax_processors/report_wip_part_info.php';

var wip_part_id;
var process_type;
var database;

$(document).ready(function () {
    wip_part_id = $('#info_table').data('wip-part-id');
    process_type = $('#info_table').data('process-type');
    database = $('#info_table').data('database');
    //load_info(wip_part_id, show_info);
    $.getJSON(
        processor,
        {
            action: 'load_data',
            wip_part_id: wip_part_id,
            process_type: process_type,
            database: database
        }, 
        show_info
    );
});

// function load_info (wip_part_id, callback) {
//     $.getJSON(processor, {action: 'load_data', wip_part_id: wip_part_id}, callback);
// }

// function load_info (params, callback) {
//     if (params) {
//         params.action = 'load_data';
//     } else {
//         params = {action: 'load_data'};
//     }

//     $.getJSON(processor, params, callback);
// }

function show_info (data) {
    show_wip_part_info(data.wip_part_info[0]);

    if (process_type == 'SEQUENCED') {
        show_module_info(data.module_info[0]);
    }
    
    show_station_history_info(data.station_history);
    if (data.process_data != undefined && data.process_data) {
        show_process_data(data.process_data);
    }
}

function show_wip_part_info (wip_part) {
    $('.serial').html(wip_part.serial_number);
    $('.part-desc').html(wip_part.part_desc);
    $('.built-time').html(wip_part.built_time ? wip_part.built_time.date : '');
    $('.line').html(wip_part.line_code);
    $('.last-station').html(wip_part.last_completed_station_desc);
}

function show_module_info (module) {    
    $('.vin').html(module.VIN);
    $('.sequence').html(module.sequence);
    $('.build-order').html(module.build_order);
    $('.module-line').html(module.line_code);

    $('#module_info').show();
    $('.sub-table.module-info').show();
}

function show_station_history_info (station_history) {
    var tbody = $('#station_instruction_history_table tbody');

    $.each(station_history, function (i, station) {
        if (station.station_desc) {
            var station_row = $('<tr></tr>');
            station_row.append('<td>'+station.station_desc+'</td>');
            //station_row.append('<td>'+(station.entry_time ? station.entry_time.date : '')+'</td>');
            //station_row.append('<td>'+(station.exit_time ? station.exit_time.date : '')+'</td>');
            station_row.append('<td>'+station.operator_name+'</td>');
            tbody.append(station_row);

            if (station.instruction_history) {
                var instructions_row = $('<tr><td colspan="4"></td></tr>');

                var instruction_table = $('<table class="sub-table"></table>');
                var instruction_thead = $(
                    '<thead>' +
                      '<tr>' +
                        '<th>Test Type</th>' +
                        '<th>Start Time</th>' +
                        '<th>End Time</th>' +
                        '<th>Completion Details</th>' +
                      '</tr>' +
                    '</thead>'
                );
                var instruction_tbody = $('<tbody></tbody>');
            
                $.each(station.instruction_history, function (j, instruction) {
                    var instruction_row = $('<tr></tr>');
                    instruction_row.append('<td>'+(instruction.type_desc ? instruction.type_desc : '')+'</td>');
                    instruction_row.append('<td>'+(instruction.entry_time ? instruction.entry_time.date : '')+'</td>');
                    instruction_row.append('<td>'+(instruction.exit_time ? instruction.exit_time.date : '')+'</td>');
                    instruction_row.append('<td>'+instruction.completion_details+'</td>');
                    instruction_tbody.append(instruction_row);
                });

                instruction_table.append(instruction_thead)
                                 .append(instruction_tbody);

                instructions_row.append(instruction_table);
                tbody.append(instructions_row);
            }
        }
    });
}

function show_process_data(process_data) {
    var tbody = $('#process_data_table tbody');

    $.each(process_data, function (j, data) {
        var row = $('<tr></tr>');
        row.append('<td>'+data.process_location+'</td>');
        row.append('<td>'+data.process_desc+'</td>');
        row.append('<td>'+data.process_value+'</td>');
        row.append('<td>'+(data.process_timestamp ? data.process_timestamp.date : '')+'</td>');
        tbody.append(row);
    });

    $('#process_data_table').show(true);
    $('#process_data').show(true);
}