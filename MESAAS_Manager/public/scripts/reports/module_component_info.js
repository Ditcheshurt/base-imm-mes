var processor = './ajax_processors/report_module_component_info.php';

var module_ID;

$(document).ready(function () {
    module_ID = $('#info_table').data('module-id');
    load_info(module_ID, show_info);
});

function load_info (module_ID, callback) {
    $.getJSON(processor, {action: 'load_data', module_ID: module_ID}, callback);
}

function show_info (data) {
    show_module_info(data.module_info[0]);
    show_component_info(data.component_info);
}

function show_module_info (module) {
    $('.vin').html(module.VIN);
    $('.sequence').html(module.sequence);
    $('.broadcast-time').html(module.recvd_time ? module.recvd_time.date : '');
    $('.built-time').html(module.built_time ? module.built_time.date : '');
}

function show_broadcast_info (broadcast) {
    $('.preview-broadcast-time').html(broadcast.preview_broadcast_time);
    $('.build-broadcast-time').html(broadcast.build_broadcast_time);
}

function show_component_info (component) {
    var cols = [
        {data: 'part_number', title: 'Part Number'},
        {data: 'qty', title: 'Quantity'}
    ];
    
    fill_table_by_cols('component_table', cols, component, true);
}
