var processor = './ajax_processors/reports/tool_change.php';

var system_select;
var machine_select;
var start_date_input;
var end_date_input;
var shift_select;
var chosen_system;
var table;
var production_panel;
var filter_run_btn;
var defectTable;

var tmpDataPT = 0;

$(document).ready(function () {
	system_select = $('#system_select');
	machine_select = $('#machine_select');
	start_date_input = $('#start_date');
	end_data_input = $('#end_date');
	
	production_panel = $('#production_panel');
    filter_run_btn = $('#filter_panel .btn-run');

    filter_run_btn.prop('disabled', true);

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    system_select.on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_time('start_date', true);
    init_date_time('end_date', false);

	// run report button
	$('body').on('click', '.btn-run', load_tool_change_data);
});

function load_filter_options () {
    filter_run_btn.prop('disabled', true);
    var system_id = system_select.find('option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    //$('#machine_select').empty();
    machine_select.nysus_select({
        name: 'machine_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_machines', database: chosen_system.database_name},
        render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		placeholder: 'Select Machine',
        callback: function (data) {
            
        }
    });
	
	machine_select.on('change', function(e) {
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			filter_run_btn.prop('disabled', true);
		} else {
			filter_run_btn.prop('disabled', false);
		}
	});
}

function load_tool_change_data () {
	$('#div_parameter_chart').empty();
	$('body .btn-run').html('Loading...');
	$('body .btn-run').addClass('disabled');
	var sdate = -1;
	var edate = -1;

	try {
		sdate = get_date_time('start_date');
		edate = get_date_time('end_date');
		var params = {
			action: 'get_tool_change_data',
			start_date: sdate,
			end_date: edate,
			database: chosen_system.database_name,
			machine_id: $('#machine_select option:selected').val(),
			system_id: function (d) {return $('#system_select option:selected').val();},
		};

		$.getJSON(processor, params)
			.done(generateToolChangeChart)
			.fail(function () {
				console.error('Unable to load param data.');
			});
	} catch (e) {
		if (sdate == -1) {
			$('.start_date').focus();
		}
		if (edate == -1) {
			$('.end_date').focus();
		}
		$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
		$('body .btn-run').removeClass('disabled');		
	}
}

function build_combo(combo, data, attr, selection) {
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function generateToolChangeChart(jso) {
	var data = [];
	var cols = [];
	var ucl = 1000;
	var lcl = 0;
	
	$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
	$('body .btn-run').removeClass('disabled');
	
	if (defectTable != null) {
		//defectTable.clear().draw();
        //defectTable.destroy();    //so the thing will update
        $('#parameter_table').find('.panel-body').empty();
    }
	var $grid_view = $('#parameter_table');
	var $table = $('<table class="table table-condensed table-bordered table-striped table-display"></table>');
	$table.appendTo($grid_view.find('.panel-body'));
/*
T	ID: 54302
T	change_state: "START"
T	change_time: {date: "2018-10-09 13:38:54.283000", timezone_type: 3, timezone: "America/Tegucigalpa"}
x	machine_ID: 3
x	machine_number: "KM03"
T	reason: "Mold Change"
T	record_end_time: {date: "2018-10-09 13:39:18.283000", timezone_type: 3, timezone: "America/Tegucigalpa"}
x	short_description: "307"
x	tool_ID: 9
x	tool_change_ID: 54302
*/	
	if(jso.length > 0) {
		for(i=0;i<jso.length;i++){
			var d = jso[i];
			var idx = data.findIndex(o => o.tool_change_ID == d.tool_change_ID);
			var rec = {
					ID: d.ID,
					state: d.change_state,
					reason: d.reason,
					start_ts: d.change_time.date.substr(0,d.change_time.date.length-7),
					end_ts: d.record_end_time.date.substr(0,d.record_end_time.date.length-7),
					duration: d.duration
				};
			if (idx >= 0) {
				// tool chng ID exists add record
				data[idx]['records'].push(rec);
			} else {
				var t = {
					tool_change_ID: d.tool_change_ID,
					desc: d.change_time.date.substr(0,d.change_time.date.length-7) + ' (' + 'Tool ' + d.short_description + ')',
					records: []
				};
				t['records'].push(rec);
				data.push(t);
			}
		}
		
		var c = $('<div />').attr('id', 'div_chart_1').appendTo($('#div_parameter_chart')).addClass('parameter-container');
		var cont = $('#div_chart_1')[0];
		var groups = new vis.DataSet();
		var items = new vis.DataSet();
		for (i=0;i<data.length;i++){
			var d = data[i];
			groups.add({
				id:d.tool_change_ID,
				content:d.desc,
				subgroupOrder: function (a,b) {return a.subgroupOrder - b.subgroupOrder;},
				subgroupStack: {'STARTEND':false,'PAUSE':false,'RESUME':false}
			});
			for (j=0;j<data[i].records.length;j++){
				var r = d.records[j];
				var ri = {
					id:r.ID,
					start: new Date(r.start_ts),
					group: d.tool_change_ID,
					subgroup: r.state.replace(/START|END/g,"STARTEND"),
					subgrouporder: (['STARTEND','PAUSE','RESUME'].indexOf(r.state.replace(/START|END/g,"STARTEND")))
				};
				if (r.state == 'PAUSE'){
					ri.end = new Date(r.end_ts);
					ri.content = '<div>'+r.state+'<br>Reason: '+r.reason+'<br>Duration: '+r.duration+' sec</div>';
				} else {
					ri.content = '<div>'+r.state+'</div>';
				}
				items.add(ri);
			}
		}
		var opts = {
			height: 400,
			stack: false,
			stackSubgroups: true,
			verticalScroll: true,
			min: new Date(get_date_time('start_date')),
			max: new Date(get_date_time('end_date')),
			zoomKey: 'ctrlKey',
			zoomMin: 1000*60*5,
			groupOrder: function (a, b) {
				return a.id - b.id;
			}
		};
		var timeline = new vis.Timeline(cont);
		timeline.setOptions(opts);
		timeline.setGroups(groups);
		timeline.setItems(items);
		
		// for(j=0;j<data.length; j++){
			// var adj_pts = Math.floor(data[j].data_points.length/1000)+1;
			// var cont = $('#div_chart_' + data[j].data_point_ID);
			// if (cont.length == 0) {
				// cont = $('<div />').attr('id', 'div_chart_' + data[j].data_point_ID).appendTo($('#div_parameter_chart')).addClass('parameter-container');
			// }
			
			// if (data[j].ucl) {
				// ucl = data[j].ucl;
			// }
			// if (data[j].lcl) {
				// lcl = data[j].lcl;
			// }
			
			// cont.highcharts({
				// chart: {
					// type: 'bar'
				// },
				// title: {
					// text: data[j].data_point_desc,
					// enabled: true
				// },
				// credits: {
					// enabled: false
				// },
				// xAxis: {
					// categories: [],
					// labels: {enabled: false},
					// tickInterval: adj_pts*1000,
					// visible: false
				// },
				// yAxis: {
					// plotBands: [
						// {
							// from: ucl,
							// to: 10000,
							// color: 'rgba(255, 0, 0, 0.1)'
						// },
						// {
							// from: -10000,
							// to: lcl,
							// color: 'rgba(255, 0, 0, 0.1)'
						// }
					// ]
				// },
				// legend: {
					// shadow: true,
					// enabled: true
				// },
				// tooltip: {
					// shared: true
				// },
				// plotOptions: {
					// line: {
						// grouping: false,
						// shadow: true,
						// borderWidth: 0,
						// turboThreshold:5000
					// }
				// },
				// series: [{
					// name: 'Values',
					// color: 'rgba(22,170,2,.7)',
					// data: [],
					// pointInterval: 100,
					// pointPlacement: 0,
					// dataLabels: { enabled: false },
					// point: {
						// events: {
							// mouseOver: function() {
								// console.log('x: ' + this.x + ', y: ' + this.y);
								// console.log(this);
							// }
						// }
					// }
				// },{
					// name: 'Upper Limit',
					// color: 'red',
					// data: [],
					// pointInterval: 100,
					// pointPlacement: 0,
					// dataLabels: { enabled: false },
					// marker: {radius: 1}
				// },{
					// name: 'Lower Limit',
					// color: 'red',
					// data: [],
					// pointInterval: 100,
					// pointPlacement: 0,
					// dataLabels: { enabled: false },
					// marker: {radius: 1}
				// }]
			// });


			// var arr = [];
			// var arr2 = [];
			// var arr3 = [];

			// for (var k = 0; k < data[j].data_points.length; k++) {
				// var x = data[j].data_points[k].cycle_id;
				// var y = data[j].data_points[k].data_point_value;
				// var c = 'limegreen';
				// if (y < lcl || y > ucl) {
					// c = 'red';
				// }
				// arr.push({y:parseFloat(y.toFixed(3)), name:('Cycle ID: ' + x), color:c});
				// arr2.push(ucl);
				// arr3.push(lcl);
			// }
			// var chart = cont.highcharts();
			// chart.setTitle(data[j].data_point_desc);
			// chart.series[0].setData(arr);
			// chart.series[1].setData(arr2);
			// chart.series[2].setData(arr3);
			// chart.redraw();
		// }
		
	} else {
		$('<div />').html("No data found").appendTo($('#div_parameter_chart'));
	}
	$('#parameter_panel').show();
}

function init_date_time (id,start_nEND) {
	// initialize the date input to the current date
	if (start_nEND === undefined) {start_nEND = true;}
	
	var today = new Date();
	if (start_nEND) {
		today.setHours(0,0,0,0);
	} else {
		today.setHours(23,59,0,0);
	}
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().slice(0,-1);
	$('#'+id).val(today_str);
}

function get_date_time (id) {
	var date_str = $('#'+id).val();
	var objDT = new Date(date_str);
	var offset = objDT.getTimezoneOffset() * 60;
	objDT = new Date(objDT.getTime() - offset * 1000);
	return objDT.toISOString().split('T')[0].replace(/\-/g,'/') + ' ' + objDT.toISOString().split('T')[1].slice(0,8);
}

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};