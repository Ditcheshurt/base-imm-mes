var processor = './ajax_processors/report_pick_rack_contents.php';
var table;

$(document).ready(function () {

    // date range filtering
    // var today = new Date();
    // var offset = today.getTimezoneOffset() * 60;
    // var offset_date = new Date(today.getTime() - offset * 1000);
    // var today_str = offset_date.toISOString().split('T')[0];
    // $('#start_date').val(today_str);
    // $('#end_date').val(today_str);

    load_pick_racks(show_pick_racks);

    $('.btn-run').on('click', function () {
        load_data();
    });

});

// function get_date_value (id) {
//     var date_str = $('#'+id).val();
//     return new Date(date_str).toISOString().split('T')[0];
// }

function load_data() {
    // var start_date_str = $('#start_date').val();
    // var end_date_str = $('#end_date').val();
    // var start_date = new Date(start_date_str).toISOString().split('T')[0];
    // var end_date = new Date(end_date_str).toISOString().split('T')[0];

    // table setup

    if (typeof table != 'undefined') {
        table.ajax.reload();
        return;
    }

    table = $('#pick_rack_contents_table').DataTable({
        dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"
            ]
        },

        ajax: {
            url: processor,
            data: {
                action: 'get_pick_rack_contents',
                pick_rack_number: function (d) {return $('#pick_rack_select option:selected').val();}
            },
            dataSrc: '',
        },

        order: [[4, 'asc']],

        columns: [
            //{data: 'ID'},
            {data: 'build_order', title: 'Build Order'},
            {data: 'sequence', title: 'Sequence'},
            {data: 'serial', title: 'Serial'},
            {data: 'pick_rack_number', title: 'Rack Number'},
            {data: 'pocket', title: 'Pocket Number'},
            {data: 'status', title: 'Status'}
        ]
    });
}

function render_date (data, type, full, meta) {
    if (data) {
        return data.date;
    } else {
        return '';
    }
}

function load_pick_racks (callback) {
    $.getJSON(
        processor,
        {action: 'get_pick_racks'},
        callback
    );
}

function show_pick_racks (data) {
    $.each(data, function (i, pick_rack) {
        var el = $('<option name="pick_rack_number"></option>');
        el.prop('id', 'pick_rack_'+pick_rack.pick_rack_number)
          .val(pick_rack.pick_rack_number)
          .html(pick_rack.pick_rack_number);

        $('#pick_rack_select').append(el);
    });
}