var processor = './ajax_processors/reports/carrier_status.php';

$(document).ready(
	loadData
);

function loadData() {
	$.getJSON(processor, {
		'action':'get_carrier_status'
	}, populate);	
}

function populate(jso) {
	// display heading
	var d = new Date().toLocaleString();
	var heading =  'Carrier Status Report ' + ' Generated: '+ d;
	if (jso.production_qty != null) {
		 heading = jso.production_qty[0].line_code + ' - Report ' + ' Generated: '+ d;
	}
	$('#ph').html(heading);
		
	// carrier status table
	var cols = ['Carrier', 'Prior Station Complete', 'Current Station', 'Build Order', 'Sequence'];	
	// table header
	th = $('#table-carriers .thead');
	$.each(cols, function(k,v) {
		th.append('<th>' + v + '</th>');
	});	
	var t = $('#table-carriers .tbody');
	$.each(jso.carrier_status, function(k,v) {
		var tr = '<tr>';		
		tr += '<td>'+ (v.carrier ? v.carrier : '') +'</td>';
		tr += '<td>'+ (v.prior_station_complete ? v.prior_station_complete : '') +'</td>';
		tr += '<td>'+ (v.current_station ? v.current_station : '') +'</td>';
		tr += '<td><div class="current_module" rel="popover">'+ (v.current_module ? v.current_module : '') +'</div></td>';		
		tr += '<td>'+ (v.sequence ? v.sequence : '') +'</td>';
		tr += '</tr>';
		t.append(tr);
		$('#table-carriers tr:last').data('module_id', v.module_id);

	});
	
	$('#table-carriers').on('click', '.current_module', function(el) {
        var selection = $(this);
        selection.focus();

        if (!selection.data('popover-shown')) {
            var module_id = selection.closest('tr').data('module_id');

            $.getJSON(processor, {
                'action':'get_module_detail',
                'module_id': module_id
            }, function (jso) {populateModuleDetail(selection, jso);}); 
        } else {
            selection.popover('hide');
            selection.data('popover-shown', false);
        }
	});
}

function populateModuleDetail(ctrl, jso) {
    jso = jso.module_detail;
    var title = 'Module ' + jso[0].build_order;
    var markup = '<ul>';
    markup += '<li>KitBox ' + jso[0].container_id + '</li>';
    $.each(jso, function(k,v) {     
        markup += '<li>' + v.internal_serial + ' - ' + v.part_desc + '</li>';
    });
    markup += '</ul>';

    ctrl.popover({
        title: title,
        content: markup,
        html: true,
        placement: 'bottom'
    }).popover('show');
    ctrl.data('popover-shown', true);
    //ctrl.html(markup);
}