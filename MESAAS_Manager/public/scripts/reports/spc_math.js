//spc_math.js

//all SPC math-related functions



function getA2(n) {
	var arr = [0,
					0,
					1.886, //n=2
					1.023,
					0.729,
					0.577, //n=5
					0.483,
					0.419,
					0.373,
					0.337,
					0.308, //n=10
					0.285,
					0.266,
					0.249,
					0.235,
					0.223, //n=15
					0.212,
					0.203,
					0.194,
					0.187,
					0.180, //n=20
					0.173,
					0.167,
					0.162,
					0.157,
					0.153]; //n=25
	return arr[n];
}

function getD3(n) {
	var arr = [0,
					0,
					0, //n=2
					0,
					0,
					0, //n=5
					0,
					0.076,
					0.136,
					0.184,
					0.223, //n=10
					0.256,
					0.283,
					0.307,
					0.328,
					0.347, //n=15
					0.363,
					0.378,
					0.391,
					0.403,
					0.415, //n=20
					0.425,
					0.434,
					0.443,
					0.451,
					0.459]; //n=25
	return arr[n];
}

function getD4(n) {
	var arr = [0,
					3.267,
					3.267, //n=2
					2.574,
					2.282,
					2.114, //n=5
					2.004,
					1.924,
					1.864,
					1.816,
					1.777, //n=10
					1.744,
					1.717,
					1.693,
					1.672,
					1.653, //n=15
					1.637,
					1.622,
					1.608,
					1.597,
					1.585, //n=20
					1.575,
					1.566,
					1.557,
					1.548,
					1.541]; //n=25
	return arr[n];
}

function getD2(n) {

	var arr = [0,
					0,
					1.128, //n=2
					1.693,
					2.059,
					2.326, //n=5
					2.534,
					2.704,
					2.847,
					2.970,
					3.078, //n=10
					3.173,
					3.258,
					3.336,
					3.407,
					3.472, //n=15
					3.532,
					3.588,
					3.640,
					3.689,
					3.735, //n=20
					3.778,
					3.819,
					3.858,
					3.895,
					3.931]; //n=25

	return arr[n];

}

function getC4(n) {

	var arr = [0,
					0,
					0.798, //n=2
					0.886,
					0.921,
					0.940, //n=5
					0.952,
					0.959,
					0.965,
					0.969,
					0.973, //n=10
					0.975,
					0.978,
					0.979,
					0.981,
					0.982, //n=15
					0.983,
					0.985,
					0.985,
					0.986,
					0.987, //n=20
					0.988,
					0.988,
					0.989,
					0.989,
					0.990]; //n=25

	return arr[n];

}

var isArray = function (obj) {
	return Object.prototype.toString.call(obj) === "[object Array]";
},
getNumWithSetDec = function( num, numOfDec ){
	var pow10s = Math.pow( 10, numOfDec || 0 );
	return ( numOfDec ) ? Math.round( pow10s * num ) / pow10s : num;
},
getAverageFromNumArr = function( numArr, numOfDec ){
	if( !isArray( numArr ) ){ return false;	}
	var i = numArr.length,
		sum = 0;
	while( i-- ){
		sum += numArr[ i ];
	}
	return getNumWithSetDec( (sum / numArr.length ), numOfDec );
},
getVariance = function( numArr, numOfDec ){
	if( !isArray(numArr) ){ return false; }
	var avg = getAverageFromNumArr( numArr, numOfDec ),
		i = numArr.length,
		v = 0;

	while( i-- ){
		v += Math.pow( (numArr[ i ] - avg), 2 );
	}
	v /= numArr.length;
	return getNumWithSetDec( v, numOfDec );
},
getStandardDeviation = function( numArr, numOfDec ){
	if( !isArray(numArr) ){ return false; }
	var stdDev = Math.sqrt( getVariance( numArr, numOfDec ) );
	return getNumWithSetDec( stdDev, numOfDec );
};

function roundIt(num) {
	return (Math.round(num*100)/100);
}

function roundIt4(num) {
	return (Math.round(num*10000)/10000);
}