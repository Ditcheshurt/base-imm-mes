utility = NYSUS.COMMON.UTILITY;
report = NYSUS.COMMON.REPORT.PRINTABLE;

$(document).ready(function () {
	report.sections = [
		{el_id: 'operator_overview', title: 'Operator Overview', loader: report.load_operator_overview, callback: report.show_operator_overview},
		{el_id: 'operator_login_history', title: 'Operator Login/Logout History', loader: report.load_login_history, callback: report.show_login_history},
		{el_id: 'operator_average_performance', title: 'Average Performance', loader: report.load_average_shift_summary, callback: report.show_average_shift_summary},
		{el_id: 'operator_performance', title: 'Performance Detail', loader: report.load_shift_summaries, callback: report.show_shift_summaries},
	];

	report.init('#main_div', './ajax_processors/reports/operator_report_card.php');
});

report.init = function (element_selector, processor) {
	report.processor = processor;
	report.main_div = $(element_selector);

	report.key = report.main_div.data('operator-id');
	report.start_date = report.main_div.data('start-date');
	report.end_date = report.main_div.data('end-date');

    var report_title = 'Operator Report Card ('+report.start_date+' - '+report.end_date+')';
    report.set_title(report_title);
    report.build_sections();
    report.load_data();
};

report.load_operator_overview = function (cb) {
    $.getJSON(report.processor, {
        action: 'get_operator',
        operator_ID: report.key
    }).done(function (data) {
		report.operator = data;
		cb(report.operator);
    });
};

report.show_operator_overview = function (operator) {
    var section = $('#operator_overview', report.main_div);
    var content = $('.section-content', section);

    var table = $('<table></table>').prop({id: 'operator_overview_table'}).addClass('section-table').appendTo(content);

    var tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Operator Name:</label> <span class="operator-name"></span></td>');
    //tr.append('<td><label>Part Description:</label> <span class="part-description"></span></td>');

    tr = $('<tr></tr>').appendTo(table);
    tr.append('<td><label>Badge ID:</label> <span class="badge-id"></span></td>');
    //tr.append('<td><label>Packout Time:</label> <span class="packout-time"></span></td>');

    var name = operator.first_name + ' ' + operator.last_name;
    $('.operator-name').html(name);
    $('.badge-id').html(operator.badge_ID);

    section.show();
};

report.load_login_history = function (cb) {
    $.getJSON(report.processor, {
        action: 'get_login_history',
        operator_ID: report.key,
        start_date: report.start_date,
        end_date: report.end_date
    }).done(function (data) {
		report.login_history = data;
		cb(report.login_history);
    });
};

report.show_login_history = function (login_history) {
    var columns = [
    	{name: 'event_date', title: 'Date', render: report.render_date},
    	{name: 'machine_name', title: 'Machine'},
    	{name: 'start_time', title: 'Login Time', render: report.render_datetime},
    	{name: 'end_time', title: 'Logout Time', render: report.render_datetime},
    	{name: 'duration', title: 'Operating Time', render: report.render_duration}
    ];

    var section = $('#operator_login_history', report.main_div);
    var content = $('.section-content', section);
    report.build_table(content, columns, login_history);
    section.show();
};

report.load_average_shift_summary = function (cb) {
    $.getJSON(report.processor, {
        action: 'get_average_shift_summary',
        operator_ID: report.key,
        start_date: report.start_date,
        end_date: report.end_date
    }).done(function (data) {
		report.average_shift_summary = data;
		cb(report.average_shift_summary);
    });
};

report.show_average_shift_summary = function (average_shift_summary) {
    var columns = [
    	{name: 'part_qty', title: 'Parts Made'},
    	{name: 'cycle_time', title: 'Average Cycle Time', render: report.render_duration},
    	{name: 'quality', title: 'Quality', render: report.render_percent},
    	{name: 'performance', title: 'Performance', render: report.render_percent},
    	{name: 'availability', title: 'Availability', render: report.render_percent},
    	{name: 'oee', title: 'OEE', render: report.render_percent}
    ];

    var section = $('#operator_average_performance', report.main_div);
    var content = $('.section-content', section);
    report.build_table(content, columns, average_shift_summary);
    section.show();
};

report.load_shift_summaries = function (cb) {
    $.getJSON(report.processor, {
        action: 'get_shift_summaries',
        operator_ID: report.key,
        start_date: report.start_date,
        end_date: report.end_date
    }).done(function (data) {
		report.shift_summaries = data;
		cb(report.shift_summaries);
    });
};

report.show_shift_summaries = function (shift_summaries) {
    var columns = [
    	{name: 'the_date', title: 'Date', render: report.render_date},
    	{name: 'machine_name', title: 'Machine'},
    	{name: 'part_qty', title: 'Parts Made'},
    	{name: 'avg_cycle_time', title: 'Average Cycle TIme', render: report.render_duration},
    	{name: 'quality', title: 'Quality', render: report.render_percent},
    	{name: 'performance', title: 'Performance', render: report.render_percent},
    	{name: 'availability', title: 'Availability', render: report.render_percent},
    	{name: 'oee', title: 'OEE', render: report.render_percent}
    ];

    var section = $('#operator_performance', report.main_div);
    var content = $('.section-content', section);
    report.build_table(content, columns, shift_summaries);
    section.show();
};


