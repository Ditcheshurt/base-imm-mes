var processor = './ajax_processors/reports/production_summary.php';
var selDate;
var systems;
var chosen_system;
var dtProductionTable;
var detailsTable;
// var daily_chart_panel;
// var hourly_chart_panel;
// var hourly_table_panel;
var wait_queue = [];

String.prototype.toProperCase = function() {
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g, 
		function($1){
			return $1.toUpperCase();
		}
	);
};

$(document).ajaxStop(function () {
	$('.btn-run').prop('disabled', false);
});

$(document).ready(function() {
    // build_panels();

    $('.btn-run').prop('disabled', true);
	var system_select = $('#system_select').nysus_select({
	    name: 'system_id',
	    key: 'ID',
	    data_url: 'ajax_processors/filter.php',
	    params: {action: 'get_systems'},
	    render: function (system) {return system.system_name;},
	    callback: function (data) {
	        systems = data;
	        load_filter_options();
	    }
	});

    system_select.on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

    $('.btn-run').on('click', function () {
		selDate = undefined;
        populateReport();
    });	
	
	$('#machine_select').change(function () {
		$('.btn-run').prop('disabled', true);
		$.getJSON(processor, {
			'action':'get_tools',
			'machine_ID':$('#machine_select').val()
		},	function(jso) {
			$('#tool_select').empty();		
			if (jso == null) {
				$('#tool_select').prop('disabled', true);
			}
			$('.btn-run').prop('disabled', true);	
			$('#tool_select').nysus_select({
				name: 'tool_id',
				key: 'ID',
				data_url: processor,
				params: {action: 'get_tools', database: chosen_system.database_name, machine_ID: $('#machine_select').val()},
				render: function (jso) {return jso.text;},
				placeholder: 'All',
				name: 'All'
			}).prop('disabled', false);
			$('#tool_select').change();
		});
    });
	
	$('#tool_select').change(function () {
		$('.btn-run').prop('disabled', true);
		$.getJSON(processor, {
			'action':'get_parts',
			'tool_ID':$('#tool_select').val()
		},	function(jso) {
			$('#part_select').empty();		
			if (jso == null) {
				$('#part_select').prop('disabled', true);
			}			
			$('.btn-run').prop('disabled', true);
			$('#part_select').nysus_select({
				name: 'part_id',
				key: 'ID',
				data_url: processor,
				params: {action: 'get_parts', database: chosen_system.database_name, tool_ID: $('#tool_select').val()},
				render: function (jso) {return jso.text;},
				placeholder: 'All',
				name: 'All'
			}).prop('disabled', false);
		});
    });

    //$('#operator_select').empty();
    $('.btn-run').prop('disabled', true);
	$('#operator_select').nysus_select({
	    name: 'operator_ID',
	    key: 'ID',
	    data_url: 'ajax_processors/filter.php',
	    params: {action: 'get_operators'},
	    render: function (jso) {return jso.name;},
	    placeholder: 'All'
	}).prop('disabled', false);

});

// function build_panels () {
//     daily_chart_panel = $('#daily_chart_panel').nysus_panel({
//         panel_title: 'Daily Production Quantities'
//     }).appendTo('#main_panel .panel-body');

//     hourly_chart_panel = $('#hourly_chart_panel').nysus_panel({
//         panel_title: 'Hourly Production'
//     }).appendTo('#main_panel .panel-body');

//     hourly_table_panel = $('#hourly_table_panel').nysus_panel({
//         panel_title: 'Production Summary Report'
//     }).appendTo('#main_panel .panel-body');
// }

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    if (chosen_system.process_type === 'SEQUENCED' || chosen_system.process_type === 'BATCH') {
        console.log(chosen_system);
        $('#line_select').empty();
        $('.btn-run').prop('disabled', true);
		$('#line_select').nysus_select({
		    name: 'line_id',
		    key: 'line_ID',
		    data_url: 'ajax_processors/filter.php',
		    params: {action: 'get_lines', system_id: chosen_system.ID},
		    render: function (line) {return line.line_desc;},
		}).prop('disabled', false);

        $('#line_select').empty().prop('disabled', false);
        $('#machine_select').empty().prop('disabled', true);
		
    } else if (chosen_system.process_type === 'REPETITIVE') {
        $('#machine_select').empty();
        $('.btn-run').prop('disabled', true);
        $('#machine_select').nysus_select({
            name: 'machine_id',
            key: 'ID',
            data_url: 'ajax_processors/filter.php',
            params: {action: 'get_machines', database: chosen_system.database_name},
            render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
            callback: function(data) {$('#machine_select').change();},
			placeholder: 'All',
			name: 'All'
        }).prop('disabled', false);

        $('#machine_select').empty().prop('disabled', false);
        $('#line_select').empty().prop('disabled', true);		
		
    }

	//show detail on cycleTimesTable click
	$('.table-prod-report tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = dtProductionTable.row(tr);
 
		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();
		}
		else {
			tr.addClass('shown');
			populate_details(row);
		}
	});
}

function populateReport() {
	var data = {
		'action':'daily_production_summary',
		'system_id':$('#system_select').val(),
		'line_id':$('#line_select').val(),        
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type		
	};
	if ($('#machine_select').val() != '') {
		data.machine_id = $('#machine_select').val();
	}
	if ($('#tool_select').val() != '') {
		data.tool_id = $('#tool_select').val();
	}
	if ($('#part_select').val() != '') {
		data.part_id = $('#part_select').val();
	}
	if ($('#shift_select').val() != '') {
		data.shift_id = $('#shift_select').val();
	}	
	if ($('#operator_select').val() != '') {
		data.operator_id = $('#operator_select').val();
	}
	
	//populateProductionChart
	$.getJSON(processor, data, populateDailyProductionChart);

	data.action = 'hourly_production_summary';
    //populateProductionChart
    $.getJSON(processor, data, populateHourlyProductionChart);
	
	data.action = 'production_detail';
	 //populateProductionTable
    $.getJSON(processor, data, populateHourlyProductionTable);
	
}

function populateDailyProductionChart(jso) {
    $('.panel-daily-chart').empty();
    if (!jso) {
		$('.panel-daily-chart').append('<span>No Results</span>');
		return;
	}

	// populate the cycle times chart	
	var categories = [];
	var data = [];	
	var fs = [];
	var ss= [];
	var ts = [];
	
	$.each(jso, function(k,v) {		
		categories.push(v.built_date);		
		fs.push(v.first_shift);
		ss.push(v.second_shift);
		ts.push(v.third_shift);		
	});
	data.push({                        
		name: 'First Shift',
		data: fs
	});
	data.push({                        
		name: 'Second Shift',
		data: ss
	});
	data.push({                        
		name: 'Third Shift',
		data: ts
	});
	
	console.log(data);
	
	// test data
	/*
	categories = ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas'];
	data = [{
            name: 'John',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Jane',
            data: [2, 2, 3, 2, 1]
        }, {
            name: 'Joe',
            data: [3, 4, 4, 2, 5]
        }];
	*/
	
	var title = 'Daily Production Summary :';
	title += $('#start_date').val() != $('#end_date').val() 
			? $('#start_date').val() + ' - ' + $('#end_date').val()
			:$('#start_date').val();
	
	//daily prod summary chart
	var options = {
        credits : {
            enabled : false
        },
		series: data,		
        chart: {
            type: 'column'
        },
        title: {
            text: title
        },
        xAxis: {
			title: {
                text: 'Date'
            }, 
			categories: categories,
            labels: {
                rotation: -60
            }
        },
        yAxis: {
			min: 0,
            title: {
                text: 'Built Quantity'
            },
			stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>' +
                    'Total: ' + this.point.stackTotal;
            }
        },
		legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
			},
			series: {
				cursor: 'pointer',
				point: {
					events: {
						click: function() {
							//console.log({this.category, this.y});
							selDate = this.category;
							
							//populateProductionChart
							$.getJSON(processor, {
								'action':'hourly_production_summary',
								'start_date':this.category,
								'end_date':this.category,
								'process_type': chosen_system.process_type,
								'system_id':$('#system_select').val(),
								'line_id':$('#line_select').val(),
								'machine_id':$('#machine_select').val()
							}, function(jso) { 
								populateHourlyProductionChart(jso);
								populateHourlyProductionTable(jso);
							});
						}
					}
				}
			}
		}
	}
	
	// populate the chart
	$('.panel-daily-chart').highcharts(options);
	
}

function populateHourlyProductionChart(jso) {
    $('.panel-hourly-chart').empty();
    if (!jso) {
		$('.panel-hourly-chart').append('<span>No Results</span>');
		return;
	}

	// populate the cycle times chart	
	var categories = [];
	var data = [];
	var title;
	var totalBuilt = 0;
	var avgPerHour = 0;
	var startDate = new Date($('#start_date').val() + ' 00:00:00');
	var endDate = new Date();
	var hours = Math.abs(startDate - endDate) / 36e5;
    console.log(hours);
	
	for (var i = 0; i <= 23; i++) {		
		data.push(0);		
	}
	$.each(jso, function(k,v) {	
		data[v.hour] = v.qty_built;
		totalBuilt += v.qty_built;
	});	
	
	if (totalBuilt > 0) {
		avgPerHour = totalBuilt / hours;
	}
	
	title = 'Hourly Production Summary : '; 
	if (selDate === undefined) {
		title += $('#start_date').val() != $('#end_date').val() 
				? $('#start_date').val() +' - ' + $('#end_date').val()
				:$('#start_date').val();
	} else {
		title += selDate;
	}
	
	subTitle = 'Total Built:<b>' 
		+ totalBuilt 
		+ '</b><br>Avg Per Hour:<b>'
		+ avgPerHour.toFixed(2) + '</b>';
	
	//hourly production report
	var options = {
        credits : {
            enabled : false
        },
        chart: {
            type: 'line'
        },
        title: {
            text: title
        },
        xAxis: {
			title: {
                text: 'Hour'
            },
			categories: ['12am','1am','2am','3am','4am','5am','6am','7am','8am','9am','10am',
			'11am','12pm','1pm','2pm','3pm','4pm','5pm','6pm','7pm','8pm','9pm','10pm',
			'11pm']            
        },
        yAxis: {
            title: {
                text: 'Built Quantity'
            }
        },
		 tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },
        series: [{
					name: 'Built Quantity',
					data: data
				}],
		subtitle: {
				text: subTitle
			},
		plotOptions: {
            column: {
                dataLabels: {
                    enabled: true
                }
            }
        }
	};
	
	// populate the chart
	$('.panel-hourly-chart').highcharts(options);

}

function populateHourlyProductionTable(jso) {
    if (dtProductionTable != null) {
        dtProductionTable.destroy();    //so the thing will update		
        //$('.table-prod-report tbody').empty();
    }

    if (!jso) {
        jso = [{'Date': 'No Results', 'Hour': 0, 'Qty_Built': 0}];
    }
	
	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	$.each(objs, function(k,v) {
		cols.push({data: v, title: v.toProperCase()});
	});

    dtProductionTable = $('.table-prod-report').DataTable({
        "bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],                        
        data: jso,		
        columns: cols
		/*[            
            {
                data: 'hour',
                visible: false,
                searchable: false
            },
            {
                data: 'hour',
                title: 'Hour',
                defaultContent: '',
                render: function (data, type, full, meta) {
                    if (data === 0) {
                        return '12am';
                    } else if (data === 12) {
                        return '12pm';
                    } else if (data < 12 && data > 0) {
                        return data + 'am';
                    } else {
                        return (data - 12) + 'pm';
                    }
                }
            },
            {data: 'qty_built', "title":"Built", "defaultContent":""}
        ]*/

    });    
}

function populate_details(row) {
	console.log('populating details');
	var $childContent = $('<div class="panel panel-primary"><div class="well"><table class="table table-bordered table-striped details-table"><tbody><tr><td>Loading...</td></tr></tbody></table></div></div>');
	var row_data = row.data();	
	console.log(row_data);
	var data = {
		'action':'drilldown',
		'date': row_data.Date,
		'hour': row_data.Hour
	};
	
	if ($('#machine_select').val() != '') {
		data.machine_id = $('#machine_select').val();
	}
	if ($('#tool_select').val() != '') {
		data.tool_id = $('#tool_select').val();
	}
	if ($('#part_select').val() != '') {
		data.part_id = $('#part_select').val();
	}
	if ($('#shift_select').val() != '') {
		data.shift_id = $('#shift_select').val();
	}
	if ($('#operator_select').val() != '') {
		data.operator_id = $('#operator_select').val();
	}
	
	row.child($childContent).show();
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		after_populate_details(row, jso);
	});
}

function after_populate_details(row, jso) {
	//console.log('Details jso');
	console.log(jso);
	if (detailsTable != null) {
        //detailsTable.destroy();    //so the thing will update
        row.child().find('.details-table').empty();
    }
	
	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);

	$.each(objs, function(k,v) {
		var title;
		if ( v == 'ID' || v == 'machine_type_ID' || v == 'reason_ID' || v == 'machine_ID' || v == 'tool_ID') {
			title = v;
			visible = false;
			//cols.push({ data: v, title: v, visible: false});
		} else if (v == 'tool') {
			title = '<span data-localize="tool.upper">Tool</span>';
			visible = true;
		} else {
			title = v.toProperCase();
			visible = true;
			//cols.push({ data: v, title: v.toProperCase()});
		}

		cols.push({data: v, title: title, visible: visible});
	});
	
	detailsTable = row.child().find('.details-table').DataTable({
        "bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],a: jso,        
        columns: cols
	
    });	
	/* moved to manage downtime
	//show detail on cycleTimesTable click
	$('.details-table tbody').on('click', 'tr', function () {		
		$('.details-table-modal').data(detailsTable.row(this).data());
		$('.details-table-modal').modal('show');		
	});
	*/
}
