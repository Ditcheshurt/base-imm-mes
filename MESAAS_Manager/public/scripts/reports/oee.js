var processor_path = './ajax_processors/reports/oee.php';

var system_select;
var machine_select;
var start_date_input;
var end_date_input;
var shift_select;
var chosen_system;
var table;
var production_panel;
var filter_run_btn;

$(document).ready(function () {
	system_select = $('#system_select');
	machine_select = $('#machine_select');
	tool_select = $('#tool_select');
	start_date_input = $('#start_date');
	end_data_input = $('#end_date');
	shift_select = $('#shift_select');
	production_panel = $('#production_panel');
    filter_run_btn = $('#filter_panel .btn-run');

    filter_run_btn.prop('disabled', true);

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    system_select.on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	// run report button
	$('body').on('click', '.btn-run', load_oee_data);
});

function load_filter_options () {
    filter_run_btn.prop('disabled', true);
    var system_id = system_select.find('option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    //$('#machine_select').empty();
    machine_select.nysus_select({
        name: 'machine_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_machines', database: chosen_system.database_name},
        render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		placeholder: 'Select Machine',
        callback: function (data) {
            
        }
    });
	
	machine_select.on('change', function(e) {
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			tool_select.prop('disabled', true);
			filter_run_btn.prop('disabled', true);
		} else {
			tool_select.prop('disabled', false);
			filter_run_btn.prop('disabled', false);
		}
		load_tool_filter_options();
	});
}

function load_tool_filter_options() {
	var machine_id = machine_select.find('option:selected').val();
	tool_select.nysus_select({
		name: 'tool_ID',
		key: 'tool_ID',
		data_url: 'ajax_processors/filter.php',
		params: {action: 'get_tools', machine_ID:machine_id, database: chosen_system.database_name },
		render: function (tools) {return tools.tool_ID + ' - ' + tools.tool_description;},
		placeholder: 'All',
		callback: function (data) {
			//filter_run_btn.prop('disabled', false);
		}
	});
}

function load_oee_data () {
    var params = {
        action: 'get_oee_summary',
        start_date: get_date_value('start_date'),
        end_date: get_date_value('end_date'),
        database: chosen_system.database_name,
        shift: $('#shift_select option:selected').val(),
        machine_ID: $('#machine_select option:selected').val(),
        system_ID: function (d) {return $('#system_select option:selected').val();},
		tool_ID: function (d) {return $('#tool_select option:selected').val();}
    };

    $.getJSON(processor_path, params)
        .done(build_panel)
        .fail(function () {
            console.error('Unable to load oee summary.');
        });
}

function build_panel (summary_data) {
    var gauge_data = [
        {name: 'oee', title: 'OEE', value: summary_data ? Math.round(summary_data.oee * 100) : 0},
        {name: 'availability', title: 'Availability', value: summary_data ? Math.round(summary_data.availability * 100) : 0},
        {name: 'quality', title: 'Quality', value: summary_data ? Math.round(summary_data.quality * 100) : 0},
        {name: 'performance', title: 'Performance', value: summary_data ? Math.round(summary_data.performance * 100) : 0}
    ];

    for(var i=0; i<gauge_data.length; i++) {
        var gauge = $('.'+gauge_data[i]['name']+'-gauge');
        build_gauge(gauge, gauge_data[i]);
    }

    build_table('.oee-table');
    production_panel.show();
}

function build_table (el) {

    // table setup
    if (typeof table != 'undefined') {
        table.ajax.reload();
        return;
    }

    var export_cols = [0, 1, 2, 3, 4, 5];

    table = $(el).DataTable({
        dom: 'T<"clear">lfrtip',
        tableTools: {
            sSwfPath: "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            aButtons: [
                {
                    sExtends: "copy",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "csv",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "xls",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "pdf",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
                {
                    sExtends: "print",
                    sPdfOrientation: "portrait",
                    mColumns: export_cols
                },
            ]
        },

        ajax: {
            url: processor_path,
            data: {
		        action: 'get_oee_data',
		        start_date: function (d) {return get_date_value('start_date');},
		        end_date: function (d) {return get_date_value('end_date');},
		        database: function (d) {return chosen_system.database_name;},
		        shift: function (d) {return $('#shift_select option:selected').val();},
		        machine_ID: function (d) {return $('#machine_select option:selected').val();},
                system_ID: function (d) {return $('#system_select option:selected').val();},
				tool_ID: function (d) {return $('#tool_select option:selected').val();}
            },
            dataSrc: '',
        },

        order: [[0, 'asc']],

        columns: [
            {data: 'machine_ID', visible: false},
	        {data: 'machine_number', title: 'Machine Number', sClass: 'text-center'},
	        {data: 'machine_name', title: 'Machine Name', sClass: 'text-center'},
			{data: 'tool_desc', title: 'Tool', sClass: 'text-center'},
	        {data: 'oee', title: 'OEE', sClass: 'text-center oee-param', createdCell: color_cell, render: render_oee},
	        {data: 'availability', title: 'Availability', sClass: 'text-center oee-param', createdCell: color_cell, render: render_oee},
	        {data: 'quality', title: 'Quality', sClass: 'text-center oee-param', createdCell: color_cell, render: render_oee},
	        {data: 'performance', title: 'Performance', sClass: 'text-center oee-param', createdCell: color_cell, render: render_oee},
	        {data: 'part_goal', title: 'Goal', sClass: 'text-center', render: function (data, type, full, meta) {return Math.ceil(data);}},
	        {data: 'part_qty', title: 'Produced', sClass: 'text-center'},
	        {data: 'scrap', title: 'Scrap', sClass: 'text-center'},
	        {data: 'total_minutes', title: 'Hours', sClass: 'text-center', render: function (data, type, full, meta) {return (data / 60).toFixed(1);}},
	        {data: 'downtime_minutes', title: 'Downtime <span class="text-muted">(min.)</span>', sClass: 'text-center', render: function (data, type, full, meta) {return data.toFixed(1);}}
        ]
    });
}

function build_gauge (el, data) {
    var series = [{
        name: data.title,
        data: [data.value],
        dataLabels: {
            format: '<div style="text-align:center"><span class="gauge-value-text">{y}</span>' +
                   '<span class="gauge-value-sign">%</span></div>'
        }
    }];

    var gaugeOptions = {

        chart: {
            type: 'solidgauge',
            events: {
                beforePrint: function () {
                    this._hasUserSize = this.hasUserSize;
                    this._reset = [this.chartWidth, this.chartHeight, false];
                    this.setSize(100, 60, false);
                },
                afterPrint: function () {
                    this.setSize.apply(this, this._reset);
                    this.hasUserSize = this._hasUserSize;
                }
            }
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.85, '#DF5353'], // red
                [0.95, '#DDDF0D'], // yellow
                [1,'#55BF3B'] // green
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    $(el).highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: null
        },

        credits: {
            enabled: false
        },

        exporting: {
            enabled: false
        },

        series: series

    }));
}

function render_date (data, type, full, meta) {
    if (data) {
        return data.date;
    } else {
        return '';
    }
}

function render_oee (data, type, full, meta) {
	return Math.round(data * 100) + '%';
}

function color_cell (td, cell_data, row_data, row, col) {
	var c;

    if (cell_data <= 0.85) {
        c = 'danger';
    } else if (cell_data <= 0.95) {
        c = 'warning';
    } else {
        c = 'success';
    }

    $(td).addClass(c);
}