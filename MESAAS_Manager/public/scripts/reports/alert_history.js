var processor = './ajax_processors/reports/alert_history.php';

var system_select;
var machine_select;
var start_date_input;
var end_date_input;
var shift_select;
var chosen_system;
var table;
var production_panel;
var filter_run_btn;
var defectTable;

var tmpDataPT = 0;
var alrt = $('table#tbl-alerts');

$(document).ready(function () {
	system_select = $('#system_select');
	machine_select = $('#machine_select');
	start_date_input = $('#start_date');
	end_data_input = $('#end_date');
	$params = $('#alert_type_combo');
	
	production_panel = $('#production_panel');
    filter_run_btn = $('#filter_panel .btn-run');

    filter_run_btn.prop('disabled', true);

    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    system_select.on('change', function (e) {
        load_filter_options();
    });

    // date range filtering
    init_date_time('start_date', true);
    init_date_time('end_date', false);

	// run report button
	$('body').on('click', '.btn-run', loadAlerts);
	
	var params_options = {
		label_text: 'Alert Severity'
		, label_tooltip: 'Select severities'
		, control_id: 'alert-type-combo'
		, control_tag: 'select'
	};
	$params.nysus_labeled_control(params_options);
	build_combo($('#alert-type-combo'), {action: 'get_alert_types'}, [{key: 'multiple', value: 'multiple'}]);

	alrt.DataTable({
		"bAutoWidth":false,
		dom: 'T<"clear">lfBrtip',
		width: '100%',
		pageLength: 10,
		lengthMenu:[
			[10,50,100],
			[10,50,150]
		],
		buttons: [
			'copyHtml5',
			{
				extend: 'excelHtml5',
				title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
			},
			{
				extend: 'csvHtml5',
				title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
			},
			'pdfHtml5'
		],
		columns:[
			{title:"Shot Time",data:"cycle_time",render:renderDateTime},
			{title:"Machine",data:"machine_name"},
			{title:"Tool",data:"tool_description"},
			{title:"Parameter",data:"data_point_desc"},
			{title:"Upper Limit",data:"upper_ctrl_limit"},
			{title:"Lower Limit",data:"lower_ctrl_limit"},
			{title:"Value",data:"data_point_value"},
			{title:"Severity",data:"severity_description"}
			//,{title:"Edit",data:"edit",defaultContent:'<button class="btn btn-primary btn-edit-alert">Edit</button>'}
			//,{title:"Ack",data:"ack",defaultContent:'<button class="btn btn-success btn-ack-alert">Ack</button>'}
		]
	});

});

function load_filter_options () {
    filter_run_btn.prop('disabled', true);
    var system_id = system_select.find('option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    //$('#machine_select').empty();
    machine_select.nysus_select({
        name: 'machine_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_machines', database: chosen_system.database_name},
        render: function (machine) {return machine.machine_number + ' - ' + machine.machine_name;},
		placeholder: 'Select Machine',
        callback: function (data) {
            
        }
    });
	
	machine_select.on('change', function(e) {
		var machine_id = machine_select.find('option:selected').val();
		if (machine_id < 0) {
			filter_run_btn.prop('disabled', true);
		} else {
			filter_run_btn.prop('disabled', false);
		}
	});
}

function loadAlerts () {
	//$('#div_parameter_chart').empty();
	$('body .btn-run').html('Loading...');
	$('body .btn-run').addClass('disabled');
	var sdate = -1;
	var edate = -1;

	try {
		sdate = get_date_time('start_date');
		edate = get_date_time('end_date');
		var params = {
			action: 'get_alert_data',
			alerts: $('#alert-type-combo').select2().val()?$('#alert-type-combo').select2().val():-1,
			start_date: sdate,
			end_date: edate,
			database: chosen_system.database_name,
			machine_ID: $('#machine_select option:selected').val(),
		};

		$.getJSON(processor, params)
			.done(afterLoadAlerts)
			.fail(function () {
				console.error('Unable to load param data.');
			});
	} catch (e) {
		if (sdate == -1) {
			$('.start_date').focus();
		}
		if (edate == -1) {
			$('.end_date').focus();
		}
		$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
		$('body .btn-run').removeClass('disabled');		
	}
}

function afterLoadAlerts(jso) {
	var tbl = $('table#tbl-alerts').DataTable();
	
	$('body .btn-run').html('<span class="glyphicon glyphicon-ok"></span> Run Report');
	$('body .btn-run').removeClass('disabled');

	tbl.clear().draw();
	
	if(jso) {
		tbl.rows.add(jso).draw(false)
	}
	$('#alert_table').show();
	//$('#parameter_panel').show();
}

function build_combo(combo, data, attr, selection) {
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function renderDateTime (data, type, row) {
	var d = 0;
	if (data.date) {d = new Date(data.date);}
	return d.toLocaleString();
}

function init_date_time (id,start_nEND) {
	// initialize the date input to the current date
	if (start_nEND === undefined) {start_nEND = true;}
	
	var today = new Date();
	if (start_nEND) {
		today.setHours(0,0,0,0);
	} else {
		today.setHours(23,59,0,0);
	}
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().slice(0,-1);
	$('#'+id).val(today_str);
}

function get_date_time (id) {
	var date_str = $('#'+id).val();
	var objDT = new Date(date_str);
	var offset = objDT.getTimezoneOffset() * 60;
	objDT = new Date(objDT.getTime() - offset * 1000);
	return objDT.toISOString().split('T')[0].replace(/\-/g,'/') + ' ' + objDT.toISOString().split('T')[1].slice(0,8);
}
