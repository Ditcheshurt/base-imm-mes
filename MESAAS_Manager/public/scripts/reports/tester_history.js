var processor = './ajax_processors/reports/tester_history.php';
var dtTesterHistory;
var systems;
var chosen_system;

$(document).ready(function() {	
    load_systems(null, function (data) {
        systems = data;
        show_systems(data);
        load_filter_options();
    });

    $('#system_select').on('change', function (e) {
        load_filter_options();
    });

    $('#line_select').on('change', line_changed);

    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');

	// run report button
	$('body').on('click', '.btn-run', loadData);
	
	//show detail on cycleTimesTable click
	$('.table-tester-history tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = dtTesterHistory.row(tr);
	
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			populateDetailTable(row);
			tr.addClass('shown');
		}
	});	

});

function load_filter_options () {
    var system_id = $('#system_select option:selected').val();
    
    var res = $.grep(systems, function (s) {
        return s.ID == system_id;
    });
    chosen_system = res[0];

    load_lines({system_id: system_id}, function (lines) {
    	show_lines(lines);
    	line_changed();
    });

    load_operators(null, function (operators) {
    	show_operators(operators);
		var d = $('<option />').prop('id', 'operator_default')
							   .prop('name', 'soperator_id')
							   .val('%')
							   .text('- Operator -');
		$('#operator_select').prepend(d);
    });

	//populate the group by
	$('#inpGroupBy').empty();
	$('#inpGroupBy').append($('<option />').val('Line').text('Line'));
	$('#inpGroupBy').append($('<option />').val('Operator').text('Operator'));
	$('#inpGroupBy').append($('<option />').val('Date').text('Date'));
    $('#inpGroupBy').append($('<option />').val('Model').text('Model'));
	
	//populate shifts
	$('#inpShift').empty();
	$('#inpShift').append($('<option />').val('%').text('- Shift -'));
	$('#inpShift').append($('<option />').val('1').text('First'));
	$('#inpShift').append($('<option />').val('2').text('Second'));
	$('#inpShift').append($('<option />').val('3').text('Third'));
}

function line_changed() {
    $('#line_model_select').empty();
    $('#line_model_select').nysus_select({
        name: 'line_model',
        key: 'model',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_line_models', line_id: $('#line_select').val()},
        render: function (line_model) {return line_model.model;},
        placeholder: '- Model -'
    }).prop('disabled', false);

    $('#station_select').empty();
    $('#station_select').nysus_select({
        name: 'station_id',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_stations', system_id: chosen_system.ID, line_id: $('#line_select').val()},
        render: function (station) {return station.name;},
        placeholder: '- Station -'
    }).prop('disabled', false);

	// load_stations({
	// 	system_id: chosen_system.ID,
	// 	line_id: $('#line_select').val()
	// }, function (stations) {
	// 	show_stations(stations);
	// 	var default_op = $('<option />').prop('id', 'station_default')
	// 								    .prop('name', 'station_id')
	// 								    .val('%')
	// 								    .text('- Station -');
	// 	$('#station_select').prepend(default_op);
	// });
}

function loadData() {
	
	loadTopFailures();
	loadTesterCycles();
	
	// set dates on panels
	var dateTitle = ($('#start_date').val() != $('#end_date').val()) 
		? $('#start_date').val() + ' -> ' + $('#end_date').val() 
		: $('#start_date').val();
	$('.panel .panel-report-date').html(dateTitle);	
	 
	$.getJSON(processor, {
		'action':'tester_history',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
        'line_model':$('#line_model_select').val(),
		'operator_id':$('#operator_select').val(),
		'station_id':$('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	}, function(jso) {

		//respond no data
		if (!jso) {
			$('.panel .panel-report-date').html('No Data to Report');
			$('.panel .panel-tester-history').empty();
			$('.table-tester-history').hide();
			return; 
		} else {
			$('.table-tester-history').show();
			populateTesterHistory(jso);
		}
		
	});
	
}

function populateTesterHistory(jso) {
	if (dtTesterHistory != null) { 
		dtTesterHistory.destroy();	//so the thing will update		
	} 
	dtTesterHistory = $('.table-tester-history').DataTable({
		"bInfo":false,
		"bAutoWidth":true,
		autoWidth:true,
		retrieve: true,		
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		},                        
		data: jso,        
		order: [[3, 'asc']],
		columns: [
			{
				"className": 'details-control',
				"orderable": false,
				"data": null,
				"defaultContent": ''
			},
			{data: 'build_order', "title":"Build Order", "defaultContent":""},
			{data: 'sequence', "title":"Sequence", "defaultContent":""},
			{data: 'station_desc', "title":"Station", "defaultContent":""},
			{data: 'local_start_date', "title":"Test Time"},
			{data: 'result', "title":"Result"}
		]
	
	});

}

function populateDetailTable(r) {
	
	var childContent = '<div class="row well">'+
				'<div><table class="test-history-detail"><thead /><tbody /></table></div>'+
				'</div>';
	r.child($(childContent)).show();
	
	$.getJSON(processor, {
		'action':'test_history_detail',
		'test_ID':r.data().ID		
	}, function(jso) {
		if (!jso) {return;}

		$('.test-history-detail').DataTable({
			"sScrollY": "100%",
			"bScrollCollapse": true,
			"bAutoWidth":true,
			autoWidth:true,
			retrieve: true,
			
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},                        
			data: jso,        
			order: [[2, 'desc']], 
			columns: [
				{data: 'test_type', "title":"Test Type"},
				{data: 'test_item', "title":"Test Item"},
				{data: 'min_float_val', "title":"Minimum"},
				{data: 'max_float_val', "title":"Maximum"},
				{data: 'float_val', "title":"Actual"},
				{data: 'formatted_passed', "title":"Passed"}
			]
		});
	});
	
}

function loadTopFailures() {
	var dat = [];
	$('.panel-top-failures').empty();
	$.getJSON(processor, {
		'action':'top_failures',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
        'line_model':$('#line_model_select').val(),
		'operator_id':$('#operator_select').val(),
		'station_id':$('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	}, function(jso) {		
		if (!jso) {
			return;
		}
		var dat = jso.map(function (test) {
			return [test.test_desc, test.failures];
		});

		var total_failures = jso.reduce(function (a, b) {
			return a + b.failures;
		}, 0);
		
		$('.panel-top-failures').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Top 10 Failures'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}/'+total_failures+' ({point.percentage:.1f} %)</b>'
        },
        credits: {
          enabled: false
				},
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}/'+total_failures+' ({point.percentage:.1f} %)',
                    //style: {
                    //    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    //}
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Test Failures',
            data: dat
        }]
    });
    
	});	
	
}

function loadTesterCycles() {
	var panel = $('.panel-tester-cycles');
	panel.empty();
	$.getJSON(processor, {
		'action':'tester_cycles',
		'start_date':$('#start_date').val(),
		'end_date':$('#end_date').val(),
		'line_id':$('#line_select').val(),
        'line_model':$('#line_model_select').val(),
		'operator_id':$('#operator_select').val(),
		'station_id':$('#station_select').val(),
		'group_by':$('#inpGroupBy').val(),
		'shift':$('#inpShift').val(),
        'database': chosen_system.database_name,
        'process_type': chosen_system.process_type
	}, function(jso) {
		console.log(jso);
		if (!jso) {
			return;
		}		
		var table = $('<table class="table table-striped"><thead><th>Station</th><th>Cycles</th><th>Failures</th></thead><tbody></tbody></table>');
		$.each(jso, function(k,v) {
		var failed = v.failed != null ? v.failed : 0;
			var row = $('<tr><td>' + v.station + '</td><td>' + v.cycles + '</td><td>' + failed + '</td></tr>');
			row.appendTo(table.find('tbody'));
		});
		table.appendTo(panel);
	});
}

