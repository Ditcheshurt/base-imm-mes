$(function() {
    tableCreate();
    $('#btn_run_report').on('click', on_operator_selected);
    console.log("content was loaded");
});

function tableCreate() {

    var body = document.body,
    tbl = document.createElement('table');

    tbl.style.width  = '100%';
    tbl.style.height = '100%';
    tbl.style.border = "1px solid white";
    tbl.style.borderCollapse = "collapse";

    $('#area_select').on('change', operator_select);
    area_select();
}

/*adds to the operator select drop down*/
function operator_select() {
    $("#operator_select_").attr("disabled", false);
    var selected_cell = $('.area_select option:selected');
    var stuff = {
        'action': 'operator_select',
        'area_id': selected_cell.val()
    }
    
    $.ajax({
        type: "POST",
        url: './ajax_processors/reports/ojt/ojt_operator_skillsheet.php',
        data: stuff, 
        success:function(jso) {  
            $("#operator_select_").find('option').remove().end(); // clears drop down 
            $("#operator_select_").append('<option>Select operator name...</option>');
            for(var i = 0; i < jso.length; i++) {
                $("#operator_select_").append('<option name="operator_select_'+i+'" id="operator_select_'+i+'" value="'+jso[i].ID+'">'+jso[i].name+'</option>')
            }
        }
    });
}

/*calls php to find data to send to drop down selection box*/
function area_select() {
    $.getJSON('./ajax_processors/reports/ojt/ojt_operator_skillsheet.php',
        {'action' : 'area_select'}
        , after_area_select);
}

/*appends data to the drop down selection box*/
function after_area_select(jso) { 
    $("#area_select_").append('<option>Select your area...</option>');
    for(var j = 0; j < jso.length; j++) {
        if(jso[j].areas !== null) {
            var group = $('<optgroup value="'+jso[j].ID+'" label="'+jso[j].department_name+'"></optgroup>').appendTo("#area_select_");
            var areas = jso[j].areas;
            if(areas){
                for(var i = 0; i < areas.length; i++) {
                    group.append('<option name="area_select_'+i+'" id="area_select_'+i+'" value="'+areas[i].ID+'" data-dept-id="'+jso[j].ID+'">'+areas[i].area_desc+'</option>');
                }
            }
        }
    }
}

/*table head population*/
function table_head(jso) {
    var stuff = {
        'action': 'table_head'
    };

    $.ajax({
        type: "POST",
        url: './ajax_processors/reports/ojt/ojt_operator_skillsheet.php',
        data: stuff,
        success:function(jso) {
            var tbody = $('#report_results thead');
            var row = $('<tr></tr>');
            var col = $();

            row.append('<th class="job_name center">Job</th>');            
            for(var i = 0; i < jso.length; i++) {     
                row.append('<th class="center" >'+jso[i].level_desc+'</th>');
                row.append(col);
            }
            tbody.append(row);
        }
    });
}

function on_operator_selected(jso) {
    var selected_operator = $('.operator_select option:selected');
    var selected_cell = $('.area_select option:selected');
    var stuff = {
        'action': 'training_history',
        'operator_id': selected_operator.val(),
        'area_id': selected_cell.val(),
        'dept_id': selected_cell.attr('data-dept-id')
    };
    
    $("#report_results tbody tr").remove();
    $("#report_results thead th").remove(); 

    /*shows query results for recent qualifications*/
    $.ajax({
        type: "POST",
        url: './ajax_processors/reports/ojt/ojt_operator_skillsheet.php',
        data: stuff,
        success:function(jso) {

            if (!jso.qualification_dates) {
                var tbody = $('#report_results tbody');
                var row = $('<tr></tr>');
                var col = $();
                row.append('<td colspan="5">No training history found.</td>');
                tbody.append(row);
                //console.log("No training history found");
            }
            else { 
                table_head();  

                for(var i = 0; i < jso.qualification_dates.length; i++) {
                    var qual = jso.qualification_dates[i];
                    var tbody = $('#report_results tbody');
                    var row = $('<tr></tr>');
                    var col = $();
     
                    row.append('<td class="job_name center" id="'+i+'">'+qual.job_name+'</td>');
                    for(var j = 0; j < jso.level.length; j++) {
                        var level = jso.level[j];
                        console.log(qual);
                        if(qual[level.ojt_level] != null) {
                            row.append('<td class="center" id="'+i+'">'+qual[level.ojt_level].date.slice(0, 10)+'</td>');
                        }
                        else{
                            row.append('<td class="level_1 center" id="'+i+'"></td>');
                        }
                    }
                    row.append(col);
                    tbody.append(row);
                }      
            }
        }
    });
}