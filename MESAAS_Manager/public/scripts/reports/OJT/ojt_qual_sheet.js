var processor_path = './ajax_processors/reports/ojt/ojt_qual_sheet.php';

var area_select;
var sub_area_select;
var department_areas;
var chosen_area;

var main_panel;


$(document).ready(function () {
    area_select = $('#area_select');
    sub_area_select = $('#sub_area_select');

    main_panel = $('#main_panel');

    area_select.on('change', load_sub_areas);

    load_areas();

    // run report button
    $('body').on('click', '.btn-run', load_data);
});


function load_areas () {
    var params = {
        action: 'get_departments_areas',
    };

    $.getJSON('ajax_processors/filter.php', params).done(function (data) {
        area_select.empty();
        if (data) {
            $('<option></option>').prop({id: 'area_select_placeholder', name: 'area_ID'})
                                  .text('Select an Area')
                                  .val(-1)
                                  .appendTo(area_select);

            data.forEach(function (department, i) {
                if (department.areas && department.areas.length > 0) {
                    var group = $('<optgroup></optgroup>').prop({label: department.department_name})
                                                          .val(department.ID)
                                                          .appendTo(area_select);
                    department.areas.forEach(function (area, j) {
                        $('<option></option>').prop({id: 'area_select_'+area.ID, name: 'area_ID'})
                                              .text(area.area_desc)
                                              .val(area.ID)
                                              .attr('data-department-id', area.department_ID)
                                              .appendTo(group);
                    });
                }
            });
            area_select.prop({disabled: false});
        }
    });
}

function load_sub_areas () {
    $('#sub_area_select').empty();
    sub_area_select.nysus_select({
        name: 'sub_area_ID',
        key: 'ID',
        data_url: 'ajax_processors/filter.php',
        params: {action: 'get_sub_areas', area_ID: $('#area_select').val()},
        render: function (sub_area) {return sub_area.sub_area_desc;},
        placeholder: 'Select A Group'
    });
}

function load_data () {
        var args = {
            type: 'plant',
            department_ID: $('#area_select option:selected').attr('data-department-id'), 
            area_ID: $('#area_select').val(),
            sub_area_ID: $('#sub_area_select').val(),
            active: 1
        };
        load_ojt_data(args, show_plant_wide_data);
        args.type = 'department';
        load_ojt_data(args, show_department_wide_data);
        args.type = 'area';
        load_ojt_data(args, show_area_wide_data);
        args.type = 'sub_area';
        load_ojt_data(args, show_sub_area_wide_data);
}

function load_ojt_data (args, callback) {
    var params = {
        action: 'get_ojt_data'
    };
    $.extend(params, args);
    $.getJSON(processor_path, params).done(callback);
}

function build_panel (el, data, title) {
    // build panel
    var main_panel_body = main_panel.children('.panel-body');
    var panel = $(el).nysus_panel({panel_title: title});
    var panel_body = $('.panel-body', panel);
    panel_body.empty();
    $('.panel-footer', panel).hide();

    // build table
    var table_container = $('<div></div>').addClass('table-responsive')
                                          .appendTo(panel_body);
    var table = $('<table></table>').addClass('table table-striped table-bordered table-condensed qual-sheet-table')
                                    .appendTo(table_container);
    var thead = $('<thead></thead>').appendTo(table);
    var tbody = $('<tbody></tbody>').appendTo(table);
    
    if (data && data.jobs && data.levels && data.operator_qualifications) {
        // add job headers
        var r1 = $('<tr></tr>').appendTo(thead);
        $('<th></th>').prop({rowspan: 2})
                      .appendTo(r1);
        $('<th></th>').prop({colspan: data.jobs ? data.jobs.length : 1})
                      .addClass('text-center')
                      .text('Jobs')
                      .appendTo(r1);
        $('<th></th>').prop({rowspan: 2})
                      .addClass('text-center')
                      .text('% Meeting Min Required')
                      .appendTo(r1);

        var r2 = $('<tr></tr>').appendTo(thead);
        var r3 = $('<tr></tr>').addClass('min-qual')
                               .appendTo(thead);

        $('<th></th>').text('Min Required').appendTo(r3);

        data.jobs.forEach(function (job) {
            $('<th></th>').addClass('text-center')
                          .text(job.job_name)
                          .appendTo(r2);

            $('<td></td>').addClass('job-cell text-center')
                          .text(job.min_qualified)
                          .attr('data-min-qualified', job.min_qualified)
                          .appendTo(r3);
        });

        $('<td></td>').addClass('text-center').appendTo(r3);


        // add levels
        if (data.levels) {
            data.levels.forEach(function (level, i) {
                var level_row = $('<tr></tr>').addClass('level level-'+(i+1))
                                              .append('<th>'+level.level_desc+'</th>')
                                              .appendTo(tbody);

                data.jobs.forEach(function (job, j) {
                    if (level.job_qual_counts[job.ID]) {
                        $('<td></td>').addClass('job-cell text-center')
                                      .attr('data-qualified-count', level.job_qual_counts[job.ID])
                                      .appendTo(level_row);
                    } else {
                        $('<td></td>').addClass('job-cell text-center')
                                      .attr('data-qualified-count', 0)
                                      .appendTo(level_row);
                    }

                });

                $('<td></td>').addClass('text-center req-met')
                              .appendTo(level_row);
            });
        }

        var operator_heading_row = $('<tr></tr>').attr({colspan: data.jobs.length+2})
                                                 .append('<th>Operators</th>')
                                                 .appendTo(tbody);

        if (data.operator_qualifications) {
            data.operator_qualifications.forEach(function (operator, i) {
                var operator_row = $('<tr></tr>').append('<td>'+operator.operator_name+'</td>').appendTo(tbody);
                //var max_levels = operator.max_levels;

                data.jobs.forEach(function (job, j) {
                    var job_cell = $('<td></td>').addClass('text-center').appendTo(operator_row);
                    $('<span></span>').addClass('tooltipper')
                                      .text(operator[job.ID] ? operator[job.ID] : '')
                                      .appendTo(job_cell);
                });

                $('<td></td>').appendTo(operator_row);
            });
        }
    }
}

function show_plant_wide_data (data) {
    build_panel('#plant_wide_panel', data, 'Plant-Wide');
    populate_levels('#plant_wide_panel');
}

function show_department_wide_data (data) {
    build_panel('#department_wide_panel', data, 'Department-Specific');
    populate_levels('#department_wide_panel');
}

function show_area_wide_data (data) {
    build_panel('#area_wide_panel', data, 'Area-Specific');
    populate_levels('#area_wide_panel');
}

function show_sub_area_wide_data (data) {
    build_panel('#sub_area_wide_panel', data, 'Group-Specific');
    populate_levels('#sub_area_wide_panel');
}

function populate_levels(el) {

    var min_quals = $(el).find('tr.min-qual td.job-cell');

    $(el).find('tr.level').each(function (i, row) {
        var req_met = 0;
        var job_cells = $(row).find('.job-cell');

        job_cells.each(function (i, cell) {
            var count = $(cell).data('qualified-count');
            var min_qual = $(min_quals[i]).data('min-qualified');
            var perc_qual;

            if (!min_qual) {
                perc_qual = 100;
            } else {
                perc_qual = (count / min_qual) * 100;
            }

            $(cell).html(count + ' - ' + perc_qual.toFixed(1) + '%');

            if (count >= min_qual) {
                req_met += 1;
            }
        });

        var req_perc = (req_met / job_cells.length) * 100;
        $(row).find('.req-met').html(req_perc.toFixed(1)  + '%');
    });

}