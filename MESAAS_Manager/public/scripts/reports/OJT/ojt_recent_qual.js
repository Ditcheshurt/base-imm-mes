var processor = './ajax_processors/reports/ojt/ojt_recent_qual.php';

$(function() {
    tableCreate();
    $('#btn_run_report').on('click', on_area_selected);
    console.log("content was loaded");
});

function tableCreate() {

    var body = document.body,
    tbl = document.createElement('table');

    tbl.style.width  = '100%';
    tbl.style.height = '100%';
    tbl.style.border = "1px solid white";
    tbl.style.borderCollapse = "collapse";

    area_select();
}

/*calls php to find data to send to drop down selection box*/
function area_select() {
    $.getJSON(processor,
        {'action' : 'area_select'}
        , after_area_select);
}

/*appends data to the drop down selection box*/
function after_area_select(jso) { 
    $("#area_select_").append('<option>Select your area...</option>');
    for(var j = 0; j < jso.length; j++) {
        if(jso[j].areas !== null) {
            var group = $('<optgroup value="'+jso[j].ID+'" label="'+jso[j].department_name+'"></optgroup>').appendTo("#area_select_");
            var areas = jso[j].areas;
            if(areas){
                for(var i = 0; i < areas.length; i++) {
                    group.append('<option name="area_select_'+i+'" id="area_select_'+i+'" value="'+areas[i].ID+'" data-dept-id="'+jso[j].ID+'">'+areas[i].area_desc+'</option>');
                }
            }
        }
    }
}

function on_area_selected(jso) {
    var selected_cell = $('.area_select option:selected');
    var signoff_date = $('#up_to_date').val();
    var d = new Date(signoff_date).toISOString().split('T')[0];
    var stuff = {
        'action': 'area_date_job',
        'area_id': selected_cell.val(), 
        'up_to_date': d
    };

    $("#report_results tbody tr").remove();
    $("#report_results thead th").remove();  

    /*shows query results for recent qualifications*/
    $.ajax({
        type: "POST",
        url: processor,
        data: stuff,
        success:function(jso) {
            if (!jso) {
                var tbody = $('#report_results tbody');
                var row = $('<tr></tr>');
                var col = $();
                row.append('<td colspan="4">No recent qualifications found.</td>');
                tbody.append(row);
                console.log("No recent qualifications found");
            }
            else {
                var thead = $('#report_results thead');
                var row = $('<tr></tr>');
                var col = $();

                row.append('<th class="job_name center">Job</th>');
                row.append('<th class="op_name center">Operator</th>');
                row.append('<th class="job_level center">Job Level</th');
                row.append('<th class="signoff_date center">Signoff Date</th>');
                row.append(col);
                thead.append(row);

                for(var i = 0; i < jso.length; i++) {
                    var tbody = $('#report_results tbody');
                    var row = $('<tr></tr>');
                    var col = $();
                    var date = new Date().toISOString();
                    
                    row.append('<td class="job_name center" id="'+i+'">'+jso[i].job_name+'</td>');
                    row.append('<td class="op_name center" id="'+i+'">'+jso[i].name+'</td>');
                    row.append('<td class="job_level center" id="'+i+'">'+jso[i].ojt_level+'</td>');
                    row.append('<td class="signoff_date center" id="'+i+'">'+jso[i].the_date.date.slice(0, 10)+'</td>');
                    row.append(col);
                    tbody.append(row);
                }
            }
        }
    });
}