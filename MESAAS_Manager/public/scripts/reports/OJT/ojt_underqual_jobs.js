$(function() {
    tableCreate();
    $('#btn_run_report').on('click', on_area_selected);
});

function tableCreate() {

    var body = document.body,
    tbl = document.createElement('table');

    tbl.style.width  = '100%';
    tbl.style.height = '100%';
    tbl.style.border = "1px solid white";
    tbl.style.borderCollapse = "collapse";

    area_select();
}

/*calls php to find data to send to drop down selection box*/
function area_select() {
    $.getJSON('./ajax_processors/reports/ojt/ojt_underqual_jobs.php',
        {'action' : 'area_select'}
        , after_area_select);
}

/*appends data to the drop down selection box*/
function after_area_select(jso) { 
    $("#area_select_").append('<option>Select your area...</option>');
    for(var j = 0; j < jso.length; j++) {
        if(jso[j].areas !== null) {
            var group = $('<optgroup value="'+jso[j].ID+'" label="'+jso[j].department_name+'"></optgroup>').appendTo("#area_select_");
            var areas = jso[j].areas;
            if(areas){
                for(var i = 0; i < areas.length; i++) {
                    group.append('<option name="area_select_'+i+'" id="area_select_'+i+'" value="'+areas[i].ID+'" data-dept-id="'+jso[j].ID+'">'+areas[i].area_desc+'</option>');
                }
            }
        }    
    }
}

/*table head population*/
function table_head(jso) {
    var stuff = {
        'action': 'table_head'
    };

    $.ajax({
        type: "POST",
        url: './ajax_processors/reports/ojt/ojt_underqual_jobs.php',
        data: stuff,
        success:function(jso) {
            var tbody = $('#report_results thead');
            var row = $('<tr></tr>');
            var col = $();

            row.append('<th class="job_name center">Job</th>');            
            for(var i = 0; i < jso.length; i++) {     
                row.append('<th class="level_'+i+' center" >'+jso[i].level_desc+' Operators</th>');
                row.append(col);
            }
            tbody.append(row);
        }
    });
}

/*changes what gets displayed once option is selected*/
function on_area_selected(jso) {
    var selected_cell = $('.area_select option:selected'); 
    var stuff = {
        'action': 'selected_area',
        'area_id': selected_cell.val(),
        'dept_id': selected_cell.attr('data-dept-id')
    };

    $("#report_results tbody tr").remove();
    $("#report_results thead th").remove(); 

$.ajax({
        type: "POST",
        url: './ajax_processors/reports/ojt/ojt_underqual_jobs.php',
        data: stuff,
        success:function(jso) {
            if (!jso.job_qualifications) {
                var tbody = $('#report_results tbody');
                var row = $('<tr></tr>');
                var col = $();
                row.append('<td colspan="5">No jobs found.</td>');
                tbody.append(row);
                console.log("No jobs found");
            }
            else {
                table_head();  

                for(var i = 0; i < jso.job_qualifications.length; i++) {
                    var qual = jso.job_qualifications[i];
                    var tbody = $('#report_results tbody');
                    var row = $('<tr></tr>');
                    var col = $();
     
                    row.append('<td class="job_name center" id="'+i+'">'+qual.job_name+'</td>');
                    for(var j = 0; j < jso.levels.length; j++) {
                        var level = jso.levels[j];
                        console.log(qual);
                        row.append('<td class="level_desc center" id="'+j+'" >'+qual[level.ojt_level]+'</td>');
                    }
                    row.append(col);
                    tbody.append(row);
                }
            }
        }
    });
}