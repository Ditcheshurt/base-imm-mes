var processor = './ajax_processors/reports/ojt/ojt_top_trainees.php'
$(function() {
    tableCreate();
    // $('#area_select').on('change', on_area_selected_most);
    // $('#area_select').on('change', on_area_selected_least);
    console.log("content was loaded");

    $('.btn-run').on('click', function () {
        on_area_selected_least();
        on_area_selected_most();
    });
});

function tableCreate() {

    var body = document.body,
    tbl = document.createElement('table');

    tbl.style.width  = '100%';
    tbl.style.height = '100%';
    tbl.style.border = "1px solid white";
    tbl.style.borderCollapse = "collapse";

    area_select();
}

/*calls php to find data to send to drop down selection box*/
function area_select() {
    $.getJSON(processor,
        {'action' : 'area_select'}
        , after_area_select);
}

/*appends data to the drop down selection box*/
function after_area_select(jso) { 
    $("#area_select").append('<option>Select your area...</option>');
    for(var j = 0; j < jso.length; j++) {
        if(jso[j].areas !== null) {
            var group = $('<optgroup value="'+jso[j].ID+'" label="'+jso[j].department_name+'"></optgroup>').appendTo("#area_select");
            var areas = jso[j].areas;
            if(areas){
                for(var i = 0; i < areas.length; i++) {
                    group.append('<option name="area_select_'+i+'" id="area_select_'+i+'" value="'+areas[i].ID+'">'+areas[i].area_desc+'</option>');
                }
            }
        }
    }
}

function on_area_selected_most(jso) {
    var selected_cell = $('#area_select option:selected'); 
    var stuff_most = {
        'action': 'selected_area_most',
        'area_id': selected_cell.val()
    };

    $("#report_results tbody tr").remove();
    $("#report_results thead th").remove();  

    /*changes what gets displayed once option is selected for most trained jobs*/
    $.ajax({
        type: "POST",
        url: processor,
        data: stuff_most,
        success:function(jso) {
            if (!jso) {
                var tbody = $('#report_results > #most');
                var row = $('<tr></tr>');
                var col = $();
                row.append('<td colspan="2">No signoffs found.</td>');
                tbody.append(row);
                console.log("No signoffs found");
            }
            else { 
                var thead = $('#report_results > #most_th');
                var row = $('<tr></tr>');
                var col = $();

                row.append('<th class="op_name_most center">Operator</th>');
                row.append('<th class="num_jobs_most center">Number of Jobs</th>');
                row.append(col);
                thead.append(row); 

                for(var i = 0; i < jso.length; i++) {
                    var tbody = $('#report_results > #most');
                    var row = $('<tr></tr>');
                    var col = $();
     
                    row.append('<td class="op_name_most center" id="'+i+'">'+jso[i].name_most+'</td>');
                    row.append('<td class="num_jobs_most center" id="'+i+'">'+jso[i].num_jobs_most+'</td>');
                    row.append(col);
                    tbody.append(row);
                }
            }
        }
    });
}

function on_area_selected_least(jso) {
    var selected_cell = $('#area_select option:selected'); 

    var stuff_least = {
        'action': 'selected_area_least',
        'area_id': selected_cell.val()
    };

    $("#report_results tbody tr").remove();
    $("#report_results thead th").remove();  

    /*changes what gets displayed once option is selected for least trained jobs*/
    $.ajax({
        type: "POST",
        url: processor,
        data: stuff_least,
        success:function(jso) {
            if (!jso) {
                var tbody = $('#report_results > #least');
                var row = $('<tr></tr>');
                var col = $();
                row.append('<td colspan="2">No signoffs found.</td>');
                tbody.append(row);
                console.log("No signoffs found");
            }
            else {
                var thead = $('#report_results > #least_th');
                var row = $('<tr></tr>');
                var col = $();

                row.append('<th class="op_name_least center">Operator</th>');
                row.append('<th class="num_jobs_least center">Number of Jobs</th>');
                row.append(col);
                thead.append(row);
                                
                for(var i = 0; i < jso.length; i++) {
                    var tbody = $('#report_results > #least');
                    var row = $('<tr></tr>');
                    var col = $();
     
                    row.append('<td class="op_name_least center" id="'+i+'">'+jso[i].name_least+'</td>');
                    row.append(col);
                    row.append('<td class="num_jobs_least center" id="'+i+'">'+jso[i].num_jobs_least+'</td>');
                    row.append(col);
                    tbody.append(row);
                }
            }
        }
    });
}