//spc_charts.js

var chart1;
var chart2;

function doCharting() {

	//should have cur_data populated
	$('#div_charts').append('<div style="text-align:right;" id="export"><button id="export_all">Export All (PDF)</button></div>');
	$('#div_charts').append('<div id="div_xbar"></div>');
	$('#div_charts').append('<div id="div_range"></div>');

	Highcharts.getSVG = function(charts) {

		var svgArr = [],
		top = 0,
		width = 0;

		charts.each(function(chart, i) {
			var svg = chart.getSVG();
			svg = svg.replace('<svg', '<g transform="translate(0,' + top + ')" ');
			svg = svg.replace('</svg>', '</g>');

			top += chart.chartHeight;
			width = Math.max(width, chart.chartWidth);

			svgArr.push(svg);
		});

		return '<svg height="'+ top +'" width="' + width + '" version="1.1" xmlns="http://www.w3.org/2000/svg">' + svgArr.join('') + '</svg>';

	};


	//Create a global exportCharts method that takes an array of charts as an argument,
	//and exporting options as the second argument
	Highcharts.exportCharts = function(charts, options) {

		var form
		svg = Highcharts.getSVG(charts);

		// merge the options
		options = Highcharts.merge(Highcharts.getOptions().exporting, options);

		// create the form
		form = Highcharts.createElement('form', {
			"method": 'post',
			"action": "https://export.highcharts.com"
		}, {
			"display": 'none'
		}, document.body);

		// add the values
		Highcharts.each(['filename', 'type', 'width', 'svg'], function(name) {
			Highcharts.createElement('input', {
				"type": 'hidden',
				"name": name,
				"value": {
					"filename": options.filename || 'chart',
					"type": options.type,
					"width": options.width,
					"svg": svg
				}[name]
			}, null, form);
		});

		form.submit();
		// clean up
		form.parentNode.removeChild(form);
	};


	var tool_cavity = cur_data_point_info.cavity_description;
	var dp_info = cur_data_point_info.data_point_name;
	var cur_date = dateFormat("mm/dd/yyyy");

	var dp_names = [];
	var split_date = '';
	var time_split = '';

	for (var i = cur_num_instances - 1; i >= 0; i--) {

		split_date = cur_instances_info[i].instance_time.split(" ", 3);
		time_split = split_date[1].split(":");

		dp_names.push(split_date[0] + ' ' + time_split[0] + split_date[2]);

	}

	/*for (var i = 0; i < cur_num_instances; i++) {

	split_date = cur_instances_info[i].instance_time.split(" ", 3);
	time_split = split_date[1].split(":");

	dp_names.push(split_date[0] + ' ' + time_split[0] + split_date[2]);

	}*/

	var series_datax = [];
	var xbar_points = [];

	var p = {
		"name": "Avg"
	};
	var a = [];

	for (var j = 0; j < cur_avgs.length; j++) {

		var point = {
			"y": cur_avgs[j]
		};

		if (parseFloat(cur_avgs[j]) > ucl_xbar) {

			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": 'left',
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};

		} else if (parseFloat(cur_avgs[j]) < lcl_xbar) {
			point.marker = {"fillColor":"red"};
			point.color = "red";
			point.name = cur_instances_info[j].instance_comment;
			point.dataLabels = {
				"align": "left",
				"enabled": true,
				"rotation": 270,
				"x": 2,
				"y": -10,
				"formatter": function() {
					return this.point.name
				}
			};
		}

		if (cur_num_parts == 1)
			a.push(point);
		else
			a.unshift(point);
		}

		p.data = a;
		series_datax.push(p);

		var pMean = {
			"name": "Mean",
			"data": [],
			"dashStyle": "Dash",
			"color": "black",
			"marker": {
				"enabled": false
			}
		};

		for (var i = cur_num_instances - 1; i >= 0; i--)
		pMean.data.push(con_xbar);

		series_datax.push(pMean);

		var const_xbar_ucl = {
			"name": "UCL",
			"data": [],
			"dashStyle": "Dash",
			"color": "orange",
			"marker": {
				"enabled":false
			}
		};

		for (var i = cur_num_instances - 1; i >= 0; i--)
		const_xbar_ucl.data.unshift(roundIt(ucl_xbar));

		series_datax.push(const_xbar_ucl);

		var const_xbar_lcl = {
			"name": "LCL",
			"data": [],
			"dashStyle": "Dash",
			"color": "orange",
			"marker": {
				"enabled":false
			}
		};

		for (var i = cur_num_instances - 1; i >= 0; i--)
		const_xbar_lcl.data.unshift(roundIt(lcl_xbar));

		series_datax.push(const_xbar_lcl);

		var label = '<strong>Mean xBar</strong>: ' + con_xbar.toFixed(3) +
		'<br /><br /><strong>UCL-xBar</strong>: ' + ucl_xbar.toFixed(3) +
		'<br /><strong>LCL-xBar</strong>: ' + lcl_xbar.toFixed(3);

		/*
		'<br /><br /><strong>UCL-Range</strong>: ' + ucl_range.toFixed(3) +
		'<br /><strong>LCL-Range</strong>: ' + lcl_range.toFixed(3);
		*/

		chart1 = new Highcharts.Chart({
			"chart": {
				"renderTo": "div_xbar",
				"type": "line",
				"borderWidth":1,
				"marginBottom": 130
			},
			"title": {
				"margin": 25,
				"text": company_name + " SPC Xbar Chart"
			},
			"subtitle": {
				"text": tool_cavity + ", " + dp_info + " on " + cur_date
			},
			"xAxis": {
				"categories": dp_names,
				"labels": {
					"rotation": 90,
					"y": 50
				},
				"max": cur_num_instances - 1,
				"maxPadding": 0.2,
				"minPadding": 0.2
			},
			"yAxis": {
				"title": {
					"text": 'mm'
				}
			},
			"tooltip": {
				"crosshairs": [true, true]
			},
			"legend": {
				"align": 'right',
				"verticalAlign": 'top',
				"x": -10,
				"y": 40,
				"shadow": true
			},
			"labels": {
				"items": [{
					"html": label,
					"style": {
						"left": "550px",
						"top": "40px",
						"fontSize": "11px"
					}
				}]
			},
			"series": series_datax,
			"credits": {
				"text": "Nysus",
				"href": "http://www.nysus.net"
			}
		});

		var series_data = [];

		var pMean = {
			"name": "Mean",
			"data": [],
			"dashStyle": "Dash",
			"color": "black",
			"marker": {
				"enabled": false
			}
		};

		var pRange = {
			"name": "Range",
			"data": []
		};

		var d2 = cur_xbar;
		var d3 = cur_std_dev_avg;
		var D4 = getD4(cur_num_parts);

		for (var i = 0; i < cur_num_instances; i++) {

			pMean.data.push(roundIt(con_range_avg));
			pRange.data.unshift(roundIt(cur_ranges[i]));

		}

		series_data.push(pMean);

		var const_range_ucl = {
			"name": "UCL",
			"data": [],
			"dashStyle":"Dash",
			"color": "orange",
			"marker": {
				"enabled": false
			}
		};

		for (var i = cur_num_instances - 1; i >= 0; i--)
		const_range_ucl.data.unshift(roundIt(ucl_range));

		series_data.push(const_range_ucl);

		var const_range_lcl = {
			"name": "LCL",
			"data": [],
			"dashStyle": "Dash",
			"color": "orange",
			"marker": {
				"enabled": false
			}
		};

		for (var i = cur_num_instances - 1; i >= 0; i--)
		const_range_lcl.data.unshift(roundIt(lcl_range));

		const_range_lcl.visible = false;
		series_data.push(const_range_lcl);

		var p = {
			"name": "Avg"
		};
		var a = [];

		for (var j = 0; j < cur_ranges.length; j++) {

			/*var point = {
			"y": cur_ranges[j]
			};*/

			var point = parseFloat(cur_ranges[j]);

			if (ucl_range < parseFloat(cur_ranges[j])) {

				point.marker = {
					"fillColor": "red"
				};
				point.color = "red";

				if (cur_instances_info[j].instance_comment != null)
				point.name = cur_instances_info[j].instance_comment;

				point.dataLabels = {
					"align": 'left',
					"enabled": true,
					"rotation": 270,
					"x": 2,
					"y": -10,
					"formatter": function() {
						return this.point.name;
					}
				};

			} else if (lcl_range > parseFloat(cur_ranges[j])) {

				point.marker = {
					"fillColor": "red"
				};
				point.color = "red";

				if (cur_instances_info[j].instance_comment != null)
				point.name = cur_instances_info[j].instance_comment;

				point.dataLabels = {
					"align": 'left',
					"enabled": true,
					"rotation": 270,
					"x": 2,
					"y": -10,
					"formatter": function() {
						return this.point.name;
					}
				};

			}

			if (cur_num_parts == 1)
			a.push(point);
			else
				a.unshift(point);

			}

			p.data = a;
			series_data.push(p);

			var label = '<strong>Mean Range</strong>: ' + con_range_avg.toFixed(3) +
			'<br /><br /><strong>UCL-Range</strong>: ' + ucl_range.toFixed(3) +
			'<br /><strong>LCL-Range</strong>: ' + lcl_range.toFixed(3);

			chart2 = new Highcharts.Chart({
				"chart": {
					"renderTo": "div_range",
					"type": "line",
					"borderWidth": 1,
					"marginBottom": 130
				},
				"title": {
					"margin": 25,
					"text": company_name + " SPC Range Chart"
				},
				"subtitle": {
					"text": tool_cavity + ", " + dp_info + " on " + cur_date
				},
				"labels": {
					"items": [{
						"html": label,
						"style": {
							"left": "550px",
							"top": "40px",
							"fontSize": "11px"
						}
					}]
				},
				"xAxis": {
					"categories": dp_names,
					"labels": {
						"rotation": 90,
						"y": 50
					},
					"max": cur_num_instances-  1,
					"maxPadding": 0.2,
					"minPadding": 0.2
				},
				"yAxis": {
					"title": {
						"text": 'mm'
					},
					"min": 0
				},
				"tooltip": {
					"crosshairs": [true, true]
				},
				"legend": {
					"align": "right",
					"verticalAlign": "top",
					"x": -10,
					"y": 40,
					"shadow": true
				},
				"series": series_data,
				"credits": {
					"text": "Nysus",
					"href": "http://www.nysus.net"
				}
			});

			$('export_all').observe('click', function (e) {
				Highcharts.exportCharts([chart1, chart2],{
					"type": "application/pdf",
					"filename": "my-pdf"
				});
			});

		}