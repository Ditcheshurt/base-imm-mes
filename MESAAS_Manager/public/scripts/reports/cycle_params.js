var processor = './ajax_processors/reports/cycle_params.php';
var defectTable;
var detailsTable;
var lang_strings;

String.prototype.toProperCase = function(){
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
		function($1){
			return $1.toUpperCase();
		}
	);
};

$(function() {

	$.getJSON('./config/language/app-en.json').done(function (data) {
		lang_strings = data;
		build_report_parameters();
		build_report_results();
		build_grid_view();
	});

	$('.details-table-modal').on('shown.bs.modal', function () {
	  //console.log($('.details-table-modal').data());
	  build_details_table_modal();
	});

});

function build_report_parameters() {
	// panel vars
	var $panel = $('.report-parameters').nysus_panel({ 'panel_title': 'Report Parameters' });
	var $panel_body = $panel.find('.panel-body');
	var $panel_footer = $panel.find('.panel-footer');
	//control vars
	var $run = $('<button class="btn btn-success btn-xs pull-right run-report">Run Report</button>');
	var $start_date = $('<div class="col-md-3" />');
	var $end_date = $('<div class="col-md-3" />');
	var $machines = $('<div class="col-md-4" />');
	var $tools = $('<div class="col-md-4" />');
	var $params = $('<div class="col-md-4" />');
	var $reason
	var $group_by = $('<div class="col-md-3" />');
	var $shifts = $('<div class="col-md-4" />');
	var $quantify_time = $('<div class="col-md-3" />');
	//var $btn_group_quantify_time = $('<div class="btn-group control-buttons btn-group-quantify-time" data-toggle="buttons">');
	var $minutes = $('<label class="btn btn-sm btn-info active"><input type="radio" id="" name="time-stuff" value="minutes" />Minutes</label>');
	var $percent = $('<label class="btn btn-sm btn-info"><input type="radio" id="" name="time-stuff" value="percent" />%</label>');
	var $hours = $('<label class="btn btn-sm btn-info"><input type="radio" id="" name="time-stuff" value="hours" />Hours</label>');
	// control option vars
	var start_date_options = {
		label_text: 'Start Date'
		, label_tooltip: 'Select start date'
		, control_id: 'start-date'
		, control_tag: 'input'
	};
	var end_date_options = {
		label_text: 'End Date'
		, label_tooltip: 'Select end date'
		, control_id: 'end-date'
		, control_tag: 'input'
	};
	var machines_options = {
		label_text: 'Machines'
		, label_tooltip: 'Select machine'
		, control_id: 'machines-combo'
		, control_tag: 'select'
	};
	var tools_options = {
		label_text: lang_strings.tool.upper + 's'
		, label_tooltip: 'Select ' + lang_strings.tool.lower
		, control_id: 'tools-combo'
		, control_tag: 'select'
	};
	var shift_options = {
		label_text: 'Shift'
		, label_tooltip: 'Select shift'
		, control_id: 'shifts-combo'
		, control_tag: 'select'
	};
	var params_options = {
		label_text: 'Parameters'
		, label_tooltip: 'Select parameters'
		, control_id: 'params-combo'
		, control_tag: 'select'
	};
	var group_by_options = {
		label_text: 'Group By'
		, label_tooltip: 'Select grouping'
		, control_id: 'group-by-combo'
		, control_tag: 'select'
	};
	/*
	var button_group_quantify_time_options = {
		label_text: 'Quantify By'
		, label_tooltip: 'Select how to quantify'
		, control_id: 'removeme'
		, control_tag: 'button'
	};*/

	// add options to controls
	$start_date.nysus_labeled_control(start_date_options);
	$end_date.nysus_labeled_control(end_date_options);
	$machines.nysus_labeled_control(machines_options);
	$tools.nysus_labeled_control(tools_options);
	$params.nysus_labeled_control(params_options);
	$group_by.nysus_labeled_control(group_by_options);
	$shifts.nysus_labeled_control(shift_options);

	// btn group
	/*
	$quantify_time.nysus_labeled_control(button_group_quantify_time_options);
	$quantify_time.find('.control-body').empty();
	$minutes.appendTo($btn_group_quantify_time);
	$percent.appendTo($btn_group_quantify_time);
	$hours.appendTo($btn_group_quantify_time);
	$btn_group_quantify_time.appendTo($quantify_time);
	*/
	// end btn group
	$start_date.find('#start-date').nysus_date_input();
	$end_date.find('#end-date').nysus_date_input();

	// additional classes
	$panel_footer.addClass('clearfix');

	// add controls to panel
	var $row = $panel_body.append('<div class="row" />');
	$start_date.appendTo($row);
	$end_date.appendTo($row);
	$group_by.appendTo($row);
	$row.appendTo($panel_body);
	$row = $panel_body.append('<div class="row" />');
	$machines.appendTo($row);
	$tools.appendTo($row);
	$shifts.appendTo($row);
	$row.appendTo($panel_body);
	$row = $panel_body.append('<div class="row" />');
	var $params_wrapper = $('<div class="col-md-12 params-wrapper"><div class="breadcrumb" />');
	$params.appendTo($params_wrapper);
	$params_wrapper.appendTo($row);
	$quantify_time.appendTo($row);
	$run.appendTo($panel_footer);

	// additional attributes
	$('#start-date').attr('type', 'date');
	$('#end-date').attr('type', 'date');

	//databinding
	build_combo($('#machines-combo'), {action: 'get_machines'}, [{key: 'multiple', value: 'multiple'}]);
	build_combo($('#tools-combo'), {action: 'get_tools'}, [{key: 'multiple', value: 'multiple'}]);

	build_params_combo();
	$('#params-combo').on('change', function() {
		var $sel = $('#params-combo :selected');
		if ($("button[data-parent-id="+$sel.val()+"]").length == 0 && $sel.val().length > 0) {
			var icon = '<div class="glyphicon glyphicon-remove" />';
			$('<button class="btn btn-primary" onClick="remove_breadcrumbs(this)" data-parent-id="' + $sel.val() + '">' + icon + '&nbsp;' + $sel.html() + '</button>').appendTo($params_wrapper.find('.breadcrumb'));
		}
	});

	var group_options = [];
	group_options.push({id:'date', text:'Date'});
	group_options.push({id:'machine', text:'Machine'});
	group_options.push({id:'tool', text:lang_strings.tool.upper});
	group_options.push({id:'part', text:'Part'});
	group_options.push({id:'shift', text:'Shift'});
	group_options.push({id:'defect_description', text:'Defect'});
	var $combo = $('#group-by-combo');
	$combo.attr('multiple', 'multiple');
	$combo.select2({data:group_options});

	var shift_options = [];
	shift_options.push({id: 1, text: 'Shift 1'});
	shift_options.push({id: 2, text: 'Shift 2'});
	shift_options.push({id: 3, text: 'Shift 3'});
	$combo = $('#shifts-combo');
	$combo.attr('multiple', 'multiple');
	$combo.select2({data:shift_options});

	// events
	$run.on('click', run_report);

}

function get_selections_as_object() {
	var sel_machines = $('#machines-combo').select2().val();
	var sel_tools = $('#tools-combo').select2().val();
	var sel_params = [];
	var sel_shifts = $('#shifts-combo').select2().val();
	var group_by = $('#group-by-combo').select2().val();
	var from_date = $('#start-date').val();
	var to_date = $('#end-date').val();
	var data = {
		from_date: from_date,
		to_date: to_date
	}

	if (sel_machines != null) {
		data.sel_machines = sel_machines;
	}
	if (sel_tools != null) {
		data.sel_tools = sel_tools;
	}
	if ($('.breadcrumb :last-child').length > 0) {
		//data.sel_params = $('.breadcrumb button:last-child').data().parentId;
		$(".breadcrumb button").each(function (d) {
			//console.log($(this).attr("data-parent-id"));
			sel_params.push($(this).attr("data-parent-id"));
		});
		data.sel_params = sel_params;
	}
	if (sel_shifts != null) {
		data.sel_shifts = sel_shifts;
	}
	if (group_by != null) {
		data.group_by = group_by;
	}

	return data;
}

function remove_breadcrumbs(obj) {
	console.log(obj);

	//$(obj).nextAll().remove();
	$(obj).remove();

}

function build_params_combo() {
	console.log('build combo')
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: {
		action: 'get_params',
	  }
	})
	.done(function(jso) {
		console.log(jso);
		var combo = $('#params-combo');
		combo.empty();
		if (!jso || jso.length <= 0) {
			combo.hide();
		} else {
			combo.show();
		}

		$('<option value="">- All -</option>').appendTo(combo);
		$.each(jso, function(k,v) {
			$('<option value="' + v.id + '">' + v.text + '</option>').appendTo(combo);
		});
	});
}

function build_combo(combo, data, attr, selection) {
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		$.each(attr, function(k,v) {
			combo.attr(v.key, v.value);
		});
		combo.select2({data:jso});
		if (selection != undefined && selection != null && selection != '') {
			combo.select2('val', selection);
		}
	});
}

function build_report_results() {
	var $panel = $('.report-results').nysus_panel({ 'panel_title': 'Report Results', 'show_footer': false });
	var $body = $panel.find('.panel-body');
	var $chart = $('<div class="chart-display col-lg-8" />');
	var $pieChart = $('<div class="pie-display col-lg-4" />');
	var $row = $('<div class="row" />');
	$body.empty();
	$row.appendTo($body);
	$chart.appendTo($row);
	$pieChart.appendTo($row);
}

function build_grid_view() {
	var $grid_view = $('.grid-view').nysus_panel({ 'panel_title': 'Grid View', 'show_footer': false });
	var $table = $('<table class="table table-condensed table-bordered table-striped table-display"></table>');
	$table.appendTo($grid_view.find('.panel-body'));
}

function run_report() {
	console.log('running report');

	var data = get_selections_as_object();

	data.action = 'run_report';
	$('.report-results').find('.panel-body').html('Loading...');

	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(after_run_report);
}

function after_run_report(jso) {
	build_report_results();
	if (jso) {
		populate_chart_view(jso);
		populate_top();
		populate_grid_view(jso);
	} else {
		$('.table-display').empty();
		$('.chart-display').empty();
		$('.report-results').find('.panel-body').html('No Results Found');
	}
}

function populate_top() {
	console.log('populate top defects');
	var quantify = $('.btn-group-quantify-time label.active input').val();
	var data = get_selections_as_object();

	data.action = 'get_top';

	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		if (!jso) {
			return;
		}
		var data = [];
		var i = 0;
		$.each(jso, function(k,v) {
			//var objs = Object.getOwnPropertyNames(v);
			data.push({ name: v.defect_description, y: v.defects, color: Highcharts.getOptions().colors[i] });
			i++;
		});

		$('.pie-display').highcharts({
			chart: {
				type: 'pie'
			},
			title: {
				text: 'Top 5 Defects'
			},
			series: [{
				data: data
			}],
			//center: [50, 10],
			//size: 200,
			plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
			credits: false
		});

	});

}

function populate_chart_view(jso) {
	console.log('populate chart');
	var data = [];

	$.each(jso, function(k,v) {
		var objs = Object.getOwnPropertyNames(v);
		data.push({ name: v[objs[0]], y: v.defects, mydata: v });
	});

	$('.chart-display').highcharts({
		chart: {
			type: 'column'
		},
		/*tooltip: {
			formatter: function () {
				var str = '';
				if (this.point.mydata.machine != undefined) {
					str += '<b>' + this.point.mydata.machine + '</b><br/>';
				}
				if (this.point.mydata.tool != undefined) {
					str += '<b>' + this.point.mydata.tool + '</b><br/>';
				}
				str += this.series.name + ': ' + this.y.toPrecision(4) + '<br/>';
				return str;
			}
			//pointFormat: 'Downtime: <b>{point.y:.1f}</b>'
		},*/
		title: {
			text: 'Defect Results'
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -60,
				style: {
					//fontSize: '13px',
					//fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Defect'
			}
		},
		series: [{
			name:'Machine Defects',
			data: data,
			dataLabels: {
				enabled: true,
				rotation: -90,
				color: '#FFFFFF',
				align: 'right',
				format: '{point.y}', // one decimal
				y: 10, // 10 pixels down from the top
				style: {
					//fontSize: '13px',
					//fontFamily: 'Verdana, sans-serif'
				}
			}
		}],
		credits: false

	});
}

function populate_grid_view(jso) {
	console.log('populate table');

	if (defectTable != null) {
		//defectTable.clear().draw();
        defectTable.destroy();    //so the thing will update
        $('.table-display').empty();
    }
	if (!jso) {
		jso = [{Date: 'No Data'}];
	}

	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	cols.push({'title':'Details', 'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	$.each(objs, function(k,v) {
		cols.push({data: v, title: v.toProperCase()});
	});
	cols.push({'title':'Plot', 'className': 'plot-control', 'orderable': false, 'data': null, 'defaultContent': ''});

	defectTable = $('.table-display').DataTable({
        bInfo:false,
        autoWidth:true,
        retrieve:true,
        //dom: 'T<"clear">lfrtip',
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"
            ]
        },
        data: jso,
        columns: cols

    });

	//show detail on click
	$('.table-display tbody').on('click', 'td.details-control', function () {
		console.log('Details control clicked');
		event.preventDefault();
		var tr = $(this).closest('tr');
		var row = defectTable.row(tr);

		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();
		}
		else {
			tr.addClass('shown');
			populate_details(row);
		}
	});

	//show detail on click
	$('.table-display tbody').on('click', 'td.plot-control', function () {
		console.log('Plot control clicked');
		event.preventDefault();
		var tr = $(this).closest('tr');
		var row = defectTable.row(tr);

		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();
		}
		else {
			tr.addClass('shown');
			populate_details_plot(row);
		}
	});
}

function populate_details_plot(row) {
	console.log('Populate image plot');
	var row_data = row.data();
	var data = get_selections_as_object();
	var $childContent = $('<div class="panel panel-primary"></div>');
	data.action = 'get_defect_locations';

	console.log(row_data);
	console.log(data);

	if (row_data != undefined && row_data.ID != undefined) {
		data.machine_defect_ID = [row_data.ID];
	}
	if (row_data != undefined && row_data.date != undefined) {
		data.sel_date = row_data.date;
	}

	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		console.log(jso);
		$.each(jso.parts, function(k,v) {
			var part_ID = v.part_ID;
			var $container = $('<div class="panel panel-primary image_container" />');
			var $header = $('<div class="panel-heading">' + v.part_number + ' - ' + v.part_desc + '</div>');
			var $body = $('<div class="panel-body" />');
			var $image = $('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="div_defect_location_container" width="600px" height="600px"></svg>');
			//$image.attr('style', "background-image:url('" + v.part_image + "');background-repeat: no-repeat;").show();

			var img = document.createElementNS('http://www.w3.org/2000/svg','image');
			img.setAttributeNS(null,'height','600');
			img.setAttributeNS(null,'width','600');
			img.setAttributeNS('http://www.w3.org/1999/xlink','href', v.part_image );
			img.setAttributeNS(null,'x','0');
			img.setAttributeNS(null,'y','0');
			img.setAttributeNS(null, 'visibility', 'visible');
			$image.append(img);

			$header.appendTo($container);
			$image.appendTo($body);
			$body.appendTo($container);
			$container.appendTo($childContent);

			$.each(jso.locations, function(k,v) {
				if(v.part_ID = part_ID) {
					var c = $('<circle />');

					c.addClass('defect-dot');
					c.attr("cx", v.defect_location_x);
					c.attr("cy", v.defect_location_y);
					c.attr("r", 5);
					c.attr("stroke", "#CCCCCC");
					c.attr("stroke-width", 2);
					c.attr("fill", "orange");
					c.attr("pointer-events", "visible");

					c.appendTo($image);
				}
			});
			$image.html($image.html());
		});
		row.child($childContent).show();
	});
}

function populate_details(row) {
	console.log('Populate details');
	var data = get_selections_as_object();
	var $childContent = $('<div class="panel panel-primary"><div class="well"><table class="table-condensed table-bordered table-striped details-table"><tbody><tr><td>Loading...</td></tr></tbody></table></div></div>');
	var row_data = row.data();
	row.child($childContent).show();

	data.action = 'drilldown';
	if (row_data.date != null) {
		data.from_date = row_data.date;
		data.to_date = row_data.date;
	}
	if (row_data.machine != null) {
		data.machine = [row_data.machine];
	}
	if (row_data.part != null) {
		data.sel_part = [row_data.part];
	}

	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		after_populate_details(row, jso);
	});

}

function after_populate_details(row, jso) {
	//console.log('Details jso');
	//console.log(jso);
	if (detailsTable != null) {
        //detailsTable.destroy();    //so the thing will update
        row.child().find('.details-table').empty();
    }

	var cols = [];
	cols.push({'title': 'Plot', 'className': 'plot-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	var objs = Object.getOwnPropertyNames(jso[0]);

	$.each(objs, function(k,v) {
		var title;
		if ( v == 'ID' || v == 'machine_type_ID' || v == 'reason_ID' || v == 'machine_ID' || v == 'tool_ID' || v == 'defect_ID' || v == 'part_ID') {
			title = v;
			visible = false;
			//cols.push({ data: v, title: v, visible: false});
		} else if (v == 'tool') {
			title = '<span data-localize="tool.upper">Tool</span>';
			visible = true;
		} else {
			title = v.toProperCase();
			visible = true;
			//cols.push({ data: v, title: v.toProperCase()});
		}

		cols.push({data: v, title: title, visible: visible});
	});

	detailsTable = row.child().find('.details-table').DataTable({
        "bInfo":false,
        autoWidth:true,
        retrieve: true,
        dom: 'T<"clear">lfrtip',
		sRowSelect: "os",
        tableTools: {
            "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "portrait"
                },
                "print"

            ]
        },
        data: jso,
        columns: cols

    });

	//show detail on click
	$('.details-table tbody').on('click', 'td.plot-control', function () {
		console.log('Plot control clicked');
		event.preventDefault();
		event.stopPropagation();
		var tr = $(this).closest('tr');
		var row = detailsTable.row(tr);

		if (row.child.isShown()) {
			// This row is already open - close it
			tr.removeClass('shown');
			row.child.hide();
		}
		else {
			tr.addClass('shown');
			populate_details_plot(row);
		}
	});

}

/*
function populate_defect_detail(row) {
	var $childContent = $('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="div_defect_location_container" height="600" width="600" style="display:none;background-repeat: no-repeat;"></svg>');
	var data = {action: 'get_defect_locations', machine_defect_ID: [row.data().ID] };

	$childContent.css('background-image', 'url(../../machine_mes2/public/images/part_images/part_' + row.data().part_ID +'/photo.jpg)').show();
	row.child($childContent).show();
	$.ajax({
	  method: "POST",
	  url: processor,
	  data: data
	})
	.done(function(jso) {
		if (!jso) {
			return;
		}
		for (var i = 0; i < jso.length; i++) {
			var c = $('<circle />');

			c.addClass('defect-dot');
			c.attr("cx", jso[i].defect_location_x);
			c.attr("cy", jso[i].defect_location_y);
			c.attr("r", 20);
			c.attr("stroke", "#CCCCCC");
			c.attr("stroke-width", 2);
			c.attr("fill", "orange");
			c.attr("pointer-events", "visible");
			//c.attr("onclick", "removeDot(event);");

			$childContent.append(c);
		}
		$childContent.html($childContent.html());
	});

}
*/
