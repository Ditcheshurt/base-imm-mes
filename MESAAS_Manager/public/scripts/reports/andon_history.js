var processor = './ajax_processors/reports/andon_history.php';
var categories = [];

$(document).ready(
	function() {
		var d = new Date();
		$('#inpStart').val(Date.Format(d, 'yyyy-mm-dd'));
		$('#inpEnd').val(Date.Format(d, 'yyyy-mm-dd'));

		$('FORM.params').on('submit', function() {
			$.getJSON(processor, {"action":"run_report", "category_ID":$('#selCategory').val(), "start":$('#inpStart').val(), "end":$('#inpEnd').val()}, populateReport);
			return false;
		});

		$('BUTTON.export').on('click', function() {
			doExport('tbl_detail');
		});

		$.getJSON(processor, {"action":"load_categories"}, populateCategories);
	}
);

function populateCategories(jso) {
	categories = jso.categories;

	var s = $('#selCategory').html('');
	for (var i = 0; i < categories.length; i++) {
		var op = $('<option />').text(categories[i].category_name).val(categories[i].ID);
		s.append(op);
	}
}

function populateReport(jso) {
	var sum = jso.summary||[];
	var det = jso.detail||[];

	$('#div_num_items').html(jso.summary[0]['num_items']);
	$('#div_avg_ack').html(parseFloat(jso.summary[0]['avg_ack']||0).toFixed(2)).addClass('minute');
	$('#div_avg_close').html(parseFloat(jso.summary[0]['avg_close']||0).toFixed(2)).addClass('minute');

	var t = $('#tbl_detail');
	var r = t.find('THEAD TR').html('');

	var th = $('<TH />').text('Entry Time');
	r.append(th);
	var th = $('<TH />').text('Entry Operator');
	r.append(th);
	var th = $('<TH />').text('Line');
	r.append(th);
	var th = $('<TH />').text('Station');
	r.append(th);
	var th = $('<TH />').text('Reason');
	r.append(th);
	var th = $('<TH />').text('Ack Time');
	r.append(th);
	var th = $('<TH />').text('Ack By');
	r.append(th);
	var th = $('<TH />').text('Pick Time');
	r.append(th);
	var th = $('<TH />').text('Closed Time');
	r.append(th);

	var b = t.find('TBODY').html('');
	if (det.length == 0) {
		var r = $('<TR />');
		var c = $('<TD />').text('No records found.');
		c.attr('colspan', '20');
		r.append(c);
		b.append(r);
		$('BUTTON.export').css('display', 'none');
	} else {
		$('BUTTON.export').css('display', 'block');
	}
	for (var i = 0; i < det.length; i++) {
		var r = $('<TR />');
		var c = $('<TD />').text(det[i].entry_time);
		r.append(c);
		var c = $('<TD />').text(det[i].enterer||'UNKNOWN');
		r.append(c);
		var c = $('<TD />').text(det[i].line_code||'UNKNOWN');
		r.append(c);
		var c = $('<TD />').text(det[i].station_desc||'UNKNOWN');
		r.append(c);
		var c = $('<TD />').text(det[i].reason_desc||'UNKNOWN');
		r.append(c);
		var c = $('<TD />').text(parseFloat(det[i].ack_delta).toFixed(2)).addClass('minute');
		r.append(c);

		var c = $('<TD />').text(det[i].acker||'UNKNOWN');
		r.append(c);
		var c = $('<TD />');
		if (det[i].select_part) {
			c.html(det[i].part_number + '<br>' + parseFloat(det[i].pick_delta).toFixed(2)).addClass('minute');
		} else {
			c.text('N/A');
		}
		r.append(c);
		var c = $('<TD />').text(parseFloat(det[i].closed_delta).toFixed(2)).addClass('minute');
		r.append(c);

		b.append(r);
	}

	$('.result').fadeIn();
}

function doExport(tbl_id) {
	var d = '';
	$('#' + tbl_id + ' TR').each(function(i, r) {
		$(r).find('TH,TD').each(function(j, c) {
			d += c.innerHTML + ',';
		});
		d += '\r';
	});

	$('#inpExport').val(d);
	$('FORM.export').submit();
}