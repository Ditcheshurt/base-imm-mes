
(function ($) {

	var timeout;
	var options = {
		language: 'en',
		pathPrefix: 'config/language'
	};

	$(document).ready(function () {

		$('[data-localize]').localize('app', options);
		
		options.skipLanguage = 'en';

		$(document).bind('DOMSubtreeModified', function () {
			clearTimeout(timeout);
			setTimeout(function () {
				$('[data-localize]').localize('app', options);
			}, 200);
		});

	});

})($);