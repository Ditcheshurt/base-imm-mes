
function load_operators (params, callback) {
	if (params) {
		params.action = 'get_operators';
	} else {
		params = {action: 'get_operators'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_operators (data) {
	$('#operator_select').empty();
	if (!data) {return;}

	$.each(data, function (i, operator) {
		var el = $('<option name="operator_id"></option>');
		el.prop('id', 'operator_'+operator.ID)
		.val(operator.ID)
		.html(operator.name);

		$('#operator_select').append(el);
	});
}

function load_stations (params, callback) {
	if (params) {
		params.action = 'get_stations';
	} else {
		params = {action: 'get_stations'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_stations (data) {
	$('#station_select').empty();
	if (!data) {return;}

	$.each(data, function (i, station) {
		var el = $('<option name="station_id"></option>');
		el.prop('id', 'station_'+station.ID)
		.val(station.ID)
		.html(station.name);

		$('#station_select').append(el);
	});
}

function load_lines (params, callback) {
	if (params) {
		params.action = 'get_lines';
	} else {
		params = {action: 'get_lines'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_lines (data) {
	$('#line_select').empty();
	if (!data) {return;}

	$.each(data, function (i, line) {
		var el = $('<option name="line_id"></option>');
		el.prop('id', 'line_'+line.line_ID)
		.val(line.line_ID)
		.html(line.line_desc)
		.data(line);

		$('#line_select').append(el);
	});
}

function load_statuses (callback) {
	$.getJSON(
	'ajax_processors/filter.php',
	{action: 'get_statuses'},
	callback
	);
}

function show_statuses (data) {
	var status_select = $('#status_select');

	status_select.empty();
	status_select.append('<option name="status_id" value="-1" selected>All Statuses</option>');

	if (!data) {return;}
	$.each(data, function (i, status) {
		var el = $('<option name="status_id"></option>');
		el.prop('id', 'status_'+status.ID)
		.val(status.ID)
		.html(status.status_desc);

		status_select.append(el);
	});
}

function load_systems (params, callback) {
	if (params) {
		params.action = 'get_systems';
	} else {
		params = {action: 'get_systems'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_systems (data) {
	$('#system_select').empty();
	if (!data) {return;}

	$.each(data, function (i, system) {
		var el = $('<option name="system_id"></option>');
		el.prop('id', 'system_'+system.ID)
		.val(system.ID)
		.html(system.system_name)
		.data(system);

		$('#system_select').append(el);
	});
}

function load_dispositions (params, callback) {
	if (params) {
		params.action = 'get_dispositions';
	} else {
		params = {action: 'get_dispositions'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_dispositions (data) {
	//$('#disposition_select').empty();
	if (!data) {return;}

	$.each(data, function (i, disposition) {
		var el = $('<option name="disposition"></option>');
		el.prop('id', 'disposition_'+i)
		.val(disposition.disposition)
		.html(disposition.disposition);

		$('#disposition_select').append(el);
	});
}

function load_machines (params, callback) {
	if (params) {
		params.action = 'get_machines';
	} else {
		params = {action: 'get_machines'};
	}

	$.getJSON(
	'ajax_processors/filter.php',
	params,
	callback
	);
}

function show_machines (data) {
	//$('#machine_select').empty();
	if (!data) {return;}

	$.each(data, function (i, machine) {
		var el = $('<option name="machine_id"></option>');
		el.prop('id', 'machine_'+machine.ID)
		.val(machine.ID)
		.html(machine.machine_name)
		.data(machine);

		$('#machine_select').append(el);
	});
}

function init_date_input (id) {
	// initialize the date input to the current date
	var today = new Date();
	var offset = today.getTimezoneOffset() * 60;
	var offset_date = new Date(today.getTime() - offset * 1000);
	var today_str = offset_date.toISOString().split('T')[0];
	$('#'+id).val(today_str);
}

function get_date_value (id) {
	var date_str = $('#'+id).val();
	return new Date(date_str).toISOString().split('T')[0];
}
