// example html
// <select id="test_select" name="test_id"></select>

// non-ajax example

// var test_data = [
//     {ID: 0, text: 'Option 0'},
//     {ID: 1, text: 'Option 1'},
//     {ID: 2, text: 'Option 2'}
// ];

// $('#test_select').nysus_select({
//     name: 'test_id',
//     key: 'ID',
//     data: test_data,
//     default_value: 1,
//     render: function (item_data) {return item_data.text;},
//     placeholder: 'Select Option'
// });


// ajax example

// $('#test_select').nysus_select({
//     name: 'line_id',
//     key: 'ID',
//     data_url: 'ajax_processors/filter.php',
//     params: {action: 'get_lines'},
//     render: function (line) {
//         // render the option
//         return line.line_desc;
//     },
//     placeholder: 'Select Line',
//     callback: function (data) {
//          //do some shit with the data
//     }
// });


// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

    "use strict";

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "nysus_select";
    var defaults = {
        propertyName: "value",
        id: 'ID'
    };

    // The actual plugin constructor
    function Plugin (element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).
            //console.log(this.element);

            // check for missing required options
            var err = false;
            var required = [
                'name',
                'key',
                'render'
            ];

            $(this.element).prop('disabled', true);

            var base_msg = 'Call to nysus_select missing required option: ';

            for (var i=0; i<required.length; i++) {
                if (this.settings[required[i]] == undefined) {
                    err = true;
                    console.error(base_msg + required[i]);
                }
            }

            if (this.settings.data == undefined && this.settings.data_url == undefined) {
                err = true;
                console.error(base_msg + 'data or data_url');
            }

            if (err) {return this;}

            if (this.settings.data != undefined) {
                // populate the select using the supplied data
                populate_select(this.element, this.settings);

                if (this.settings.callback != undefined) {
                    this.settings.callback(this.settings.data);
                }

            } else if (this.settings.data_url != undefined) {
                var that = this;
                // populate via ajax
                $.getJSON(
                    this.settings.data_url,
                    this.settings.params,
                    function (data) {
                        that.settings.data = data;
                        populate_select(that.element, that.settings);

                        if (that.settings.callback != undefined) {
                            that.settings.callback(data);
                        }
                    }
                );
            }
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function() {
            // if (!$.data(this, "plugin_" + pluginName)) {
            //     $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            // }
            $.data(this, "plugin_" + pluginName, new Plugin(this, options));
        });
    };

    function populate_select (element, options) {
        // populate the select input with options
        var el = $(element);
        el.empty();

        // set the placeholder option
        if (options.placeholder != undefined) {
            var option_el = $('<option name="' + options.name + '"></option>');
            option_el.prop('id', options.name + '_placeholder')
                     .val(-1)
                     .html(options.placeholder);
            el.append(option_el);
        }

        // add the rest of the options
        if (options.data) {
            $.each(options.data, function (i, item) {
                var option_el = $('<option name="' + options.name + '"></option>');
                option_el.prop('id', options.name + '_' + item[options.key])
                         .val(item[options.key])
                         .html(options.render(item));
                el.append(option_el);
            });
            el.prop('disabled', false);
        }

        // set the default value of the select
        if (options.default_value != undefined) {
            el.val(options.default_value);   
        }
    }

})(jQuery, window, document);