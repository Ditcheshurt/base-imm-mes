var G = G || {};
var NYSUS = NYSUS || {};
NYSUS.COMMON = NYSUS.COMMON || {};

NYSUS.COMMON.UTILITY = (function ($, G) {
    var that = this;

    var self = {
        
        open_location: function (url, params, target) {
            var keys = Object.keys(params);

            if (keys.length > 0) {
                var param_strs = keys.map(function (key) {
                    return key + '=' + params[key];
                });

                url += '?' + param_strs.join('&');
            }

            window.open(url, target);
        },

        seconds_to_hms: function (seconds) {
            var sec_num = parseInt(seconds, 10);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours   < 10) {hours = '0' + hours;}
            if (minutes < 10) {minutes = '0' + minutes;}
            if (seconds < 10) {seconds = '0' + seconds;}

            return hours + ':' + minutes + ':' + seconds;
        }

    };

    return self;

})($, G);