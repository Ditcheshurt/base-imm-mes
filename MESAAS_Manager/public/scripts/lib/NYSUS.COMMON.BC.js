var NYSUS = NYSUS || {};
NYSUS.COMMON = NYSUS.COMMON || {};
 
NYSUS.COMMON.BC = {

	sections: [
    	//{el_id: 'assembly_overview', title: 'Assembly Overview', loader: NYSUS.COMMON.BC.load_assembly, callback: NYSUS.COMMON.BC.show_assembly},
    	//{el_id: 'component_lots', title: 'Component Lots', loader: NYSUS.COMMON.BC.load_component_lots, callback: NYSUS.COMMON.BC.show_component_lots}
    ],

	init: function (element_ID, processor, key) {
		NYSUS.COMMON.BC.key = key;
		NYSUS.COMMON.BC.processor = processor;
		NYSUS.COMMON.BC.main_div = $('#'+element_ID);

	    NYSUS.COMMON.BC.set_title();
	    NYSUS.COMMON.BC.build_sections();
	    NYSUS.COMMON.BC.load_data();
	},

	set_title: function () {
		$('<div>Birth Certificate (Key: <span class="key"></span>)</div>').addClass('main-header').appendTo(NYSUS.COMMON.BC.main_div);
	},

	build_sections: function () {
		NYSUS.COMMON.BC.sections.forEach(function (section) {
			var section_el = $('<div></div>').prop({id: section.el_id}).addClass('section').hide().appendTo(NYSUS.COMMON.BC.main_div);
			$('<div class="section-header">'+section.title+':</div>').appendTo(section_el);
			$('<div class="section-content"></div>').appendTo(section_el);
		});
	},

	load_data: function () {
		NYSUS.COMMON.BC.sections.forEach(function (section) {
			section.loader(section.callback);
			//console.log(section);
		});
	},

	build_table: function (element, columns, data) {
		var table = $('<table></table>').prop({id: element.id+'_table'}).addClass('section-table').appendTo(element);
		var tbody = $('<tbody></tbody>').appendTo(table);
		var thead = $('<thead></thead>').appendTo(table);

		var htr = $('<tr></tr>').appendTo(thead);
		columns.forEach(function (column) {
			$('<th></th>').html(column.title).appendTo(htr);
		});

		if (data) {
		    data.forEach(function (item) {
		    	var tr = $('<tr></tr>').appendTo(tbody);
		    	columns.forEach(function (column) {
		    		if (column.render) {
		    			var value = column.render(item, item[column.name]);
		    		} else {
		    			var value = item[column.name];
		    		}
		    		$('<td></td>').html(value).appendTo(tr);
		    	});
		    });
		}
	},

	render_datetime: function (row, value) {
		return value ? value.date : '';
	}

};