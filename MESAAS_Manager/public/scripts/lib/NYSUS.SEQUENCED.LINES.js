var NYSUS = NYSUS || {};
NYSUS.SEQUENCED = NYSUS.SEQUENCED || {};

$(document).ready(function () {
    var processor = './ajax_processors/lib/sequenced/lines.php';

    NYSUS.SEQUENCED.LINES = {

        get_lines: function (cb) {
            $.getJSON(processor, { 'action':'get_lines' }, cb);
        }

    }
});