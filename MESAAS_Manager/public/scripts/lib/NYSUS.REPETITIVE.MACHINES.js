$(document).ready(
		function() {
			var processor = './ajax_processors/lib/repetitive/machines.php';

			if (typeof NYSUS == 'undefined') {
				NYSUS = {};
			}
			if (null == NYSUS.REPETITIVE) {
				NYSUS.REPETITIVE = [];
			}

			NYSUS.REPETITIVE.MACHINES = (function() {
				// private vars
				var debug = false;

				// private methods
				function get_machines(field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action": "get_machines"
							,"field_names": field_names
						}
					}).done(cb);
				}

				function update_machine(machine_id, field_names,
						field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "update_machines",
							"machine_id" : machine_id,
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}
				
				function get_machine_tool_parts(machine_id, field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action": "get_machine_tool_parts"
							,"machine_id": machine_id
							,"field_names": field_names
						}
					}).done(cb);
				}
				
				function get_machine_cycle_parts_by_serial(serial_number, field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action": "get_machine_cycle_parts_by_serial"
							,"serial_number": serial_number
							,"field_names": field_names
						}
					}).done(cb);
				}

				// return public methods
				return {
					get_machines : function(field_names, cb) {
						get_machines(field_names, cb);
					},
					update_machine : function(machine_id, field_names,
							field_values, cb) {
						update_lot_material_types(machine_id, field_names,
								field_values, cb);
					},
					get_machine_tool_parts : function(machine_id, field_names, cb) {
						get_machine_tool_parts(machine_id, field_names, cb);
					},
					get_machine_cycle_parts_by_serial : function(serial_number, field_names, cb) {
						get_machine_cycle_parts_by_serial(serial_number, field_names, cb);
					}

				} // end return

			})(); // end NYSUS.REPETITIVE.MACHINES

		});