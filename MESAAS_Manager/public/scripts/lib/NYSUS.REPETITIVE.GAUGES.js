$(document).ready(
		function() {
			var processor = './ajax_processors/lib/repetitive/gauges.php';

			if (typeof NYSUS == 'undefined') {
				NYSUS = {};
			}
			if (null == NYSUS.REPETITIVE) {
				NYSUS.REPETITIVE = [];
			}

			NYSUS.REPETITIVE.GAUGES = (function() {
				// private vars
				var debug = false;

				// private methods
				
				// GAUGE DETAIL
				function get_data(cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_gauges"
						}
					}).done(cb);
				}	
				
				// GAUGES
				function insert_gauge(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_gauge"
							,"field_names": field_names
							,"field_values": field_values							
						}
					}).done(cb);
				}
				
				function update_gauge(gauge_id, field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "update_gauge"
							,"gauge_id" : gauge_id
							,"field_names": field_names
							,"field_values": field_values							
						}
					}).done(cb);
				}	

				// INSTRUCTIONS
				function insert_instruction(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_instruction"
							,"field_names": field_names
							,"field_values": field_values							
						}
					}).done(cb);
				}
				
				function update_instruction(instruction_id, field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "update_instruction"
							,"instruction_id" : instruction_id
							,"field_names": field_names
							,"field_values": field_values							
						}
					}).done(cb);
				}
				
				
				// GAUGE EXTRA DATA
				function get_gauge_machine_cycle_parts_extra_data(field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_gauge_machine_cycle_parts_extra_data"
							,"field_names": field_names							
						}
					}).done(cb);
				}
				
				function insert_gauge_machine_cycle_parts_extra_data(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_gauge_machine_cycle_parts_extra_data"
							,"field_names": field_names
							,"field_values": field_values
						}
					}).done(cb);
				}
				
				// Litchfield parse mahrmmq incominf file
				function parse_mahrmmq(machine_cycle_part_ID, file, passed, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "parse_mahrmmq"
							,"machine_cycle_part_ID": machine_cycle_part_ID
							,"file": file
							,"passed": passed
						}
					}).done(cb);
				}
				
				function get_marhmmq_detail(machine_cycle_part_ID, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_mahrmmq_detail"
							,"machine_cycle_part_ID": machine_cycle_part_ID
						}
					}).done(cb);
				}

				// return public methods
				return {
					
					get_data : function(cb) {
						get_data(cb);
					},
					insert_gauge : function (field_names, field_values, cb) {
						insert_gauge(field_names, field_values, cb);
					},
					update_gauge : function (gauge_id, field_names, field_values, cb) {
						update_gauge(gauge_id, field_names, field_values, cb);
					},
					insert_instruction : function (field_names, field_values, cb) {
						insert_instruction(field_names, field_values, cb);
					},
					update_instruction : function (instruction_id, field_names, field_values, cb) {
						update_instruction(instruction_id, field_names, field_values, cb);
					},
					insert_gauge_machine_cycle_parts_extra_data : function (field_names, field_values, cb) {
						insert_gauge_machine_cycle_parts_extra_data(field_names, field_values, cb);
					},
					get_gauge_machine_cycle_parts_extra_data : function (field_names, cb) {
						get_gauge_machine_cycle_parts_extra_data(field_names, cb);
					},
					parse_mahrmmq : function(machine_cycle_part_ID, file, passed, cb) {
						parse_mahrmmq(machine_cycle_part_ID, file, passed, cb);
					},
					get_marhmmq_detail : function(machine_cycle_part_ID, cb) {
						get_marhmmq_detail(machine_cycle_part_ID, cb);
					}
				
				} // end return

			})(); // end NYSUS.REPETITIVE.GAUGES
	
		});