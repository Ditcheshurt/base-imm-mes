var G = G || {};
var NYSUS = NYSUS || {};
NYSUS.COMMON = NYSUS.COMMON || {};
 
NYSUS.COMMON.REPORT = (function ($, G) {
    var that = this;

    var self = {
        page_title: '',

        export_pdf: function (table) {
            console.log('pdf export');
            var doc = new jsPDF('p', 'pt');

            var totalPagesExp = "{total_pages_count_string}";

            var footer = function (data) {
                var str = "Page " + data.pageCount;
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }
                doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 30);
            };

            doc.text(self.page_title, 40, 50);
            
            var cols = table.columns().eq(0).map(function (i) {
                var c = table.column(i);
                return {dataKey: c.dataSrc(), title: $(c.header()).text()};    
                
                //return $(c.header()).text();
            }).toArray().filter(function (c) {return c.dataKey != null;});

            console.log(cols);
            var rows = table.rows().data().map(function (r) {
                // console.log(r);
                //var rendered = table.cells(r, null).render();
                var res_row = {};
                for (i=0; i<cols.length; i++) {
                    var c = cols[i];
                    var key = c.dataKey;
                    var value = r[key];
                    // console.log(c);
                    // console.log(value);
                    
                    if (key && value && value.date) {
                        res_row[key] = value.date;
                    } else if (key && value) {
                        res_row[key] = value.toString();
                    }
                    else if(key.includes(".date") && r[key.substring(0, key.length - 5)].date){
                        res_row[key] = r[key.substring(0, key.length - 5)].date;
                    }
                    
                }

                return res_row;
            }).toArray();
            console.log(rows);
            //var res = doc.autoTableHtmlToJson(document.getElementById('SearchResults'));
            //console.
            doc.autoTable(cols, rows, {
                startY: 60,
                styles: {
                    fontSize: 6,
                    cellPadding: 3,
                    halign: 'left',
                    valign: 'middle',
                },
                headerStyles: {
                    fillColor: [44, 62, 80],
                    halign: 'left',
                    valign: 'middle',
                },
                afterPageContent: footer
            });

            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            //return doc;
            doc.save('export.pdf');
        },
		  
		  export_landscape_pdf: function (table) {
            console.log('pdf export');
            var doc = new jsPDF('landscape', 'pt');

            var totalPagesExp = "{total_pages_count_string}";

            var footer = function (data) {
                var str = "Page " + data.pageCount;
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === 'function') {
                    str = str + " of " + totalPagesExp;
                }
                doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 30);
            };

            doc.text(self.page_title, 40, 50);
            
            var cols = table.columns().eq(0).map(function (i) {
                var c = table.column(i);
                return {dataKey: c.dataSrc(), title: $(c.header()).text()};    
                
                //return $(c.header()).text();
            }).toArray().filter(function (c) {return c.dataKey != null;});

            console.log(cols);
            var rows = table.rows().data().map(function (r) {
                // console.log(r);
                //var rendered = table.cells(r, null).render();
					 
                var res_row = {};
                for (i=0; i<cols.length; i++) {
                    var c = cols[i];
                    var key = c.dataKey;
                    var value = r[key];
                    // console.log(c);
                    // console.log(value);

                    if (key && value && value.date) {
                        res_row[key] = value.date;
                    } else if ((!isNaN(key)) && value) {
                        res_row[key] = value.toString();
                    }
                    
                }

                return res_row;
            }).toArray();
            console.log(rows);
            //var res = doc.autoTableHtmlToJson(document.getElementById('SearchResults'));
            //console.
            doc.autoTable(cols, rows, {
                startY: 60,
                styles: {
                    fontSize: 6,
                    cellPadding: 1,
                    halign: 'center',
                    valign: 'middle',
                },
                headerStyles: {
                    fillColor: [44, 62, 80],
                    halign: 'center',
                    valign: 'middle',
                },
                afterPageContent: footer
            });

            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            //return doc;
            doc.save('export.pdf');
        },
		  
		  print_element: function(element, use_table_style, custom_style){
			  elToPrint = element[0];
			
			var style_to_write = '<style type = text/css>';
			
			if(use_table_style){
				style_to_write += 'table, .panel, .container-fluid, td, th, .table-bordered th, .table-bordered td {border: none; padding: 0; margin: 0; box-shadow: none; font-size: 10px; }' +  
				'.table>thead>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td, td, th, table.dataTable thead > tr > th {padding: 4px; text-align: center; border-spacing: 0;}' +  
				'.printable-title {display: block;} .table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th {background-color: #f9f9f9;}'; 
			
			}
			style_to_write += ' .no-export {display: none;} ';
			if(custom_style){
				style_to_write += custom_style;
			}
			style_to_write += ' </style>';
			newWin= window.open("");
			newWin.document.write(style_to_write);
			newWin.document.write(elToPrint.outerHTML);
			newWin.print();
			newWin.close();
		  },

        export_excel: function (table, file_name){
            var max_character_length = [];
				var cells = table.cells();
            var nodes = cells.nodes();
            var num_rows = table.rows()[0].length;
            var num_columns = table.columns()[0].length;
            var rendered_data = cells.render('display');
				
				// get columns that need to be ignored (buttons, pictures etc.)
				var k = 0;
            var columns_to_ignore = [];
            for(var i = 0; i < num_columns; i++){
                var td = nodes[i];
                var class_list = td.classList;
                
                for(var j = 0; j < class_list.length; j++){
                    if(class_list[j] == "no-export"){
                        columns_to_ignore[k] = i;
                        k++;
                    }
                }
            }
				var k = 0;
            for(var i = 0; i < table.columns()[0].length; i++){
					if(i == columns_to_ignore[k]){
						k++;
					}
					else{
						max_character_length[i] = String(table.column(i).header().innerHTML).length;
					}
            }
				
    //max_character_length now contains the number of characters each column header has
    //now we check the data to see how many characters are in the actual data results
            
            k = 0;
            for(var i = 0; i < num_columns; i++){
					 if(i == columns_to_ignore[k]){
                        k++;
                  }
						else{
                for(var j = 0; j < num_rows; j++){
						
							if(String(rendered_data[(j * num_columns) + i]).length > max_character_length[i]){
								max_character_length[i] = String(rendered_data[(j * num_columns) + i]).length;
							}
						}
                
                }
            }
    //Now max_character_length contains the max character length of the header or data, whichever is largest, to use to set width for excel columns
            var workbook = new $.ig.excel.Workbook($.ig.excel.WorkbookFormat.excel2007);
            var sheet = workbook.worksheets().add('Sheet1');
           
            
            // Set Width of Columns
            for(var i = 0; i < table.columns()[0].length; i++){
                sheet.columns(i).setWidth((max_character_length[i] + 6), $.ig.excel.WorksheetColumnWidthUnit.character);
            }

            
            // Populate Headers
            var k =0;
				var char_int = 65;
				var outer_char_int = 64;
				var over_26 = false;
            for(var i = 65; i < (num_columns + 65); i++){
                if((i - 65) == columns_to_ignore[k]){
                    k++;
                }
                else{ 
						var chr = "";
						if(over_26){
							chr += String.fromCharCode(outer_char_int)
						}
                    chr += String.fromCharCode(char_int);
						  char_int++;
						  if(char_int == 91){
							  char_int = char_int - 26;
							  outer_char_int++;
							  over_26 = true;
						  }
						
                    var cell_address = chr + String(1);
                    sheet.getCell(cell_address).value(String(table.column(i - 65).header().innerHTML));
                }
            }

           // Populate Cells
			  
            for(var i = 0; i < num_rows; i++){ 
					 var k =0;
					 var ignored_columns = 0;
                for(var j = 0; j < num_columns; j++){
                    if(j == columns_to_ignore[k]){
                        k++;
								ignored_columns++;
                    }
                    else{
                        sheet.rows(i + 1).setCellValue((j - ignored_columns), rendered_data[(i * num_columns) + j]);
                    }
                }
            }


            // Save the workbook with date time and name of report
            var report_name = file_name
            var date_time = new Date()
            var date_string = String(date_time);
            date_string = date_string.substring(4, 24);
            var save_name = date_string + report_name;
            NYSUS.COMMON.REPORT.saveWorkbook(workbook, save_name);
        },
        saveWorkbook: function (workbook, name) {
            workbook.save({ type: 'blob' }, function (data) {
                saveAs(data, name);
            }, function (error) {
                alert('Error exporting: : ' + error);
            });
        }

    };

    return self;

})($, G);