$(document).ready(
		function() {
			var processor = './ajax_processors/lib/repetitive/lots.php';

			if (typeof NYSUS == 'undefined') {
				NYSUS = {};
			}
			if (null == NYSUS.REPETITIVE) {
				NYSUS.REPETITIVE = [];
			}

			NYSUS.REPETITIVE.LOT = (function() {
				// private vars
				var debug = false;

				// private methods
				function get_lot_material_types(field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_lot_material_types",
							"field_names" : field_names
						}
					}).done(cb);
				}

				function get_lot_machines(lot_id, field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_lot_machines",
							"lot_id" : lot_id,
							"field_names" : field_names
						}
					}).done(cb);
				}

				function get_lot_tool_parts(lot_id, field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_lot_tool_parts",
							"lot_id" : lot_id,
							"field_names" : field_names
						}
					}).done(cb);
				}

				function update_lot_material_types(lot_id, field_names,
						field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "update_lot_material_types",
							"lot_id" : lot_id,
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}	
				
				function insert_lot_material_type(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_lot_material_type",
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}	
				
				function insert_lot_machine(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_lot_machine",							
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}
				
				function delete_lot_machine(id, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "delete_lot_machine",							
							"ID" : id
						}
					}).done(cb);
				}
				
				function delete_lot_tool_part(lot_tool_part_id, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "delete_lot_tool_part",							
							"lot_tool_part_id" : lot_tool_part_id		
						}
					}).done(cb);
				}
				
				function get_lot_tool_parts(lot_id, machine_id, field_names, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_lot_tool_parts",							
							"lot_id" : lot_id,
							"machine_id" : machine_id,
							"field_names" : field_names							
						}
					}).done(cb);
				}
				
				function insert_lot_tool_part(field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "insert_lot_tool_part",							
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}
				
				function update_lot_tool_part(lot_tool_part_id, field_names, field_values, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "update_lot_tool_part",
							"lot_tool_part_id" : lot_tool_part_id,
							"field_names" : field_names,
							"field_values" : field_values
						}
					}).done(cb);
				}

				// return public methods
				return {
					get_lot_material_types : function(field_names, cb) {
						get_lot_material_types(field_names, cb);
					},
					insert_lot_material_type : function(field_names,
							field_values, cb) {
						insert_lot_material_type(field_names,
								field_values, cb);
					},
					update_lot_material_types : function(lot_id, field_names,
							field_values, cb) {
						update_lot_material_types(lot_id, field_names,
								field_values, cb);
					},
					get_lot_machines : function(lot_id, field_names, cb) {
						get_lot_machines(lot_id, field_names, cb);
					},					
					get_lot_tool_parts : function(lot_id, field_names, cb) {
						get_lot_tool_parts(lot_id, field_names, cb);
					},
					insert_lot_machine : function(field_names, field_values, cb) {
						insert_lot_machine(field_names, field_values, cb);
					},
					delete_lot_machine : function(id, cb) {
						delete_lot_machine(id, cb);
					},
					get_lot_tool_parts : function(lot_id, machine_id, field_names, cb) {
						get_lot_tool_parts(lot_id, machine_id, field_names, cb);						
					},
					insert_lot_tool_part : function(field_names, field_values, cb) {
						insert_lot_tool_part(field_names, field_values, cb);
					},
					delete_lot_tool_part : function(lot_id, field_names, field_values, cb) {
						delete_lot_tool_part(lot_id, field_names, field_values, cb);
					},
					update_lot_tool_part : function(lot_tool_part_id, field_names, field_values, cb) {
						update_lot_tool_part(lot_tool_part_id, field_names, field_values, cb);
					}

				} // end return

			})(); // end NYSUS.REPETITIVE.LOT
	
		});