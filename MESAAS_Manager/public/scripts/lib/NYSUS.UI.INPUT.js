var G = G || {};
var NYSUS = NYSUS || {};
NYSUS.UI = NYSUS.UI || {};

NYSUS.UI.INPUT = (function ($, G) {
    var that = this;
    
    var self = {

        init_date_input: function (selector) {
            // initialize the date input to the current date
            var today = new Date();
            var offset = today.getTimezoneOffset() * 60;
            var offset_date = new Date(today.getTime() - offset * 1000);
            var today_str = offset_date.toISOString().split('T')[0];
            $(selector).val(today_str);
        },

        get_date_value: function (selector) {
            var date_str = $(selector).val();
            return new Date(date_str).toISOString().split('T')[0];
        },

    };

    return self;

})($, G);