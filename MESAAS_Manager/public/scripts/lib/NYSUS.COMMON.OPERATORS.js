$(document).ready(
		function() {
			var processor = './ajax_processors/lib/common/operators.php';

			if (typeof NYSUS == 'undefined') {
				NYSUS = {};
			}
			if (null == NYSUS.COMMON) {
				NYSUS.COMMON = [];
			}

			NYSUS.COMMON.OPERATORS = (function() {
				// private vars
				var debug = false;

				// private methods
				function get_operators(cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_operators"
						}
					}).done(cb);
				}	
				
				function get_operator_by_badge(badge_id, cb) {
					$.ajax(processor, {
						method : "POST",
						data : {
							"action" : "get_operator_by_badge",
							"badge_id": badge_id
						}
					}).done(cb);
				}

				// return public methods
				return {
					
					get_operators : function(cb) {
						get_operators(cb);
					},
					get_operator_by_badge: function (badge_id, cb) {
						get_operator_by_badge(badge_id, cb);
					}
				
				} // end return

			})(); // end NYSUS.COMMON.OPERATORS
	
		});