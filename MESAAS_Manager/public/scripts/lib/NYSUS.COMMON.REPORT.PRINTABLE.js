var G = G || {};
var NYSUS = NYSUS || {};
NYSUS.COMMON = NYSUS.COMMON || {};
NYSUS.COMMON.REPORT = NYSUS.COMMON.REPORT || {};
 
NYSUS.COMMON.REPORT.PRINTABLE = (function ($, G) {
	var that = this;

	var self = {

		sections: [
	    	//{el_id: 'assembly_overview', title: 'Assembly Overview', loader: self.load_assembly, callback: self.show_assembly},
	    	//{el_id: 'component_lots', title: 'Component Lots', loader: self.load_component_lots, callback: self.show_component_lots}
	    ],

		init: function (element_selector, title, processor, key) {
			self.key = key;
			self.processor = processor;
			self.main_div = $(element_selector);

		    self.set_title(title);
		    self.build_sections();
		    self.load_data();
		},

		set_title: function (title) {
			$('<div></div>').addClass('main-header').html(title).appendTo(self.main_div);
		},

		build_sections: function () {
			self.sections.forEach(function (section) {
				var section_el = $('<div></div>').prop({id: section.el_id}).addClass('section').hide().appendTo(self.main_div);
				$('<div class="section-header">'+section.title+':</div>').appendTo(section_el);
				$('<div class="section-content"></div>').appendTo(section_el);
			});
		},

		load_data: function () {
			self.sections.forEach(function (section) {
				section.loader(section.callback);
				//console.log(section);
			});
		},

		build_table: function (element, columns, data) {
			var table = $('<table></table>').prop({id: element.id+'_table'}).addClass('section-table full-width').appendTo(element);
			var thead = $('<thead></thead>').appendTo(table);
			var tbody = $('<tbody></tbody>').appendTo(table);

			var htr = $('<tr></tr>').appendTo(thead);
			columns.forEach(function (column) {
				$('<th></th>').html(column.title).appendTo(htr);
			});

			if (data) {
			    data.forEach(function (item) {
			    	var tr = $('<tr></tr>').appendTo(tbody);
			    	columns.forEach(function (column) {
			    		if (column.render) {
			    			var value = column.render(item, item[column.name]);
			    		} else {
			    			var value = item[column.name];
			    		}
			    		$('<td></td>').addClass('cell').html(value).appendTo(tr);
			    	});
			    });
			}
		},

		render_datetime: function (row, value) {
			return value ? value.date : '';
		},

		render_date: function (row, value) {
			return value ? value.date.split(' ')[0] : '';
		},

		render_duration: function (row, value) {
			var result;

			if (value || value === 0) {
				result = utility.seconds_to_hms(value);
			} else {
				result = '';
			}

			return result;
		},

		render_percent: function (row, value) {
			var result;

			if (value === 0) {
				result = '0%'
			} else if (value) {
				var v = value * 100;
				result = v.toFixed(1) + '%';
			} else {
				result = '';
			}

			return result;
		}

	};

	return self;

})($, G);