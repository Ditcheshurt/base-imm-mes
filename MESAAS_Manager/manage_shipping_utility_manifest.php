<?php
	require ('ajax_processors/init.php');
	header("Content-Type: text/html");

	$lang = $GLOBALS['APP_CONFIG']['language'];
	$GLOBALS['STRINGS'] = json_decode(file_get_contents("config/strings_".$lang.".json"), true);
	require ('load_template_engine.php');

	//get shipment ID
	$ship_to_code = $_REQUEST['ship_to_code'];
	$sql = "SELECT TOP 1 ID
			  FROM shipments
			  WHERE ship_to_code = '".$ship_to_code."'
			  ORDER BY ID DESC";
	$res = $db->query($sql);
	if ($res) {
		if (count($res) > 0) {
			$shipment_ID = $res[0]['ID'];
		}
	}

	$smarty->assign('shipment_ID', $shipment_ID);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'CUSTOMER_NAME'";
	$res = $db->query($sql);
	$smarty->assign('customer_name', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'FROM_ADDR_1'";
	$res = $db->query($sql);
	$smarty->assign('from_addr_1', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'FROM_ADDR_2'";
	$res = $db->query($sql);
	$smarty->assign('from_addr_2', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'SHIPTO_NAME'";
	$res = $db->query($sql);
	$smarty->assign('shipto_name', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'SHIPTO_ADDR_1'";
	$res = $db->query($sql);
	$smarty->assign('shipto_addr_1', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'SHIPTO_ADDR_2'";
	$res = $db->query($sql);
	$smarty->assign('shipto_addr_2', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'CARRIER_CODE'";
	$res = $db->query($sql);
	$smarty->assign('carrier_code', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'SHIPPED_VIA'";
	$res = $db->query($sql);
	$smarty->assign('shipped_via', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'DOCK_CODE'";
	$res = $db->query($sql);
	$smarty->assign('dock_code', $res[0]['option_value']);

	$d = getdate();
	$smarty->assign('date', $d['month']." ".$d['mday'].", ".$d['year']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'BOL_NUMBER'";
	$res = $db->query($sql);
	$smarty->assign('bol_number', $res[0]['option_value']);

	$sql = "SELECT option_value FROM _system_options WHERE option_name = 'PRO_NUMBER'";
	$res = $db->query($sql);
	$smarty->assign('pro_number', $res[0]['option_value']);

	$smarty->assign('fob', 'Shipping Point');

	$sql = "SELECT COUNT(*) as num_containers FROM shipment_lanes WHERE shipment_ID = ".$shipment_ID;
	$res = $db->query($sql);
	$smarty->assign('num_containers', $res[0]['num_containers']);

	$sql = "SELECT truck_number FROM shipments WHERE ID = ".$shipment_ID;
	$res = $db->query($sql);
	$smarty->assign('trailer_number', $res[0]['truck_number']);

	$sql = "SELECT l.module_code, l.line_code, l.weight_per_rack, l.weight_per_FEM, COUNT(DISTINCT m.ID) as num_modules, COUNT(DISTINCT sl.ID) as num_containers, MIN(m.sequence) as starting_seq, MAX(m.sequence) as ending_seq
			  FROM modules m
			  	  JOIN lines l ON m.line_ID = l.ID
			  	  JOIN shipment_items i ON m.ID = i.mod_ID
			  	  JOIN shipment_lanes sl ON i.lane_ID = sl.ID
			  WHERE sl.shipment_ID = ".$shipment_ID."
			  GROUP BY l.module_code, l.line_code, l.weight_per_rack, l.weight_per_FEM
			  ORDER BY l.module_code";
	$res = $db->query($sql);
	$smarty->assign('module_container_summary', $res);

	$smarty->display ( './templates/manage/' . $_REQUEST ['type'] . '.html' );

?>