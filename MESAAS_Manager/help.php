<?php
require ('load_config.php');
require ('load_template_engine.php');

if (! isset ( $_SESSION ['user_ID'] ) && false) {
    $smarty->display ( './templates/bad_session.html' );
} else {
    $smarty->display ( './templates/help/' . $_REQUEST ['type'] . '.html' );
}

?>