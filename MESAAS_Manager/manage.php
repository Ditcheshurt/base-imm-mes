<?php
require ('load_config.php');
require ('load_template_engine.php');

if (! isset($_SESSION['op_ID'])) {
	$smarty->display ( './templates/bad_session.html' );
	//die(json_encode($_SESSION));
} else {	
	$smarty->display ( './templates/manage/' . $_REQUEST ['type'] . '.html' );
}

?>