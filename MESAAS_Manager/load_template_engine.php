<?php

	//LOAD TEMPLATING ENGINE

	define('SMARTY_CLASS', $GLOBALS['APP_CONFIG']['smarty_class']);
	require(dirname(__DIR__).SMARTY_CLASS);
	$smarty = new Smarty();
	$smarty->setTemplateDir('./templates')
				->setCompileDir('./templates/templates_c')
				->setCacheDir('./templates/cache');
	
	$smarty->assign('globals', $GLOBALS['APP_CONFIG']);  //assign all variables
	$smarty->assign('strings', $GLOBALS['STRINGS']);
	$smarty->assign('request', $_REQUEST);
?>