<?php

	ini_set("display_errors", true);
	error_reporting(E_ALL);
	
	//load in app config
	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("./config/app_config.json"), true);

	//get strings, based on language in app config
	$lang = $GLOBALS['APP_CONFIG']['language'];
	$GLOBALS['STRINGS'] = json_decode(file_get_contents("./config/strings_".$lang.".json"), true);
	
	session_save_path(realpath(dirname(__FILE__)).$GLOBALS['APP_CONFIG']['session_path']);
	session_start();

?>