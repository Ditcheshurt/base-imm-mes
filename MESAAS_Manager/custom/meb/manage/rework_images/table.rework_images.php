<?php
require("../../../../ajax_processors/init.php");

$app_config = $GLOBALS['APP_CONFIG'];

// DataTables PHP library and database connection
include("../../../../lib/DataTables/DataTables.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
$editor = Editor::inst($db, 'machine_defect_reasons', 'ID')
	->fields(
		Field::inst('machine_defect_reasons.ID')
			->validator('Validate::numeric'),
		Field::inst('p.defect_description'),
		Field::inst('machine_defect_reasons.defect_description'),
		Field::inst('machine_defect_reasons.rework_instr_url')
			->upload(
				Upload::inst( function ( $file, $id ) {
					global $app_config;
					$rand = mt_rand();
					move_uploaded_file( $file['tmp_name'], $app_config["rework_image_upload_path"].$rand.'~~'.$file['name'] );
					return $app_config["rework_image_upload_url"].$rand.'~~'.$file['name'];
				} )
					->allowedExtensions( array( 'pdf', 'png', 'jpg', 'gif' ), "Please upload an image file" ))
				->setFormatter( 'Format::nullEmpty' ),
		Field::inst('machine_defect_reasons.approval_instr_url')
			->upload(
				Upload::inst( function ( $file, $id ) {
					global $app_config;
					$rand = mt_rand();
					move_uploaded_file( $file['tmp_name'], $app_config["rework_image_upload_path"].$rand.'~~'.$file['name'] );
					return $app_config["rework_image_upload_url"].$rand.'~~'.$file['name'];
				} )
					->allowedExtensions( array( 'pdf', 'png', 'jpg', 'gif' ), "Please upload an image file" ))
			->setFormatter( 'Format::nullEmpty' )
	)
	->leftJoin('machine_defect_reasons p', 'p.ID', '=', 'machine_defect_reasons.parent_ID');

$editor->where( function ( $q ) {
    $q->where( 'machine_defect_reasons.plc_defect_code', 'NULL', 'IS NOT', false );
} );

$editor->process($_POST)->json();