(function ($) {
	$(document).ready(function () {
		var processor = 'custom/meb/manage/rework_images/table.rework_images.php';
		var editor = new $.fn.dataTable.Editor({
			colReorder: true,
			ajax: processor,
			table: '#rework_images',
			fields: [
				{
					"label": "Defect Desc",
					"name": "machine_defect_reasons.defect_description",
					"type": "readonly"
				},
				{
					"label": "Parent Defect",
					"name": "p.defect_description",
					"type": "readonly"
				},
				{
					"label": "Rework Instruction",
					"name": "machine_defect_reasons.rework_instr_url",
					"type": "upload",
					"display": function (data) {
						if (data && data.endsWith('.pdf')) {
							//return `<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../${data}"></iframe>`;
							return '<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + data + '"></iframe>';
						}
						return '<img src="' + data + '" width="100px" height="100px" />';
					},
					"clearText": "Clear",
					"noImageText": 'No image'
				},
				{
					"label": "Rework Approval",
					"name": "machine_defect_reasons.approval_instr_url",
					"type": "upload",
					"display": function (data) {
						if (data && data.endsWith('.pdf')) {
							//return `<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../${data}"></iframe>`;
							return '<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + data + '"></iframe>';
						}
						return '<img src="' + data + '" width="100px" height="100px" />';
					},
					"clearText": "Clear",
					"noImageText": 'No image'
				}
			]
		});

		var table = $('#rework_images').DataTable({
			colReorder: true,
			ajax: processor,
			fixedHeader: true,
			select: "single",
			lengthChange: false,
			order: [[ 1, "desc" ]],
			columns: [
				{
					"data": null,
					"defaultContent": "",
					"className": "select-checkbox",
					"orderable": false
				},
				{
					"data": "machine_defect_reasons.defect_description"
				},
				{
					"data": "p.defect_description",
					render: function (data) {
						return data ? data : 'No Parent';
					}
				},
				{
					"data": "machine_defect_reasons.rework_instr_url",
					type: "upload",
					render: function (data) {
						if (!data) {
							return 'NO IMAGE...';
						}
						if (data && data.endsWith('.pdf')) {
							//return `<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../${data}"></iframe>`;
							//return '<iframe style="width:50%; height:50%" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + data + '"></iframe>';
							return '<a target="blank" href="../../../../' + data + '">Show PDF File</a>';
						}
						return '<img src="' + data + '" width="50px" height="50px" alt="NO IMAGE..." />';
					}
				},
				{
					"data": "machine_defect_reasons.approval_instr_url",
					type: "upload",
					render: function (data) {
						if (!data) {
							return 'NO IMAGE...';
						}
						if (data && data.endsWith('.pdf')) {
							//return `<iframe style="width:100%; height:100%" src="../common/library/pdfjs/web/viewer.html?file=../../../../${data}"></iframe>`;
							//return '<iframe style="width:50%; height:50%" src="../common/library/pdfjs/web/viewer.html?file=../../../../' + data + '"></iframe>';
							return '<a target="blank" href="../../../../' + data + '">Show PDF File</a>';
						}
						return '<img src="' + data + '" width="50px" height="50px" alt="NO IMAGE..." />';
					}
				}
			]
		});

		new $.fn.dataTable.Buttons(table, [
			{
				text: 'Reload',
				action: function (e, dt, node, config) {
					dt.ajax.reload();
				}
			},
			{extend: "create", editor: editor},
			{extend: "edit", editor: editor},
			{extend: "remove", editor: editor}
		]);

		table.buttons().container()
			.appendTo($('.col-sm-6:eq(0)', table.table().container()));

	});

}(jQuery));