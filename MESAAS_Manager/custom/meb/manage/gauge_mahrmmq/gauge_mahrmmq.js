$(document).ready(function () {
	var file_upload_path = "c:\\mmqupload\\";
	var file_upload_prefix;
	var files_uploaded = [];
	var passed;
	var machine_cycle_part_ID;

	$('.btn-passed').prop('disabled', true);
	$('.btn-failed').prop('disabled', true);

	$('#inp_serial_number').on('change', function (e) {
		e.preventDefault();

		var serial_number = $(this).val().substr(18).substr(0, 13);
		var field_names = ['ID', 'serial_number', 'insert_time'];
		var view = $('#part_view');

		view.empty();
		$('#upload_detail').empty();

		NYSUS.REPETITIVE.MACHINES.get_machine_cycle_parts_by_serial(serial_number, field_names, function (jso) {
			files_uploaded = [];
			machine_cycle_part_ID = null;
			file_upload_prefix = null;

			if (jso) {

				file_upload_prefix = serial_number + '_';
				machine_cycle_part_ID = jso[0].ID;

				var uploader = $('#uploader').pluploadQueue({
					url: './ajax_processors/upload.php',
					filters : [
						{title : "Image files", extensions : "jpg,gif,png"}
					],
					rename: true,
					flash_swf_url : '../../js/Moxie.swf',
					silverlight_xap_url : '../../js/Moxie.xap',
					multi_selection: false,
					max_file_count: 1,
					multipart_params: {
						"file_prefix": file_upload_prefix,
						"file_path": file_upload_path
					},
					filters: {
						prevent_duplicates: true,
						max_file_size: '1mb',
						mime_types: [
							{title: "Text files", extensions: "txt"}
						]
					},
					init : {
						FileUploaded: function(up, file, info) {
							$('.btn-passed').prop('disabled', false);
							$('.btn-failed').prop('disabled', false);
							files_uploaded.push(file_upload_path + file_upload_prefix + file.name);
						},
						FilesAdded: function (up, files) {
							up.disableBrowse(false);
						}
					}
				});

			} else {
				view.append('<h3>No Serial Number Found</h3>');
				$('#uploader').empty();
			}

		});

		return false;

	});

	$('.btn-passed').on('click', function (e) {
		e.preventDefault();
		passed = 1;
		insert_record();
		return false;
	});

	$('.btn-failed').on('click', function (e) {
		e.preventDefault();
		passed = 0;
		insert_record();
		return false;
	});

	function insert_record() {

		if (validate_input()) {

			NYSUS.REPETITIVE.GAUGES.parse_mahrmmq(machine_cycle_part_ID, files_uploaded[0], passed, function (jso) {
				if (jso.success == 1) {
					$.growl(
						{
							"title": " SUCCESS: ",
							"message": "RECORD SAVED!!",
							"icon": "glyphicon glyphicon-exclamation-sign"
						},
						{
							"type": "success",
							"allow_dismiss": false,
							"placement": {"from": "top", "align": "right"},
							"offset": {"x": 10, "y": 80},
							delay: 10e3
						}
					);
					$('.btn-passed').prop('disabled', true);
					$('.btn-failed').prop('disabled', true);
					show_upload_detail();

				} else {
					var message = "ERROR SAVING RECORD!!<br />" + jso.error;
					$.growl(
						{"title": " ERROR: ", "message": message, "icon": "glyphicon glyphicon-exclamation-sign"},
						{
							"type": "danger",
							"allow_dismiss": false,
							"placement": {"from": "top", "align": "right"},
							"offset": {"x": 10, "y": 80},
							delay: 10e3
						}
					);
				}
			});

		}

	}

	function show_upload_detail() {
		var container = $('#upload_detail');
		var table = $('<table class="table table-condensed table-striped"><thead><tr><td>Name</td><td>Datum</td><td>Nominal</td><td>L Tol</td><td>U Tol</td><td>Value</td><td>Unit</td><td>Feature</td><td>Low Nat Border</td><td>Type</td><td>No</td><td>Date</td><td>Time</td><td>Operator</td></tr></thead></table>');
		var tbody = $('<tbody />');

		NYSUS.REPETITIVE.GAUGES.get_marhmmq_detail(machine_cycle_part_ID, function (jso) {
			if (jso) {
				$.each(jso, function (k, v) {
					var tr = $('<tr />');
					tr.append('<td>' + v.name + '</td>');
					tr.append('<td>' + v.datum + '</td>');
					tr.append('<td>' + v.nominal + '</td>');
					tr.append('<td>' + v.l_tol + '</td>');
					tr.append('<td>' + v.u_tol + '</td>');
					tr.append('<td>' + v.value + '</td>');
					tr.append('<td>&#' + v.unit + ';</td>');
					tr.append('<td>' + v.feature + '</td>');
					tr.append('<td>' + v.low_nat_border + '</td>');
					tr.append('<td>' + v.type + '</td>');
					tr.append('<td>' + v.no + '</td>');
					tr.append('<td>' + v.date + '</td>');
					tr.append('<td>' + v.time + '</td>');
					tr.append('<td>' + v.operator + '</td>');
					tbody.append(tr);
				});
				table.append(tbody);
				container.empty().append(table);
			} else {
				container.empty().append('<h2>THERE WAS AN ERROR UPLOADING TEST FILE! No Data Exists');
			}

		});

	}

	function validate_input() {
		var growl_style = {
			"type": "danger",
			"allow_dismiss": false,
			"placement": {"from": "top", "align": "right"},
			"offset": {"x": 10, "y": 80},
			delay: 10e3
		};

		if (machine_cycle_part_ID == undefined || machine_cycle_part_ID == null) {
			$.growl(
				{"title": " DANGER: ", "message": "NO PART SELECTED", "icon": "glyphicon glyphicon-exclamation-sign"},
				growl_style
			);
			return false;
		}
		if (files_uploaded.length <= 0) {
			$.growl(
				{"title": " DANGER: ", "message": "NO FILE SELECTED", "icon": "glyphicon glyphicon-exclamation-sign"},
				growl_style
			);
			return false;
		}

		return true;
	}

}); // end document ready