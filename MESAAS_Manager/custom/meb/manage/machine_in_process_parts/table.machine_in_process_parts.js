(function ($) {
	$(document).ready(function () {
		var processor = 'custom/meb/manage/machine_in_process_parts/table.machine_in_process_parts.php';
		var editor = new $.fn.dataTable.Editor({
			colReorder: true,
			ajax: processor,
			table: '#machine_in_process_parts',
			fields: [
				{
					"label": "Serial",
					"name": "machine_in_process_parts.serial"
				},
				{
					"label": "Created",
					"name": "machine_in_process_parts.created_time",
					//"type": "select",
					//"placeholder": "Select a Tool Part"
				},
				{
					"label": "Last Station",
					"name": "machine_in_process_parts.last_machine_started_ID"
				},
				{
					"label": "Last Station Order",
					"name": "machine_in_process_parts.last_machine_started_order"
				},
				{
					"label": "Started Time",
					"name": "machine_in_process_parts.last_machine_started_time",
					"type": "datetime",
					"def": function () { return new Date(); },
	                "format": "MM-DD-YYYY h:mm A",
	                "fieldInfo": "US style m-d-y date input with 12 hour clock"
				},
				{
					"label": "Completed Station",
					"name": "machine_in_process_parts.last_machine_completed_ID"
				},
				{
					"label": "Completed Station Order",
					"name": "machine_in_process_parts.last_machine_completed_order"
				},
				{
					"label": "Completed Time",
					"name": "machine_in_process_parts.last_machine_completed_time",
					"type": "datetime",
					"def": function () { return new Date(); },
	                "format": "MM-DD-YYYY h:mm A",
	                "fieldInfo": "US style m-d-y date input with 12 hour clock"
				}
			]
		});

		var table = $('#machine_in_process_parts').DataTable({
			colReorder: true,
			ajax: processor,
			fixedHeader: true,
			select: "single",
			lengthChange: false,
			order: [[ 1, "desc" ]],
			columns: [
				{
					"data": null,
					"defaultContent": "",
					"className": "select-checkbox",
					"orderable": false
				},
				{
					"data": "machine_in_process_parts.serial"
				},
				{
					"data": "machine_in_process_parts.created_time",
					//"render": function (data, type, row) {
					//	return '<span>' + row.tools.short_description + ' | ' + row.parts.part_desc + '</span>';
					//}
				},
				{
					"data": "machine_in_process_parts.last_machine_started_ID"
				},
				{
					"data": "machine_in_process_parts.last_machine_started_order"
				},				
				{
					"data": "machine_in_process_parts.last_machine_started_time"
				},
				{
					"data": "machine_in_process_parts.last_machine_completed_ID"
				},
				{
					"data": "machine_in_process_parts.last_machine_completed_order"
				},
				{
					"data": "machine_in_process_parts.last_machine_completed_time"
				}
			]
		});

		new $.fn.dataTable.Buttons(table, [
			{
				text: 'Reload',
				action: function (e, dt, node, config) {
					dt.ajax.reload();
				}
			},
			{extend: "create", editor: editor},
			{extend: "edit", editor: editor},
			{extend: "remove", editor: editor}
		]);

		table.buttons().container()
			.appendTo($('.col-sm-6:eq(0)', table.table().container()));

	});

}(jQuery));