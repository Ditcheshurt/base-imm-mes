var NYSUS = NYSUS || [];
NYSUS.GAUGE = NYSUS.GAUGE || [];
NYSUS.GAUGE.INSTRUCTION_EDITOR = {};
(function ($) {
	$(document).ready(function () {
		var processor = 'custom/meb/manage/gauge_material_types/table.gauge_material_types.php';
		var editor = new $.fn.dataTable.Editor({
			colReorder: true,
			ajax: processor,
			table: '#gauge_material_types',
			fields: [
				{
					"label": "Description",
					"name": "gauge_material_types.description"
				},
				{
					"label": "Model Part",
					"name": "gauge_material_types.tool_part_ID",
					"type": "select",
					"placeholder": "Select Model Part"
				},
				//{
				//	"label": "Part Count",
				//	"name": "gauge_material_types.gauge_parts_used"
				//},
				{
					"label": "Parts Per Check",
					"name": "gauge_material_types.parts_per_gauging"
				},
				{
					"label": "Warning Offset",
					"name": "gauge_material_types.parts_per_gauging_warning_offset"
				},
				{
					"label": "Maximum Offset",
					"name": "gauge_material_types.parts_per_gauging_maximum_offset"
				},
				{
					"label": "Station IP Address",
					"name": "gauge_material_types.station_ip_address"
				},
				{
					"label": "Skip Part Scan",
					"name": "gauge_material_types.skip_part_scan",
					"type": "checkbox",
					"default": 0,
					"separator": ",",
					"options": [
						{label: "", value: 1}
					]
				},
				{
					"label": "Active",
					"name": "gauge_material_types.active",
					"type": "checkbox",
					"default": 0,
					"separator": ",",
					"options": [
						{label: "", value: 1}
					]
				}
			]
		});

		var table = $('#gauge_material_types').DataTable({
			colReorder: true,
			ajax: processor,
			fixedHeader: true,
			select: "single",
			lengthChange: false,
			columns: [
				{
					"data": null,
					"defaultContent": "",
					"className": "select-checkbox",
					"orderable": false
				},
				{
					"data": "gauge_material_types.description"
				},
				{
					"data": "gauge_material_types.tool_part_ID",
					"render": function (data, type, row) {
						return '<span>' + row.tools.short_description + ' | ' + row.parts.part_desc + '</span>';
					}
				},
				{
					"data": "gauge_material_types.gauge_parts_used"
				},
				{
					"data": "gauge_material_types.parts_per_gauging"
				},
				{
					"data": "gauge_material_types.parts_per_gauging_warning_offset"
				},
				{
					"data": "gauge_material_types.parts_per_gauging_maximum_offset"
				},
				{
					"data": "gauge_material_types.station_ip_address"
				},
				{
					"data": "gauge_material_types.skip_part_scan",
					"className": "dt-body-center",
					"render": function (data, type, row) {
						var isActive = !(data == null || data == false);
						return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
					}
				},
				{
					"data": "gauge_material_types.active",
					"className": "dt-body-center",
					"render": function (data, type, row) {
						var isActive = !(data == null || data == false);
						return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
					}
				}
			]
		});

		new $.fn.dataTable.Buttons(table, [
			{
				text: 'Reload',
				action: function (e, dt, node, config) {
					dt.ajax.reload();
				}
			},
			{extend: "create", editor: editor},
			{extend: "edit", editor: editor},
			{
				extend: "edit",
				text: 'Edit Instructions',
				action: function (e, dt, node, config) {
					//var ids = $.map(table.rows('.selected').data(), function (item) {
					//	console.log( item[0] );
					//});
					var id = table.rows('.selected').data()[0].gauge_material_types.ID;
					NYSUS.GAUGE.INSTRUCTION_EDITOR.gauge_material_type_ID = id;
					NYSUS.GAUGE.INSTRUCTION_EDITOR.table.ajax.reload();
					//NYSUS.GAUGE.INSTRUCTION_EDITOR.table.ajax.url(NYSUS.GAUGE.INSTRUCTION_EDITOR.processor + '?gauge_material_type_ID='+id).load();

					$('#modal_instructions').modal('show');
				}
			},
			{extend: "remove", editor: editor}
		]);

		table.buttons().container()
			.appendTo($('.col-sm-6:eq(0)', table.table().container()));

	});

}(jQuery));