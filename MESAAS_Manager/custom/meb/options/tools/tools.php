<?php

	require_once("../../../../ajax_processors/init.php");

	$action = $_REQUEST['action'];	

	if ($action == 'get_tools') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT
					t.ID,
					t.tool_description,
					t.short_description,
					t.active,
					t.parent_tool_ID,
					t2.tool_description AS parent_tool_description,
					t.current_part_ID
				FROM
					tools t
					LEFT JOIN tools t2 ON t2.ID = t.parent_tool_ID
				ORDER BY
					t.tool_description ASC;";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_tool') {
	    global $db;
	    $res = new StdClass();

	    $tool_id = $_REQUEST['tool_ID'];

	    $sql = "SELECT TOP 1
					t.ID,
					t.tool_description,
					t.short_description,
					t.active,
					t.parent_tool_ID,
					t2.tool_description AS parent_tool_description,
					t.current_part_ID,
					p.part_desc AS current_part_description,
					t.ojt_group_ID
				FROM
					tools t
					LEFT JOIN tools t2 ON t2.ID = t.parent_tool_ID
					LEFT JOIN tool_parts tp ON tp.ID = t.current_part_ID
					LEFT JOIN parts p ON p.ID = tp.part_ID
				WHERE
					t.ID = $tool_id";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'insert_tool') {
	    global $db;
	    $res = new StdClass();

	    if ($_REQUEST['tool_data']['parent_tool_ID'] > -1) {
	    	$parent_tool_ID = $_REQUEST['tool_data']['parent_tool_ID'];
	    } else {
	    	$parent_tool_ID = 'NULL';
	    }
	    
	    $sql = "INSERT
	    		INTO
					tools (
						tool_description,
						short_description,
						parent_tool_ID,
						active
					)
				VALUES (
					'".$_REQUEST['tool_data']['tool_description']."',
					'".$_REQUEST['tool_data']['short_description']."',
					".$parent_tool_ID.",
					".$_REQUEST['tool_data']['active']."
				);";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'update_tool') {
	    global $db;
	    $res = new StdClass();

	    if ($_REQUEST['tool_data']['parent_tool_ID'] > -1) {
	    	$parent_tool_ID = $_REQUEST['tool_data']['parent_tool_ID'];
	    } else {
	    	$parent_tool_ID = 'NULL';
	    }
	    
	    $sql = "UPDATE
					tools
				SET
					tool_description = '".$_REQUEST['tool_data']['tool_description']."',
					short_description = '".$_REQUEST['tool_data']['short_description']."',
					parent_tool_ID = ".$parent_tool_ID.",
					active = ".$_REQUEST['tool_data']['active']."
				WHERE
					ID = ".$_REQUEST['tool_ID'].";";
	   	//die($sql);
	   	$res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_tool_parts') {
	    global $db;
	    $res = new StdClass();

	    $tool_id = $_REQUEST['tool_ID'];

	    $sql = "SELECT
					tp.ID,
					tool_ID,
					p.ID AS part_ID,
					part_desc,
					part_number,
					mrp_part_number,
					part_image,
					parts_per_tool,
					operation,
					scrap_cost,
					sub_tool_order,
					serialized,
					zoned,
					tp.active,
					tp.last_update,
					standard_pack_qty,
					part_weight,
					box_weight
				FROM
					tool_parts tp
					JOIN parts p ON p.ID = tp.part_ID
				WHERE
					tool_ID = $tool_id
				ORDER BY
					operation ASC,
					sub_tool_order ASC;";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_tool_part') {
	    global $db;
	    $res = new StdClass();
	    $tool_part_ID = $_REQUEST['tool_part_ID'];
	    
	    $sql = "SELECT TOP 1
					tp.ID,
					tp.tool_ID,
					p.ID AS part_ID,
					p.part_desc,
					p.part_number,
					p.mrp_part_number,
					p.part_image,
					tp.parts_per_tool,
					tp.operation,
					tp.scrap_cost,
					tp.sub_tool_order,
					tp.serialized,
					tp.zoned,
					tp.active,
					tp.last_update,
					p.standard_pack_qty,
					p.part_weight,
					p.box_weight
				FROM
					tool_parts tp
				JOIN 
					parts p ON p.ID = tp.part_ID
				WHERE
					tp.ID = $tool_part_ID";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'insert_tool_part') {
	    global $db;
	    $res = new StdClass();

	    $data = $_REQUEST['tool_part_data'];

	    $optional = array(
	    	'mrp_part_number' => null,
	    	'part_image' => null,
	    	'scrap_cost' => null,
	    	'standard_pack_qty' => null,
	    	'part_weight' => null,
	    	'box_weight' => null
	    );

	    foreach($optional as $param => &$value) {
		    if (!isset($data[$param]) || empty($data[$param])) {
		    	$value = 'NULL';
		    } else if (!is_numeric($data[$param])) {
		    	$value = "'".$data[$param]."'";	
	    	} else {
	    		$value = $data[$param];
	    	}
	    }

	    $sql = "INSERT INTO
					tool_parts (
						tool_ID,
						part_ID,						
						parts_per_tool,
						operation,
						scrap_cost,
						sub_tool_order,
						serialized,
						zoned,
						active,
						last_update
					)
				OUTPUT INSERTED.ID
				VALUES (
					".$data['tool_ID'].",
					".$data['part_ID'].",					
					".$data['parts_per_tool'].",
					".$data['operation'].",
					".$optional['scrap_cost'].",
					".$data['sub_tool_order'].",
					".$data['serialized'].",
					".$data['zoned'].",
					".$data['active'].",
					GETDATE()					
				);";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'update_tool_part') {
	    global $db;
	    $res = new StdClass();

	    $data = $_REQUEST['tool_part_data'];

	    $optional = array(
	    	'scrap_cost' => null	    	
	    );

	    foreach($optional as $param => &$value) {
		    if (!isset($data[$param]) || empty($data[$param])) {
		    	$value = 'NULL';
		    } else if (!is_numeric($data[$param])) {
		    	$value = "'".$data[$param]."'";	
	    	} else {
	    		$value = $data[$param];
	    	}
	    }
/*
	    if (isset($data['part_image']) && $data['part_image']) {
	    	$part_image_str = "part_image = ".$data['part_image'].",";
	    } else {
	    	$part_image_str = '';
	    }
	    */
	    $sql = "UPDATE
					tool_parts
				SET
					part_ID = ".$data['part_ID'].",
					parts_per_tool = ".$data['parts_per_tool'].",
					operation = ".$data['operation'].",
					scrap_cost = ".$optional['scrap_cost'].",
					sub_tool_order = ".$data['sub_tool_order'].",
					serialized = ".$data['serialized'].",
					zoned = ".$data['zoned'].",
					active = ".$data['active'].",
					last_update = GETDATE()					
				WHERE
					ID = ".$data['tool_part_ID'];				

	//die($sql);
	   	$res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'delete_tool_part') {
	    global $db;
	    $res = new StdClass();

	    try {
			//$dir = 'samples' . DIRECTORY_SEPARATOR . 'sampledirtree';
			$dir = $_SERVER['DOCUMENT_ROOT'].'\\machine_mes2\\public\\images\\part_images\\part_'.$_REQUEST['tool_part_ID'];
			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

			foreach($files as $file) {
			    if ($file->isDir()){
			        rmdir($file->getRealPath());
			    } else {
			        unlink($file->getRealPath());
			    }
			}

			$res = rmdir($dir);
	    } catch (Exception $e) {
	    	$res = true;
	    }

		if ($res) {
		    $sql = "DELETE
		    		FROM
		    			tool_parts
					WHERE
						ID = ".$_REQUEST['tool_part_ID'].";";
		   	//die($sql);
		   	$res = $db->query($sql);
		}

	    echo json_encode($res);
	}

	if ($action == 'get_parent_defect_reasons') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT
					ID,
					parent_ID,
					defect_description,
					active
				FROM
					machine_defect_reasons
				WHERE
					parent_ID = 0
					--AND active = 1
				ORDER BY
					defect_description ASC;";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_machine_defect_reasons') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT
					ID,
					parent_ID,
					defect_description,
					active
				FROM
					machine_defect_reasons
				WHERE
					parent_ID = ".$_REQUEST['parent_ID']."
					AND active = 1
					AND ID NOT IN (
						SELECT defect_reason_ID FROM tool_defect_reasons WHERE tool_ID = ".$_REQUEST['tool_ID']."
					)
				ORDER BY
					defect_description ASC;";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_tool_defect_reasons') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT
					tdr.ID,
					tdr.defect_reason_ID,
					mdr.defect_description,
					mdr.parent_ID,
					mdr2.defect_description AS parent_defect_description,
					mdr.active
				FROM
					tool_defect_reasons tdr
					LEFT JOIN machine_defect_reasons mdr ON mdr.ID = tdr.defect_reason_ID
					LEFT JOIN machine_defect_reasons mdr2 ON mdr2.ID = mdr.parent_ID
				WHERE
					tdr.tool_ID = ".$_REQUEST['tool_ID']."
				ORDER BY
					parent_defect_description ASC,
					mdr.defect_description ASC;";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'insert_tool_defect_reason') {
	    global $db;
	    $res = new StdClass();

	    $data = $_REQUEST['tool_defect_reason_data'];
	    
	    $sql = "INSERT INTO
					tool_defect_reasons (
						tool_ID,
						defect_reason_ID
					)
				VALUES (
					".$data['tool_ID'].",
					".$data['defect_reason_ID']."
				);";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'delete_tool_defect_reason') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "DELETE FROM tool_defect_reasons WHERE ID = ".$_REQUEST['tool_defect_reason_ID'].";";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}


	if ($action == 'store_tool_part_image') {
	    global $db;
	    $file = $_FILES['file'];
	    $tmp_name = $file['tmp_name'];
	    $name = $file['name'];
	    $extension = pathinfo($name, PATHINFO_EXTENSION);

	    $directory = $_SERVER['DOCUMENT_ROOT'].'\\machine_mes2\\public\\images\\part_images\\part_'.$_REQUEST['part_ID'];

	    //$image_dir = 'part_'.$_REQUEST['part_ID'];
	    if (!file_exists($directory)) {
    		mkdir($directory, 0777, true);
		}

	    $path = '/machine_mes2/public/images/part_images/part_'.$_REQUEST['part_ID'] . '/' . $name;
	    $real_path = $directory . '\\' . $name;

	    if ($file['size'] > 0) {
		    if ($file['size'] < 10000000000 && is_uploaded_file($tmp_name) && move_uploaded_file($tmp_name, $real_path)) {
		        
		        $sql = "UPDATE
		                    tool_parts
		                SET
		                    part_image = '".$path."'
		                WHERE
		                    ID = ".$_REQUEST['part_ID'];

		        $res = $db->query($sql);

		        if ($res === null) {
		            $res = array('location' => $path);
		        } else {
		            $res = false;
		        }
		        
		    } else {
		    	$res = false;
		    }
	    } else {
	    	$res = array('location' => null);
	    }

	    echo json_encode($res);
	}

	if ($action == 'get_parts') {
	    global $db;
		$res = new StdClass();

	    $sql = "SELECT
	    			[ID]
			      ,[part_desc]
			      ,[part_number]
			      ,[mrp_part_number]			      
	    		FROM
	    			parts
	    		WHERE
	    			active = 1";
	    
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'set_tool_ojt_group') {
	    global $db;

	    $ojt_group_ID = $_REQUEST['ojt_group_ID'];
	    if (!$ojt_group_ID) {
	    	$ojt_group_ID = 'NULL';
	    }

	    $sql = "UPDATE
	    			tools
	    		SET
	    			ojt_group_ID = {$ojt_group_ID}
				WHERE
					ID = {$_REQUEST['tool_ID']}";
	    echo json_encode($db->query($sql));
	}
?>