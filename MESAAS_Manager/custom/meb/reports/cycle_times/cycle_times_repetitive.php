<?php

function get_cycles_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	$where = " WHERE CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) BETWEEN '$start_date' AND '$end_date'";

	if (isset($_REQUEST["system_ID"]) && $_REQUEST["system_ID"] != null) {
		$where .= " AND m.system_ID = " . $_REQUEST["system_ID"];
	}
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND t.ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND MES_COMMON.dbo.fn_getShift(m.system_ID, mc.cycle_time) IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "SELECT
				m.machine_number
			  , m.machine_name
			  ,t.short_description
		      ,CONVERT(VARCHAR(13), mc.cycle_time, 101) + ' ' + CONVERT(VARCHAR(13), mc.cycle_time, 108) AS 'cycle_time' --mc.cycle_time
		      ,mc.cycle_duration
			  ,CONVERT(VARCHAR, o.badge_ID) + ' ' + o.name AS operator
		  FROM machine_cycles mc
		  LEFT JOIN tools t ON t.ID = mc.tool_ID
		  LEFT JOIN machines m ON m.iD = mc.machine_ID
		  LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
		  $where AND mc.cycle_duration < 600";
//die($sql);
	$result = $db->query($sql);

	echo json_encode($result);
}

function get_cycles_scatter_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	$where = " WHERE CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) BETWEEN '$start_date' AND '$end_date'";

	if (isset($_REQUEST["system_ID"]) && $_REQUEST["system_ID"] != null) {
		$where .= " AND m.system_ID = " . $_REQUEST["system_ID"];
	}
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND t.ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND MES_COMMON.dbo.fn_getShift(m.system_ID, mc.cycle_time) IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "SELECT
				CONVERT(varchar(13), mc.cycle_time, 101) AS category
				,mc.cycle_duration AS [value]
				,t.short_description AS [name]
			FROM machine_cycles mc
			LEFT JOIN tools t ON t.ID = mc.tool_ID
			LEFT JOIN machines m ON m.iD = mc.machine_ID
			LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
			$where AND mc.cycle_duration < 600";

	$result = format_to_highchart_scatter($db->query($sql));
	echo json_encode($result);
}

function get_cycles_bubble_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	$where = " WHERE CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) BETWEEN '$start_date' AND '$end_date'";

	if (isset($_REQUEST["system_ID"]) && $_REQUEST["system_ID"] != null) {
		$where .= " AND m.system_ID = " . $_REQUEST["system_ID"];
	}
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND t.ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND MES_COMMON.dbo.fn_getShift(m.system_ID, mc.cycle_time) IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "SELECT
				CONVERT(varchar(13), mc.cycle_time, 101) AS category
				,AVG(mc.cycle_duration) AS [value]
				,t.short_description AS [name]
				,COUNT(mc.ID) AS quantity
			FROM machine_cycles mc
			LEFT JOIN tools t ON t.ID = mc.tool_ID
			LEFT JOIN machines m ON m.iD = mc.machine_ID
			LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
			$where AND mc.cycle_duration < 600
			GROUP BY CONVERT(varchar(13), mc.cycle_time, 101), t.short_description";

	$result = format_to_highchart_bubble($db->query($sql));
	echo json_encode($result);
}