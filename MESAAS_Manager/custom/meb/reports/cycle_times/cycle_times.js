var processor = './custom/meb/reports/cycle_times/cycle_times.php';
var chart1, chart2, dtTable1, table1, data;
var column_titles = [
	{name: 'machine_number', value: 'Number', display: true},
	{name: 'machine_name', value: 'Machine Name', display: true},
	{name: 'short_description', value: 'Tool', display: true},
	{name: 'cycle_time', value: 'Cycle Time', display: true},
	{name: 'cycle_duration', value: 'Duration', display: true},
	{name: 'operator', value: 'Operator', display: true},
	{name: 'result', value: 'Result', display: true}
];

var scatter_plot_options = {
	chart: {
		type: 'scatter',
		zoomType: 'xy',
		renderTo: 'chart1',
		events: {
			load: function () {
				$(document).trigger('complete');
			}
		}
	},
	title: {
		text: 'Cycle Times'
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: 'Time (sec)'
		},
		startOnTick: true,
		endOnTick: true,
		showLastLabel: true
	},
	yAxis: {
		title: {
			text: 'Date'
		}
	},
	//legend: {
	//	layout: 'vertical',
	//	align: 'left',
	//	verticalAlign: 'top',
	//	x: 100,
	//	y: 70,
	//	floating: true,
	//	backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF',
	//	borderWidth: 1
	//},
	tooltip: {
		useHTML: true,
		headerFormat: '<table>',
		pointFormat: '<tr><th colspan="2"><h3>{series.name}</h3></th></tr>' +
		'<tr><th>Time (sec):</th><td>{point.x:.0f}</td></tr>',
		footerFormat: '</table>',
		followPointer: true
	},
	plotOptions: {
		scatter: {
			marker: {
				radius: 3,
				states: {
					hover: {
						enabled: true,
						lineColor: 'rgb(100,100,100)'
					}
				}
			},
			states: {
				hover: {
					marker: {
						enabled: false
					}
				}
			}
			//tooltip: {
			//	headerFormat: '<b>{series.name}</b><br>',
			//	pointFormat: '{point.x}, {point.y}'
			//}

		}
	},
	series: [],

	credits: {
		enabled: false
	},
	lang: {
		noData: "No Data"
	},
	noData: {
		style: {
			fontWeight: 'bold',
			fontSize: '15px',
			color: '#303030'
		}
	}
	//	[{
	//	name: 'my tool',
	//	color: 'rgba(223, 83, 83, .5)',
	//	data: [[161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6],
	//		[170.0, 59.0], [159.1, 47.6], [166.0, 69.8], [176.2, 66.8], [160.2, 75.2],
	//		[176.5, 71.8], [164.4, 55.5], [160.7, 48.6], [174.0, 66.4], [163.8, 67.3]]
	//
	//}]
};

var bubble_chart_options = {

	chart: {
		type: 'bubble',
		plotBorderWidth: 1,
		zoomType: 'xy',
		renderTo: 'chart2'
	},

	legend: {
		enabled: false
	},

	title: {
		text: 'Cycle Time Average'
	},

	subtitle: {
		text: ''
	},

	xAxis: {
		gridLineWidth: 1,
		title: {
			text: 'Time (sec)'
		},
		labels: {
			format: '{value}'
		},
		plotLines: [{
			color: 'black',
			dashStyle: 'dot',
			width: 2,
			value: 65,
			label: {
				rotation: 0,
				y: 15,
				style: {
					fontStyle: 'italic'
				},
				text: 'Time (sec)'
			},
			zIndex: 3
		}]
	},

	yAxis: {
		startOnTick: false,
		endOnTick: false,
		title: {
			text: 'Date'
		},
		labels: {
			format: '{value}'
		},
		maxPadding: 0.2,
		plotLines: [{
			color: 'black',
			dashStyle: 'dot',
			width: 2,
			value: 50,
			label: {
				align: 'right',
				style: {
					fontStyle: 'italic'
				},
				text: 'Date',
				x: -10
			},
			zIndex: 3
		}],
		categories: []
	},

	tooltip: {
		useHTML: true,
		headerFormat: '<table>',
		pointFormat: '<tr><th colspan="2"><h3>{point.tool}</h3></th></tr>' +
		'<tr><th>Date:</th><td>{point.date}</td></tr>' +
		'<tr><th>Time (sec):</th><td>{point.x:.0f}</td></tr>' +
		'<tr><th># Cycles:</th><td>{point.z}</td></tr>',
		footerFormat: '</table>',
		followPointer: true
	},

	plotOptions: {
		series: {
			dataLabels: {
				enabled: true,
				format: '{point.name}'
			}
		}
	},

	series: [{
		data: [
			//{x: 200, y: 0, z: 433, name: '4/13/16', tool: 'my tool'}
		]
	}],

	credits: {
		enabled: false
	},
	lang: {
		noData: "No Data"
	},
	noData: {
		style: {
			fontWeight: 'bold',
			fontSize: '15px',
			color: '#303030'
		}
	}

};

$(document).ready(function () {
	chart1 = $('#chart1');
	chart2 = $('#chart2');
	table1 = $('#table1');

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.before_show_process_type_filter = function () {
		$('#parts-group').hide();
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);

	$(document).on('running', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running...</b>');
		//btn_run.prop("disabled", true);
	});
	$(document).on('complete', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop("disabled", false);
	});

});

function run_report() {
	data = NYSUS.REPORTS.REPORT_FILTER.get_selections();
	$(document).trigger('running');

	populate_chart1();
	populate_chart2();
	populate_table1();

}

function populate_chart1() {
	data.action = 'get_cycles_scatter_data';

	$.getJSON(processor, data, function (jso) {
		scatter_plot_options.yAxis.categories = jso.categories;
		scatter_plot_options.series = jso.series;
		chart1 = new Highcharts.Chart(scatter_plot_options);
	});

}

function populate_chart2() {
	data.action = 'get_cycles_bubble_data';

	$.getJSON(processor, data, function (jso) {
		bubble_chart_options.yAxis.plotLines = [];
		bubble_chart_options.xAxis.plotLines = [];
		bubble_chart_options.yAxis.categories = jso.categories;
		bubble_chart_options.series[0].data = jso.series;
		chart2 = new Highcharts.Chart(bubble_chart_options);
	});

}

function populate_table1() {
	data.action = 'get_cycles_data';

	$.getJSON(processor, data, function (jso) {
		if (dtTable1 != null) {
			dtTable1.destroy();
		}

		if (jso == null) {
			jso = [{result: 'No Data'}];
		}

		var cols = [];
		var objs = Object.getOwnPropertyNames(jso[0]);
		table1.remove();
		$('#panel-data .panel-body').append('<table id="table1" class="table table-striped table-condensed table-responsive"><thead></thead><tbody></tbody></table>');
		table1 = $('#table1');
		$.map(objs, function (item) {
			var i = column_titles.inArray(item, 'name'); // see if its in the column map
			var tit = item;
			if (i >= 0) {
				tit = column_titles[i].value;

				if (column_titles[i].display) {
					if (item != 'cycle_time') {
						cols.push({data: item, title: tit});
					} else {
						cols.push({data: 'cycle_time', title: tit});
					}
				}
			}
		});

		dtTable1 = table1.DataTable({
			"bAutoWidtch":false,
			dom: 'T<"clear">lfBrtip',
			width: "100%",
			buttons: [
					'copyHtml5',
					{
						extend: 'excelHtml5',
						//title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
					},
					{
						extend: 'csvHtml5',
						//title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
					},
					'pdfHtml5'
				],
			data: jso,
			columns: cols
		});
	});
}

Array.prototype.inArray = function (searchFor, property) {
	var retVal = -1;
	var self = this;
	for (var index = 0; index < self.length; index++) {
		var item = self[index];
		if (item.hasOwnProperty(property)) {
			if (item[property].toLowerCase() === searchFor.toLowerCase()) {
				retVal = index;
				return retVal;
			}
		}
	}
	return retVal;
};