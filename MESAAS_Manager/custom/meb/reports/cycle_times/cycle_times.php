<?php

require_once("../../../../ajax_processors/init.php");

/**
 * require_once the appropriate processor
 */
$process_type = $_REQUEST["process_type"];
switch (strtolower($process_type)) {
	case "repetitive":
		require_once(dirname(__FILE__)."\\cycle_times_repetitive.php");
		break;
	case "sequenced":
		require_once(dirname(__FILE__)."\\cycle_times_sequence.php");
		break;
	case "batch":
		require_once(dirname(__FILE__)."\\cycle_times_batch.php");
		break;
	default:
		break;
}

call_user_func($_REQUEST["action"], $db);

/**
 * formats a result to what highcharts expects
 * required: category, name, quantity
 * @param $result
 * @return array
 */
function format_to_highchart_scatter($result)
{
	if (count($result) < 1) {
		return array("categories" => "No Data", "series" => array());
	}

	// format result for highchart
	$categories = array();
	$series = array();

	// create categories
	foreach ($result as &$value) {
		// push to categories (distinct prod_dates)
		if (!in_array($value["category"], $categories)) {
			array_push($categories, $value["category"]);
		}

	}

	// create named series datapoints
	foreach($result as &$value) {
		$exists = false;
		foreach($series as &$ser) {
			if ($ser["name"] == $value["name"]) {
				$exists = true;
			}
		}
		if (!$exists) {
			$arr = array();
			$arr["name"] = $value["name"];
			array_push($series, $arr);
		}
	}

	// create data for each named series
	foreach($series as &$ser) {
		$ser["data"] = array();
		foreach($result as &$value) {
			if($value["name"] == $ser["name"]) {
				$arr = array($value["value"], array_search($value["category"], $categories));
				array_push($ser["data"], $arr);
			}
		}
	}

	return array("categories" => $categories, "series" => $series);

}

/**
 * formats a result to what highcharts expects
 * required: shift_desc, category, interval_desc, quantity fields
 * @param $result
 * @return array
 */
function format_to_highchart_bubble($result)
{
	if (count($result) < 1) {
		return array("categories" => "No Data", "series" => array());
	}

	// format result for highchart
	$categories = array();
	$series = array();

	// create categories
	foreach ($result as &$value) {
		// push to categories (distinct prod_dates)
		if (!in_array($value["category"], $categories)) {
			array_push($categories, $value["category"]);
		}
	}

	// create teh bubbles
	foreach ($result as &$value) {
		//{x: 200, y: 0, z: 433, name: '4/13/16', tool: 'poop'},
		$arr = array();
		$arr["x"] = $value["value"];
		$arr["y"] = array_search($value["category"], $categories);
		$arr["z"] = $value["quantity"];
		$arr["name"] = $value["name"];
		$arr["tool"] = $value["name"];
		$arr["date"] = $value["category"];
		array_push($series, $arr);
	}

	return array("categories" => $categories, "series" => $series);

}