$(document).ready(function () {
	var processor = 'custom/meb/reports/fault_report/fault_report.php';
	var dtResults;

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.before_show_process_type_filter = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(['#tools-group', '#parts-group']);
		NYSUS.REPORTS.REPORT_FILTER.show_elements(['#plcs-group', '#fault-codes-group'])
	};
	NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter = function () {
		NYSUS.REPORTS.REPORT_FILTER.controls.group_bys_select.ajax.url = processor;
		NYSUS.REPORTS.REPORT_FILTER.controls.group_bys_select.multiple = false;
		NYSUS.REPORTS.REPORT_FILTER.show_misc_filter();
	};
	NYSUS.REPORTS.REPORT_FILTER.after_show_misc_filter = function () {
		add_custom_misc_filters();
	};
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(['#operators-group']);
		$('.btn-run-report').on('click', function (e) {
			e.preventDefault();
			load_data();
		});
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	function load_data() {
		var data = NYSUS.REPORTS.REPORT_FILTER.get_selections();
		data.action = 'get_fault_chart_data';
		data.fault_codes = $('#sel_fault_codes').val();
		data.result_type = 'count';
		$.getJSON(processor, data, create_chart);
		data.result_type = 'duration';
		$.getJSON(processor, data, create_duration_chart);
		data.action = 'get_fault_data';
		data.top_by = $('input[name=opt]:checked').val();
		$.getJSON(processor, data, create_table);
	}

	function add_custom_misc_filters() {
		var template = $('#custom-filters').html();
		$('#filter-misc').append(template);
		$('#show-by').hide();
		$('#sel_top').on("change", function (e) {
			if ($(this).val().length > 0) {
				$('#show-by').show();
			} else {
				$('#show-by').hide();
			}
		});
	}

	function create_chart(jso) {

		$('#chart-fault-count').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Fault Count'
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -45,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Number of Faults'
				},
				stackLabels: {
					style: {
						color: 'black'
					},
					enabled: true
				}
			},
			plotOptions: {
				column: {
					stacking: 'normal',
					pointPadding: 0,
					groupPadding: 0,
					dataLabels: {
						enabled: false
					}
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Faults: <b>{point.y}</b>'
			},
			series: [jso],
			credits: {
				enabled: false
			}
		});

	}

	function create_duration_chart(jso) {

		$('#chart-fault-duration').highcharts({
			chart: {
				type: 'column'
			},
			title: {
				text: 'Fault Duration (sec)'
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				type: 'category',
				labels: {
					rotation: -45,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Sum of Fault Duration'
				},
				stackLabels: {
					style: {
						color: 'black'
					},
					enabled: true
				}
			},
			plotOptions: {
				column: {
					stacking: 'normal',
					pointPadding: 0,
					groupPadding: 0,
					dataLabels: {
						enabled: false
					}
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Duration: <b>{point.y}</b>'
			},
			series: [jso],
			credits: {
				enabled: false
			}
		});

	}

	function create_table(jso) {

		if (dtResults != null) {
			dtResults.destroy();
			$('#panel-data .panel-body').empty();
			$('#panel-data .panel-body').append('<table id="result-table" class="table table-striped table-condensed table-responsive"></table>');
		}

		if (jso.length == 0) {
			jso = [{'Result': 'No Data'}];
		}
		var cols = [];
		var objs = Object.getOwnPropertyNames(jso[0]);
		$.each(objs, function (k, v) {
			if (jso[0][v].date) {
				cols.push({data: v, title: v.toProperCase(), render: renderDate});
			} else {
				cols.push({data: v, title: v.toProperCase()});
			}
		});

		dtResults = $('#result-table').DataTable({
			destroy: true,
			responsive: true,
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},
			data: jso,
			columns: cols
		});

		$(document).trigger('report.complete');

	}

	String.prototype.toProperCase = function () {
		return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
			function ($1) {
				return $1.toUpperCase();
			}
		);
	};

	function renderDate(data, type, full) {
		if (data && data.date) {
			return moment(data.date).format('MM/DD/YYYY HH:mm');
		}
		return 'No Date';
	}

});