<?php

require_once("../../../../ajax_processors/init.php");

/**
 * require_once the appropriate processor
 */
$process_type = $_REQUEST["process_type"];
switch (strtolower($process_type)) {
	case "repetitive":
		require_once(dirname(__FILE__)."\\birth_certificate_repetitive.php");
		break;
	case "sequenced":
		require_once(dirname(__FILE__)."\\birth_certificate_sequence.php");
		break;
	case "batch":
		require_once(dirname(__FILE__)."\\birth_certificate_batch.php");
		break;
	default:
		break;
}

call_user_func($_REQUEST["action"], $db);

