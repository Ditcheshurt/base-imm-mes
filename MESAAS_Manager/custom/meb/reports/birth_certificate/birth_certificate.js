var processor = './custom/meb/reports/birth_certificate/birth_certificate.php';
var detailTable, data;
var column_titles = [
	{name: 'mc_ID', value: 'Cycle ID', display: false},
	{name: 'm_ID', value: 'Machine ID', display: false},
	{name: 'machine_name', value: 'Machine', display: true},
	{name: 'short_description', value: 'Tool', display: true},
	{name: 'tool_description', value: ' Tool Name', display: true},
	{name: 'part_desc', value: 'Part', display: true},
	{name: 'cycle_time', value: 'Cycle Date', display: true},
	{name: 'serial_number', value: 'Serial', display: true},
	{name: 'disposition', value: 'Disposition', display: true}
];

/*
				 mc.ID AS mc_ID
				,m.ID AS m_ID
				,m.machine_name
				,t.short_description
				,t.tool_description
				,p.part_desc
				,mc.cycle_time
				,mcp.serial_number
				,mcp.disposition

*/

$(document).ready(function () {
	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.render();
	
	var btn_run = $('.btn-run-report');
	btn_run.prop("disabled", true);
	btn_run.on('click', run_report);
	
	$(document).on('running', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running...</b>');
		btn_run.prop("disabled", true);
	});
	$(document).on('complete', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop("disabled", false);
	});

	NYSUS.REPORTS.ELEMENTS.shifts = false;	//disable shifts select
	
	// //clear all other system select event handlers and register our own
	// $(document).off('select2:select', '#sel_systems');
	// $(document).on('select2:select', '#sel_systems', function () {
		// var sel_systems = $('#sel_systems');

		// //$('#sel_shifts').select2(NYSUS.REPORTS.REPORT_FILTER.controls.shifts_select);
		// NYSUS.REPORTS.REPORT_FILTER.selected_system_ID = sel_systems.select2('data')[0].id;
		// NYSUS.REPORTS.REPORT_FILTER.selected_process_type = sel_systems.select2('data')[0].process_type;
		// NYSUS.REPORTS.REPORT_FILTER.show_process_type_filter();
		// $('.btn-run-report').prop("disabled", false);
	// });

});

function run_report() {
	data = NYSUS.REPORTS.REPORT_FILTER.get_selections();
	$(document).trigger('running');
	populate_production();
}

function populate_production() {
	$('#panel-results .panel-body').empty();

	data.action = 'get_production';
	$.getJSON(processor, data, function (jso) {
		if (jso) {
			if (jso.data && jso.data.length > 0) {
				populate_production_table(jso.data);
			} else {
				$('#panel-results .panel-body').append('<h3>No Results</h3>');
			}
		}
		$(document).trigger('complete');
	});
}

function populate_production_table(jso) {
	if (detailTable != null) {
		detailTable.destroy();
	}

	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	$('#panel-results .panel-body').append('<table id="production-table" class="table table-striped table-condensed table-responsive"><thead></thead><tbody></tbody></table>');
	var table = $('#production-table');

	//cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	$.map(objs, function (item) {
		var i = column_titles.inArray(item, 'name'); // see if its in the column map
		var tit = item;
		if (i >= 0) {
			tit = column_titles[i].value;

			if (column_titles[i].display) {
				cols.push({
					data: item,
					title: tit,
					sortable: item == 'cycle_time' ? false : true,
					render: item == 'cycle_time' ? function(d,t,r) {
						return d.date.substr(0,d.date.length-7);
					} : false
				});
			}
		}
	});

	cols.push({
		data: null,
		title: 'Birth Certificate',
		display: true,
		sortable: false,
		render: function (d,t,r) {
			return '<button class="btn btn-success btn-gen-cert" id="'+r.mc_ID+'" serial="'+r.serial_number+'"><span class="glyphicon glyphicon-book"></span>';
		}
	});

	detailTable = table.DataTable({
		"bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],
		data: jso,
        columns: cols
	});
	
	$('table#production-table tbody').on('click', '.btn-gen-cert', function (e) {
		var mc_id = this.id;
		var serial = this.attributes.serial.value;
		
		console.log("BCERT FOR MC_ID: " + mc_id + ", serial: " + serial);
		window.open('custom/meb/reports/birth_certificate/generate_bcert.html?cycle_ID='+mc_id+'&serial='+serial);
	});
}

Array.prototype.inArray = function (searchFor, property) {
	var retVal = -1;
	var self = this;
	for (var index = 0; index < self.length; index++) {
		var item = self[index];
		if (item.hasOwnProperty(property)) {
			if (item[property].toLowerCase() === searchFor.toLowerCase()) {
				retVal = index;
				return retVal;
			}
		}
	}
	return retVal;
};