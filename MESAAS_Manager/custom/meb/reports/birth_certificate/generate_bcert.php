<?php
	require_once("../../../../ajax_processors/init.php");

    $action = $_REQUEST['action'];
    	
    if ($action == 'load_data') {
        global $db;
        $res = new StdClass();
        $r = $_REQUEST;
        
		$cycle_ID = $r['cycle_ID'];
        $serial = $r['serial'];
        
        $sql = "SELECT TOP (200)
					 mc.cycle_sequence
					,mc.mrp_backflush_time
					,mc.cycle_duration
					,mcp.cycle_ID
					,mcp.serial_number
					,mcp.full_barcode
					,mcp.insert_time
					,mcp.disposition
					,mcp.disposition_time
					,tp.part_order
					,t.short_description
					,t.tool_description
					,p.part_desc
					,p.mrp_part_number
					,p.part_image
					,o.name
					,mct.lot_number
				FROM machine_cycles mc
				JOIN machine_cycle_parts mcp ON mcp.cycle_ID = mc.ID
				JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				LEFT JOIN machines m ON m.ID = mc.machine_ID
				LEFT JOIN tools t ON t.ID = tp.tool_ID
				LEFT JOIN parts p ON p.ID = tp.part_ID
				LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
				LEFT JOIN CUSTOM_machine_lot_tracking mlt ON mcp.insert_time BETWEEN mlt.start_timestamp
					AND ISNULL(mlt.end_timestamp,GETDATE())
					AND mlt.machine_ID = mc.machine_ID
				LEFT JOIN CUSTOM_material_certification_test mct ON mct.ID = mlt.material_cert_test_ID
				WHERE mc.ID = ".$cycle_ID." AND mcp.serial_number = '".$serial."'";
		//die($sql);
        $res->part_info = $db->query($sql);
        
        $sql = "
				DECLARE @sql AS nvarchar(MAX)
				DECLARE @cols AS varchar(MAX)

				SELECT @cols = COALESCE(@cols + ',','') + QUOTENAME(data_point_name)
				FROM machine_cycle_data_points mcdp
				LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
				WHERE mcdp.cycle_ID = ".$cycle_ID."

				SET @sql = N'SELECT p1.*
				FROM (
					SELECT mcdp.cycle_ID, dps.data_point_name AS k, CONCAT(CAST(mcdp.data_point_value AS varchar), '' '', dps.units) AS v
					FROM machine_cycle_data_points mcdp
					LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
					WHERE mcdp.cycle_ID = ".$cycle_ID."
				) t1
				PIVOT (
					MAX(t1.v)
					FOR t1.k IN ('+@cols+')
				) AS p1'

				EXEC sp_executesql @sql";
		//die($sql)
		$res->param_info = $db->query($sql);
		
		$sql = "
			DECLARE @cycle_ID int = {$cycle_ID}
			DECLARE @serial varchar(10) = '{$serial}'

			SELECT p1.*
			FROM (
				SELECT
					mct.[timestamp]
					,mcs.data_point_name AS [name]
					,CONCAT(
						mcr.matcert_datapoint_value,
						REPLACE(ISNULL(mcs.range_separator,''),'+-','+/-'),
						mcr.matcert_datapoint_range,
						mcs.units
					) AS [value]
				FROM machine_cycle_parts mcp
				JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
				JOIN CUSTOM_machine_lot_tracking mlt ON mcp.insert_time BETWEEN mlt.start_timestamp
					AND ISNULL(mlt.end_timestamp,GETDATE())
					AND mlt.machine_ID = mc.machine_ID
				JOIN CUSTOM_material_certification_test mct ON mct.ID = mlt.material_cert_test_ID
				JOIN CUSTOM_material_certification_results mcr ON mcr.matcert_test_ID = mlt.material_cert_test_ID
				LEFT JOIN CUSTOM_material_certification_setup mcs ON mcs.ID = mcr.matcert_datapoint_ID
				WHERE mcp.cycle_ID = @cycle_ID
				AND mcp.serial_number = @serial
			) t1
			PIVOT(
				MAX(t1.value)
				FOR t1.name IN (
					[sample_name],
					[final_result],
					[pass_per_fail],
					[start_weight],
					[start_wt_target],
					[end_weight],
					[test_duration],
					[start_temp],
					[test_temp]
				)
			) p1;
		";
		//die($sql);
		
		$res->mcert_info = $db->query($sql);
		
		$sql = "
				SELECT TOP (200)
					 mcp.cycle_ID
					,mcp.serial_number
					,mcp.full_barcode
					,mcp.disposition
					,mcp.disposition_time
					,mdl.ID
					,p.part_image
					,o.name
					,mdl.defect_time
					,mdr.defect_description
					--,mdloc.*
				FROM machine_cycles mc
				JOIN machine_cycle_parts mcp ON mcp.cycle_ID = mc.ID
				JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				LEFT JOIN machine_defect_log mdl ON mdl.machine_ID = mc.machine_ID AND mdl.tool_ID = tp.tool_ID AND mdl.part_ID = tp.part_ID AND mdl.serial_number = mcp.serial_number
				LEFT JOIN machine_defect_reasons mdr ON mdr.ID = mdl.defect_ID
				--LEFT JOIN machine_defect_locations mdloc ON mdloc.machine_defect_ID = mdl.ID
				LEFT JOIN parts p ON p.ID = tp.part_ID
				LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mdl.operator_ID
				WHERE mc.ID = ".$cycle_ID." AND mcp.serial_number = '".$serial."'";
		//die($sql);
		$res->disp_info = $db->query($sql);

        if ($res->part_info[0]['disposition'] == 'SCRAP') {
			$sql = "
					SELECT TOP (200)
						mdloc.ID
						,mdloc.defect_location_x
						,mdloc.defect_location_y
					FROM machine_cycles mc
					JOIN machine_cycle_parts mcp ON mcp.cycle_ID = mc.ID
					JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
					LEFT JOIN machine_defect_log mdl ON mdl.machine_ID = mc.machine_ID AND mdl.tool_ID = tp.tool_ID AND mdl.part_ID = tp.part_ID AND mdl.serial_number = mcp.serial_number
					LEFT JOIN machine_defect_reasons mdr ON mdr.ID = mdl.defect_ID
					LEFT JOIN machine_defect_locations mdloc ON mdloc.machine_defect_ID = mdl.ID
					WHERE mc.ID = ".$cycle_ID." AND mcp.serial_number = '".$serial."'";
			//die($sql);
			$res->scrap_info = $db->query($sql);
        }

        echo json_encode($res);
    }
?>