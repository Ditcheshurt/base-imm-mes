<?php
/**
 * Created by IntelliJ IDEA.
 * User: sdaviduk
 * Date: 3/24/2016
 * Time: 1:55 PM
 */

function get_production($db)
{
	$system_ID = $_REQUEST["system_ID"];
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

//	$where = "WHERE 1=1 ";
	$where = "AND 1=1";

	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND tp.tool_ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["parts"]) && $_REQUEST["parts"] != null) {
		$where .= " AND tp.part_ID IN (" . implode(",", $_REQUEST["parts"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}

	$sql = "SET NOCOUNT ON;

			DECLARE @start_date datetime
			DECLARE @end_date datetime

			SET @start_date = CAST('$start_date' AS datetime)
			SET @end_date = CAST('$end_date' AS datetime)

			SELECT
				 mc.ID AS mc_ID
				,m.ID AS m_ID
				,m.machine_name
				,t.short_description
				,t.tool_description
				,p.part_desc
				,mc.cycle_time
				,mcp.serial_number,mcp.disposition
			FROM machine_cycles mc
			JOIN machine_cycle_parts mcp ON mcp.cycle_ID = mc.ID
			JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
			LEFT JOIN tools t ON t.ID = tp.tool_ID
			LEFT JOIN parts p ON p.ID = tp.part_ID
			LEFT JOIN machines m ON m.ID = mc.machine_ID
			LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
			WHERE mc.cycle_time BETWEEN CONVERT(DATE, @start_date, 113) AND DATEADD(day,1,CONVERT(DATE, @end_date, 113))
			AND m.system_ID = 1 AND mcp.disposition IS NOT NULL
			$where
			ORDER BY mc.ID ASC";
//die($sql);
	$data = $db->query($sql);

	$result = array();
	$result["data"] = $data;

	echo json_encode($result);
}