var processor = './generate_bcert.php';
var cur_defect_locs = [];
var overview_tbl = {
	width: 2,
	fields:[
		{name: 'insert_time', value: 'Part Created', display: true, type: 'datetime'},
			{name: 'short_description', value: 'Tool Short Desc', display: true},
		{name: 'part_desc', value: 'Part Desc', display: true},
			{name: 'tool_description', value: 'Tool Full Desc', display: true},
		{name: 'disposition', value: 'Disposition', display: true},
			{name: 'serial_number', value: 'Serial', display: true},
		{name: 'disposition_time', value: 'Disposition Time', display: true, type: 'datetime'},
			{name: 'full_barcode', value: 'Barcode', display: true},
		{name: 'mrp_part_number', value: 'MRP Part Number', display: true},
				{name: 'mrp_backflush_time', value: 'MRP Backflush Time', display: false},
				{name: 'cycle_ID', value: 'Machine Cycle ID', display: false},
				{name: 'cycle_sequence', value: 'Cycle Sequence', display: false},
				{name: 'cycle_duration', value: 'Cycle Duration', display: false},
				{name: 'part_order', value: 'Part Order', display: false},
			{name: 'lot_number', value: 'Lot Number', display: true}
	]
};
var param_tbl = {
	width: 1,
	fields: [
		{name: 'COUNT', value: 'Cycle Count', display: false},
		{name: 'ActTimCyc', value: 'Cycle Time', display: true},
		{name: 'ActTimFill[1]', value: 'Fill Time', display: true},
		{name: 'ActTimPlst[1]', value: 'Plasticize Time', display: true},
		{name: 'ActTmpBrlZn[1,5]', value: 'Barrel Temp Zn[5]', display: true},
		{name: 'ActStrCsh[1]', value: 'Stroke Pos At Cushion', display: true},
		{name: 'ActTmpOil', value: 'Oil Temp', display: true},
		{name: 'CycClpClsTim', value: 'Clamp Close Time', display: true},
		{name: 'CycClpOpnTim', value: 'Clamp Open Time', display: true},
		{name: 'CycCoolTim', value: 'Cycle Cool Time', display: true},
		{name: 'ActPrsXfrSpec[1]', value: 'Specific Transfer Pressure', display: true},
		{name: 'ActPrsHldSpecMax[1]', value: 'Specific Hold Pressure Max', display: true},
		{name: 'ActStrPlst[1]', value: 'Plastic Inject Stroke', display: true},
		{name: 'MldHtCl1T1', value: 'Mold Tmp Z1', display: true},
		{name: 'MldHtCl1T2', value: 'Mold Tmp Z2', display: true},
		{name: 'MldHtCl1T3', value: 'Mold Tmp Z3', display: true},
		{name: 'MldHtCl1T4', value: 'Mold Tmp Z4', display: true},
		{name: 'MldHtCl1T5', value: 'Mold Tmp Z5', display: true},
		{name: 'MldHtCl1T6', value: 'Mold Tmp Z6', display: true},
		{name: 'MldHtCl1T7', value: 'Mold Tmp Z7', display: true},
		{name: 'MldHtCl1T8', value: 'Mold Tmp Z8', display: true},
		{name: 'CycDataFlow1', value: 'Water Flow', display: true},
		{name: 'CycPlstTim', value: 'Recovery Time', display: true},
		{name: 'Plst1Prf_Fce', value: 'Back Pressure', display: true},
		{name: 'CycHldTim', value: 'Total Hold Time', display: true},
		{name: 'CycScrPosXfr', value: 'Transfer Postion', display: true},
		{name: 'CycBreakTim', value: 'Mold Pause Time', display: true},
		{name: 'CycDemold', value: 'Demold Time', display: true},
		{name: 'MldHtCl1T1_TmpSet', value: 'Thermolator Set Point', display: true},
		{name: 'MldHtCl1T1_CycDataTmpZone', value: 'Thermolator Actual', display: true},
		{name: 'Inj1T1_CycDataTmpZone', value: 'Barrel Temperature - Actual', display: true},
		{name: 'MldHtg1T1_CycDataTmpZone', value: 'Hot Runner Temperature - Actual', display: true},
		{name: 'MLD_ClpFceAct', value: 'Clamp Force - Cycle', display: true},
		{name: 'Hld1Prf_Fce', value: 'Hold Pressure', display: true},
		{name: 'Plst1Prf_Vel', value: 'Screw Speed - Cycle', display: true},
		{name: 'Inj1_PlstRpmAct', value: 'Alt Screw Speed - Cycle', display: true}
	]
};

var matcert_tbl = {
	width: 2,
	fields: [
		{name: 'sample_name', value:'Sample Name', display: true},
		{name: 'timestamp', value: 'Test Timestamp', display: true, type: 'datetime'},
		{name: 'start_wt_target', value:'Target Start Weight', display: true},
		{name: 'test_duration', value:'Test Duration (mm:ss)', display: true},
		{name: 'start_weight', value:'Start Weight', display: true},
		{name: 'end_weight', value:'End Weight', display: true},
		{name: 'start_temp', value:'Start Temperature', display: true},
		{name: 'test_temp', value:'Test Temperature', display: true},
		{name: 'final_result', value:'Percent Ash', display: true},
		{name: 'pass_per_fail', value:'Pass/Fail', display: true}
	]
};

// /imm_mes/public/images/part_images/Liftgate Inner.png
var disp_tbl = {
	width: 1,
	fields: [
		{name: 'disposition', value: 'Disposition', display: true},
		{name: 'ID', value: 'Defect ID', display: true},
		{name: 'defect_time', value: 'Defect Recorded Time', display: true, type: 'datetime'},
		{name: 'name', value: 'Defect Recorded By', display: true},
		{name: 'defect_description', value: 'Defect Desc', display: true},
		{name: 'part_image', value: 'Part Image', display: true, type: 'image'},
			{name: 'cycle_ID', value: 'Cycle ID', display: false},
			{name: 'serial_number', value: 'Serial', display: false},
			{name: 'full_barcode', value: 'Barcode', display: false},
			{name: 'disposition_time', value: 'Disposition Time', display: false, type: 'datetime'}
	]
};

$(document).ready(function () {
	var data = {};
	var req = getRequests();
	
	console.log(req);
	
	data.action = "load_data";
	data.cycle_ID = req.cycle_id;
	data.serial = req.serial;

	$('#serial').html(data.serial);

	$.getJSON(processor, data, function (jso) {
		var jso = jso || [];
		var part_info = jso.part_info?jso.part_info[0]:[];
		var mcert_info = jso.mcert_info?jso.mcert_info[0]:[];
		var param_info = jso.param_info?jso.param_info[0]:[];
		var disp_info = jso.disp_info?jso.disp_info[0]:[];
		cur_defect_locs = jso.scrap_info?jso.scrap_info[0]:[];
		
		var tpart = $('#part_overview_table tbody');
		var tparam = $('#param_overview_table tbody');
		var tmcert = $('#mcert_overview_table tbody');
		var tdisp = $('#disp_overview_table tbody');
		
		if(mcert_info.length == 0){$('#mcert_overview').attr('hidden',true);}
		
		tableFromObject(part_info, overview_tbl, tpart);
		tableFromObject(param_info, param_tbl, tparam);
		tableFromObject(mcert_info, matcert_tbl, tmcert);
		tableFromObject(disp_info, disp_tbl, tdisp);
	});
});


function getRequests() {
    var s1 = location.search.substring(1, location.search.length).split('&'),
        r = {}, s2, i;
    for (i = 0; i < s1.length; i += 1) {
        s2 = s1[i].split('=');
        r[decodeURIComponent(s2[0]).toLowerCase()] = decodeURIComponent(s2[1]);
    }
    return r;
};

function tableFromObject(obj, config, tbody) {
	var fields = config.fields;
	var tr = $('<TR />'), l, v;
	var w = config.width || 2, x = 0;

	for(var i = 0; i < fields.length; i++) {
		var k = fields[i].name;
		if (fields[i].display && obj[k]) {
			l = fields[i].value;
			v = obj[k];
			if (l) {
				var fld_type = fields[i].type || '';
				var el;
				switch (fld_type) {
					case 'datetime':
						el = $('<TD />').html('<label>'+l+': </label>'+formatDate(v.date));
						break;
					case 'image':
						var el = document.createElement('div', 'div_defect_container');
						el.innerHTML = '<svg id="div_defect_location_container"></svg>';

						var svg = el.children[0];
						svg.innerHTML = '<image xlink:href="'+v+'" id="part_image"/>';

						var img = svg.children[0];
						img.onload = function () {
							var dfct = $('#div_defect_location_container');
							drawDefectLocations(cur_defect_locs, dfct);
						};
						break;
					default:
						el = $('<TD />').html('<label>'+l+': </label>'+v);
				}
				tr.append(el);
				x++;
			}
		}
		
		if (x%w == 0 || i == (fields.length - 1)) {
			if (tr) {
				tbody.append(tr);
			}
			tr = $('<TR />');
			x = 0;
		}
	}
}

function formatDate (d) {
	return d.substring(0,d.length-7);
}

function drawDefectLocations(locs, elem) {
	elem.children('CIRCLE').remove();
	
	var i = document.getElementById('part_image')
	var b = i.getBBox()
	
	for (var i = 0; i < locs.length; i++) {
		var c = $('<circle />');
		var obj = locs[i];
		c.attr("cx", b.width*(obj.defect_location_x/600));
		c.attr("cy", b.height*(obj.defect_location_y/600));
		c.attr("r", 5);
		c.attr("stroke", "#CCCCCC");
		c.attr("stroke-width", 2);
		c.attr("fill", "orange");

		$('#div_defect_location_container').append(c);
	}
	elem.html(elem.html());
}
