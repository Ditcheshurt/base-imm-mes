var processor = './custom/meb/reports/gauging/autogauge.php';
var hide_me = ['#machines-group', '#tools-group', '#operators-group', '#shifts-group'];
var dtResults;
var cols = [
	{'data': 'serial_number', 'title': 'Serial Number'},
	{'data': 'part_desc', 'title': 'Description'},
	//{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
	//{ 'data': 'lot_num', 'title': 'Lot Number' },
	{'data': 'cycle_time.date', 'title': 'Check Time', 'render': renderDate},
	{'data': 'good_part', 'title': 'Pass/Fail', 'render': renderBool},
	{'data': 'm1', 'title': 'm1'},
	{'data': 'm2', 'title': 'm2'},
	{'data': 'm3', 'title': 'm3'},
	{'data': 'm4', 'title': 'm4'},
	{'data': 'taper', 'title': 'Taper'},
	{'data': 'sp1_low', 'title': 'Sp1 Low'},
	{'data': 'sp1_high', 'title': 'Sp1 High'},
	{'data': 'sp2_low', 'title': 'Sp2 Low'},
	{'data': 'sp2_high', 'title': 'Sp2 High'},
	{'data': 'sp3_low', 'title': 'Sp3 Low'},
	{'data': 'sp3_high', 'title': 'Sp3 High'},
	{'data': 'sp4_low', 'title': 'Sp4 Low'},
	{'data': 'sp4_high', 'title': 'Sp4 High'},
	{'data': 'spt_low', 'title': 'Spt1 Low'},
	{'data': 'spt_high', 'title': 'Spt1 High'},
	{'data': 'operator', 'title': 'Operator'},
	{'data': 'recorded_date.date', 'title': 'Recorded', 'render': renderDate}
];

$(document).ready(function () {

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);

	$(document).on('report.running', function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running..</b>');
		btn_run.prop('disabled', true);
	});
	$(document).on('report.complete', function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop('disabled', false);
	});

});

function run_report() {
	var system_id = $('#sel_systems').val();
	var start_date = $('#inp_start_date').val();
	var end_date = $('#inp_end_date').val();
	var data = {
		'url': processor,
		'type': 'POST',
		'data': {
			'action': 'get_data',
			'system_id': system_id,
			'start_date': start_date,
			'end_date': end_date
		},
		'dataSrc': function (jso) {
			$(document).trigger('report.complete');
			return jso;
		}
	};

	$('#sel_parts').val() != null ? data.data.parts = $('#sel_parts').val() : null;
	$('#sel_operators').val() != null ? data.data.operators = $('#sel_operators').val() : null;
	$('#sel_shifts').val() != null ? data.data.shifts = $('#sel_shifts').val() : null;

	$(document).trigger('report.running');

	dtResults = $('#result-table').DataTable({
		destroy: true,
		responsive: true,
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		},
		ajax: data,
		columns: cols
	});

}

function renderBool(data, type, full, meta) {
	if (data == 1) {
		return '<span class="glyphicon glyphicon-ok" />';
	} else {
		return '<span class="glyphicon glyphicon-remove" />';
	}
}

function renderDate(data, type, full) {
	return moment(data).format('MM/DD/YYYY HH:mm');
}