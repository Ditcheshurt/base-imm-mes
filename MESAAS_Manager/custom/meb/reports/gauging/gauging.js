var processor = './custom/meb/reports/gauging/gauging.php';
var hide_me = ['#machines-group', '#tools-group', '#parts-group', '#operators-group', '#shifts-group'];
var dtResults;

$(document).ready(function () {

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter = function() {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
		show_gauge_types_combo();
	};
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);

	$(document).on('report.running', function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running..</b>');
		btn_run.prop('disabled', true);
	});
	$(document).on('report.complete', function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop('disabled', false);
	});

});

function run_report() {
	var system_id = $('#sel_systems').val();
	var start_date = $('#inp_start_date').val();
	var end_date = $('#inp_end_date').val();
	var gauge_material_type_ID = $('#sel_gauge_types').val();
	var data = {
		'action': 'get_data',
		'system_id': system_id,
		'start_date': start_date,
		'end_date': end_date,
		'gauge_material_type_ID': gauge_material_type_ID
	};

	$('#sel_parts').val() != null ? data.data.parts = $('#sel_parts').val() : null;
	$('#sel_operators').val() != null ? data.data.operators = $('#sel_operators').val() : null;
	$('#sel_shifts').val() != null ? data.data.shifts = $('#sel_shifts').val() : null;

	$(document).trigger('report.running');

	$.getJSON(processor, data, function (jso) {
		if (dtResults != null) {
			dtResults.destroy();
			$('#panel-data .panel-body').empty();
			$('#panel-data .panel-body').append('<table id="result-table" class="table table-striped table-condensed table-responsive"></table>');
		}

		if (jso.length == 0) {
			jso = [{'Result': 'No Data'}];
		}
		var cols = [];
		var objs = Object.getOwnPropertyNames(jso[0]);
		
		$.each(objs, function (k, v) {
			if (v == 'recorded_date') {
				cols.push({data: v, title: v.toProperCase(), render: renderDate});
			} else {
				cols.push({data: v, title: v.toProperCase()});
			}
		});

		dtResults = $('#result-table').DataTable({
			destroy: true,
			responsive: true,
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},
			data: jso,
			columns: cols
		});

		$(document).trigger('report.complete');

	});

}

function show_gauge_types_combo() {
	$('<div>').load('./custom/meb/reports/gauging/gauging.html #types-group', function() {
		var gauge_type_options = {
			width: '100%',
			placeholder: 'Check Types',
			multiple: false,
			ajax: {
				cache: true,
				url: processor,
				dataType: 'json',
				type: "GET",
				quietMillis: 250,
				theme: "bootstrap",
				data: function (params) {
					return {
						q: params.term,
						action: 'get_gauge_types',
						system_ID: $('#sel_systems').val(),
						page: params.page
					};
				},
				processResults: function (data) {
					return {
						results: $.map(data, function (item) {
							return {
								id: item.ID,
								text: item.description
							}
						})
					}
				}
			}
		};
		$('#filter-repetitive').append($(this).html());
		$('#sel_gauge_types').select2(gauge_type_options);
		$('#types-group').show();
	});

}

String.prototype.toProperCase = function () {
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
		function ($1) {
			return $1.toUpperCase();
		}
	);
};

function renderBool(data, type, full, meta) {
	if (data == 1) {
		return '<span class="glyphicon glyphicon-ok" />';
	} else {
		return '<span class="glyphicon glyphicon-remove" />';
	}
}

function renderDate(data, type, full) {
	if (data && data.date) {
		return moment(data.date).format('MM/DD/YYYY HH:mm');
	}
	return 'No Date';
}