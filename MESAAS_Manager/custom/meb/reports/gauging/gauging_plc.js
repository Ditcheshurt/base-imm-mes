var processor = './custom/meb/reports/gauging/gauging_plc.php';
var hide_me = ['#tools-group', '#operators-group', '#shifts-group'];
var dtResults;

$(document).ready(function () {	

	NYSUS.REPORTS.REPORT_FILTER.controls.machines_select.multiple = false;
	//litchfield custom filter the machines results
	NYSUS.REPORTS.REPORT_FILTER.controls.machines_select.ajax.processResults = function (data) {
		return {
			results: $.map(data, function (item) {

				if ($('#sel_systems').val() != 2) {
					return {
						id: item.ID,
						text: item.machine_number + ': ' + item.machine_name
					}
				} else if (item.ID == 10030 || item.ID == 20030 || item.ID == 30080) {
					return {
						id: item.ID,
						text: item.machine_number + ': ' + item.machine_name
					}
				}
			})
		}
	};

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.after_show_process_type_filter = function() {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);

	$(document).on('report.running', function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running..</b>');
		btn_run.prop('disabled', true);
	});
	$(document).on('report.complete', function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop('disabled', false);
	});

});

function run_report() {
	var system_id = $('#sel_systems').val();
	var start_date = $('#inp_start_date').val();
	var end_date = $('#inp_end_date').val();
	var machine_ID = $('#sel_machines').val();
	var data = {
		'action': 'get_data',
		'system_id': system_id,
		'start_date': start_date,
		'end_date': end_date,
		'machine_ID': machine_ID
	};

	$('#sel_parts').val() != null ? data.parts = $('#sel_parts').val() : null;
	$('#sel_operators').val() != null ? data.operators = $('#sel_operators').val() : null;
	$('#sel_shifts').val() != null ? data.shifts = $('#sel_shifts').val() : null;

	$(document).trigger('report.running');

	$.getJSON(processor, data, function (jso) {
		if (dtResults != null) {
			dtResults.destroy();
			$('#panel-data .panel-body').empty();
			$('#panel-data .panel-body').append('<table id="result-table" class="table table-striped table-condensed table-responsive"></table>');
		}

		if (jso == null || jso.length == 0) {
			jso = [{'Result': 'No Data'}];
		}

		var cols = [];
		var objs = Object.getOwnPropertyNames(jso[0]);
		$.each(objs, function (k, v) {
			if (v == 'recorded_date') {
				cols.push({data: v, title: 'Recorded', render: renderDate});
			} else {
				cols.push({data: v, title: v.toProperCase()});
			}
		});

		dtResults = $('#result-table').DataTable({
			"scrollX": true,
			destroy: true,
			responsive: true,
			dom: 'T<"clear">lfrtip',
			tableTools: {
				"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
				"aButtons": [
					"copy",
					"csv",
					"xls",
					{
						"sExtends": "pdf",
						"sPdfOrientation": "portrait"
					},
					"print"
				]
			},
			data: jso,
			columns: cols
		});

		$(document).trigger('report.complete');
	});

}

String.prototype.toProperCase = function () {
	return this.toLowerCase().replace(/(^[a-z]| [a-z]|-[a-z])/g,
		function ($1) {
			return $1.toUpperCase();
		}
	);
};

function renderBool(data, type, full, meta) {
	if (data == 1) {
		return '<span class="glyphicon glyphicon-ok" />';
	} else {
		return '<span class="glyphicon glyphicon-remove" />';
	}
}

function renderDate(data, type, full) {
	if (data && data.date) {
		return moment(data.date).format('MM/DD/YYYY HH:mm');
	}
	return 'No Date';
}

function sortResults(a, b) {
	if (a.data.split('_').length > 1 && b.data.split('_').length > 1) {
		return parseInt(a.data.split('_')[1]) - parseInt(b.data.split('_')[1]);
	}
	return -1;

}