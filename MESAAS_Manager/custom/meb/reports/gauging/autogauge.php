<?php

require_once("../../../../ajax_processors/init.php");

$action = isset($_REQUEST['action']) ? fixDB($_REQUEST['action']) : null;

call_user_func($action, $db);

function get_data($db)
{
	$system_ID = isset($_REQUEST['system_id']) ? fixDB($_REQUEST['system_id']) : null;
	$start_date = isset($_REQUEST['start_date']) ? fixDB($_REQUEST['start_date']) : null;
	$end_date = isset($_REQUEST['end_date']) ? fixDB($_REQUEST['end_date']) : null;
	$parts = isset($_REQUEST['parts']) ? fixDB($_REQUEST['parts']) : null;
	$operators = isset($_REQUEST['operators']) ? fixDB($_REQUEST['operators']) : null;
	$shifts = isset($_REQUEST['shifts']) ? fixDB($_REQUEST['shifts']) : null;

	$sql = "SELECT
                mc.lotnum AS lot_num
                ,mc.cycle_time
                ,a.serial_number
                ,p.part_number
                ,p.mrp_part_number
                ,p.part_desc
                ,a.[cycle_ID]
                ,[part_name]
                ,[m1]
                ,[m2]
                ,[m3]
                ,[m4]
                ,[taper]
                ,[sp1_low]
                ,[sp1_high]
                ,[sp2_low]
                ,[sp2_high]
                ,[sp3_low]
                ,[sp3_high]
                ,[sp4_low]
                ,[sp4_high]
                ,[spt_low]
                ,[spt_high]
                ,[good_part]
                ,[recorded_date]
                ,o.last_name + ', ' + o.first_name AS operator
            FROM
                [dbo].[gauge_autogauge] a
                JOIN [dbo].[machine_cycles] mc ON a.cycle_ID = mc.ID
                JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
                JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
                JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
                JOIN [dbo].[parts] p ON p.ID = tp.part_ID
                JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
                JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
                JOIN [MES_COMMON].[dbo].[operators] o ON o.ID = mc.operator_ID";

	if ($shifts != null) {
		$sql .= " JOIN MES_COMMON.dbo.time_intervals ti ON CONVERT(time, a.recorded_date) BETWEEN ti.interval_start_time AND ti.interval_end_time
				JOIN MES_COMMON.dbo.shift_time_intervals sti ON sti.time_interval_ID = ti.ID
				JOIN MES_COMMON.dbo.shifts s ON sti.shift_ID = s.ID AND m.system_ID = s.system_ID";
	}

	$sql .= " WHERE
				m.system_ID = $system_ID
                AND [dbo].getProdDate(mc.cycle_time) BETWEEN '$start_date' AND '$end_date 23:59:59.99'";

	if ($parts != null) {
		$sql .= " AND p.ID IN (" . implode(", ", $parts) . ")";
	}
	if ($operators != null) {
		$sql .= " AND mc.operator_ID IN (" . implode(", ", $operators) . ")";
	}
	if ($shifts != null) {
		$sql .= " AND s.ID IN (" . implode(", ", $shifts) . ")";
	}

	$sql .= " ORDER BY mc.cycle_time desc";
//die($sql);
	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}
	echo json_encode($res);
}