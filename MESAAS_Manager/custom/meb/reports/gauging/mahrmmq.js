var processor = './custom/meb/reports/gauging/mahrmmq.php';
var hide_me = ['#machines-group', '#tools-group', '#operators-group', '#shifts-group', '#systems-group'];
var dtResults;
var cols = [
	{'data': 'serial_number', 'title': 'Serial Number'},
	{'data': 'part_desc', 'title': 'Description'},
	//{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
	//{ 'data': 'lot_num', 'title': 'Lot Number' },
	//{'data': 'cycle_time.date', 'title': 'Cycle Time', 'render': renderDate},
	{'data': 'created_date.date', 'title': 'Test Time'},
	{'data': 'name', 'title': 'Name'},
	{'data': 'l_tol', 'title': 'l_tol'},
	{'data': 'u_tol', 'title': 'u_tol'},
	{'data': 'value', 'title': 'Value'},
	{'data': 'unit', 'title': 'Unit'},
	{'data': 'low_nat_border', 'title': 'low_nat_border'},
	{'data': 'type', 'title': 'type'},
	{'data': 'no', 'title': 'no'},
	{'data': 'operator', 'title': 'Operator'},
	{'data': 'date', 'title': 'date'},
	{'data': 'time', 'title': 'time'}
];

$(document).ready(function () {

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.after_show_filter = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
	};
	NYSUS.REPORTS.REPORT_FILTER.after_render = function () {
		NYSUS.REPORTS.REPORT_FILTER.hide_elements(hide_me);
		$('.btn-run-report').prop('disabled', false);
	};
	NYSUS.REPORTS.REPORT_FILTER.render();

	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);

	$(document).on('report.running', function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running..</b>');
		btn_run.prop('disabled', true);
	});
	$(document).on('report.complete', function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop('disabled', false);
	});

});

function run_report() {
	var start_date = $('#inp_start_date').val();
	var end_date = $('#inp_end_date').val();
	var data = {
		'url': processor,
		'type': 'POST',
		'data': {
			'action': 'get_data',
			'start_date': start_date,
			'end_date': end_date
		},
		'dataSrc': ''
	};

	$('#sel_machines').val() != null ? data.data.machines = $('#sel_machines').val() : null;
	$('#sel_operators').val() != null ? data.data.operators = $('#sel_operators').val() : null;
	$('#sel_shifts').val() != null ? data.data.shifts = $('#sel_shifts').val() : null;

	$(document).trigger('report.running');

	dtResults = $('#result-table').DataTable({
		destroy: true,
		responsive: true,
		dom: 'T<"clear">lfrtip',
		tableTools: {
			"sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
			"aButtons": [
				"copy",
				"csv",
				"xls",
				{
					"sExtends": "pdf",
					"sPdfOrientation": "portrait"
				},
				"print"
			]
		},
		ajax: data,
		columns: cols
	});

	$(document).trigger('report.complete');
}

function renderBool(data, type, full, meta) {
	if (data == 1) {
		return '<span class="glyphicon glyphicon-ok" />';
	} else {
		return '<span class="glyphicon glyphicon-remove" />';
	}
}

function renderDate(data, type, full) {
	return moment(data).format('MM/DD/YYYY HH:mm');
}