; $(document).ready(function () {
    var dtResults;
    var dtResultDetails;
    var processor = './custom/meb/reports/gauge_material_types/gauge_material_types.php';

    // Events
    $('.panel-footer').on('click', '.btn-run', run_report);

    // use moment plugin for datatables dates
    $.fn.dataTable.moment('dd/MM/YYYY HH:mm:ss');
    
    //show detail
    $(document).on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dtResults.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            populateDetailRow(row);
            tr.addClass('shown');
        }
    });	
	
    // date range filtering
    init_date_input('start_date');
    init_date_input('end_date');
    
    // populate operators
    load_operators(null, function (operators) {
        show_operators(operators);
        var d = $('<option />').prop('id', 'operator_default')
            .prop('name', 'soperator_id')
            .val('%')
            .text('- Operator -');
        $('#operator_select').prepend(d);
    });
	
    //populate the group by
    $('#inpGroupBy').empty();
    $('#inpGroupBy').append($('<option />').val('Date').text('Date'));

    function run_report() {
        var gauge_method = $('#machine_select :selected').data('gaugeMethod');
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var machine_id = $('#machine_select :selected').val();
        var cols;
        var data = {
            'url': processor,
            'type': 'POST',
            'data': {
                'action': 'get_gauge_checks',
                'gauge_method': gauge_method,
                'start_date': start_date,
                'end_date': end_date,
                'machine_id': machine_id
            },
            'dataSrc': ''
        };

        switch (gauge_method) {
            case 'manual':
                cols = [
                    { 'title': 'Details', 'className': 'details-control', 'orderable': false, 'defaultContent': '' },
                    { 'data': 'serial_number', 'title': 'Serial Number' },
                    { 'data': 'part_desc', 'title': 'Description' },
                    //{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
                    //{ 'data': 'lot_num', 'title': 'Lot Number' },
                    { 'data': 'cycle_time.date', 'title': 'Cycle Time', 'render': renderDate },
                    { 'data': 'pass_fail', 'title': 'Pass/Fail', 'render': renderBool },
                    { 'data': 'gauge_readings', 'title': 'Samples' }
                ];
                break;
            case 'auto':
                cols = [
                    { 'data': 'serial_number', 'title': 'Serial Number' },
                    { 'data': 'part_desc', 'title': 'Description' },
                    //{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
                    //{ 'data': 'lot_num', 'title': 'Lot Number' },
                    { 'data': 'cycle_time.date', 'title': 'Check Time', 'render': renderDate },
                    { 'data': 'good_part', 'title': 'Pass/Fail', 'render': renderBool },
                    { 'data': 'm1', 'title': 'm1' },
                    { 'data': 'm2', 'title': 'm2' },
                    { 'data': 'm3', 'title': 'm3' },
                    { 'data': 'm4', 'title': 'm4' },
                    { 'data': 'taper', 'title': 'Taper' },
                    { 'data': 'sp1_low', 'title': 'Sp1 Low' },
                    { 'data': 'sp1_high', 'title': 'Sp1 High' },
                    { 'data': 'sp2_low', 'title': 'Sp2 Low' },
                    { 'data': 'sp2_high', 'title': 'Sp2 High' },
                    { 'data': 'sp3_low', 'title': 'Sp3 Low' },
                    { 'data': 'sp3_high', 'title': 'Sp3 High' },
                    { 'data': 'sp4_low', 'title': 'Sp4 Low' },
                    { 'data': 'sp4_high', 'title': 'Sp4 High' },
                    { 'data': 'spt_low', 'title': 'Spt1 Low' },
                    { 'data': 'spt_high', 'title': 'Spt1 High' },
                    { 'data': 'recorded_date.date', 'title': 'Recorded', 'render': renderDate }
                ];
                break;
            case 'mahrmmq':
                cols = [
                    { 'data': 'serial_number', 'title': 'Serial Number' },
                    { 'data': 'part_desc', 'title': 'Description' },
                    //{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
                    //{ 'data': 'lot_num', 'title': 'Lot Number' },
                    { 'data': 'cycle_time.date', 'title': 'Cycle Time', 'render': renderDate },
                    { 'data': 'name', 'title': 'Name' },
                    { 'data': 'l_tol', 'title': 'l_tol' },
                    { 'data': 'u_tol', 'title': 'u_tol' },
                    { 'data': 'value', 'title': 'Value' },
                    { 'data': 'unit', 'title': 'Unit' },
                    { 'data': 'low_nat_border', 'title': 'low_nat_border' },
                    { 'data': 'type', 'title': 'type' },
                    { 'data': 'no', 'title': 'no' },
                    { 'data': 'date', 'title': 'date' },
                    { 'data': 'time', 'title': 'time' },
                ];
                break;
            case 'process':
                cols = [
                    { 'title': 'Details', 'className': 'details-control', 'orderable': false, 'defaultContent': '' },
                    { 'data': 'serial_number', 'title': 'Serial Number' },
                    //{ 'data': 'mrp_part_number', 'title': 'MRP Part Number' },
                    //{ 'data': 'lot_num', 'title': 'Lot Number' },
                    { 'data': 'part_desc', 'title': 'Description' },
                    { 'data': 'cycle_time.date', 'title': 'Check Time', 'render': renderDate }
                ];
                break;
            default:
                break;
        }

        if (dtResults != undefined) {
            var container = $('#table-gauge-results').closest('.panel-body');
            dtResults.destroy();
            container.empty();
            container.append('<table id="table-gauge-results" class="table table-condensed" />')
        }
        dtResults = $('#table-gauge-results').DataTable({
            destroy: true,
            responsive: true,
            dom: 'T<"clear">lfrtip',
            tableTools: {
                "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "portrait"
                    },
                    "print"
                ]
            },
            ajax: data,
            columns: cols
        });

    }

    function renderBool(data, type, full, meta) {
        if (data == 1) {
            return '<span class="glyphicon glyphicon-ok" />';
        } else {
            return '<span class="glyphicon glyphicon-remove" />';
        }
    }

    function renderDate(data, type, full) {
        return moment(data).format('MM/DD/YYYY HH:mm');
    }
    
    /* Formatting function for row details */
    function populateDetailRow(r) {
        var childContent = '<div class="well">' +
            '<table class="table table-striped table-condensed table-gauge-result-details"></table>' +
            '</div>';

        r.child($(childContent)).show();
        buildDetailTable(r);

    }

    function buildDetailTable(tr) {
        var gauge_method = $('#machine_select :selected').data('gaugeMethod') + '-details';
        var machine_id = $('#machine_select :selected').val();
        var sel_serial_number = tr.data().serial_number;
        var cols;
        var data = {
            'url': processor,
            'type': 'POST',
            'data': {
                'action': 'get_gauge_checks',
                'gauge_method': gauge_method,
                'machine_id': machine_id,
                'serial_number': sel_serial_number
            },
            'dataSrc': ''
        };

        switch (gauge_method) {
            case 'manual-details':
                cols = [
                    { 'data': 'instr_order', 'title': 'Order' },
                    { 'data': 'description', 'title': 'Instruction' },
                    { 'data': 'min_value', 'title': 'Min Value' },
                    { 'data': 'max_value', 'title': 'Max Value' },
                    { 'data': 'data_point_value', 'title': 'Actual' },
                    { 'data': 'units', 'title': 'Unit' }
                ]
                break;
            case 'process-details':
                cols = [
                    { 'data': 'cycle_time.date', 'title': 'Cycle Time', 'render': renderDate },
                    { 'data': 'data_point_name', 'title': 'Name' },
                    { 'data': 'tag_value', 'title': 'Value' },
                    { 'data': 'units', 'title': 'Unit' }
                ];
                break;
            case 'auto-details':
                // autogauge has no detail
                break;
            case 'mahrmmq-details':
                // mahrmmq has no detail
                break;
            default:
                break;

        }

        // if (dtResultDetails != undefined) {
        //     dtResultDetails.destroy();
        // }
        dtResultDetails = tr.child().find('.table-gauge-result-details').DataTable({
            destroy: true,
            responsive: true,
            ajax: data,
            columns: cols,
            dom: 'T<"clear">lfrtip',
            tableTools: {
                "sSwfPath": "../common/library/js/dataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sPdfOrientation": "portrait"
                    },
                    "print"
                ]
            }
        });
    }

});