var processor = './custom/meb/reports/production_summary/production_summary.php';
var detailTable, data;
var column_titles = [
	{name: 'shift_desc', value: 'Shift', display: true},
	{name: 'is_day_start', value: 'No Display', display: false},
	{name: 'prod_date', value: 'Production Date', display: true},
	{name: 'interval_desc', value: 'Interval', display: true},
	{name: 'quantity', value: 'Quantity', display: true},
	{name: 'interval_desc', value: 'Hour', display: true},
	{name: 'interval_start_time', value: 'Start', display: true},
	{name: 'interval_end_time', value: 'End', display: true}
];

function shart_options() {
	return {
		chart: {
			type: 'column',
			renderTo: 'chart-id'
		},
		title: {
			text: 'Chart Title'
		},
		xAxis: {
			title: {
				text: ''
			},
			categories: []
		},
		yAxis: {
			min: 0,
			title: {
				text: ''
			},
			stackLabels: {
				enabled: true,
				style: {
					fontWeight: 'bold',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				}
			}
		},
		legend: {
			align: 'right',
			x: -30,
			verticalAlign: 'top',
			y: 25,
			floating: true,
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
			borderColor: '#CCC',
			borderWidth: 1,
			shadow: false
		},
		tooltip: {
			headerFormat: '<b>{point.x}</b><br/>',
			pointFormat: '{series.name}: <b>{point.y}</b><br/>Total: <b>{point.stackTotal}</b>'
		},
		plotOptions: {
			column: {
				stacking: 'normal',
				dataLabels: {
					enabled: true,
					color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
					style: {
						textShadow: '0 0 3px black'
					}
				}
			}
		},
		credits: {
			enabled: false
		},
		series: [{name: 'No Data', data: []}]
	};
}

$(document).ready(function () {

	NYSUS.REPORTS.REPORT_FILTER.display_element = '#panel-filter .panel-body';
	NYSUS.REPORTS.REPORT_FILTER.render();
	var btn_run = $('.btn-run-report');
	btn_run.on('click', run_report);
	$(document).on('running', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-time"></span><b>Running...</b>');
		btn_run.prop("disabled", true);
	});
	$(document).on('complete', btn_run, function () {
		btn_run.html('<span class="glyphicon glyphicon-play-circle"></span><b>Run Report</b>');
		btn_run.prop("disabled", false);
	});

});

function run_report() {
	data = NYSUS.REPORTS.REPORT_FILTER.get_selections();

	$(document).trigger('running');

	populate_daily_chart();
	populate_hourly_chart();
	populate_production();

}

function populate_daily_chart() {
	var chart1;
	var chart_options = shart_options();

	chart_options.title.text = 'Daily Production';
	chart_options.xAxis.title.text = 'Production Date';
	chart_options.yAxis.title.text = 'Parts';
	chart_options.chart.renderTo = 'daily-chart';

	data.action = 'get_daily_production';
	$.getJSON(processor, data, function (jso) {
		if (jso.series && jso.series.length > 0) {
			chart_options.xAxis.categories = jso.categories;
			chart_options.series = jso.series;

			var chart1 = new Highcharts.Chart(chart_options);
		} else {
			chart_options.title.text = 'No Daily Production';
			chart1 = new Highcharts.Chart(chart_options);
		}
	});

}

function populate_hourly_chart() {
	var chart2;
	var chart_options = shart_options();

	chart_options.title.text = 'Hourly Production Averages';
	chart_options.xAxis.title.text = 'Production Interval';
	chart_options.yAxis.title.text = 'Average Hourly Parts';
	chart_options.chart.renderTo = 'hourly-chart';
	chart_options.chart.type = 'line';
	chart_options.tooltip.pointFormat = '{series.name}: <b>{point.y}</b>';

	data.action = 'get_hourly_production';
	$.getJSON(processor, data, function (jso) {
		if (jso.series) {
			chart_options.xAxis.categories = jso.categories;
			chart_options.series = jso.series;

			chart2 = new Highcharts.Chart(chart_options);
		}
	});

}

function populate_production() {
	$('#panel-data .panel-body').empty();

	data.action = 'get_production';
	$.getJSON(processor, data, function (jso) {
		if (jso) {
			if (jso.data && jso.data.length > 0) {
				populate_production_table(jso.data);
			} else {
				$('#panel-data .panel-body').append('<h3>No Results</h3>');
			}
			if (jso.chart && jso.chart.categories.length > 0) {
				populate_production_chart(jso.chart);
			} else {
				populate_production_chart({categories: [], series: []});
			}
		}
		$(document).trigger('complete');
	});
}

function populate_production_chart(jso) {
	var chart3;
	var chart_options = shart_options();

	chart_options.title.text = 'Production Summary';
	chart_options.xAxis.title.text = 'Production Interval';
	chart_options.yAxis.title.text = 'Parts';
	chart_options.chart.renderTo = 'summary-chart';
	chart_options.chart.type = 'column';
	//chart_options.tooltip.pointFormat = '{series.name}: <b>{point.y}<b><br/>Shift: <b>{series.userOptions.shift_desc}</b>';
	chart_options.tooltip = {
		formatter: function () {
			return this.series.name + ': <b>' + this.y + '</b><br/>' +
				'Shift: <b>' + this.key + '</b>';
		}
	};
	chart_options.plotOptions.column.stacking = '';

	chart_options.xAxis.categories = jso.categories;
	chart_options.series = jso.series;

	chart3 = new Highcharts.Chart(chart_options);

}

function populate_production_table(jso) {
	if (detailTable != null) {
		detailTable.destroy();
	}

	var cols = [];
	var objs = Object.getOwnPropertyNames(jso[0]);
	$('#panel-data .panel-body').append('<table id="production-table" class="table table-striped table-condensed table-responsive"><thead></thead><tbody></tbody></table>');
	var table = $('#production-table');

	//cols.push({'className': 'details-control', 'orderable': false, 'data': null, 'defaultContent': ''});
	$.map(objs, function (item) {
		var i = column_titles.inArray(item, 'name'); // see if its in the column map
		var tit = item;
		if (i >= 0) {
			tit = column_titles[i].value;

			if (column_titles[i].display) {
				cols.push({data: item, title: tit, sortable: item == 'prod_date' ? false : true});
			}
		}
	});

	detailTable = table.DataTable({
		"bAutoWidtch":false,
        dom: 'T<"clear">lfBrtip',
		width: "100%",
        buttons: [
                'copyHtml5',
                {
                    extend: 'excelHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                {
                    extend: 'csvHtml5',
                    //title: $('#machine_select option:selected').html()+' Param Data - '+get_date_value('start_date')
                },
                'pdfHtml5'
            ],
		data: jso,
        columns: cols
	});

}

Array.prototype.inArray = function (searchFor, property) {
	var retVal = -1;
	var self = this;
	for (var index = 0; index < self.length; index++) {
		var item = self[index];
		if (item.hasOwnProperty(property)) {
			if (item[property].toLowerCase() === searchFor.toLowerCase()) {
				retVal = index;
				return retVal;
			}
		}
	}
	return retVal;
};