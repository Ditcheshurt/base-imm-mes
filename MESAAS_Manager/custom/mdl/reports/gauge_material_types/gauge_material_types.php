<?php

require_once("../../../../ajax_processors/init.php");

//*********** Request Variables *********//
$action = isset($_REQUEST['action']) ? fixDB($_REQUEST['action']) : null;
$gauge_method = isset($_REQUEST['gauge_method']) ? fixDB($_REQUEST['gauge_method']) : null;
$start_date = isset($_REQUEST['start_date']) ? fixDB($_REQUEST['start_date']) : null;
$end_date = isset($_REQUEST['end_date']) ? fixDB($_REQUEST['end_date']) : null;
$machine_id = isset($_REQUEST['machine_id']) ? fixDB($_REQUEST['machine_id']) : null;
$gauge_method = isset($_REQUEST['gauge_method']) ? fixDB($_REQUEST['gauge_method']) : null;
$shift_id = isset($_REQUEST['shift_id']) ? fixDB($_REQUEST['shift_id']) : null;
$serial_number = isset($_REQUEST['serial_number']) ? fixDB($_REQUEST['serial_number']) : null;

// ********** SQL Queries ***********// 
$sql_manual_gauge_checks = "
    SELECT
        MAX(mc.lotnum) AS lot_num
        , MAX(mc.cycle_time) AS cycle_time
        , MAX(mcp.serial_number) AS serial_number
        , MAX(p.part_number) AS part_number
        , MAX(p.mrp_part_number)
        , MAX(p.part_desc) AS part_desc
        , MAX(CASE WHEN gmcp.data_point_value >= gmti.min_value AND gmcp.data_point_value <= gmti.max_value THEN 1 ELSE 0 END) AS pass_fail
        , COUNT(gmcp.data_point_value) AS gauge_readings
    FROM 
        [dbo].[machine_cycles] mc
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
        JOIN [dbo].[gauge_machine_cycle_parts] gmcp ON mcp.ID = gmcp.machine_cycle_part_ID
        JOIN [dbo].[gauge_material_types_instructions] gmti ON gmti.ID = gmcp.gauge_material_types_instruction_ID
    WHERE
        m.ID = $machine_id
    AND 
        [dbo].getProdDate(mc.cycle_time) >= '$start_date'
    AND 
        [dbo].getProdDate(mc.cycle_time) <= '$end_date'
    GROUP BY
        serial_number";

$sql_manual_gauge_check_details = "
    SELECT
        t.short_description
        ,p.part_number
        ,gmti.is_pass_fail
        ,gmti.instr_order
        ,gmti.description
        ,gmti.min_value
        ,gmti.max_value
        ,gmcp.data_point_value
        ,gmti.units AS units
    FROM 
        [dbo].[machine_cycles] mc
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
        JOIN [dbo].[gauge_machine_cycle_parts] gmcp ON mcp.ID = gmcp.machine_cycle_part_ID
        JOIN [dbo].[gauge_material_types_instructions] gmti ON gmti.ID = gmcp.gauge_material_types_instruction_ID
    WHERE
        m.ID = $machine_id
        AND mcp.serial_number = '$serial_number'
    ORDER BY 
        gmti.instr_order";

$sql_auto_gauge_checks = "
    SELECT
        mc.lotnum AS lot_num
        ,mc.cycle_time
        ,a.serial_number
        ,p.part_number
        ,p.mrp_part_number
        ,p.part_desc
        ,a.[cycle_ID]
        ,[part_name]
        ,[m1]
        ,[m2]
        ,[m3]
        ,[m4]
        ,[taper]
        ,[sp1_low]
        ,[sp1_high]
        ,[sp2_low]
        ,[sp2_high]
        ,[sp3_low]
        ,[sp3_high]
        ,[sp4_low]
        ,[sp4_high]
        ,[spt_low]
        ,[spt_high]
        ,[good_part]
        ,[recorded_date]
    FROM 
		[dbo].[gauge_autogauge] a
        JOIN [dbo].[machine_cycles] mc ON a.cycle_ID = mc.ID
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
    WHERE
        m.ID = $machine_id
        AND 
            [dbo].getProdDate(mc.cycle_time) >= '$start_date'
        AND 
            [dbo].getProdDate(mc.cycle_time) <= '$end_date'";

$sql_mahrmmq_gauge_checks = "
    SELECT 
        mc.lotnum AS lot_num
        ,mc.cycle_time
        ,mcp.serial_number
        ,p.part_number
        ,p.mrp_part_number
        ,p.part_desc
        ,mmq.[name]
        ,mmq.[l_tol]
        ,mmq.[u_tol]
        ,mmq.[value]
        ,mmq.[unit]
        ,mmq.[low_nat_border]
        ,mmq.[type]
        ,mmq.[no]
        ,mmq.[date]
        ,mmq.[time]
    FROM 
        [dbo].[machine_cycles] mc
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
        JOIN [dbo].[gauge_mahrmmq] mmq ON mmq.machine_cycle_part_ID = mc.ID
    WHERE
        m.ID = $machine_id
        AND 
            [dbo].getProdDate(mc.cycle_time) >= '$start_date'
        AND 
            [dbo].getProdDate(mc.cycle_time) <= '$end_date'";
            
$sql_process_gauge_checks = "
    SELECT 
        mc.lotnum AS lot_num
        ,MAX(mc.cycle_time) AS cycle_time
        ,mcp.serial_number
        ,p.part_number
        ,p.part_desc
        ,p.mrp_part_number
    FROM 
        [dbo].[machine_cycles] mc
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID    
    WHERE
        m.ID = $machine_id                    
        AND 
            [dbo].getProdDate(mc.cycle_time) >= '$start_date'
        AND 
            [dbo].getProdDate(mc.cycle_time) <= '$end_date'
    GROUP BY
        mc.lotnum, p.part_number, p.part_desc, mcp.serial_number, p.mrp_part_number";

$sql_process_gauge_check_details = "
    SELECT 
        MAX(mc.cycle_time) AS cycle_time
        ,p.part_number
        ,p.part_desc
        ,v.serial AS serial_number
        ,v.machine_ID
        ,s.data_point_name
        ,v.tag_value
        ,s.units
    FROM 
        [dbo].[machine_cycles] mc
        JOIN [dbo].[machine_cycle_parts] mcp ON mc.ID = mcp.cycle_ID
        JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
        JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
        JOIN [dbo].[parts] p ON p.ID = tp.part_ID
        JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
        JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
        JOIN [NYSUS_REPETITIVE_MES].[dbo].[machine_cycle_parts_plc_values] v ON v.serial = mcp.serial_number
        JOIN [NYSUS_PLC_POLLER].[dbo].[nysus_plc_tags] pt ON pt.name = v.tag_name  
        JOIN [NYSUS_REPETITIVE_MES].[dbo].[data_point_setup] s ON s.ID = pt.description2    
    WHERE
        m.ID = $machine_id
        AND 
            mcp.serial_number = '$serial_number' 
    GROUP BY
        p.part_number, p.part_desc,v.serial, v.machine_ID, s.data_point_name, v.tag_value, s.units";

call_user_func($action);

//*********** Functions ************//
function get_gauge_checks() {
    // get the global vars
    global $db, $gauge_method, $sql_process_gauge_checks,
        $sql_mahrmmq_gauge_checks, $sql_manual_gauge_checks, 
        $sql_manual_gauge_check_details,
        $sql_auto_gauge_checks, $sql_process_gauge_check_details;
    $res = new stdClass();
    $query = '';

    switch ($gauge_method) {
        case 'manual':
            $query = $sql_manual_gauge_checks;
            break;
        case 'manual-details':
            $query = $sql_manual_gauge_check_details;
            break;
        case 'process':
            $query = $sql_process_gauge_checks;
            break;
        case 'process-details':
            $query = $sql_process_gauge_check_details;
            break;
        case 'auto':
            $query = $sql_auto_gauge_checks;
            break;
        case 'mahrmmq':
            $query = $sql_mahrmmq_gauge_checks;
            break;
        default:
            die('{\"error\":\"No gauge_method defined\"}');
            break;
    }
    try {
        $res = $db->query($query);
        if (count($res) > 0) {
            echo json_encode($res);
        } else {
            echo '{}';
        }

    } catch (Exception $e) {
        echo $e->getMessage();
    }

}

?>