<?php

require_once("../../../../ajax_processors/init.php");

$action = $_REQUEST['action'];

call_user_func($action, $db);

function get_fault_chart_data($db) {

	$result_type = $_REQUEST["result_type"];
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];
	$top = "";
	$select_fields = array();
	$group_by = array();
	$order_by = array();

	if ($result_type == "count") {
		array_push($select_fields, "COUNT(fh.ID) AS value");
	} else {
		array_push($select_fields, "SUM(fh.duration) AS value");
	}

	$where = "WHERE CONVERT(DATE, dbo.getProdDate(fh.date_time), 113) BETWEEN '$start_date' AND '$end_date'";

	if (isset($_REQUEST["system_ID"]) && $_REQUEST["system_ID"] != null) {
		$where .= " AND m.system_ID = " . $_REQUEST["system_ID"];
	}
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND MES_COMMON.dbo.fn_getShift(m.system_ID, fh.date_time) IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}
	if (isset($_REQUEST["plcs"]) && $_REQUEST["plcs"] != null) {
		$where .= " AND p.ID IN (" . implode(",", $_REQUEST["plcs"]) . ")";
	}
	if (isset($_REQUEST["fault_codes"]) && $_REQUEST["fault_codes"] != null) {
		$where .= " AND fc.ID IN (" . implode(",", $_REQUEST["fault_codes"]) . ")";
	}
	if (isset($_REQUEST["top"]) && $_REQUEST["top"] != null) {
		$top = "TOP " . $_REQUEST["top"];
		if (isset($_REQUEST["top_by"]) && $_REQUEST["top_by"] == "count") {
			array_push($order_by, "COUNT(fh.id) desc");
		} else {
			array_push($order_by, "SUM(fh.duration) desc");
		}
	}

	if (isset($_REQUEST["group_by"]) && $_REQUEST["group_by"] != null) {
//		foreach($_REQUEST["group_by"] as $value) {
			switch($_REQUEST["group_by"]) {
				case "Date":
					array_push($select_fields, "CONVERT(VARCHAR(13), fh.date_time, 101) AS category");
					array_push($select_fields, "CONVERT(VARCHAR(13), fh.date_time, 101) AS name");
					array_push($group_by, "CONVERT(VARCHAR(13), fh.date_time, 101)");
					break;
				case "Interval":
					array_push($select_fields, "ti.interval_desc AS category");
					array_push($select_fields, "ti.interval_desc AS name");
					array_push($select_fields, "ti.interval_start_time");
					array_push($group_by, "ti.interval_desc");
					array_push($group_by, "ti.interval_start_time");
					array_push($order_by, "ti.interval_start_time asc");
					break;
				case "Machine":
					array_push($select_fields, "m.machine_name AS category");
					array_push($select_fields, "m.machine_name AS name");
					array_push($group_by, "m.machine_name");
					break;
				case "Fault Code":
					array_push($select_fields, "fc.description AS category");
					array_push($select_fields, "fc.description AS name");
					array_push($group_by, "fc.description");
					break;
				default:
					break;
			}
//		}
	}

	if (count($group_by) < 1) {
		array_push($select_fields, "m.machine_name AS category");
		array_push($select_fields, "fc.description AS name");
		array_push($group_by, "m.machine_name");
		array_push($group_by, "fc.description");
		array_push($group_by, "fc.description");
	}

	// check for alt_PLC_ID column in machines table
	$sql = "SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'machines'
				AND TABLE_SCHEMA='dbo'
				AND COLUMN_NAME = 'alt_PLC_ID'";
	$result = $db->query($sql);

	$sql_alt_plc_ids = "";
	if (count($result) > 0) {
		$sql_alt_plc_ids = " OR m.alt_PLC_ID = p.ID ";
	}

	$sql = "SELECT $top	" . implode(",", $select_fields) . "
			FROM
				fault_history fh
			JOIN
				fault_codes fc ON fh.fault_id = fc.id
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON fc.tag_ID = t.ID
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plcs p ON t.plc_ID = p.ID
			JOIN
				machines m ON m.PLC_ID = p.ID $sql_alt_plc_ids
			JOIN
				MES_COMMON.dbo.time_intervals ti ON CONVERT(time, fh.date_time) BETWEEN ti.interval_start_time AND ti.interval_end_time
			JOIN
				MES_COMMON.dbo.shift_time_intervals sti ON sti.time_interval_ID = ti.ID
			JOIN
				MES_COMMON.dbo.shifts s ON sti.shift_ID = s.ID
			$where
			GROUP BY " . implode(",", $group_by);

	if (count($order_by) > 0) {
		$sql .= " ORDER BY " . implode(",", $order_by);
	}
//die($sql);
	$res = $db->query($sql);
	if ($res == null) {
		die(json_encode(array()));
	}
	echo json_encode(format_to_highchart($res));

}

function get_group_by($db) {
	$result = array('Date', 'Interval', 'Machine', 'Fault Code');

	echo json_encode($result);
}

/**
 * formats a result to what highcharts expects
 * required: shift_desc, category, interval_desc, quantity fields
 * @param $result
 * @return array
 */
function format_to_highchart($result)
{
	$series = array();
	$series["name"] = $result[0]["category"];
	$series["data"] = array();

	foreach ($result as &$value) {
		$arr = array($value["name"], $value["value"]);
		array_push($series["data"], $arr);
	}

	return $series;

}

function get_fault_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	$where = "WHERE CONVERT(DATE, dbo.getProdDate(fh.date_time), 113) BETWEEN '$start_date' AND '$end_date'";

	if (isset($_REQUEST["system_ID"]) && $_REQUEST["system_ID"] != null) {
		$where .= " AND m.system_ID = " . $_REQUEST["system_ID"];
	}
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND MES_COMMON.dbo.fn_getShift(m.system_ID, fh.date_time) IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}
	if (isset($_REQUEST["plcs"]) && $_REQUEST["plcs"] != null) {
		$where .= " AND p.ID IN (" . implode(",", $_REQUEST["plcs"]) . ")";
	}
	if (isset($_REQUEST["fault_codes"]) && $_REQUEST["fault_codes"] != null) {
		$where .= " AND fc.ID IN (" . implode(",", $_REQUEST["fault_codes"]) . ")";
	}

	// check for alt_PLC_ID column in machines table
	$sql = "SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'machines'
				AND TABLE_SCHEMA='dbo'
				AND COLUMN_NAME = 'alt_PLC_ID'";
	$result = $db->query($sql);

	$sql_alt_plc_ids = "";
	if (count($result) > 0) {
		$sql_alt_plc_ids = " OR m.alt_PLC_ID = p.ID ";
	}

	$sql = "SELECT
				m.machine_name AS machine,
				fc.description,
				fh.date_time AS [date],
				fh.duration
			FROM
				fault_history fh
			JOIN
				fault_codes fc ON fh.fault_id = fc.id
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON fc.tag_ID = t.ID
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plcs p ON t.plc_ID = p.ID
			JOIN
				machines m ON m.PLC_ID = p.ID $sql_alt_plc_ids
			JOIN
				MES_COMMON.dbo.time_intervals ti ON CONVERT(time, fh.date_time) BETWEEN ti.interval_start_time AND ti.interval_end_time
			JOIN
				MES_COMMON.dbo.shift_time_intervals sti ON sti.time_interval_ID = ti.ID
			JOIN
				MES_COMMON.dbo.shifts s ON sti.shift_ID = s.ID
			$where";

//die($sql);
	$res = $db->query($sql);
	if ($res == null) {
		die(json_encode(array()));
	}
	echo json_encode($res);
}