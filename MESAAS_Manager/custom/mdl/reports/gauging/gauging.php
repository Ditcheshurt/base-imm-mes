<?php

require_once("../../../../ajax_processors/init.php");

$action = isset($_REQUEST['action']) ? fixDB($_REQUEST['action']) : null;

call_user_func($action, $db);

function get_data($db)
{
	$system_ID = isset($_REQUEST['system_id']) ? fixDB($_REQUEST['system_id']) : null;
	$start_date = isset($_REQUEST['start_date']) ? fixDB($_REQUEST['start_date']) : null;
	$end_date = isset($_REQUEST['end_date']) ? fixDB($_REQUEST['end_date']) : null;
    $gauge_material_type_ID = isset($_REQUEST['gauge_material_type_ID']) ? fixDB($_REQUEST['gauge_material_type_ID']) : null;
//	$shifts = isset($_REQUEST['shifts']) ? fixDB($_REQUEST['shifts']) : null;

    if ($gauge_material_type_ID == null) {
        $res = array();
        $arr["Result"] = "No Gauge Type Selected";
        array_push($res, $arr);
        die(json_encode($res));
    }

	$sql = "DECLARE @cols AS NVARCHAR(MAX),
            @query  AS NVARCHAR(MAX)
            SELECT @cols = STUFF((SELECT ',' + QUOTENAME(REPLACE(i.description, '.', ''))
                        FROM gauge_material_types_instructions i
                        JOIN gauge_material_types t ON i.gauge_material_type_ID = t.ID
                        WHERE t.ID = $gauge_material_type_ID
                        GROUP BY i.instr_order, i.description
                        ORDER BY i.instr_order, i.description
                FOR XML PATH(''), TYPE
                ).value('.', 'NVARCHAR(MAX)')
            ,1,1,'')

            set @query = 'SELECT * FROM
                         (
                            SELECT o.last_name + '', '' + o.first_name AS operator, mcp.serial_number, p.sample_group, DATEADD(ms, -DATEPART(ms, p.recorded_date), p.recorded_date) AS recorded_date,
								REPLACE(i.description, ''.'', '''') AS description, data_point_value
                            FROM gauge_machine_cycle_parts p
                            JOIN machine_cycle_parts mcp ON mcp.ID = p.machine_cycle_part_ID
                            JOIN gauge_material_types_instructions i on i.id = p.gauge_material_types_instruction_ID
                            JOIN gauge_material_types t ON i.gauge_material_type_ID = t.ID
                            JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
									 JOIN machines m ON m.ID = mc.machine_ID
                            LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = p.operator_ID
                            WHERE t.ID = $gauge_material_type_ID
                            AND m.system_ID = $system_ID
                            AND p.recorded_date BETWEEN ''$start_date'' AND ''$end_date 23:59:59.99''
							GROUP BY mcp.serial_number, p.sample_group, p.recorded_date, i.description, data_point_value, o.last_name, o.first_name
                        ) x
                        pivot
                        (
                            SUM(data_point_value)
                            FOR description IN (' + @cols + ')
                        ) p '

            execute(@query);";
//die($sql);
	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}
	echo json_encode($res);
}

function get_gauge_types($db) {
	$system_ID = isset($_REQUEST['system_ID']) ? fixDB($_REQUEST['system_ID']) : null;

	$sql = "SELECT DISTINCT
              t.ID,
              t.description
            FROM
              gauge_material_types t
              JOIN machines m ON m.station_IP_address = t.station_ip_address
            WHERE
            	m.system_ID = $system_ID
            ORDER BY description";

    $res = $db->query($sql);

    if ($res == null) {
        $res = array();
    }
    echo json_encode($res);
}