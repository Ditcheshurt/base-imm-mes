<?php

require_once("../../../../ajax_processors/init.php");

//*********** Request Variables *********//
$action = isset($_REQUEST['action']) ? fixDB($_REQUEST['action']) : null;

call_user_func($action, $db);

function get_data($db) {
    $start_date = isset($_REQUEST['start_date']) ? fixDB($_REQUEST['start_date']) : null;
    $end_date = isset($_REQUEST['end_date']) ? fixDB($_REQUEST['end_date']) : null;
    $machines = isset($_REQUEST['machines']) ? fixDB($_REQUEST['machines']) : null;
    $operators = isset($_REQUEST['operators']) ? fixDB($_REQUEST['operators']) : null;
    $shifts = isset($_REQUEST['shifts']) ? fixDB($_REQUEST['shifts']) : null;

    $sql = "SELECT
                mc.lotnum AS lot_num
                ,mc.cycle_time
                ,mcp.serial_number
                ,p.part_number
                ,p.mrp_part_number
                ,p.part_desc
                ,mmq.[name]
                ,mmq.[l_tol]
                ,mmq.[u_tol]
                ,mmq.[value]
                ,mmq.[unit]
                ,mmq.[low_nat_border]
                ,mmq.[type]
                ,mmq.[no]
                ,mmq.[date]
                ,mmq.[time]
                ,mmq.created_date
                ,o.last_name + ', ' + o.first_name AS operator
            FROM
                [dbo].[gauge_mahrmmq] mmq
                JOIN [dbo].[machine_cycle_parts] mcp ON mmq.machine_cycle_part_ID = mcp.ID
                JOIN [dbo].[machine_cycles] mc ON mcp.cycle_ID = mc.ID
                JOIN [dbo].[tool_parts] tp ON tp.ID = mcp.tool_part_ID
                JOIN [dbo].[tools] t ON t.ID = tp.tool_ID
                JOIN [dbo].[parts] p ON p.ID = tp.part_ID
                JOIN [dbo].[machine_tools] mt ON t.ID = mt.tool_ID
                JOIN [dbo].[machines] m ON m.ID = mt.machine_ID
                JOIN [MES_COMMON].[dbo].[operators] o ON o.ID = mmq.operator_ID
            WHERE
                [dbo].getProdDate(mmq.created_date) BETWEEN '$start_date' AND '$end_date 23:59:59.99'";

    if ($machines != null) {
        $sql .= "AND m.ID IN (" . implode(", ", $machines) . ")";
    }
    if ($operators != null) {
        $sql .= "AND mc.operator_ID IN (" . implode(", ", $operators) . ")";
    }
    if ($shifts != null) {
        $sql .= "AND s.ID IN (" . implode(", ", $shifts) . ")";
    }
//die($sql);
    $res = $db->query($sql);

    if ($res == null) {
        $res = array();
    }
    echo json_encode($res);
}