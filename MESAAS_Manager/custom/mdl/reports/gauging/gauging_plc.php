<?php

require_once("../../../../ajax_processors/init.php");

$action = isset($_REQUEST['action']) ? fixDB($_REQUEST['action']) : null;

call_user_func($action, $db);

function get_data($db)
{
//	$system_ID = isset($_REQUEST['system_id']) ? fixDB($_REQUEST['system_id']) : null;
	$start_date = isset($_REQUEST['start_date']) ? fixDB($_REQUEST['start_date']) : null;
	$end_date = isset($_REQUEST['end_date']) ? fixDB($_REQUEST['end_date']) : null;
	$machine_ID = isset($_REQUEST['machine_ID']) ? $_REQUEST['machine_ID'] : null;
	$parts = isset($_REQUEST['parts']) ? fixDB($_REQUEST['parts']) : null;
//	$shifts = isset($_REQUEST['shifts']) ? fixDB($_REQUEST['shifts']) : null;

    if ($machine_ID == null) {
        $res = array();
        $arr["Result"] = "No Machine Selected";
        array_push($res, $arr);
        die(json_encode($res));
    }

    $where_parts = "";
	if ($parts != null) {
		if (is_array($parts)) {
			$where_parts = " AND tp.part_ID IN (" . implode(",", $parts) . ")";
		} else {
			$where_parts = " AND tp.part_ID = $parts";
		}
	}

	$sql = "DECLARE @cols AS NVARCHAR(MAX),
			@query  AS NVARCHAR(MAX)
			SELECT @cols = STUFF((SELECT DISTINCT ',' + QUOTENAME(dps.data_point_name)
			            FROM machine_cycle_parts_plc_values v
			            JOIN NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON t.name = tag_name
						JOIN data_point_setup dps ON dps.ID = t.description2
			            WHERE v.machine_ID = $machine_ID
			            ORDER BY 1
			    FOR XML PATH(''), TYPE
			    ).value('.', 'NVARCHAR(MAX)')
			,1,1,'')

			set @query = 'SELECT * FROM
			                (
			                SELECT
			                	o.last_name + '', '' + o.first_name AS operator,
			                    v.serial,
			                    CONVERT(datetime, mc.cycle_time, 101) AS recorded_date,
			                    dps.data_point_name AS description,
			                    v.tag_value
			                FROM machine_cycle_parts_plc_values v
							JOIN NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON t.name = tag_name
							JOIN data_point_setup dps ON dps.ID = t.description2
			                JOIN machines m ON m.ID = v.machine_ID
			                JOIN machine_cycle_parts mcp ON mcp.serial_number = v.serial
			                JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
			                JOIN tool_parts tp ON mcp.tool_part_ID = tp.ID							
							JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
			                WHERE v.machine_ID = $machine_ID
			                $where_parts
			                AND mc.cycle_time BETWEEN ''$start_date'' AND ''$end_date 23:59:59.99''
			                GROUP BY v.serial, CONVERT(datetime, mc.cycle_time, 101), dps.data_point_name, v.tag_value, o.last_name, o.first_name
			            ) x
			            pivot
			            (
			                SUM(tag_value)
			                FOR description IN (' + @cols + ')
			            ) p '
			execute(@query);";
    //die($sql);
	$res = $db->query($sql);

	if ($res == null) {
		$res = array();
	}
	echo json_encode($res);
}