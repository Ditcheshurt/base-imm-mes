<?php

require_once("../../../../ajax_processors/init.php");

/**
 * require_once the appropriate processor
 */
$process_type = $_REQUEST["process_type"];
switch (strtolower($process_type)) {
	case "repetitive":
		require_once(dirname(__FILE__)."\\production_summary_repetitive.php");
		break;
	case "sequenced":
		require_once(dirname(__FILE__)."\\production_summary_sequence.php");
		break;
	case "batch":
		require_once(dirname(__FILE__)."\\production_summary_batch.php");
		break;
	default:
		break;
}

call_user_func($_REQUEST["action"], $db);

/**
 * formats a result to what highcharts expects
 * required: shift_desc, category, prod_date, quantity fields
 * @param $result
 * @return array
 */
function format_daily_prod_to_highchart($result)
{
	// format result for highchart
	$categories = array();
	$shifts = array();
	foreach ($result as &$value) {
		// push to categories (distinct prod_dates)
		if (!in_array($value["category"], $categories)) {
			array_push($categories, $value["category"]);
		}

		// create shift if it doesnt exist
		$exists = false;
		foreach ($shifts as &$shift) {
			if ($shift["name"] == $value["shift_desc"]) {
				$exists = true;
			}
		}
		if (!$exists) {
			array_push($shifts, array("name" => $value["shift_desc"], "data" => array()));
		}

		// push the qty to the shifts data
		foreach ($shifts as &$shift) {
			if ($shift["name"] == $value["shift_desc"]) {
				array_push($shift["data"], intval($value["quantity"]));
				break;
			}
		}

	}
	return array("categories" => $categories, "series" => $shifts);
}

/**
 * formats a result to what highcharts expects
 * required: shift_desc, category, interval_desc, quantity fields
 * @param $result
 * @return array
 */
function format_hourly_prod_to_highchart($result)
{
	// format result for highchart
	$categories = array();
	$qtys = array();
	$series = array();

	foreach ($result as &$value) {
		// push to categories (distinct prod_dates)
		if (!in_array($value["category"], $categories)) {
			array_push($categories, $value["category"]);
		}

		array_push($qtys, intval($value["quantity"]));

	}
	array_push($series, array("name" => "Quantity", "data" => $qtys));
	return array("categories" => $categories, "series" => $series);
}

/**
 * formats a result to what highcharts expects
 * required: prod_date, interval_desc, quantity
 * @param $result
 * @return array
 */
function format_prod_to_highchart($result)
{
	// format result for highchart
	$categories = array();
	$prod_dates = array();
	foreach ($result as &$value) {
		// push to categories (distinct prod_dates)
		if (!in_array($value["interval_desc"], $categories)) {
			array_push($categories, $value["interval_desc"]);
		}

		// create prod_date if it doesnt exist
		$exists = false;
		foreach ($prod_dates as &$prod_date) {
			if ($prod_date["name"] == $value["prod_date"]) {
				$exists = true;
			}
		}
		if (!$exists) {
			array_push($prod_dates, array("name" => $value["prod_date"], "data" => array()));
		}

		// push the qty to the data
		foreach ($prod_dates as &$prod_date) {
			if ($prod_date["name"] == $value["prod_date"]) {
				array_push($prod_date["data"], array("y" => intval($value["quantity"]), "name" => $value["shift_desc"]));
				break;
			}
		}

	}
	return array("categories" => $categories, "series" => $prod_dates);
}