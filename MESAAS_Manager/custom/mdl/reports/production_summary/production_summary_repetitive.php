<?php
/**
 * Created by IntelliJ IDEA.
 * User: sdaviduk
 * Date: 3/24/2016
 * Time: 1:55 PM
 */

function get_production($db)
{
	$system_ID = $_REQUEST["system_ID"];
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	$where = "WHERE 1=1 ";
	$where2 = "WHERE s.system_ID = $system_ID";

	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND tp.tool_ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["parts"]) && $_REQUEST["parts"] != null) {
		$where .= " AND tp.part_ID IN (" . implode(",", $_REQUEST["parts"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where2 .= " AND s.ID IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "WITH show_days AS (
				SELECT CAST('$start_date' AS DATETIME) AS dt
				UNION ALL
				SELECT DATEADD(dd, 1, dt)
					FROM show_days s
				WHERE DATEADD(dd, 1, dt) <= CAST('$end_date' AS DATETIME)
			)
			SELECT
				sti.is_day_start,
				CONVERT(VARCHAR(13), CONVERT(DATE, dt, 113)) AS prod_date,
				s.shift_desc,
				ti.interval_desc,
				CONVERT(VARCHAR(8), MAX(ti.interval_start_time)) AS interval_start_time,
				CONVERT(VARCHAR(8), MAX(ti.interval_end_time)) AS interval_end_time,
				COUNT(cycles.cycle_time) AS quantity
			FROM
				show_days d
			CROSS APPLY
				MES_COMMON.dbo.shifts s
			JOIN
				MES_COMMON.dbo.shift_time_intervals sti ON s.ID = sti.shift_ID
			JOIN
				MES_COMMON.dbo.time_intervals ti ON ti.ID = sti.time_interval_ID
			LEFT JOIN
			(
				SELECT m.system_ID, mc.cycle_time, o.ID AS operator_ID, mcp.tool_part_ID
					FROM
						machines m
					JOIN
						machine_cycles mc ON m.ID = mc.machine_ID
					JOIN
						machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
					JOIN
						tool_parts tp ON tp.ID = mcp.tool_part_ID
					LEFT JOIN
						MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
					$where
				) AS cycles ON
					CONVERT(DATE, dbo.getProdDate(cycles.cycle_time), 113) = d.dt
					AND CONVERT(time, cycles.cycle_time) BETWEEN ti.interval_start_time AND ti.interval_end_time
				$where2
				GROUP BY
					dt, is_day_start, s.shift_desc, interval_desc
				ORDER BY
					prod_date, is_day_start desc, interval_start_time, interval_end_time";
//die($sql);
	$data = $db->query($sql);

	$result = array();
	$result["data"] = $data;
	$result["chart"] = format_prod_to_highchart($data);

	echo json_encode($result);
}

function get_daily_production($db)
{
	$system_ID = $_REQUEST["system_ID"];
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

	// build the where clause
	$where = "WHERE s.system_ID = $system_ID AND m.system_ID = $system_ID";
	$where .= " AND CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) BETWEEN '$start_date' AND '$end_date'";
	//$where .= " AND CONVERT(time, mc.cycle_time) BETWEEN ti.interval_start_time AND ti.interval_end_time";

	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND tp.tool_ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["parts"]) && $_REQUEST["parts"] != null) {
		$where .= " AND tp.part_ID IN (" . implode(",", $_REQUEST["parts"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where .= " AND s.ID IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "SELECT
				s.shift_desc,
				CONVERT(CHAR(10), dbo.getProdDate(mc.cycle_time), 101) AS category,
				COUNT(mcp.ID) AS quantity
			FROM
				machine_cycles mc
			JOIN
				machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
			JOIN
				machines m ON m.ID = mc.machine_ID
			JOIN
				tool_parts tp ON tp.ID = mcp.tool_part_ID
			LEFT JOIN
				MES_COMMON.dbo.shifts s ON s.ID = MES_COMMON.dbo.fn_getShift(m.system_ID, mc.cycle_time)
			LEFT JOIN
				MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
			$where
			GROUP BY
				s.shift_desc, dbo.getProdDate(mc.cycle_time)";
//die($sql);
	$result = $db->query($sql);

	echo json_encode(format_daily_prod_to_highchart($result));
}

function get_hourly_production($db) {
	$system_ID = $_REQUEST["system_ID"];
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];

//	DECLARE @prod_days int
//			SELECT
//				@prod_days = CASE WHEN COUNT(prod_days) = 0 THEN 1 ELSE COUNT(prod_days) END
//			FROM (
//				SELECT
//					DISTINCT CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) AS prod_days
//				$from
//				$where
//				) AS prod_days;

	$where = "WHERE CONVERT(DATE, dbo.getProdDate(mc.cycle_time), 113) BETWEEN '$start_date' AND '$end_date'";
	$where2 = "WHERE s.system_ID = $system_ID";

	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(",", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND tp.tool_ID IN (" . implode(",", $_REQUEST["tools"]) . ")";
	}
	if (isset($_REQUEST["parts"]) && $_REQUEST["parts"] != null) {
		$where .= " AND tp.part_ID IN (" . implode(",", $_REQUEST["parts"]) . ")";
	}
	if (isset($_REQUEST["operators"]) && $_REQUEST["operators"] != null) {
		$where .= " AND o.ID IN (" . implode(",", $_REQUEST["operators"]) . ")";
	}
	if (isset($_REQUEST["shifts"]) && $_REQUEST["shifts"] != null) {
		$where2 .= " AND s.ID IN (" . implode(",", $_REQUEST["shifts"]) . ")";
	}

	$sql = "SELECT
				s.shift_desc,
				ti.interval_desc AS category,
				--SUM(1) / @prod_days AS quantity,
				CASE WHEN DATEDIFF(DD, '$start_date', '$end_date') = 0 THEN
					COUNT(cycles.cycle_time) ELSE
					COUNT(cycles.cycle_time) / DATEDIFF(DD, '$start_date', '$end_date') END
					AS quantity,
				ti.interval_start_time
			FROM
				MES_COMMON.dbo.shifts s
			JOIN
				MES_COMMON.dbo.shift_time_intervals sti ON s.ID = sti.shift_ID
			JOIN
				MES_COMMON.dbo.time_intervals ti ON ti.ID = sti.time_interval_ID
			LEFT JOIN
			(
				SELECT m.system_ID, mc.cycle_time, o.ID AS operator_ID, mcp.tool_part_ID
				FROM
					machines m
				JOIN
					machine_cycles mc ON m.ID = mc.machine_ID
				JOIN
					machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
				JOIN
					tool_parts tp ON tp.ID = mcp.tool_part_ID
				LEFT JOIN
					MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
				$where
			) AS cycles ON CONVERT(time, cycles.cycle_time) BETWEEN ti.interval_start_time AND ti.interval_end_time
			$where2
			GROUP BY
				sti.is_day_start, s.shift_desc, ti.interval_desc, ti.interval_start_time
			ORDER BY
				sti.is_day_start desc, ti.interval_start_time";
//die($sql);
	$result = $db->query($sql);

	echo json_encode(format_hourly_prod_to_highchart($result));
}

//function get_production_details($db) {
//	global $from, $where;
//
//	$from .= " JOIN parts p ON p.ID = tp.part_ID";
//
//	$sql = "SELECT
//				m.machine_number + ' - ' + m.machine_name AS Machine
//				,CONVERT(VARCHAR, mc.cycle_time, 109) AS [Cycle]
//				,mc.cycle_duration AS 'Cycle Duration'
//				,p.part_number AS 'Part Number'
//				,p.part_desc AS 'Part Desc'
//				,o.name AS Operator
//			$from
//			$where
//			ORDER BY
//				mc.cycle_time";
//
//	$result = $db->query($sql);
//
//	echo json_encode($result);
//}