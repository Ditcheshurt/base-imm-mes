<?php

// DataTables PHP library and database connection
include("../../../../lib/DataTables/DataTables.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
$editor = Editor::inst($db, 'machine_in_process_parts', 'serial')
	->fields(
		Field::inst('machine_in_process_parts.serial')
			->validator('Validate::numeric'),
		Field::inst('machine_in_process_parts.created_time')
			->validator( 'Validate::notEmpty'),
		Field::inst('machine_in_process_parts.last_machine_started_ID'),
		Field::inst('machine_in_process_parts.last_machine_started_order')
			->validator('Validate::numeric'),
		Field::inst('machine_in_process_parts.last_machine_started_time')
			->validator('Validate::notEmpty'),
		Field::inst('machine_in_process_parts.last_machine_completed_ID'),
		Field::inst('machine_in_process_parts.last_machine_completed_order')
			->validator('Validate::numeric'),
		Field::inst('machine_in_process_parts.last_machine_completed_time')
			->validator( 'Validate::notEmpty')
	);

$editor->where( function ( $q ) {
    $q->where( 'machine_in_process_parts.created_time', 'dateadd(dd, -30, getDate())', '>=', false );
} );

$editor->process($_POST)->json();