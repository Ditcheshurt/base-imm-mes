var NYSUS = NYSUS || [];
NYSUS.GAUGE = NYSUS.GAUGE || [];
NYSUS.GAUGE.INSTRUCTION_EDITOR = {};
(function ($) {
	$(document).ready(function () {
		var processor = 'custom/metaldyne_litchf/manage/gauge_material_types/table.gauge_material_type_instructions.php';
		NYSUS.GAUGE.INSTRUCTION_EDITOR.editor = new $.fn.dataTable.Editor({
			colReorder: true,
			ajax: {
				url: processor,
				data: function (d) {
					$.each(d.data, function (k, v) {
						v.gauge_material_type_ID = NYSUS.GAUGE.INSTRUCTION_EDITOR.gauge_material_type_ID;
					});
				}
			},
			table: '#gauge_material_type_instructions',
			cache: 'false',
			fields: [
				{
					"label": "Order",
					"name": "instr_order"
				},
				{
					"label": "Description",
					"name": "description"
				},
				{
					"label": "Is Pass Fail",
					"name": "is_pass_fail",
					"type": "checkbox",
					"default": 0,
					"separator": ",",
					"options": [
						{label: "", value: 1}
					]
				},
				{
					"label": "Is Calculated",
					"name": "is_calculated",
					"type": "checkbox",
					"default": 0,
					"separator": ",",
					"options": [
						{label: "", value: 1}
					]
				},
				{
					"label": "Base Value",
					"name": "base_value"
				},
				{
					"label": "Min Value",
					"name": "min_value",
					"render": $.fn.dataTable.render.number(',', '.', 2, null)
				},
				{
					"label": "Max Value",
					"name": "max_value",
					"render": $.fn.dataTable.render.number(',', '.', 2, null)
				},
				{
					"label": "Image",
					"name": "image_url",
					"type": "upload",
					"display": function (data) {
						return '<img src="' + data + '" width="100px" height="100px" />';
					},
					"clearText": "Clear",
					"noImageText": 'No image'
				},
				{
					"label": "Instruction",
					"name": "instruction"
				},
				{
					"label": "Units",
					"name": "units"
				},
				{
					"label": "Active",
					"name": "active",
					"type": "checkbox",
					"default": 0,
					"separator": ",",
					"options": [
						{label: "", value: 1}
					]
				}
			]
		});

		NYSUS.GAUGE.INSTRUCTION_EDITOR.table = $('#gauge_material_type_instructions').DataTable({
			colReorder: true,
			ajax: {
				url: processor,
				data: function (d) {
					return $.extend({}, d, {
						"gauge_material_type_ID": NYSUS.GAUGE.INSTRUCTION_EDITOR.gauge_material_type_ID
					});
					//d.gauge_material_type_ID = NYSUS.GAUGE.INSTRUCTION_EDITOR.gauge_material_type_ID; // a variable that can be accessed and contains the correct id
				}
			},
			fixedHeader: true,
			select: "single",
			lengthChange: false,
			columns: [
				{
					"data": null,
					"defaultContent": "",
					"className": "select-checkbox",
					"orderable": false
				},
				{
					"data": "instr_order"
				},
				{
					"data": "description"
				},
				{
					"data": "is_pass_fail",
					"className": "dt-body-center",
					"render": function (data, type, row) {
						var isActive = !(data == null || data == false);
						return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
					}
				},
				{
					"data": "is_calculated",
					"className": "dt-body-center",
					"render": function (data, type, row) {
						var isActive = !(data == null || data == false);
						return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
					}
				},
				{
					"data": "base_value"
				},
				{
					"data": "min_value",
					"render": $.fn.dataTable.render.number(',', '.', 2, null)
				},
				{
					"data": "max_value",
					"render": $.fn.dataTable.render.number(',', '.', 2, null)
				},
				{
					"data": "image_url",
					type: "upload",
					render: function (data) {
						return data ?
						'<img src="' + data + '" width="50px" height="50px" />' :
							'No image';
					}
				},
				{
					"data": "instruction"
				},
				{
					"data": "units"
				},
				{
					"data": "active",
					"className": "dt-body-center",
					"render": function (data, type, row) {
						var isActive = !(data == null || data == false);
						return $('<span />').text(isActive ? "Yes" : "No").addClass('label label-xs').addClass(isActive ? 'label-success' : 'label-danger')[0].outerHTML;
					}
				}
			]
		});

		new $.fn.dataTable.Buttons(NYSUS.GAUGE.INSTRUCTION_EDITOR.table, [
			{
				text: 'Reload',
				action: function (e, dt, node, config) {
					dt.ajax.reload();
				}
			},
			{extend: "create", editor: NYSUS.GAUGE.INSTRUCTION_EDITOR.editor},
			{extend: "edit", editor: NYSUS.GAUGE.INSTRUCTION_EDITOR.editor},
			{extend: "remove", editor: NYSUS.GAUGE.INSTRUCTION_EDITOR.editor}
		]);

		NYSUS.GAUGE.INSTRUCTION_EDITOR.table.buttons().container()
			.appendTo($('.col-sm-6:eq(0)', NYSUS.GAUGE.INSTRUCTION_EDITOR.table.table().container()));

	});

}(jQuery));