<?php

// DataTables PHP library and database connection
include("../../../../lib/DataTables/DataTables.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

$gauge_material_type_ID = isset($_REQUEST["gauge_material_type_ID"]) ? $_REQUEST["gauge_material_type_ID"] : null;

// Build our Editor instance and process the data coming from _POST
$editor = Editor::inst($db, 'gauge_material_types_instructions', 'ID')
	->fields(
		Field::inst('gauge_material_type_ID')
			->validator('Validate::numeric'),
		Field::inst('instr_order')
			->validator('Validate::numeric'),
		Field::inst('description')
			->validator('Validate::notEmpty'),
		Field::inst('is_pass_fail')
			->validator('Validate::boolean'),
		Field::inst('is_calculated')
			->validator('Validate::boolean'),
		Field::inst('base_value')
			->validator('Validate::numeric'),
		Field::inst('min_value')
			->validator('Validate::numeric'),
		Field::inst('max_value')
			->validator('Validate::numeric'),
		Field::inst('image_url')
			->upload(
				Upload::inst( function ( $file, $id ) {
					$rand = mt_rand();
					$path =  $_SERVER["DOCUMENT_ROOT"]."\\common\\images\\gauge\\";
					$url = "../common/images/gauge/";
					$file_name = $rand.'~~'.$file['name'];

					move_uploaded_file( $file['tmp_name'], $path.$file_name );
					return $url.$file_name;
				} )
				->allowedExtensions( array( 'png', 'jpg', 'gif' ), "Please upload an image file" )),
		Field::inst('instruction')
			->validator('Validate::notEmpty'),
		Field::inst('units'),
		Field::inst('active')
			->validator('Validate::boolean')
	);

if ($gauge_material_type_ID != null) {
	$editor->where('gauge_material_type_ID', $gauge_material_type_ID);
}
$editor->process($_POST)
->json();