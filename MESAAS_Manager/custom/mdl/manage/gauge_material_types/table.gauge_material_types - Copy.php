<?php

// DataTables PHP library and database connection
include("../../../../lib/DataTables/DataTables.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst($db, 'gauge_material_types', 'ID')
	->fields(
		Field::inst('gauge_material_types.ID'),
		Field::inst('gauge_material_types.tool_part_ID')
			->options(function () {
				global $db;

				$sql = "SELECT tp.[ID] AS value
                          , t.[short_description] + ' | ' + p.[part_desc] AS label
                      FROM [tool_parts] tp
                      JOIN [tools] t ON t.ID = tp.tool_ID
                      JOIN [parts] p ON p.ID = tp.part_ID";

				return $db->sql($sql)->fetchAll(PDO::FETCH_ASSOC);
			}),
		Field::inst('gauge_material_types.description')
			->validator('Validate::notEmpty'),
		Field::inst('gauge_material_types.gauge_parts_used'),
		Field::inst('gauge_material_types.parts_per_gauging')
			->validator('Validate::numeric'),
		Field::inst('gauge_material_types.parts_per_gauging_warning_offset')
			->validator('Validate::numeric')
			->setFormatter('Format::nullEmpty'),
		Field::inst('gauge_material_types.parts_per_gauging_maximum_offset')
			->validator('Validate::numeric')
			->setFormatter('Format::nullEmpty'),
		Field::inst('gauge_material_types.station_ip_address')
			->validator('Validate::ip'),
		Field::inst('gauge_material_types.skip_part_scan')
			->validator('Validate::boolean'),
		Field::inst('gauge_material_types.active')
			->validator('Validate::boolean'),
		Field::inst('tools.short_description'),
		Field::inst('parts.part_desc')
	)
	->leftJoin('tool_parts', 'tool_parts.ID', '=', 'gauge_material_types.tool_part_ID')
	->leftJoin('tools', 'tool_parts.tool_ID', '=', 'tools.ID')
	->leftJoin('parts', 'tool_parts.part_ID', '=', 'parts.ID')
	->process($_POST)
	->json();