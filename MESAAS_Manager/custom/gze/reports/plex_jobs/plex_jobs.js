var processor = './custom/genze/reports/plex_jobs/plex_jobs.php';


$(document).ready(function () {
	loadPlexJobs();

	$('.btn-refresh-jobs').on('click', loadPlexJobs);
	$('.btn-confirm-pull-jobs-from-plex').on('click', confirmPullJobsFromPlex);
	$('.btn-pull-jobs-from-plex').on('click', pullJobsFromPlex);
});

function loadPlexJobs() {
	$('#div_plex_jobs TABLE TBODY').html('');

	$.getJSON(processor, {"action":"load_plex_jobs"}, showPlexJobs);
}

function showPlexJobs(jso) {
	var b = $('#div_plex_jobs TABLE TBODY');
	for (var i = 0; i < jso.length; i++) {
		var r = $('<TR />');

		r.append($('<TD />').html(jso[i].Job_No));
		r.append($('<TD />').html(jso[i].Job_Key));
		r.append($('<TD />').html(jso[i].Part_No));
		r.append($('<TD />').html(jso[i].Quantity));

		var d = $('<DIV />');

		var btn = $('<BUTTON />').addClass('btn btn-info btn-xs').html('View Module(s) Created').data(jso[i]).on('click', getModules);
		d.append(btn);

		var btn = $('<BUTTON />').addClass('btn btn-info btn-xs').html('View Plex BOM').data(jso[i]).on('click', getBOM);
		d.append(btn);

		r.append($('<TD />').append(d));

		b.append(r);
	}
}

function getModules() {
	var job_data = $(this).data();
	$('#div_modules_modal H4.modal-title').html('Modules for Plex Job: ' + job_data.Job_No);
	$('#div_modules_modal TABLE TBODY').html('');
	$.getJSON(processor, {"action":"load_plex_job_modules", "Job_Op_Key":job_data.Job_Op_Key}, showPlexJobModules);
}

function showPlexJobModules(jso) {
	var b = $('#div_modules_modal TABLE TBODY');
	for (var i = 0; i < jso.length; i++) {
		var r = $('<TR />');

		r.append($('<TD />').html(jso[i].VIN));
		r.append($('<TD />').html(jso[i].build_order));
		r.append($('<TD />').html(jso[i].loaded_time.date));
		r.append($('<TD />').html(jso[i].status_desc));

		b.append(r);
	}

	$('#div_modules_modal').modal('show');
}

function getBOM() {
	var job_data = $(this).data();
	$('#div_BOM_modal H4.modal-title').html('BOM for Plex Job: ' + job_data.Job_No);
	$.getJSON(processor, {"action":"load_plex_job_BOM", "Job_Key":job_data.Job_Key}, showPlexJobBOM);
}

function showPlexJobBOM(jso) {
	var b = $('#div_BOM_modal TABLE TBODY');
	for (var i = 0; i < jso.length; i++) {
		var r = $('<TR />');

		r.append($('<TD />').html(jso[i].Component_Part_No));
		r.append($('<TD />').html(jso[i].Component_Part_Name));

		b.append(r);
	}

	$('#div_BOM_modal').modal('show');
}


function confirmPullJobsFromPlex() {
	$('#div_pull_confirm_modal').modal('show');
}

function pullJobsFromPlex() {
	$.getJSON(processor, {"action":"pull_jobs_from_plex"}, afterPullJobsFromPlex);
}

function afterPullJobsFromPlex() {
	$('#div_pull_confirm_modal').modal('hide');
}