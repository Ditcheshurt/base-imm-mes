<?php
header ( "Content-Type: application/json" );
$GLOBALS ['APP_CONFIG'] = json_decode ( file_get_contents ( "config/app_config.json" ), true );
require_once ($_SERVER ['DOCUMENT_ROOT'] . $GLOBALS ['APP_CONFIG'] ['db_class']);

error_reporting ( E_ALL );
ini_set ( 'display_errors', 'On' );

$db = new db ();
$db->server = $GLOBALS ['APP_CONFIG'] ['server'];
$db->catalog = $GLOBALS ['APP_CONFIG'] ['catalog'];
$db->username = $GLOBALS ['APP_CONFIG'] ['username'];
$db->password = $GLOBALS ['APP_CONFIG'] ['password'];
$db->testactive = $GLOBALS ['APP_CONFIG'] ['testactive'];

if (isset ( $_REQUEST ['database'] )) {
	$db->catalog = $_REQUEST ['database'];
}

$db->connect ();

?>