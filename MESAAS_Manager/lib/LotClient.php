<?php
class LotClient {
	private $lotType;
	public function setLotType($lotType) {
		$this->lotType = $lotType;
	}
	public function getLotById($lot_id) {
		return $this->lotType->getLotById ( $lot_id );
	}
	public function getLotsByStationIP($ip_address) {
		return $this->lotType->getLotsByStationIP ( $ip_address );
	}
	public function checkLotExists($material_type_ID, $lot_number) {
		return $this->lotType->checkLotExists ( $material_type_ID, $lot_number );
	}
}
?>