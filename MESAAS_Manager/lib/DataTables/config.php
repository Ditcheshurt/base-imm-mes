<?php if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

/*
 * DB connection script for Editor
 * Created by http://editor.datatables.net/generator
 */

// Enable error reporting for debugging (remove for production)
error_reporting(E_ALL);
ini_set('display_errors', '1');

$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents(dirname(dirname(dirname(__FILE__))).'\config\app_config.json'), true);

/*
 * Edit the following with your database connection options
 */
// Allows us to override the catalog setting in app_config.json.$_COOKIE
 // This is required when we need to connect to MES_COMMON for the operator management screen for example.
 if ( ! isset( $database ) ) {
	 $database = $GLOBALS['APP_CONFIG']['catalog'];
 } 
 
$sql_details = array(
	"type" => "Sqlserver",
	"user" => $GLOBALS['APP_CONFIG']['username'],
	"pass" => $GLOBALS['APP_CONFIG']['password'],
	"host" => $GLOBALS['APP_CONFIG']['server'],
	"port" => "",
	"db"   => $database
);