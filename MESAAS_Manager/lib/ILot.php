<?php
interface ILot {
	public function getLotById($lot_id);
	public function getLotsByStationIP($ip_address);
	public function checkLotExists($material_type_ID, $lot_number);
}
?>