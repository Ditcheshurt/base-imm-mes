<?php

/**
 * Encapsulates MQ broadcast type functionality. 
 *
 * @version 1.0
 * @author clazette
 */
abstract class MqBroadcastTypeBase extends BroadcastTypeBase // implements IBroadcastType
{
    const BROADCAST_TYPE = 'MQ';    

    public function __construct($messageType, $db)
    {
        parent::__construct($messageType, $db);
    }   
}
