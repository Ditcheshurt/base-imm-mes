<?php

/**
 * Encapsulates MQ SR broadcast message type functionality. 
 *
 * MqBroadcastMessageTypeSr description.
 *
 * @version 1.0
 * @author clazette
 */
class MqBroadcastMessageTypeSr extends MqBroadcastTypeBase implements IBroadcastMessage
{
    // [ Constants ]
    const MESSAGE_TYPE            = 'SR';
    const TYPE_OF_REPLY_STANDARD  = 'S'; 
    const TYPE_OF_REPLY_EFFECTIVE = 'E';
    const ADD_FLAG                = 'A';
    const DELETE_FLAG             = 'D';   

    // [ Constructors ]
    public function __construct($db)
    {
        parent::__construct(self::MESSAGE_TYPE, $db);
    }

    // [ Protected Functions ]

    protected function decipherBroadcastOutMessageRawData($moduleId)
    {
        $broadcastsOutData = parent::retrieveOutboundBroadcastMessageData($moduleId);

        if ($broadcastsOutData == null){ return null; }
       
       return $this->toEntity($broadcastsOutData);
    }

    protected function formatBroadcastMessage($moduleId, $moduleLineData, $modulePartChangesData, $isStandardTypeOfReply)
    {
        $typeOfReplyCode = $this->setTypeOfReplyCode($isStandardTypeOfReply);

        // Always generate standard portion of the message with applicable type of reply code.
        $standardPortionOfSrMessage = $this->generateStandardPortionOfSrMessage($typeOfReplyCode, $moduleLineData);

        if ($isStandardTypeOfReply){ return $standardPortionOfSrMessage; }
        
        // If not a standard type of reply, deal with effective part changes.
        $effectivePartsPortionOfSrMessage = null;

        $effectivePartsPortionOfSrMessage = $this->generateEffectivePartsPortionOfSrMessage($modulePartChangesData);

        return (string) $standardPortionOfSrMessage . $effectivePartsPortionOfSrMessage;
    }

    // [ Private Functions ]

    private function toEntity($broadcastsOutData)
    {
        $broadcastOutEntities = array();

        foreach ($broadcastsOutData as $broadcastOut)
        {
            $messageTypeEntity = $this->parseRawData($broadcastOut['RawData']);

            $broadcastOutEntities[] = parent::broadcastOutToEntity((object)$broadcastOut, $messageTypeEntity);
        }

        return $broadcastOutEntities;
    }

    private function parseRawData($rawData)
    {
        $entity = new MqMessageTypeSr();
        
        // Standard portion of message.
        $entity->setMessageType         (substr( $rawData, 0,  2  ));
        $entity->setPlantCode           (substr( $rawData, 2,  1  ));
        $entity->setSupplierCode        (substr( $rawData, 3,  7  ));
        $entity->setVin                 (substr( $rawData, 10, 8  ));
        $entity->setModuleType          (substr( $rawData, 18, 3  ));
        $entity->setStatusCode          (substr( $rawData, 21, 2  ));
        $entity->setSequenceNumber      (substr( $rawData, 23, 11 ));
        $entity->setTypeOfReply         (substr( $rawData, 34, 1  ));

        // Effective parts portion of message. 
        if ($entity->getTypeOfReply() === self::TYPE_OF_REPLY_EFFECTIVE)
        {
            $entity->setNumberOfPartNumbers (substr( $rawData, 35, 4 ));

            $entity->setEffectiveComponents($this->hydrateEffectiveComponents(substr( $rawData, 39 ))); // Send everything after 39. The rest should be effective components that need to be parsed.
        }        

        return $entity;
    }

    private function hydrateEffectiveComponents($rawDataEffectiveComponents)
    {
        $componentStringLength          = 39; // Cumulative number of characters for fields 10, 11, 12, and 13 per the specification document.
        $effectiveComponents            = array();
        $rawComponents                  = array();
        $numberOfEffectiveComponents    = strlen($rawDataEffectiveComponents) / $componentStringLength;

        for ($i = 0; $i < $numberOfEffectiveComponents; $i++ )
        {
            $rawComponents[] = substr($rawDataEffectiveComponents, $i * $componentStringLength, $componentStringLength);
        }

        foreach ($rawComponents as $component)
        {
            $effectiveComponent = new EffectiveComponent();

            $effectiveComponent->setPartNumber          (substr( $component, 0,  10 ));
            $effectiveComponent->setQuantity            (substr( $component, 10, 8  ));
            $effectiveComponent->setAddDeleteFlag       (substr( $component, 18, 1  ));
            $effectiveComponent->setChangeAuthorization (substr( $component, 19, 20 ));

            $effectiveComponents[] = $effectiveComponent;
        }

        return $effectiveComponents;
    }

    private function generateStandardPortionOfSrMessage($typeOfReplyCode, $moduleLineBroadcastData)
    {
        $supplierId      = $this->extractSupplierIdFromBroadcastQueueName($moduleLineBroadcastData->QueueOut);
        $substrVin       = $this->substrRight($moduleLineBroadcastData->VIN, 8);
        $strPadStausCode = str_pad($moduleLineBroadcastData->BuildStatus, 2, ' ', STR_PAD_RIGHT);
        $strPadSequence  = str_pad($moduleLineBroadcastData->Sequence, 11, ' ', STR_PAD_LEFT);

        return (string) self::MESSAGE_TYPE . $moduleLineBroadcastData->PlantCode . $supplierId . '  ' . $substrVin . $moduleLineBroadcastData->ModuleType . $strPadStausCode . $strPadSequence . $typeOfReplyCode;
    }

    private function generateEffectivePartsPortionOfSrMessage($modulePartChangesData)
    {
        $effectivePointChange = str_pad($this->numberOfEffectivePartsChanges, 4, '0', STR_PAD_LEFT);  // Number of Part Numbers - Field 9 :: Numeric 4

        foreach ($modulePartChangesData as $part)
        {
            $effectivePointChange .= str_pad($part['PartNumber'], 10, ' ', STR_PAD_LEFT);             // Part Number          - Field 10 :: Alphanumeric 10
            $effectivePointChange .= str_pad($part['Quantity'], 3, '0', STR_PAD_LEFT) . '.0000';      // Quantity             - Field 11 :: Alphanumeric 8 (Nnn.nnnn)
            $effectivePointChange .= (int)$part['Added'] === 1 ? self::ADD_FLAG : self::DELETE_FLAG;  // Add or Delete Flag   - Field 12 :: Alphanumeric 1
            $effectivePointChange .= str_pad($part['ChangeAuthorization'], 20, ' ', STR_PAD_RIGHT);   // Change Authorization - Field 13 :: Alphanumeric 20
        }

        return (string) $effectivePointChange;
    }    

    private function setTypeOfReplyCode($isStandardTypeOfReply)
    {
        return (string) $isStandardTypeOfReply ? self::TYPE_OF_REPLY_STANDARD : self::TYPE_OF_REPLY_EFFECTIVE;
    }   

    private function substrRight($stringToParse, $numberOfCharactersToKeep)
    {
        return (string) substr($stringToParse, strlen($stringToParse) - $numberOfCharactersToKeep, $numberOfCharactersToKeep);
    }

    //private function sequencePadLeft($sequence, $buildStatus)
    //{
    //    return (string) str_pad($sequence, 11, ' ', STR_PAD_LEFT); // strlen($sequence) + (2 - strlen($buildStatus))
    //}
}
