//<?php

///**
// * IBroadcastMessageType is a contract established to provide the ability to swap broadcast message related behavior at run time.
// *
// * @version 1.0
// * @author clazette
// */
//interface IBroadcastType
//{
//    public function retrieveOutboundBroadcastMessages($moduleId);
//    public function generateBroadcastMessageSr($moduleId, $typeOfReplyOverrideStandard);
//}
