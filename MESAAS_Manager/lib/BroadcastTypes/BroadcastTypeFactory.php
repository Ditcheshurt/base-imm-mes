//<?php

///**
// * BroadcastMessageFactory returns the appropriate IBroadcastMessage implementation based on the broadcast type.
// *
// * @version 1.0
// * @author clazette
// */
//class BroadcastTypeFactory
//{
//    public static function manufactureBroadcastType($broadcastType, $broadcastMessageType, $db)
//    {
//        $broadcastTypeInstance = null;

//        switch (strtolower($broadcastType))
//        {
//            case 'mq':
//                $broadcastTypeInstance = new MqBroadcastType($broadcastMessageType, $db);
//                break;
//            case 'ilvs':
//                $broadcastTypeInstance = new IlvsBroadcastType($db);
//                break;
//            default:
//                die(sprintf('No IBroadcastMessage implementation exists for the specified broadcast type [ %s ].<br>Please ensure the MES_COMMON.dbo.systems.broadcast_type field is populated correctly and that you have added the appropriate case statement to the BroadcastTypeFactory class.', $broadcastType));
//        }

//        // My attempt to enforce type checking in php.
//        if (!$broadcastTypeInstance instanceof IBroadcastType){ throw new Exception(sprintf('The broadcast type [ %s ] does not implement IBroadcastType. Please try again.', $broadcastType)); }

//        return $broadcastTypeInstance;
//    }
//}
