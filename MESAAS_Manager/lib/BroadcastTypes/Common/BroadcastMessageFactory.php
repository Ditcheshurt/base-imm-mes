<?php

/**
 * BroadcastMessageFactory short summary.
 *
 * BroadcastMessageFactory description.
 *
 * @version 1.0
 * @author clazette
 */
class BroadcastMessageFactory
{
    public static function manufactureBroadcastMessageType($broadcastType, $broadcastMessageType, $db)
    {
        $broadcastMessageInstance = null;

        switch ([strtolower($broadcastType), strtolower($broadcastMessageType)])
        {
            case ['mq', 'sr']:
                $broadcastMessageInstance = new MqBroadcastMessageTypeSr($db);
                break;
            //case 'ilvs':
            //    $broadcastMessageInstance = new IlvsBroadcastType();
            //    break;
            default:
                die(sprintf('No IBroadcastMessage implementation exists for the specified broadcast type / message type [ %s / %s ]. Ensure that you have added the appropriate case statement to the BroadcastMessageFactory class.', $broadcastType, $broadcastMessageType));
        }

        // My attempt to enforce type checking in php.
        if (!$broadcastMessageInstance instanceof IBroadcastMessage){ throw new Exception(sprintf('Broadcast message type [ %s ] does not implement IBroadcastMessage. Please try again.', $broadcastMessageType)); }

        if (!is_subclass_of($broadcastMessageInstance, 'BroadcastTypeBase', false)){ throw new Exception(sprintf('Broadcast message type [ %s ] does not extend BroadcastTypeBase. Please try again.', $broadcastMessageType)); }

        return $broadcastMessageInstance;
    }
}
