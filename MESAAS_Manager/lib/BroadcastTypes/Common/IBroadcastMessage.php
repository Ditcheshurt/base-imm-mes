<?php

/**
 * IBroadcastMessage short summary.
 *
 * IBroadcastMessage description.
 *
 * @version 1.0
 * @author clazette
 */
interface IBroadcastMessage
{
    public function retrieveOutboundBroadcastMessages($moduleId);
    public function generateBroadcastMessage($moduleId, $typeOfReplyOverrideStandard);
    public function resendOutboundBroadcastMessage($broadcastOutId);
}
