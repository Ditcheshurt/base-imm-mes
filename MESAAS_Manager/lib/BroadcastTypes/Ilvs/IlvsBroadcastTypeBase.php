<?php

/**
 * Encapsulates ILVS broadcast type functionality.
 * 
 * @version 1.0
 * @author clazette
 */
abstract class IlvsBroadcastTypeBase extends BroadcastTypeBase // implements IBroadcastType
{
    const BROADCAST_TYPE = 'ILVS';    

    public function __construct($messageType, $db)
    {
        parent::__construct($messageType, $db);
    }   
}
