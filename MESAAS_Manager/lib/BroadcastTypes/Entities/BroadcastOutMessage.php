<?php

/**
 * BroadcastOut short summary.
 *
 * BroadcastOut description.
 *
 * @version 1.0
 * @author clazette
 */
class BroadcastOutMessage implements JsonSerializable
{
    // [ Private Member Variables ]
    private $id;
    private $messageType;
    private $responseQueue;
    private $rawData;
    private $queuedTime;
    private $sentTime;
    private $moduleId;
    private $broadcastMessageType;

    // [ Constructors ]
    public function __construct($id, $messageType, $responseQueue, $rawData, $queuedTime, $sentTime, $moduleId, $broadcastMessageType)
    {
        $this->id                   = $id;
        $this->messageType          = $messageType;
        $this->responseQueue        = $responseQueue;
        $this->rawData              = $rawData;
        $this->queuedTime           = $queuedTime;
        $this->sentTime             = $sentTime;
        $this->moduleId             = $moduleId;
        $this->broadcastMessageType = $broadcastMessageType;
    }

    // [ Public Properties ]
    public function getId()                     { return $this->id;                     }
    public function getMessageId()              { return $this->messageType;            }
    public function getResponseQueue()          { return $this->responseQueue;          }
    public function getRawData()                { return $this->rawData;                }
    public function getQueuedTime()             { return $this->queuedTime;             }
    public function getSentTime()               { return $this->sentTime;               }
    public function getModuleId()               { return $this->moduleId;               }
    public function getBroadcastMessateType()   { return $this->broadcastMessageType;   }

    // JsonSerializable implementation
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}