<?php

/**
 * MqMessageTypeSr short summary.
 *
 * MqMessageTypeSr description.
 *
 * @version 1.0
 * @author clazette
 */
class MqMessageTypeSr extends MqBroadcastMessageBase implements JsonSerializable
{
    // [ Private Member Variables ]
    private $vin;
    private $moduleType;
    private $statusCode;
    private $sequenceNumber;
    private $typeOfReply;
    private $numberOfPartNumbers;
    private $effectiveComponents = array();

    // [ Public Properties ]
    public function setVin($value) { $this->vin = $value; }
    public function getVin(){ return $this->vin; }

    public function setModuleType($value) { $this->moduleType = $value; }
    public function getModuleType() { return $this->moduleType; }

    public function setStatusCode($value) { $this->statusCode = $value; }
    public function getStatusCode() { return $this->statusCode; }

    public function setSequenceNumber($value) { $this->sequenceNumber = $value; }
    public function getSequenceNumber() { return $this->sequenceNumber; }

    public function setTypeOfReply($value) { $this->typeOfReply = $value; }
    public function getTypeOfReply() { return $this->typeOfReply; }

    public function setNumberOfPartNumbers($value) { $this->numberOfPartNumbers = $value; }
    public function getNumberOfPartNumbers() { return $this->numberOfPartNumbers; }

    public function setEffectiveComponents($value) { $this->effectiveComponents = $value; }
    public function getEffectiveComponents() { return $this->effectiveComponents;}    

    // Constructors
    //public function __construct($messageType, $plantCode, $supplierCode)
    //{
    //    parent::__construct($messageType, $plantCode, $supplierCode);
    //}

    // JsonSerializable implementation
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
