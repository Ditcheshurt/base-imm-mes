<?php

/**
 * MqMessageBase short summary.
 *
 * MqMessageBase description.
 *
 * @version 1.0
 * @author clazette
 */
abstract class MqBroadcastMessageBase
{
    // This may just become BroadcastMessageBase if I decide to use entities. I don't think these properties are specific to MQ.

    private $messageType;
    private $plantCode;
    private $supplierCode;   

    //public function __construct($messageType, $plantCode, $supplierCode)
    //{
    //    $this->messageType  = $messageType;
    //    $this->plantCode    = $plantCode;
    //    $this->supplierCode = $supplierCode;
    //}

    // [ Public Properties ]
    public function setMessageType($value) { $this->messageType = $value; }
    public function getMessageType() { return $this->messageType; }

    public function setPlantCode($value) { $this->plantCode = $value; }
    public function getPlantCode() { return $this->plantCode; }    

    public function setSupplierCode($value) { $this->supplierCode = $value; }
    public function getSupplierCode() { return $this->supplierCode; }    
}
