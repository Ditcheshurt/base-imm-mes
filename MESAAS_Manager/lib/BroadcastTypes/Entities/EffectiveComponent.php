<?php

/**
 * EffectiveComponent short summary.
 *
 * EffectiveComponent description.
 *
 * @version 1.0
 * @author clazette
 */
class EffectiveComponent implements JsonSerializable
{
    // [ Private Member Variables ]
    private $partNumber;
    private $quantity;
    private $addDeleteFlag;
    private $changeAuthorization;

    // [ Public Properties ]
    public function setPartNumber($value) { $this->partNumber = $value; }
    public function getPartNumber(){ return $this->partNumber; }

    public function setQuantity($value) { $this->quantity = $value; }
    public function getQuantity(){ return $this->quantity; }

    public function setAddDeleteFlag($value) { $this->addDeleteFlag = $value; }
    public function getAddDeleteFlag(){ return $this->addDeleteFlag; }

    public function setChangeAuthorization($value) { $this->changeAuthorization = $value; }
    public function getChangeAuthorization(){ return $this->changeAuthorization; }

    // [ Constructors ]
    // Can't have more than one ctor in php and I wanted a no arg.
    //public function __construct(){}

    //public function __construct($partNumber, $quantity, $addDeleteFlag, $changeAuthorization)
    //{
    //    $this->partNumber           = $partNumber;
    //    $this->quantity             = $quantity;
    //    $this->addDeleteFlag        = $addDeleteFlag;
    //    $this->changeAuthorization  = $changeAuthorization;
    //}

    // JsonSerializable implementation
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
