<?php

/**
 * BroadcastTypeBase encapsulates common functionality for the broadcast message concrete types.
 *
 * @version 1.0
 * @author clazette
 */
abstract class BroadcastTypeBase
{
    // [ Private Member Variables ]
    private $db;                              // Data access really should be in a data class and this shouldn't be passed in.
    private $messageType;                     // Anything that needs this will likely move to the concrete message type class.
    private $queueOut;
    private $persistMessage           = true; // Sets whether the formatted message is inserted into the broadcast_out able.
    private $insertMessageAsProcessed = true; // If the message is being inserted, sets the date for now; otherwise 8/24/2015

    // [ Protected Variables ]
    protected $numberOfEffectivePartsChanges;

    // [ Constructors ]
    public function __construct($messageType, $db)
    {
        $this->db          = $db;
        $this->messageType = $messageType;
    }

    // [ Public Functions ]

    public function retrieveOutboundBroadcastMessages($moduleId)
    {
        return $this->decipherBroadcastOutMessageRawData($moduleId);
    }

    // The $typeOfReplyOverrideStandard parameter is specific to an MQ SR message type.
    // Additionally, the same is true of the isStandardTypeOfReply method. These need to move
    // but I'm not quite sure how to address this at this point.
    public function generateBroadcastMessage($moduleId, $typeOfReplyOverrideStandard)
    {
        $moduleLineData          = $this->retrieveModuleLineData($moduleId);
        $modulePartChangesData   = $this->retrieveModulePartChangesData($moduleId);
        $isStandardTypeOfReply   = $this->isStandardTypeOfReply($typeOfReplyOverrideStandard, $modulePartChangesData);

        $formattedMessage        = $this->formatBroadcastMessage($moduleId, $moduleLineData, $modulePartChangesData, $isStandardTypeOfReply);

        $this->persistOutboundMessage($moduleId, $formattedMessage);
    }

    public function resendOutboundBroadcastMessage($broadcastId)
    {
        $sql = "UPDATE broadcasts_out
                SET
                sent_time   = null,
                queued_time = GETDATE()
                WHERE ID    = " . $broadcastId;

        $this->db->query($sql);
    }

    // [ Abstract Functions ]
    protected abstract function formatBroadcastMessage($moduleId, $moduleLineData, $modulePartChangesData, $isStandardTypeOfReply);
    protected abstract function decipherBroadcastOutMessageRawData($moduleId);

    // [ Private Functions ]
    private function isStandardTypeOfReply($typeOfReplyOverrideStandard, $modulePartChangedData)
    {
        if ($typeOfReplyOverrideStandard == 'true' || $modulePartChangedData === null){ return true; }
        return (bool) false;
    }

    private function retrieveModuleLineData($moduleId)
    {
        // Get Module Line Broadcast Information
        $sql = "SELECT
                    m.line_ID       AS LineId,
                    m.sequence      AS Sequence,
                    m.VIN           AS VIN,
                    l.plant_code    AS PlantCode,
                    l.build_status  AS BuildStatus,
                    l.module_code   AS ModuleType,
                    l.queue_out     AS QueueOut
                FROM modules m
                JOIN lines l        ON m.line_ID = l.ID
                WHERE m.ID          = " . $moduleId;

        $res = (object) $this->db->query($sql)[0];
        
        $this->queueOut = $res->QueueOut;

        return $res;
    }

    private function retrieveModulePartChangesData($moduleId)
    {
        // Changed from what was in [sp_MESAAS_SEQUENCE_generateSRWithChangeAuth]
        // to a single statement that pulls change authorization in this query
        // instead of a separate call.
        $sql = "SELECT DISTINCT
                    mpc.part_number AS PartNumber,
                    mpc.qty			AS Quantity,
                    mpc.added		AS Added,
                    mpc.deleted		AS Deleted,
                    ISNULL(
		                     (
			                    SELECT TOP 1 change_number
			                    FROM change_authorization
			                    WHERE mpc.part_number =
									                    CASE
										                    WHEN mpc.added = 1 THEN new_part_number
										                    ELSE old_part_number
									                    END
			                    ORDER BY effective_date DESC
			                 ), ''
		                  ) AS ChangeAuthorization

                FROM module_part_changes mpc

                WHERE mpc.mod_ID = " . $moduleId;

        $res = $this->db->query($sql);

        $this->numberOfEffectivePartsChanges = count((array)$res);

        return $res;
    }

    private function persistOutboundMessage($moduleId, $formattedMessage)
    {
        if (!$this->persistMessage) { return null; }

        $queuedTime = $this->insertMessageAsProcessed ? date("Y/m/d H:i:s") : strtotime('8/24/2015');

        $sql = "INSERT INTO broadcasts_out (msg_type, queue, raw_data, queued_time, module_id)
                                    VALUES ('" . $this->messageType  . "', '"
                                               . $this->queueOut     . "', '"
                                               . $formattedMessage   . "', '"
                                               . $queuedTime         . "', "
                                               . $moduleId . ")";

        $this->db->query($sql);
    }

    // [ Protected Functions ]

    /*
        This function could be exposed publicly but there is no reason at this point.
        The intent is for this function to provide the raw data to the specific
        concrete message type class implementation in order to parse the raw_data
        and return a well-defined object graph for display in the manage broadcast modal.
    */
    protected function retrieveOutboundBroadcastMessageData($moduleId)
    {
        $sql = "SELECT
                     ID			                         AS BroadcastOutId,
                     msg_type                            AS MessageType,
                     queue                               AS ResponseQueue,
                     raw_data                            AS RawData,
                     CONVERT(VARCHAR, queued_time, 120)  AS QueuedTime,
                     CONVERT(VARCHAR, sent_time, 120)    AS SentTime,
                     module_Id                           AS ModuleId

                 FROM broadcasts_out

                 WHERE module_id = " . $moduleId . "
                 AND   msg_type    = '" . $this->messageType . "'

                 ORDER By ID DESC";

        $res = $this->db->query($sql);

        return $res;

        //$sql = "SELECT
        //            bo.ID			                        AS BroadcastOutId,
        //            CONVERT(VARCHAR, bo.queued_time, 120)   AS QueuedTime,
        //            CONVERT(VARCHAR, bo.sent_time, 120)     AS SentTime,
        //            mpc.mod_ID                              AS ModuleId,
        //            mpc.part_number                         AS PartNumber,
        //            mpc.qty			                        AS Quantity,
        //            mpc.added		                        AS Added,
        //            mpc.deleted		                        AS Deleted

        //        FROM module_part_changes mpc
        //        INNER JOIN broadcasts_out bo ON mpc.mod_ID = bo.module_id

        //        WHERE mpc.mod_ID = " . $moduleId . "
        //        AND msg_type     = '" . $this->messageType . "'

        //        ORDER By bo.ID DESC";

        //$res = $this->db->query($sql);

        //return $res;
    }

    protected function extractSupplierIdFromBroadcastQueueName($broadcastQueueName)
    {
        return substr($broadcastQueueName, 5, 5);
    }

    protected function broadcastOutToEntity($broadcastOut, $broadcastMessageType)
    {
        return new BroadcastOutMessage($broadcastOut->BroadcastOutId,
                                $broadcastOut->MessageType,
                                $broadcastOut->ResponseQueue,
                                $broadcastOut->RawData,
                                $broadcastOut->QueuedTime,
                                $broadcastOut->SentTime,
                                $broadcastOut->ModuleId,
                                $broadcastMessageType);
    }
}
