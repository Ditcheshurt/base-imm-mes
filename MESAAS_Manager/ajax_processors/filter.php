<?php

require_once("init.php");

$action = $_REQUEST['action'];


if ($action == 'get_systems') {
	 global $db;
	 $res = new StdClass();

	 if (isset($_REQUEST['process_type'])) {
		  $process_type_sql = "AND process_type = '" . $_REQUEST['process_type'] . "'";
	 } else {
		  $process_type_sql = "AND process_type IS NOT NULL";
	 }

	 $sql = "SELECT
					 ID,
					 system_name,
					 database_name,
					 process_type,
					 mrp_company_code,
					 oem,
					 broadcast_type
				FROM
					 MES_COMMON.dbo.systems
				WHERE
					 system_active = 1 " . $process_type_sql . "
				ORDER BY
					 system_name ASC;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_lines') {
	 global $db;
	 $res = new StdClass();

	 if (isset($_REQUEST['line_type'])) {
		  $line_type_sql = "AND line_type = ".$_REQUEST['line_type'];
	 } else {
		  $line_type_sql = "";
	 }

	 if (isset($_REQUEST['exclude_main_line']) && ($_REQUEST['exclude_main_line'] == 1 || $_REQUEST['exclude_main_line'] == true)) {
		  $exclude_main_sql = "AND line_type != 0";
	 } else {
		  $exclude_main_sql = "";
	 }

	 if (isset($_REQUEST['only_batch_mode_lines']) && ($_REQUEST['only_batch_mode_lines'] == 1 || $_REQUEST['only_batch_mode_lines'] == true)) {
		  $only_batch_lines_sql = " AND dbo.fn_getJSONBoolValue(line_options, 'allow_batch_mode') = 1 ";
	 } else {
		  $only_batch_lines_sql = "";
	 }

	 $sql = "SELECT
					 system_ID,
					 line_ID,
					 line_desc,
					 line_type
				FROM
					 MES_COMMON.dbo.v_MES_LINES
				WHERE
					 system_ID = ".$_REQUEST['system_id']."
					 ".$line_type_sql."
					 ".$exclude_main_sql."
					 ".$only_batch_lines_sql.";";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_statuses') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 ID,
					 status_desc
				FROM
					 module_statuses
				WHERE ID < 3
				ORDER BY ID;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_operators') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 o.ID,
					 CONCAT(o.badge_ID,' - ', o.name) AS name
				FROM
					 MES_COMMON.dbo.operators o
				ORDER BY
					 o.name;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_stations') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 station_ID,
					 line_ID,
					 (CAST(line_ID AS VARCHAR) + ' - ' + station_desc) AS name
				FROM
					 MES_COMMON.dbo.v_MES_LINE_STATIONS
				WHERE
					 system_ID = " . $_REQUEST['system_id'] . "
					 AND line_ID = " . $_REQUEST['line_id'] . "
				ORDER BY
					 line_ID;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_dispositions') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 disposition
				FROM
					 machine_defect_log
				WHERE
					 disposition IS NOT NULL
				GROUP BY
					 disposition;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_machines') {
	 global $db;
	 $res = new StdClass();

	$system_filter_sql = "";
	 if (isset($_REQUEST['system_ID'])) {
		  $system_filter_sql = "AND system_ID = ".$_REQUEST['system_ID'];
	 }

	 $sql = "SELECT
					 ID,
					 machine_name,
					 machine_number,
					 spc_use_auto_gauge_data,
					 spc_use_mmq_data
				FROM
					 machines
				WHERE
					 ignored = 0 ".$system_filter_sql."
				ORDER BY machine_name";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_tools') {
	global $db;
	$res = new StdClass();
	
	$machine_filter_sql = "";
	if (isset($_REQUEST['machine_ID'])) {
		$machine_filter_sql = "machine_ID = ".$_REQUEST['machine_ID'];
	}
	
	$sql = "SELECT
				 mt.ID
				,mt.machine_ID
				,mt.tool_ID
				,t.tool_description
			FROM
				machine_tools mt
			JOIN
				tools t
			ON
				t.ID = mt.tool_ID
			WHERE ".$machine_filter_sql.";";
	
	$res = $db->query($sql);
	
	echo json_encode($res);
}

if ($action == 'get_departments') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 ID,
					 department_name
				FROM
					 MES_COMMON.dbo.departments
				ORDER BY
					 department_name ASC;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == 'get_areas') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 ID,
					 area_desc
				FROM
					 MES_COMMON.dbo.areas
				WHERE
					 department_ID = " . $_REQUEST['department_ID'] . "
				ORDER BY
					 area_desc ASC;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

if ($action == "get_departments_areas") {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 ID,
					 department_name
				FROM
					 MES_COMMON.dbo.departments
				ORDER BY
					 department_name ASC;";

	 $res = $db->query($sql);
	 if ($res) {
		  foreach ($res as $i => &$department) {
				$sql = "SELECT
								ID,
								area_desc,
								department_ID
						  FROM
								MES_COMMON.dbo.areas
						  WHERE
								dept_ID = " . $department['ID'] . ";";
				$department['areas'] = $db->query($sql);
		  }
	 }

	 echo json_encode($res);
}


if ($action == 'get_sub_areas') {
	 global $db;
	 $res = new StdClass();

	 $sql = "SELECT
					 ID,
					 sub_area_desc
				FROM
					 MES_COMMON.dbo.sub_areas
				WHERE
					 area_ID = " . $_REQUEST['area_ID'] . "
				ORDER BY
					 sub_area_desc ASC;";

	 $res = $db->query($sql);

	 echo json_encode($res);
}

?>