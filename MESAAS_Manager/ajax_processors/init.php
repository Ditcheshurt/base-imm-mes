<?php

	//********* Error Handling ************//
	set_error_handler(
	create_function('$severity, $message, $file, $line', 'echo "{\"error\":\"$message\"}";exit(1);'));
	//create_function('$severity, $message, $file, $line', 'throw new ErrorException($message, $severity, $severity, $file, $line);'));

    header("Content-Type: application/json");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');	

	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents(dirname(dirname(__FILE__)).'\config\app_config.json'), true);

	require($_SERVER['DOCUMENT_ROOT'].$GLOBALS['APP_CONFIG']['db_class']);

	session_save_path(dirname(__DIR__).$GLOBALS['APP_CONFIG']['session_path']);
	session_start();
	
	$db = new db();
	$db->server = $GLOBALS['APP_CONFIG']['server'];
	$db->catalog = $GLOBALS['APP_CONFIG']['catalog'];
	$db->username = $GLOBALS['APP_CONFIG']['username'];
	$db->password = $GLOBALS['APP_CONFIG']['password'];
	$db->testactive = $GLOBALS['APP_CONFIG']['testactive'];

    if (isset($_REQUEST['database'])) {
        $db->catalog = $_REQUEST['database'];
    }

	$db->connect();

	function fixDB($s) {
	    $s = str_replace("'", "''", $s);
		return $s;
	}

?>