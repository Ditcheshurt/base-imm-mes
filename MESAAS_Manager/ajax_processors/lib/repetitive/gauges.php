<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	/**
	 * get a detail view of the gauges
	 */
	function get_gauges($request, $db) {
		$res = new StdClass();		
		
		// get the gauges
		$sql = "SELECT 
					[ID]
					,[tool_part_ID]
					,[description]
					,[gauge_parts_used]
					,[parts_per_gauging]
					,[parts_per_gauging_warning_offset]
					,[parts_per_gauging_maximum_offset]
					,[station_ip_address]
					,[active]
				FROM 
					[dbo].[gauge_material_types]";

		$res->gauges = $db->query($sql);		
		
		// get the available parts
		$sql = "SELECT
					tp.[ID]
					,m.[machine_number]
					,m.[machine_name]
					,p.[part_desc]
					,p.[part_number]
				FROM
					[dbo].[tool_parts] tp
				JOIN
					[dbo].[parts] p 
					ON p.ID = tp.part_ID
				JOIN
					[dbo].[tools] t 
					ON t.ID = tp.tool_ID
				JOIN
					[dbo].[machine_tools] mt 
					ON t.ID = mt.tool_ID
				JOIN 
					[dbo].[machines] m
					ON m.ID = mt.machine_ID
				WHERE
					tp.[active_end] IS NULL
				OR
					tp.[active_end] < getDate()";
			
		$res->tool_parts = $db->query($sql);
		
		// hydrate objects
		for ($i = 0; $i < count($res->gauges); $i++) {		
			
			// get part tied to gauge
			$sql = "SELECT
						tp.[ID]
						,m.[machine_number]
						,m.[machine_name]
						,p.[part_desc]
						,p.[part_number]
					FROM 
						[dbo].[tool_parts] tp
					JOIN
						[dbo].[parts] p 
						ON p.ID = tp.part_ID
					JOIN
						[dbo].[tools] t 
						ON t.ID = tp.tool_ID
					JOIN
						[dbo].[machine_tools] mt 
						ON t.ID = mt.tool_ID
					JOIN 
						[dbo].[machines] m
						ON m.ID = mt.machine_ID
					WHERE 
						tp.[ID] = ".$res->gauges[$i]["tool_part_ID"];
		
			$res->gauges[$i]["tool_part"] = $db->query($sql);
			
			// get instructions tied to gauge
			$sql = "SELECT 
						[ID]
						,[gauge_material_type_ID]
						,[instr_order]
						,[description]
						,[is_pass_fail]
						,[base_value]
						,[min_value]
						,[max_value]
						,[image_url]
						,[instruction]
						,[units]
						,[active]
					FROM 
						[dbo].[gauge_material_types_instructions]
					WHERE
						[gauge_material_type_ID] = ".$res->gauges[$i]["ID"]."
					ORDER BY 
						instr_order";
			
			$res->gauges[$i]["instructions"] = $db->query($sql);		
			
		}

		echo json_encode($res);

	}
	
	/**
	 * insert a gauge record
	 * @param $_REQUEST['field_names']
	 * @param $_REQUEST['field_values']
	 */
	function insert_gauge($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$field_values = $_REQUEST['field_values'];		
		
		$names = implode(", ", $field_names);
		$values = "'".implode("', '", fixDB($field_values))."'";
		
		$sql = "INSERT INTO 
					[dbo].[gauge_material_types]
						(".$names.")
				VALUES
					(".$values.")";
		
	//die($sql);
		$db->query($sql);
		
		$res->success = 1;
		
		echo json_encode($res);
	}
	
	/**
	 * update a gauge record
	 * @param $_REQUEST['field_names']
	 * @param $_REQUEST['field_values']
	 */
	function update_gauge($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$field_values = $_REQUEST['field_values'];
		$gauge_id = $_REQUEST['gauge_id'];
		$values = "";
		
		for ($i = 0; $i < count($field_names); $i++) {
			$values .= $field_names[$i]."='".$field_values[$i];
			if ($i+1 != count($field_names)) {
				$values .= "', ";
			} else {
				$values .= "' ";
			}
		}
		
		$sql = "UPDATE
			[dbo].[gauge_material_types]
		SET
			$values
		WHERE
			ID = ".$gauge_id;
		
		//die($sql);
		$db->query($sql);
		
		$res->success = 1;
		
		echo json_encode($res);
		
	}
	
	/**
	 * update a gauge instruction
	 * @param $_REQUEST['field_names']
	 * @param $_REQUEST['field_values']
	 * @param $_REQUEST['instruction_id']
	 */
	function update_instruction($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$field_values = $_REQUEST['field_values'];
		$instruction_id = $_REQUEST['instruction_id'];
		$values = "";
		
		for ($i = 0; $i < count($field_names); $i++) {
			$values .= $field_names[$i]."='".$field_values[$i];
			if ($i+1 != count($field_names)) {
				$values .= "', ";
			} else {
				$values .= "' ";
			}
		}
		
		$sql = "UPDATE 
					[dbo].[gauge_material_types_instructions]
   				SET 
				    $values
				WHERE 
				    ID = ".$instruction_id;
		
		//die($sql);
		$db->query($sql);
		
		$res->success = 1;
		
		echo json_encode($res);
		
	}
	
	/**
	 * insert an instruction
	 * @param $_REQUEST['field_names']
	 * @param $_REQUEST['field_values']
	 */
	function insert_instruction($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$field_values = $_REQUEST['field_values'];
	
		$sql = "INSERT INTO	[dbo].[gauge_material_types_instructions]
					(".implode(", ", $field_names).")
				VALUES
					('".implode("', '", $field_values)."')";
	
		//die($sql);
		$db->query($sql);
	
		$res->success = 1;
	
		echo json_encode($res);
	
	}
	
	/**
	 * get extra data for gaugings
	 * @param unknown $request
	 * @param unknown $db
	 */
	function get_gauge_machine_cycle_parts_extra_data($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$implodeArray = implode ( ', ', $field_names );
		
		$sql = "SELECT
					$implodeArray
				FROM
					[dbo].[gauge_machine_cycle_parts_extra_data]";
		
		//die($sql);
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	/**
	 * insert extra data for a part gauging
	 * @param $_REQUEST['field_names']
	 * @param $_REQUEST['field_values']
	 */
	function insert_gauge_machine_cycle_parts_extra_data($request, $db) {
		$res = new StdClass();
		$field_names = $_REQUEST['field_names'];
		$field_values = $_REQUEST['field_values'];

		array_push($field_names, "operator_ID");
		array_push($field_values, $_SESSION['op_ID']);
		
		$names = implode(", ", $field_names);
		
		for($i = 0; $i < count($field_values); $i++) {
			if (is_array($field_values[$i])) {
				$field_values[$i] = json_encode($field_values[$i]);
			}
		}
		
		$values = "'".implode("', '", fixDB($field_values))."'";
		
		$sql = "INSERT INTO 
					[dbo].[gauge_machine_cycle_parts_extra_data]
						(".$names.")
				VALUES
					(".$values.")";
		
	//die($sql);
		$db->query($sql);
		
		$res->success = 1;
		
		echo json_encode($res);
	}
	
	/**
	 * parse the mahrmmq file
	 * @param $_REQUEST["file"]
	 * @param $_REQUEST["machine_cycle_part_ID"]
	 */
	function parse_mahrmmq($request, $db) {
		$res = new StdClass();
		$file_name = $_REQUEST["file"];
		$machine_cycle_part_ID = $_REQUEST["machine_cycle_part_ID"];
		$passed = $_REQUEST["passed"];

		$sql = "SELECT COALESCE(MAX(sample_group + 1), '1') AS sample_group
				FROM gauge_mahrmmq
				WHERE machine_cycle_part_ID = $machine_cycle_part_ID";

		$res = $db->query($sql);

		$sample_group = $res[0]["sample_group"];
		
		try {
			//$row = 1;
			
			if ( !file_exists($file_name) ) {
				throw new Exception('File not found.');
			}
			
			if (($handle = fopen($file_name, "r")) !== FALSE) {
				$flag = true; // to hide the header
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					if($flag) { $flag = false; continue; }
					$num = count($data);					
					//echo "<p> $num fields in line $row: <br /></p>\n";
					//$row++;
					$field_values = array();
					for ($c=0; $c < $num; $c++) {
						//echo $data[$c] . "<br />\n";
						$field_values[] = $data[$c];					
					}
					array_push($field_values, $_SESSION["op_ID"]);
					array_push($field_values, $sample_group);
					array_push($field_values, $passed);
					$values = "'".implode("', '", fixDB($field_values))."'";
					
					$sql = "INSERT INTO
								[dbo].[gauge_mahrmmq]
									([machine_cycle_part_ID]
									,[name]
									,[datum]
									,[nominal]
									,[l_tol]
									,[u_tol]
									,[value]
									,[unit]
									,[feature]
									,[low_nat_border]
									,[type]
									,[no]
									,[date]
									,[time]
									,[operator]
									,operator_ID
									,sample_group
									,passed)
								VALUES
									($machine_cycle_part_ID, ".$values.")";
					//echo($sql);
					$db->query($sql);

				}
				
				fclose($handle);
			}
			$res = array();			
			$res["success"] = 1;			
			
		} catch (Exception $e) {
			$res = array();
			$res["success"] = 0;
			$res["error"] = $e->getMessage();			
		}
		echo json_encode($res);
	}
	
	/**
	 * get_mahrmmq_detail
	 * @param $_REQUEST["machine_cycle_part_ID"]
	 */
	function get_mahrmmq_detail($request, $db) {
		$res = new StdClass();
		$machine_cycle_part_ID = $_REQUEST["machine_cycle_part_ID"];
		
		$sql = "SELECT		
					[machine_cycle_part_ID]
					,[name]
					,[datum]
					,[nominal]
					,[l_tol]
					,[u_tol]
					,[value]
					,UNICODE ([unit]) AS unit
					,[feature]
					,[low_nat_border]
					,[type]
					,[no]
					,[date]
					,[time]
					,[operator]
				FROM
					[dbo].[gauge_mahrmmq]
				WHERE
					[machine_cycle_part_ID] = ".$machine_cycle_part_ID;
		//die($sql);
		
		$res = $db->query($sql);
		
		echo json_encode($res);
	}


?>