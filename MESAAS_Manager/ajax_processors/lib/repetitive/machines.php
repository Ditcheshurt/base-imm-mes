<?php

require_once("../../init.php");

$action = $_REQUEST ['action'];

call_user_func ( $action, $_REQUEST, $db );
function get_machines($request, $db) {
	$res = new StdClass ();
	
	$sql = "SELECT 
				[ID]
				,[machine_type_ID]
				,[machine_name]
				,[machine_number]
				,[machine_group_ID]
				,[op_order]
				,[up]
				,[current_tool_ID]
				,[current_PLC_tool_ID]
				,[alt_tool_ID_set_time]
				,[current_std_pack]
				,[ignored]
				,[ignore_reason]
				,[ignore_time]
				,[ignore_operator_ID]
				,[queue_type]
				,[per_part_printer_ID]
				,[rack_printer_ID]
				,[PLC_ID]
				,[PLC_file_address]
				,[station_IP_address]
				,[last_cycle_time]
				,[img_src]
				,[master_machine_ID]
				,[current_operator_ID]
			FROM 
				[dbo].[machines] m								
			ORDER BY 
				m.[machine_number]";
	
	$res = $db->query ( $sql );
	
	echo json_encode ( $res );
}
function update_machine($request, $db) {
	$res = new StdClass ();
	$field_names = $_REQUEST ['field_names'];
	$field_values = $_REQUEST ['field_values'];
	$values = "";
	
	for($i = 0; $i < count ( $field_names ); $i ++) {
		$values .= $field_names [$i] . "='" . $field_values [$i];
		if ($i + 1 != count ( $field_names )) {
			$values .= "', ";
		} else {
			$values .= "' ";
		}
	}
	
	$sql = "UPDATE 
					[dbo].[lot_material_types]
   				SET 
				    $values
				WHERE 
				    ID = " . $request ["lot_id"];
	// die($sql);
	$res = $db->query ( $sql );
	
	get_lot_material_types ( $request, $db );
}
function get_machine_tool_parts($request, $db) {
	$res = new StdClass ();
	$machine_id = $_REQUEST ['machine_id'];
	$field_names = $_REQUEST ['field_names'];
	
	$implodeArray = implode ( ', ', $field_names );
	
	$sql = "SELECT
		$implodeArray
	FROM					
		[dbo].[machines] m 
	LEFT JOIN
		[dbo].[machine_tools] mt ON m.ID = mt.machine_ID
	LEFT JOIN
		[dbo].[tools] t ON t.ID = mt.tool_ID
	LEFT JOIN
		[dbo].[tool_parts] tp ON t.ID = tp.tool_ID
	LEFT JOIN
		[dbo].[parts] p ON p.ID = tp.part_ID
	WHERE
		m.ID = $machine_id
	ORDER BY
		t.tool_description";
	
	// die($sql);
	$res = $db->query ( $sql );
	
	echo json_encode ( $res );
}
function get_machine_cycle_parts_by_serial($request, $db) {
	$res = new StdClass ();
	$serial_number = $_REQUEST ['serial_number'];
	$field_names = $_REQUEST ['field_names'];
	
	$implodeArray = implode ( ', ', $field_names );
	
	$sql = "SELECT
				[tool_part_ID]
				,$implodeArray
			FROM
				[dbo].[machine_cycle_parts]
			WHERE
				[serial_number] = '".$serial_number."'";
	
	//die($sql);
	$res = $db->query ( $sql );
	
	// get tools
	for ($i = 0; $i < count($res); $i++) {
		
		$sql = "SELECT
					*
				FROM
					tool_parts tp
				JOIN
					tools t ON t.ID = tp.tool_ID				
				WHERE
					tp.ID = ".$res[$i]["tool_part_ID"];
		
		$res[$i]["tool"] = $db->query($sql);
		
		// get tool machine
		for ($t = 0; $t < count($res[$i]["tool"]); $t++) {
			
			$sql = "SELECT 
						m.*
					FROM
						machine_tools mt 
					JOIN
						machines m ON m.ID = mt.machine_ID
					WHERE
						mt.tool_ID = ".$res[$i]["tool"][$t]["ID"];
			
			$res[$i]["tool"][$t]["machine"] = $db->query($sql);
		}
	}
	
	echo json_encode ( $res );
}

?>