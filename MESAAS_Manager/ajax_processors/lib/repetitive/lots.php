<?php

    require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_lot_material_types($request, $db) {
		$res = new StdClass();		
		$field_names = $request['field_names'];
				
		$implodeArray = implode(', ', $field_names);
		
		$sql = "SELECT 
					$implodeArray
				FROM 
					[dbo].[lot_material_types]									
				ORDER BY 
					[description]";

		$res = $db->query($sql);

		echo json_encode($res);

	}
	
	function get_lot_machines($request, $db) {
		$res = new StdClass();
		$lot_id = $request['lot_id'];
		$field_names = $request['field_names'];
		
		$implodeArray = implode(', ', $field_names);
		
		$sql = "SELECT
					$implodeArray
				FROM
					[dbo].[lot_machines] lm
				JOIN 
					[dbo].[machines] m 
					ON m.ID = lm.machine_ID
				WHERE
					[lot_material_type_ID] = ".$lot_id;
		//die($sql);
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	function get_lot_tool_parts($request, $db) {
		$res = new StdClass();
		$lot_id = $request['lot_id'];
		$machine_id = $request['machine_id'];
		$field_names = $request['field_names'];
	
		$implodeArray = implode(', ', $field_names);
	
		$sql = "SELECT
			$implodeArray
		FROM
			[dbo].[lot_tool_parts] ltp
		LEFT JOIN
			[dbo].[tool_parts] tp ON tp.ID = ltp.tool_part_ID
		LEFT JOIN
			[dbo].[parts] p ON p.ID = tp.part_ID
		LEFT JOIN
			[dbo].[tools] t ON t.ID = tp.tool_ID
		LEFT JOIN
			[dbo].[machine_tools] mt ON t.ID = mt.tool_ID
		LEFT JOIN
			[dbo].[machines] m ON m.ID = mt.machine_ID
		WHERE
			ltp.lot_material_type_ID = $lot_id
		AND
			m.ID = $machine_id
		ORDER BY
			t.tool_description";
	
		//die($sql);
		$res = $db->query($sql);
	
		echo json_encode($res);
	
	}
	
	function update_lot_material_types($request, $db) {
		$res = new StdClass();
		$field_names = $request['field_names'];
		$field_values = $request['field_values'];
		$values = "";
		
		for ($i = 0; $i < count($field_names); $i++) {
			$values .= $field_names[$i]."='".$field_values[$i];
			if ($i+1 != count($field_names)) {
				$values .= "', ";
			} else {
				$values .= "' ";
			}
		}
		
		$sql = "UPDATE 
					[dbo].[lot_material_types]
   				SET 
				    $values
				WHERE 
				    ID = ".$request["lot_id"];
		//die($sql);
		$res = $db->query($sql);

		get_lot_material_types($request, $db);
	}
	
	function insert_lot_material_type($request, $db) {
		$res = new StdClass();
		$field_names = $request['field_names'];
		$field_values = $request['field_values'];
	
		$sql = "INSERT INTO	[dbo].[lot_material_types]
					(".implode(", ", $field_names).")
				VALUES
					('".implode("', '", $field_values)."')";
		//die($sql);
		$db->query($sql);
	
		$res->success = 1;
	
		echo json_encode($res);
	}
	
	function insert_lot_machine($request, $db) {
		$res = new StdClass();
		$field_names = $request['field_names'];
		$field_values = $request['field_values'];		
		
		$sql = "INSERT INTO	[dbo].[lot_machines]
					(".implode(', ', $field_names).")
				VALUES
					(".implode(', ', $field_values).")";
		//die($sql);
		$db->query($sql);
		
		$res->success = 1;

		echo json_encode($res);
	}
	
	function insert_lot_tool_part($request, $db) {
		$res = new StdClass();
		$field_names = $request['field_names'];
		$field_values = $request['field_values'];
	
		$sql = "INSERT INTO	[dbo].[lot_tool_parts]
					(".implode(', ', $field_names).")
				VALUES
					(".implode(', ', $field_values).")";
		//die($sql);
		$db->query($sql);
	
		$res->success = 1;
	
		echo json_encode($res);
	}
	
	function update_lot_tool_part($request, $db) {
		$res = new StdClass();
		$lot_tool_part_id = $request["lot_tool_part_id"];
		$field_names = $request['field_names'];
		$field_values = $request['field_values'];
		$values = "";
	
		for ($i = 0; $i < count($field_names); $i++) {
			$values .= $field_names[$i]."='".$field_values[$i];
			if ($i+1 != count($field_names)) {
				$values .= "', ";
			} else {
				$values .= "' ";
			}
		}
	
		$sql = "UPDATE
			[dbo].[lot_tool_parts]
		SET
			$values
		WHERE
			ID = ".$lot_tool_part_id;
	//die($sql);
		$db->query($sql);
		
		$res->success = 1;
		
		echo json_encode($res);
	}
	
	function delete_lot_machine($request, $db) {
		$res = new StdClass();		
	
		$sql = "DELETE FROM 
					[dbo].[lot_machines]
				WHERE 
					ID = ".$request["ID"];
		
		//die($sql);
		$db->query($sql);
	
		$res->success = 1;
	
		echo json_encode($res);
	}
	
	function delete_lot_tool_part($request, $db) {
		$res = new StdClass();
		$lot_tool_part_id = $request['lot_tool_part_id'];
	
		$sql = "DELETE FROM
			[dbo].[lot_tool_parts]
		WHERE
			[ID] = $lot_tool_part_id";
		
	//die($sql);
		$res = $db->query($sql);
	
		echo json_encode($res);
	}


?>