<?php

require_once("init.php");

$action = $_REQUEST['action'];

if ($action == "logout") {
    unset($_SESSION['op_ID']);
    $action = "check_session";
}

if ($action == "check_session") {
    $res = new StdClass();

    if (is_session_started() === FALSE) {

        $res->session_matched = true;
        $res->new_session = true;
    } else {
        if (isset($_COOKIE['PHPSESSID'])) {
            if (session_id() == $_COOKIE['PHPSESSID']) {
                $res->session_matched = true;
                $res->new_session = false;
            } else {
                $res->session_matched = false;
            }
        } else {
            $res->session_matched = false;
        }
    }

    $res->session = session_id();

    if (isset($_SESSION['op_ID'])) {
        //get operator info
        $sql = "SELECT ID, name, email FROM MES_COMMON.dbo.operators WHERE id = " . $_SESSION['op_ID'];
        $ops = $db->query($sql);
        $res->operator = $ops[0];

        //get operator roles
        $sql = getOperatorRoles("WHERE o.ID = " . $_SESSION['op_ID']);
        $res->operator_roles = $db->query($sql);
    } else {
        $res->operator = array("name" => "", "ID" => 0);
        $res->operator_roles = array();
    }

    echo json_encode($res);
}

if ($action == "verify_login") {
    $res = new StdClass();
    $r = $_REQUEST;

    $r['email'] = fixDB($r['email']);
    $r['password'] = fixDB($r['password']);
    $r['password'] = unencrypt($r['password']);


    if ($GLOBALS['APP_CONFIG']['auth_type'] == "domain") {
        $sql = "SELECT ID, name, email, domain_user FROM MES_COMMON.dbo.operators WHERE email = '" . $r['email'] . "'";
        $op = $db->query($sql);
        if ($op) {
            if (count($op) > 0) {
                $domain_user = $op[0]['domain_user'];
                $res->domain_user = $domain_user;

                try {
                    $connect = ldap_connect($GLOBALS['APP_CONFIG']['dns_server'], 389);
                } catch (Exception $e) {
                    $connect = false;
                }
                if ($connect) {
                    $res->debug = "Connected to: " . $GLOBALS['APP_CONFIG']['dns_server'];
                    $bind = false;
                    try {
                        $bind = ldap_bind($connect, $GLOBALS['APP_CONFIG']['domain'] . "\\" . $domain_user, $r['password']);
                    } catch (Exception $ex) {
                        $bind = false;
                    }
                    if ($bind) {
                        $sql = "SELECT ID, name, email FROM MES_COMMON.dbo.operators WHERE domain_user = '" . $domain_user . "'";
                        $res->operator = $db->query($sql);
                        if (count($res->operator) > 0) {
                            $_SESSION['op_ID'] = $res->operator[0]['ID'];
                        }
						

                        //get operator roles
                        $sql = getOperatorRoles("WHERE operator_ID IN (SELECT ID FROM MES_COMMON.dbo.operators WHERE domain_user = '" . $domain_user . "')");
                        $res->operator_roles = $db->query($sql);
                    } else {
                        $res->operator = array();
                        $res->operator_roles = array();
                        $res->message = "Unable to bind";
                    }
                } else {
                    $res->operator = array();
                    $res->operator_roles = array();
                    $res->message = "Unable to connect";
                }


            } else {
                $res->operator = array();
                $res->operator_roles = array();
            }
        } else {
            $res->operator = array();
            $res->operator_roles = array();
        }
    } else {

        $sql = "SELECT ID, name, email FROM MES_COMMON.dbo.operators WHERE email = '" . $r['email'] . "' AND password = '" . $r['password'] . "'";
        $res->operator = $db->query($sql);
        if (count($res->operator) > 0) {
            $_SESSION['op_ID'] = $res->operator[0]['ID'];
            $res->op_session_set = $_SESSION['op_ID'];
        }

        //get operator roles
        $sql = getOperatorRoles("WHERE operator_ID IN (SELECT ID FROM MES_COMMON.dbo.operators WHERE email = '" . $r['email'] . "' AND password = '" . $r['password'] . "')");
        $res->operator_roles = $db->query($sql);
    }

    echo json_encode($res);
}

function unencrypt($s)
{
    $a = str_split($s);
    $b = array();
    for ($i = 0; $i < count($a); $i++) {
        array_push($b, chr(ord($a[$i]) - 1));
    }

    return join("", $b);
}

function is_session_started()
{
    if (php_sapi_name() !== 'cli') {
        if (version_compare(phpversion(), '5.4.0', '>=')) {
            return session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE;
        } else {
            //echo session_id();
            return session_id() === '' ? FALSE : TRUE;
        }
    }
}

function getOperatorRoles($whereClause)
{

    $sql = "SELECT
                r.name as role,
                CASE WHEN( ( SELECT COUNT(role_id)
                            FROM MES_COMMON.dbo.operator_roles opr
                            INNER JOIN MES_COMMON.dbo.roles r ON opr.role_ID = r.ID
                            WHERE r.name = 'editor' AND opr.operator_ID = o.ID ) > 0 )
                    THEN 1
                    ELSE 0
                END AS editor
                FROM MES_COMMON.dbo.operators o
                INNER JOIN MES_COMMON.dbo.operator_roles opr ON o.ID = opr.operator_ID
                INNER JOIN MES_COMMON.dbo.roles r ON opr.role_ID = r.ID 
                {$whereClause}";

    return $sql;
}

?>