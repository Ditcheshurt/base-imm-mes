<?php

    require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "get_data") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		// station status data
		$sql = "SELECT line_id, station_id, station_order, ip_address,
					(SELECT TOP (1) m.build_order
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID = s.id)
								AND (mh.exit_time IS NULL)
								AND status = 1
								AND m.line_id = {$r['line_id']}
                            ORDER BY mh.entry_time DESC) AS current_module,
					(SELECT TOP (1) m.id
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID = s.id)
								AND (mh.exit_time IS NULL)
								AND status = 1
								AND m.line_id = {$r['line_id']}
                            ORDER BY mh.entry_time DESC) AS current_module_id,
                    (SELECT TOP (1) m.build_order
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID = s.id)
								AND (mh.exit_time IS NOT NULL)
								AND status = 1
								AND m.line_id = {$r['line_id']}
                            ORDER BY mh.entry_time DESC) AS last_module,
                    (SELECT TOP (1) m.id
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID = s.id)
								AND (mh.exit_time IS NOT NULL)
								AND status = 1
								AND m.line_id = {$r['line_id']}
                            ORDER BY mh.entry_time DESC) AS last_module_id,
                    (SELECT TOP (1) m.build_order
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID =
                                (SELECT TOP (1) station_id
                                    FROM line_stations
                                    WHERE (station_id < s.id)
										AND status = 1
                                        ORDER BY station_id DESC))
							AND m.line_id = {$r['line_id']}
							ORDER BY mh.entry_time DESC) AS next_module,
                    (SELECT TOP (1) m.id
                            FROM module_station_history AS mh INNER JOIN
                                modules AS m ON m.ID = mh.module_ID
                            WHERE (mh.station_ID =
                                (SELECT TOP (1) station_id
                                    FROM line_stations
                                    WHERE (station_id < s.id)
										AND status = 1
                                        ORDER BY station_id DESC))
							AND m.line_id = {$r['line_id']}
							ORDER BY mh.entry_time DESC) AS next_module_id
			FROM line_stations AS s
			WHERE (line_id = {$r['line_id']})
			ORDER BY station_order";

		$res->station_status = $db->query($sql);

		/*
		// lines data
		$sql = "select top 200 m.id as module_id
				  ,[build_order]
				  ,[sequence]
				  ,[VIN]
				  ,[line_ID]
				  ,isnull(ms.[status_desc], m.status) as [status]
				  ,[recvd_time]
				  ,[loaded_time]
				  ,[built_time]
				  ,isnull(o.name, [operator_id]) as [operator]
				  ,[pallet_number]
				  ,[reject_status]
				  ,[reject_time]
				  ,[reject_original_build_order]
				  ,[pallet_position]
			  from [modules] m left join module_statuses ms on m.status = ms.ID
				left join [MES_COMMON].[dbo].operators o on o.ID = m.operator_id
			  where line_id = '".$r["line_id"]."'
			  and station_id = '".$r["station_id"]."'
			  order by m.id desc";

		$res->lines = $db->query($sql);
		*/

		// broadcast data
		$sql = "SELECT
				(SELECT COUNT(*) FROM modules WHERE line_ID = ".$r["line_id"]." AND recvd_time >= convert(varchar(10), GETDATE(), 101)) as broadcasts_today,
				(SELECT MAX(recvd_time) FROM modules WHERE line_ID = ".$r["line_id"].") as last_broadcast,
				SUM(CASE WHEN status = 0 AND line_ID = ".$r["line_id"]." THEN 1 ELSE 0 END) as broadcasts_inqueue,
				SUM(CASE WHEN status = 1 AND line_ID = ".$r["line_id"]." THEN 1 ELSE 0 END) as broadcasts_inbuild,
				(SELECT MAX(built_time) FROM modules WHERE line_ID = ".$r["line_id"]." AND built_time IS NOT NULL) as broadcast_lastbuild,
				(SELECT COUNT(*) FROM modules WHERE line_ID = ".$r["line_id"]." AND built_time >= convert(varchar(10), GETDATE(), 101)) as broadcasts_builttoday
			FROM modules WHERE status < 2";

		$res->broadcast_status = $db->query($sql);

		// production qty
		$sql = "SELECT lines.line_code, DATEPART(HOUR, built_time) as [hour],
					count(DATEPART(HOUR, built_time)) as qty_built
				FROM modules join lines on line_id = lines.id
				WHERE line_ID = ".$r["line_id"]."
					AND dbo.getProdDate(".$r["line_id"].", built_time) >= CONVERT(VARCHAR(10), GETDATE(), 101)
				GROUP BY lines.line_code, DATEPART(HOUR, built_time)";

		$res->production_qty = $db->query($sql);

		$sql = "SELECT w.ID, m.build_order, m.VIN, station_desc, m.sequence,
					ISNULL(w.internal_serial, w.external_serial) as serial_number,
					w.built_time, last_completed_station_ID, lines.line_code
				FROM wip_parts w
					JOIN modules m ON w.used_by_module_ID = m.ID AND m.status < 2
					JOIN line_stations ON line_stations.id = last_completed_station_ID
					JOIN lines ON lines.id = line_stations.line_id
				WHERE w.line_ID in
					(SELECT id FROM lines WHERE line_type = 2
						AND main_line_ID = ".$r["line_id"].")";

		$res->wip_parts = $db->query($sql);

		$sql = "SELECT k.id, k.container_ID, m.build_order, k.status, k.time_queued
				FROM kit_container_history k
					LEFT JOIN modules m on m.id = k.mod_ID
				WHERE k.status < 2 AND m.line_ID = {$r["line_id"]}
				ORDER BY k.container_ID";

		$res->kit_boxes = $db->query($sql);

		$sql = 'SELECT [id],[line_ID],[first_shift_qty],[second_shift_qty],[third_shift_qty],[last_updated]
				FROM [production_quantity_required_by_shift] WHERE line_id = '.$r['line_id'];

		$res->shift_goals = $db->query($sql);


		$sql = "SELECT *
				 FROM (SELECT TOP 15 m.sequence, m.build_order, m.VIN, m.reject_status
					FROM modules m
					WHERE m.status = 0 AND m.line_ID = {$r["line_id"]}
					ORDER BY m.build_order) as v
				 ORDER BY build_order DESC ";

		$res->upcoming = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "get_module_wip") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT
		            wp.ID,
		            wp.part_ID,
		            wps.part_desc,
		            wp.internal_serial,
		            wp.built_time,
					wp.line_ID,
					wp.used_by_wip_part_ID,
					wp.used_time
				FROM
					dbo.wip_parts wp
				LEFT JOIN
					wip_part_setup wps ON wps.id = wp.part_ID
				WHERE
					used_by_module_id = ".$r['module_id'];

		$res->module_wip = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "get_module_detail") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT wp.ID, wp.part_ID, wps.part_desc, wp.internal_serial,
		            wp.line_ID, wp.used_by_wip_part_ID, m.build_order, k.container_id
				FROM dbo.wip_parts wp
				LEFT JOIN
					wip_part_setup wps ON wps.id = wp.part_ID
				JOIN modules m on m.id = wp.used_by_module_id
				JOIN kit_container_history k on k.mod_id = m.id
				WHERE
					used_by_module_id = ".$r['module_id'];
		//die($sql);
		$res->module_detail = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "rebuild_wip_part") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		//echo 'FEATURE DISABLED';
		//die;
		$sql = "UPDATE wip_parts
				  SET used_by_wip_part_ID = NULL, used_by_module_id = NULL, used_time = NULL
				  WHERE used_by_wip_part_ID = ".$r['wip_part_id'];
		$db->query($sql);

		$sql = "DELETE FROM wip_parts
				WHERE id = ".$r['wip_part_id'].'; SELECT 1;';

		$res->success = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'move_back') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		//echo 'FEATURE DISABLED';
		//die;

		$sql = 'DELETE FROM dbo.module_station_history
				WHERE module_ID IN
					(SELECT id FROM modules
						WHERE build_order = '.$r['build_order'].'
							AND line_ID = '.$r['line_id'].'
							AND station_ID ='.$r['station_id'].');
				DELETE FROM dbo.module_station_instruction_history " & _
				WHERE module_ID IN
					(SELECT id FROM modules
						WHERE build_order = '.$r['build_order'].'
							AND line_ID = '.$r['line_id'].'
							AND station_ID = '.$r['station_id'].');';

		$res->success = $db->query($sql);

		echo json_encode($res);

	}

	if ($action == "delete_kitbox") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = 'DELETE FROM kit_container_history
                WHERE ID = '.$r['id'];

		$res->success = $db->query($sql);

		echo json_encode($res);

	}

	if ($action == "set_shift_goals") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = 'UPDATE [dbo].[production_quantity_required_by_shift]
				SET [first_shift_qty] = '.$r['first_shift'].'
					,[second_shift_qty] = '.$r['second_shift'].'
					,[third_shift_qty] = '.$r['third_shift'].'
					,[last_updated] = getDate()
				WHERE line_id = '.$r['line_id'];

		$res->success = $db->query($sql);

		echo json_encode($res);
	}

?>