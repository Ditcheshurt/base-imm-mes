<?php

    require_once("../init.php");

    /*
    if (isset($_REQUEST['database'])) {
        $db->catalog = $_REQUEST['database'];
    }

    if (isset($_REQUEST['process_type'])) {
        if ($_REQUEST['process_type'] == 'SEQUENCED') {
            $db->catalog = 'NYSUS_SEQUENCE_MES';
        } else if ($_REQUEST['process_type'] == 'BATCH') {
            $db->catalog = 'NYSUS';
        }
    }*/

    $action = $_REQUEST['action'];

    if ($action == "load_cells") {
	    global $db;
	    $res = new StdClass();
	
	    $sql = "SELECT ID, cell_desc
				    FROM cells
				    WHERE cell_type = 1
				    ORDER BY cell_desc";
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);
	
    }

    if ($action == "load_parts") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;
	
	    $sql = "SELECT ID, part_number, part_desc, part_desc_short
				    FROM part_setup
				    WHERE cell_ID = ".$r['cell_ID']."
				    ORDER BY part_number";
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);
	
    }

    if ($action == "load_part_quantities") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;
	
	    $sql = "SELECT 
		    ps.ID AS part_ID,
		    ps.part_number,
		    CASE WHEN [alternate_std_pack_qty] IS NULL 
		    THEN ct.std_pack_qty 
		    ELSE [alternate_std_pack_qty] 
		    END AS pack_qty 
	    FROM part_setup ps 
		    LEFT JOIN cells c ON ps.cell_ID = c.ID 
		    LEFT JOIN container_types ct ON ct.ID = c.container_type_ID
	    WHERE ps.ID = ".$r['part_ID'];
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "move_up") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;

	    $sql = "EXEC sp_MESAAS_BATCH_EDITOR_moveBatchSchedule ".$r['batch_ID'].", 'up'";
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "move_down") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;

	    $sql = "EXEC sp_MESAAS_BATCH_EDITOR_moveBatchSchedule ".$r['batch_ID'].", 'down'";
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "load_batch_totals") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;

	    $sql = "SELECT 
					    part_number, part_desc_short,
					    SUM(rack_qty) AS build_total
				    FROM batches b
					    JOIN part_setup ps on ps.ID = b.part_ID
				    WHERE b.end_time IS NULL
					    AND ps.cell_ID = ".$r['cell_ID']."
				    GROUP BY part_number, part_desc_short";
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "load_batches") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;
	
	    $sql = "SELECT 
					    ps.ID AS part_ID,
					    b.ID AS batch_ID, 
					    part_number, 
					    rack_qty AS pack_qty 
				    FROM batches b 
					    JOIN part_setup ps 
					    ON ps.ID = b.part_ID
				    WHERE end_time IS NULL
					    AND b.cell_ID = ".$r['cell_ID']."
				    ORDER BY batch_order ASC";
	
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "insert_batch") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;

	    $sql = "INSERT INTO batches 
					    (schedule_time, scheduled_by, 
					    production_date, cell_ID, 
					    station_ID, batch_order, part_ID, rack_qty)  
				    VALUES
					    (getDate(),
					    ".$_SESSION['op_ID'].",
					    getDate(),".$r['cell_ID'].",1,					
					    (SELECT 
				        CASE
				        WHEN EXISTS(
				            SELECT TOP 1 batch_order+1 FROM batches WHERE end_time IS NULL ORDER BY batch_order DESC) 
				        THEN (
						    SELECT TOP 1 batch_order+1 FROM batches WHERE end_time IS NULL ORDER BY batch_order DESC)
					    ELSE  1
				        END),
					    ".$r['part_ID'].
					    ",".$r['pack_qty'].
					    ")";

	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

    if ($action == "delete_batch") {
	    global $db;
	    $res = new StdClass();
	    $r = $_REQUEST;

	    $sql = "UPDATE 
					    batches 
				    SET 
					    end_time = getDate()
				    WHERE 
					    ID = ".$r['batch_ID'];
				
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);	
    }

?>