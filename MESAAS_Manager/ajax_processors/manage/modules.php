<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];
    $r = $_REQUEST;

    if ($action == 'list_modules') {
	    global $db;
	    $res = new StdClass();

	    $status = $_REQUEST['status'];
	    $status_filter = "";
	    if ($status != "-1") {
		    $status_filter = " AND m.status = ".$status." ";
	    }

	    //module data
	    $sql = "SELECT
				    m.ID,
				    m.sequence,
				    m.build_order,
				    m.VIN,
				    l.line_code,
				    m.loaded_time,
				    m.built_time,
				    ms.status_desc,
				    m.status,
                    m.pallet_number,
                    m.pallet_position
			    FROM
			    modules m
				    LEFT JOIN
					    lines l ON l.ID = m.line_ID
				    LEFT JOIN
				    module_statuses ms ON ms.ID = m.status
			    WHERE
				    m.recvd_time >= '".$_REQUEST['start_date']." 00:00:00'
				    AND m.recvd_time <= '".$_REQUEST['end_date']." 23:59:59'
                AND l.ID = ".$_REQUEST['line_id']."
				    ".$status_filter."
			    ORDER BY
				    m.build_order DESC;";
	    //die($sql);
	    $res = $db->query($sql);
	    if (!$res) {
		    $res = array();
	    }
	    echo json_encode($res);
    }

    if ($action == 'list_components') {
	    global $db;
	    $res = new StdClass();
	    $module_ID = $_REQUEST['module_ID'];

	    // get components
	    $sql = "SELECT
					    ID,
					    part_number,
					    qty
				    FROM
					    components_in
				    WHERE
					    veh_id = ".$module_ID."
				    ORDER BY part_number ASC;";

	    $res = $db->query($sql);
	    echo json_encode($res);
    }

    if ($action == 'rebuild_module') {
	    // set test to false to activate
	    $test = false;

	    global $db;
	    $res = new StdClass();
	    $res->success = false;

	    $module_id = $_REQUEST['module_id'];
	    $sql = "EXEC dbo.sp_MESAAS_SEQUENCE_rejectSetDisposition @module_ID = {$module_id}, @disposition = 'REBUILD', @operator_ID = 1005, @station_ID = 99";

	    $res = $db->query($sql);
	    echo json_encode($res);
    }

    if ($action == 'rebuild_module_immediate') {
	    // set test to false to activate
	    $test = false;

	    global $db;
	    $res = new StdClass();
	    $res->success = false;

	    $module_id = $_REQUEST['module_id'];
	    $sql = "EXEC dbo.sp_MESAAS_SEQUENCE_rejectSetDisposition @module_ID = {$module_id}, @disposition = 'REBUILD', @operator_ID = 1005, @station_ID = 99";
	    $res = $db->query($sql);

	    if ($res) {
		    $sql = "EXEC dbo.sp_MESAAS_SEQUENCE_buildFromHoldQueue @module_ID = {$module_id}";
		    $res = $db->query($sql);
	    }

	    echo json_encode($res);
    }

    if ($action == 'manual_build') {
	    global $db;
	    $res = new StdClass();

	    $sql = "EXEC dbo.sp_MESAAS_SEQUENCE_markModuleManuallyBuilt @module_ID = ".$_REQUEST['module_ID'];

	    $res = $db->query($sql);
	    echo json_encode($res);
    }

    if ($action === 'get_broadcast_data') {
	    global $db;
	    $res = new StdClass();

	    // get module data
	    $sql = "SELECT
				    ID,
				    VIN,
				    sequence
			    FROM
				    modules
			    WHERE
				    ID = {$_REQUEST['module_ID']};";
	    $m = $db->query($sql);
	    if ($m) {
		    $res->module = $m[0];
	    } else {
		    $res->module = $m;
	    }
	
	    // get broadcasted components
	    $sql = "SELECT
				    ID,
				    part_number,
				    qty
			    FROM
				    components_in
			    WHERE
				    veh_id = {$_REQUEST['module_ID']}
			    ORDER BY
				    part_number ASC;";
	    //die($sql);
	    $res->components = $db->query($sql);

	    // get raw broadcast
	    $sql = "SELECT
				    b.ID,
				    b.VIN,
				    b.sequence,
				    b.line_ID,
				    b.raw_data
			    FROM
				    broadcasts_in b
				    LEFT JOIN modules m ON m.VIN = b.VIN
			    WHERE
				    m.ID = {$_REQUEST['module_ID']};";
	    $b = $db->query($sql);
	    if ($b) {
		    $res->broadcast = $b[0];
	    } else {
		    $res->broadcast = $b;
	    }

	    // get component changes
	    $sql = "SELECT
				    ID,
				    qty,
				    mod_ID,
				    part_number,
				    added,
				    deleted
			    FROM
				    module_part_changes
			    WHERE
				    mod_ID = {$_REQUEST['module_ID']}
			    ORDER BY
				    part_number ASC,
				    ID ASC;";
	    $res->part_changes = $db->query($sql);

	    //get effective components
	    $sql = "DECLARE @module_ID INT = {$_REQUEST['module_ID']};

			    SELECT
				    ep.part_number,
				    ep.added
			    FROM (
				    (
					    SELECT
						    ISNULL(mpc.part_number, c.part_number) AS part_number,
						    mpc.added
					    FROM
						    components_in c
						    LEFT JOIN module_part_changes mpc ON dbo.getRootPartNumber(mpc.part_number) = dbo.getRootPartNumber(c.part_number)
					    WHERE
						    c.veh_id = @module_ID
						    AND (
							    mpc.deleted = 0
							    OR mpc.ID is NULL
						    )
				    ) UNION (
					    SELECT
						    mpc.part_number,
						    mpc.added
					    FROM
						    module_part_changes mpc
						    LEFT JOIN components_in c ON dbo.getRootPartNumber(c.part_number) = dbo.getRootPartNumber(mpc.part_number)
					    WHERE
						    mpc.mod_ID = @module_ID
						    AND mpc.added = 1
						    AND c.ID IS NULL
				    )
			    ) AS ep
			    ORDER BY
				    ep.part_number ASC;";
	    $res->effective_components = $db->query($sql);

	    echo json_encode($res);
    }

    if ($action === 'get_component') {
	    global $db;
	    $res = new StdClass();

	    // get component
	    $sql = "SELECT
				    ID,
				    part_number,
				    qty
			    FROM
				    components_in
			    WHERE
				    ID = {$_REQUEST['component_ID']};";
	    $c = $db->query($sql);
	    if ($c) {
		    $res= $c[0];
	    } else {
		    $res = $c;
	    }

	    echo json_encode($res);
    }

    if ($action === 'remove_component') {
	    global $db;
	    $module_ID = $part_number = $_REQUEST['module_ID'];
	    $part_number = $_REQUEST['part_number'];
	    //die($part_number);

	    // wipe out the current part changes
	    $sql = "DELETE
			    FROM
				    module_part_changes
			    WHERE
				    mod_ID = {$module_ID}
				    AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$part_number}');";
	    $res = $db->query($sql);

	    if ($res === null) {
		    // get the quantity for the component
		    $sql = "SELECT
					    ID,
					    qty
				    FROM
					    components_in
				    WHERE
					    veh_id = {$module_ID}
					    AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$part_number}')";
		    $components_in = $db->query($sql);

		    if ($components_in) {
			    // create a new delete change for the component
			    $sql = "INSERT INTO
						    module_part_changes (
							    mod_ID,
							    part_number,
							    added,
							    deleted,
							    qty
						    ) VALUES (
							    {$module_ID},
							    '{$part_number}',
							    0,
							    1,
							    {$components_in[0]['qty']}
						    );";
			    $res = $db->query($sql);

			    // delete the actual component
			    $sql = "DELETE
					    FROM
						    components_in
					    WHERE
						    ID = {$components_in[0]['ID']};";
			    $res = $db->query($sql);
		    } else {
			    $res = $components_in;
		    }
	    }

	    echo json_encode($res);
    }

    if ($action === 'update_component') {
	    global $db;
	    $module_ID = $part_number = $_REQUEST['module_ID'];
	    $original_part_number = $_REQUEST['original_part_number'];
	    $new_part_number = $_REQUEST['new_part_number'];

	    // wipe out the component changes
	    $sql = "DELETE
			    FROM
				    module_part_changes
			    WHERE
				    mod_ID = {$module_ID}
				    AND (
					    dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$original_part_number}')
					    OR dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$new_part_number}')
				    );";
	    $res = $db->query($sql);

	    if ($res === null) {
		    // get the component
		    $sql = "SELECT
					    ID,
					    qty
				    FROM
					    components_in
				    WHERE
					    veh_id = {$module_ID}
					    AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$original_part_number}')";
		    $components_in = $db->query($sql);

		    if ($components_in) {
			    // add a delete change
			    $sql = "INSERT INTO
						    module_part_changes (
							    mod_ID,
							    part_number,
							    added,
							    deleted,
							    qty
						    ) VALUES (
							    {$module_ID},
							    '{$original_part_number}',
							    0,
							    1,
							    {$components_in[0]['qty']}
						    );";
			    $res = $db->query($sql);

			    // add an add change
			    $sql = "INSERT INTO
						    module_part_changes (
							    mod_ID,
							    part_number,
							    added,
							    deleted,
							    qty
						    ) VALUES (
							    {$module_ID},
							    '{$new_part_number}',
							    1,
							    0,
							    {$components_in[0]['qty']}
						    );";
			    $res = $db->query($sql);

			    // update the component
			    $sql = "UPDATE
						    components_in
					    SET
						    part_number = '{$new_part_number}'
					    WHERE
						    ID = {$components_in[0]['ID']};";
			    $res = $db->query($sql);
		    } else {
			    $res = $components_in;
		    }
	    }

	    echo json_encode($res);
    }

    if ($action === 'add_component') {
	    global $db;
	    $module_ID = $part_number = $_REQUEST['module_ID'];
	    $part_number = $_REQUEST['part_number'];

	    // wipe out any changes for the part that we're trying to add
	    $sql = "DELETE
			    FROM
				    module_part_changes
			    WHERE
				    mod_ID = {$module_ID}
				    AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$part_number}');";
	    $delete_res = $db->query($sql);

	    if ($delete_res === null) {

		    // check for a matching part in the broadcast
		    $sql = "SELECT
					    ID,
					    part_number,
					    qty
				    FROM
					    components_in
				    WHERE
					    veh_id = {$module_ID}
					    AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$part_number}');";
		    $components_in = $db->query($sql);

		    if ($components_in) {
			    // if we find a matching root part number, we check to see if it has a different rev level
			    if ($components_in[0]['part_number'] != $part_number) {
				    // if the rev level is different, we treat it like a rev change
				    // add a delete change
				    $sql = "INSERT INTO
							    module_part_changes (
								    mod_ID,
								    part_number,
								    added,
								    deleted,
								    qty
							    ) VALUES (
								    {$module_ID},
								    '{$components_in[0]['part_number']}',
								    0,
								    1,
								    {$components_in[0]['qty']}
							    );";
				    $res = $db->query($sql);

				    // add an add change
				    $sql = "INSERT INTO
							    module_part_changes (
								    mod_ID,
								    part_number,
								    added,
								    deleted,
								    qty
							    ) VALUES (
								    {$module_ID},
								    '{$part_number}',
								    1,
								    0,
								    {$components_in[0]['qty']}
							    );";
				    $res = $db->query($sql);

				    // update the component
				    $sql = "UPDATE
							    components_in
						    SET
							    part_number = '{$part_number}'
						    WHERE
							    ID = {$components_in[0]['ID']};";
				    $res = $db->query($sql);
			    } else {
				    // otherwise, we're trying to add the original part, so we leave it alone
				    $res = null;
			    }
		    } else {
			    // if we find no matching part numbers in the broadcast, we just insert an addition
			    $sql = "INSERT INTO
						    module_part_changes (
							    mod_ID,
							    part_number,
							    added,
							    deleted,
							    qty
						    ) VALUES (
							    {$module_ID},
							    '{$part_number}',
							    1,
							    0,
							    1
						    );";
			    $res = $db->query($sql);

			    $sql = "INSERT INTO
						    components_in (
							    veh_id,
							    part_number,
							    qty
						    ) VALUES (
							    {$module_ID},
							    '{$part_number}',
							    1
						    );";
			    $res = $db->query($sql);
		    }

	    } else {
		    $res = $delete_res;
	    }

	    echo json_encode($res);
    }

    // if ($action === 'add_component') {
    // 	global $db;

    // 	// check for a matching part in the broadcast
    // 	$sql = "SELECT
    // 				ID,
    // 				part_number
    // 			FROM
    // 				components_in
    // 			WHERE
    // 				veh_id = {$_REQUEST['module_ID']}
    // 				AND dbo.getRootPartNumber(part_number) = dbo.getRootPartNumber('{$_REQUEST['part_number']}');";
    // 	$components_in = $db->query($sql);

    // 	if (!$components_in) {
    // 		// if we find no matching part numbers in the broadcast, we insert an addition
    // 		$sql = "INSERT INTO
    // 					module_part_changes (
    // 						module_ID,
    // 						part_number,
    // 						added
    // 					) VALUES (
    // 						{$_REQUEST['module_ID']},
    // 						'{$_REQUEST['part_number']}',
    // 						1
    // 					);";
    // 		$res = $db->query($sql);
    // 	} else {
    // 		// otherwise fail
    // 		$res = 0;
    // 	}

    // 	echo json_encode($res);
    // }


    // if ($action === 'reset_component_changes') {
    // 	global $db;

    // 	// wipe out the changes for the module
    // 	$sql = "DELETE
    // 			FROM
    // 				module_part_changes
    // 			WHERE
    // 				mod_ID = {$_REQUEST['module_ID']};";
    // 	$res = $db->query($sql);

    // 	echo json_encode($res);
    // }

?>