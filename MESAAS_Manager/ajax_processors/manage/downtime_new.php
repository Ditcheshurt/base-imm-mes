<?php
    $GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../config/app_config.json"), true);
	require($GLOBALS['APP_CONFIG']['db_class']);

	include_once("init_db.php");

	$action = $_REQUEST['action'];

	IF($action == 'get_machine_list'){
		$sql = "SELECT m.ID, m.machine_number, m.machine_name, mt.tool_ID
				FROM [dbo].[machines] m 
				LEFT JOIN machine_tools mt
					ON m.ID = mt.machine_ID";
		
	}

	IF($action == 'get_tool_list'){
		$sql = "SELECT t.ID, t.tool_description, mt.machine_ID
				FROM [dbo].[tools] t
				LEFT JOIN machine_tools mt
					ON t.ID = mt.tool_ID"
		
	}
	
	IF($action == 'get_shift_list'){
		$sql = "SELECT sti.ID, shift_desc, shift_ID, ti1.interval_start_time, ti2.interval_end_time, sti.shift_end, sti.shift_start, system_ID
					FROM [MES_COMMON].[dbo].shifts s
					INNER JOIN [MES_COMMON].[dbo].shift_time_intervals sti
					ON s.ID = sti.shift_ID
					LEFT JOIN (SELECT t1.interval_start_time, t1.ID
								FROM [MES_COMMON].[dbo].time_intervals t1) as ti1
					ON sti.time_interval_ID = ti1.ID AND sti.shift_start = 1
					LEFT JOIN (SELECT t2.interval_end_time, t2.ID
								FROM [MES_COMMON].[dbo].time_intervals t2) as ti2
					ON sti.time_interval_ID = ti2.ID AND sti.shift_end = 1
					WHERE (interval_start_time is NOT NULL OR interval_end_time is NOT NULL)
					AND system_ID = 1"
		
	}
	
	IF($action == 'get_downtime_reason_list'){
		
		$sql = "SELECT ID
					  ,parent_ID
					  ,reason_description
					  ,count_against
					  ,tool_change_ignore
					  ,active
					  ,force_comment
					  ,machine_type_ID
					  ,oee_category
					  ,maximum_duration
					  ,is_micro_stop
				FROM [dbo].[machine_downtime_reasons]";
		
	}
	
	IF($action == 'get_unknown_downtime'){
		$sql = "SELECT *
				FROM [dbo].machine_downtime_log
				WHERE time_in_state is not NULL
				AND (reason_ID is NULL OR reason_ID = 0)";
	}
	
	IF($action == 'get_searched_downtime'){
		// machine_downtime_log
		$where = " AND";
		if(!is_null($_REQUEST['machine_ID'])){
			$temp = explode('~',$_REQUEST['machine_ID']);
			foreach($temp => $k){
				$where .= " machine_ID = ".$k. " AND";
			}
		}
		if(!is_null($_REQUEST['tool_ID'])){
			$temp = explode('~',$_REQUEST['tool_ID']);
			foreach($temp => $k){
				$where .= " tool_ID = ".$k. " AND";
			}
		}
		if(!is_null($_REQUEST['start_date'])){
			$where .= " change_date >= '".$_REQUEST['start_date']. "' AND";
		}
		if(!is_null($_REQUEST['end_date'])){
			$where .= " machine_ID <= '".$_REQUEST['end_date']. "' AND";
		}
		if(!is_null($_REQUEST['shift_ID'])){
			$temp = explode('~',$_REQUEST['shift_ID']);
			foreach($temp => $k){
				$where .= " shift_ID = ".$k. " AND";
			}
		}
		if(!is_null($_REQUEST['reason_ID'])){
			$temp = explode('~',$_REQUEST['shift_ID']);
			foreach($temp => $k){
				$where .= " reason_ID = ".$k. " AND";
			}
		}
		$where = substr($where,0,-4);
		$sql = "SELECT *
				FROM [dbo].machine_downtime_log
				WHERE time_in_state is not NULL
				$where"
		echo $sql;
	}

	IF($action == 'set_downtime_reason'){
		if(is_null($_REQUEST['comment'])){
			$comment = "";
		}else{
			$comment = $_REQUEST['comment'];
		}
		$sql = "UPDATE [dbo].machine_downtime_log
					SET reason_ID = ".$_REQUEST['reason_ID'].", comment = ".$_REQUEST['comment']."
					WHERE ID = ".$_REQUEST['downtime_ID'];
	}
?>