<?php

    require_once("../init.php");

    // if (isset($_REQUEST['process_type'])) {
    //     if ($_REQUEST['process_type'] == 'SEQUENCED') {
    //         $db->catalog = 'NYSUS_SEQUENCE_MES';
    //     } else if ($_REQUEST['process_type'] == 'BATCH') {
    //         $db->catalog = 'NYSUS';
    //     }
    // }


    $action = $_REQUEST['action'];

    if ($action == "reprint") {
	    global $db;
	    $res = new StdClass();

	    $id = $_REQUEST['id'];
	    $reason = $_REQUEST['reason'];
	    $printer_id = $_REQUEST['printer_id'];

	    $sql = "UPDATE
				    print_queue
			    SET printed_time = null,
				    reprint_reason_text = '".$reason."',
				    printer_ID = ".$printer_id."
				    WHERE id = ".$id;
	    //die($sql);
	    $res = $db->query($sql);

	    echo json_encode($res);
    }

    if ($action == "get_print_queue") {
	    global $db;
	    $res = new StdClass();

        if ($_REQUEST['process_type'] == 'SEQUENCED') {

            if ($_REQUEST['printer_id'] == 1) {
                // Special case for the chute number, limited because it's horribly slow
                $sql = "SELECT TOP 300
                            q.ID,
                            q.mod_ID,
                            l.line_code,
                            RIGHT(m.build_order, 5) as build_order,
                            m.VIN,
                            m.sequence,
                            RIGHT(wpsih.completion_details, 7) AS chute_number,
                            q.printer_ID,
                            p.printer_name,
                            q.queue_time,
                            q.printed_time,
                            q.qty,
                            q.print_text
                        FROM print_queue AS q
                            LEFT JOIN printer_setup p ON p.ID = q.printer_ID
                            LEFT JOIN lines l ON l.per_part_printer = q.printer_ID AND l.ID < 1000
                            LEFT JOIN wip_parts wp ON q.print_text LIKE '%-' + wp.internal_serial + '\"%'
                            --LEFT JOIN wip_parts wp ON q.print_text CONTAINS('-' + wp.internal_serial + '\"')
                            LEFT JOIN wip_part_station_instruction_history wpsih ON
                                wpsih.wip_part_ID = wp.ID
                                AND wpsih.station_ID = 160010
                                AND wpsih.step_order = 20
                                AND wpsih.part_order = 10
                            LEFT JOIN modules m ON q.mod_ID = m.ID
                        WHERE
                            q.printed_time >= '".$_REQUEST['start_date']." 00:00:00'
                            AND q.printed_time <= '".$_REQUEST['end_date']." 23:59:59'
                            AND q.printer_ID = ".$_REQUEST['printer_id']."
                        ORDER BY
                            q.printed_time DESC;";
            } else {
                $sql = "SELECT
                            q.ID,
                            q.mod_ID,
                            l.line_code,
                            RIGHT(m.build_order, 5) as build_order,
                            m.VIN,
                            m.sequence,
                            NULL AS chute_number,
                            q.printer_ID,
                            p.printer_name,
                            q.queue_time,
                            q.printed_time,
                            q.qty,
                            q.print_text
                        FROM print_queue AS q
                            LEFT JOIN printer_setup p ON p.ID = q.printer_ID
                            LEFT JOIN lines l ON l.per_part_printer = q.printer_ID AND l.ID < 1000
                            LEFT JOIN modules m ON q.mod_ID = m.ID
                        WHERE
                            q.printed_time >= '".$_REQUEST['start_date']." 00:00:00'
                            AND q.printed_time <= '".$_REQUEST['end_date']." 23:59:59'
                            AND q.printer_ID = ".$_REQUEST['printer_id']."
                        ORDER BY
                            q.printed_time DESC;";
            }
        } else if ($_REQUEST['process_type'] == 'BATCH' || $_REQUEST['process_type'] == 'REPETITIVE') {
            $sql = "SELECT
                        q.ID,
                        -- NULL AS mod_ID,
                        -- NULL AS line_code,
                        -- NULL AS build_order,
                        -- NULL AS VIN,
                        -- NULL AS sequence,
                        -- NULL AS chute_number,
                        q.printer_ID,
                        p.printer_name,
                        q.queue_time,
                        q.printed_time,
                        q.qty,
                        q.print_text
                    FROM print_queue AS q
                        LEFT JOIN printer_setup p ON p.ID = q.printer_ID
                    WHERE
                        q.printed_time >= '".$_REQUEST['start_date']." 00:00:00'
                        AND q.printed_time <= '".$_REQUEST['end_date']." 23:59:59'
                        AND q.printer_ID = ".$_REQUEST['printer_id']."
                    ORDER BY
                        q.printed_time DESC;";
        }

	    $res = $db->query($sql);
        if (!$res) {
            $res = array();
        }
	    echo json_encode($res);
    }

    if ($action == 'get_printers') {
	    global $db;
	    $res = new StdClass();

	    $sql = "SELECT
				    ID,
				    printer_name,
				    printer_IP
			    FROM
				    printer_setup;";

	    $res = $db->query($sql);

	    echo json_encode($res);
    }

?>