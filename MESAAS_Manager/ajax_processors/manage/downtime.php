<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	

	call_user_func($action, $_REQUEST, $db);
	
	function get_machines($request, $db) {
		$query = "
			SELECT 
				ID AS id
				, machine_number + ' - ' + machine_name AS text
			FROM
				machines
			ORDER BY
				ID";	
		echo json_encode($db->query($query));
	}
	
	function get_tools($request, $db) {
		$query = "
			SELECT 
				ID AS id
				,tool_description AS text
			FROM
				tools
			ORDER BY 
				tool_description";
				
		echo json_encode($db->query($query));
	}
	
	
	function get_reasons_description_as_id($request, $db) {
		$query = "
			SELECT 'Not Dispositioned' AS id
				,'Not Dispositioned' AS text
			UNION
			SELECT DISTINCT
				CAST(r.[ID] AS varchar) AS id
				,r.[reason_description] AS text	
				--,(SELECT reason_description FROM [machine_downtime_reasons] WHERE ID = r.parent_ID) AS parent
			FROM 
				[machine_downtime_reasons] r
			WHERE 
				active = 1
			ORDER BY 
				2";
		echo json_encode($db->query($query));	
	}
	
	function get_reasons_by_machine_type_ID($request, $db) {
		$machine_type_ID = $request['machine_type_ID'];
		
		$query = "
			SELECT 'Not Dispositioned' AS id
				,'Not Dispositioned' AS text
			UNION
			SELECT DISTINCT
				ID AS id
				,r.[reason_description] AS text	
				--,(SELECT reason_description FROM [machine_downtime_reasons] WHERE ID = r.parent_ID) AS parent
			FROM 
				[machine_downtime_reasons] r
			WHERE 
				active = 1
				AND machine_type_ID = " . $machine_type_ID . "
				AND parent_ID IS NOT NULL 
				AND parent_ID > 0
			ORDER BY 
				2";
		echo json_encode($db->query($query));	
	}

	function get_selectable_reasons_by_machine_type_ID($request, $db) {
		$machine_type_ID = $request['machine_type_ID'];
		
		$query = "
			SELECT DISTINCT
				ID AS id
				,r.[reason_description] AS text	
				--,(SELECT reason_description FROM [machine_downtime_reasons] WHERE ID = r.parent_ID) AS parent
			FROM 
				[machine_downtime_reasons] r
			WHERE 
				active = 1
				AND machine_type_ID = " . $machine_type_ID . "
			ORDER BY 
				reason_description";
		echo json_encode($db->query($query));	
	}

	function run_report($request, $db) {
		$quantify = "";
		if (isset($request['quantify'])) {
			if ($request['quantify'] == "hours") {
				$quantify = "(SUM(time_in_state) / 60)";
			} else if ($request['quantify'] == "percent") {
				$quantify = "(SUM(time_in_state) / 1440 ) * 100";
			} else {
				if (isset($request['group_by'])) {
					$quantify = "SUM(time_in_state)";
				} else {
					$quantify = "time_in_state";
				}
			}
		}
		
		//define group bys
		require("../../../common/library/php/groupBy.php");
		$gby = new groupBy();
		$gby->set_group("date", "CONVERT(VARCHAR(10), dbo.getProdDate(mdl.change_time), 101)");
		$gby->set_group("machine", "m.machine_name");
		$gby->set_group("sub_machine", "sm.sub_machine_name");
		$gby->set_group("tool", "t.tool_description");
		$gby->set_group("reason_description", "ISNULL(d.reason_description, 'Not Dispositioned')");
		$gby->set_group("week", "DATEPART(wk, mdl.change_time)");
		$gby->set_group("operator", "o.badge_ID, o.name");
		$gby->set_group("shift", "dbo.getShift(m.system_ID, mdl.change_time)");
		$gby->set_group("hour", "DATEPART(hh, mdl.change_time)");
		$gby->set_group("machine_type_ID", "m.machine_type_ID");
		$gby->set_group("tool_ID", "t.ID");
		$gby->set_group("machine_ID", "m.ID");
		//$gby->set_group("hour", "CAST(mdl.change_time AS date)", 'date');
		
		$query = "SELECT ";

		if (isset($request["sel_top10"]) && $request["sel_top10"] == "true") {
			$query .= " TOP 10 ";
		}

		//get group by based on request
		if (isset($request['group_by'])) {
			$group_by = $gby->get_group_by($request["group_by"]);
			$query .= implode(",", $group_by->selects).",";
		} else {
			$group_by = $gby->get_group_by(array());
			$query .= " CONVERT(VARCHAR(13), mdl.change_time, 101) + ' ' + CONVERT(VARCHAR(13), mdl.change_time, 108) AS 'datetime', 
						m.machine_name AS 'Machine', t.tool_description AS 'Tool', o.name AS 'Operator', dbo.getShift(m.system_ID, mdl.change_time) AS 'Shift',
						ISNULL(d.reason_description, 'Not Dispositioned') as 'reason_description',
						m.machine_type_ID AS machine_type_ID,
						t.ID AS tool_ID,
						m.ID AS machine_ID,
						mdl.ID, 
						mdl.reason_ID, ";
		}

		//$downtime = "(dbo.getDowntime(mdl.ID, '{$request["from_date"]} 00:00:00', '{$request["to_date"]} 23:59:59', 0) $quantify";

		if (isset($request["sel_uptime"])) {
			$query .= "CAST((DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') - ROUND(SUM({$downtime}), 3)) as float)
						/ CAST(DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') as float) * 100.0 as 'Uptime (%)'";
		} else {
			$query .= "ROUND($quantify, 3) As 'downtime'";
		}

		$query .= " FROM
						machine_downtime_log mdl
						LEFT JOIN machine_downtime_reasons d ON mdl.reason_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t on mdl.tool_ID = t.ID
						LEFT JOIN MES_COMMON.dbo.operators o on o.badge_id = mdl.recorded_by 
					WHERE
						mdl.up = 0
						AND mdl.time_in_state IS NOT NULL
						--AND mdl.reason_ID IS NOT NULL
						AND dbo.getProdDate(mdl.change_time) >= '{$request["from_date"]}'
						AND dbo.getProdDate(mdl.change_time) <= '{$request["to_date"]}'";

		if (isset($request["sel_operators"]) && $request["sel_operators"] != null)
			$query .= " AND o.badge_ID IN ('" . implode("','", $request["sel_operators"]) . "') ";
		
		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";
		
		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";
		
		if (isset($request["sel_reasons"]) && $request["sel_reasons"] != null) {
			$query .= " AND (d.reason_description IN ('" . implode("','", $request["sel_reasons"]) . "') ";
			foreach ($request["sel_reasons"] as $value) {
				if ($value == 'Not Dispositioned') {					
			    	$query .= " OR d.reason_description IS NULL ";
				}
			}	
			$query .= ") ";
		}

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";

		/*if (isset($request["sel_reasons"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_reasons"]) . "%'";
*/		
		if (isset($request['group_by'])) {
			$query .= " GROUP BY " . implode(",", $group_by->groups);
		}
		$query .= " ORDER BY downtime desc, " . implode(",", $group_by->groups);

//die($query);
		echo json_encode($db->query($query));
		
	}
	
	function drilldown($request, $db) {
		$quantify = "";
		if (isset($request['quantify'])) {
			if ($request['quantify'] == "hours") {
				$quantify = "ROUND((mdl.time_in_state / 60), 3)";
			} else if ($request['quantify'] == "percent") {
				$quantify = "ROUND((mdl.time_in_state / 1440) * 100, 3)";
			} else {
				$quantify = "ROUND(mdl.time_in_state, 3)";
			}
		}
		
		$query = "SELECT
					mdl.ID,
					CONVERT(VARCHAR, mdl.change_time,101)+' '+CONVERT(VARCHAR, mdl.change_time,108) AS 'datetime',
					m.machine_name AS machine,
					--sm.sub_machine_name AS 'Sub Machine',
					t.tool_description AS tool,
					ISNULL(d.reason_description, 'Not Dispositioned') AS reason,
					d.ID AS reason_ID,
					mdl.comment AS comment,
					$quantify AS downtime,
					o.name AS 'Entered By',
					o.badge_ID AS 'badge_ID',
					m.machine_type_ID AS machine_type_ID,
					t.ID AS tool_ID,
					m.ID AS machine_ID
				FROM
					machine_downtime_log mdl
					LEFT JOIN machine_downtime_reasons d ON mdl.reason_ID = d.ID
					JOIN machines m ON m.ID = mdl.machine_ID
					--LEFT OUTER JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
					JOIN tools t ON mdl.tool_ID = t.ID
					LEFT JOIN [MES_COMMON].dbo.operators o ON o.badge_ID = mdl.recorded_by
				WHERE
					mdl.up = 0
					--AND mdl.reason_ID IS NOT NULL
					AND dbo.getProdDate(mdl.change_time) >= '{$request["from_date"]}'
					AND dbo.getProdDate(mdl.change_time) <= '{$request["to_date"]}'";
		
		if (isset($request["sel_operators"]) && $request["sel_operators"] != null)
			$query .= " AND o.badge_ID IN ('" . implode("','", $request["sel_operators"]) . "') ";
		
		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";
		
		if (isset($request["machine"]) && $request["machine"] != null)
			$query .= " AND m.machine_name IN ('" . implode("','", $request["machine"]) . "') ";
		
		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";
		
		if (isset($request["tool"]) && $request["tool"] != null)
			$query .= " AND t.tool_description IN ('" . implode("','", $request["tool"]) . "') ";

		if (isset($request["sel_reasons"]) && $request["sel_reasons"] != null) {
			$query .= " AND (d.reason_description IN ('" . implode("','", $request["sel_reasons"]) . "') ";
			foreach ($request["sel_reasons"] as $value) {
				if ($value == 'Not Dispositioned') {					
			    	$query .= " OR d.reason_description IS NULL ";
				}
			}	
			$query .= ") ";		
		}

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";
		
	//die($query);
		echo json_encode($db->query($query));

	}
	
	function sel_shifts($orm) {

		$obj_1 = new stdClass();
		$obj_1->text = "Shift 1";
		$obj_1->value = "1";

		$obj_2 = new stdClass();
		$obj_2->text = "Shift 2";
		$obj_2->value = "2";

		$obj_3 = new stdClass();
		$obj_3->text = "Shift 3";
		$obj_3->value = "3";

		return array($obj_1, $obj_2, $obj_3);

	}

	function update_downtime_log($request, $db) {
		$ID = $_POST['machine_downtime_log_ID'];
		$table = 'machine_downtime_log';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'machine_downtime_log_ID' && $field != 'downtime') {					
					$r[$field] = $value;
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);

		$res = $db->query($sql);
		
		if(isset($request["downtime"])) {
			$dt = $request["downtime"];
			$sql = "EXEC [dbo].[sp_splitDowntime]
					@dt_ID = $ID,
					@split_val = $dt";
			$db->query($sql);
		}
		
		echo json_encode($res);

	}
	
	function get_base_downtime($request = NULL, $db){
		$sql = "SELECT mdl.[ID]
					  ,[change_time],CAST(change_time AS time(3))
					  ,mdl.[up]
					  ,[machine_ID]
					  ,[tool_ID]
					  ,[time_in_state]
					  ,[startup_time]
					  ,[reason_ID]
					  ,[comment]
					  ,[tool_change_ID]
					  ,[is_tool_change_pause]
					  ,[recorded_by]
					  ,[sub_machine_ID]
					  ,m.[machine_name]
					  ,r.[reason_description]
				  FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_downtime_log] mdl
				  INNER JOIN [NYSUS_REPETITIVE_MES].[dbo].[machines] m
				  ON mdl.machine_ID = m.ID
				  INNER JOIN [NYSUS_REPETITIVE_MES].[dbo].[machine_downtime_reasons] r
				  ON mdl.reason_ID = r.ID
				  WHERE time_in_state IS NOT NULL
					AND (reason_ID = 0 OR reason_ID IS NULL)";
		echo json_encode($db->query($sql));
	}
	
	function get_special_downtime($request = NULL, $db){
		$where = ' AND ';
		if(!is_null($_REQUEST['machines']) && $_REQUEST['machines'] != ''){
			$temp = explode('~',$_REQUEST['machines']);
			$where .= "(";
			foreach($temp as $v){
				$where .= "machine_ID = $v OR ";
			}
			$where = substr($where,0,-4);
			$where .= ") AND ";
		}
		if(!is_null($_REQUEST['reasons']) && $_REQUEST['reasons'] != ''){
			$temp = explode('~',$_REQUEST['reasons']);
			$where .= "(";
			foreach($temp as $v){
				if ($v == 'Not Dispositioned') {
					$where .= "reason_ID IS NULL OR ";
				} else {
					$where .= "reason_ID = $v OR ";
				}
			}
			$where = substr($where,0,-4);
			$where .= ") AND ";
		}
		if(!is_null($_REQUEST['tools']) && $_REQUEST['tools'] != ''){
			$temp = explode('~',$_REQUEST['tools']);
			$where .= "(";
			foreach($temp as $v){
				$where .= "tool_ID = $v OR ";
			}
			$where = substr($where,0,-4);
			$where .= ") AND ";
		}
		if($_REQUEST['start'] != ''){
			$where .= "CAST(change_time AS date) >= '".$_REQUEST['start']."' AND ";
		}
		if($_REQUEST['end'] != ''){
			$where .= "CAST(change_time AS date) <= '".$_REQUEST['end']."' AND ";
		}
		if(!is_null($_REQUEST['shifts']) && $_REQUEST['shifts'] != ''){
			$temp_sql = "SELECT min(ti1.interval_start_time) as 'start', max(ti2.interval_end_time) as 'end'
						FROM [MES_COMMON].[dbo].shift_time_intervals sti
						LEFT JOIN (SELECT t1.interval_start_time, t1.ID
									FROM [MES_COMMON].[dbo].time_intervals t1) as ti1
							ON sti.time_interval_ID = ti1.ID AND sti.shift_start = 1
						LEFT JOIN (SELECT t2.interval_end_time, t2.ID
									FROM [MES_COMMON].[dbo].time_intervals t2) as ti2
							ON sti.time_interval_ID = ti2.ID AND sti.shift_end = 1
						WHERE (interval_start_time is NOT NULL OR interval_end_time is NOT NULL)
							AND (sti.shift_ID = ".$_REQUEST['shifts'].")
						GROUP BY sti.shift_ID";
			$temp = $db->query($temp_sql);
			$where .= "(";
			foreach($temp as $k => $v){
				$start_time = date_format($v['start'], 'H:i:s');
				$end_time = date_format($v['end'], 'H:i:s');
				$where .= "(CAST(change_time AS time(3)) >= CAST('$start_time' AS time(3)) AND ";
				$where .= "CAST(change_time AS time(3)) <= CAST('$end_time' AS time(3))) OR ";
			}
			$where = substr($where,0,-3);
			$where .= ") AND ";
		}
		
		$where = substr($where,0,-4);
		$sql = "SELECT mdl.[ID]
					  ,[change_time],CAST(change_time AS time(3))
					  ,mdl.[up]
					  ,[machine_ID]
					  ,[tool_ID]
					  ,[time_in_state]
					  ,[startup_time]
					  ,[reason_ID]
					  ,[comment]
					  ,[tool_change_ID]
					  ,[is_tool_change_pause]
					  ,[recorded_by]
					  ,[sub_machine_ID]
					  ,m.[machine_name]
					  ,r.[reason_description]
				  FROM [NYSUS_REPETITIVE_MES].[dbo].[machine_downtime_log] mdl
				  INNER JOIN [NYSUS_REPETITIVE_MES].[dbo].[machines] m
				  ON mdl.machine_ID = m.ID
				  LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[machine_downtime_reasons] r
				  ON mdl.reason_ID = r.ID
				  WHERE time_in_state IS NOT NULL
				  $where";
		echo json_encode($db->query($sql));
	}
	
	
	function get_dt_info($request = NULL, $db) {
		$res = new StdClass();
		$machine_type_ID = 0;

		if (isset($_REQUEST['machine_type_ID'])) {
			$machine_type_ID = $_REQUEST['machine_type_ID'];
		}

		$sql = "SELECT dl.ID, dl.change_time, dl.time_in_state, m.machine_name, t.tool_description
					 FROM machine_downtime_log dl
					 	JOIN machines m ON dl.machine_ID = m.ID
					 	LEFT OUTER JOIN tools t ON dl.tool_ID = t.ID
					 WHERE dl.ID = ".$_REQUEST['id']."
					 ";
		$res->dt_info = $db->query($sql);

		//code_info
		$sql = "SELECT *
					 FROM machine_downtime_reasons
					 WHERE active = 1 AND is_micro_stop IS NULL
					 	AND machine_type_ID = 1
					 ORDER BY parent_ID, reason_description" ;
		$res->code_info = $db->query($sql);

		echo json_encode($res);
	}
	
	function save_downtime_reason($request = NULL, $db) {
		$res = new StdClass();
		$sql = "UPDATE machine_downtime_log SET reason_ID = ".$_REQUEST['dtrid'].", comment = '".str_replace("'", "''", $_REQUEST['comment'])."' WHERE id = ".$_REQUEST['dtid'];
		$db->query($sql);
		$res->success = 1;

		echo json_encode($res);
	}
	
	function split_old_time($request = NULL, $db){
		$res = new StdClass();
		if($_REQUEST['starting_point'] == 1){
			$sql_insert = "
				INSERT INTO [dbo].[machine_downtime_log] (
					[change_time]
					,[up]
					,[machine_ID]
					,[tool_ID]
					,[time_in_state]
					,[startup_time]
					,[reason_ID]
					,[comment]
					,[tool_change_ID]
					,[is_tool_change_pause]
					,[recorded_by]
					,[sub_machine_ID]
				)
				SELECT
					DATEADD(MINUTE, ".$_REQUEST['length']." ,[change_time])
					,[up]
					,[machine_ID]
					,[tool_ID]
					, ([time_in_state] - ".$_REQUEST['length']." )
					,[startup_time]
					,[reason_ID]
					,[comment]
					,[tool_change_ID]
					,[is_tool_change_pause]
					,[recorded_by]
					,[sub_machine_ID]
				FROM [dbo].[machine_downtime_log]
				WHERE ID = ".$_REQUEST['dt_id'];
			$db->query($sql_insert);
			
			$sql_update = "
				UPDATE [dbo].[machine_downtime_log] 
				SET [reason_ID] = NULL, [time_in_state] = ".$_REQUEST['length'].", 
					[recorded_by] = ".$_SESSION['op_ID'].", [comment] = 'after the fact split'
				WHERE ID = ".$_REQUEST['dt_id'];
			$db->query($sql_update);	
			$res -> success = $_REQUEST['dt_id'];
		}else{
			$sql_insert = "
				INSERT INTO [dbo].[machine_downtime_log] (
					[change_time]
					,[up]
					,[machine_ID]
					,[tool_ID]
					,[time_in_state]
					,[startup_time]
					,[reason_ID]
					,[comment]
					,[tool_change_ID]
					,[is_tool_change_pause]
					,[recorded_by]
					,[sub_machine_ID]
				)
				SELECT
					DATEADD(MINUTE, ([time_in_state] - ".$_REQUEST['length'].") ,[change_time])
					,[up]
					,[machine_ID]
					,[tool_ID]
					, ".$_REQUEST['length']."
					,[startup_time]
					,NULL
					,'after the fact split'
					,[tool_change_ID]
					,[is_tool_change_pause]
					,".$_SESSION['op_ID']."
					,[sub_machine_ID]
				FROM [dbo].[machine_downtime_log]
				WHERE ID = ".$_REQUEST['dt_id'];
			$db->query($sql_insert);
			
			$sql_update = "
				UPDATE [dbo].[machine_downtime_log] 
				SET [time_in_state] = [time_in_state] -  ".$_REQUEST['length']." 
				WHERE ID = ".$_REQUEST['dt_id'];
			$db->query($sql_update);
			
			$sql = "SELECT TOP 1 ID FROM [dbo].[machine_downtime_log] ORDER BY ID DESC";
			$res -> success = $db->query($sql);
		}
		echo json_encode($res);
	}
?>