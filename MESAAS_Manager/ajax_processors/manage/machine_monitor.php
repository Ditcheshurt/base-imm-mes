<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == 'get_machines') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT m.ID, CONCAT(m.machine_name, ' - ', m.machine_number) AS [machine_name], m.up, t.short_description AS [tool], m.last_cycle_time
				FROM machines m
				JOIN tools t ON t.ID = m.current_tool_ID
				ORDER BY m.ID;";
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_severities') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT * FROM machine_monitor_severity ORDER BY severity_order ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_param_data') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT TOP ".$_REQUEST['data_points']."
					m.ID AS [machine_id]
					,t.short_description AS [tool]
					,mc.ID AS [cycle_ID]
					,mc.cycle_time
					,mcdp.data_point_ID
					,dps.data_point_desc
					,mcdp.data_point_value
					,ISNULL(mtcl.upper_ctrl_limit, dps.default_ucl) AS ucl
					,ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl) AS lcl
				FROM
					machines m
					JOIN machine_cycles mc ON mc.machine_ID = m.ID
					JOIN machine_cycle_data_points mcdp ON mcdp.cycle_ID = mc.ID
					LEFT JOIN tools t ON t.ID = mc.tool_ID
					LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_id
					LEFT JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.deactivated = 0
				WHERE
					mc.machine_ID = ".$_REQUEST['machine_ID']."
					AND t.ID =  ".$_REQUEST['tool_ID']."
					AND dps.ID = ".$_REQUEST['data_point_ID']."
				ORDER BY
					mc.cycle_time DESC;";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_press_param_data') {
		global $db;
		$res = new StdClass();
		
		$machine_ID = $_REQUEST["machine_ID"];
		
		$sql = "DECLARE @cycle_ID int, @tool_ID int
				SELECT TOP 1 @cycle_ID = ID, @tool_ID = t1.tool_ID FROM machine_cycles t1 WHERE t1.machine_ID = $machine_ID ORDER BY t1.ID DESC

				SELECT dps.ID AS [dps_ID], dps.data_point_desc, mcdp.data_point_value, mtcl.ID AS [mtcl_ID], mc.machine_ID, mc.tool_ID, m.machine_name, t.short_description AS [tool_short], t.tool_description,
				CASE WHEN (mcdp.data_point_value > ISNULL(mtcl.upper_ctrl_limit,dps.default_ucl)) THEN ISNULL(mtcl.upper_ctrl_limit,1) ELSE CASE WHEN (mcdp.data_point_value < ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl)) THEN ISNULL(mtcl.lower_ctrl_limit,-1) ELSE 0 END END AS [limit],
				ISNULL(mtcl.upper_ctrl_limit, dps.default_ucl) AS [upper_ctrl_limit], ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl) AS [lower_ctrl_limit],
				cls.machine_monitor_severity_ID AS [severity], cls.active
				FROM machine_cycle_data_points mcdp
				JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
				LEFT JOIN machine_tool_ctrl_limits mtcl ON mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.deactivated = 0
				LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
				LEFT JOIN machine_monitor_control_limit_severity cls ON cls.machine_tool_ctrl_limit_ID = mtcl.ID
				LEFT JOIN machines m ON m.ID = mc.machine_ID
				LEFT JOIN tools t ON t.ID = mc.tool_ID
				WHERE mcdp.cycle_ID = @cycle_ID
				ORDER BY dps.ID;";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'get_active_alerts') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT mc.ID AS [mc_ID],cls.ID AS [cls_ID],mtcl.ID AS [mtcl_ID], dps.ID AS [dps_ID], cls.machine_monitor_severity_ID AS [severity], cls.active AS [active], mtcl.machine_ID, mtcl.tool_ID, mma.limit_triggered,mc.cycle_time,m.machine_name,t.tool_description,t.short_description AS [tool_short],dps.data_point_desc,ISNULL(mtcl.upper_ctrl_limit,dps.default_ucl) AS [upper_ctrl_limit],ISNULL(mtcl.lower_ctrl_limit,dps.default_lcl) AS [lower_ctrl_limit],mcdp.data_point_value,mms.severity_description
				FROM dbo.machine_monitor_alerts mma
				JOIN machine_cycle_data_points mcdp ON mcdp.ID = mma.machine_cycle_datapoint_ID
				JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
				JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.deactivated = 0
				JOIN machine_monitor_control_limit_severity cls ON cls.machine_tool_ctrl_limit_ID = mtcl.ID
				LEFT JOIN machines m ON m.ID = mc.machine_ID
				LEFT JOIN tools t ON t.ID = mc.tool_ID
				LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
				LEFT JOIN machine_monitor_severity mms ON mms.ID = cls.machine_monitor_severity_ID
				WHERE mma.acknowledged_timestamp IS NULL
				ORDER BY mma.created_timestamp ASC;";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'save_alert') {
		global $db;
		$res = new StdClass();
		
		$sql = "EXECUTE sp_MACHINE_MES_processMachineMonitorAlertConfig
				   @machine_ID = ".$_REQUEST['machine_ID']."
				  ,@tool_ID = ".$_REQUEST['tool_ID']."
				  ,@dps_ID = ".$_REQUEST['dps_ID']."
				  ,@mtcl_ID = ".(STRLEN($_REQUEST['mtcl_ID'])>0?$_REQUEST['mtcl_ID']:'NULL')."
				  ,@upper_cl = ".$_REQUEST['upper_cl']."
				  ,@lower_cl = ".$_REQUEST['lower_cl']."
				  ,@severity = ".$_REQUEST['severity']."
				  ,@active = ".$_REQUEST['alrt_active']."
				  ,@operator_ID = ".$_REQUEST['operator_ID']."
				  ,@target_val = NULL";
			  
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'ack_alert') {
		global $db;
		$res = new StdClass();
		
		$sql = "UPDATE machine_monitor_alerts
				SET acknowledged_timestamp = GETDATE(), acknowledged_by = ".$_REQUEST['operator_ID']."
				OUTPUT inserted.ID
				WHERE machine_cycle_datapoint_ID IN (
					SELECT ID
					FROM machine_cycle_data_points
					WHERE cycle_ID = ".$_REQUEST['mc_ID']."
					AND data_point_ID = ".$_REQUEST['dps_ID']."
				)";
		
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'ack_all_alerts') {
		global $db;
		$res = new StdClass();
		
		$sql = "UPDATE machine_monitor_alerts
				SET acknowledged_timestamp = GETDATE(), acknowledged_by = ".$_REQUEST['operator_ID']."
				OUTPUT inserted.ID
				WHERE acknowledged_timestamp IS NULL";
		
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
				
?>