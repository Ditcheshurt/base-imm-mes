<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;

	if ($action == "get_batch_status") {
		get_batch_status();
	}

	if ($action == "get_bom_IDs") {
		$sql = "SELECT u.bom_ID, u.bom_string, DATEDIFF(DAY, u.last_module_time, GETDATE()) as days_ago,
					SUM(CASE WHEN m.batch_status = 'BATCH_QUEUED' THEN 1 ELSE 0 END) as num_in_queue,
					SUM(CASE WHEN m.batch_status = 'BATCH_BUILDING' THEN 1 ELSE 0 END) as num_building,
					SUM(CASE WHEN m.batch_status = 'BATCH_BUILT' THEN 1 ELSE 0 END) as num_built
				  FROM module_unique_BOMs u
				  	  LEFT OUTER JOIN modules m ON u.bom_ID = m.bom_ID AND u.line_ID = m.line_ID AND m.batch_status IN ('BATCH_QUEUED','BATCH_BUILDING','BATCH_BUILT')
				  WHERE u.line_ID = ".$_REQUEST['line_ID']."
				  GROUP BY u.bom_ID, u.bom_string, u.last_module_time
				  ORDER BY u.last_module_time DESC";
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "add_batch_modules") {
		$res = new StdClass();

		$sql = "EXEC dbo.sp_MESAAS_SEQUENCE_CUSTOM_TRW_scheduleBatchBuilds
					@line_ID = ".$r['line_ID'].",
					@bom_ID  = ".$r['bom_ID'].",
					@qty     = ".$r['qty'].";";
		$db->query($sql);

		$res->success = 1;
		echo json_encode($res);
	}

	if ($action == "move_module") {
		//move it
		$sql = "EXEC dbo.sp_MESAAS_SEQUENCE_EDITOR_moveBatchModule
					@line_ID = ".$r['line_ID'].",
					@module_ID = ".$r['module_ID'].",
					@direction = '".$r['direction']."';";
		$db->query($sql);
		get_batch_status();
	}

	if ($action == "delete_module") {
		//delete it
		$sql = "DELETE FROM modules WHERE ID = ".$r['module_ID'];
		$db->query($sql);

		get_batch_status();
	}

	function get_batch_status() {
		global $db;
		$res = new StdClass();

		$line_ID = $_REQUEST['line_ID'];

		$res->queued = $db->query("SELECT *
											FROM modules
											WHERE line_ID = ".$line_ID."
												AND batch_status = 'BATCH_QUEUED'
											ORDER BY sequence");
		$res->in_progress = $db->query("SELECT v.*, ls2.station_desc
													FROM (
															SELECT m.bom_ID, m.sequence, m.build_order, MAX(ls.station_order) as max_station_order
															FROM modules m
																JOIN module_station_history msh ON m.ID = msh.module_ID AND msh.exit_time IS NOT NULL
																JOIN line_stations ls ON msh.station_ID = ls.station_ID
															WHERE m.line_ID = ".$line_ID."
																AND m.batch_status = 'BATCH_BUILDING'
															GROUP BY m.bom_ID, m.sequence, m.build_order) as v
														JOIN line_stations ls2 ON v.max_station_order = ls2.station_order AND ls2.line_id = ".$line_ID."
													ORDER BY v.build_order");
		$res->built = $db->query("SELECT *
											FROM modules
											WHERE line_ID = ".$line_ID."
												AND batch_status = 'BATCH_BUILT'
											ORDER BY build_order");
		echo json_encode($res);
	}

?>