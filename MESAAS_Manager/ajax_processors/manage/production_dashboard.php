<?php
try
{

    require_once("../init.php");
	
	$action = $_GET["action"];
	
	global $db;
	$res = new StdClass();
	
	//Getting records (listAction)
	if($action == "list")
	{	
		$pageSize = $_GET["jtPageSize"];
		$sorting = $_GET["jtSorting"];
		$startIndex = $_GET["jtStartIndex"];
		$pageSize = $_GET["jtPageSize"];
		$endRow = $startIndex + $pageSize;
		
		$sql = "SELECT COUNT(*) AS RecordCount FROM displays_hourly_part_requirements";
		$recordCount = $db->query($sql);
		
		$sql =  "SELECT * FROM (SELECT TOP $pageSize
						t.*, c.line_desc as cell_desc, ROW_NUMBER() OVER (ORDER BY $sorting) AS rn
					FROM displays_hourly_part_requirements t
					JOIN [MES_COMMON].dbo.v_MES_LINES c ON 
						c.line_ID = t.cell_ID
					ORDER BY
						$sorting
				) t
				WHERE rn > $startIndex AND rn < $endRow";

		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount[0]['RecordCount'];
		$jTableResult['Records'] = $res;
		echo json_encode($jTableResult);
	}
	//Creating a new record (createAction)
	else if($action == "create")
	{		
		$cell_ID = $_POST["cell_ID"];
		$hour = $_POST["hour"];
		$req_qty = $_POST["required_qty"];
		
		//Insert record into database		
		$sql =  "INSERT INTO displays_hourly_part_requirements(cell_ID, hour, required_qty) VALUES($cell_ID, $hour, $req_qty)";
		
		$db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$sql = "SELECT * FROM displays_hourly_part_requirements WHERE ID = @@IDENTITY";
		
		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $res;
		echo json_encode($jTableResult);
	}	
	//Updating a record (updateAction)
	else if($action == "update")
	{
		$ID = $_POST["ID"];
		$cell_ID = $_POST["cell_ID"];
		$hour = $_POST["hour"];
		$req_qty = $_POST["required_qty"];
		
		//Update record in database
		$sql = "UPDATE displays_hourly_part_requirements SET cell_ID = $cell_ID, hour = $hour, required_qty = $req_qty WHERE ID = $ID";

		$db->query($sql);
		
		$sql = "SELECT r.ID, r.cell_ID, c.cell_desc , r.hour, r.required_qty
					FROM displays_hourly_part_requirements r 
					JOIN cells c on c.ID = r.cell_ID
					WHERE r.ID = $ID";
		//die($sql);
		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $res[0];
		echo json_encode($jTableResult);
	}
	//Deleting a record (deleteAction)
	else if($action == "delete")
	{
		//Delete from database
		$sql = "DELETE FROM displays_hourly_part_requirements WHERE ID = " . $_POST["ID"];

		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		echo json_encode($jTableResult);
	}
	else if ($action == "get_lines")
	{
		$sql =  "SELECT 
					line_ID AS Value
					, line_desc AS DisplayText
				FROM 
					 [MES_COMMON].dbo.v_MES_LINES				
				ORDER BY line_ID asc";
		
		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Options'] = $res;
		echo json_encode($jTableResult);
	}

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>