<?php
	
    require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);
	
	function get_machines($request, $db) {
		$query = "
			SELECT 
				ID AS id
				,machine_name AS text
			FROM
				machines
			ORDER BY
				machine_name";
				
		echo json_encode($db->query($query));
	}
	
	function get_tools($request, $db) {
		$query = "
			SELECT 
				ID AS id
				,tool_description AS text
			FROM
				tools
			ORDER BY 
				tool_description";
				
		echo json_encode($db->query($query));
	}
	
	function get_reasons_description_as_id($request, $db) {
		$query = "
			SELECT DISTINCT
				r.[reason_description] AS id
				,r.[reason_description] AS text	
				--,(SELECT reason_description FROM [machine_downtime_reasons] WHERE ID = r.parent_ID) AS parent
			FROM 
				[machine_downtime_reasons] r
			WHERE 
				active = 1
			ORDER BY 
				reason_description";
		echo json_encode($db->query($query));	
	}
	
	function get_reasons_by_machine_type_ID($request, $db) {
		$machine_type_ID = $request['machine_type_ID'];
		
		$query = "
			SELECT DISTINCT
				ID AS id
				,r.[reason_description] AS text	
				--,(SELECT reason_description FROM [machine_downtime_reasons] WHERE ID = r.parent_ID) AS parent
			FROM 
				[machine_downtime_reasons] r
			WHERE 
				active = 1
				AND machine_type_ID = " . $machine_type_ID . "
			ORDER BY 
				reason_description";
		echo json_encode($db->query($query));	
	}

	function run_report($request, $db) {
		$quantify = "";
		if (isset($request['quantify'])) {
			if ($request['quantify'] == "hours") {
				$quantify = " / 60)";
			} else if ($request['quantify'] == "percent") {
				$quantify = " / 1440 ) * 100";
			} else {
				$quantify = ")";
			}
		}
		
		//define group bys
		require("../../../common/library/php/groupBy.php");
		$gby = new groupBy();
		$gby->set_group("date", "CONVERT(VARCHAR(10), mdl.change_time, 101)");
		$gby->set_group("machine", "m.machine_name");
		$gby->set_group("sub_machine", "sm.sub_machine_name");
		$gby->set_group("tool", "t.tool_description");
		$gby->set_group("reason_code", "d.reason_description");
		$gby->set_group("week", "DATEPART(wk, mdl.change_time)");
		$gby->set_group("shift", "dbo.getShift(mdl.change_time)");
		$gby->set_group("hour", "DATEPART(hh, mdl.change_time)");
		//$gby->set_group("hour", "CAST(mdl.change_time AS date)", 'date');
		
		//get group by based on request
		if (isset($request['group_by']))
			$group_by = $gby->get_group_by($request["group_by"]);
		else
			$group_by = $gby->get_group_by(array());

		$downtime = "(dbo.getDowntime(mdl.ID, '{$request["from_date"]} 00:00:00', '{$request["to_date"]} 23:59:59', 0) $quantify";

		$query = "SELECT ".implode(",", $group_by->selects).",";

		if (isset($request["sel_uptime"])) {
			$query .= "CAST((DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') - ROUND(SUM({$downtime}), 3)) as float)
						/ CAST(DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') as float) * 100.0 as 'Uptime (%)'";
		} else {
			$query .= "ROUND(SUM({$downtime}), 3) As 'downtime'";
		}

		$query .= " FROM
						machine_downtime_log mdl
						LEFT JOIN machine_downtime_reasons d ON mdl.reason_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t on mdl.tool_ID = t.ID
					WHERE
						mdl.up = 0
						AND mdl.time_in_state IS NOT NULL
						--AND mdl.reason_ID IS NOT NULL
						AND mdl.change_time >= '{$request["from_date"]} 00:00:00'
						AND mdl.change_time <= '{$request["to_date"]} 23:59:59'";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";
		
		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";
		
		if (isset($request["sel_reasons"]) && $request["sel_reasons"] != null)
			$query .= " AND d.reason_description IN ('" . implode("','", $request["sel_reasons"]) . "') ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";

		/*if (isset($request["sel_reasons"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_reasons"]) . "%'";
*/
		$query .= " GROUP BY " . implode(",", $group_by->groups);
		$query .= " ORDER BY " . implode(",", $group_by->groups);
//die($query);
		echo json_encode($db->query($query));
		
	}
	
	function drilldown($request, $db) {

		$query = "SELECT
						mdl.ID,
						CONVERT(VARCHAR, mdl.change_time,101)+' '+CONVERT(VARCHAR, mdl.change_time,108) AS 'datetime',
						m.machine_name AS machine,
						--sm.sub_machine_name AS 'Sub Machine',
						t.tool_description AS tool,
						d.reason_description AS reason,
						d.ID AS reason_ID,
						mdl.comment AS comment,						
						mdl.time_in_state AS downtime,
						o.name AS 'Entered By',
						m.machine_type_ID AS machine_type_ID,
						t.ID AS tool_ID,
						m.ID AS machine_ID
					FROM
						machine_downtime_log mdl
						LEFT JOIN machine_downtime_reasons d ON mdl.reason_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--LEFT OUTER JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t ON mdl.tool_ID = t.ID
						LEFT JOIN [MES_COMMON].dbo.operators o ON o.ID = mdl.recorded_by
					WHERE
						mdl.up = 0
						--AND mdl.reason_ID IS NOT NULL
						AND mdl.change_time >= '{$request["from_date"]} 00:00:00'
						AND mdl.change_time <= '{$request["to_date"]} 23:59:59' ";


	//die($query);
		echo json_encode($db->query($query));

	}

	function update_downtime_log($request, $db) {
		$ID = $_POST['machine_downtime_log_ID'];
		$table = 'machine_downtime_log';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'machine_downtime_log_ID' && $field != 'downtime') {					
					$r[$field] = $value;
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);

		$res = $db->query($sql);
		
		if(isset($request["downtime"])) {
			$dt = $request["downtime"];
			$sql = "EXEC [dbo].[sp_splitDowntime]
					@dt_ID = $ID,
					@split_val = $dt";
			$db->query($sql);
		}
		
		echo json_encode($res);
	}	
	
	function sel_shifts($orm) {

		$obj_1 = new stdClass();
		$obj_1->text = "Shift 1";
		$obj_1->value = "1";

		$obj_2 = new stdClass();
		$obj_2->text = "Shift 2";
		$obj_2->value = "2";

		$obj_3 = new stdClass();
		$obj_3->text = "Shift 3";
		$obj_3->value = "3";

		return array($obj_1, $obj_2, $obj_3);

	}

?>