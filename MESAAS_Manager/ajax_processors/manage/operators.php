<?php
	
    require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_operators") {
		loadOperators();
	}

	if ($action == "save_operator") {
		$r = $_REQUEST;

		if ($r['active'] == "1") {
			$deactive_date = "NULL";
		} else {
			$deactive_date = "GETDATE()";
		}

		if ($r['edit_type'] == "ADD") {
			$sql = "SET NOCOUNT ON;
					  INSERT INTO MES_COMMON.dbo.operators (ID, name, badge_ID, first_name, last_name, deactive_date, email, password, shift) VALUES (
					 '".$r['badge_ID']."',
					 '".$r['first_name']." ".$r['last_name']."',
					 '".$r['badge_ID']."',
					 '".$r['first_name']."',
					 '".$r['last_name']."',
					 ".$deactive_date.",
					 '".$r['email']."',
					 '".$r['password']."',
					 '".$r['shift']."'
					 );";
			if (isset($r['areas'])) {
				if ($r['areas'] != "") {
					$a = explode(",", $r['areas']);
					for ($i = 0; $i < count($a); $i++) {
						$sql .= "INSERT INTO MES_COMMON.dbo.operator_areas (operator_ID, area_ID, add_time) VALUES ('".$r['badge_ID']."',".$a[$i].",GETDATE());";
					}
				}
			}
		} else {
			$sql = "UPDATE MES_COMMON.dbo.operators SET
						  ID = '".$r['badge_ID']."',
					     name = '".$r['first_name']." ".$r['last_name']."',
					     badge_ID = '".$r['badge_ID']."',
					     deactive_date = ".$deactive_date.",
					     first_name = '".$r['first_name']."',
					     last_name = '".$r['last_name']."',
					     email = '".$r['email']."',
					     password = '".$r['password']."',
					     shift = '".$r['shift']."'
					  WHERE id = '".$r['op_ID']."';";
			$sql .= "DELETE FROM MES_COMMON.dbo.operator_areas WHERE operator_ID = ".$r['op_ID'].";";
			if (isset($r['areas'])) {
				if ($r['areas'] != "") {
					$a = explode(",", $r['areas']);
					for ($i = 0; $i < count($a); $i++) {
						$sql .= "INSERT INTO MES_COMMON.dbo.operator_areas (operator_ID, area_ID, add_time) VALUES (".$r['op_ID'].",".$a[$i].",GETDATE());";
					}
				}
			}
		}
		//echo $sql;
		//die($sql);
		$db->query($sql);

		loadOperators();
	}

	if ($action == "save_operator_role") {
		$r = $_REQUEST;
		$sql = "DELETE FROM MES_COMMON.dbo.operator_roles WHERE operator_ID = ".$r['op_ID']." AND role = '".$r['role']."'";
		$db->query($sql);

		if ($r['on'] == "1") {
			$sql = "INSERT INTO MES_COMMON.dbo.operator_roles (operator_ID, role, editor) VALUES (".$r['op_ID'].",'".$r['role']."', 1)";
			$db->query($sql);
		}

		loadOperators();
	}

	if ($action == "print_operator_badge") {
		$r = $_REQUEST;

		//get template
		$sql = "SELECT label_template
				  FROM MES_COMMON.dbo.common_label_templates
				  WHERE ID = 1";
		$res = $db->query($sql);
		if (count($res) > 0) {
			$template = $res[0]['label_template'];
		}

		//get op name
		$sql = "SELECT * FROM MES_COMMON.dbo.operators WHERE ID = ".$_REQUEST['op_ID'];
		$res = $db->query($sql);
		if ($res) {
			$name = $res[0]['name'];
		}

		$template = str_replace("<<badge_ID>>", $_REQUEST['op_ID'], $template);
		$template = str_replace("<<name>>", $name, $template);

		//get printer ID
		$sql = "SELECT ID FROM printer_setup WHERE ID = '".$GLOBALS['APP_CONFIG']['operator_badge_printer_ID']."'";
		$res = $db->query($sql);
		if ($res) {
			$printer_ID = $res[0]['ID'];
		}

		$sql = "INSERT INTO print_queue (mod_ID, printer_ID, print_text, queue_time) VALUES (".
				 $_REQUEST['op_ID'].", ".
				 $printer_ID.", ".
				 "'".$template."', ".
				 "GETDATE()".
				 ")";
		$db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	function loadOperators() {
		global $db;
		$res = new StdClass();

		$sql = "SELECT o.*
				  FROM MES_COMMON.dbo.operators o
				  ORDER BY o.name";
		$res->operators = $db->query($sql);

		$sql = "SELECT *
				  FROM MES_COMMON.dbo.operator_roles ";
		$res->operator_roles = $db->query($sql);

		$res->roles = $GLOBALS['APP_CONFIG']['roles'];

		$sql = "SELECT *
				  FROM MES_COMMON.dbo.operator_areas ";
		$res->operator_areas = $db->query($sql);

		$sql = "SELECT *
				  FROM MES_COMMON.dbo.areas ";
		$res->areas = $db->query($sql);

		echo json_encode($res);
	}
?>