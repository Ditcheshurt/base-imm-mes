<?php

/*
 * Editor server script for DB table Operators
 * Created by http://editor.datatables.net/generator
 */

// Override the catalog setting in app_config.json
$database = "MES_COMMON";

// DataTables PHP library and database connection
include( "../../lib/DataTables/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;
 
 // Chad - development notes
 // Notes:
 // Setting the validator to required causes bubble editing to fail because not all fields are part of the submit.

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'operators', 'ID' ) // Having the PK set here ensures that a non-identity PK is captured for associative inserts.
        ->fields(
            //Field::inst( 'ID' ) // ID is not automatically set by the database on create
                //->set( Field::SET_BOTH )
                //->setFormatter( function($val, $data, $field) {return $data['badge_ID'];}),   
            
            Field::inst( 'first_name' )
                ->validator( 'Validate::notEmpty', array( 'message' => "First name is required.")  ),	
                
            Field::inst( 'last_name' )
                ->validator( 'Validate::notEmpty' ), 
            
            Field::inst( 'badge_ID' )
                ->validator( 'Validate::unique' )
                ->validator( 'Validate::notEmpty' ),
            
            Field::inst( 'email' )
                ->validator( 'Validate::email' ),
            
            Field::inst( 'deactive_date' )
                ->validator( 'Validate::dateFormat', array(
                    "format"  => Format::DATE_ISO_8601,
                    "message" => "Please enter a date in the format yyyy-mm-dd"
                ) )
                ->getFormatter( 'Format::date_sql_to_format', Format::DATE_ISO_8601 )
                ->setFormatter( 'Format::date_format_to_sql', Format::DATE_ISO_8601 ),
            
            Field::inst( 'domain_user' ),
               
            
            Field::inst( 'password' )
               
        )        
        ->join(
            Mjoin::inst( 'roles' )
                ->link( 'operators.ID', 'operator_roles.operator_ID' )
                ->link( 'roles.ID', 'operator_roles.role_ID' )
                ->fields(
                    Field::inst( 'ID' )
                        ->validator( 'Validate::notEmpty' )
                        ->options( 'roles', 'ID', 'name'),
					Field::inst( 'name' ) // supports display in DataTable not Editor.
                )
        )
        ->join(
            Mjoin::inst( 'areas' )
                ->link( 'operators.ID', 'operator_areas.operator_ID' )                    
                ->link( 'areas.ID', 'operator_areas.area_ID' )                
                ->fields(
                    Field::inst( 'ID' )
                        ->validator( 'Validate::notEmpty' )
                        ->options( 'areas', 'ID', 'area_desc', 
                                   function($q){ $q->where('areas.active', true ); // Ensure only active areas are in the editor select.
                        } ),
                    Field::inst( 'area_desc' ) // supports display in DataTable not Editor.                                            
                )
                ->where('areas.active', true) // ensures only active areas are displayed in the table.
        )        
        ->process( $_POST )
        ->json();
