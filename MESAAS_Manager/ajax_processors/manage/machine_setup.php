<?php
	
    require_once("../init.php");

	$action = $_REQUEST['action'];
	
	if ($action == "get_machines") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$where = "";
		if ($r['machine_name'] != null) {
			$where = "WHERE machine_name LIKE '%".$r['machine_name']."%' OR machine_number LIKE '%".$r['machine_name']."%'";
		}
		
		$sql = "SELECT 
							ID
							, machine_type_ID
							, machine_name
							, machine_number
							, up
							, station_ip_address
					FROM 
						machines m
					$where
					ORDER BY m.machine_name ASC";

		$res = $db->query($sql);

		echo json_encode($res);
	} 
	
	else if ($action == "get_machine_tool_parents") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT 
							t.ID
							, tool_description
							, short_description
							, target_cycle_time
							, parent_tool_ID
							, current_part_ID
							, t.active
					FROM 
						tools t
					JOIN machine_tools mt 
						ON mt.tool_ID = t.ID
					WHERE t.active = 1
						AND parent_tool_ID IS NULL
						AND mt.machine_ID = ".$r['machine_ID']."
					ORDER BY t.tool_description ASC";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if ($action == "get_machine_tool_children") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT 
							ID
							, tool_description
							, short_description
							, target_cycle_time
							, parent_tool_ID
							, current_part_ID
							, active
					FROM 
						tools t
					WHERE t.active = 1
						AND parent_tool_ID = ".$r['tool_ID']."
					ORDER BY t.tool_description ASC";
		
		$res = $db->query($sql);

		echo json_encode($res);
	}	
	
	else if ($action == "get_tool_parts") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT 
							*
					FROM 
						tool_parts tp
					WHERE tp.active = 1
						AND tp.tool_ID = ".$r['tool_ID']."
					ORDER BY tp.operation ASC";
		
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if ($action == "get_machine_tools") {
		
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT 
							t.*
					FROM 
						tools t
					JOIN machine_tools mt
						ON mt.tool_ID = t.ID
					JOIN machines m 
						ON mt.machine_ID = m.ID
					--WHERE t.active = 1
					ORDER BY t.tool_description ASC";
		
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if($action == "insert_tool_part") {
		
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				$r[$field] = "'".$value."'";				
			}
		}

		$sql = "INSERT INTO tool_parts
				   (".implode(', ', array_keys($r)).")
				VALUES
				   (".implode(', ', array_values($r)).")";
		
		$db->query($sql);
		//die($sql);
	}	
	
	else if($action == "update_tool") {
		$ID = $_POST['tool_ID'];
		$table = 'tools';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'tool_ID') {					
					$r[$field] = $value;				
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);
		//die($sql);
		$db->query($sql);
		
		$sql = "SELECT 
							*
					FROM 
						tools tp
					WHERE 
						tp.ID = $ID";
		
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if($action == "insert_tools") {
		
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				$r[$field] = "'".$value."'";				
			}
		}

		$sql = "INSERT INTO tools
				   (".implode(', ', array_keys($r)).")
				VALUES
				   (".implode(', ', array_values($r)).")";
		
		$db->query($sql);
		
	}	
	
	else if($action == "update_tool_part") {
		$ID = $_POST['part_ID'];
		$table = 'tool_parts';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'part_ID') {					
					$r[$field] = $value;				
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);
		//die($sql);
		
		$db->query($sql);
		//die($sql);
		$ID = $_POST['part_ID'];
		$table = 'tool_parts';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'part_ID') {					
					$r[$field] = $value;				
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);

		$db->query($sql);		

	}
	
	else if ($action == "get_machine_production_targets") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT 
							*
					FROM 
						machine_production_targets t
					WHERE 
						t.machine_ID = ".$r['machine_ID']."
					ORDER BY t.the_hour ASC";
		
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if ($action == "update_machine_production_targets") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		$machine_ID = $r['machine_ID'];
		
		foreach ($r as $hour => $target) {
			
			if ($hour != "action" && $hour != "machine_ID") {
				
				$sql = "SELECT 
									* 
								FROM 
									machine_production_targets
								WHERE 
									machine_ID = $machine_ID
									AND the_hour = $hour";
									
				$res = $db->query($sql);
				
				if(count($res) > 0) {
					$sql = "UPDATE machine_production_targets
			   						SET
			      					target_percent = $target
									WHERE 
										machine_ID = $machine_ID
										AND the_hour = $hour";										
					
					$db->query($sql);
				} else {
					$sql = "INSERT INTO 
										machine_production_targets
			           		(machine_ID
			           		,the_hour
			           		,target_percent)
	     						VALUES
				           ($machine_ID
				           ,$hour
				           ,$target)";
				  $db->query($sql);
				}
			}
		}

		//echo json_encode($res);
	}
	
?>