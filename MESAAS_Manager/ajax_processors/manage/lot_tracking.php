<?php

	require("../db_helper.php");

	$sql = [];
	$err_msg = [];

	$req = new dbhelper($sql, $err_msg);
	
	switch ($action) {
		case 'get_machines':
			$sql[0] = "
				SELECT
					 ID
					,machine_name
					,machine_number
				FROM machines
			";
			$err_msg = array(
				0 => 'Unable to load machines'
			);
			
			$req->processSqlRequest();
			break;
			
		case 'get_recent_tests':
			$date_filter = '1=1';
			if (isset($start_date) && isset($end_date)) {
				$date_filter .= " AND [timestamp] BETWEEN '{$start_date}' AND '{$end_date}'";
			}
			
			$sql[0] = "
				SELECT
					mct.ID
					,mct.[timestamp]
					,mct.test_result AS [pass_fail]
					,ISNULL(mct.lot_number,'') AS lot_number
					,CONVERT(varchar(128),mct.unique_test_ID,2) AS unique_test_ID
				FROM CUSTOM_material_certification_test mct
				WHERE mct.ID IN (
					SELECT TOP 100 ID FROM CUSTOM_material_certification_test WHERE {$date_filter} ORDER BY [timestamp] DESC
				)
				ORDER BY mct.[timestamp] DESC
			";
			
			$sql[1] = "
				SELECT
					mcr.ID
					,mcr.matcert_test_ID AS mct_ID
					,mcs.data_point_desc
					,CONCAT(
						CAST(mcr.matcert_datapoint_value AS varchar),
						' ',
						mcs.units,
						(CASE WHEN LEN(mcr.matcert_datapoint_range) > 0 THEN CONCAT('+/-',CAST(mcr.matcert_datapoint_range AS varchar),mcs.units) ELSE '' END)
					) AS [value],
					display_order
				FROM CUSTOM_material_certification_results mcr
				LEFT JOIN CUSTOM_material_certification_setup mcs ON mcs.ID = mcr.matcert_datapoint_ID
				WHERE mcr.matcert_test_ID IN (
					SELECT TOP 100 ID FROM CUSTOM_material_certification_test WHERE {$date_filter} ORDER BY [timestamp] DESC
				)
				ORDER BY mcr.matcert_test_ID ASC, mcr.matcert_datapoint_ID ASC
			";

			$sql[2] = "
				SELECT
					 mlt.ID
					,mlt.material_cert_test_ID
					,mlt.machine_ID
					,m.machine_number
					,mlt.start_timestamp
				FROM CUSTOM_machine_lot_tracking mlt
				LEFT JOIN machines m ON m.ID = mlt.machine_ID
				WHERE mlt.material_cert_test_ID IN (
					SELECT TOP 100 ID FROM CUSTOM_material_certification_test WHERE {$date_filter} ORDER BY [timestamp] DESC  
				)
				AND mlt.end_timestamp IS NULL 
				AND mlt.active = 1
				ORDER BY mlt.machine_ID ASC
			";
			
			$err_msg = array(
				0=>'Failed to get material cert tests!',
				1=>'Failed to get material cert test results!',
				2=>'Failed to get machine active lots!'
			);
			
			$req->processSqlRequest();
			break;
			
		case 'get_test_results':
			$sql[0] = "
				SELECT
					 mct.ID
					,mcs.data_point_desc
					,CONCAT(CAST(mcr.matcert_datapoint_value AS varchar), ' ', mcs.units) AS [value]
					,(CASE WHEN LEN(mcr.matcert_datapoint_range) > 0 THEN CONCAT('+/-',CAST(mcr.matcert_datapoint_range AS varchar),mcs.units) ELSE '' END) AS [range]
				FROM CUSTOM_material_certification_test mct
				JOIN CUSTOM_material_certification_results mcr ON mcr.matcert_test_ID = mct.ID
				LEFT JOIN CUSTOM_material_certification_setup mcs ON mcs.ID = mcr.matcert_datapoint_ID
				WHERE mct.ID = {$test_ID}
			";
			
			$err_msg = array(
				0=>'No test results returned'
			);

			$req->processSqlRequest();
			break;

		case 'assign_lot_number':
			$sql[0] = "
				UPDATE CUSTOM_material_certification_test
				SET lot_number = '{$lot_number}'
				OUTPUT inserted.ID, inserted.lot_number, inserted.[timestamp]
				WHERE ID = {$test_ID}
			";
			$err_msg = array(
				0=>"No test record was updated! Lot number: {$lot_number}"
			);

			$req->processSqlRequest();
			break;
			
		case 'set_active_lot':
			$mal = explode('~',$lot_machines);
			$sql[0] = "
				EXEC [dbo].[sp_MACHINE_MES_CUSTOM_updateMachineLotNumber]
					@operator_ID = {$operator_ID},
					@test_ID = {$test_ID},
					@machine_str = '{$lot_machines}'
			";
			$err_msg = array(
				0=>"Unable to save active lot for machines"
			);
		
			$req->processSqlRequest();
			break;
		
		default:
			$req->missingActionHandler();
			break;
	}
	$req->rtnResults();
?>