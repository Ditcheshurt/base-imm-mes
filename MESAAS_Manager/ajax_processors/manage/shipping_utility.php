<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;

	if ($action == "get_shipment_status") {
		get_shipment_status();
	}

	if ($action == "add_shipment") {
		$sql = "INSERT INTO shipments (ship_to_code, operator, status) VALUES (
				 	 '".$_REQUEST['ship_to_code']."',
				 	 ".$_SESSION['op_ID'].",
				 	 0);";
		$db->query($sql);

		get_shipment_status();
	}

	if ($action == "add_rack_to_shipment") {
		$res = new StdClass();

		$res->success = 1;
		$res->err_msg = "";

		//look it up
		$rack_prefix = "S";
		$scan = $_REQUEST['rack_scan'];
		$ship_to_code = $_REQUEST['ship_to_code'];

		if (substr($scan, 0, 1) != $rack_prefix) {
			$res->success = 0;
			$res->err_msg = "Invalid rack prefix: ".substr($scan, 0, 1)." Expecting: ".$rack_prefix;
		}

		if ($res->success == 1) {
			$sql = "SELECT *
					  FROM shipment_lanes
					  WHERE CAST(ID as varchar(20)) = '".substr($scan, 1)."'";

			$rack_res = $db->query($sql);

			if (!$rack_res) {
				$res->success = 0;
				$res->err_msg = "Rack scan: ".substr($scan, 1)." not found";
			} else {
				if (count($rack_res) < 1) {
					$res->success = 0;
					$res->err_msg = "Rack scan: ".substr($scan, 1)." not found";
				} else {
					$rack = $rack_res[0];

					//look up how many racks are allowed per truck for that line
					$sql = "SELECT ID, lanes_per_truck, ship_to_code
							  FROM lines
							  WHERE ID = ".$rack['module_line_ID'];
					$line_res = $db->query($sql);
					$line_ID = $line_res[0]['ID'];
					$lanes_per_truck = $line_res[0]['lanes_per_truck'];
					$ship_to_code_scanned = $line_res[0]['ship_to_code'];

					//get the current shipment
					$sql = "SELECT TOP 1 *
							  FROM shipments
							  WHERE ship_to_code = '".$ship_to_code."'
							  	  AND status < 2
							  ORDER BY ID DESC";
					$shipment_res = $db->query($sql);
					$shipment_ID = $shipment_res[0]['ID'];

					//look up how many are loaded for the current shipment
					$sql = "SELECT COUNT(*) as racks_loaded
							  FROM shipment_lanes
							  WHERE shipment_ID = ".$shipment_ID."
							  	  AND module_line_ID = ".$line_ID;
					$racks_loaded_res = $db->query($sql);

					if ($racks_loaded_res[0]['racks_loaded'] >= $lanes_per_truck) {
						$res->success = 0;
						$res->err_msg = "Too many racks loaded: ".$racks_loaded_res[0]['racks_loaded'];
					} else {
						if ($ship_to_code_scanned != $ship_to_code) {
							$res->success = 0;
							$res->err_msg = "Invalid rack type (".$ship_to_code_scanned.") for this shipment (".$ship_to_code.")";
						} else {
							//verify the rack is the next in sequence
							$sql = "SELECT MAX(rack_sequence) as last_rack_sequence
									  FROM shipment_lanes
									  WHERE module_line_ID = ".$line_ID."
									     AND shipment_ID IS NOT NULL";
							$last_res = $db->query($sql);
							if ($last_res) {
								if (count($last_res) > 0) {
									$last_rack_sequence = $last_res[0]['last_rack_sequence'];
								} else {
									$last_rack_sequence = 0;
								}
							} else {
								$last_rack_sequence = 0;
							}

							if ($last_rack_sequence != 0) {
								if ($last_rack_sequence == $rack['rack_sequence'] - 1) {
									//good
									$sql = "UPDATE shipment_lanes
											  SET shipment_ID = ".$shipment_ID."
											  WHERE ID = ".$rack['id'];
									$db->query($sql);
								} else {
									$res->success = 0;
									$res->err_msg = "Incorrect rack sequence.  Next rack sequence should be: ".($last_rack_sequence + 1);
								}
							} else {
								$sql = "UPDATE shipment_lanes
										  SET shipment_ID = ".$shipment_ID."
										  WHERE ID = ".$rack['id'];
								$db->query($sql);
							}
						}
					}
				}
			}
		}

		echo json_encode($res);
	}

	if ($action == "remove_rack_from_shipment") {
		$sql = "UPDATE shipment_lanes
				  SET shipment_ID = NULL
				  WHERE ID = ".$_REQUEST['rack_ID'];
		$db->query($sql);

		get_shipment_status();
	}

	if ($action == "end_shipment") {
		$ship_to_code = $_REQUEST['ship_to_code'];
		$shipment_ID = 0;

		//get shipment ID
		$sql = "SELECT TOP 1 ID
				  FROM shipments
				  WHERE ship_to_code = '".$ship_to_code."'
				  	  AND status < 2
				  ORDER BY ID DESC";
		$res = $db->query($sql);
		if ($res) {
			if (count($res) > 0) {
				$shipment_ID = $res[0]['ID'];
			}
		}

		if ($shipment_ID != 0) {
			$sql = "EXEC dbo.sp_MESAAS_sequenceShipping_markShipmentComplete
						 @shipment_ID = ".$shipment_ID.",
						 @operator_ID = ".$_SESSION['op_ID'];
			$db->query(sql);
		}

		get_shipment_status();
	}

	function get_shipment_status() {
		global $db;
		$res = new StdClass();

		//shipment groups
		$sql = "SELECT DISTINCT(ship_to_code) as ship_to_code FROM lines WHERE ship_to_code IS NOT NULL ORDER BY ship_to_code;";
		//echo $sql;
		$res->shipment_groups = $db->query($sql);
		if (!$res->shipment_groups) {
			$res->shipment_groups = array();
		}

		//lines
		$sql = "SELECT ID, line_code, ship_to_code, lanes_per_truck FROM lines WHERE ship_to_code IS NOT NULL ORDER BY line_code;";
		//echo $sql;
		$res->lines = $db->query($sql);
		if (!$res->lines) {
			$res->lines = array();
		}

		//shipments
		$sql = "SELECT * FROM shipments WHERE status < 2;";
		//echo $sql;
		$res->shipments = $db->query($sql);
		if (!$res->shipments) {
			$res->shipments = array();
		}

		//shipment lanes
		$sql = "SELECT sl.id, sl.rack_sequence, sl.module_line_ID, ISNULL(MIN(m.sequence), '') as min_sequence, ISNULL(MAX(m.sequence), '') as max_sequence
				  FROM shipment_lanes sl
				  	  LEFT OUTER JOIN shipment_items si ON sl.ID = si.lane_ID
				  	  LEFT OUTER JOIN modules m ON si.mod_ID = m.ID
				  WHERE sl.shipment_ID IN (SELECT ID FROM shipments WHERE status < 2)
				  GROUP BY sl.id, sl.rack_sequence, sl.module_line_ID
				  ORDER BY sl.rack_sequence;";
		//echo $sql;
		$res->shipment_lanes = $db->query($sql);
		if (!$res->shipment_lanes) {
			$res->shipment_lanes = array();
		}

		echo json_encode($res);
	}

?>