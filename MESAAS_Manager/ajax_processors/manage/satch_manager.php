<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;

	if ($action == "get_satch_status") {
		get_satch_status();
	}

	if ($action == "save_buffer_level") {
		$sql = "UPDATE wip_part_buffer_levels
				SET buffer_level = ".$_REQUEST['buffer_level']."
				WHERE bom_ID = ".$_REQUEST['bom_ID'];
		$db->query($sql);

		get_satch_status();
	}
	
	if ($action == "mark_wip_part_used") {
		$sql = "UPDATE wip_parts
				SET used_by_module_ID = 0
				WHERE ID = ".$_REQUEST['wip_part_ID'];
		$db->query($sql);

		get_satch_status();
	}

	function get_satch_status() {
		global $db;
		$res = new StdClass();

		$line_ID = $_REQUEST['line_ID'];
		
		$res->datetime = $db->query("SELECT GETDATE() as the_time");

		$res->wip_part_buffer_part_setup = $db->query("SELECT *
														FROM wip_part_buffer_part_setup
														WHERE line_ID = ".$line_ID."
														ORDER BY ID");
		$res->wip_part_buffer_levels = $db->query("SELECT *
														FROM wip_part_buffer_levels
														WHERE line_ID = ".$line_ID."
														ORDER BY ID");
		$res->in_progress_parts = $db->query("SELECT wp.*, ct.ID as carrier_ID, m.ID as module_ID, m.VIN, m.sequence, m.build_order
											FROM wip_parts wp
												LEFT OUTER JOIN carrier_tracking ct ON wp.ID = ct.wip_part_ID AND wp.line_ID = ct.line_ID
												LEFT OUTER JOIN modules m ON wp.used_by_module_ID = m.ID
											WHERE wp.line_ID = ".$line_ID."
												AND wp.built_time IS NULL
											ORDER BY wp.wip_build_order");
		$res->built_unused_parts = $db->query("SELECT wp.*, -1 as carrier_ID, m.ID as module_ID, m.VIN, m.sequence, m.build_order
											FROM wip_parts wp
												LEFT OUTER JOIN modules m ON wp.used_by_module_ID = m.ID
											WHERE wp.line_ID = ".$line_ID."
												AND wp.built_time IS NOT NULL
												AND wp.used_by_module_ID IS NULL
											ORDER BY wp.wip_build_order");
		echo json_encode($res);
	}

?>