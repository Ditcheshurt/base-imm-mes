<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);

	function get_departments_and_areas() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT *
					FROM departments
					ORDER BY department_name";
		$res->departments = $db->query($sql);
		
		$sql = "SELECT *
					FROM areas
					ORDER BY area_desc";
		$res->areas = $db->query($sql);
		
		echo json_encode($res);
	}

	function get_operators() {
		global $db;
		$sql = "SELECT o.* 
				FROM operators o 
					JOIN operator_areas oa ON o.ID = oa.operator_ID AND oa.area_ID = " . $_REQUEST['area_ID'] . " 
				ORDER BY o.name";
				
		echo json_encode($db->query($sql));
	}
	
	function get_ojt() {
		global $db;
		$res = new StdClass();
	
		// $sql = "SELECT * 
		// 			FROM ojt_jobs 
		// 			WHERE ((area_ID = " . $_REQUEST['area_ID'] . ") 
		// 						OR 
		// 					  (area_ID IS NULL AND department_ID = (SELECT department_ID FROM areas WHERE id = " . $_REQUEST['area_ID'] . ")) 
		// 						OR 
		// 					  (area_ID IS NULL AND department_ID IS NULL)) 
		// 				AND active = 1 
		// 			ORDER BY department_ID, area_ID, job_name";

		$sql = "SELECT
					oj.ID,
					oj.department_ID,
					oj.area_ID,
					oj.sub_area_ID,
					oj.job_name,
					oj.job_details,
					oj.active,
					sa.ID AS sub_area_ID,
					sa.sub_area_desc
				FROM ojt_jobs oj
					LEFT JOIN sub_areas sa ON sa.ID = oj.sub_area_ID
				WHERE (
					(oj.area_ID = ".$_REQUEST['area_ID'].") OR (
						oj.area_ID IS NULL AND oj.department_ID = (SELECT department_ID FROM areas WHERE id = ".$_REQUEST['area_ID'].")
					) OR (
						oj.area_ID IS NULL AND oj.department_ID IS NULL
					)
				) AND oj.active = 1
				ORDER BY oj.department_ID, oj.area_ID, sa.ID, oj.job_name";
								
		$res->jobs = $db->query($sql);

		$sql = "SELECT * FROM ojt_level_descriptions WHERE dept_ID = (SELECT dept_ID FROM areas WHERE ID = " . $_REQUEST['area_ID'] . ") ORDER BY ojt_level";
		$res->levels = $db->query($sql);
		
		$sql = "SELECT o.*, t.name as trainer_name, s.name as supervisor_name 
					FROM ojt_job_signoffs o 
						JOIN ojt_jobs j ON o.ojt_job_ID = j.ID 
							AND ((j.area_ID = " . $_REQUEST['area_ID'] . ") OR 
								  (j.area_ID IS NULL AND j.department_ID = (SELECT department_ID FROM areas WHERE id = " . $_REQUEST['area_ID'] . ")) OR 
								  (j.area_ID IS NULL AND j.department_ID IS NULL)) 
							AND j.active = 1 
						JOIN operators t ON o.trainer_ID = t.ID 
						JOIN operators s ON o.supervisor_ID = s.ID 
					WHERE o.operator_ID = " . $_REQUEST['operator_ID'] . " 
					ORDER BY j.job_name, o.ojt_level";
		$res->quals = $db->query($sql);

		$res->operator = $db->query("SELECT * FROM operators WHERE ID = " . $_REQUEST['operator_ID']);

		$res->area = $db->query("SELECT * FROM areas WHERE ID = " . $_REQUEST['area_ID']);
		
		echo json_encode($res);
	}
	
	function get_op_info() {
		global $db;
		$sql = "SELECT * FROM operators WHERE badge_ID = '" . $_REQUEST['scan'] . "' OR ID = '" . $_REQUEST['scan'] . "'";
		$res = $db->query($sql);
		
		if ($res) {
			$sql = "SELECT role FROM operator_roles WHERE operator_ID = ".$res[0]['ID'].";";
			$res[0]['roles'] = $db->query($sql);
			
			if ($res[0]['roles']) {
				foreach($res[0]['roles'] as $i => &$role) {
					$role = $role['role'];
				}
			} else {
				$res[0]['roles'] = array();
			}
		}
		
		echo json_encode($res);
	}

	function record_ojt() {
		global $db;
		$sql = "INSERT INTO ojt_job_signoffs (ojt_job_ID, ojt_level, the_date, operator_ID, trainer_ID, supervisor_ID) VALUES (
							" . $_REQUEST['jobid'] . ", 
							" . $_REQUEST['level'] . ", 
							GETDATE(), 
							" . $_REQUEST['opid'] . ", 
							" . $_REQUEST['trainerid'] . ", 
							" . $_REQUEST['supervisorid'] . ")";
							
		echo json_encode($db->query($sql));
	}


	function load_alert_signoffs() {
		global $db;
		$sql = "SELECT *, o.name as team_leader_name 
					FROM qa_signoffs s 
						JOIN qa_items i ON s.qa_item_ID = i.ID 
						JOIN operators o ON s.team_leader_ID = o.ID 
					WHERE i.qa_area_ID = " . $_REQUEST['area_ID'];
			echo json_encode($db->query($sql));
	}
	
	function record_alert_signoffs() {
		global $db;
		$res = new StdClass();
		$ops = explode("~", $_REQUEST['ops']);
		$sql = "";
		for ($i = 0; $i < count(ops); $i++) {
				$sql .= "IF NOT EXISTS (SELECT * FROM qa_signoffs WHERE qa_item_ID = " . $_REQUEST['qa_item_ID'] . " AND operator_ID = " . $ops[i] . ") 
											INSERT INTO qa_signoffs (qa_item_ID, team_leader_ID, operator_ID, signoff_time) VALUES (
												" . $_REQUEST['qa_item_ID'] . ", 
												" . $_SESSION['user_ID'] . ", 
												" . $ops[i] . ", 
												GETDATE()) 
										 ELSE 
											UPDATE qa_signoffs 
											SET signoff_time = GETDATE(), team_leader_ID = " . $_SESSION['op_ID'] . " 
											WHERE qa_item_ID = " . $_REQUEST['qa_item_ID'] . " AND operator_ID = " . $ops[i] . ";";
		}
		$db->query($sql);
		
		$res->success = 1;
		echo json_encode($res);
	}

?>