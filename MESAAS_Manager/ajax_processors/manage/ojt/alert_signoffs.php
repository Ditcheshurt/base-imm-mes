<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);

	function get_departments_and_areas() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT *
					FROM departments
					ORDER BY department_name";
		$res->departments = $db->query($sql);
		
		$sql = "SELECT *
					FROM areas
					ORDER BY area_desc";
		$res->areas = $db->query($sql);
		
		echo json_encode($res);
	}

	function get_operators() {
		global $db;
		$sql = "SELECT o.* 
				FROM operators o 
					JOIN operator_areas oa ON o.ID = oa.operator_ID AND oa.area_ID = " . $_REQUEST['area_ID'] . " 
				ORDER BY o.name";
				
		echo json_encode($db->query($sql));
	}
	
	function get_op_info() {
		global $db;
		$sql = "SELECT * FROM operators WHERE badge_ID = '" . $_REQUEST['scan'] . "' OR ID = '" . $_REQUEST['scan'] . "'";
		
		echo json_encode($db->query($sql));
	}

	function load_alert_signoffs__operatrs__alerts() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT o.* 
				FROM operators o 
					JOIN operator_areas oa ON o.ID = oa.operator_ID AND oa.area_ID = " . $_REQUEST['area_ID'] . " 
				ORDER BY o.name";
		$res->operators = $db->query($sql);
		
		$sql = $sql = "SELECT q.*, o.name as entered_name, o2.name as last_updated_name 
					FROM qa_items q 
						JOIN operators o ON q.entered_by = o.ID 
						LEFT OUTER JOIN operators o2 ON q.last_updated_by = o2.ID 
					WHERE q.qa_area_ID = " . $_REQUEST["area_ID"] . " 
					ORDER BY q.item_prefix, q.ID";
		$res->alerts = $db->query($sql);
		
		$sql = "SELECT *, o.name as team_leader_name 
					FROM qa_signoffs s 
						JOIN qa_items i ON s.qa_item_ID = i.ID 
						JOIN operators o ON s.team_leader_ID = o.ID 
					WHERE i.qa_area_ID = " . $_REQUEST['area_ID'];
					
		$res->signoffs = $db->query($sql);
		echo json_encode($res);
	}
	
	function load_alert() {
		global $db;
		$sql = "SELECT * FROM qa_items WHERE id = " . $_REQUEST['qa_item_ID'];
		echo json_encode($db->query($sql));
	}
	
	function record_alert_signoffs() {
		global $db;
		$res = new StdClass();
		$ops = explode("~", $_REQUEST['ops']);
		$sql = "";
		for ($i = 0; $i < count($ops); $i++) {
				$sql .= "IF NOT EXISTS (SELECT * FROM qa_signoffs WHERE qa_item_ID = " . $_REQUEST['qa_item_ID'] . " AND operator_ID = " . $ops[$i] . ") 
											INSERT INTO qa_signoffs (qa_item_ID, team_leader_ID, operator_ID, signoff_time) VALUES (
												" . $_REQUEST['qa_item_ID'] . ", 
												" . $_SESSION['op_ID'] . ", 
												" . $ops[$i] . ", 
												GETDATE()) 
										 ELSE 
											UPDATE qa_signoffs 
											SET signoff_time = GETDATE(), team_leader_ID = " . $_SESSION['op_ID'] . " 
											WHERE qa_item_ID = " . $_REQUEST['qa_item_ID'] . " AND operator_ID = " . $ops[$i] . ";";
		}
		$db->query($sql);
		
		$res->success = 1;
		echo json_encode($res);
	}

?>