<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];
    $r = $_REQUEST;

    if ($action == 'list_modules_on_hold') {
	    global $db;
	    $res = new StdClass();

	    //module data
	    $sql = "DECLARE @hold_line int;
	
			    SELECT @hold_line = dbo.fn_getJSONIntegerValue(ISNULL(line_options, '{}'), 'hold_line') FROM lines WHERE ID = ".$_REQUEST['line_id']." ;
		
			    SELECT
				    m.ID,
				    m.sequence,
				    m.VIN,
				    m.loaded_time,
                    m.pallet_number,
                    m.pallet_position,
				    dbo.fn_getJSONIntegerValue(ISNULL(l.line_options, '{}'), 'retrigger_position') as retrigger_position
			    FROM
				    modules m
				    JOIN lines l ON m.line_ID = l.ID
                WHERE l.ID = @hold_line
			    ORDER BY
				    m.sequence DESC;";
	    //echo $sql;
	    $res = $db->query($sql);
	    if (!$res) {
		    $res = array();
	    }
	    echo json_encode($res);
    }

    if ($action == 'list_components') {
	    global $db;
	    $res = new StdClass();
	    $module_ID = $_REQUEST['module_ID'];

	    // get components
	    $sql = "SELECT
				    ID,
				    part_number,
				    qty
			    FROM
				    components_in
			    WHERE
				    veh_id = {$_REQUEST['module_ID']}
			    ORDER BY
				    part_number ASC;";

	    echo json_encode($db->query($sql));
    }

    if ($action == 'rebuild_module') {
	    // set test to false to activate
	    $test = false;

	    global $db;
	    $res = new StdClass();
	    $res->success = false;

	    $module_ID = $_REQUEST['module_ID'];

	    $sql = "EXEC dbo.sp_MESAAS_SEQUENCE_buildFromHoldQueue @module_ID = ".$module_ID."";

	    $res = $db->query($sql);
	    echo json_encode($res);
    }

?>