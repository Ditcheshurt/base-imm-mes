<?php
    
    require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_lines") {
		$res = new StdClass();

		$sql = "SELECT c.*, ct.std_pack_qty, ct.containers_per_tare
				  FROM cells c
				     JOIN container_types ct ON c.container_type_ID = ct.ID
				  ORDER BY c.cell_desc";
		$res->lines = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "reprint_part") {
		$sql = "EXEC dbo.sp_MESAAS_BATCH_printPartLabel @built_part_ID = ".$_REQUEST['built_part_ID'].", @wip_part_ID = 0, @station_position_ID = 1, @station_ID = 0";
		$res = new StdClass();

		$db->query($sql);

		$res->sql = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "reprint_tare") {
		$sql = "EXEC dbo.sp_printTareLabel @tare_ID = ".$_REQUEST['tare_ID'].", @printer_ID = 0, @compute_qty = 1";
		$res = new StdClass();

		$db->query($sql);

		$res->sql = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "verify_tare") {
		$sql = "UPDATE tares SET verified_time = GETDATE() WHERE ID = ".$_REQUEST['tare_ID'];
		$res = new StdClass();

		$db->query($sql);

		$res->sql = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "end_as_partial") {
		$sql = "EXEC dbo.sp_MESAAS_BATCH_RACK_processEndTareAsPartial @tare_ID = ".$_REQUEST['tare_ID'].", @operator_ID = ".$_SESSION['op_ID'];
		$res = new StdClass();

		$db->query($sql);

		$res->sql = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

	if ($action == "get_part_history") {
		$sql = "SELECT h.*, s.station_desc, o.name
				  FROM built_part_history h
				  	  JOIN part_setup p ON h.part_ID = p.ID
				  	  JOIN stations s ON h.cell_ID = s.cell_ID AND h.station_ID = s.station_order
				  	  JOIN operators o ON h.operator = o.ID
				  WHERE h.built_part_ID = ".$_REQUEST['built_part_ID'];
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "get_tares_containers") {
		$res = new StdClass();
		$line_ID = $_REQUEST['line_ID'];

		$sql = "SELECT t.*, p.part_desc, p.part_number, 0 as complete, ct.std_pack_qty, ct.containers_per_tare, 1 as in_progress
				  FROM tares t
				  	  JOIN part_setup p ON t.part_ID = p.ID
				  	  JOIN cells c ON p.cell_ID = c.ID
				     JOIN container_types ct ON c.container_type_ID = ct.ID
				  WHERE t.end_time IS NULL
				     AND t.part_ID IN (SELECT id FROM part_setup WHERE cell_ID = ".$line_ID.")
				  UNION
				  SELECT *
				  FROM (
				  	  SELECT TOP 10 t.*, p.part_desc, p.part_number, 1 as complete, ct.std_pack_qty, ct.containers_per_tare, 0 as in_progress
					  FROM tares t
					  	  JOIN part_setup p ON t.part_ID = p.ID
					  	  JOIN cells c ON p.cell_ID = c.ID
					     JOIN container_types ct ON c.container_type_ID = ct.ID
					  WHERE t.end_time IS NOT NULL
					     AND t.part_ID IN (SELECT id FROM part_setup WHERE cell_ID = ".$line_ID.")
					  ORDER BY t.ID DESC) as v
				  ORDER BY complete, ID DESC";
		$res->tares = $db->query($sql);

		for ($i = 0; $i < count($res->tares); $i++) {
			$sql = "SELECT c.ID, c.start_time, c.end_time, ct.std_pack_qty
					  FROM containers c
					     JOIN tares t ON c.tare_ID = t.ID
					     JOIN part_setup ps ON t.part_ID = ps.ID
					     JOIN cells cl ON ps.cell_ID = cl.ID
					     JOIN container_types ct ON cl.container_type_ID = ct.ID
					  WHERE c.tare_ID = ".$res->tares[$i]['ID']."
					  ORDER BY c.id";
			$res->tares[$i]['containers'] = $db->query($sql);

			for ($j = 0; $j < count($res->tares[$i]['containers']); $j++) {
				$sql = "SELECT b.ID, b.serial, b.built_time
						  FROM built_parts b
						  WHERE b.container_ID = ".$res->tares[$i]['containers'][$j]['ID']."
						  ORDER BY b.id";
				$res->tares[$i]['containers'][$j]['built_parts'] = $db->query($sql);
			}
		}

		//now partials
		$sql = "SELECT *
				  FROM partial_containers pc
				  	  JOIN station_statuses ss ON pc.pack_out_ip_address = ss.ip_address
				  	  JOIN stations s ON ss.station_ID = s.ID
				  WHERE s.cell_ID = ".$line_ID."
				  ORDER BY pc.ID DESC";
		$res->partials = $db->query($sql);

		for ($j = 0; $j < count($res->partials); $j++) {
			$sql = "SELECT b.ID, b.serial, b.built_time, p.part_desc, p.part_number
					  FROM built_parts b
					     JOIN part_setup p on b.part_ID = p.ID
					  WHERE b.partial_container_ID = ".$res->partials[$j]['ID']."
					  ORDER BY b.id";
			$res->partials[$j]['built_parts'] = $db->query($sql);
		}

		echo json_encode($res);
	}
?>