<?php
	
	require_once("../init.php");
		
	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);
	
	function insertSelectedRecords($request, $db) {
		
		$selectedRecords = $_REQUEST['selectedRecords'];
		$numberOfRecords = count($selectedRecords);
		
		// Get line_ID based on the old asp implementation, this is only based on the first record as it should be the same across all records.
		// That makes me nervous because there isn't anything stopping anyone from paste data from multiple broadcasts for different lines.
		$query = "SELECT ID 
				  FROM  lines 
				  WHERE plant_code   = '{$selectedRecords[0]['PLT']}'
				  AND   module_code  = '{$selectedRecords[0][' MOD TYPE']}'";
		
		$lineId = $db->query($query)[0]['ID']; 
		
		if (!$lineId > 0)
		{ 
			echo json_encode(array('message' => sprintf('No line ID found for Plant Code : [%s] and Module Type : [%s]', $selectedRecords[0]['PLT'], $selectedRecords[0][' MOD TYPE']))); 
			return;
		}
		
		try 
		{
			for ($i = 0; $i < $numberOfRecords; $i++)
			{
				$loadTime = date("Y/m/d H:i:s");
				
				// Do not reformat the raw_data lines.
				
				$query = "INSERT INTO dbo.broadcasts_in (line_ID, msg_status, VIN, sequence, raw_data, load_time, module_type)
				  		  VALUES(
									{$lineId},
									'{$selectedRecords[$i]['MSG   ']}',
									'{$selectedRecords[$i]['VIN 17']}',
									'{$selectedRecords[$i]['SEQUENCE  ']}',
									
									'{$selectedRecords[$i]['MSG   ']}{$selectedRecords[$i]['PLT']}{$selectedRecords[$i]['SUPPLIER']}{$selectedRecords[$i]['VIN    ']}{$selectedRecords[$i]['MODEL CD']}{$selectedRecords[$i][' MOD TYPE']}{$selectedRecords[$i]['STATUS CD']}{$selectedRecords[$i]['SEQUENCE  ']}{$selectedRecords[$i][' TRANS TYPE']}{$selectedRecords[$i]['SALES CODE']}{$selectedRecords[$i]['PARTS']}{$selectedRecords[$i]['VIN 17']}',
									
									'{$loadTime}',
									'{$selectedRecords[$i][' MOD TYPE']}'
								)";
				
				$db->query($query);
				
				// SQL exceptions aren't being thrown. I don't know why.				
			} 				
		}
		catch (Exception $e)
	    {
	        echo json_encode(array('type' => 'error', 'message' => $e->getMessage()));
			return;
	    }	
        
        echo json_encode(array('type' => 'success', 'message' => sprintf('Records inserted: [%s]', $numberOfRecords)));	
	}
    
?>