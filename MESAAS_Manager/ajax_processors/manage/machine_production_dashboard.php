<?php

    require_once("../init.php");

	$action = $_REQUEST['action'];	
	
	if ($action === 'get_slides') {
		global $db;
		$res = new StdClass();

		if ($_REQUEST['machine_dashboard_group_ID'] == 0) {
			$machine_group_str = 'machine_dashboard_group_ID IS NULL ';
		} else {
			$machine_group_str = 'machine_dashboard_group_ID = '.$_REQUEST['machine_dashboard_group_ID'].' ';
		}

		$sql = "SELECT
					ID,
					slide_order,
					image_src,
					active,
					duration,
					machine_dashboard_group_ID
				FROM
					production_dashboard_slides
				WHERE
					".$machine_group_str."
				ORDER BY
					slide_order ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'get_machine_dashboard_groups') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					description
				FROM
					machine_dashboard_groups
				ORDER BY
					ID ASC;";

		$res = $db->query($sql);
		echo json_encode($res);	
	}

	if ($action === 'get_machine_dashboard_groups_slides') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					description
				FROM
					machine_dashboard_groups
				ORDER BY
					ID ASC;";

		$res = $db->query($sql);

		if ($res) {
			foreach($res as $i => &$group) {
				$sql = "SELECT
							ID,
							slide_order,
							image_src,
							active,
							duration,
							machine_dashboard_group_ID
						FROM
							production_dashboard_slides
						WHERE
							machine_dashboard_group_ID = ".$group['ID']."
						ORDER BY
							slide_order ASC;";
				$group['slides'] = $db->query($sql);
			}
		}

		echo json_encode($res);
	}

	if ($action == 'get_slide') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT
					ID,
					slide_order,
					image_src,
					active,
					duration,
					machine_dashboard_group_ID
				FROM
					production_dashboard_slides
				WHERE
					ID = ".$_REQUEST['slide_ID'].";";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'insert_slide') {
		global $db;
		$res = new StdClass();

		$data = $_REQUEST['slide_data'];

		if ($data['machine_dashboard_group_ID'] == 0) {
			$machine_group_str = 'NULL ';
		} else {
			$machine_group_str = $data['machine_dashboard_group_ID'];
		}

		$sql = "INSERT INTO
					production_dashboard_slides (
						slide_order,
						duration,
						active,
						machine_dashboard_group_ID
					)
				OUTPUT INSERTED.ID
				VALUES (
					".$data['slide_order'].",
					".$data['duration'].",
					".$data['active'].",
					".$machine_group_str."
				);";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}


	if ($action == 'update_slide') {
		global $db;
		$res = new StdClass();

		$data = $_REQUEST['slide_data'];

		if (isset($data['image_src']) && $data['image_src']) {
			$image_str = ",image_src = '".$data['image_src']."' ";
		} else {
			$image_str = '';
		}
		
		$sql = "UPDATE
					production_dashboard_slides
				SET
					slide_order = ".$data['slide_order'].",
					duration = ".$data['duration'].",
					active = ".$data['active']."
					".$image_str."
				WHERE
					ID = ".$data['slide_ID'].";";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}


	if ($action == 'delete_slide') {
		global $db;
		$res = new StdClass();

		try {
			$dir = $_SERVER['DOCUMENT_ROOT'].'\\machine_mes2\\public\\images\\dashboard_slides\\slide_'.$_REQUEST['slide_ID'];
			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

			foreach($files as $file) {
				if ($file->isDir()){
					rmdir($file->getRealPath());
				} else {
					unlink($file->getRealPath());
				}
			}

			$res = rmdir($dir);
		} catch (Exception $e) {
			$res = true;
		}

		if ($res) {
			$sql = "DELETE
					FROM
						production_dashboard_slides
					WHERE
						ID = ".$_REQUEST['slide_ID'].";";
			//die($sql);
			$res = $db->query($sql);
		}

		echo json_encode($res);
	}


	if ($action == 'store_slide_image') {
		global $db;
		$file = $_FILES['file'];
		$tmp_name = $file['tmp_name'];
		$name = $file['name'];
		$extension = pathinfo($name, PATHINFO_EXTENSION);

		$directory = $_SERVER['DOCUMENT_ROOT'].'\\machine_mes2\\public\\images\\dashboard_slides\\slide_'.$_REQUEST['slide_ID'];

		if (!file_exists($directory)) {
			mkdir($directory, 0777, true);
		}

		$path = '/machine_mes2/public/images/dashboard_slides/slide_'.$_REQUEST['slide_ID'] . '/' . $name;
		$real_path = $directory . '\\' . $name;

		if ($file['size'] > 0) {
			if ($file['size'] < 10000000000 && is_uploaded_file($tmp_name) && move_uploaded_file($tmp_name, $real_path)) {
				
				$sql = "UPDATE
							production_dashboard_slides
						SET
							image_src = '".$path."'
						WHERE
							ID = ".$_REQUEST['slide_ID'];

				$res = $db->query($sql);

				if ($res === null) {
					$res = array('location' => $path);
				} else {
					$res = false;
				}
				
			} else {
				$res = false;
			}
		} else {
			$res = array('location' => null);
		}

		echo json_encode($res);
	}
?>