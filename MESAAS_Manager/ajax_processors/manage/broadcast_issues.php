<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;

	if ($action == "get_issues") {
		get_issues();
	}

	if ($action == "ack_issue") {
		$sql = "UPDATE broadcast_issues
				  SET ack_time = GETDATE(), ack_by = ".$_SESSION['op_ID']."
				  WHERE ID = ".$_REQUEST['issue_ID'];
		$db->query($sql);

		get_issues();
	}

	if ($action == "save_comment") {
		$sql = "UPDATE broadcast_issues
				  SET fix_comment = '".fixDb($_REQUEST['fix_comment'])."'
				  WHERE ID = ".$_REQUEST['issue_ID'];
		$db->query($sql);

		get_issues();
	}

	if ($action == "load_all_BOMs_for_line") {
		echo json_encode($db->query("SELECT * FROM module_unique_BOMs WHERE line_ID = ".$_REQUEST['line_ID']." ORDER BY last_module_time DESC"));
	}

	if ($action == "get_issue_actions") {
		$sql = "SELECT a.*
				  FROM broadcast_issue_actions a
				     JOIN broadcast_issues i ON a.broadcast_issue_ID = i.ID
				  	  JOIN broadcasts_previews_in b ON i.broadcast_ID = b.ID AND b.line_ID = ".$_REQUEST['line_ID']."
				  	     AND b.VIN = '".$_REQUEST['VIN']."'
				  WHERE i.broadcast_type = 'PREVIEW'
				  ORDER BY a.ID";
		echo json_encode($db->query($sql));
	}

	if ($action == "get_all_issue_actions") {
		$sql = "SELECT a.*
				  FROM broadcast_issue_actions a
				     JOIN broadcast_issues i ON a.broadcast_issue_ID = i.ID
				  	  JOIN broadcasts_previews_in b ON i.broadcast_ID = b.ID
				  WHERE i.broadcast_type = 'PREVIEW'
				  	  AND i.ID = ".$r['issue_ID']."
				  UNION
				  SELECT a.*
				  FROM broadcast_issue_actions a
				     JOIN broadcast_issues i ON a.broadcast_issue_ID = i.ID
				  	  JOIN broadcasts_in b ON i.broadcast_ID = b.ID
				  WHERE i.broadcast_type = 'BUILD'
				  	  AND i.ID = ".$r['issue_ID']."
				  ORDER BY ID";
		echo json_encode($db->query($sql));
	}

	if ($action == "save_and_mark_fixed") {
		$response = new StdClass();
		$response->success = 1;
		$response->msg = "";

		$issue_ID = $_REQUEST['issue_ID'];
		$new_bom_ID = 0;
		$sql = "SELECT *
				  FROM broadcast_issues WHERE ID = ".$issue_ID;
		$res = $db->query($sql);
		if (count($res) > 0) {
			$broadcast_type = $res[0]['broadcast_type'];
			$broadcast_ID =   $res[0]['broadcast_ID'];
			$response->broadcast_ID = $broadcast_ID;
			$response->broadcast_type = $broadcast_type;

			//get raw data BOM string
			if ($broadcast_type == "PREVIEW") {
				$sql = "SELECT line_ID, dbo.fn_MESAAS_SEQUENCE_getBOMStringFromRawData(raw_data) as bom_string FROM broadcasts_previews_in WHERE ID = ".$broadcast_ID;
			}
			if ($broadcast_type == "BUILD") {
				$sql = "SELECT line_ID, dbo.fn_MESAAS_SEQUENCE_getBOMStringFromRawData(raw_data) as bom_string FROM broadcasts_in WHERE ID = ".$broadcast_ID;
			}
			$res = $db->query($sql);
			if (count($res) > 0) {
				$bom_string = $res[0]['bom_string'];
				$line_ID = $res[0]['line_ID'];
			}
		}

		//generate SQL for inserting the actions
		for ($i = 0; $i < count($r['BOM_actions']); $i++) {
			$a = $r['BOM_actions'][$i];
			$insert_action_sql = "";
			switch ($a['action']) {
				case "CHANGE_REV":
					$insert_action_sql .= "INSERT INTO broadcast_issue_actions (broadcast_issue_ID, part_number, action_desc, action, new_rev) VALUES (
								".$issue_ID.",
								'".$a['part_number']."',
								'".$a['action_desc']."',
								'".$a['action']."',
								'".$a['new_rev']."');
							";
					break;
				case "CHANGE_QTY":
					$insert_action_sql .= "INSERT INTO broadcast_issue_actions (broadcast_issue_ID, part_number, action_desc, action, new_qty) VALUES (
								".$issue_ID.",
								'".$a['part_number']."',
								'".$a['action_desc']."',
								'".$a['action']."',
								'".$a['new_qty']."');
							";
					break;
				case "CHANGE_PART":
					$insert_action_sql .= "INSERT INTO broadcast_issue_actions (broadcast_issue_ID, part_number, action_desc, action, new_part) VALUES (
								".$issue_ID.",
								'".$a['part_number']."',
								'".$a['action_desc']."',
								'".$a['action']."',
								'".$a['new_part']."');
							";
					break;
				case "REMOVE_PART":
					$insert_action_sql .= "INSERT INTO broadcast_issue_actions (broadcast_issue_ID, part_number, action_desc, action) VALUES (
								".$issue_ID.",
								'".$a['part_number']."',
								'".$a['action_desc']."',
								'".$a['action']."');
							";
					break;
			}
		}

		//figure out what the new bom string would be, based on the changes
		//do this by looping through each BOM component, then through each action.
		$new_bom_string = "";
		for ($i = 0; $i < strlen($bom_string); $i+=18) {
			$part_number = substr($bom_string, $i, 10);
			$qty 			 = substr($bom_string, $i + 10, 8);

			$action_found = false;
			for ($j = 0; $j < count($r['BOM_actions']); $j++) {
				$a = $r['BOM_actions'][$j];
				if ($part_number == $a['part_number']) {
					$action_found = true;
					$root_pn = getRootPartNumber($part_number);
					switch ($a['action']) {
						case "CHANGE_REV":
							$new_bom_string .= $root_pn.$a['new_rev'].$qty;
							break;
						case "CHANGE_QTY":
							$new_bom_string .= $part_number.formatQty($a['new_qty']);
							break;
						case "CHANGE_PART":
							$new_bom_string .= $a['new_part'].$qty;
							break;
						case "REMOVE_PART":
							//add nothing!
							break;
					}
				}
			}

			if (!$action_found) {
				$new_bom_string .= $part_number.$qty;
			}
		}

		//if they flagged that
		$response->create_new_BOM_ID = $r['create_new_BOM_ID'];
		if ($r['create_new_BOM_ID'] == 1) {

			//make sure it doesn't already exist
			$sql = "SELECT *
					  FROM module_unique_BOMs
					  WHERE line_ID = ".$line_ID."
					  	  AND bom_string = '".$new_bom_string."'";
			$res = $db->query($sql);
			if (count($res) > 0) {
				$response->new_bom_ID = 0;
				$response->existing_bom_ID = $res[0]['bom_ID'];
				$response->success = 0;
				$response->msg = "BOM string already exists!";
			} else {
				//get last BOM ID
				$sql = "SELECT MAX(bom_ID) + 1 as new_bom_ID FROM module_unique_BOMs WHERE line_ID = ".$line_ID;
				$res = $db->query($sql);
				if (count($res) > 0) {
					$new_bom_ID = $res[0]['new_bom_ID'];
				} else {
					$new_bom_ID = 1;
				}

				//insert new BOM ID
				$sql = "INSERT INTO module_unique_BOMs (line_ID, bom_string, bom_ID, created_time, last_module_time) VALUES (
							 	".$line_ID.",
							 	'".$new_bom_string."',
							 	".$new_bom_ID.",
							 	GETDATE(),
							 	GETDATE()
						   )";
				$db->query($sql);

				//also populate the BOM ID for any satch line
				$sql = "SELECT ISNULL(ID, 0) as satch_line_ID
							FROM lines
							WHERE main_line_ID = ".$line_ID."
								AND line_type = 7";
				$res= $db->query($sql);
				if (count($res) > 0) {
					$sql = "EXEC dbo.sp_MESAAS_SEQUENCE_populateBOMIDForSatchLine @satch_line_ID = ".$res[0]['satch_line_ID'].", @bom_ID = ".$new_bom_ID;
					$db->query($sql);
				}

				$response->new_bom_ID = $new_bom_ID;
				$response->existing_bom_ID = 0;
			}
		} else {
			//no new BOM ID to generate, but make sure it matches one that exists already
			$sql = "SELECT *
					  FROM module_unique_BOMs
					  WHERE line_ID = ".$line_ID."
					  	  AND bom_string = '".$new_bom_string."'";
			$res = $db->query($sql);
			if (count($res) == 0) {
				$response->success = 0;
				$response->msg = "No BOM exists that matches the actions you've selected!";
			}
		}

		if ($response->success == 1) {
			if ($insert_action_sql != "") {
				$db->query($insert_action_sql);
			}

			$sql = "UPDATE broadcast_issues SET fixed_time = GETDATE(), fixed_by = ".$_SESSION['op_ID']." WHERE ID = ".$issue_ID;
			$db->query($sql);
		}

		echo json_encode($response);
	}

	function get_issues() {
		global $db;
		$res = new StdClass();

		$res->issues = $db->query("SELECT TOP 100 i.*, o.name as ack_operator, o2.name as fix_operator, l.line_code, b.VIN, b.sequence, b.ID as broadcast_ID, b.raw_data, b.line_ID
											FROM broadcast_issues i
												JOIN broadcasts_previews_in b ON i.broadcast_ID = b.ID
												JOIN lines l ON b.line_ID = l.ID
												LEFT OUTER JOIN MES_COMMON.dbo.operators o ON i.ack_by = o.ID
												LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON i.fixed_by = o2.ID
											WHERE i.broadcast_type = 'PREVIEW'
											UNION
											SELECT TOP 100 i.*, o.name as ack_operator, o2.name as fix_operator, l.line_code, b.VIN, b.sequence, b.ID as broadcast_ID, b.raw_data, b.line_ID
											FROM broadcast_issues i
												JOIN broadcasts_in b ON i.broadcast_ID = b.ID
												JOIN lines l ON b.line_ID = l.ID
												LEFT OUTER JOIN MES_COMMON.dbo.operators o ON i.ack_by = o.ID
												LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON i.fixed_by = o2.ID
											WHERE i.broadcast_type = 'BUILD'
											ORDER BY ID DESC");
		$res->part_descriptions = $db->query("SELECT DISTINCT part_number, part_desc FROM mrp_part_mstr");
		echo json_encode($res);
	}

	function getRootPartNumber($part_number) {
		return substr($part_number, 0, 8);
	}

	function getRevLevel($part_number) {
		return substr($part_number, 0, -2);
	}

	function formatQty($qty) {
		return sprintf("%03d", $qty).".0000";
	}

?>