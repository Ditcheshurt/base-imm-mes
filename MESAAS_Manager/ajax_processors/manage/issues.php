<?php

    require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "select_issues") {
		global $db;
		$res = new StdClass();		

		$sql = "select top 200 * from issue_tracker order by id desc";
  
		$res->issues = $db->query($sql);

		echo json_encode($res);
	}
	
	if ($action == "insert_issue") {
		global $db;
		$res = new StdClass();
		
		$r = array();
		foreach ($_REQUEST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field == 'line_id' || $field == 'station_id') {
					$r[$field] = (int)$value;
				} else {
					$r[$field] = "'".$value."'";
				}
				
			}
		}

		$sql = "INSERT INTO issue_tracker
				   (".implode(', ', array_keys($r)).")
				VALUES
				   (".implode(', ', array_values($r)).")";

		//die($sql);
		$res->issues = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "delete_issue") {
		global $db;
		$res = new StdClass();
		
		$r = $_REQUEST;
		
		$sql = "DELETE FROM [dbo].[issue_tracker] WHERE id = '".$r['id']."'";
		$res->issues = $db->query($sql);

		echo json_encode($res);
	}
	
	if ($action == "update_issue") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$r = array();
		foreach ($_REQUEST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field == 'line_id' || $field == 'station_id') {
					$r[$field] = (int)$value;
				} else {
					$r[$field] = "'".$value."'";
				}
				
			}
		}
		
		$sql = "UPDATE issue_tracker SET ";
			    

		foreach ($_REQUEST as $field => $value) {
			if ($field != 'action' && $field != 'id' && $value) {
				if ($field == 'line_id' || $field == 'station_id') {
					$sql .= $field." = ".$value.", ";
				} else {
					$sql .= $field." = '".$value."', ";
				}
			}
		}

		$sql = trim($sql, ', ');

		$sql .= " WHERE id = ".$_REQUEST['id'];

		//die($sql);
		$res->issues = $db->query($sql);

		echo json_encode($res);
	}
	
?>