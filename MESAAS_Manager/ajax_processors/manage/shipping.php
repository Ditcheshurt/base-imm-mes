<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_production_snapshots ($request, $db) {
		$sql = "SELECT
					l.ID AS line_ID,
					l.line_code,
					ps.last_broadcast_time_ago,
					ps.current_buffer,
					ps.current_queue,
					l.parts_per_rack,
					CASE WHEN so.option_value = 'yes' THEN 1 ELSE 0 END AS override_rack_count
				FROM
					production_snapshot ps
					LEFT JOIN lines l ON ps.line_ID = l.ID
					LEFT JOIN _system_options so ON so.option_name = 'override_line_' + CAST(l.ID AS VARCHAR) + '_rack_count'
				WHERE
					l.system_ID = {$request['system_ID']};";
		$lines = $db->query($sql);
		echo json_encode($lines);
	}

	function set_override_status ($request, $db) {
		if ($request['override']) {
			$override = 'yes';
		} else {
			$override = 'no';
		}

		$sql = "UPDATE
					_system_options
				SET
					option_value = '{$override}'
				WHERE
					option_name = 'override_line_{$request['line_ID']}_rack_count';";

		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
?>