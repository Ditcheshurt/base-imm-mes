<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);
	
	function get_machine_types() {
		global $db;
		$sql = "SELECT [ID]
					,[machine_type_description]
				FROM [machine_types]
				ORDER BY [machine_type_description]";
		echo json_encode($db->query($sql));
	}
	
	function get_defect_categories() {
		global $db;

		$sql = "SELECT DISTINCT 
					category
				FROM 
					[defect_reasons]
				ORDER BY 
					[category]";
	
		echo json_encode($db->query($sql));		
	}

	function get_defect_codes() {
		global $db;
		
		$sql = "SELECT ID, series, defect_description, machine_type_ID, active, L,
						CASE WHEN parent_ID = 0 THEN NULL ELSE parent_ID END as parent_ID
					FROM v_defect_reasons
					ORDER BY S";
		echo json_encode($db->query($sql));
	}

	function save_code(){
		global $db;
		if ($_REQUEST['edit_type'] == 'ADD' or $_REQUEST['edit_type'] == 'ADD_Parent') {
			insert_code();
		} else {
			update_code();
		}

		get_defect_codes();
	}

	function insert_code(){
		global $db;
		
		if (isset($_REQUEST['series'])) {
			$series = $_REQUEST['series'];
			$sql = "INSERT INTO machine_defect_reasons (parent_ID, defect_description, active, series) VALUES (
				 	".$_REQUEST['parent_ID'].",
				 	'".fixDB($_REQUEST['defect_description'])."',
				 	".$_REQUEST['active'].",
					'".$series."')";
		} else {
			$sql = "INSERT INTO machine_defect_reasons (parent_ID, defect_description, active) VALUES (
				 	".$_REQUEST['parent_ID'].",
				 	'".fixDB($_REQUEST['defect_description'])."',
				 	".$_REQUEST['active'].")";
		}		
		
	//die($sql);
		$db->query($sql);
	}

	function update_code(){
		global $db;
		
		if (isset($_REQUEST['series'])) {
			$sql = "UPDATE machine_defect_reasons
					SET parent_ID = '".$_REQUEST['parent_ID']."',
						 defect_description = '".fixDB($_REQUEST['defect_description'])."',
						 series = '".$_REQUEST['series']."',
						 active = '".$_REQUEST['active']."' 
					WHERE ID = ".$_REQUEST['ID'];
		} else {
			$sql = "UPDATE machine_defect_reasons
					SET parent_ID = '".$_REQUEST['parent_ID']."',
						 defect_description = '".fixDB($_REQUEST['defect_description'])."',
						 series = null,
						 active = '".$_REQUEST['active']."' 
					WHERE ID = ".$_REQUEST['ID'];
		}

		$db->query($sql);
	}
	
	function clone_defect_codes() {
		global $db;		
		
		$from_machine_type_ID = $_REQUEST['from_machine_type_ID'];
		$to_machine_type_ID	=$_REQUEST['to_machine_type_ID'];
		
		$sql = "EXEC [dbo].[sp_cloneDefectReasons]
					@from_machine_type_ID = $from_machine_type_ID,
					@to_machine_type_ID = $to_machine_type_ID";

		$db->query($sql);
		
		get_defect_codes();
	}

	function del_node(){
		global $db;
		$sql = "UPDATE machine_defect_reasons
					SET active = 0
					WHERE ID = ".$_REQUEST['node'];
		echo $db->query($sql);
	}


?>