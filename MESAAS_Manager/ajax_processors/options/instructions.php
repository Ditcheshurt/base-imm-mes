<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_systems_lines_stations") {
		$res = new StdClass();

		$sql = "SELECT * FROM MES_COMMON.dbo.systems WHERE process_type IS NOT NULL AND system_active = 1 ORDER BY system_name";
		$res->systems = $db->query($sql);

		$sql = "SELECT * FROM MES_COMMON.dbo.v_MES_LINES ORDER BY system_ID, line_ID";
		$res->lines = $db->query($sql);

		$sql = "SELECT * FROM MES_COMMON.dbo.v_MES_LINE_STATIONS ORDER BY system_ID, line_ID, station_order";
		$res->stations = $db->query($sql);

		//$sql = "SELECT * FROM station_statuses ORDER BY station_ID";
		//$res->station_statuses = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "load_part_groups") {
		loadPartGroups();
	}

	if ($action == "load_wip_parts") {
		loadWIPParts();
	}

	if ($action == "load_instructions") {
		loadInstructions();
	}

	if ($action == "load_instructions_for_part") {
		loadInstructionsForPart();
	}

	if ($action == "move_instr") {
		$instr_ID = $_REQUEST['instr_ID'];
		addRevision($instr_ID, "MOVE");

		if ($_REQUEST['process_type'] == "BATCH") {
			$sql = "EXEC sp_MESAAS_BATCH_EDITOR_moveInstruction @instr_ID = ".$instr_ID.", @direction = '".$_REQUEST['direction']."'";
			$db->query($sql);
			loadInstructions();
		}
		if ($_REQUEST['process_type'] == "SEQUENCED") {
			$sql = "EXEC sp_MESAAS_SEQUENCE_EDITOR_moveInstruction @instr_ID = ".$instr_ID.", @direction = '".$_REQUEST['direction']."'";
			$db->query($sql);
			loadInstructionsForPart();
		}
	}

	if ($action == "move_part") {
		$r = $_REQUEST;
		//addRevision($instr_ID, "MOVE");
		$sql = "EXEC dbo.sp_MESAAS_SEQUENCE_EDITOR_movePart
					@line_ID = ".$r['line_ID'].",
					@station_ID = ".$r['station_ID'].",
					@part_order = ".$r['part_order'].",
					@direction = '".$r['direction']."'";
		$db->query($sql);

		loadInstructions();
	}

	if ($action == "save_instr") {
		$r = $_REQUEST;
		if ($r['process_type'] == "BATCH") {
			if ($r['part_ID'] == "0") {
				$r['part_ID'] = "NULL";
			}
		}

		if ($r['process_type'] == "SEQUENCED") {
			$wip_part_ID = "NULL";
			if ($r['line_type'] == "1") {
				if ($r['wip_part_ID'] != "0") {
					$wip_part_ID = $r['wip_part_ID'];
				}
			}
		}


		if ($r['edit_type'] == 'ADD') {
			if ($r['process_type'] == "BATCH") {
				$sql = "INSERT INTO instructions (cell_ID, station_order, part_id, instr_order, test_type, title, text, prompt, image_file, active_flag, instr_options, max_seconds, jump_back_steps) VALUES (
						 ".$r['line_ID'].",
						 ".$r['station_order'].",
						 ".$r['part_ID'].",
						 ".$r['instr_order'].",
						 ".$r['test_type'].",
						 '".fixDB($r['title'])."',
						 '".fixDB($r['instr_text'])."',
						 '".fixDB($r['prompt'])."',
						 '".fixDB($r['image_file'])."',
						 ".$r['active_flag'].",
						 '".fixDB($r['instr_options'])."',
						 ".$r['max_seconds'].",
						 ".$r['jump_back_steps']."
						 )";
			}
			if ($r['process_type'] == "SEQUENCED") {
				$sql = "SET NOCOUNT ON;
						 INSERT INTO instructions (line_ID, station_ID, part_number, current_rev_level, part_order, instr_order, test_type, title, instr_text, prompt, image_file, active_flag, instr_options, wip_part_ID, max_seconds, jump_back_steps) VALUES (
						 ".$r['line_ID'].",
						 ".$r['station_ID'].",
						 '".fixDB($r['part_number'])."',
						 '".fixDB($r['current_rev_level'])."',
						 ".$r['part_order'].",
						 ".$r['instr_order'].",
						 ".$r['test_type'].",
						 '".fixDB($r['title'])."',
						 '".fixDB($r['instr_text'])."',
						 '".fixDB($r['prompt'])."',
						 '".fixDB($r['image_file'])."',
						 ".$r['active_flag'].",
						 '".fixDB($r['instr_options'])."',
						 ".$wip_part_ID.",
						 ".$r['max_seconds'].",
						 ".$r['jump_back_steps'].");
						 SELECT @@IDENTITY AS new_ID;";
			}
			$res = $db->query($sql);

			if (count($res) > 0) {
				$r['instr_ID'] = $res[0]['new_ID'];
			}
		}

		if ($r['edit_type'] == 'EDIT') {
			addRevision($r['instr_ID'], "EDIT");

			//get existing options and merge
			$sql = "SELECT ISNULL(instr_options, '{}') as instr_options FROM instructions WHERE id = ".$r['instr_ID'];
			$res = $db->query($sql);
			$old_ops = json_decode($res[0]['instr_options'], true);
			//echo $old_ops;

			$new_ops = json_decode($r['instr_options'], true);

			$merged_arr = array_merge($old_ops, $new_ops);

			$ops = json_encode($merged_arr);

			if ($ops == "[]") {
				$ops = "{}";
			}

			if ($r['process_type'] == "BATCH") {
				$sql = "UPDATE instructions
							SET part_id = ".$r['part_ID'].",
								instr_order = ".$r['instr_order'].",
								test_type = ".$r['test_type'].",
								title = '".fixDB($r['title'])."',
								text = '".fixDB($r['instr_text'])."',
								prompt = '".fixDB($r['prompt'])."',
								image_file = '".fixDB($r['image_file'])."',
								active_flag = ".$r['active_flag'].",
								instr_options = '".fixDB($ops)."',
								max_seconds = ".$r['max_seconds'].",
								jump_back_steps = ".$r['jump_back_steps']."
							WHERE id = ".$r['instr_ID'];
			}
			if ($r['process_type'] == "SEQUENCED") {
				$sql = "UPDATE instructions
							SET instr_order = ".$r['instr_order'].",
								test_type = ".$r['test_type'].",
								title = '".fixDB($r['title'])."',
								instr_text = '".fixDB($r['instr_text'])."',
								prompt = '".fixDB($r['prompt'])."',
								image_file = '".fixDB($r['image_file'])."',
								active_flag = ".$r['active_flag'].",
								instr_options = '".fixDB($ops)."',
								wip_part_ID = ".$wip_part_ID.",
								max_seconds = ".$r['max_seconds'].",
								jump_back_steps = ".$r['jump_back_steps']."
							WHERE id = ".$r['instr_ID'];
			}

			$db->query($sql);
		}

		if ($r['process_type'] == "BATCH") {
			loadInstructions();
		}
		if ($r['process_type'] == "SEQUENCED") {

			//do dependent and subtractive parts
			$sql = "DELETE FROM dependent_instructions WHERE instr_ID = ".$r['instr_ID'].";";
			$db->query($sql);

			if ($r['dependent'] != "") {
				$dep = explode("~", $r['dependent']);
				for ($i = 0; $i < count($dep); $i++) {
					$sql = "INSERT INTO dependent_instructions (instr_ID, dependent_part) VALUES (
								".$r['instr_ID'].",
								'".fixDB($dep[$i])."');";
					$db->query($sql);
				}
			}

			$sql = "DELETE FROM instruction_subtractive_parts WHERE instr_ID = ".$r['instr_ID'].";";
			$db->query($sql);

			if ($r['subtractive'] != "") {
				$sub = explode("~", $r['subtractive']);
				for ($i = 0; $i < count($sub); $i++) {
					$sql = "INSERT INTO instruction_subtractive_parts (instr_ID, subtractive_part) VALUES (
								".$r['instr_ID'].",
								'".fixDB($sub[$i])."');";
					$db->query($sql);
				}
			}

			loadInstructionsForPart();
		}
	}

	if ($action == "delete_instr") {
		$r = $_REQUEST;

		addRevision($r['instr_ID'], "DELETE");

		$sql = "DELETE FROM instructions WHERE ID = ".$r['instr_ID'];
		$db->query($sql);

		if ($r['process_type'] == "BATCH") {
			loadInstructions();
		}

		if ($r['process_type'] == "SEQUENCED") {
			loadInstructionsForPart();
		}
	}

	if ($action == "clone_instr") {
		$r = $_REQUEST;

		if ($r['process_type'] == "BATCH") {
			if ($r['part_ID'] == "0") {
				$r['part_ID'] = "NULL";
			}
			$sql = "INSERT INTO instructions (cell_ID, station_order, part_id, instr_order, test_type, title, text, prompt, image_file, active_flag, instr_options)
						SELECT cell_ID,
							station_order,
							".$r['part_ID'].",
							".$r['instr_order'].",
							test_type,
							title,
							text,
							prompt,
							image_file,
							active_flag,
							instr_options
						FROM instructions
						WHERE id = ".$r['instr_ID'];

			$db->query($sql);
			loadInstructions();
		}

		if ($r['process_type'] == "SEQUENCED") {
			$sql = "INSERT INTO instructions (line_ID, station_ID, part_number, current_rev_level, part_order, instr_order, test_type, title, instr_text, prompt, image_file, active_flag, instr_options)
						SELECT line_ID,
							station_ID,
							part_number,
							current_rev_level,
							part_order,
							".$r['instr_order'].",
							test_type,
							title,
							instr_text,
							prompt,
							image_file,
							active_flag,
							instr_options
						FROM instructions
						WHERE id = ".$r['instr_ID'];
			$db->query($sql);
			loadInstructionsForPart();
		}
	}

	if ($action == "load_instruction_revisions") {
		$sql = "SELECT ir.*, t.type_desc, ISNULL(o.name, 'UNKNOWN') as revisor
				  FROM instruction_revisions ir
				  	  JOIN test_types t ON ir.test_type = t.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o ON ir.revised_by_operator_ID = o.ID
				  WHERE ir.instr_ID = ".$_REQUEST['instr_ID']."
				  ORDER BY revised_time DESC";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "save_part") {
		$r = $_REQUEST;
		if ($r['edit_type'] == 'ADD') {
			$sql = "INSERT INTO instructions (line_ID, part_number, current_rev_level, station_ID, part_order, instr_order, test_type, title, instr_text, prompt, image_file, active_flag, instr_options) VALUES (
					 ".$r['line_ID'].",
					 '".fixDB($r['new_part_number'])."',
					 '".fixDB($r['new_current_rev_level'])."',
					 ".$r['station_ID'].",
					 ".fixDB($r['new_part_order']).",
					 10,
					 ".$GLOBALS['APP_CONFIG']['MESAAS_badge_scan_test_type'].",
					 'NEW_ADDED_PART',
					 '',
					 '',
					 NULL,
					 0,
					 '{}')";
			$db->query($sql);
			//echo "QUERY: ".$sql."\n";
		}
		if ($r['edit_type'] == 'EDIT') {
			//addRevision($r['instr_ID'], "EDIT");

			$sql = "UPDATE instructions
						SET part_number = '".$r['new_part_number']."',
							part_order = ".$r['new_part_order'].",
							current_rev_level = '".$r['new_current_rev_level']."'
						WHERE line_ID = ".$r['line_ID']."
							AND station_ID = ".$r['station_ID']."
							AND part_number = '".$r['old_part_number']."'
							AND part_order = ".$r['old_part_order']."
							AND current_rev_level = '".$r['old_current_rev_level']."'";
			$db->query($sql);
			//echo "QUERY: ".$sql."\n";
		}
		loadInstructions();
	}

	if ($action == "delete_part") {
		$r = $_REQUEST;
		$sql = "DELETE FROM instructions
				  WHERE line_ID = ".$r['line_ID']."
							AND station_ID = ".$r['station_ID']."
							AND part_number = '".$r['part_number']."'
							AND part_order = ".$r['part_order']."
							AND current_rev_level = '".$r['current_rev_level']."'";
		$db->query($sql);
		//echo "QUERY: ".$sql."\n";
		loadInstructions();
	}

	if ($action == "clone_part") {
		$r = $_REQUEST;
		$sql = "INSERT INTO instructions (line_ID, part_number, current_rev_level, station_ID, part_order, instr_order, test_type, title, instr_text, prompt, image_file, active_flag, instr_options)
				  SELECT line_ID, '".fixDB($r['new_part_number'])."', '".fixDB($r['new_current_rev_level'])."', ".$r['new_station_ID'].", ".$r['new_part_order'].", instr_order, test_type, title, instr_text, prompt, image_file, active_flag, instr_options
				  FROM instructions
				  WHERE line_ID = ".$r['line_ID']."
				  			AND station_ID = ".$r['station_ID']."
							AND part_order = ".$r['old_part_order']."
							AND part_number = '".$r['old_part_number']."'
							AND current_rev_level = '".$r['old_current_rev_level']."'";
		$db->query($sql);
		//echo "QUERY: ".$sql."\n";
		loadInstructions();
	}

	//WIP PARTS:
	if ($action == "save_wip_part") {
		$r = $_REQUEST;
		if ($r['edit_type'] == "ADD") {
			$sql = "INSERT INTO wip_part_setup (ID, line_ID, part_number, part_desc) VALUES (
				 ".$r['new_wip_part_ID'].",
				 ".$r['line_ID'].",
				 '".fixDB($r['part_number'])."',
				 '".fixDB($r['part_desc'])."');";
		}
		if ($r['edit_type'] == "EDIT") {
			$sql = "UPDATE wip_part_setup
					  SET ID = ".$r['new_wip_part_ID'].",
					  	   part_number = '".fixDB($r['part_number'])."',
					  	   part_desc = '".fixDB($r['part_desc'])."'
					  WHERE id = ".$r['old_wip_part_ID'];
		}
		$db->query($sql);

		loadWIPParts();
	}

	if ($action == "delete_wip_part") {
		$r = $_REQUEST;
		$sql = "DELETE FROM wip_part_setup WHERE id = ".$r['wip_part_ID'];
		$db->query($sql);

		loadWIPParts();
	}

	if ($action == 'load_station_options') {
		if ($_REQUEST['process_type'] == 'BATCH') {
			$sql = "SELECT
						cycle_time AS target_cycle_time,
						transit_time
					FROM
						stations
					WHERE
						ID = ".$_REQUEST['station_ID'];
		} else if ($_REQUEST['process_type'] == 'SEQUENCED') {
			$sql = "SELECT
						target_takt_time AS target_cycle_time,
						transit_time
					FROM
						line_stations
					WHERE
						ID = ".$_REQUEST['station_ID'];
		}
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'update_station_options') {
		if ($_REQUEST['process_type'] == 'BATCH') {
			$sql = "UPDATE
						stations
					SET
						cycle_time = ".$_REQUEST['target_cycle_time'].",
						transit_time = ".$_REQUEST['transit_time']."
					WHERE
						ID = ".$_REQUEST['station_ID'];
		} else if ($_REQUEST['process_type'] == 'SEQUENCED') {
			$sql = "UPDATE
						line_stations
					SET
						target_takt_time = ".$_REQUEST['target_cycle_time'].",
						transit_time = ".$_REQUEST['transit_time']."
					WHERE
						ID = ".$_REQUEST['station_ID'];
		}
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}

	function addRevision($instr_ID, $revised_action) {
		global $db;

		if (!isset($_REQUEST['reason'])) {
			$_REQUEST['reason'] = "";
		}

		if ($_REQUEST['process_type'] == "BATCH") {
			$sql = "INSERT INTO instruction_revisions ([instr_ID]
						  ,[revised_time]
						  ,[revised_by_operator_ID]
						  ,[revised_action]
						  ,[cell_ID]
						  ,[station_order]
						  ,[part_id]
						  ,[instr_order]
						  ,[test_type]
						  ,[title]
						  ,[text]
						  ,[prompt]
						  ,[image_file]
						  ,[active_flag]
						  ,[instr_options]
						  ,[revision_reason])
					  SELECT ID, GETDATE(), ".$_SESSION['op_ID'].", '".$revised_action."',[cell_ID]
						,[station_order]
						,[part_id]
						,[instr_order]
						,[test_type]
						,[title]
						,[text]
						,[prompt]
						,[image_file]
						,[active_flag]
						,[instr_options]
						,'".fixDB($_REQUEST['reason'])."'
					  FROM instructions
					  WHERE id = ".$instr_ID;
		}

		if ($_REQUEST['process_type'] == "SEQUENCED") {
			$sql = "INSERT INTO instruction_revisions ([instr_ID]
						,[revised_time]
						,[revised_by_operator_ID]
						,[revised_action]
						,[line_ID]
						,[station_ID]
						,[part_order]
						,[instr_order]
						,[part_number]
						,[current_rev_level]
						,[test_type]
						,[title]
						,[instr_text]
						,[prompt]
						,[image_file]
						,[active_flag]
						,[instr_options]
						,[revision_reason])
					  SELECT ID, GETDATE(), ".$_SESSION['op_ID'].", '".$revised_action."',[line_ID]
						,[station_ID]
						,[part_order]
						,[instr_order]
						,[part_number]
						,[current_rev_level]
						,[test_type]
						,[title]
						,[instr_text]
						,[prompt]
						,[image_file]
						,[active_flag]
						,[instr_options]
						,'".fixDB($_REQUEST['reason'])."'
					  FROM instructions
					  WHERE id = ".$instr_ID;
		}
		$db->query($sql);
	}

	function loadWIPParts() {
		global $db;

		$sql = "SELECT * FROM wip_part_setup WHERE line_ID = ".$_REQUEST['line_ID']." ORDER BY id";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	function loadPartGroups() {
		global $db;

		$sql = "SELECT *, cell_ID as line_ID FROM v_part_groups ORDER BY cell_ID, part_group_desc";
		$res = $db->query($sql);

		echo json_encode($res);
	}

	function loadInstructions() {
		global $db;

		$res = new StdClass();

		switch ($_REQUEST['process_type']) {
			case "BATCH":
				$sql = "SELECT i.*, p.part_desc, t.type_desc
							FROM instructions i
								LEFT OUTER JOIN part_setup p ON i.part_ID = p.ID
								LEFT OUTER JOIN test_types t ON i.test_type = t.ID
							WHERE i.cell_ID = ".$_REQUEST['line_ID']."
								AND i.station_order = ".$_REQUEST['station_order']."
							ORDER BY i.station_order, i.instr_order, p.part_number;";
				$res->instructions = $db->query($sql);
				$res->test_types = $db->query("SELECT * FROM test_types ORDER BY type_desc;");
				$res->test_type_options = $db->query("SELECT * FROM test_type_options ORDER BY test_type_id, option_order;");
				$res->parts = $db->query("SELECT * FROM part_setup WHERE cell_ID = ".$_REQUEST['line_ID']." ORDER BY cell_ID, part_desc");
				break;
			case "SEQUENCED":
				$sql = "SELECT DISTINCT i.part_order, i.part_number, i.current_rev_level,
								m.part_desc, COUNT(i.ID) as num_steps, SUM(CASE WHEN i.active_flag = 0 THEN 1 ELSE 0 END) as num_inactive_steps
							FROM instructions i
								LEFT OUTER JOIN (SELECT DISTINCT part_number, part_desc FROM mrp_part_mstr) as m ON i.part_number + i.current_rev_level = m.part_number
							WHERE i.line_ID = ".$_REQUEST['line_ID']."
								AND i.station_ID = ".$_REQUEST['station_ID']."
							GROUP BY i.part_order, i.part_number, i.current_rev_level, m.part_desc
							ORDER BY i.part_order, i.part_number;";
				$res->instructions = $db->query($sql);
				$res->test_types = $db->query("SELECT * FROM test_types ORDER BY type_desc;");
				$res->test_type_options = $db->query("SELECT * FROM test_type_options ORDER BY test_type_id, option_order;");
				//$res->parts = $db->query("SELECT * FROM part_setup WHERE cell_ID = ".$_REQUEST['line_ID']." ORDER BY cell_ID, part_desc");
				break;
		}

		echo json_encode($res);
	}

	function loadInstructionsForPart() {
		global $db;

		$res = new StdClass();

		$sql = "SELECT i.*, t.type_desc
							FROM instructions i
								LEFT OUTER JOIN test_types t ON i.test_type = t.ID
							WHERE i.line_ID = ".$_REQUEST['line_ID']."
								AND i.station_ID = ".$_REQUEST['station_ID']."
								AND i.part_order = ".$_REQUEST['part_order']."
								AND i.part_number = '".$_REQUEST['part_number']."'
								AND i.current_rev_level = '".$_REQUEST['current_rev_level']."'
							ORDER BY i.instr_order;";
		$res->instructions = $db->query($sql);
		$res->test_types = $db->query("SELECT * FROM test_types ORDER BY type_desc;");
		$res->test_type_options = $db->query("SELECT * FROM test_type_options ORDER BY test_type_id, option_order;");
		//$res->parts = $db->query("SELECT * FROM part_setup WHERE cell_ID = ".$_REQUEST['line_ID']." ORDER BY cell_ID, part_desc");

		$sql = "SELECT * FROM dependent_instructions WHERE instr_ID IN (SELECT i.ID
							FROM instructions i
								LEFT OUTER JOIN test_types t ON i.test_type = t.ID
							WHERE i.line_ID = ".$_REQUEST['line_ID']."
								AND i.station_ID = ".$_REQUEST['station_ID']."
								AND i.part_order = ".$_REQUEST['part_order']."
								AND i.part_number = '".$_REQUEST['part_number']."'
								AND i.current_rev_level = '".$_REQUEST['current_rev_level']."')
							ORDER BY dependent_part;";
		$res->dependent = $db->query($sql);

		$sql = "SELECT * FROM instruction_subtractive_parts WHERE instr_ID IN (SELECT i.ID
							FROM instructions i
								LEFT OUTER JOIN test_types t ON i.test_type = t.ID
							WHERE i.line_ID = ".$_REQUEST['line_ID']."
								AND i.station_ID = ".$_REQUEST['station_ID']."
								AND i.part_order = ".$_REQUEST['part_order']."
								AND i.part_number = '".$_REQUEST['part_number']."'
								AND i.current_rev_level = '".$_REQUEST['current_rev_level']."')
							ORDER BY subtractive_part;";
		$res->subtractive = $db->query($sql);

		echo json_encode($res);
	}
?>
