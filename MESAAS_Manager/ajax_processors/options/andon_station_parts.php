<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_lines_stations_parts") {
		loadLinesStationsParts();
	}

	if ($action == "save_xref") {
		$p = $_REQUEST['part_ID'];
		$l = $_REQUEST['line_ID'];
		$s = $_REQUEST['station_ID'];

		$sql = "DELETE FROM ANDON_station_parts WHERE part_ID = ".$p." AND line_ID = ".$l." AND station_ID = ".$s." ";
		$res = $db->query($sql);

		if ($_REQUEST['is_on'] == "1") {
			$sql = "INSERT INTO ANDON_station_parts (part_ID, line_ID, station_ID) VALUES (
						".$p.",
						".$l.",
						".$s.")";
			$res = $db->query($sql);
		}

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	if ($action == "save_part") {
		$r = $_REQUEST;
		if ($r['edit_type'] == "EDIT") {
			$sql = "UPDATE ANDON_parts SET
						 part_number = '".$r['inpPartNumber']."',
						 part_scan = '".$r['inpPartScan']."',
						 part_desc = '".$r['inpPartDesc']."',
						 pick_location = '".$r['inpPickLoc']."',
						 target_location = '".$r['inpDestLoc']."'
					  WHERE id = ".$r['part_ID'];
		}
		if ($r['edit_type'] == "ADD") {
			$sql = "INSERT INTO ANDON_parts (part_number, part_scan, part_desc, pick_location, target_location) VALUES (
							'".$r['inpPartNumber']."',
						 	'".$r['inpPartScan']."',
						 	'".$r['inpPartDesc']."',
						 	'".$r['inpPickLoc']."',
							'".$r['inpDestLoc']."')";
		}
		$db->query($sql);
		loadLinesStationsParts();
	}

	if ($action == "delete_part") {
		$r = $_REQUEST;
		$sql = "DELETE FROM ANDON_parts WHERE id = ".$r['part_ID'];
		$db->query($sql);
		loadLinesStationsParts();
	}

	function loadLinesStationsParts() {
		global $db;

		$res = new StdClass();

		$sql = "SELECT * FROM TEAM_MES.dbo.lines WHERE ID IN (5, 46, 47, 56, 110) OR main_line_ID IN (5, 46, 47, 56) ORDER BY line_code";
		$res->lines = $db->query($sql);

		$sql = "SELECT * FROM TEAM_MES.dbo.line_stations WHERE line_ID IN (SELECT ID FROM TEAM_MES.dbo.lines WHERE ID IN (5, 46, 47, 56) OR main_line_ID IN (5, 46, 47, 56)) ORDER BY line_ID, station_order";
		$res->stations = $db->query($sql);

		$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_parts ORDER BY part_desc";
		$res->parts = $db->query($sql);

		$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_station_parts";
		$res->xref = $db->query($sql);

		echo json_encode($res);
	}
?>