<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	
	if ($action == "get_machine_types") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT [ID]
					,[machine_type_description]
				FROM 
					[machine_types]
				ORDER BY 
					[machine_type_description]";

		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if ($action == "get_downtime_reasons") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$type_ID = $_REQUEST['machine_type_ID'];
		
		$sql = "SELECT r.[ID]
				  ,r.[parent_ID]
				  ,r.[reason_description]				  
				FROM 
					[machine_downtime_reasons] r
				WHERE
					r.ID NOT IN (SELECT [machine_downtime_reason_ID] FROM [machine_types_downtime_reasons] WHERE [machine_type_ID] = $type_ID)
				ORDER BY
					[reason_description]";
		
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);

	}
	
	else if ($action == "get_machine_types_downtime_reasons") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$machine_type_ID = $r['machine_type_ID'];
		
		$sql = "SELECT d.[ID]
					,r.[parent_ID] AS reason_parent_ID
					,r2.[reason_description] AS reason_parent_description
					,r.[reason_description]
					,r.[active] AS reason_active								
				FROM 
					[machine_types_downtime_reasons] d
					JOIN [machine_downtime_reasons] r 
						ON d.machine_downtime_reason_ID = r.ID
					LEFT JOIN [machine_downtime_reasons] r2
						ON r.parent_ID = r2.ID
				WHERE
					[machine_type_ID] = $machine_type_ID
				ORDER BY
					[reason_description]";
		
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	else if($action == "insert_machine_type") {
		$table = 'machine_types';
		
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				$r[$field] = "'".$value."'";				
			}
		}

		$sql = "INSERT INTO $table
				   (".implode(', ', array_keys($r)).")
				VALUES
				   (".implode(', ', array_values($r)).")";
		
		$res = $db->query($sql);
		//die($sql);
		echo json_encode($res);
	}
	
	else if($action == "insert_machine_downtime_reason") {
		$table = 'machine_types_downtime_reasons';
		$arr =  explode(',', $_POST["machine_downtime_reason_ID"]);
		$machine_type_ID = $_POST["machine_type_ID"];
		foreach ($arr as $value) {
			$sql = "INSERT INTO $table
					   (machine_type_ID, machine_downtime_reason_ID)
					VALUES
					   ($machine_type_ID, $value)";
			
			$res = $db->query($sql);
		}
		//die($sql);
		echo json_encode($res);
	}
	
	else if($action == "delete_machine_downtime_reason") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$ID = $r['ID'];
		
		$table = 'machine_types_downtime_reasons';		

		$sql = "DELETE FROM $table WHERE ID = $ID";
		
		$res = $db->query($sql);
		//die($sql);
		echo json_encode($res);
	}
	/*
	else if($action == "update_downtime_reason") {
		$ID = $_POST['reason_ID'];
		$table = 'machine_downtime_reasons';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'reason_ID') {					
					$r[$field] = $value;
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);

		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	*/
	
	function fixDB($s) {
			$s = str_replace("'", "''", $s);
			return $s;
	}
?>