<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_lines_stations_parts") {
		loadLinesStationsParts();
	}

	if ($action == "save_part") {
		$r = $_REQUEST;
		if ($r['edit_type'] == "EDIT") {
			$sql = "UPDATE alias_setup SET
						 component_part = '".$r['inpBroadcastedPartNumber']."',
						 alias_part = '".$r['inpAliasPart']."'
					  WHERE id = ".$r['part_ID'];
		}
		if ($r['edit_type'] == "ADD") {
			$sql = "INSERT INTO alias_setup (component_part, alias_part) VALUES (
							'".$r['inpBroadcastedPartNumber']."',
						 	'".$r['inpAliasPart']."')";
		}
		$db->query($sql);
		loadLinesStationsParts();
	}

	if ($action == "delete_part") {
		$r = $_REQUEST;
		$sql = "DELETE FROM alias_setup WHERE id = ".$r['part_ID'];
		$db->query($sql);
		loadLinesStationsParts();
	}

	function loadLinesStationsParts() {
		global $db;

		$res = new StdClass();

		$sql = "SELECT * FROM lines WHERE line_type = 0 ORDER BY line_code";
		$res->lines = $db->query($sql);

		$sql = "SELECT * FROM line_stations WHERE line_ID IN (SELECT ID FROM lines WHERE line_type = 0) ORDER BY line_ID, station_order";
		$res->stations = $db->query($sql);

		$sql = "SELECT * FROM alias_setup ORDER BY alias_part, component_part";
		$res->parts = $db->query($sql);

		echo json_encode($res);
	}
?>