<?php

	require_once("../init.php");

	//require('../upload.php');

	$action = $_REQUEST['action'];

	if ($action != "upload_instruction_image") {
		header("Content-Type: application/json");
	} else {
		header("Content-Type: text/html");
	}


	if ($action == 'get_machines') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					machine_name,
					machine_number
				FROM
					machines;";

		$res = $db->query($sql);

		echo json_encode($res);
	}


	if ($action == 'get_machine') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT TOP 1
					m.ID,
					m.machine_number,
					m.machine_name,
					m.machine_type_ID,
					mtypes.machine_type_description AS machine_type,
					m.machine_group_ID,
					mg.name AS machine_group,
					m.current_tool_ID,
					m.current_PLC_tool_ID,
					m.alt_tool_ID_set_time,
					m.current_std_pack,
					m.queue_type,
					m.per_part_printer_ID,
					m.rack_printer_ID,
					m.PLC_ID,
					m.PLC_file_address,
					m.station_IP_address,
					m.img_src,
					plct.ID AS primary_plc_tag_ID,
					plct.name AS primary_plc_tag_name,
					plcs.ip_address as primary_plc_ip,
					m.op_order,
					m.over_cycle_grace_period
				FROM
					machines m
					LEFT JOIN machine_types mtypes ON mtypes.ID = m.machine_type_ID
					LEFT JOIN machine_groups mg ON mg.ID = m.machine_group_ID
					LEFT JOIN NYSUS_PLC_POLLER.dbo.nysus_plc_tags plct ON plct.description = m.machine_number AND plct.is_primary = 1
					LEFT JOIN NYSUS_PLC_POLLER.dbo.nysus_plcs plcs ON plcs.ID = plct.plc_ID
				WHERE
					m.ID = ".$_REQUEST['machine_ID'].";";

		$res = $db->query($sql);

		if ($res) {
			$sql = "SELECT
						plct.ID,
						plct.name,
						plct.is_primary
					FROM
						NYSUS_PLC_POLLER.dbo.nysus_plc_tags plct
						LEFT JOIN machines m ON m.machine_number = plct.description
					WHERE
						m.ID = ".$_REQUEST['machine_ID'].";";
			$res[0]['plc_tags'] = $db->query($sql);
		}

		echo json_encode($res);
	}

	if ($action == 'insert_machine') {
		global $db;
		$res = new StdClass();
		$data = $_REQUEST['data'];


		$sql = "INSERT INTO
					machines (
						machine_number,
						machine_name,
						machine_type_ID,
						machine_group_ID,
						up,
						station_IP_address,
						image_ID
					) VALUES (
						'".$data['machine_number']."',
						'".$data['machine_name']."',
						".$data['machine_type_ID'].",
						".$data['machine_group_ID'].",
						0,
						'".$data['station_IP_address']."',
						".$image_ID."
					);";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'update_machine') {
		global $db;
		$res = new StdClass();
		$data = $_REQUEST['data'];

		$sql = "UPDATE
					machines
				SET
					machine_number = '".$data['machine_number']."'
					,machine_name = '".$data['machine_name']."'
					,machine_type_ID = ".$data['machine_type_ID']."
					,machine_group_ID = ".$data['machine_group_ID']."
					,station_IP_address = '".$data['station_IP_address']."'
					,op_order = ".$data['op_order']."
				WHERE
					ID = ".$_REQUEST['machine_ID'].";";

					//,over_cycle_grace_period = ".$data['over_cycle_grace_period']."
		//die($sql);
		$res = $db->query($sql);

		/*
 		if ($res === null && $data['primary_plc_tag_ID']) {
			$sql = "UPDATE
						plct
					SET
						plct.is_primary = (CASE WHEN plct.ID = ".$data['primary_plc_tag_ID']." THEN 1 ELSE 0 END)
					FROM
						NYSUS_PLC_POLLER.dbo.nysus_plc_tags plct
						LEFT JOIN machines m ON m.machine_number = plct.description
					WHERE
						m.ID = ".$_REQUEST['machine_ID'].";";
			//die($sql);
			$res = $db->query($sql);
		}
		*/

		echo json_encode($res);
	}

	if ($action == 'get_machine_types') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					machine_type_description
				FROM
					machine_types
				ORDER BY
					machine_type_description ASC;";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_groups') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					description
				FROM
					machine_groups
				ORDER BY
					ID ASC;";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_tools') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					tool_description,
					short_description
				FROM
					tools
				ORDER BY
					tool_description ASC;";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_tools') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					mt.ID AS machine_tool_ID,
					mt.tool_ID,
					t.tool_description,
					t.short_description,
					mt.target_cycle_time
				FROM
					machine_tools mt
					LEFT JOIN tools t ON t.ID = mt.tool_ID
				WHERE
					mt.machine_ID = ".$_REQUEST['machine_ID'].";";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_printers') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT mp.*, ps.printer_name, ps.printer_IP
					FROM machine_printers mp
						JOIN printer_setup ps ON mp.printer_ID = ps.ID
					WHERE mp.machine_ID = ".$_REQUEST['machine_ID'].";";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_tool') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT TOP 1
					mt.ID AS machine_tool_ID,
					mt.tool_ID,
					t.tool_description,
					t.short_description,
					mt.target_cycle_time
				FROM
					machine_tools mt
					LEFT JOIN tools t ON t.ID = mt.tool_ID
				WHERE
					mt.ID = ".$_REQUEST['machine_tool_ID'].";";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'insert_machine_tool') {
		global $db;
		$res = new StdClass();

		$sql = "INSERT INTO
					machine_tools (
						tool_ID,
						machine_ID,
						target_cycle_time
					)
				--OUTPUT inserted.ID
				VALUES (
					".$_REQUEST['machine_tool_data']['tool_ID'].",
					".$_REQUEST['machine_tool_data']['machine_ID'].",
					".$_REQUEST['machine_tool_data']['target_cycle_time']."
				);";
		//die($sql);
		$res = $db->query($sql);
		//$machine_tool_ID = $res[0]['ID'];

//		if ($res) {
//			$sql = "INSERT INTO
//						tool_target_cycle_time (
//							machine_tool_ID,
//							target_cycle_time
//						)
//					VALUES (
//						".$machine_tool_ID.",
//						".$_REQUEST['machine_tool_data']['target_cycle_time']."
//					);";
//			//die($sql);
//			$res = $db->query($sql);
//		}

		echo json_encode($res);
	}

	if ($action == 'update_machine_tool') {
		global $db;
		$res = new StdClass();

		if ($_REQUEST['machine_tool_data']['target_cycle_time']) {
			$target_cycle_time = $_REQUEST['machine_tool_data']['target_cycle_time'];
		} else {
			$target_cycle_time = 'NULL';
		}

		$sql = "UPDATE
					machine_tools
				SET
					tool_ID = ".$_REQUEST['machine_tool_data']['tool_ID'].",
					target_cycle_time = ".$target_cycle_time."
				WHERE
					ID = ".$_REQUEST['machine_tool_data']['machine_tool_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		if ($res === false) {
			die($res);
		}

//		$sql = "UPDATE
//					tool_target_cycle_time
//				SET
//					target_cycle_time = ".$target_cycle_time."
//				WHERE
//					machine_tool_ID = ".$_REQUEST['machine_tool_data']['machine_tool_ID'].";";
//		//die($sql);
//		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'delete_machine_tool') {
		global $db;
		$res = new StdClass();

//		$sql = "DELETE
//				FROM
//					tool_target_cycle_time
//				WHERE
//					machine_tool_ID = ".$_REQUEST['machine_tool_ID'].";";
//		//die($sql);
//		$res = $db->query($sql);
//
//		if ($res === false) {
//			die($res);
//		}

		$sql = "DELETE
				FROM
					machine_tools
				WHERE
					ID = ".$_REQUEST['machine_tool_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}


	if ($action == 'get_machine_plc_tags') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					plct.ID,
					plct.name,
					plct.is_primary
				FROM
					NYSUS_PLC_POLLER.dbo.nysus_plc_tags plct
					LEFT JOIN machines m ON m.machine_number = plct.description
				WHERE
					m.ID = ".$_REQUEST['machine_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'set_primary_machine_plc_tag') {
		global $db;
		$res = new StdClass();

		$sql = "UPDATE
					plct
				SET
					plct.is_primary = (CASE WHEN plct.ID = ".$_REQUEST['primary_tag_ID']." THEN 1 ELSE 0 END)
				FROM
					NYSUS_PLC_POLLER.dbo.nysus_plc_tags plct
					LEFT JOIN machines m ON m.machine_number = plct.description
				WHERE
					m.ID = ".$_REQUEST['machine_ID'].";";
		die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_ojt_jobs') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					mj.ID,
					j.ID,
					j.job_name,
					COUNT(s.operator_ID) AS qualified,
					j.min_operators_qualified
				FROM
					machine_ojt_jobs mj
					LEFT JOIN MES_COMMON.dbo.ojt_jobs j ON j.ID = mj.ojt_job_ID
					LEFT JOIN MES_COMMON.dbo.ojt_job_signoffs s ON s.ojt_job_ID = j.ID
				WHERE
					mj.machine_ID = ".$_REQUEST['machine_ID']."
				GROUP BY
					mj.ID,
					j.ID,
					j.job_name,
					j.min_operators_qualified,
					mj.machine_ID;";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_ojt_jobs') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					department_name
				FROM
					MES_COMMON.dbo.departments
				ORDER BY
					department_name ASC;";

		$res->departments = $db->query($sql);

		$sql = "SELECT
					ID,
					job_name
				FROM
					MES_COMMON.dbo.ojt_jobs
				WHERE
					department_ID IS NULL
					AND area_ID IS NULL
					AND sub_area_ID IS NULL
				GROUP BY
					ID,
					job_name,
					department_ID,
					area_ID,
					sub_area_ID
				ORDER BY
					job_name ASC;";

		$res->jobs = $db->query($sql);

		foreach($res->departments as $i => &$department) {
			$sql = "SELECT
						ID,
						job_name
					FROM
						MES_COMMON.dbo.ojt_jobs
					WHERE
						department_ID = ".$department['ID']."
						AND area_ID IS NULL
						AND sub_area_ID IS NULL
					GROUP BY
						ID,
						job_name,
						department_ID,
						area_ID,
						sub_area_ID
					ORDER BY
						job_name ASC;";

			$department['jobs'] = $db->query($sql);

			$sql = "SELECT
						ID,
						area_desc
					FROM
						MES_COMMON.dbo.areas
					WHERE
						department_ID = ".$department['ID']."
					ORDER BY
						area_desc ASC;";

			$department['areas'] = $db->query($sql);

			foreach($department['areas'] as $j => &$area) {
				$sql = "SELECT
							ID,
							job_name
						FROM
							MES_COMMON.dbo.ojt_jobs
						WHERE
							area_ID = ".$area['ID']."
						GROUP BY
							ID,
							job_name,
							area_ID
						ORDER BY
							job_name ASC;";

				$area['jobs'] = $db->query($sql);
			}
		}

		echo json_encode($res);
	}

	if ($action == 'insert_machine_ojt_job') {
		global $db;
		$res = new StdClass();

		$sql = "INSERT INTO
					machine_ojt_jobs (
						machine_ID,
						ojt_job_ID
					) VALUES (
						".$_REQUEST['machine_ID'].",
						".$_REQUEST['ojt_job_ID']."
					);";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'delete_machine_ojt_job') {
		global $db;
		$res = new StdClass();

		$sql = "DELETE FROM
					machine_ojt_jobs
				WHERE
					ID = ".$_REQUEST['machine_ojt_job_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}


	//INSTRUCTIONS
	if ($action == "get_machine_instructions") {
		$res = new StdClass();

		$sql = "SELECT *
				  FROM machine_instructions
				  WHERE machine_ID = ".$_REQUEST['machine_ID']."
				  ORDER BY instr_order;";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'get_machine_instruction') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT TOP 1 *
				  FROM machine_instructions mi
				  WHERE mi.ID = ".$_REQUEST['machine_instruction_ID'].";";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'insert_machine_instruction') {
		global $db;
		$res = new StdClass();

		$sql = "INSERT INTO
					machine_instructions (
						machine_ID,
						instr_order,
						title,
						instr_text,
						instr_image
					) VALUES (
						".$_REQUEST['machine_instruction_data']['machine_ID'].",
						".$_REQUEST['machine_instruction_data']['instr_order'].",
						'".fixDB($_REQUEST['machine_instruction_data']['title'])."',
						'".fixDB($_REQUEST['machine_instruction_data']['instr_text'])."',
						'".str_replace("C:\\fakepath\\", "", fixDB($_REQUEST['machine_instruction_data']['instr_image']))."'
					);";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'update_machine_instruction') {
		global $db;
		$res = new StdClass();

		$sql = "UPDATE machine_instructions
					SET instr_order = ".$_REQUEST['machine_instruction_data']['instr_order'].",
						title = '".fixDB($_REQUEST['machine_instruction_data']['title'])."',
						instr_text = '".fixDB($_REQUEST['machine_instruction_data']['instr_text'])."'
					WHERE id = ".$_REQUEST['machine_instruction_data']['machine_instruction_ID'];
		//die($sql);
		$res = $db->query($sql);

		if ($_REQUEST['machine_instruction_data']['instr_image'] != "") {
			$sql = "UPDATE machine_instructions
						SET instr_image = '".str_replace("C:\\fakepath\\", "", fixDB($_REQUEST['machine_instruction_data']['instr_image']))."'
						WHERE id = ".$_REQUEST['machine_instruction_data']['machine_instruction_ID'];
			//die($sql);
			$res = $db->query($sql);
		}

		echo json_encode($res);
	}

	if ($action == 'delete_machine_instruction') {
		global $db;
		$res = new StdClass();

		$sql = "DELETE FROM
					machine_instructions
				WHERE
					ID = ".$_REQUEST['machine_instruction_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == "upload_instruction_image") {
		$targetDir = "D:\GitRepos\machine_mes2\public\images\work_instructions\MACHINE_".$_POST['image_upload_machine_ID'];
		// Create target dir
		if (!file_exists($targetDir)) {
			@mkdir($targetDir);
		}

		// Get a file name
		if (isset($_REQUEST["instr_image"])) {
			$fileName = $_REQUEST["instr_image"];
		} elseif (!empty($_FILES)) {
			$fileName = $_FILES["instr_image"]["name"];
		} else {
			$fileName = uniqid("file_");
		}

		$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

		// Chunking might be enabled
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

		// Open temp file
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}

		if (!empty($_FILES)) {
			if ($_FILES["instr_image"]["error"] || !is_uploaded_file($_FILES["instr_image"]["tmp_name"])) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}

			// Read binary input stream and append it to temp file
			if (!$in = @fopen($_FILES["instr_image"]["tmp_name"], "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		} else {
			if (!$in = @fopen("php://input", "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		}

		while ($buff = fread($in, 4096)) {
			fwrite($out, $buff);
		}

		@fclose($out);
		@fclose($in);

		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);
		}
		echo "<script type=\"text/javascript\">top.afterImageUpload('".$_FILES["instr_image"]["name"]."');</script>";
	}

	if ($action == "move_machine_instruction") {
		$sql = "SET NOCOUNT ON;
					DECLARE @cur_order int, @swap_instr_ID int, @swap_instr_order int

					SELECT @cur_order = instr_order
					FROM machine_instructions
					WHERE ID = ".$_REQUEST['machine_instruction_ID']."

					IF '".$_REQUEST['direction']."' = 'DOWN'
						SELECT TOP 1 @swap_instr_ID = ID, @swap_instr_order = instr_order
						FROM machine_instructions
						WHERE machine_ID = ".$_REQUEST['machine_ID']."
							AND instr_order > @cur_order
						ORDER BY instr_order ASC

					IF '".$_REQUEST['direction']."' = 'UP'
						SELECT TOP 1 @swap_instr_ID = ID, @swap_instr_order = instr_order
						FROM machine_instructions
						WHERE machine_ID = ".$_REQUEST['machine_ID']."
							AND instr_order < @cur_order
						ORDER BY instr_order DESC

					UPDATE machine_instructions
					SET instr_order = @swap_instr_order
					WHERE ID = ".$_REQUEST['machine_instruction_ID'].";

					UPDATE machine_instructions
					SET instr_order = @cur_order
					WHERE ID = @swap_instr_ID;";
		$db->query($sql);

		$res = new StdClass();
		//$res->query = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

?>