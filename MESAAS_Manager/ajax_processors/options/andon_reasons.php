<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_categories") {
		loadCategories();
	}

	if ($action == "load_reasons") {
		loadReasons();
	}

	if ($action == "save_reason") {
		$r = $_REQUEST;
		if ($r['edit_type'] == "EDIT") {
			$sql = "UPDATE ANDON_reasons SET
						 reason_desc = '".$r['reason_desc']."'
					  WHERE id = ".$r['part_ID'];
		}
		if ($r['edit_type'] == "ADD") {
			$sql = "INSERT INTO ANDON_reasons (category_ID, parent_ID, reason_desc) VALUES (
							'".$r['category_ID']."',
						 	'".$r['parent_ID']."',
						 	'".$r['reason_desc']."')";
		}
		$db->query($sql);
		loadReasons();
	}

	if ($action == "delete_reason") {
		$r = $_REQUEST;
		$sql = "DELETE FROM ANDON_reasons WHERE id = ".$r['reason_ID'];
		$db->query($sql);
		loadReasons();
	}

	function loadCategories() {
		global $db;

		$res = new StdClass();

		$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_categories ORDER BY category_name";
		$res->categories = $db->query($sql);

		echo json_encode($res);
	}

	function loadReasons() {
		global $db;

		$r = $_REQUEST;
		$res = new StdClass();

		$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_reasons WHERE category_ID = ".$r['category_ID']." AND parent_ID = 0 ORDER BY reason_desc";
		$res->reasons = $db->query($sql);

		for ($i = 0; $i < count($res->reasons); $i++) {
			$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_reasons WHERE category_ID = ".$r['category_ID']." AND parent_ID = ".$res->reasons[$i]['ID']." ORDER BY reason_desc";
			$res->reasons[$i]['reasons'] = $db->query($sql);
		}

		echo json_encode($res);
	}
?>