<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_operators ($request, $db) {
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					email
				FROM
					MES_COMMON.dbo.operators
				WHERE
					deactive_date IS NULL
					AND email IS NOT NULL
					AND email <> ''
				ORDER BY
					name ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	function get_machines($request, $db) {
		$query = "
			SELECT
				ID AS id
				, machine_number + ' - ' + machine_name AS text
			FROM
				machines
			ORDER BY
				ID";

		echo json_encode($db->query($query));
	}

	function get_tools($request, $db) {
		$query = "
			SELECT
				 m.ID AS machine_ID
				,t.ID as tool_ID
				,t.short_description
				,t.tool_description
				,COUNT(mtcl.ID) AS configured_ctrl_limits
			FROM machine_tools mt
			JOIN machines m ON m.ID = mt.machine_ID
			LEFT JOIN tools t ON t.ID = mt.tool_ID
			LEFT JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = m.ID AND mtcl.tool_ID = t.ID
			LEFT JOIN machine_monitor_control_limit_severity mmcls ON mmcls.machine_tool_ctrl_limit_ID = mtcl.ID
			LEFT JOIN machine_monitor_severity mms ON mms.ID = mmcls.machine_monitor_severity_ID
			WHERE mmcls.machine_monitor_severity_ID >= 3 AND m.ID = ".$request['machine_ID']."
			GROUP BY m.ID, t.ID, t.tool_description, t.short_description
			ORDER BY m.ID, t.ID";

		echo json_encode($db->query($query));
	}
	
	function drilldown($request, $db) {
		$res = new StdClass();
		
		$sql = "
			SELECT
				 mt.machine_ID
				,mt.tool_ID
				,dps.ID AS data_point_ID
				,mms.severity_description
				,dps.data_point_desc
				,CASE WHEN mtcl.upper_ctrl_limit IS NULL THEN dps.default_ucl ELSE mtcl.upper_ctrl_limit END AS ucl
				,CASE WHEN mtcl.lower_ctrl_limit IS NULL THEN dps.default_lcl ELSE mtcl.lower_ctrl_limit END AS lcl
				,COUNT(mmel.ID) AS recipients
			FROM machine_tools mt
			JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mt.machine_ID AND mtcl.tool_ID = mt.tool_ID
			LEFT JOIN data_point_setup dps ON dps.ID = mtcl.data_point_ID
			LEFT JOIN machine_monitor_control_limit_severity mmcls ON mmcls.machine_tool_ctrl_limit_ID = mtcl.ID
			LEFT JOIN machine_monitor_severity mms ON mms.ID = mmcls.machine_monitor_severity_ID
			LEFT JOIN machine_monitor_email_list mmel ON mmel.machine_tool_ctrl_limit_ID = mtcl.ID AND mmel.active = 1
			WHERE mmcls.machine_monitor_severity_ID >= 3 AND mt.machine_ID = ".$request["machine"]." AND mt.tool_ID = ".$request["tool"]."
			GROUP BY mt.machine_ID, mt.tool_ID,dps.ID, dps.data_point_desc, mms.severity_description, CASE WHEN mtcl.upper_ctrl_limit IS NULL THEN dps.default_ucl ELSE mtcl.upper_ctrl_limit END, CASE WHEN mtcl.lower_ctrl_limit IS NULL THEN dps.default_lcl ELSE mtcl.lower_ctrl_limit END
			ORDER BY mt.machine_ID, mt.tool_ID, dps.ID;";

		echo json_encode($db->query($sql));
	}
	
	function get_email_alerts ($request, $db) {
		$res = new StdClass();

		$sql = "
			SELECT
				 mmel.ID AS mmel_ID
				,mtcl.ID AS mtcl_ID
				,o.ID, o.badge_ID, o.[name], o.email
				,mmel.active AS mmel_active
				,mtcl.deactivated AS mtcl_deactivated
			FROM machine_monitor_email_list mmel
			LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mmel.operator_ID
			LEFT JOIN machine_tool_ctrl_limits mtcl on mtcl.ID = mmel.machine_tool_ctrl_limit_ID
			LEFT JOIN data_point_setup dps ON dps.ID = mtcl.data_point_ID
			WHERE mmel.active = 1
			AND mtcl.machine_ID = ".$request['machine_ID']."
			AND mtcl.tool_ID = ".$request['tool_ID']."
			AND dps.ID = ".$request['data_point_ID'].";";

			
		$res = $db->query($sql);
		echo json_encode($res);
	}

	function update_alert ($request, $db) {
		global $db;
		$res = new StdClass();

// action:"update_alert"
// machine_id:"1"
// tool_id:"12"
// data_point_id:"2"
// operators:(2) ["69", "1005"]
		
		$operator_str = implode(',',$request['operators']);
		
		$machine_ID = $request['machine_id'];
		$tool_ID = $request['tool_id'];
		$data_point_ID = $request['data_point_id'];

		// update the alert
		$sql = "EXEC dbo.sp_MACHINE_MES_UpdateAddMachineMonitorEmailUsers
					@machine_ID = $machine_ID, 
					@tool_ID = $tool_ID, 
					@data_point_ID = $data_point_ID, 
					@op_list = N'$operator_str'";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}

?>