<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	
	header("Content-Type: application/json");


	if ($action == 'load_group_list') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT ID,part_group_description,is_active
				FROM NYSUS_REPETITIVE_MES.dbo.part_group_hrt";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'add_group') {
		global $db;
		$res = new StdClass();

		$sql = "INSERT INTO 
					NYSUS_REPETITIVE_MES.dbo.part_group_hrt 
				(part_group_description, is_active) 
				VALUES
				('".$_REQUEST['group_name']."',".$_REQUEST['active'].")";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'update_group') {
		global $db;
		$res = new StdClass();
		$set_active = 1;
		if($_REQUEST['active'] == 1){
			$set_active = 0;
		}
		$sql = "UPDATE
					NYSUS_REPETITIVE_MES.dbo.part_group_hrt
				SET
					is_active = $set_active 
				WHERE ID = ". $_REQUEST['group_id'];

		$res = $db->query($sql);

		echo json_encode($res);
	}	
?>