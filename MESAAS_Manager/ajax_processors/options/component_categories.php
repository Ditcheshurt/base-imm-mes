<?php

	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents("../../config/app_config.json"), true);
	require("../".$GLOBALS['APP_CONFIG']['db_class']);

	include_once("..\init_db.php");

	$action = $_REQUEST['action'];

	if ($action == "load_data") {
		loadData();
	}

	if ($action == "add_edit") {
		$r = $_REQUEST;
		switch ($r['edit_mode']) {
			case "ADD_CATEGORY":
				$sql = "INSERT INTO component_categories (line_ID, component_category_name, min_qty, max_qty) VALUES (".$r['line_ID'].", '".$r['new_value']."', 1, 1)";
				break;
			case "EDIT_CATEGORY":
				$sql = "UPDATE component_categories SET component_category_name = '".$r['new_value']."' WHERE ID = ".$r['ID'];
				break;
			case "EDIT_MIN":
				$sql = "UPDATE component_categories SET min_qty = '".$r['new_value']."' WHERE ID = ".$r['ID'];
				break;
			case "EDIT_MAX":
				$sql = "UPDATE component_categories SET max_qty = '".$r['new_value']."' WHERE ID = ".$r['ID'];
				break;
			case "ADD_PART":
				$sql = "INSERT INTO component_category_parts (component_category_ID, part_number) VALUES (".$r['ID'].", '".$r['new_value']."')";
				break;
			case "EDIT_PART":
				$sql = "UPDATE component_category_parts SET part_number = '".$r['new_value']."' WHERE ID = ".$r['ID'];
				break;
		}
		$db->query($sql);
		loadData();
	}

	if ($action == "delete") {
		$r = $_REQUEST;
		switch ($r['edit_mode']) {
			case "DELETE_CATEGORY":
				$sql = "DELETE FROM component_categories WHERE ID = ".$r['ID'];
				break;
			case "DELETE_PART":
				$sql = "DELETE FROM component_category_parts WHERE ID = ".$r['ID'];
				break;
		}
		$db->query($sql);
		loadData();
	}

	function loadData() {
		global $db;

		$res = new StdClass();
		$sql = "SELECT * FROM component_categories WHERE line_ID = ".$_REQUEST['line_ID']." ORDER BY component_category_name";
		$res->component_categories = $db->query($sql);

		$sql = "SELECT * FROM component_category_parts WHERE component_category_ID IN (SELECT ID FROM component_categories WHERE line_ID = ".$_REQUEST['line_ID'].") ORDER BY component_category_ID";
		$res->component_category_parts = $db->query($sql);

		echo json_encode($res);
	}
?>