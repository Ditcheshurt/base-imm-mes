<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_OJT_jobs") {
		$sql = "SELECT *
				  FROM MES_COMMON.dbo.OJT_jobs
				  WHERE active = 1
				  	  AND ID NOT IN (SELECT OJT_job_ID FROM station_OJT_requirements WHERE station_ID = ".$_REQUEST['station_ID'].")
				  ORDER BY job_name";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "load_quality_alerts") {
		$sql = "SELECT *
				  FROM MES_COMMON.dbo.qa_items
				  WHERE ID NOT IN (SELECT qa_item_ID FROM station_QA_requirements WHERE station_ID = ".$_REQUEST['station_ID'].")
				  ORDER BY qa_title";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "add_OJT_requirements") {
		global $db;
		$sql = "INSERT INTO station_OJT_requirements
           			(station_ID
           			,ojt_job_ID
           			,signoff_expiration_days)
     			VALUES
           			(".$_REQUEST['station_ID']."
           			,".$_REQUEST['ojt_job_ID']."
           			,null)";
		$db->query($sql);
		loadOJTRequirements();
	}

	if ($action == "delete_OJT_requirements") {
		global $db;
		$sql = "DELETE FROM station_OJT_requirements
					WHERE station_ID = ".$_REQUEST['station_ID']."
					AND ojt_job_ID = ".$_REQUEST['ojt_job_ID'];
		$db->query($sql);
		loadOJTRequirements();
	}

	if ($action == "add_QA_requirements") {
		global $db;
		$sql = "INSERT INTO station_QA_requirements
           			(station_ID
           			,qa_item_ID
           			,signoff_expiration_days
           			,allow_MES_signoff)
     			VALUES
           			(".$_REQUEST['station_ID']."
           			,".$_REQUEST['qa_item_ID']."
           			,null
           			,'".$_REQUEST['allow_MES_signoff']."')";
		$db->query($sql);
		loadQualityAlertRequirements();
	}

	if ($action == "delete_QA_requirements") {
		global $db;
		$sql = "DELETE FROM station_QA_requirements
					WHERE station_ID = ".$_REQUEST['station_ID']."
					AND qa_item_ID = ".$_REQUEST['qa_item_ID'];
		$db->query($sql);
		loadQualityAlertRequirements();
	}

	if ($action == "load_OJT_requirements") {
		loadOJTRequirements();
	}

	if ($action == "load_quality_alert_requirements") {
		loadQualityAlertRequirements();
	}


	function loadOJTRequirements() {
		global $db;
		$sql = "SELECT j.ID, j.job_name, COUNT(s.ID) as num_signoffs
				  FROM station_OJT_requirements r
				  	  JOIN MES_COMMON.dbo.OJT_jobs j ON r.ojt_job_ID = j.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.OJT_job_signoffs s ON j.ID = s.ojt_job_ID
				  WHERE r.station_ID = ".$_REQUEST['station_ID']."
				  GROUP BY j.ID, j.job_name
				  ORDER BY j.job_name";
		$res = $db->query($sql);
		echo json_encode($res);
	}

	function loadQualityAlertRequirements() {
		global $db;

		$sql = "SELECT q.ID, q.qa_title, r.allow_MES_signoff, COUNT(s.ID) as num_signoffs
				  FROM station_QA_requirements r
				  	  JOIN MES_COMMON.dbo.qa_items q ON r.qa_item_ID = q.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.qa_signoffs s ON q.ID = s.qa_item_ID
				  WHERE r.station_ID = ".$_REQUEST['station_ID']."
				  GROUP BY q.ID, q.qa_title, r.allow_MES_signoff
				  ORDER BY q.qa_title";
		$res = $db->query($sql);
		echo json_encode($res);
	}

?>