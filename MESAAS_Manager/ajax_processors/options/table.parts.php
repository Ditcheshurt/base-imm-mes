<?php

// DataTables PHP library and database connection
include( "../../lib/DataTables/DataTables.php" );
$app_config = json_decode(file_get_contents("../../config/app_config.json"), true);

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

// The following statement can be removed after the first run (i.e. the database
// table has been created). It is a good idea to do this to help improve
// performance.
//$db->sql( "IF object_id('parts', 'U') is null
//	CREATE TABLE parts (
//		ID int not null identity,
//		part_description varchar(255),
//		part_number varchar(255),
//		mrp_part_num varchar(255),
//		part_image varchar(255),
//		std_pack_qty numeric(9,2),
//		part_wgt numeric(9,2),
//		box_wgt numeric(9,2),
//		mrp_co_code varchar(255),
//		fg_part_id numeric(9,2),
//		active varchar(255),
//		PRIMARY KEY( ID )
//	);" );

// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'NYSUS_REPETITIVE_MES.dbo.parts', 'ID' )
    ->fields(
        Field::inst( 'part_desc' )
            ->validator( 'Validate::notEmpty' ),
        Field::inst( 'part_number' )
            ->validator( 'Validate::notEmpty' ),
        Field::inst( 'mrp_part_number' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'part_image' )
            ->upload(
                Upload::inst( function ( $file, $id ) {
	                global $app_config;
                    $rand = mt_rand();
                    move_uploaded_file( $file['tmp_name'], $app_config["part_image_upload_path"].$rand.'~~'.$file['name'] );
                    return $app_config["part_image_upload_url"].$rand.'~~'.$file['name'];
                } )
                    ->allowedExtensions( array( 'png', 'jpg', 'gif' ), "Please upload an image file" ))
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'standard_pack_qty' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'part_weight' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'box_weight' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'mrp_company_code' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'fg_part_id' )
            ->setFormatter( 'Format::nullEmpty' ),
        Field::inst( 'active' )
    )
    ->process( $_POST )
    ->json();

//Field::inst( 'part_image' )
//    ->setFormatter( 'Format::nullEmpty' )
//    ->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'\mesaas-manager-metaldyne-litchfield\public\images\parts\__ID__--__NAME__' )
//        ->allowedExtensions( array( 'png', 'jpg', 'gif' ), "Please upload an image file" )
//        ->db( 'NYSUS_REPETITIVE_MES.dbo.parts', 'id', array(
//            'part_image' => Upload::DB_FILE_NAME
//        ) )
//    ),