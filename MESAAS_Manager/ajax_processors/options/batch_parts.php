<?php
try
{

	require_once("../init.php");
	
	$action = $_REQUEST["action"];
	
	global $db;
	$res = new StdClass();

	if ($action == "load_batch_parts")
	{
		$cell_ID = $_REQUEST["cell_ID"];

		$sql = "SELECT
					t.[ID]
	      			,t.[cell_ID]
					,[end_item_part]
					,[part_number]
					,[shipping_part_number]
					,[part_rev_level]
					,[part_desc]
					,[blind_cats_1]
					,[blind_cats_2]
					,[blind_cats_3]
					,[blind_cats_4]
					,[blind_cats_5]
					,[blind_cats_6]
				FROM part_setup t				
				WHERE
					cell_ID = $cell_ID
				ORDER BY
					part_desc";

		$res = $db->query($sql);
		echo json_encode($res);
	} 

	else if($action == "select")
	{
		$part_ID = $_REQUEST["part_ID"];
		$sql = "SELECT
					t.[ID]
	      			,t.[cell_ID]
					,[end_item_part]
					,[part_number]
					,[shipping_part_number]
					,[part_rev_level]
					,[part_desc]
					,[blind_cats_1]
					,[blind_cats_2]
					,[blind_cats_3]
					,[blind_cats_4]
					,[blind_cats_5]
					,[blind_cats_6]
				FROM part_setup t				
				WHERE
					t.ID = $part_ID
				ORDER BY
					part_desc";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	else if($action == "insert")
	{
		$cell_ID = $_POST["cell_ID"];

		// get next id, add 10000 to cell_ID starter if more than 99 parts for that cell
		$sql = "SELECT ISNULL(
					(SELECT TOP 1 (ID+1)
						FROM part_setup 
						WHERE ID > $cell_ID
							AND ID < $cell_ID + 100
						ORDER BY ID desc)
				, (SELECT ISNULL((SELECT TOP 1 ID
						FROM part_setup
						WHERE ID > 10000+$cell_ID			
						ORDER BY ID desc), 10000+$cell_ID))
				) AS nextID";

		$res = $db->query($sql);	
//echo json_encode($res);
		$r = array();
		$r['ID'] = "'".$res[0]['nextID']."'";

		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				$r[$field] = "'".$value."'";				
			}
		}

		$sql = "INSERT INTO part_setup
				   (".implode(', ', array_keys($r)).")
				VALUES
				   (".implode(', ', array_values($r)).")";
		
		$db->query($sql);
		
		//Get last inserted record (to return to jTable)
		$sql = "SELECT * FROM part_setup WHERE ID = @@IDENTITY";
		
		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $res;
		echo json_encode($jTableResult);
	}	
	//Updating a record (updateAction)
	else if($action == "update")
	{
		$ID = $_POST['part_ID'];
		$table = 'part_setup';

		$r = array();
		foreach ($_POST as $field => $value) {
			if ($field != 'action' && $value) {
				if ($field != 'part_ID') {					
					$r[$field] = $value;				
				}
			}
		}

		foreach ($r as $key => $value) {
	        $value = fixDB($value);
	        $value = "'$value'";
	        $updates[] = "$key = $value";
      	}
      	$implodeArray = implode(', ', $updates);

		$sql = sprintf("UPDATE %s SET %s WHERE id=%s", $table, $implodeArray, $ID);

		$db->query($sql);		
		
		$sql = "SELECT ps.[ID]
	      			,[cell_ID]
					,[part_number]
					,[shipping_part_number]
					,[part_rev_level]
					,[part_desc]
					,[blind_cats_1]
					,[blind_cats_2]
					,[blind_cats_3]
					,[blind_cats_4]
					,[blind_cats_5]
					,[blind_cats_6]
				FROM part_setup ps
				JOIN cells c on c.ID = ps.cell_ID
				WHERE ps.ID = $ID";
		//die($sql);
		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['Record'] = $res[0];
		echo json_encode($jTableResult);
	}

	//Deleting a record (deleteAction)
	else if($action == "delete")
	{
		$ID = $_POST["part_ID"];

		//Delete from database
		$sql = "DELETE FROM part_setup WHERE ID = $ID";

		$res = $db->query($sql);
		
		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		echo json_encode($jTableResult);
	}

}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}

?>