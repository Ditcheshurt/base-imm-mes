<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	
	
	if ($action === 'get_email_alerts') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					a.ID,
					a.description,
					a.min_threshold,
					a.max_threshold,
					a.alert_type_ID,
					at.description AS type_description,
					at.name AS type_name,
					a.active
				FROM
					[NYSUS_REPETITIVE_MES].dbo.alerts a
					LEFT JOIN [NYSUS_REPETITIVE_MES].dbo.alert_types at ON at.ID = a.alert_type_ID
				WHERE
					removed_at IS NULL
				ORDER BY
					at.description ASC,
					a.description ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action === 'get_email_alert') {
		global $db;
		$res = new StdClass();
		$alert_ID = $_REQUEST['alert_ID'];

		$sql = "SELECT
					a.ID,
					a.description,
					a.min_threshold,
					a.max_threshold,
					a.frequency,
					a.alert_type_ID,
					at.description AS type_description,
					at.name AS type_name,
					a.active,
					a.removed_at
				FROM
					[NYSUS_REPETITIVE_MES].dbo.alerts a
					LEFT JOIN [NYSUS_REPETITIVE_MES].dbo.alert_types at ON at.ID = a.alert_type_ID
				WHERE
					a.ID = $alert_ID;";
		$res = $db->query($sql);

		if ($res) {
			$res = $res[0];

			$sql = "SELECT
						ID,
						the_day,
						active
					FROM
						alert_schedule
					WHERE
						alert_ID = $alert_ID;";
			$res['schedule'] = $db->query($sql);

			$sql = "SELECT
						ID,
						the_shift,
						active
					FROM
						MES_COMMON.dbo.alert_shifts
					WHERE
						alert_ID = $alert_ID;";
			$res['shifts'] = $db->query($sql);

			$sql = "SELECT
						machine_ID
					FROM
						[NYSUS_REPETITIVE_MES].dbo.alert_machines
					WHERE
						alert_ID = $alert_ID;";
			$res['machines'] = $db->query($sql);

			$sql = "SELECT
						operator_ID
					FROM
						MES_COMMON.dbo.alert_operators
					WHERE
						alert_ID = $alert_ID;";
			$res['operators'] = $db->query($sql);
		}

		echo json_encode($res);
	}


	if ($action === 'get_alert_types') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					description
				FROM
					[NYSUS_REPETITIVE_MES].dbo.alert_types
				ORDER BY
					description ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action === 'get_operators') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					name,
					email
				FROM
					MES_COMMON.dbo.operators
				WHERE
					deactive_date IS NULL
					AND email IS NOT NULL
					AND email <> ''
				ORDER BY
					name ASC;";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action === 'insert_alert') {
		global $db;
		$res = new StdClass();
		
		$description = $_REQUEST['description'];
		$max_threshold = $_REQUEST['max_threshold'];
		$alert_type_ID = $_REQUEST['alert_type_ID'];
		$active = $_REQUEST['active'];

		// create the new alerts
		$sql = "INSERT INTO
					alerts (
						description,
						max_threshold,
						alert_type_ID,
						active
					)
					OUTPUT INSERTED.ID
					VALUES (
						'$description',
						$max_threshold,
						$alert_type_ID,
						$active
					);";

		//die($sql);
		$res = $db->query($sql);

		if ($res) {
			$alert_ID = $res[0]['ID'];

			// add machines to the alert
			$machines = $_REQUEST['machines'];
			$sql = "INSERT INTO
						alert_machines (
							machine_ID,
							alert_ID
						) VALUES ";
			
			$machine_strs = array();
			foreach ($machines as $i => $ID) {
				array_push($machine_strs, "($ID, $alert_ID)");
			}
			$sql .= implode(', ', $machine_strs);
			//die($sql);
			$res = $db->query($sql);

			// add operators to the alert
			$operators = $_REQUEST['operators'];
			$sql = "INSERT INTO
						alert_operators (
							operator_ID,
							alert_ID
						) VALUES ";
			
			$operator_strs = array();
			foreach ($operators as $i => $ID) {
				array_push($operator_strs, "($ID, $alert_ID)");
			}
			$sql .= implode(', ', $operator_strs);
			//die($sql);
			$res = $db->query($sql);

			// add schedule to the alert
			$schedule = $_REQUEST['schedule'];
			$sql = "INSERT INTO
						alert_schedule (
							the_day,
							alert_ID,
							active
						) VALUES ";
			
			$schedule_strs = array();
			foreach ($schedule as $i => $d) {
				$the_day = $d['the_day'];
				$active = $d['active'];
				array_push($schedule_strs, "($the_day, $alert_ID, $active)");
			}
			$sql .= implode(', ', $schedule_strs);
			//die($sql);
			$res = $db->query($sql);

			// add shifts to the alert
			$shifts = $_REQUEST['shifts'];
			$sql = "INSERT INTO
						alert_shifts (
							the_shift,
							alert_ID,
							active
						) VALUES ";
			
			$shift_strs = array();
			foreach ($shifts as $i => $s) {
				$the_shift = $s['the_shift'];
				$active = $s['active'];
				array_push($shift_strs, "($the_shift, $alert_ID, $active)");
			}
			$sql .= implode(', ', $shift_strs);
			//die($sql);
			$res = $db->query($sql);
		}

		echo json_encode($res);
	}



	if ($action === 'update_alert') {
		global $db;
		$res = new StdClass();

		$alert_ID = $_REQUEST['alert_ID'];
		$description = $_REQUEST['description'];
		$max_threshold = $_REQUEST['max_threshold'];
		$alert_type_ID = $_REQUEST['alert_type_ID'];
		$active = $_REQUEST['active'];

		// update the alert
		$sql = "UPDATE
					alerts
				SET
					description = '$description',
					max_threshold = $max_threshold,
					alert_type_ID = $alert_type_ID,
					active = $active
				WHERE
					ID = $alert_ID;";

		//die($sql);
		$res = $db->query($sql);


		// update alert machines
		$sql = "DELETE FROM
					alert_machines
				WHERE
					alert_ID = $alert_ID;";
		//die($sql);
		$res = $db->query($sql);

		$sql = "INSERT INTO
					alert_machines (
						machine_ID,
						alert_ID
					) VALUES ";
		
		$machines = $_REQUEST['machines'];
		$machine_strs = array();
		foreach ($machines as $i => $ID) {
			array_push($machine_strs, "($ID, $alert_ID)");
		}
		$sql .= implode(', ', $machine_strs);
		//die($sql);
		$res = $db->query($sql);


		// update alert operators
		$sql = "DELETE FROM
					alert_operators
				WHERE
					alert_ID = $alert_ID;";
		//die($sql);
		$res = $db->query($sql);

		$sql = "INSERT INTO
					alert_operators (
						operator_ID,
						alert_ID
					) VALUES ";
		
		$operators = $_REQUEST['operators'];
		$operator_strs = array();
		foreach ($operators as $i => $ID) {
			array_push($operator_strs, "($ID, $alert_ID)");
		}
		$sql .= implode(', ', $operator_strs);
		//die($sql);
		$res = $db->query($sql);

		// update the alert schedule
		$schedule = $_REQUEST['schedule'];
		foreach ($schedule as $i => $day) {
			$the_day = $day['the_day'];
			$active = $day['active'];

			$sql = "UPDATE
						alert_schedule
					SET
						active = $active
					WHERE
						the_day = $the_day
						AND alert_ID = $alert_ID;";
			$res = $db->query($sql);
		}

		// update the alert shifts
		$shifts = $_REQUEST['shifts'];
		foreach ($shifts as $i => $shift) {
			$the_shift = $shift['the_shift'];
			$active = $shift['active'];

			$sql = "UPDATE
						alert_shifts
					SET
						active = $active
					WHERE
						the_shift = $the_shift
						AND alert_ID = $alert_ID;";
			$res = $db->query($sql);
		}

		echo json_encode($res);
	}


	if ($action === 'delete_alert') {
		global $db;
		$res = new StdClass();
		$alert_ID = $_REQUEST['alert_ID'];

		$sql = "UPDATE
					alerts
				SET
					removed_at = GETDATE()
				WHERE
					ID = $alert_ID;";
		$res = $db->query($sql);


		echo json_encode($res);
	}
?>