<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];
	call_user_func($action);

	function get_departments_and_areas() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.departments
					ORDER BY department_name";
		$res->departments = $db->query($sql);
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.areas
					ORDER BY area_desc";
		$res->areas = $db->query($sql);
		
		echo json_encode($res);
	}

	function get_departments_and_areas_and_sub_areas() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.departments
					ORDER BY department_name";
		$res->departments = $db->query($sql);
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.areas
					ORDER BY area_desc";
		$res->areas = $db->query($sql);
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.sub_areas
					ORDER BY sub_area_desc";
		$res->sub_areas = $db->query($sql);
		
		echo json_encode($res);
	}

	function get_jobs() {
		global $db;
		$dept_ID = $_REQUEST['dept_ID'];
		$area_ID = $_REQUEST['area_ID'];
		$sub_area_ID = $_REQUEST['sub_area_ID'];
		
		// if ($dept_ID == "") {
		// 	$dept_ID = "IS NULL";
		// } else {
		// 	$dept_ID = " = ".$dept_ID;
		// }
		
		// if ($area_ID == "") {
		// 	$area_ID = "IS NULL";
		// } else {
		// 	$area_ID = " = ".$area_ID;
		// }

		// if ($sub_area_ID == "") {
		// 	$sub_area_ID = "IS NULL";
		// } else {
		// 	$sub_area_ID = " = ".$sub_area_ID;
		// }

		if ($sub_area_ID != "") {
			$where = "WHERE j.sub_area_ID = ".$sub_area_ID." ";
		} else if ($area_ID != "") {
			$where = "WHERE j.area_ID = ".$area_ID." AND sub_area_ID IS NULL ";
		} else if ($dept_ID != "") {
			$where = "WHERE j.department_ID = ".$dept_ID." AND j.area_ID IS NULL AND j.sub_area_ID IS NULL ";
		} else if ($dept_ID == "" && $area_ID == "" && $sub_area_ID == "") {
			$where = "WHERE j.department_ID IS NULL AND j.area_ID IS NULL AND j.sub_area_ID IS NULL ";
		} else {
			$where = "";
		}
		
		$sql = "SELECT j.ID, j.department_ID, j.area_ID, j.sub_area_ID, j.job_name, j.job_details, j.active, j.min_operators_qualified, COUNT(DISTINCT s.operator_ID) as num_signoffs
				  FROM MES_COMMON.dbo.ojt_jobs j
						LEFT OUTER JOIN MES_COMMON.dbo.ojt_job_signoffs s ON j.ID = s.ojt_job_ID
				  ".$where."
				  GROUP BY j.ID, j.department_ID, j.area_ID, j.sub_area_ID, j.job_name, j.job_details, j.active, j.min_operators_qualified
				  ORDER BY j.active DESC, j.job_name ";
		//die($sql);
		echo json_encode($db->query($sql));
	}
	
	function save_job() {
		global $db;
		$res = new StdClass();
		
		if (!isset($_REQUEST["job_ID"])) {
			$_REQUEST['job_ID'] = "";
		}
		if (!isset($_REQUEST["area_ID"])) {
			$_REQUEST['area_ID'] = "";
		}
		if (!isset($_REQUEST["dept_ID"])) {
			$_REQUEST['dept_ID'] = "";
		}
		if (!isset($_REQUEST["sub_area_ID"])) {
			$_REQUEST['sub_area_ID'] = "";
		}
		
		
		if ($_REQUEST["job_ID"] != "null" && $_REQUEST["job_ID"] != "") {
				$sql = "UPDATE MES_COMMON.dbo.ojt_jobs
						  SET job_name = '" . fixDB($_REQUEST["job_name"]) . "', job_details = '" . fixDB($_REQUEST["job_details"]) . "', active = " . $_REQUEST["active"] . ", min_operators_qualified = " . $_REQUEST['min_operators_qualified'] . "
						  WHERE ID = " . $_REQUEST["job_ID"];
		} else {
			if ($_REQUEST['sub_area_ID'] != "null" && $_REQUEST['sub_area_ID'] != "") {
				$sql = "SELECT sa.area_ID, a.department_ID
						FROM MES_COMMON.dbo.sub_areas sa
						LEFT JOIN MES_COMMON.dbo.areas a ON a.ID = sa.area_ID
						WHERE sa.ID = ".$_REQUEST['sub_area_ID'];
				$res2 = $db->query($sql);

				if ($res2) {
					$sub_area = $res2[0];
					$sql = "INSERT INTO MES_COMMON.dbo.ojt_jobs (department_ID, area_ID, sub_area_ID, job_name, job_details, min_operators_qualified, active) VALUES (
							".$sub_area['department_ID'].",
							".$sub_area['area_ID'].",
							".$_REQUEST['sub_area_ID'].",
							'".fixDB($_REQUEST["job_name"])."', 
							'".fixDB($_REQUEST["job_details"])."', 
							".$_REQUEST['min_operators_qualified'].",
							1)";
				}
			} else {
				if ($_REQUEST["area_ID"] != "null" && $_REQUEST["area_ID"] != "") {
					$sql = "SELECT dept_ID FROM MES_COMMON.dbo.areas WHERE ID = " . $_REQUEST["area_ID"];
					$res2 = $db->query($sql);
					if (count($res2) > 0) {
						$sql = "INSERT INTO MES_COMMON.dbo.ojt_jobs (department_ID, area_ID, job_name, job_details, active, min_operators_qualified) VALUES (
										NULL, 
										" . $_REQUEST["area_ID"] . ", 
										'" . fixDB($_REQUEST["job_name"]) . "', 
										'" . fixDB($_REQUEST["job_details"]) . "', 
										1,
										" . $_REQUEST['min_operators_qualified'] . ")";
					}
				} else {
					if ($_REQUEST["dept_ID"] != "null" && $_REQUEST["dept_ID"] != "") {
						//dept wide
						$sql = "INSERT INTO MES_COMMON.dbo.ojt_jobs (department_ID, area_ID, job_name, job_details, active, min_operators_qualified) VALUES (
										" . $_REQUEST["dept_ID"] . ", 
										NULL, 
										'" . fixDB($_REQUEST["job_name"]) . "', 
										'" & fixDB($_REQUEST["job_details"]) . "', 
										1,
										" . $_REQUEST['min_operators_qualified'] . ")";
					} else {
						$sql = "INSERT INTO MES_COMMON.dbo.ojt_jobs (department_ID, area_ID, job_name, job_details, active, min_operators_qualified) VALUES (
										NULL, 
										NULL, 
										'" . fixDB($_REQUEST["job_name"]) . "', 
										'" . fixDB($_REQUEST["job_details"]) . "', 
										1,
										" . $_REQUEST['min_operators_qualified'] . ")";
					}
				}
			}
		}
			
		$db->query($sql);
		
		$res->success = 1;
		$res->sql = $sql;
		echo json_encode($res);
	}

?>