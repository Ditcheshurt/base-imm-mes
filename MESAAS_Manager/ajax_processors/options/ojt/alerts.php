<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);

	function get_departments_and_areas() {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.departments
					ORDER BY department_name";
		$res->departments = $db->query($sql);
		
		$sql = "SELECT *
					FROM MES_COMMON.dbo.areas
					ORDER BY area_desc";
		$res->areas = $db->query($sql);
		
		echo json_encode($res);
	}

	function get_alerts() {
		global $db;
		$area_ID = $_REQUEST['area_ID'];
		
		if ($area_ID == "") {
			$area_ID = "IS NULL";
		} else {
			$area_ID = " = ".$area_ID;
		}
		
		$sql = "SELECT q.*, o.name as entered_name, o2.name as last_updated_name 
					FROM MES_COMMON.dbo.qa_items q
						JOIN MES_COMMON.dbo.operators o ON q.entered_by = o.ID
						LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON q.last_updated_by = o2.ID
					WHERE q.qa_area_ID = " . $_REQUEST["area_ID"] . " 
					ORDER BY q.item_prefix, q.ID";
		echo json_encode($db->query($sql));
	}
	
	function delete_alert() {
		global $db;
		$alert_ID = $_REQUEST['alert_ID'];
		
		$sql = "DELETE FROM MES_COMMON.dbo.qa_items WHERE ID = " . $alert_ID;
		
		$db->query($sql);
		
		get_alerts();
	}

?>