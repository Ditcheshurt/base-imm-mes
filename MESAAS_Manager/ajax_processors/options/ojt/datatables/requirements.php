<?php

// Override the catalog setting in app_config.json
$database = "MES_COMMON";

// DataTables PHP library and database connection
include("../../../../lib/DataTables/DataTables.php");

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
Editor::inst($db, 'ojt_requirements', 'ID')
	->fields(
		Field::inst('ojt_requirements.ojt_requirement_type_ID')
			->options('ojt_requirement_types', 'ID', 'type_description'),
		Field::inst('ojt_requirements.ojt_signoff_template_ID')
			->options('ojt_signoff_templates', 'ID', 'template_description'),
		Field::inst('ojt_requirements.requirement_description'),
		Field::inst('ojt_requirements.requirement_text'),
		Field::inst('ojt_requirements.allow_mes_signoff'),
		Field::inst('ojt_requirements.expire_days')
			->setFormatter(  'Format::ifEmpty', null ),
		Field::inst('ojt_requirements.video_url')
			->setFormatter(  'Format::ifEmpty', null ),
		Field::inst('ojt_requirements.image_url')
			->upload(
				Upload::inst(function ($file, $id) {
					$rand = mt_rand();
					$path = $_SERVER["DOCUMENT_ROOT"] . "\\common\\images\\ojt\\";
					$url = "../common/images/ojt/";
					$file_name = $rand . '~~' . $file['name'];

					move_uploaded_file($file['tmp_name'], $path . $file_name);
					return $url . $file_name;
				})
			->allowedExtensions(array('pdf', 'png', 'jpg', 'gif'), "Please upload pdf, png, jpg or gif file"))
			->setFormatter(  'Format::ifEmpty', null ),
		Field::inst('ojt_requirements.created_date')
			->getFormatter('Format::date_sql_to_format', Format::DATE_ISO_8601)
			->setFormatter('Format::date_format_to_sql', Format::DATE_ISO_8601),
		Field::inst('ojt_requirements.updated_date')
			->setValue(date('c'))
			->setFormatter('Format::date_format_to_sql'),
		Field::inst('ojt_requirements.active'),
		Field::inst('ojt_requirement_types.type_description')

	)
	->leftJoin('ojt_requirement_types', 'ojt_requirements.ojt_requirement_type_ID', '=', 'ojt_requirement_types.ID')
	->leftJoin('ojt_signoff_templates', 'ojt_requirements.ojt_signoff_template_ID', '=', 'ojt_signoff_templates.ID')
	->process($_POST)
	->json();