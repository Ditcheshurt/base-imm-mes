<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	if ($action == 'get_sub_areas') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT
					sa.ID,
					sa.sub_area_desc,
					ISNULL(jc.job_count, 0) AS job_count
				FROM
					MES_COMMON.dbo.sub_areas sa
					JOIN MES_COMMON.dbo.areas a ON a.ID = sa.area_ID
					LEFT JOIN (
						SELECT sub_area_ID, COUNT(*) AS job_count FROM MES_COMMON.dbo.ojt_jobs GROUP BY sub_area_ID
					) jc ON jc.sub_area_ID = sa.ID
				WHERE
					a.ID = ".$_REQUEST['area_ID'].";";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'get_sub_area') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "SELECT ID, sub_area_desc FROM MES_COMMON.dbo.sub_areas WHERE ID = ".$_REQUEST['sub_area_ID'].";";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'insert_sub_area') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "INSERT INTO MES_COMMON.dbo.sub_areas (area_ID, sub_area_desc) VALUES (".$_REQUEST['area_ID'].", '".$_REQUEST['sub_area_desc']."');";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

	if ($action == 'update_sub_area') {
	    global $db;
	    $res = new StdClass();
	    
	    $sql = "UPDATE MES_COMMON.dbo.sub_areas SET sub_area_desc = '".$_REQUEST['sub_area_desc']."' WHERE ID = ".$_REQUEST['sub_area_ID'].";";
	    //die($sql);
	    $res = $db->query($sql);
	    echo json_encode($res);
	}

?>