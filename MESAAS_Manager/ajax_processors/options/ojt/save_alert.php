<?php

	require_once("../../init.php");
	
	$area_ID = $_REQUEST['area_ID'];
	$upload_dir = $GLOBALS['APP_CONFIG']['alert_upload_dir']."\\area_".$area_ID;
	$cleanupTargetDir = true;
	
	if (!file_exists($upload_dir)) {
		@mkdir($upload_dir);
	}
	
	
	// Get a file name
	$fileName = "";
	if (isset($_REQUEST["txt_file"])) {
		$fileName = $_REQUEST["txt_file"];
	} elseif (!empty($_FILES)) {
		$fileName = $_FILES["txt_file"]["name"];
	}
	
	if ($fileName != "") {
		$filePath = $upload_dir . "\\" . $fileName;
		
		// Chunking might be enabled
		$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
		$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
		
		
		// Remove old temp files
		if ($cleanupTargetDir) {
			if (!is_dir($upload_dir) || !$dir = opendir($upload_dir)) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
			}
		
			while (($file = readdir($dir)) !== false) {
				$tmpfilePath = $upload_dir . "\\" . $file;
		
				// If temp file is current file proceed to the next
				if ($tmpfilePath == "{$filePath}.part") {
					continue;
				}
		
				// Remove temp file if it is older than the max age and is not the current file
				if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
					@unlink($tmpfilePath);
				}
			}
			closedir($dir);
		}
		
		
		// Open temp file
		if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
		}
		
		if (!empty($_FILES)) {
			if ($_FILES["txt_file"]["error"] || !is_uploaded_file($_FILES["txt_file"]["tmp_name"])) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
			}
		
			// Read binary input stream and append it to temp file
			if (!$in = @fopen($_FILES["txt_file"]["tmp_name"], "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		} else {
			if (!$in = @fopen("php://input", "rb")) {
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			}
		}
		
		while ($buff = fread($in, 4096)) {
			fwrite($out, $buff);
		}
		
		@fclose($out);
		@fclose($in);
		
		// Check if file has been uploaded
		if (!$chunks || $chunk == $chunks - 1) {
			// Strip the temp .part suffix off
			rename("{$filePath}.part", $filePath);
		}
	}		
	
		
	$alert_ID = "";
	
	if (isset($_REQUEST['alert_ID'])) {
		$alert_ID = $_REQUEST['alert_ID'];
	}
	
	if ($alert_ID == "") {
		$sql = "INSERT INTO MES_COMMON.dbo.[qa_items] ([item_prefix],[qa_area_ID],[entered_time],[entered_by],[last_updated],[last_updated_by],[qa_title],[qa_link],[start_date],[end_date],[expiring_interval],[severity]) VALUES (
						'" . $_REQUEST['sel_item_prefix'] . "', 
						" . $area_ID . ", 
						GETDATE(), 
						" . $_SESSION['op_ID'] . ", 
						GETDATE(), 
						" . $_SESSION['op_ID'] . ", 
						'" . fixDB($_REQUEST['txt_title']) . "', 
						'" . $fileName . "', 
						'" . $_REQUEST['txt_start_date'] . "', 
						'" . $_REQUEST['txt_end_date'] . "', 
						0, 
						'" . $_REQUEST['sel_severity'] . "')";
	} else {
		$sql = "UPDATE MES_COMMON.dbo.qa_items
					SET item_prefix = '" . $_REQUEST['sel_item_prefix'] . "', 
						  last_updated = GETDATE(), 
						  last_updated_by = " . $_SESSION['op_ID'] . ", 
						  qa_title = '" . fixDB($_REQUEST['txt_title']) . "', 
						  start_date = '" . fixDB($_REQUEST['txt_start_date']) . "', 
						  end_date = '" . fixDB($_REQUEST['txt_end_date']) . "', 
						  severity = " . $_REQUEST['sel_severity'] . " 
					WHERE id = " . $alert_ID . ";";
					
		if ($fileName != "") {
			$sql .= "UPDATE qa_items 
						SET qa_link = '" . $fileName . "' 
						WHERE id = " . $alert_ID .";";
		}
		
		if (isset($_REQUEST['cb_reset']) && $_REQUEST['cb_reset'] == "on") {
			$sql .= "DELETE FROM MES_COMMON.dbo.qa_signoffs WHERE qa_item_ID = " . $alert_ID . ";";
		}
	}
	
	//echo $sql;
	
	$res = $db->query($sql);
	
	
	// Return Success JSON-RPC response
	//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');

?>
<html>
	<body style="margin:0px;padding:0px;">
		<script type="text/javascript">
			var el = top.document.getElementById('div_manage_info');
			el.innerHTML = 'Alert saved!';
			el.style.display = 'block';
			el.className = 'alert alert-success';
			
			
			var el2 = top.document.getElementById('selArea');

			setTimeout(function() {
				top.loadAlerts(el2.value);
			}, 2000);
		</script>
	</body>
</html>