<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_tools") {
		global $db;

		$sql = "
			SELECT t.ID, t.tool_description, t.short_description, t.active, t.shot_count, v.group_description, t.auto_label, t.ojt_group_id, COUNT(DISTINCT pg.part_group_ID) as num_part_groups,tlt.mat_number
			FROM tools t
				LEFT OUTER JOIN CUSTOM_tool_mat_tracking tlt ON tlt.tool_ID = t.ID AND tlt.active = 1 AND tlt.end_timestamp IS NULL AND tlt.start_timestamp < GETDATE()
				LEFT OUTER JOIN v_OJT_groups v ON t.ojt_group_ID = v.ID
				LEFT OUTER JOIN v_tool_part_groups pg ON t.ID = pg.tool_ID
			GROUP BY t.ID, t.tool_description, t.short_description, t.active, t.shot_count, v.group_description, t.auto_label, t.ojt_group_id, tlt.mat_number
			ORDER BY t.tool_description";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	else if ($action == "load_ojt_groups") {
		global $db;

		$sql = "SELECT *
					FROM v_OJT_groups
					ORDER BY group_description";

		$res = $db->query($sql);

		echo json_encode($res);
	}

	else if ($action == "load_tool_parts_and_machines") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$tool_ID = $_REQUEST['tool_ID'];

		$sql = "SELECT p.*, tp.parts_per_tool, g.part_group_description, tp.part_order
					FROM tool_parts tp
						JOIN parts p ON tp.part_ID = p.ID
						LEFT OUTER JOIN part_group_hrt g ON p.part_group_ID = g.ID
					WHERE tp.tool_ID = $tool_ID
					ORDER BY tp.part_order";

		//die($sql);
		$res->parts = $db->query($sql);

		$sql = "SELECT m.*, mt.target_cycle_time, mt.over_cycle_grace_period
					FROM machine_tools mt
						JOIN machines m ON mt.machine_ID = m.ID
					WHERE mt.tool_ID = $tool_ID
					ORDER BY m.ID";

		//die($sql);
		$res->machines = $db->query($sql);
		
		for($i = 0; $i< count($res->machines); $i++){
			$machine_ID = $res->machines[$i]["ID"];
			$sql = "SELECT
						mtcl.ID AS ctrl_limit_ID, dps.ID AS data_point_ID, CONCAT(dps.data_point_desc,' (',dps.units,')') AS data_point_desc ,mtcl.upper_ctrl_limit,mtcl.lower_ctrl_limit,mtcl.target_value
					FROM data_point_setup dps
					LEFT JOIN machine_tool_ctrl_limits mtcl ON mtcl.data_point_ID = dps.ID AND mtcl.tool_ID = $tool_ID AND mtcl.machine_ID = $machine_ID AND mtcl.deactivated = 0
					WHERE dps.ID NOT IN (1,2)";
			$res->machines[$i]["ctrl_lmts"] = $db->query($sql);
		}
		echo json_encode($res);
	}

	else if ($action == "load_machines_and_tools") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT m.*
					FROM machines m
					ORDER BY m.machine_name ";

		//die($sql);
		$res->machines = $db->query($sql);

		$sql = "SELECT mt.*
					FROM machine_tools mt ";

		//die($sql);
		$res->machine_tools = $db->query($sql);

		echo json_encode($res);
	}

	else if($action == "load_part_and_tool_parts") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT p.*
					FROM parts p
					ORDER BY p.part_desc ";

		//die($sql);
		$res->parts = $db->query($sql);

		$sql = "SELECT tp.*
					FROM tool_parts tp ";

		//die($sql);
		$res->tool_parts = $db->query($sql);

		echo json_encode($res);
	}

	else if($action == "add_tool") {
		$sql = "
			DECLARE @id_tbl TABLE (id int);
			DECLARE @tool_ID int;
			
			INSERT INTO @id_tbl
			SELECT t1.ID FROM (
				INSERT INTO tools (tool_description, short_description, active, ojt_group_id, auto_label)
				OUTPUT inserted.ID
				VALUES (
					'".fixDB($_REQUEST['tool_description'])."',
					'".fixDB($_REQUEST['short_description'])."',
					".$_REQUEST['active'].",
					".$_REQUEST['ojt_group_id'].",
					".$_REQUEST['auto_label']."
				)
			) AS t1;
			SELECT TOP 1 @tool_ID = id FROM @id_tbl;";
			
		$sql .= "
			UPDATE CUSTOM_tool_mat_tracking
			SET end_timestamp = GETDATE(),
				active = 0
			WHERE tool_ID = @tool_ID
				AND start_timestamp < GETDATE()
				AND end_timestamp IS NULL
				AND active = 1;
				
			INSERT INTO CUSTOM_tool_mat_tracking (operator_ID, tool_ID, mat_number, start_timestamp, active) VALUES (
				".$_REQUEST['operator_ID'].",
				@tool_ID,
				'".$_REQUEST['mat_number']."',
				GETDATE(),
				1
			);";
		
		//die($sql);
		$db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	else if($action == "edit_tool") {
		$sql = "UPDATE tools
					SET tool_description = '".fixDB($_REQUEST['tool_description'])."',
						short_description = '".fixDB($_REQUEST['short_description'])."',
						active = ".$_REQUEST['active'].",
						ojt_group_id = ".$_REQUEST['ojt_group_id'].",
						auto_label = ".$_REQUEST['auto_label']."
					WHERE ID = ".$_REQUEST['tool_ID'];
		$sql .= "
			UPDATE CUSTOM_tool_mat_tracking
			SET end_timestamp = GETDATE(),
				active = 0
			WHERE tool_ID = ".$_REQUEST['tool_ID']."
				AND start_timestamp < GETDATE()
				AND end_timestamp IS NULL
				AND active = 1;
			
			IF (LEN('".$_REQUEST['mat_number']."') > 0) BEGIN
				INSERT INTO CUSTOM_tool_mat_tracking (operator_ID, tool_ID, mat_number, start_timestamp, active) VALUES (
					".$_REQUEST['operator_ID'].",
					".$_REQUEST['tool_ID'].",
					'".$_REQUEST['mat_number']."',
					GETDATE(),
					1
				)
			END;";
		
		//die($sql);
		$db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	else if($action == "edit_machine_tool") {
		$sql = "UPDATE machine_tools
					SET target_cycle_time = ".$_REQUEST['target_cycle_time'].",
						over_cycle_grace_period = ".$_REQUEST['over_cycle_grace_period']."
					WHERE machine_ID = ".$_REQUEST['machine_ID']."
						AND tool_ID = ".$_REQUEST['tool_ID'];
		
		//die($sql);
		$db->query($sql);
		
		$cl = $_REQUEST['ctrl_lmts'];
		$cl_str = '';
		for ($i=0; $i<count($cl); $i++) {
			$elem = array_keys($cl[$i]);
			$lk = end($elem);
			foreach ($cl[$i] as $k => $v) {
				$cl_str .= ($k . ":" . (strlen($v)==0?"NULL":$v));
				if ($k != $lk){
					$cl_str .= ",";
				}
			}
			if ($i<count($cl)-1) {
				$cl_str .= "|";
			}
		}

		$sql = "EXEC dbo.sp_MACHINE_MES_submitNewMachineToolCtrlLimits @machine_ID = ".$_REQUEST['machine_ID'].", @tool_ID = ".$_REQUEST['tool_ID'].", @cl_str = N'" . $cl_str . "', @operator_ID = " . $_REQUEST['operator_ID'];
		//die($sql);
		$db->query($sql);
		
		$res = new StdClass();
		$res->success = 1;
		//$res->update_mtcl = $sql;
		echo json_encode($res);
	}

	else if($action == "DISASSOCIATE_MACHINE") {
		global $db;

		$sql = "DELETE FROM machine_tools
					WHERE machine_ID = ".$_REQUEST['machine_ID']."
						AND tool_ID = ".$_REQUEST['tool_ID'];

		$res = $db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	else if ($action == "save_tool_machines") {
		$tool_ID = $_REQUEST['tool_ID'];
		$post_data = $_REQUEST['post_data'];
		$sql = "";

		for ($i = 0; $i < count($post_data); $i++) {
			$d = explode("~", $post_data[$i]);
			$machine_ID = $d[0];
			$tct = $d[1];
			$ocgp = $d[2];

			if ($d[3] == 1) {
				$sql .= "IF NOT EXISTS(SELECT * FROM machine_tools WHERE machine_ID = $machine_ID AND tool_ID = $tool_ID)
								INSERT INTO machine_tools (machine_ID, tool_ID, target_cycle_time, over_cycle_grace_period) VALUES (
									$machine_ID,
									$tool_ID,
									$tct,
									$ocgp
								)
							ELSE
								UPDATE machine_tools
								SET target_cycle_time = $tct,
									over_cycle_grace_period = $ocgp
								WHERE machine_ID = $machine_ID
									AND tool_ID = $tool_ID; ";
			} else {
				$sql .= "DELETE FROM machine_tools WHERE machine_ID = $machine_ID AND tool_ID = $tool_ID; ";
			}
		}

		$db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}


	else if ($action == "load_part_groups") {
		$sql = "SELECT * FROM part_group_hrt ORDER BY part_group_description";

		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	else if ($action == "load_label_templates") {
		$sql = "SELECT * FROM print_templates WHERE archived_timestamp IS NULL AND active = 1 ORDER BY template_name;";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}

	else if ($action == "add_part") {

		$image_save_path = $GLOBALS['APP_CONFIG']['MESAAS_image_path'];

		//var_dump($_FILES);

		$force_po = 0;
		if ($_POST['force_part_order'] > 0) {
			$force_po = $_POST['force_part_order'];
		} else {
			$force_po = "ISNULL((SELECT MAX(ISNULL(part_order, 0)) FROM tool_parts WHERE tool_ID = ".$_POST['tool_ID']."), 0) + 10";
		}
		
		$sql = "	SET NOCOUNT ON;
					DECLARE @part_ID int;

					INSERT INTO parts (part_desc, part_number, unit_cost, standard_pack_qty, part_group_id, print_template_ID) VALUES (
						'".fixDB($_POST['part_desc'])."',
						'".fixDB($_POST['part_number'])."',
						ROUND(ISNULL(TRY_CAST('".fixDB($_POST['unit_cost'])."' AS float),0),2),
						".$_POST['std_pack_qty'].",
						".$_POST['part_group_ID'].",
						".$_POST['print_template_ID']."
					);

					SET @part_ID = @@IDENTITY;

					INSERT INTO tool_parts (tool_ID, part_ID, parts_per_tool, serialized, zoned, active, part_order) VALUES (
						".$_POST['tool_ID'].",
						@part_ID,
						".$_POST['parts_per_tool'].",
						1,
						1,
						1,
						".$force_po."
					);";

		if (!empty($_FILES['part_image']['name'])) {
			//do file upload:
			$uploaded_file_name = $_FILES['part_image']['name'];
			$new_file_name = tempnam ( $image_save_path , "PART_" );
			move_uploaded_file($_FILES['part_image']['tmp_name'], __DIR__.'/../../../proper-imm-mes/public/images/part_images/'. $_FILES["part_image"]['name']);

			$sql .= "	UPDATE parts
							SET part_image = '/proper-injection-mes/public/images/part_images/".fixDB($uploaded_file_name)."'
							WHERE ID = @part_ID;";
		}

		$db->query($sql);

		$res = new StdClass();
		$res->image_save_path = $image_save_path;
		$res->sql = $sql;
		$res->success = 1;
		echo json_encode($res);
	}

	else if ($action == "edit_part") {

		$image_save_path = $GLOBALS['APP_CONFIG']['MESAAS_image_path'];
		$video_save_path = $GLOBALS['APP_CONFIG']['MESAAS_video_path'];

		//var_dump($_FILES);


		$sql = "	UPDATE parts
					SET part_desc = '".fixDB($_POST['part_desc'])."',
						part_number = '".fixDB($_POST['part_number'])."',
						unit_cost = ROUND(ISNULL(TRY_CAST('".fixDB($_POST['unit_cost'])."' AS float),0),2),
						standard_pack_qty = ".$_POST['std_pack_qty'].",
						part_group_id = ".$_POST['part_group_ID'].",
						print_template_ID = ".$_POST['print_template_ID']."
					WHERE ID = ".$_POST['part_ID'].";

					UPDATE tool_parts
					SET parts_per_tool = ".$_POST['parts_per_tool'].",
					part_order = ".$_POST['force_part_order']."
					WHERE tool_ID = ".$_POST['tool_ID']."
						AND part_ID = ".$_POST['part_ID'].";
					";

		if (!empty($_FILES['part_image']['name'])) {
			//do file upload:
			$uploaded_file_name = $_FILES['part_image']['name'];
			$new_file_name = tempnam ( $image_save_path , "PART_" );
			move_uploaded_file($_FILES['part_image']['tmp_name'], __DIR__.'/../../../imm_mes/public/images/part_images/'. $_FILES["part_image"]['name']);

			$sql .= "	UPDATE parts
							SET part_image = '/imm_mes/public/images/part_images/".fixDB($uploaded_file_name)."'
							WHERE ID = ".$_POST['part_ID'].";";
		}

		if (!empty($_FILES['part_video']['name'])) {
			$uploaded_vid_name = $_FILES['part_video']['name'];
			$new_filename = tempnam ( $video_save_path , "WIV_" );
			move_uploaded_file($_FILES['part_video']['tmp_name'], __DIR__.'/../../../imm_mes/public/videos/work_instructions/'.$_FILES['part_video']['name']);
			
			$sql .= "	UPDATE parts
							SET part_video = '/imm_mes/public/videos/work_instructions/".fixDB($uploaded_vid_name)."'
							WHERE ID = ".$_POST['part_ID'].";";
		}
		
		$db->query($sql);

		$res = new StdClass();
		$res->image_save_path = $image_save_path;
		$res->sql = $sql;
		$res->success = 1;
		echo json_encode($res);
	}

	else if ($action == "save_tool_parts") {
		$tool_ID = $_REQUEST['tool_ID'];
		$post_data = $_REQUEST['post_data'];
		$sql = "";

		for ($i = 0; $i < count($post_data); $i++) {
			$d = explode("~", $post_data[$i]);
			$part_ID = $d[0];
			$ppt = $d[1];

			if ($d[2] == 1) {
				$sql .= "IF NOT EXISTS(SELECT * FROM tool_parts WHERE part_ID = $part_ID AND tool_ID = $tool_ID)
								INSERT INTO tool_parts (part_ID, tool_ID, parts_per_tool, serialized, zoned, active, part_order) VALUES (
									$part_ID,
									$tool_ID,
									$ppt,
									1,
									1,
									1,
									ISNULL((SELECT MAX(ISNULL(part_order, 0)) FROM tool_parts WHERE tool_ID = $tool_ID), 0) + 10
								)
							ELSE
								UPDATE tool_parts
								SET parts_per_tool = $ppt
								WHERE part_ID = $part_ID
									AND tool_ID = $tool_ID; ";
			} else {
				$sql .= "DELETE FROM tool_parts WHERE part_ID = $part_ID AND tool_ID = $tool_ID; ";
			}
		}

		$db->query($sql);

		$res = new StdClass();
		$res->sql = $sql;
		$res->success = 1;
		echo json_encode($res);
	}

	else if($action == "DISASSOCIATE_PART") {
		global $db;

		$sql = "DELETE FROM tool_parts
					WHERE part_ID = ".$_REQUEST['part_ID']."
						AND tool_ID = ".$_REQUEST['tool_ID'];

		$res = $db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}

	else if ($action == "move_part") {
		$sql = "EXEC sp_MACHINE_MES_swap_part_order
					@tool_ID = ".$_REQUEST['tool_ID'].",
					@part_ID = ".$_REQUEST['part_ID'].",
					@direction = '".$_REQUEST['direction']."';";

		$res = $db->query($sql);

		$res = new StdClass();
		$res->success = 1;
		echo json_encode($res);
	}
?>