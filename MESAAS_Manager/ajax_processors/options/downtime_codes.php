<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);
	
	function get_machine_types() {
		global $db;
		
		$sql = "SELECT 
					[ID]
					,[machine_type_description]
				FROM 
					[dbo].[machine_types]
				ORDER BY 
					[machine_type_description]";
		
		echo json_encode($db->query($sql));
	}
	
	function get_downtime_codes() {
		global $db;		
		$machine_type_ID = $_REQUEST["machine_type_ID"];
		
		$sql = "SELECT 
					ID, 
					reason_description,
					maximum_duration,
					oee_category,
					count_against, 
					active, 
					L,
					CASE WHEN parent_ID = 0 THEN NULL ELSE parent_ID END as parent_ID
				FROM 
					v_downtime_reasons
				WHERE 
					machine_type_ID = $machine_type_ID
				ORDER BY 
					S";
		
		echo json_encode($db->query($sql));
	}
	
	function get_oee_categories() {
		global $db;
		
		$sql = "SELECT 
					[ID]
					,[oee_category_desc]
					,[count_against_oee]
					,[is_scheduled]
  				FROM 
					[dbo].[machine_downtime_reason_oee_categories]
				ORDER BY
					[oee_category_desc]";
		
		echo json_encode($db->query($sql));
	}

	function save_code(){
		global $db;
		if ($_REQUEST['edit_type'] == 'ADD' or $_REQUEST['edit_type'] == 'ADD_Parent') {
			insert_code();
		} else {
			update_code();
		}

		get_downtime_codes();
	}

	function insert_code(){
		global $db;
		
		$sql = "INSERT INTO 
					[dbo].[machine_downtime_reasons]
					(parent_ID, reason_description, maximum_duration, count_against, active, force_comment, oee_category, machine_type_ID) 
				VALUES (
				 	".$_REQUEST['parent_ID'].",
				 	'".fixDB($_REQUEST['reason_description'])."',
				 	".$_REQUEST['maximum_duration'].",
				 	".$_REQUEST['count_against'].",
				 	".$_REQUEST['active'].",
				 	0,
				 	".$_REQUEST['oee_category'].",
				 	".$_REQUEST['machine_type_ID'].")";

		$db->query($sql);
	}

	function update_code(){
		global $db;
		
		$sql = "UPDATE 
					[dbo].[machine_downtime_reasons]
				SET 
					[parent_ID] = '".$_REQUEST['parent_ID']."',
					[maximum_duration] = ".$_REQUEST['maximum_duration'].",
					[reason_description] = '".fixDB($_REQUEST['reason_description'])."',
					[count_against] = '".$_REQUEST['count_against']."',
					[oee_category] = '".$_REQUEST['oee_category']."',
					[active] = '".$_REQUEST['active']."'
				WHERE 
					[ID] = ".$_REQUEST['ID'];
		
		$db->query($sql);
	}

	function del_node(){
		global $db;
		
		$sql = "UPDATE 
					[dbo].[machine_downtime_reasons]
				SET 
					[active] = 0
				WHERE 
					[ID] = ".$_REQUEST['node'];
		
		echo $db->query($sql);
	}

?>