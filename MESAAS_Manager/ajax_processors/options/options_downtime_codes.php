<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action);

	function get_downtime_codes() {
		global $db;
		$sql = "SELECT ID, reason_description, count_against, active, L,
							CASE WHEN parent_ID = 0 THEN NULL ELSE parent_ID END as parent_ID
					FROM v_downtime_reasons
					ORDER BY S";
		echo json_encode($db->query($sql));
	}

	function save_code(){
		global $db;
		if ($_REQUEST['edit_type'] == 'ADD') {
			insert_code();
		} else {
			update_code();
		}

		get_downtime_codes();
	}

	function insert_code(){
		global $db;
		$sql = "INSERT INTO machine_downtime_reasons	(parent_ID, reason_description, count_against, active, force_comment) VALUES (
				 	".$_REQUEST['parent_ID'].",
				 	'".fixDB($_REQUEST['reason_description'])."',
				 	".$_REQUEST['count_against'].",
				 	".$_REQUEST['active'].",
				 	0)";
		$db->query($sql);
	}

	function update_code(){
		global $db;
		$sql = "UPDATE machine_downtime_reasons
					SET parent_ID = '".$_REQUEST['parent_ID']."',
						 reason_description = '".fixDB($_REQUEST['reason_description'])."',
						 count_against = '".$_REQUEST['count_against']."',
						 active = '".$_REQUEST['active']."'
					WHERE ID = ".$_REQUEST['ID'];
		$db->query($sql);
	}

	function del_node(){
		global $db;
		$sql = "UPDATE machine_downtime_reasons
					SET active = 0
					WHERE ID = ".$_REQUEST['node'];
		echo $db->query($sql);
	}

?>