<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	if(array_key_exists('data', $_REQUEST)) {
		$data = $_REQUEST['data'];
	}

	if ($action == 'get_templates') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					ID,
					template_name,
					template_text,
					active AS [template_active]
				FROM
					print_templates
				WHERE
					archived_timestamp IS NULL;";

		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	if ($action == 'get_template') {
		global $db;
		$res = new StdClass();
		
		$sql = "SELECT
					ID
					,template_name
					,template_text
					,created_by
					,created_timestamp
					,modified_by
					,modified_timestamp
					,archived_timestamp
					,active AS [template_active]
					,revision AS [template_revision]
				FROM
					print_templates
				WHERE
					ID = ".$_REQUEST['template_ID'].";";
		$res = $db->query($sql);
		
		echo json_encode($res);
	}

	if ($action == 'add_template') {
		global $db;
		$res = new StdClass();
		$data = $_REQUEST['data'];


		$sql = "INSERT INTO
					print_templates (
						template_name
						,template_text
						,active
						,created_by
						,created_timestamp
						,parent_id
						,revision
					) VALUES (
						'".$data['template_name']."'
						,'".$data['template_text']."'
						,'".($data['template_active']?1:0)."'
						,'".$data['operator_ID']."'
						,GETDATE()
						,0
						,1
						
					);
				SELECT @@IDENTITY AS [template_ID];";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'update_template') {
		global $db;
		$res = new StdClass();
		$data = $_REQUEST['data'];

		$sql = "UPDATE
					print_templates
				SET
					modified_by = ".$data['operator_ID']."
					,modified_timestamp = GETDATE()
					,archived_timestamp = GETDATE()
					,active = 0
				WHERE
					ID = ".$data['template_ID'].";
				
				INSERT INTO
					print_templates (
						template_name
						,template_text
						,created_by
						,created_timestamp
						,active
						,parent_id
						,revision
					) values (
						'".$data['template_name']."'
						,'".$data['template_text']."'
						,".$data['operator_ID']."
						,GETDATE()
						,".($data['template_active']=='true'?1:0)."
						,".$data['template_ID']."
						,(SELECT revision+1 FROM print_templates WHERE ID = ".$data['template_ID'].")
					);";
		//die($sql);
		$db->query($sql);
		
		$sql = "SELECT @@IDENTITY AS [template_ID];";
		$res = $db->query($sql);
		
		$sql = "UPDATE
					parts
				SET
					print_template_ID = ".$res[0]['template_ID']."
				WHERE
					print_template_ID = ".$data['template_ID'].";";
					
		$db->query($sql);

		echo json_encode($res);
	}

	if ($action == 'archive_template') {
		global $db;
		$res = new StdClass();

		$sql = "UPDATE
					print_templates
				SET
					modified_by = ".$data['operator_ID']."
					,modified_timestamp = GETDATE()
					,archived_timestamp = GETDATE()
					,active = 0
				WHERE
					ID = ".$data['template_ID'].";";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
	
	if ($action == 'get_archive_hist') {
		global $db;
		$res = new StdClass();
		$data = $_REQUEST['data'];
		
		$sql = "
			WITH history AS (
				SELECT pt.* FROM print_templates pt WHERE pt.id = ".$data['ID']."
				UNION ALL
				SELECT px.*FROM print_templates px INNER JOIN history h ON h.parent_id = px.id
			)
			SELECT h.*,op.name FROM history h LEFT JOIN MES_COMMON.dbo.operators op on h.modified_by = op.badge_ID ORDER BY h.ID DESC;
		";
		$res = $db->query($sql);
		
		echo json_encode($res);
	}

?>