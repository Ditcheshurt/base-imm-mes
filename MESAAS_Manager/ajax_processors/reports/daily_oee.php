<?php


	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == 'get_oee_data') {
		global $db;
		$res = new StdClass();

		$system_ID = 2;
		if (isset($_REQUEST["system_ID"])) {
			$system_ID = $_REQUEST["system_ID"];
		}

		$sql = "SELECT TOP 3
					CONVERT(VARCHAR(10), the_date, 120) AS the_date,
					--the_date,
					the_shift
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[shift_summary]
				WHERE
					the_date <= dbo.getProdDate(GETDATE())
					AND NOT (the_date = dbo.getProdDate(GETDATE()) AND the_shift = dbo.getShift($system_ID, GETDATE()))
				GROUP BY
					the_date,
					the_shift
				ORDER BY
					the_date DESC,
					dbo.getShiftOrder(the_shift) DESC;";

		$res->shifts = $db->query($sql);

		foreach ($res->shifts as $i => &$shift) {
			$sql = "SELECT
						ss.machine_ID,
						m.machine_number,
						m.machine_name,
						t.short_description AS [tool_desc],
						ss.part_qty,
						ss.part_goal,
						ss.part_target_qty,
						ss.performance,
						ss.scrap_numerator,
						ss.scrap_denominator,
						ss.scrap_denominator - ss.scrap_numerator AS scrap,
						ROUND(ss.quality, 2) AS quality,
						ss.total_minutes,
						ss.downtime_minutes,
						ROUND(ss.availability, 2) AS availability,
						ROUND(ss.oee, 2) AS oee
					FROM
						[NYSUS_REPETITIVE_MES].[dbo].[shift_summary] ss
						JOIN [NYSUS_REPETITIVE_MES].[dbo].[machines] m ON m.ID = ss.machine_ID
						LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[tools] t ON t.ID = ss.tool_ID
					WHERE
						CONVERT(DATE, ss.the_date) = CONVERT(DATE, '".$shift['the_date']."')
						AND ss.the_shift = ".$shift['the_shift']."
						AND m.ignored = 0
						AND m.show_reporting = 1
					ORDER BY
						m.machine_number ASC;";

			$shift['machines'] = $db->query($sql);

			$sql = "SELECT
						SUM(ss.part_qty) AS part_qty,
						SUM(ss.part_goal) AS part_goal,
						SUM(ss.part_target_qty) AS part_target_qty,
						SUM(ss.scrap_numerator) AS scrap_numerator,
						SUM(ss.scrap_denominator) AS scrap_denominator,
						SUM(ss.scrap_denominator - ss.scrap_numerator) AS scrap,
						SUM(ss.total_minutes) AS total_minutes,
						SUM(ss.downtime_minutes) AS downtime_minutes
					FROM
						shift_summary ss
						JOIN machines m ON m.ID = ss.machine_ID AND m.ignored = 0
					WHERE
						CONVERT(DATE, ss.the_date) = CONVERT(DATE, '".$shift['the_date']."')
						AND ss.the_shift = ".$shift['the_shift']."
						AND m.show_reporting = 1;";

			$summary = $db->query($sql);
			$shift['summary'] = $summary[0];
			calc_oee($shift['summary']);
		}


		$sql = "SELECT
					ss.machine_ID,
					m.machine_number,
					m.machine_name,
					t.short_description AS [tool_desc],
					SUM(ss.part_qty) AS part_qty,
					SUM(ss.part_goal) AS part_goal,
					SUM(ss.part_target_qty) AS part_target_qty,
					SUM(ss.scrap_numerator) AS scrap_numerator,
					SUM(ss.scrap_denominator) AS scrap_denominator,
					SUM(ss.scrap_denominator - ss.scrap_numerator) AS scrap,
					SUM(ss.total_minutes) AS total_minutes,
					SUM(ss.downtime_minutes) AS downtime_minutes
				FROM
					[NYSUS_REPETITIVE_MES].[dbo].[shift_summary] ss
					JOIN [NYSUS_REPETITIVE_MES].[dbo].[machines] m ON m.ID = ss.machine_ID
					LEFT JOIN [NYSUS_REPETITIVE_MES].[dbo].[tools] t ON t.ID = ss.tool_ID
					JOIN (
						SELECT TOP 3
							the_date,
							the_shift
						FROM
							[NYSUS_REPETITIVE_MES].[dbo].[shift_summary]
						WHERE
							the_date <= dbo.getProdDate(GETDATE())
							AND NOT the_date = dbo.getProdDate(GETDATE())
						GROUP BY
							the_date,
							the_shift
						ORDER BY
							the_date DESC,
							dbo.getShiftOrder(the_shift) DESC
					) AS s ON s.the_date = ss.the_date AND s.the_shift = ss.the_shift AND s.the_shift = dbo.getShift(m.system_ID, GETDATE())
				WHERE
					m.ignored = 0
					AND m.show_reporting = 1
				GROUP BY
					ss.machine_ID,
					m.machine_number,
					m.machine_name,
					t.short_description;";

		$res->total = new StdClass();
		$res->total->summary = array();
		$res->total->summary['part_qty'] = 0;
		$res->total->summary['part_goal'] = 0;
		$res->total->summary['part_target_qty'] = 0;
		$res->total->summary['scrap_numerator'] = 0;
		$res->total->summary['scrap_denominator'] = 0;
		$res->total->summary['total_minutes'] = 0;
		$res->total->summary['downtime_minutes'] = 0;

		$res->total->machines = $db->query($sql);

		foreach ($res->total->machines as $i => &$machine) {
			calc_oee($machine);

			$res->total->summary['part_qty'] += $machine['part_qty'];
			$res->total->summary['part_goal'] += $machine['part_goal'];
			$res->total->summary['part_target_qty'] += $machine['part_target_qty'];
			$res->total->summary['scrap_numerator'] += $machine['scrap_numerator'];
			$res->total->summary['scrap_denominator'] += $machine['scrap_denominator'];
			$res->total->summary['total_minutes'] += $machine['total_minutes'];
			$res->total->summary['downtime_minutes'] += $machine['downtime_minutes'];
		}

		calc_oee($res->total->summary);

		echo json_encode($res);
	}

	function calc_oee(&$data) {
		$data['performance'] = 0;
		$data['quality'] = 0;
		$data['availability'] = 0;

		if ($data['part_target_qty']) {
			$data['performance'] = $data['part_qty'] / $data['part_target_qty'];
		}
		if ($data['scrap_denominator'] != 0) {
			$data['quality'] = $data['scrap_numerator'] / $data['scrap_denominator'];
		}
		if ($data['total_minutes']) {
			$data['availability'] = ($data['total_minutes'] - $data['downtime_minutes']) / $data['total_minutes'];
		}
		
		$data['oee'] = $data['performance'] * $data['quality'] * $data['availability'];
	}

?>