<?php

    require_once("../../init.php");

    $action = $_REQUEST['action'];

    call_user_func($action, $_REQUEST, $db);


    function get_wip_part ($request, $db) {
        $sql = "SELECT TOP 1 * FROM v_MESAAS_BATCH_wip_parts WHERE ID = {$request['wip_part_ID']};";

        $result = $db->query($sql);

        if ($result) {
            $data = $result[0];
        } else {
            $data = $result;
        }

        echo json_encode($data);
    }

    function get_station_history ($request, $db) {
        // get wip station history
        $sql = "SELECT
                    wpsh.wip_part_ID,
                    s.station_desc,
                    NULL AS entry_time,
                    NULL AS exit_time,
                    op.name AS operator_name,
                    wpsh.station_order,
                    ps.cell_ID
                    --MAX(wpsh.station_order) AS last_completed_station_ID
                FROM
                    MESAAS_wip_parts w
                    LEFT JOIN (SELECT DISTINCT wip_part_ID, station_order, operator_ID FROM MESAAS_wip_part_step_history) wpsh ON wpsh.wip_part_ID = w.ID
                    JOIN MES_COMMON.dbo.operators op ON op.id = wpsh.operator_id
                    JOIN part_setup ps ON ps.ID = w.part_ID
                    LEFT JOIN stations s ON s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                WHERE
                    wpsh.wip_part_ID = {$request['wip_part_ID']}
                GROUP BY
                    wpsh.wip_part_ID,
                    s.station_desc,
                    op.name,
                    wpsh.station_order,
                    ps.cell_ID
                ORDER BY
                    wpsh.station_order ASC;";

        $station_history = $db->query($sql);

        foreach($station_history as $j => &$station) {
            // get wip instruction history
            $sql = "SELECT
                        wpsh.ID,
                        wpsh.start_time AS entry_time,
                        wpsh.complete_time AS exit_time,
                        op.name AS operator_name,
                        wpsh.instruction_step_order,
                        wpsh.completion_details,
                        tt.type_desc,
                        i.text AS instruction_text
                    FROM
                        MESAAS_wip_part_step_history wpsh
                        LEFT JOIN MESAAS_wip_parts w ON w.ID = wpsh.wip_part_ID
                        LEFT JOIN part_setup ps ON ps.ID = w.part_ID
                        LEFT JOIN stations s ON  s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                        LEFT JOIN MES_COMMON.dbo.operators op ON op.ID = wpsh.operator_ID
                        LEFT JOIN instructions i ON
                            i.cell_ID = ps.cell_ID
                            AND i.station_order = wpsh.station_order
                            AND i.instr_order = wpsh.instruction_step_order
                            AND i.part_ID = ps.ID
                        LEFT JOIN test_types tt ON tt.ID = i.test_type
                    WHERE
                        wpsh.wip_part_ID = {$request['wip_part_ID']}
                        AND s.station_order = {$station['station_order']}
                    ORDER BY
                        wpsh.instruction_step_order ASC;";

            $station['instruction_history'] = $db->query($sql);
        }

        echo json_encode($station_history);
    }

    function get_process_data ($request, $db) {
       // get process data
        $sql = "SELECT
                    wppd.ID,
                    wppd.process_location,
                    wppi.description AS process_desc,
                    wppd.process_value_desc,
                    wppd.process_value,
                    wppd.process_timestamp
                FROM
                    MESAAS_wip_parts w
                    JOIN wip_part_process_data wppd ON wppd.wip_part_ID = w.ID
                    JOIN wip_part_process_index wppi ON
                        wppi.ID = wppd.process_index
                        AND wppd.process_location LIKE wppi.process_location 
                WHERE
                    w.ID = {$request['wip_part_ID']}
                ORDER BY
                    wppd.process_location ASC, wppd.ID ASC;";

        echo json_encode($db->query($sql));
    }

    
    // if ($action == 'load_data') {
    //     global $db;
    //     $res = new StdClass();

    //     if ($_REQUEST['process_type'] == 'BATCH') {

    //         $sql = "SELECT TOP 1 * FROM v_MESAAS_BATCH_wip_parts WHERE ID = ".$_REQUEST['wip_part_ID'].";";

    //         $res->wip_part_info = $db->query($sql);

    //         // get wip station history
    //         $sql = "SELECT
    //                     wpsh.wip_part_ID,
    //                     s.station_desc,
    //                     NULL AS entry_time,
    //                     NULL AS exit_time,
    //                     op.name AS operator_name,
    //                     wpsh.station_order,
    //                     ps.cell_ID
    //                     --MAX(wpsh.station_order) AS last_completed_station_ID
    //                 FROM
    //                     MESAAS_wip_parts w
    //                     LEFT JOIN (SELECT DISTINCT wip_part_ID, station_order, operator_ID FROM MESAAS_wip_part_step_history) wpsh ON wpsh.wip_part_ID = w.ID
    //                     JOIN MES_COMMON.dbo.operators op ON op.id = wpsh.operator_id
    //                     JOIN part_setup ps ON ps.ID = w.part_ID
    //                     LEFT JOIN stations s ON s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
    //                 WHERE
    //                     wpsh.wip_part_ID = ".$_REQUEST['wip_part_ID']."
    //                 GROUP BY
    //                     wpsh.wip_part_ID,
    //                     s.station_desc,
    //                     op.name,
    //                     wpsh.station_order,
    //                     ps.cell_ID
    //                 ORDER BY
    //                     wpsh.station_order ASC;";

    //         $res->station_history = (array) $db->query($sql);

    //         foreach($res->station_history as $j => &$station) {
    //             // get wip instruction history
    //             $sql = "SELECT
    //                         wpsh.ID,
    //                         wpsh.start_time AS entry_time,
    //                         wpsh.complete_time AS exit_time,
    //                         op.name AS operator_name,
    //                         wpsh.instruction_step_order,
    //                         wpsh.completion_details,
    //                         tt.type_desc,
    //                         i.text AS instruction_text
    //                     FROM
    //                         MESAAS_wip_part_step_history wpsh
    //                         LEFT JOIN MESAAS_wip_parts w ON w.ID = wpsh.wip_part_ID
    //                         LEFT JOIN part_setup ps ON ps.ID = w.part_ID
    //                         LEFT JOIN stations s ON  s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
    //                         LEFT JOIN MES_COMMON.dbo.operators op ON op.ID = wpsh.operator_ID
    //                         LEFT JOIN instructions i ON
    //                             i.cell_ID = ps.cell_ID
    //                             AND i.station_order = wpsh.station_order
    //                             AND i.instr_order = wpsh.instruction_step_order
    //                             AND i.part_ID = ps.ID
    //                         LEFT JOIN test_types tt ON tt.ID = i.test_type
    //                     WHERE
    //                         wpsh.wip_part_ID = ".$_REQUEST['wip_part_ID']."
    //                         AND s.station_order = ".$station['station_order']."
    //                     ORDER BY
    //                         wpsh.instruction_step_order ASC;";

    //             $station['instruction_history'] = $db->query($sql);
    //         }

    //         $sql = "SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wip_part_process_data'";
    //         $table_data = $db->query($sql);
    //         if ($table_data) {
    //             // get process data
    //             $sql = "SELECT
    //                         wppd.ID,
    //                         wppd.process_location,
    //                         wppi.description AS process_desc,
    //                         wppd.process_value_desc,
    //                         wppd.process_value,
    //                         wppd.process_timestamp
    //                     FROM
    //                         MESAAS_wip_parts w
    //                         JOIN wip_part_process_data wppd ON wppd.wip_part_ID = w.ID
    //                         JOIN wip_part_process_index wppi ON
    //                             wppi.ID = wppd.process_index
    //                             AND wppd.process_location LIKE wppi.process_location 
    //                     WHERE
    //                         w.ID = ".$_REQUEST['wip_part_ID']."
    //                     ORDER BY
    //                         wppd.process_location ASC, wppd.ID ASC;";

    //             $res->process_data = $db->query($sql);
    //         }

    //     } else {
        
    //         // module info
    //         $sql = "SELECT TOP 1
    //                     m.ID,
    //                     m.build_order,
    //                     m.sequence,
    //                     m.VIN,
    //                     l.line_code
    //                 FROM
    //                     wip_parts w
    //                     LEFT JOIN modules m ON w.used_by_module_ID = m.ID
    //                     LEFT JOIN line_stations ls ON ls.id = w.last_completed_station_ID
    //                     LEFT JOIN lines l ON l.id = m.line_id
    //                 WHERE
    //                     w.ID = ".$_REQUEST['wip_part_ID'].";";

    //         $res->module_info = $db->query($sql);
            

    //         // wip part info
    //         $sql = "SELECT TOP 1
    //                     w.ID,
    //                     ls.station_desc AS last_completed_station_desc,
    //                     ISNULL(w.internal_serial, w.external_serial) as serial_number,
    //                     w.built_time,
    //                     w.last_completed_station_ID,
    //                     l.line_code,
    //                     wps.part_desc
    //                 FROM wip_parts w
    //                     LEFT JOIN modules m ON w.used_by_module_ID = m.ID
    //                     LEFT JOIN line_stations ls ON ls.id = w.last_completed_station_ID
    //                     LEFT JOIN lines l ON l.id = ls.line_id
    //                     LEFT JOIN wip_part_setup wps ON wps.line_id = ls.line_id 
    //                 WHERE
    //                     w.ID = ".$_REQUEST['wip_part_ID'].";";

    //         $res->wip_part_info = $db->query($sql);


    //         // get wip station history
    //         $sql = "SELECT
    //                     wpsh.wip_part_ID,
    //                     ls.station_desc,
    //                     wpsh.entry_time,
    //                     wpsh.exit_time,
    //                     op.name AS operator_name,
    //                     wpsh.station_ID
    //                 FROM
    //                     wip_part_station_history wpsh
    //                     LEFT JOIN operators op ON op.id = wpsh.operator_id
    //                     LEFT JOIN (SELECT DISTINCT station_ID, station_desc FROM line_stations) ls ON ls.station_id = wpsh.station_id
    //                 WHERE
    //                     wip_part_ID = ".$_REQUEST['wip_part_ID'].";";

    //         $res->station_history = (array) $db->query($sql);


            
    //         foreach($res->station_history as $j => &$station) {
    //             // get wip instruction history
    //             $sql = "SELECT DISTINCT
    //                         wpsih.ID,
    //                         ls.station_id,
    //                         wpsih.step_order,
    //                         tt.type_desc,
    //                         wpsih.entry_time,
    //                         wpsih.exit_time,
    //                         wpsih.completion_details
    //                     FROM
    //                         wip_part_station_instruction_history wpsih
    //                         LEFT JOIN
    //                             instructions i ON i.part_number = wpsih.part_number
    //                             AND i.instr_order = wpsih.step_order
    //                             AND i.station_id = wpsih.station_id
    //                             AND i.part_order = wpsih.part_order
    //                         LEFT JOIN
    //                             test_types tt ON tt.ID = i.test_type
    //                         LEFT JOIN
    //                             (SELECT DISTINCT station_ID FROM line_stations) AS ls ON ls.station_id = wpsih.station_id
    //                     WHERE
    //                         wpsih.wip_part_ID = ".$_REQUEST['wip_part_ID']."
    //                         AND wpsih.station_id = ".$station['station_ID']."
    //                     ORDER BY wpsih.step_order;";

    //             $station['instruction_history'] = $db->query($sql);

    //             // get machine data
    //             $sql = "SELECT
    //                         *
    //                     FROM
    //                         machine_cycle_part_statuses mcps
    //                         LEFT JOIN machine_cycles mc ON mc.ID = mcps.cycle_ID
    //                         LEFT JOIN MESAAS_wip_parts wp ON wp.ID = mcps.wip_part_ID
    //                     WHERE
    //                         mcps.wip_part_ID = {$_REQUEST['wip_part_ID']}
    //                         AND mc.sation_ID = {$_REQUEST['station_ID']};";

    //             $station['machine_cycle_data'] = $db->query($sql);
    //         }

    //         $sql = "SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wip_part_process_data'";
    //         $table_data = $db->query($sql);
    //         if ($table_data) {
    //             // get process data
    //             $sql = "SELECT
    //                         ID,
    //                         process_location,
    //                         REPLACE(REPLACE(SUBSTRING(process_tag, CHARINDEX('.', process_tag), LEN(process_tag)), '.', ''), '_', ' ') AS process_desc,
    //                         process_value_desc,
    //                         process_value,
    //                         process_timestamp
    //                     FROM
    //                         wip_part_process_data
    //                     WHERE
    //                         wip_part_ID = ".$_REQUEST['wip_part_ID']."
    //                     ORDER BY
    //                         process_location ASC, ID ASC;";

    //             $res->process_data = $db->query($sql);
    //         }

    //     }

    //     echo json_encode($res);
    // }

?>