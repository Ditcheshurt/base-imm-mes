<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);
	

	function get_wip_parts ($request, $db) {
		if ($request['status'] === null || $request['status'] < 0) {
			$status = '%';
		} else {
			$status = $request['status'];
		}

		if ($request['line_ID'] === null || $request['line_ID'] < 0) {
			$line_ID = '%';
		} else {
			$line_ID = $request['line_ID'];
		}

		$sql = "SELECT
					wp.ID,
					wp.external_serial,
					ps.part_number,
					ps.part_desc,
					c.cell_desc AS line_desc,
					wp.last_station_completed AS last_station_completed_ID,
					s.station_desc AS last_station_completed_desc,
					wp.created_at
				FROM MESAAS_wip_parts wp
					LEFT JOIN part_setup ps ON ps.ID = wp.part_ID
					LEFT JOIN cells c ON c.ID = ps.cell_ID
					LEFT JOIN stations s ON s.ID = wp.last_station_completed
				WHERE
					c.ID LIKE '{$line_ID}'
					AND wp.created_at >= '{$request['start_date']} 00:00:00'
					AND wp.created_at <= '{$request['end_date']} 23:59:59.99'
					AND CASE WHEN wp.rejected_at IS NOT NULL THEN 9 ELSE 1 END LIKE '$status';";
		//die($sql);
		$result = $db->query($sql);
		if ($result === null) {
			$result = array();
		}
		echo json_encode($result);
	}
	
?>