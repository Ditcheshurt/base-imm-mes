<?php

	require_once("../init.php");

  	$r = $_REQUEST;
	$action = $r['action'];
	
	if ($action == 'tester_history') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$group_by = $r['group_by'];
		$station_id = $r['station_id'] == null || $r['station_id'] == -1 ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		$sql = "SELECT 
					th.ID 
					, m.build_order
					, m.sequence
					, s.station_desc + ' ' + th.station_instance AS station_desc														
					, CONVERT(VARCHAR(19), start_time, 121) AS local_start_date 
					, CASE WHEN EXISTS(SELECT passed FROM EOL_test_history_detail WHERE test_history_ID = th.ID AND passed = 0) THEN 'FAILED' ELSE 'PASSED' END as result
				FROM 
					EOL_test_history th 
					JOIN line_stations s ON s.ID = th.station_id 
					JOIN EOL_test_history_detail thd ON thd.test_history_ID = th.ID	
					LEFT JOIN modules m ON m.ID = th.module_ID
					LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
				WHERE 
					start_time >= '{$start_date} 00:00:00'
					AND start_time <= '{$end_date} 23:59:59'
					AND dbo.getShift({$line_id}, start_time) LIKE '{$shift}'
					AND th.station_ID LIKE '{$station_id}'
					AND mm.model LIKE '{$line_model}'
				GROUP BY
					th.ID 
					, m.build_order
					, m.sequence
					, s.station_desc													
					, start_time
					, th.station_instance
				ORDER BY start_time DESC";

		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
		
	} else if ($action == 'test_history_detail') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$test_ID = $r['test_ID'];
		
		$sql = "SELECT 
					*
					, CASE WHEN passed = 1 THEN 'PASSED' ELSE 'FAILED' END AS formatted_passed 
				FROM 
					EOL_test_history_detail 
				WHERE 
					test_history_ID = {$test_ID}
				ORDER BY ID DESC";
		
		$res = $db->query($sql);
		echo json_encode($res);
		
	} else if ($action == 'top_failures') {
		
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$group_by = $r['group_by'];
		$station_id = $r['station_id'] == null || $r['station_id'] == -1 ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		$sql = "SELECT TOP 10
					COUNT(thd.ID) AS failures
					, CONCAT(thd.test_type, ' - ', thd.test_item) AS test_desc
				FROM
					EOL_test_history_detail thd
					JOIN EOL_test_history th ON thd.test_history_ID = th.ID
					LEFT JOIN modules m ON m.ID = th.module_ID
					LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
				WHERE 
					th.start_time >= '{$start_date} 00:00:00'
					AND th.start_time <= '{$end_date} 23:59:59'
					AND dbo.getShift({$line_id}, th.start_time) LIKE '{$shift}'
					AND th.station_ID LIKE '{$station_id}'
					AND mm.model LIKE '{$line_model}'
					AND passed = 0
				GROUP BY
					test_type,
					test_item
				ORDER BY
					1 DESC";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);

	} else if ($action == 'tester_cycles') {
	
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$group_by = $r['group_by'];
		$station_id = $r['station_id'] == null || $r['station_id'] == -1 ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		$sql = "SELECT 	
					CASE WHEN station_instance IS NULL OR station_instance = '' THEN CONVERT(VARCHAR, th.station_ID) ELSE station_instance END AS station
					, COUNT(th.id) AS cycles 
					, SUM(res.failed) AS failed 
				FROM
					EOL_test_history th
					LEFT JOIN (
						SELECT DISTINCT
							td.test_history_ID
							, 1 AS failed
						FROM 
							EOL_test_history_detail td 
							JOIN EOL_test_history th ON th.ID = td.test_history_ID
						WHERE 
							th.start_time >= '{$start_date} 00:00:00'
							AND th.start_time <= '{$end_date} 23:59:59'
							AND dbo.getShift({$line_id}, th.start_time) LIKE '{$shift}'
							AND passed = 0
					) as res ON res.test_history_ID = th.id
					LEFT JOIN modules m ON m.ID = th.module_ID
					LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
				WHERE
					th.start_time >= '{$start_date} 00:00:00'
					AND th.start_time <= '{$end_date} 23:59:59'
					AND dbo.getShift({$line_id}, th.start_time) LIKE '{$shift}'
					AND mm.model LIKE '{$line_model}'
				GROUP BY
					station_instance,
					th.station_ID										
				ORDER BY
					station";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	
?>