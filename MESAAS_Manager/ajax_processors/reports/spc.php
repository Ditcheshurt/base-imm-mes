<?php

    require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;

	if ($action == 'get_tools') {
		$sql = "SELECT t.*
				  FROM tools t
					  JOIN machine_tools mt ON t.ID = mt.tool_ID AND mt.machine_ID = ".$r['machine_ID']."
				  ORDER BY t.tool_description ASC";
		echo json_encode($db->query($sql));
	}

	if ($action == 'get_subtools') {
		$sql = "SELECT p.part_desc, tp.ID
				  FROM parts p
					  JOIN tool_parts tp ON p.ID = tp.part_ID AND tp.tool_ID = ".$r['tool_ID']."
				  ORDER BY p.part_desc ASC";
		echo json_encode($db->query($sql));
	}

	if ($action == 'get_data_points') {
		$sql = "SELECT i.ID, gmt.description, i.description as instruction_description, 0 as auto_gauge, 0 as use_mmq,
					  ISNULL(i.tolerance_target, 0) as tolerance_target, ISNULL(tolerance_delta, 0) as tolerance_delta,
					  ISNULL(i.xbar_ucl, 0) as xbar_ucl, ISNULL(i.xbar_lcl, 0) as xbar_lcl, ISNULL(i.range_ucl, 0) as range_ucl, ISNULL(i.range_lcl, 0) as  range_lcl
				  FROM gauge_material_types gmt
					  JOIN gauge_material_types_instructions i ON gmt.ID = i.gauge_material_type_ID AND i.is_pass_fail = 0
				  WHERE gmt.tool_part_ID = ".$r['subtool_ID']."
				  UNION
				  SELECT 0 - ID, CASE WHEN use_mmq = 0 THEN 'AUTO' ELSE 'MMQ' END, data_point_column, 1, use_mmq,
				  	  tolerance_target, tolerance_delta,
				  	  xbar_ucl, xbar_lcl, range_ucl, range_lcl
				  FROM spc_autogauge_data_points
				  ORDER BY gmt.description ASC, i.description";
		echo json_encode($db->query($sql));
	}

	if ($action == "run_report") {
		$res = new stdClass();
		$sql = "SELECT -1";
		if ($r['auto_gauge'] == 1) {
			$use_mmq = 0;			
			if (isset($r['use_mmq'])) {
				$use_mmq = $r['use_mmq'];
			}
			if ($use_mmq == 0) {
				$sql = "SELECT TOP 125 g.ID, mc.cycle_time, g.".$r['data_point_desc']." as data_point_value, ISNULL(mcp.serial_number, '') as serial_number, g.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						  FROM gauge_autogauge g
							 JOIN machine_cycles mc ON g.cycle_ID = mc.ID
							 JOIN machines m ON mc.machine_ID = m.ID
							 LEFT OUTER JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
						  WHERE mc.cycle_time >= '".$r['start_date']." 00:00:00.000' AND mc.cycle_time < '".$r['end_date']." 23:59:59.999'
							  AND mc.tool_ID = ".$r['tool_ID']."
						  ORDER BY mc.cycle_time DESC"
						  ;
			} else {
				// litchfield hack as the mmq stored it on OP10 not OP40
				$tool_ID = null;
				if ($r['tool_ID'] == 102) {
					$tool_ID = 1;
				}
				if ($r['tool_ID'] == 115) {
					$tool_ID = 112;
				}
				$sql = "SELECT TOP 125 g.ID, mc.cycle_time, g.[".$r['data_point_desc']."] as data_point_value, ISNULL(mcp.serial_number, '') as serial_number, g.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						  FROM v_spc_mmq_data g
							 JOIN machine_cycles mc ON g.cycle_ID = mc.ID
							 --JOIN machines m ON mc.machine_ID = m.ID
							 LEFT OUTER JOIN machine_cycle_parts mcp ON mc.ID = mcp.cycle_ID
						  WHERE mc.cycle_time >= '".$r['start_date']." 00:00:00.000' AND mc.cycle_time < '".$r['end_date']." 23:59:59.999'
							  AND mc.tool_ID = $tool_ID
						  ORDER BY mc.cycle_time DESC"
						  ;
			}

		} else {
			$sql = "SELECT TOP 125 gmcp.ID, mc.cycle_time, gmcp.data_point_value, ISNULL(mcp.serial_number, '') as serial_number, gmcp.comment, ROW_NUMBER() OVER(ORDER BY mc.cycle_time DESC) AS Row
						FROM gauge_machine_cycle_parts gmcp
							JOIN machine_cycle_parts mcp ON gmcp.machine_cycle_part_ID = mcp.ID
							JOIN machine_cycles mc ON mcp.cycle_ID = mc.ID
							JOIN gauge_material_types_instructions gmti ON gmcp.gauge_material_types_instruction_ID = gmti.ID AND gmti.is_pass_fail = 0
						WHERE mc.cycle_time >= '".$r['start_date']." 00:00:00.000' AND mc.cycle_time < '".$r['end_date']." 23:59:59.999'
							AND mc.tool_ID = ".$r['tool_ID']."
							AND gmti.ID = ".$r['data_point_ID']."
						ORDER BY mc.cycle_time DESC";
		}

		//echo $sql;
		$res->query = $sql;
		$res->results = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == "save_control_limits") {
		$res = new stdClass();

		if ($r['auto_gauge'] == "1") {
			$sql = "UPDATE spc_autogauge_data_points
					  SET xbar_ucl = ".$r['tmp_ucl_xbar'].",
					  		xbar_lcl = ".$r['tmp_lcl_xbar'].",
					  		range_ucl = ".$r['tmp_ucl_range'].",
					  		range_lcl = ".$r['tmp_lcl_range']."
					  WHERE ID = 0 - ".$r['data_point_ID'];
		} else {
			$sql = "UPDATE gauge_material_types_instructions
					  SET xbar_ucl = ".$r['tmp_ucl_xbar'].",
					  		xbar_lcl = ".$r['tmp_lcl_xbar'].",
					  		range_ucl = ".$r['tmp_ucl_range'].",
					  		range_lcl = ".$r['tmp_lcl_range']."
					  WHERE ID = ".$r['data_point_ID'];
		}

		$db->query($sql);

		$res->query = $sql;
		$res->success = 1;
		echo json_encode($res);
	}

	if ($action == "save_target") {
		$res = new stdClass();

		if ($r['auto_gauge'] == "1") {
			$sql = "UPDATE spc_autogauge_data_points
					  SET tolerance_target = ".$r['tolerance_target'].",
					  		tolerance_delta = ".$r['tolerance_delta']."
					  WHERE ID = 0 - ".$r['data_point_ID'];
		} else {
			$sql = "UPDATE gauge_material_types_instructions
					  SET tolerance_target = ".$r['tolerance_target'].",
					  		tolerance_delta = ".$r['tolerance_delta']."
					  WHERE ID = ".$r['data_point_ID'];
		}

		$db->query($sql);

		$res->query = $sql;
		$res->success = 1;
		echo json_encode($res);
	}

	if ($action == "save_comment") {
		$sql = "UPDATE gauge_machine_cycle_parts SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
		if ($r['auto_gauge'] == "1") {
			if ($r['use_mmq'] == "0") {
				$sql = "UPDATE gauge_autogauge SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
			} else {
				$sql = "UPDATE gauge_mahrmmq SET comment = '".$r['comment']."' WHERE ID = ".$r['record_ID'];
			}
		}
		$db->query($sql);

		$res = new stdClass();
		$res->query = $sql;
		$res->success = 1;

		echo json_encode($res);
	}

?>