<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "load_categories") {
		loadCategories();
	}

	if ($action == "run_report") {
		$r = $_REQUEST;
		$res = new StdClass();

		$sql = "SELECT COUNT(*) as num_items, AVG(DATEDIFF(SECOND, entry_time, ack_time) / 60.0) as avg_ack, AVG(DATEDIFF(SECOND, entry_time, closed_time) / 60.0) as avg_close
				  FROM TEAM_WMS.dbo.ANDON_queue q
				  WHERE category_ID = ".$r['category_ID']."
				  	  AND entry_time >= '".$r['start']."'
				  	  AND entry_time <= '".$r['end']." 23:59:59'
				  	  AND closed_time IS NOT NULL";
		$res->summary = $db->query($sql);

		$sql = "SELECT q.*, c.category_name, c.select_part,
					l.line_code, ls.station_desc, o.name as enterer, o2.name as acker,
					DATEDIFF(SECOND, entry_time, ack_time)/60.0 as ack_delta,
					DATEDIFF(SECOND, entry_time, material_pick_time)/60.0 as pick_delta,
					DATEDIFF(SECOND, entry_time, closed_time)/60.0 as closed_delta,
					p.part_number, p.part_desc, p.pick_location, p.target_location,
					ISNULL(r.reason_desc, 'UNKNOWN') as reason_desc
				  FROM TEAM_WMS.dbo.ANDON_queue q
				  	  JOIN TEAM_WMS.dbo.ANDON_categories c ON q.category_ID = c.ID
				  	  LEFT OUTER JOIN TEAM_MES.dbo.lines l ON q.line_ID = l.ID
				  	  LEFT OUTER JOIN TEAM_MES.dbo.line_stations ls ON ls.line_ID = q.line_ID AND q.station_ID = ls.station_ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o ON q.entry_operator_ID = o.ID
				  	  LEFT OUTER JOIN MES_COMMON.dbo.operators o2 ON q.ack_operator_ID = o2.ID
				  	  LEFT OUTER JOIN TEAM_WMS.dbo.ANDON_parts p ON q.material_part_ID = p.ID
				  	  LEFT OUTER JOIN TEAM_WMS.dbo.ANDON_reasons r ON q.reason_ID = r.ID
				  WHERE q.category_ID = ".$r['category_ID']."
				  	  AND q.closed_time IS NOT NULL
				  	  AND q.entry_time >= '".$r['start']."'
				  	  AND q.entry_time <= '".$r['end']." 23:59:59'
				  ORDER BY q.entry_time";
		$res->detail = $db->query($sql);
		echo json_encode($res);
	}

	function loadCategories() {
		global $db;

		$res = new StdClass();

		$sql = "SELECT * FROM TEAM_WMS.dbo.ANDON_categories ORDER BY category_name";
		$res->categories = $db->query($sql);

		echo json_encode($res);
	}
?>