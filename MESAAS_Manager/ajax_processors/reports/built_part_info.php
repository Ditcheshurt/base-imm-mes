<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];


    $sys = new StdClass();
    $sql = "SELECT TOP 1
                database_name
            FROM
                MES_COMMON.dbo.systems
            WHERE
                system_active = 1 AND process_type = 'BATCH';";
    $sys = $db->query($sql);
    

    //die(var_dump($sys));
    $db->disconnect();

    $db->catalog = $sys[0]['database_name'];
    $db->connect();

    
    if ($action == 'load_data') {
        global $db;
        $res = new StdClass();

        // part info
        $sql = "SELECT TOP 1
                    bp.ID,
                    ISNULL(bp.serial, bp.external_serial) AS serial_number,
                    bp.serial,
                    bp.external_serial,
                    bp.built_time,
                    ps.part_number,
                    ps.internal_part_number,
                    ps.shipping_part_number,
                    ps.part_desc,
                    c.cell_desc,
                    s.station_order AS last_completed_station_ID,
                    s.station_desc AS last_completed_station_desc
                FROM built_parts bp
                    LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
                    LEFT JOIN cells c ON c.ID = ps.cell_ID
                    LEFT JOIN stations s ON s.station_order = bp.last_station_ID
                WHERE
                    bp.ID = ".$_REQUEST['built_part_id'].";";

        $res->built_part_info = $db->query($sql);
        
        // get station instruction history
        $sql = "SELECT DISTINCT
                    s.ID,
                    s.station_order,
                    s.station_desc
                FROM
                    stations s
                    LEFT JOIN built_part_history bph ON bph.cell_ID = s.cell_ID AND bph.station_ID = s.station_order
                WHERE
                    bph.built_part_ID = ".$_REQUEST['built_part_id']."
                ORDER BY
                    s.station_order ASC;";

        $res->station_history = (array) $db->query($sql);
        
        foreach($res->station_history as $j => &$station) {
            // get station instruction history
            $sql = "SELECT
                        bph.ID,
                        bph.entry_time,
                        bph.exit_time,
                        op.name AS operator_name,
                        bph.step_order,
                        bph.completion_details,
                        tt.type_desc,
                        i.text AS instruction_text
                    FROM
                        built_part_history bph
                        LEFT JOIN stations s ON  s.cell_ID = bph.cell_ID AND s.station_order = bph.station_ID
                        LEFT JOIN MES_COMMON.dbo.operators op ON op.ID = bph.operator
                        LEFT JOIN instructions i ON
                            i.cell_ID = bph.cell_ID
                            AND i.station_order = bph.station_ID
                            AND i.instr_order = bph.step_order
                        LEFT JOIN test_types tt ON tt.ID = i.test_type
                    WHERE
                        bph.built_part_ID = ".$_REQUEST['built_part_id']."
                        AND s.station_order = ".$station['station_order']."
                    ORDER BY
                        bph.step_order ASC;";

            $station['instruction_history'] = $db->query($sql);
        }

        $sql = "SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wip_part_process_data'";
        $table_data = $db->query($sql);
        if ($table_data) {
            // get process data
            $sql = "SELECT
                        wppd.ID,
                        wppd.process_location,
                        wppi.description AS process_desc,
                        wppd.process_value_desc,
                        wppd.process_value,
                        wppd.process_timestamp
                    FROM
                        built_parts bp
                        JOIN wip_part_process_data wppd ON wppd.wip_part_ID = bp.wip_ID
                        JOIN wip_part_process_index wppi ON
                            wppi.ID = wppd.process_index
                            AND wppd.process_location LIKE wppi.process_location 
                    WHERE
                        bp.ID = ".$_REQUEST['built_part_id']."
                    ORDER BY
                        wppd.process_location ASC, wppd.ID ASC;";

            $res->process_data = $db->query($sql);
        }

        echo json_encode($res);
    }

?>