<?php

	require_once("../init.php");

	$r = $_REQUEST;
	$action = $r['action'];
	
	// if ($action == 'lines') {
	// 	global $db;
	// 	$res = new StdClass();
	// 	$r = $_REQUEST;
		
	// 	$sql = "SELECT id, line_code FROM lines WHERE line_type = 0";
		
	// 	$res = $db->query($sql);
	// 	echo json_encode($res);
	// }

	if ($action == "broadcast_summary") {
		global $db;
		$res = new StdClass();
		
		// broadcast data
		$sql = "SELECT 
				(SELECT COUNT(*) FROM modules WHERE line_ID = ".$r["line_id"]." AND recvd_time >= convert(varchar(10), GETDATE(), 120)) as broadcasts_today,
				(SELECT MAX(recvd_time) FROM modules WHERE line_ID = ".$r["line_id"].") as last_broadcast,
				SUM(CASE WHEN status = 0 AND line_ID = ".$r["line_id"]." THEN 1 ELSE 0 END) as broadcasts_inqueue, 
				SUM(CASE WHEN status = 1 AND line_ID = ".$r["line_id"]." THEN 1 ELSE 0 END) as broadcasts_inbuild,
				(SELECT MAX(built_time) FROM modules WHERE line_ID = ".$r["line_id"]." AND built_time IS NOT NULL) as broadcast_lastbuild,
				(SELECT COUNT(*) FROM modules WHERE line_ID = ".$r["line_id"]." AND built_time >= convert(varchar(10), GETDATE(), 120)) as broadcasts_builttoday
			FROM modules WHERE status < 2";
		
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == "daily_production_summary") {
		global $db;
		$res = new StdClass();

		// production qty
		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT 
						CONVERT(VARCHAR(10), dbo.getProdDate(built_time), 120) AS built_date
						, SUM(CASE WHEN dbo.getShift(built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]	
						, SUM(CASE WHEN dbo.getShift(built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]	
						, SUM(CASE WHEN dbo.getShift(built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE ps.cell_ID = ".$r["line_id"]."
						AND dbo.getProdDate(built_time) >= '".$r['start_date']." 00:00:00'
						AND dbo.getProdDate(built_time) <= '".$r['end_date']." 23:59:59'
					GROUP BY
						dbo.getProdDate(built_time)
					ORDER BY
						built_date ASC;";
		} else if ($r['process_type'] == 'SEQUENCED') {
			$sql = "SELECT * FROM (
						(
							SELECT 
								CONVERT(VARCHAR(10), dbo.getProdDate('".$r["line_id"]."',built_time), 120) AS built_date
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
							FROM modules JOIN lines ON line_id = lines.id
							WHERE line_ID LIKE '".$r["line_id"]."'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) >= '".$r['start_date']." 00:00:00'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) <= '".$r['end_date']." 23:59:59'
							GROUP BY dbo.getProdDate('".$r["line_id"]."',built_time)
						) UNION (
							SELECT
								CONVERT(VARCHAR(10), dbo.getProdDate('".$r["line_id"]."',built_time), 120) AS built_date
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]
								, SUM(CASE WHEN dbo.getShift(line_ID, built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
							FROM wip_parts JOIN lines ON line_id = lines.id
							WHERE line_ID LIKE '".$r["line_id"]."'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) >= '".$r['start_date']." 00:00:00'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) <= '".$r['end_date']." 23:59:59'
							GROUP BY dbo.getProdDate('".$r["line_id"]."',built_time)
						)
					) AS prod;";
					
		} else if ($r['process_type'] == 'REPETITIVE') {
			$sql = "
				SELECT 
					CONVERT(VARCHAR(13), dbo.getproddate(mc.cycle_time), 101) AS built_date,
					SUM(CASE WHEN dbo.getShift(m.system_ID, mc.cycle_time) = 1 THEN 1 ELSE 0 END) AS first_shift,
					SUM(CASE WHEN dbo.getShift(m.system_ID, mc.cycle_time) = 2 THEN 1 ELSE 0 END) AS second_shift,
					SUM(CASE WHEN dbo.getShift(m.system_ID, mc.cycle_time) = 3 THEN 1 ELSE 0 END) AS third_shift
				FROM machine_cycles AS mc
					JOIN machine_cycle_parts AS mcp ON mc.ID = mcp.cycle_ID
					JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				WHERE					
					dbo.getproddate(mc.cycle_time) >= '".$r['start_date']." 00:00:00'
					AND dbo.getproddate(mc.cycle_time) <= '".$r['end_date']." 23:59:59'
			";
			/*"SELECT
						CONVERT(VARCHAR(10), the_date, 120) AS built_date,
						SUM(CASE WHEN the_shift = 1 THEN part_qty ELSE 0 END) AS first_shift,
						SUM(CASE WHEN the_shift = 2 THEN part_qty ELSE 0 END) AS second_shift,
						SUM(CASE WHEN the_shift = 3 THEN part_qty ELSE 0 END) AS third_shift
					FROM
						shift_summary
					WHERE
						system_ID = ".$r['system_id']."
						AND machine_ID = ".$r['machine_id']."
						AND the_date >= '".$r['start_date']." 00:00:00'
						AND the_date <= '".$r['end_date']." 23:59:59'
					";*/
			if (isset($r['machine_id']) && $r['machine_id'] >= 0) {
				$sql .= " AND mc.machine_ID = ".$r['machine_id'];
			}
			if (isset($r['shift_id']) && $r['shift_id'] >= 0) {
				$sql .= " AND dbo.getShift(m.system_ID, mc.cycle_time) = ".$r['shift_id'];
			}
			if (isset($r['tool_id']) && $r['tool_id'] >= 0) {
				$sql .= " AND tp.tool_ID = ".$r['tool_id'];
			}
			if (isset($r['part_id']) && $r['part_id'] >= 0) {
				$sql .= " AND mcp.tool_part_ID = ".$r['part_id'];
			}
			if (isset($r['operator_id']) && $r['operator_id'] >= 0) {
				$sql .= " AND mc.operator_id = ".$r['operator_id'];
			}
			$sql .= " GROUP BY dbo.getproddate(mc.cycle_time) ORDER BY built_date;";
		}
    //die($sql);
		
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "hourly_production_summary") {
		global $db;
		$res = new StdClass();
		
		if ($r['process_type'] === 'REPETITIVE') {
			$sql = "SELECT 
					DATEPART(HOUR, cycle_time) AS hour,
					SUM(1) / CASE WHEN DATEDIFF(dd, '".$r['start_date']." 00:00:00', '".$r['end_date']." 23:59:00') > 0 THEN DATEDIFF(dd, '".$r['start_date']." 00:00:00', '".$r['end_date']." 23:59:00') ELSE 1 END AS qty_built
				FROM machine_cycles AS mc
					JOIN machine_cycle_parts AS mcp ON mc.ID = mcp.cycle_ID
					JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				WHERE				
					dbo.getproddate(mc.cycle_time) >= '".$r['start_date']." 00:00:00'
					AND dbo.getproddate(mc.cycle_time) <= '".$r['end_date']." 23:59:59'
			";
			if (isset($r['machine_id']) && $r['machine_id'] >= 0) {
				$sql .= " AND mc.machine_ID = ".$r['machine_id'];
			}
			if (isset($r['shift_id']) && $r['shift_id'] >= 0) {
				$sql .= " AND dbo.getShift(cycle_time) = ".$r['shift_id'];
			}
			if (isset($r['tool_id']) && $r['tool_id'] >= 0) {
				$sql .= " AND tp.tool_ID = ".$r['tool_id'];
			}
			if (isset($r['part_id']) && $r['part_id'] >= 0) {
				$sql .= " AND mcp.tool_part_ID = ".$r['part_id'];
			}
			if (isset($r['operator_id']) && $r['operator_id'] >= 0) {
				$sql .= " AND mc.operator_id = ".$r['operator_id'];
			}
			$sql .= " GROUP BY DATEPART(HOUR, cycle_time)";
		
		}
		
	//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "production_detail") {
		global $db;
		$res = new StdClass();	
		// production qty
		if ($r['process_type'] === 'BATCH') {
			$sql = "SELECT
						--CONVERT(VARCHAR(10), built_time, 120) AS built_date,
						DATEPART(HOUR, built_time) AS [hour]
						, COUNT(built_time) / 
							CASE WHEN DATEDIFF(DD, MIN(built_time),MAX(built_time)) = 0 
								THEN 1 
								ELSE DATEDIFF(DD, MIN(built_time),MAX(built_time)) + 1 
							END AS qty_built
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE c.ID = ".$r["line_id"]."
						AND dbo.getProdDate(built_time) >= '".$r['start_date']." 00:00:00'
						AND dbo.getProdDate(built_time) <= '".$r['end_date']." 23:59:59'
					GROUP BY DATEPART(HOUR, built_time)
					ORDER BY [hour] ASC;";

		} else if ($r['process_type'] === 'SEQUENCED') {
			$sql = "SELECT * FROM (
						(
							SELECT
								--CONVERT(VARCHAR(10), built_time, 120) AS built_date,
								DATEPART(HOUR, built_time) AS [hour]
								, COUNT(built_time) / 
									CASE WHEN DATEDIFF(DD, MIN(built_time),MAX(built_time)) = 0 
										THEN 1 
										ELSE DATEDIFF(DD, MIN(built_time),MAX(built_time))+1 
									END AS qty_built
							FROM modules JOIN lines ON line_id = lines.id
							WHERE line_ID LIKE '".$r["line_id"]."'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) >= '".$r['start_date']." 00:00:00'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) <= '".$r['end_date']." 23:59:59'
							GROUP BY DATEPART(HOUR, built_time)		
						) UNION (
							SELECT
								--CONVERT(VARCHAR(10), built_time, 120) AS built_date,
								DATEPART(HOUR, built_time) AS [hour]
								, COUNT(built_time) / 
									CASE WHEN DATEDIFF(DD, MIN(built_time),MAX(built_time)) = 0 
										THEN 1 
										ELSE DATEDIFF(DD, MIN(built_time),MAX(built_time))+1 
									END AS qty_built
							FROM wip_parts JOIN lines ON line_id = lines.id
							WHERE line_ID LIKE '".$r["line_id"]."'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) >= '".$r['start_date']." 00:00:00'
								AND dbo.getProdDate('".$r["line_id"]."',built_time) <= '".$r['end_date']." 23:59:59'
							GROUP BY DATEPART(HOUR, built_time)
						)
					) AS prod
					ORDER BY [hour] ASC;";

		} else if ($r['process_type'] === 'REPETITIVE') {			
			$sql = "
				SELECT CONVERT(VARCHAR(13), cycle_time, 101) AS [Date], DATEPART(HOUR, cycle_time) AS [Hour],
					SUM(1) / CASE WHEN DATEDIFF(dd, '".$r['start_date']." 00:00:00', '".$r['end_date']." 23:59:00') > 0 THEN DATEDIFF(dd, '".$r['start_date']." 00:00:00', '".$r['end_date']." 23:59:00') ELSE 1 END AS [Qty_Built]
				FROM machine_cycles AS mc
					JOIN machine_cycle_parts AS mcp ON mc.ID = mcp.cycle_ID
					JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				WHERE					
					dbo.getproddate(mc.cycle_time) >= '".$r['start_date']." 00:00:00'
					AND dbo.getproddate(mc.cycle_time) <= '".$r['end_date']." 23:59:59'
			";
			if (isset($r['machine_id']) && $r['machine_id'] >= 0) {
				$sql .= " AND mc.machine_ID = ".$r['machine_id'];
			}
			if (isset($r['shift_id']) && $r['shift_id'] >= 0) {
				$sql .= " AND dbo.getShift(cycle_time) = ".$r['shift_id'];
			}
			if (isset($r['tool_id']) && $r['tool_id'] >= 0) {
				$sql .= " AND tp.tool_ID = ".$r['tool_id'];
			}
			if (isset($r['part_id']) && $r['part_id'] >= 0) {
				$sql .= " AND mcp.tool_part_ID = ".$r['part_id'];
			}
			if (isset($r['operator_id']) && $r['operator_id'] >= 0) {
				$sql .= " AND mc.operator_id = ".$r['operator_id'];
			}
			$sql .= " GROUP BY CONVERT(VARCHAR(13), cycle_time, 101), DATEPART(HOUR, cycle_time)";
			$sql .= " ORDER BY DATEPART(HOUR, cycle_time) ASC";
			
		}
       //die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "production_summary") {
		global $db;
		$res = new StdClass();	
		// production
		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						*
						, CONVERT(varchar, built_time, 101) + ' ' + CONVERT(varchar, built_time, 108) AS built_time_formatted
						, NULL AS VIN
						, NULL AS pallet_number
						, NULL AS pallet_position
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE c.ID = ".$r["line_id"]."
						AND dbo.getProdDate(built_time) >= '".$r['start_date']." 00:00:00'
						AND dbo.getProdDate(built_time) <= '".$r['end_date']." 23:59:59'					
					ORDER BY built_time DESC;";
			//die($sql);
		} else {
			$sql = "SELECT
						*
						, CONVERT(varchar, built_time, 101) + ' ' + CONVERT(varchar, built_time, 108) AS built_time_formatted
					FROM modules JOIN lines ON line_id = lines.id
					WHERE line_ID LIKE '".$r["line_id"]."'
						AND dbo.getProdDate('".$r["line_id"]."',built_time) >= '".$r['start_date']." 00:00:00'
						AND dbo.getProdDate('".$r["line_id"]."',built_time) <= '".$r['end_date']." 23:59:59'								
					ORDER BY built_time DESC;";
		}
		
      //die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "get_tools") {
		global $db;
		$res = new StdClass();	
		
		//$machine_ID = $r['machine_ID'];
		
		$sql = "
			SELECT 
				t.ID AS ID
				,t.tool_description AS text
			FROM
				tools t
			";
			
		if (isset($r['machine_ID']) && $r['machine_ID'] >= 0) {
			$sql .= " JOIN machine_tools mt ON mt.tool_ID = t.ID ";
			$sql .= " WHERE mt.machine_ID = ".$r['machine_ID'];
		}
		$sql .= " ORDER BY t.tool_description";
//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "drilldown") {
		global $db;
		$res = new StdClass();	
		
		$sql = "SELECT 
					m.machine_number + ' - ' + m.machine_name AS Machine
					,CONVERT(VARCHAR, mc.cycle_time, 101) + ' ' + CONVERT(VARCHAR, mc.cycle_time, 108) AS [Cycle]
					,mc.cycle_duration AS 'Cycle Duration'
					,p.part_number AS 'Part Number'
					,p.part_desc AS 'Part Desc'
					,o.name AS Operator
				FROM machine_cycles AS mc
					JOIN machine_cycle_parts AS mcp ON mc.ID = mcp.cycle_ID
					JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
					JOIN parts p ON p.ID = tp.part_ID
					JOIN machines m ON m.ID = mc.machine_ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.badge_ID = mc.operator_ID
				WHERE 1=1
			";
			
		if (isset($r['machine_id']) && $r['machine_id'] >= 0) {
			$sql .= " AND mc.machine_ID = ".$r['machine_id'];
		}
		if (isset($r['shift_id']) && $r['shift_id'] >= 0) {
			$sql .= " AND dbo.getShift(cycle_time) = ".$r['shift_id'];
		}
		if (isset($r['tool_id']) && $r['tool_id'] >= 0) {
			$sql .= " AND tp.tool_ID = ".$r['tool_id'];
		}
		if (isset($r['part_id']) && $r['part_id'] >= 0) {
			$sql .= " AND mcp.tool_part_ID = ".$r['part_id'];
		};
		if (isset($r['operator_id']) && $r['operator_id'] >= 0) {
			$sql .= " AND mc.operator_id = ".$r['operator_id'];
		};
		if(isset($r['date'])) {
			$sql .= " AND CONVERT(VARCHAR(13), mc.cycle_time, 101) = '".$r['date']."'";
		}
		if(isset($r['hour'])) {
			$sql .= " AND DATEPART(hh, mc.cycle_time) = '".$r['hour']."'";
		}
		
	//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "get_parts") {
		global $db;
		$res = new StdClass();	
		
		$sql = "
			SELECT DISTINCT
				tp.ID AS ID
				,p.part_number + ' - ' + p.part_desc AS text
				,p.part_number
				,p.part_desc
			FROM
				tool_parts tp 
				JOIN parts p ON p.ID = tp.part_ID ";

		if(isset($r['tool_ID']) && $r['tool_ID'] >= 0) {
			$sql .= "WHERE tp.tool_ID = ".$r['tool_ID'];
		}
		$sql .= " ORDER BY p.part_number, p.part_desc";

		$res = $db->query($sql);
		echo json_encode($res);
	}
	
?>