<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	
	if ($action == 'cycle_times') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$group_by = $r['group_by'];
		$station_id = $r['station_id'] == null || $r['station_id'] == -1 ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		//check for line_type		
		$sql = "SELECT line_type FROM MES_COMMON.dbo.v_MES_LINES 
				WHERE line_ID = ".$r['line_id'];
		$res = $db->query($sql);


		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id
						, CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station
						, bpsh.station_order
						, COUNT(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS num_cycles
						, AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS avg_cycle_time
						, bpsh.ID
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							o.ID,
							o.badge_ID,
							o.name
					) AS bpsh
					WHERE
						entry_time > '".$start_date." 00:00:00' 
						AND entry_time < '".$end_date." 23:59:59.99' 
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.operator_ID LIKE '".$operator_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5 
					##GROUPBY##
					ORDER BY
						bpsh.cell_ID,
						bpsh.ID;";
			
			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY bpsh.badge_ID,bpsh.name,bpsh.cell_ID,bpsh.station_order,bpsh.id,bpsh.station_desc";
				} else if ($group_by === "Date") {
					$sql = str_replace(", CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),bpsh.cell_ID,bpsh.station_order,bpsh.ID,bpsh.station_desc";			
				} else {
					$sql = str_replace(", CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY bpsh.cell_ID,bpsh.station_order,bpsh.ID,bpsh.station_desc";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}

		} else if ($r['process_type'] == 'SEQUENCED') {
			if ($res[0]['line_type'] == 0) {
				$sql = "SELECT s.line_id
						, CONCAT(s.id, ' : ', s.station_desc) AS station
						, s.station_order
						, COUNT(msh.cycle_time) AS num_cycles
						, AVG(msh.cycle_time) AS avg_cycle_time
						, s.id
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					FROM module_station_history msh 
					JOIN line_stations s ON s.id = msh.station_id 
						and s.line_id = msh.line_ID
					JOIN operators o ON o.id = msh.operator_id
					WHERE entry_time > '".$start_date." 00:00:00' 
						AND entry_time < '".$end_date." 23:59:59.99' 
						AND s.line_id LIKE '".$line_id."'
						AND o.id LIKE '".$operator_id."'
						AND s.id LIKE '".$station_id."'
						AND msh.[cycle_time] < 500 
						AND msh.[cycle_time] > 5 
						AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."' 
					##GROUPBY##
					ORDER BY s.line_id, s.id";
			
				if ($group_by != null && $group_by != "") {
					if ($group_by === "Operator") {
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY badge_id,name,s.line_id,s.station_order,s.id,s.station_desc";
					} else if ($group_by === "Date") {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),s.line_id,s.station_order,s.id,s.station_desc";			
					} else {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY s.line_id,s.station_order,s.id,s.station_desc";
					}
					$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
				}
				
			} else {
				$sql = "SELECT s.line_id
						, CONCAT(s.id, ' : ', s.station_desc) AS station
						, s.station_order
						, COUNT(msh.entry_time) AS num_cycles
						, ROUND(AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)), 2) AS avg_cycle_time
						, s.id
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					FROM wip_part_station_history msh 
					JOIN line_stations s ON s.id = msh.station_id 
						and s.line_id = msh.line_ID
					JOIN operators o ON o.id = msh.operator_id
					WHERE entry_time > '".$start_date." 00:00:00' 
						AND entry_time < '".$end_date." 23:59:59.99' 
						AND s.line_id LIKE ".$line_id."
						AND o.id LIKE '".$operator_id."'
						AND s.id LIKE '".$station_id."'
						AND DATEDIFF(ss, msh.entry_time, msh.exit_time) < 500 
						AND DATEDIFF(ss, msh.entry_time, msh.exit_time) > 5 
						AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
					##GROUPBY##
					ORDER BY s.line_id, s.id";
				
				if ($group_by != null && $group_by != "") {
					if ($group_by === "Operator") {
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY badge_id,name,s.line_id,s.station_order,s.id,s.station_desc";
					} else if ($group_by === "Date") {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),s.line_id,s.station_order,s.id,s.station_desc";			
					} else {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY s.line_id,s.station_order,s.id,s.station_desc";
					}
					$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
				}
			}
			//die($sql);
		} else if ($r['process_type'] == 'REPETITIVE') {
			
			$sql = "SELECT 
						m.machine_name
						, t.tool_description
						, COUNT(c.cycle_time) AS num_cycles
						, ROUND(AVG(c.cycle_duration), 2) AS avg_cycle_time
						, convert(varchar(10),c.cycle_time,101) AS entry_date
						, c.tool_ID
					  FROM 
						machine_cycles c
					  JOIN machines m 
						ON m.ID = c.machine_ID
					  JOIN tools t 
						ON t.ID = c.tool_ID
					WHERE dbo.getProdDate(c.cycle_time) >= '".$start_date."' 
						AND dbo.getProdDate(c.cycle_time) <= '".$end_date."'						
						--AND c.cycle_duration < 500 
						--AND c.cycle_duration > 5 
						AND dbo.getShift(c.cycle_time) LIKE '".$shift."'
						AND c.machine_ID = ".$r['machine_ID']."
						AND (c.operator_ID LIKE '".$operator_id."'  OR c.operator_ID IS NULL) 
						##GROUPBY##
					ORDER BY m.machine_name, convert(varchar(10),c.cycle_time,101) DESC";
			
			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", convert(varchar(10),c.cycle_time,101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY badge_id,name,m.machine_name, t.tool_description, c.tool_ID";
				} else if ($group_by === "Date") {
					//$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10),c.cycle_time,101),m.machine_name, t.tool_description, c.tool_ID";			
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}
		}

	    //die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'station_operator_cycle_times') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$station_id = $r['station_id'] == null ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		//check for line_type		
		$sql = "SELECT line_type FROM lines 
				WHERE id = ".$r['line_id'];
		$res = $db->query($sql);

		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id,
						CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station,
						bpsh.station_order,
						COUNT(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS num_cycles,
						AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS avg_cycle_time,
						bpsh.name,
						bpsh.ID
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							o.ID,
							o.badge_ID,
							o.name
					) AS bpsh
					WHERE
						entry_time > '".$start_date." 00:00:00' 
						AND entry_time < '".$end_date." 23:59:59.99' 
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5 
					GROUP BY
						bpsh.cell_ID,
						bpsh.station_order,
						bpsh.ID,
						bpsh.station_desc,
						bpsh.badge_ID,
						bpsh.name
					ORDER BY
						num_cycles DESC,
						bpsh.cell_ID ASC,
						bpsh.ID ASC;";
		} else {
		
			if ($res[0]['line_type'] == 0) {
				$sql = "SELECT s.line_id
							, CONCAT(msh.station_id, ' - ', s.station_desc) AS station
							, s.station_order
							, COUNT(msh.[cycle_time]) AS num_cycles
							, ROUND(AVG(msh.[cycle_time]), 2) AS avg_cycle_time
							, o.name
							, msh.station_id
						FROM module_station_history msh 
						JOIN line_stations s ON s.id = msh.station_id 
							and s.line_id = msh.line_ID
						JOIN operators o ON o.id = msh.operator_id
						WHERE entry_time > '".$start_date." 00:00:00' 
							AND entry_time < '".$end_date." 23:59:59.99' 
							AND s.line_id LIKE '".$line_id."'
							AND msh.station_id LIKE '".$station_id."'
							AND msh.[cycle_time] < 500 
							AND msh.[cycle_time] > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
						GROUP BY o.name,s.line_id,s.station_order,msh.station_id,s.station_desc
						ORDER BY num_cycles desc, s.line_id, msh.station_ID";	
			} else {
				$sql = "SELECT s.line_id
							, CONCAT(msh.station_id, ' - ', s.station_desc) AS station
							, s.station_order
							, COUNT(msh.[entry_time]) AS num_cycles
							, ROUND(AVG(DATEDIFF(ss, entry_time, exit_time)), 2) AS avg_cycle_time
							, o.name
							, msh.station_id
						FROM wip_part_station_history msh 
						JOIN line_stations s ON s.id = msh.station_id 
							and s.line_id = msh.line_ID
						JOIN operators o ON o.id = msh.operator_id
						WHERE entry_time > '".$start_date." 00:00:00' 
							AND entry_time < '".$end_date." 23:59:59.99' 
							AND s.line_id LIKE '".$line_id."'
							AND msh.station_id LIKE '".$station_id."'
							AND DATEDIFF(ss, entry_time, exit_time) < 500 
							AND DATEDIFF(ss, entry_time, exit_time) > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
						GROUP BY o.name,s.line_id,s.station_order,msh.station_id,s.station_desc
						ORDER BY num_cycles desc, s.line_id, msh.station_ID";
			
			}
		}
		//echo $sql; die;
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == 'cycle_times_detail') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$station_id = $r['station_id'] == null ? "%" : $r['station_id'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		//check for line_type		
		$sql = "SELECT line_type FROM lines 
				WHERE id = ".$r['line_id'];
		$res = $db->query($sql);

		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id,
						CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station,
						bpsh.station_order,
						bpsh.entry_time,
						DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) AS cycle_time,
						bpsh.name,
						bpsh.ID
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							o.ID,
							o.badge_ID,
							o.name
						) AS bpsh
					WHERE
						entry_time > '".$start_date." 00:00:00' 
						AND entry_time < '".$end_date." 23:59:59.99' 
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND bpsh.badge_ID LIKE '".$operator_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5 
					GROUP BY
						bpsh.cell_ID,
						bpsh.station_order,
						bpsh.ID,
						bpsh.station_desc,
						bpsh.badge_ID,
						bpsh.name,
						bpsh.entry_time,
                        bpsh.exit_time
					ORDER BY
						bpsh.entry_time DESC;";
		} else if ($r['process_type'] == 'SEQUENCED') {

			if ($res[0]['line_type'] == 0) {		
				$sql = "SELECT s.line_id					
							, CONCAT(msh.station_id, ' - ', s.station_desc) AS station
							, s.station_order
							, o.name
							, entry_time
							, msh.[cycle_time] AS cycle_time
							, msh.station_id
						FROM module_station_history msh 
						JOIN line_stations s ON s.id = msh.station_id 
							and s.line_id = msh.line_ID
						JOIN operators o ON o.id = msh.operator_id
						WHERE entry_time > '".$start_date." 00:00:00' 
							AND entry_time < '".$end_date." 23:59:59.99' 
							AND s.line_id LIKE '".$line_id."'
							AND msh.station_id LIKE '".$station_id."'
							AND o.badge_id LIKE '".$operator_id."'
							AND msh.[cycle_time] < 500 
							AND msh.[cycle_time] > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'				
						ORDER BY entry_time DESC";
			} else {
				$sql = "SELECT s.line_id					
							, CONCAT(msh.station_id, ' - ', s.station_desc) AS station
							, s.station_order
							, o.name
							, entry_time
							, ROUND(DATEDIFF(ss,entry_time,exit_time), 2) AS cycle_time
							, msh.station_id
						FROM wip_part_station_history msh 
						JOIN line_stations s ON s.id = msh.station_id 
							and s.line_id = msh.line_ID
						JOIN operators o ON o.id = msh.operator_id
						WHERE entry_time > '".$start_date." 00:00:00' 
							AND entry_time < '".$end_date." 23:59:59.99' 
							AND s.line_id LIKE '".$line_id."'
							AND msh.station_id LIKE '".$station_id."'
							AND o.badge_id LIKE '".$operator_id."'
							AND DATEDIFF(ss,entry_time,exit_time) < 500 
							AND DATEDIFF(ss,entry_time,exit_time) > 5
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
						ORDER BY entry_time DESC";
				
			}
		} else if ($r['process_type'] == 'REPETITIVE') {
			$sql = "SELECT 
						t.tool_description
						,[cycle_time] AS entry_time
						, ROUND([cycle_duration], 2) AS cycle_time
						, o.name AS name
					  FROM [machine_cycles] c
					  JOIN tools t on c.tool_ID = t.ID
					  LEFT JOIN MES_COMMON.dbo.operators o ON o.badge_ID = c.operator_ID
					  WHERE 
						c.machine_ID = ".$r['machine_ID']."
						AND c.tool_ID = ".$r['tool_ID']."
						AND dbo.getProdDate(c.cycle_time) >= '".$start_date."' 
						AND dbo.getProdDate(c.cycle_time) <= '".$end_date."'
						AND dbo.getShift(cycle_time) LIKE '".$shift."'
						AND (c.operator_ID LIKE '".$operator_id."'  OR c.operator_ID IS NULL)
					ORDER BY cycle_time DESC";
			//die($sql);
		}
		
		//echo $sql; die;
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
?>