<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	switch ($action) {
		case "load_plex_jobs":
			$sql = "SELECT TOP 100 *
					  FROM CUSTOM_plexJobs
					  ORDER BY Job_No DESC";
			$res = $db->query($sql);
			echo json_encode($res);
			break;

		case "load_plex_job_modules":
			$sql = "SELECT m.VIN, m.build_order, m.loaded_time, ms.status_desc
					  FROM modules m
					  	  JOIN module_statuses ms ON m.status = ms.ID
					  WHERE m.raw_data_ID = ".$_REQUEST['Job_Op_Key']."
					  ORDER BY m.VIN";
			$res = $db->query($sql);
			echo json_encode($res);
			break;

		case "load_plex_job_BOM":
			$sql = "SELECT *
					  FROM CUSTOM_plexJobPickerBOMs
					  WHERE Job_Key = ".$_REQUEST['Job_Key']."
					  ORDER BY Component_Part_No";
			$res = $db->query($sql);
			echo json_encode($res);
			break;

		case "pull_jobs_from_plex":
			$sql = "INSERT INTO [dbo].[CUSTOM_plexDataSourceQueue] ([command_desc],[queue_time],[param_name_1],[param_value_1],[param_name_2],[param_value_2])	VALUES (
						'Scheduled_Jobs_PLC_Get',
						GETDATE(),
						'PLC_Name',
						'Nysus',
						'Workcenter_Key',
						45742
					)";

			$db->query($sql);

			$sql = "EXEC [dbo].[sp_MESAAS_SEQUENCE_CUSTOM_processPlexCommandQueue]";

			$db->query($sql);
			break;
}
?>
