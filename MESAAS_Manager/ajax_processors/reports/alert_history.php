<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];


	if ($action == 'get_alert_types') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					mms.ID AS id
					,CONCAT(mms.severity_description, ' (',mms.severity_order,')') AS text
				FROM
					dbo.machine_monitor_severity mms
				ORDER BY
					mms.severity_order ASC;";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'get_alert_data') {
		global $db;
		$res = new StdClass();

		if ($_REQUEST['machine_ID'] > -1) {
			$machine_str = " AND mc.machine_ID = ".$_REQUEST['machine_ID'];
		} else {
			$machine_str = "";
		}
		if ($_REQUEST['alerts'] > -1) {
			$alert_str = " AND cls.machine_monitor_severity_ID IN (".implode(",",$_REQUEST['alerts']).") ";
		} else {
			$alert_str = "";
		}

		$sql = "SELECT
					mma.ID AS mma_ID
					,mma.created_timestamp
					,mma.processed_timestamp
					,mma.acknowledged_timestamp
					,mma.acknowledged_by
					,o.name
					,mma.limit_triggered
					,cls.ID AS cls_ID
					,cls.machine_monitor_severity_ID AS severity_ID
					,mms.severity_description
					,mcdp.cycle_ID
					,mcdp.data_point_value
					,mtcl.ID AS mtcl_ID
					,mtcl.upper_ctrl_limit
					,mtcl.lower_ctrl_limit
					,mtcl.target_value
					,mtcl.deactivated
					,dps.ID as dps_ID
					,dps.data_point_desc
					,m.machine_name
					,mc.cycle_time
					,mc.tool_ID
					,t.short_description AS tool
					,t.tool_description
				FROM machine_monitor_alerts mma
				JOIN machine_cycle_data_points mcdp ON mcdp.ID = mma.machine_cycle_datapoint_ID
				JOIN machine_cycles mc ON mc.ID = mcdp.cycle_ID
				JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.deactivated = 0
				JOIN machine_monitor_control_limit_severity cls ON cls.machine_tool_ctrl_limit_ID = mtcl.ID
				LEFT JOIN machine_monitor_severity mms ON mms.ID = cls.machine_monitor_severity_ID
				LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_ID
				LEFT JOIN tools t ON t.ID = mc.tool_ID
				LEFT JOIN machines m ON m.ID = mc.machine_ID
				LEFT JOIN operators o ON mma.acknowledged_by = o.badgeID
				WHERE
					mma.created_timestamp BETWEEN '".$_REQUEST['start_date']."' AND '".$_REQUEST['end_date']."'
					".$machine_str."
					".$alert_str."
				ORDER BY
					created_timestamp ASC;";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
?>