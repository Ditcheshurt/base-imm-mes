<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "get_carrier_status") {	
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		// carrier status
		$sql = "SELECT c.[ID] AS carrier,
					c.[module_ID],
					m.build_order AS current_module,
					m.sequence,
					m.id AS module_id,
					(SELECT max(station_ID) FROM module_station_history 
						WHERE module_ID = c.module_id 
						AND exit_time IS NOT NULL
					) AS prior_station_complete,
					(SELECT max(station_ID) FROM module_station_history 
						WHERE module_ID = c.module_id 
						AND exit_time IS NULL
					) AS current_station					
				FROM carrier_tracking c
					LEFT JOIN modules m ON m.id = module_id				
				ORDER BY c.module_ID DESC";
				
		$res->carrier_status = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == "get_module_detail") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT wp.ID, wp.part_ID,
					CASE WHEN wps.part_desc IS NULL THEN '' ELSE wps.part_desc END AS part_desc,
					wp.internal_serial,
		            wp.line_ID, wp.used_by_wip_part_ID, m.build_order, k.container_id
				FROM dbo.wip_parts wp
				LEFT JOIN
					wip_part_setup wps ON wps.id = wp.part_ID
				JOIN modules m on m.id = wp.used_by_module_id
				JOIN kit_container_history k on k.mod_id = m.id
				WHERE
					used_by_module_id = ".$r['module_id'];
					
		$res->module_detail = $db->query($sql);
		
		echo json_encode($res);
	}

?>