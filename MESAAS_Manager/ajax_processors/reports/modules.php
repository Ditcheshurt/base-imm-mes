<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;
	
if ($action == 'list_modules') {
    global $db;
    $res = new StdClass();

    $status = $_REQUEST['status'];
    $status_filter = "";
    if ($status != "-1") {
        $status_filter = " AND m.status = ".$status." ";
    }

    //module data
    $sql = "SELECT
                m.ID,
                m.sequence,
                m.build_order,
                m.VIN,
                l.line_code,
                m.loaded_time,
                m.built_time,
                ms.status_desc,
                m.status,
                m.pallet_number,
                m.pallet_position
            FROM
                modules m
                LEFT JOIN
                    lines l ON l.ID = m.line_ID
                LEFT JOIN
                module_statuses ms ON ms.ID = m.status
            WHERE
                m.recvd_time >= '".$_REQUEST['start_date']." 00:00:00'
                AND m.recvd_time <= '".$_REQUEST['end_date']." 23:59:59'
                AND l.ID = ".$_REQUEST['line_id']."
                ".$status_filter."
            ORDER BY
                m.build_order DESC;";

    $res = $db->query($sql);
    if (!$res) {
        $res = array();
    }
    echo json_encode($res);
}

if ($action == 'list_components') {
    global $db;
    $res = new StdClass();
    $module_ID = $_REQUEST['module_ID'];

    // get components
    $sql = "SELECT
                    ID,
                    part_number,
                    qty
                FROM
                    components_in
                WHERE
                    veh_id = ".$module_ID."
                ORDER BY part_number ASC;";

    $res = $db->query($sql);
    echo json_encode($res);
}
	

?>