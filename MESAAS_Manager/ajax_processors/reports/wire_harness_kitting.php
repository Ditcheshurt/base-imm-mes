<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == "get_wire_harness_info") {	
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		// rack info - last 24 hours
		$sql = "  SELECT ID, start_time, status, pick_rack_number 
					FROM NYSUS_SEQUENCE_MES.dbo.pick_racks 
					WHERE line_ID = ".$r['line_id']."
					AND start_time BETWEEN DATEADD(d,-1,CURRENT_TIMESTAMP) AND CURRENT_TIMESTAMP";
				
		$res = $db->query($sql);

		echo json_encode($res);
	}
	/*
	if ($action == "get_module_detail") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$sql = "SELECT wp.ID, wp.part_ID, wps.part_desc, wp.internal_serial,
		            wp.line_ID, wp.used_by_wip_part_ID, m.build_order, k.container_id
				FROM dbo.wip_parts wp
				LEFT JOIN
					wip_part_setup wps ON wps.id = wp.part_ID
				JOIN modules m on m.id = wp.used_by_module_id
				JOIN kit_container_history k on k.mod_id = m.id
				WHERE
					used_by_module_id = ".$r['module_id'];
					
		$res->module_detail = $db->query($sql);
		
		echo json_encode($res);
	}
	*/

?>