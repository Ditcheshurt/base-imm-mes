<?php

require_once("../init.php");

call_user_func($_REQUEST["action"], $db);

function get_systems($db)
{
	$sql = "SELECT
				ID,
				system_name,
				process_type,
				database_name
			FROM
				MES_COMMON.dbo.systems";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_operators($db)
{
	$sql = "SELECT
				ID,
				badge_ID,
				[name]
			FROM
				MES_COMMON.dbo.operators
			ORDER BY badge_ID";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_shifts($db)
{
	$system_id = $_REQUEST["system_ID"];
	$sql = "SELECT
				ID,
				shift_desc
			FROM
				MES_COMMON.dbo.shifts
			WHERE
				system_ID = $system_id";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

// sequenced
function get_lines($db)
{
	$where = "l.system_ID = " . $_REQUEST["system_ID"];

	$sql = "SELECT
				ID,
				line_code
			FROM
				lines l
			WHERE
				$where";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_line_stations($db)
{
	$where = "l.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["lines"]) && $_REQUEST["lines"] != null) {
		$values = is_array($_REQUEST["lines"]) ? implode(", ", $_REQUEST["lines"]) : $_REQUEST["lines"];
		$where .= " AND ls.line_ID IN ($values)";
	}

	$sql = "SELECT
				ls.ID,
				ls.station_desc
			FROM
				line_stations ls
			JOIN
				lines l ON ls.line_ID = l.ID
			WHERE
				$where";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

// repetitive
function get_machines($db)
{
	$where = "m.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {
		$where .= " AND t.ID IN (" . implode(", ", $_REQUEST["tools"]) . ")";
	}

	$sql = "SELECT DISTINCT
				m.ID,
				m.machine_number,
				m.machine_name
			FROM
				machines m
			JOIN
				machine_tools mt ON m.ID = mt.machine_ID
			JOIN
				tools t ON t.ID = mt.tool_ID
			JOIN
				tool_parts tp ON t.ID = mt.tool_ID
			JOIN
				parts p ON p.ID = tp.part_ID
			WHERE
				$where
			ORDER BY
				machine_name";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_tools($db)
{
	$where = "m.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(", ", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["parts"]) && $_REQUEST["parts"] != null) {
		$where .= " AND p.ID IN (" . implode(", ", $_REQUEST["parts"]) . ")";
	}
	$sql = "SELECT DISTINCT
				t.ID,
				t.short_description
			FROM
				tools t
			JOIN
				machine_tools mt ON mt.tool_ID = t.ID
			JOIN
				machines m ON m.ID = mt.machine_ID
			JOIN
				tool_parts tp ON t.ID = mt.tool_ID
			JOIN
				parts p ON p.ID = tp.part_ID
			WHERE
				$where
			ORDER BY t.short_description";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_parts($db)
{
	$where = "m.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		if (is_array($_REQUEST["machines"])) {
			$where .= " AND m.ID IN (" . implode(", ", $_REQUEST["machines"]) . ")";
		} else {
			$where .= " AND m.ID = " . $_REQUEST["machines"];
		}
	}
	if (isset($_REQUEST["tools"]) && $_REQUEST["tools"] != null) {		
		$where .= " AND t.ID IN (" . implode(", ", $_REQUEST["tools"]) . ")";
	}
	$sql = "SELECT DISTINCT
				p.ID,
				p.part_number,
				p.part_desc
			FROM
				parts p
			JOIN
				tool_parts tp ON p.ID = tp.part_ID
			JOIN
				tools t ON t.ID = tp.tool_ID
			JOIN
				machine_tools mt ON t.ID = mt.tool_ID
			JOIN
				machines m ON m.ID = mt.machine_ID
			WHERE
				$where
			ORDER BY ID";

	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

// function get_plcs($db)
// {
// 	$where = "m.system_ID = " . $_REQUEST["system_ID"];
// 	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
// 		$where .= " AND m.ID IN (" . implode(", ", $_REQUEST["machines"]) . ")";
// 	}

// 	$sql = "SELECT DISTINCT
// 				p.ID,
// 				p.description,
// 				p.ip_address
// 			FROM
// 				NYSUS_PLC_POLLER.dbo.nysus_plcs p
// 			JOIN
// 				machines m ON m.plc_ID = p.ID
// 			WHERE
// 				$where
// 			ORDER BY p.ID";
// //die($sql);
// 	$result = $db->query($sql);
// 	if ($result == null) {
// 		$result = array();
// 	}
// 	echo json_encode($result);
// }
function get_plcs($db)
{
	$where = "m.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(", ", $_REQUEST["machines"]) . ")";
	}

	// check for alt_PLC_ID column in machines table
	$sql = "SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'machines'
				AND TABLE_SCHEMA='dbo'
				AND COLUMN_NAME = 'alt_PLC_ID'";
	$result = $db->query($sql);

	$sql_alt_plc_ids = "";
	if (count($result) > 0) {
		$sql_alt_plc_ids = " UNION
			SELECT DISTINCT
				p.ID,
				p.description,
				p.ip_address
			FROM
				NYSUS_PLC_POLLER.dbo.nysus_plcs p
			JOIN
				machines m ON m.alt_PLC_ID = p.ID
			WHERE
				$where";
	}

	$sql = "SELECT DISTINCT
				p.ID,
				p.description,
				p.ip_address
			FROM
				NYSUS_PLC_POLLER.dbo.nysus_plcs p
			JOIN
				machines m ON m.plc_ID = p.ID
			WHERE
				$where
			$sql_alt_plc_ids
			ORDER BY p.ID";
//die($sql);
	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_fault_codes($db)
{
	$where = "m.system_ID = " . $_REQUEST["system_ID"];
	if (isset($_REQUEST["machines"]) && $_REQUEST["machines"] != null) {
		$where .= " AND m.ID IN (" . implode(", ", $_REQUEST["machines"]) . ")";
	}
	if (isset($_REQUEST["plcs"]) && $_REQUEST["plcs"] != null) {
		$where .= " AND p.ID IN (" . implode(", ", $_REQUEST["plcs"]) . ")";
	}

	// check for alt_PLC_ID column in machines table
	$sql = "SELECT COLUMN_NAME
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = 'machines'
				AND TABLE_SCHEMA='dbo'
				AND COLUMN_NAME = 'alt_PLC_ID'";
	$result = $db->query($sql);

	$sql_alt_plc_ids = "";
	if (count($result) > 0) {
		$sql_alt_plc_ids = " OR m.alt_plc_ID = p.ID ";
	}

	$sql = "SELECT DISTINCT
				fc.ID,
				fc.description
			FROM
				fault_codes fc
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plc_tags t ON fc.tag_ID = t.ID
			JOIN
				NYSUS_PLC_POLLER.dbo.nysus_plcs p ON t.plc_ID = p.ID
			JOIN
				machines m ON m.plc_ID = p.ID $sql_alt_plc_ids
				AND m.machine_number = t.description				
			WHERE	
				$where
			ORDER BY fc.description";
//die($sql);
	$result = $db->query($sql);
	if ($result == null) {
		$result = array();
	}
	echo json_encode($result);
}

function get_group_by($db) {
	$result = array('Operator', 'Shift');

	echo json_encode($result);
}

function get_top($db)
{
	$result = array(10,20,50,100);

	echo json_encode($result);
}

