<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];


	if ($action == 'get_params') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					dps.ID AS id
					,dps.data_point_desc AS text
				FROM
					data_point_setup dps
				WHERE
					dps.active = 1;";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'get_param_data') {
		global $db;
		$res = new StdClass();

		$system_ID = $_REQUEST["system_ID"];

		if ($_REQUEST['machine_ID'] > -1) {
			$machine_str = "AND mc.machine_ID = ".$_REQUEST['machine_ID'];
		} else {
			$machine_str = "";
		}
		if ($_REQUEST['params'] > -1) {
			$param_str = " AND dps.ID IN (".implode(",",$_REQUEST['params']).") ";
		} else {
			$param_str = "";
		}

		$sql = "SELECT
					 m.machine_name AS [machine]
					,m.ID AS [machine_id]
					,t.tool_description AS [tool]
					,mc.ID AS [cycle_ID]
					,mc.cycle_time
					,mcdp.data_point_ID
					,dps.data_point_desc
					,mcdp.data_point_value
					,ISNULL(mtcl.upper_ctrl_limit, dps.default_ucl) AS ucl
					,ISNULL(mtcl.lower_ctrl_limit, dps.default_lcl) AS lcl
					,mc.cycle_time
				FROM
					machines m
					JOIN machine_cycles mc ON mc.machine_ID = m.ID
					JOIN machine_cycle_data_points mcdp ON mcdp.cycle_ID = mc.ID
					LEFT JOIN tools t ON t.ID = mc.tool_ID
					LEFT JOIN data_point_setup dps ON dps.ID = mcdp.data_point_id
					LEFT JOIN machine_tool_ctrl_limits mtcl ON mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.data_point_ID = mcdp.data_point_ID
				WHERE
					mc.cycle_time >= '".$_REQUEST['start_date']."'
					AND mc.cycle_time < '".$_REQUEST['end_date']."'
					".$machine_str."
					".$param_str."
					AND m.system_ID = ".$system_ID."
					AND dps.active = 1
				ORDER BY
					m.machine_name, mc.cycle_time, mcdp.data_point_id;";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
?>