<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);


	function get_part ($request, $db) {
		$query = "SELECT
					mcp.serial_number,
					mcp.full_barcode,
					p.part_number,
					p.part_desc,
					p.mrp_part_number,
					p.mrp_company_code,
					dbo.fn_getChryslerPartNumberFromFullBarcode(mcp.full_barcode) AS chrysler_part_number,
					dbo.fn_getFCAETPartNumberFromFullBarcode(mcp.full_barcode) AS fcaet_part_number,
					dbo.fn_getSupplierCodeFromFullBarcode(mcp.full_barcode) AS supplier_code,
					dbo.fn_getAdditionalInfoFromFullBarcode(mcp.full_barcode) AS additional_info,
					MIN(mcph.cycle_start_time) AS start_time,
					MAX(mcph2.cycle_complete_time) AS complete_time
				FROM
					machine_cycle_parts mcp
					LEFT JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
					LEFT JOIN parts p ON p.ID = tp.part_ID
					LEFT JOIN machine_cycle_part_history mcph ON mcph.serial = mcp.serial_number
					LEFT JOIN machine_cycle_part_history mcph2 ON mcph2.serial = mcp.serial_number
						AND mcph2.machine_ID = (
							SELECT TOP 1 ID FROM machines ORDER BY op_order DESC
						)
				WHERE
					mcp.serial_number = '{$request['serial_number']}'
				GROUP BY
					mcp.serial_number,
					mcp.full_barcode,
					p.part_number,
					p.part_desc,
					p.mrp_part_number,
					p.mrp_company_code;";
		$result = $db->query($query);

		if ($result) {
			$data = $result[0];
		} else {
			$data = $result;
		}

		echo json_encode($data);
	}

	function get_component_lots ($request, $db) {
		$query = "SELECT
					lmt.part_number,
					lmt.description,
					lmcp.lot_number
				FROM
					machine_cycle_parts mcp
					JOIN lot_machine_cycle_parts lmcp ON lmcp.machine_cycle_part_ID = mcp.ID
					JOIN lot_material_types lmt ON lmt.ID = lmcp.lot_material_type_ID
				WHERE
					mcp.serial_number = '{$request['serial_number']}'
				GROUP BY
					lmt.part_number,
					lmt.description,
					lmcp.lot_number;";

		echo json_encode($db->query($query));
	}

	function get_machine_history ($request, $db) {
		$query = "SELECT
					mcph.serial,
					mcph.machine_ID,
					m.machine_name,
					MIN(mcph.check_time) AS check_time,
					MIN(mcph.cycle_start_time) AS start_time,
					MAX(mcph.cycle_complete_time) AS complete_time,
					o.name AS operator_name,
					DATEDIFF(SECOND, MIN(mcph.cycle_start_time), MAX(mcph.cycle_complete_time)) AS cycle_time,
					DATEDIFF(SECOND, MIN(mcph.check_time), MAX(mcph.cycle_complete_time)) AS full_cycle_time
				FROM
					machine_cycle_part_history mcph
					JOIN machine_cycle_parts mcp ON mcp.serial_number = mcph.serial
					JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID AND mc.machine_ID = mcph.machine_ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
					LEFT JOIN machines m ON m.ID = mcph.machine_ID
				WHERE
					mcph.serial = '{$request['serial_number']}'
				GROUP BY
					mcph.serial,
					mcph.machine_ID,
					m.machine_name,
					o.name;";

		echo json_encode($db->query($query));
	}

	function get_machine_cycle_history ($request, $db) {
		$query = "SELECT
					mcph.serial,
					mcph.machine_ID,
					m.machine_name,
					MIN(mcph.check_time) AS check_time,
					MIN(mcph.cycle_start_time) AS start_time,
					MAX(mcph.cycle_complete_time) AS complete_time,
					DATEDIFF(SECOND, MIN(mcph.cycle_start_time), MAX(mcph.cycle_complete_time)) AS cycle_time,
					DATEDIFF(SECOND, MIN(mcph.check_time), MAX(mcph.cycle_complete_time)) AS full_cycle_time
				FROM
					machine_cycle_part_history mcph
					JOIN machine_cycle_parts mcp ON mcp.serial_number = mcph.serial
					JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID AND mc.machine_ID = mcph.machine_ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
					LEFT JOIN machines m ON m.ID = mcph.machine_ID
				WHERE
					mcph.serial = '{$request['serial_number']}'
				GROUP BY
					mcph.serial,
					mcph.machine_ID,
					m.machine_name;";
		$machine_history = $db->query($query);

		if ($machine_history) {
			foreach($machine_history as $i => &$machine) {
				$query = "SELECT
							mcph.check_time,
							mcph.cycle_start_time,
							mcph.cycle_complete_time,
							mcph.check_result,
							mcph.cycle_complete_result,
							DATEDIFF(SECOND, mcph.cycle_start_time, mcph.cycle_complete_time) AS cycle_time,
							DATEDIFF(SECOND, mcph.check_time, mcph.cycle_complete_time) AS full_cycle_time
						FROM
							machine_cycle_part_history mcph
						WHERE
							mcph.serial = '{$machine['serial']}'
							AND mcph.machine_ID = {$machine['machine_ID']};";
				$machine['machine_cycle_part_history'] = $db->query($query);
			}
		}

		echo json_encode($machine_history);
	}

	function get_part_gauge_details ($request, $db) {
		$query = "SELECT
					mcph.serial,
					mcph.machine_ID,
					m.machine_name,
					MIN(mcph.cycle_start_time) AS start_time,
					MAX(mcph.cycle_complete_time) AS complete_time,
					o.name AS operator_name,
					DATEDIFF(SECOND, MIN(mcph.cycle_start_time), MAX(mcph.cycle_complete_time)) AS cycle_time
				FROM
					machine_cycle_part_history mcph
					JOIN machine_cycle_parts mcp ON mcp.serial_number = mcph.serial
					JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID AND mc.machine_ID = mcph.machine_ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mc.operator_ID
					LEFT JOIN machines m ON m.ID = mcph.machine_ID
					JOIN gauge_material_types gmt ON gmt.tool_part_ID = mcp.tool_part_ID
				WHERE
					mcph.serial = '{$request['serial_number']}'
				GROUP BY
					mcph.serial,
					mcph.machine_ID,
					m.machine_name,
					o.name;";
		$machine_history = $db->query($query);

		if ($machine_history) {
			foreach($machine_history as $i => &$machine) {
				$query = "SELECT
							m.machine_name,
							gmt.description,
							gmcp.data_point_value,
							gmti.min_value,
							gmti.max_value
						FROM
							gauge_machine_cycle_parts gmcp
							LEFT JOIN machine_cycle_parts mcp ON mcp.ID = gmcp.machine_cycle_part_ID
							LEFT JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
							LEFT JOIN machines m ON m.ID = mc.machine_ID
							LEFT JOIN gauge_material_types_instructions gmti ON gmti.ID = gmcp.gauge_material_types_instruction_ID
							LEFT JOIN gauge_material_types gmt ON gmt.ID = gmti.gauge_material_type_ID
						WHERE
							mcp.serial_number = '{$machine['serial']}'
							AND m.ID = {$machine['machine_ID']};";
				$machine['machine_part_gauge_detail'] = $db->query($query);
			}
		}

		echo json_encode($machine_history);
	}

	function get_part_gauge_history ($request, $db) {
		// $query = "SELECT TOP 1
		//             m.system_ID
		//         FROM
		//             machine_cycle_parts mcp
		//             LEFT JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
		//             LEFT JOIN machines m ON m.ID = mc.machine_ID
		//         WHERE
		//             mcp.serial_number = '{$request['serial_number']}'
		//         GROUP BY
		//             m.system_ID,
		//             mcp.serial_number;";
		// $systems = $db->query($query);

		$query = "SELECT
					m.ID,
					m.machine_name,
					mcp.tool_part_ID
				FROM
					machine_cycle_parts mcp
					LEFT JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
					LEFT JOIN machines m ON m.ID = mc.machine_ID
					JOIN gauge_material_types gmt ON gmt.tool_part_ID = mcp.tool_part_ID
				WHERE
					mcp.serial_number = '{$request['serial_number']}'
					--AND gmt.ID IS NOT NULL
				GROUP BY
					m.ID,
					m.machine_name,
					mcp.tool_part_ID;";
		$machines = $db->query($query);

		if ($machines) {
			foreach($machines as $i => &$machine) {
				$query = "SELECT TOP 1
							mcp.serial_number,
							--mc.cycle_time,
							gmcp.recorded_date,
							gmcp.operator_ID,
							o.name AS operator_name
						FROM
							gauge_machine_cycle_parts gmcp
							LEFT JOIN machine_cycle_parts mcp ON mcp.ID = gmcp.machine_cycle_part_ID
							LEFT JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
							LEFT JOIN machines m ON m.ID = mc.machine_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = gmcp.operator_ID
						WHERE
							m.ID = {$machine['ID']}
							AND mcp.tool_part_ID = {$machine['tool_part_ID']}
						ORDER BY
							gmcp.recorded_date DESC;";
				//die($query);
				$result = $db->query($query);
				if ($result) {
					$machine['serial_number'] = $result[0]['serial_number'];
					$machine['recorded_date'] = $result[0]['recorded_date'];
					$machine['operator_ID'] = $result[0]['operator_ID'];
					$machine['operator_name'] = $result[0]['operator_name'];
				} else {
					$machine['serial_number'] = null;
					$machine['recorded_date'] = null;
					$machine['operator_ID'] = null;
					$machine['operator_name'] = null;
				}
			}
		}

		echo json_encode($machines);
	}

	function get_plc_values ($request, $db) {
		$query = "SELECT
					mcppv.serial,
					mcppv.machine_ID,
					mcppv.tag_name,
					mcppv.tag_value,
					dps.data_point_name,
					dps.units
				FROM
					machine_cycle_parts_plc_values mcppv
					LEFT JOIN NYSUS_PLC_POLLER.dbo.nysus_plc_tags npt ON npt.name = mcppv.tag_name
					LEFT JOIN data_point_setup dps ON dps.ID = npt.description2
				WHERE
					mcppv.serial = '{$request['serial_number']}'
				ORDER BY
					mcppv.machine_ID,
					dps.ID;";
		echo json_encode($db->query($query));
	}

	function get_auto_gauge_data ($request, $db) {
		$query = "SELECT
					*
				FROM
					gauge_autogauge
				WHERE
					serial_number = '{$request['serial_number']}';";
		echo json_encode($db->query($query));
	}

	function get_mahr_mmq_data ($request, $db) {
		$query = "SELECT
					mmq.*,
					o.name AS operator_name
				FROM
					gauge_mahrmmq mmq
					LEFT JOIN machine_cycle_parts mcp ON mcp.ID = mmq.machine_cycle_part_ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mmq.operator_ID
				WHERE
					mcp.serial_number = '{$request['serial_number']}';";
		//die($query);
		echo json_encode($db->query($query));
	}

	function get_mahr_mmq_history ($request, $db) {
		// get the most recent mahr gauge check for this type of part
		$sql = "SELECT TOP 1
					mcp.serial_number,
					---CAST(('20' + SUBSTRING(mmq.date, 7, 2) + '-' + SUBSTRING(mmq.date, 4, 2) + '-' + SUBSTRING(mmq.date, 1, 2) + ' ' + [time])  AS DATETIME) AS recorded_date,
					o.name AS operator_name,
					mmq.created_date
				FROM
					gauge_mahrmmq mmq
					LEFT JOIN machine_cycle_parts mcp ON mcp.ID = mmq.machine_cycle_part_ID
					LEFT JOIN parts p ON LEFT(p.mrp_part_number, 8) = LEFT(mcp.full_barcode, 8)
					RIGHT JOIN (
						SELECT
							mcp.*,
							p.ID AS part_ID
						FROM
							machine_cycle_parts mcp
							LEFT JOIN parts p ON LEFT(p.mrp_part_number, 8) = LEFT(mcp.full_barcode, 8)
						WHERE
							mcp.serial_number = '{$request['serial_number']}'
					) mcp2 ON mcp2.part_ID = p.ID
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = mmq.operator_ID
				ORDER BY
					--CAST(('20' + SUBSTRING(mmq.date, 7, 2) + '-' + SUBSTRING(mmq.date, 4, 2) + '-' + SUBSTRING(mmq.date, 1, 2) + ' ' + [time])  AS DATETIME) DESC,
					mmq.created_date DESC,
					mmq.ID DESC";
		echo json_encode($db->query($sql));
	}

	function get_rework_history ($request, $db) {
		$query = "SELECT
					o.name AS operator_name,
					o2.name AS approval_operator_name,
					rwh.rework_date,
					rwh.approval_date,
					m.machine_name AS rejected_station_desc,
					rwh.rejected_station_date,
					m2.machine_name AS injected_station_desc,
					rwh.injected_station_date
				FROM
					rework_history rwh
					LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = rwh.operator_ID
					LEFT JOIN MES_COMMON.dbo.operators o2 ON o2.ID = rwh.approval_operator_ID
					LEFT JOIN machines m ON m.ID = rwh.rejected_station_ID
					LEFT JOIN machines m2 ON m2.ID = rwh.injected_station_ID
				WHERE
					rwh.serial = '{$request['serial_number']}'
				ORDER BY
					rwh.rejected_station_date ASC;";
		//die($query);
		echo json_encode($db->query($query));
	}

?>