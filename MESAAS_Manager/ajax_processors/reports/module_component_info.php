<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];
    
    if ($action == 'load_data') {
        global $db;
        $res = new StdClass();
        $r = $_REQUEST;
        
        $module_ID = $r['module_ID'];
        
        $sql = "SELECT
                    m.ID,
                    m.build_order,
                    m.sequence,
                    m.VIN,
                    m.line_ID,
                    l.line_code,
                    o.name AS operator_name,
                    m.recvd_time,
                    m.loaded_time,
                    m.built_time
                FROM
                    modules m
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON o.ID = m.operator_id
                    LEFT JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = ".$module_ID;

        $res->module_info = $db->query($sql);
        
        $vin = $res->module_info[0]['VIN'];
        
        //broadcast info
        $sql = "SELECT
                    l.line_code,
                    m.sequence,
                    m.loaded_time,
                    l.preview_status,
                    l.build_status,
                    l.info_status
                FROM
                    modules m
                    LEFT OUTER JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = ".$module_ID;
        $res->broadcast_info = $db->query($sql);
        
        $mod_line_ID = $res->module_info[0]['line_ID'];
        
        
        // components
        $sql = "SELECT
                    part_number,
                    qty
                FROM
                    components_in
                WHERE
                    veh_id = ".$module_ID."
                ORDER BY part_number ASC;";

        $res->component_info = $db->query($sql);

        echo json_encode($res);
    }

?>