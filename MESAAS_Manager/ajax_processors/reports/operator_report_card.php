<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];

    call_user_func($action, $_REQUEST, $db);


    function get_operator ($request, $db) {
        $sql = "SELECT
                    o.ID,
                    o.first_name,
                    o.last_name,
                    o.badge_ID
                FROM
                    MES_COMMON.dbo.operators o
                WHERE
                    o.ID = {$request['operator_ID']};";
        $result = $db->query($sql);

        if ($result) {
            $data = $result[0];
        } else {
            $data = $result;
        }

        echo json_encode($data);
    }

    function get_login_history ($request, $db) {
        $sql = "SELECT
                    CONVERT(DATE, oeh.start_time) AS event_date,
                    oeh.start_time,
                    oeh.end_time,
                    ISNULL(oeh.duration, DATEDIFF(SECOND, oeh.start_time, GETDATE())) AS duration,
                    m.machine_name
                FROM
                    machine_operator_event_history moeh
                    LEFT JOIN MES_COMMON.dbo.operator_event_history oeh ON oeh.ID = moeh.operator_event_history_ID
                    LEFT JOIN machines m ON m.ID = moeh.machine_ID
                WHERE
                    oeh.operator_ID = {$request['operator_ID']}
                    AND oeh.operator_event_ID = 1
                    AND oeh.start_time >= '{$request['start_date']} 00:00:00'
                    AND oeh.start_time <= '{$request['end_date']} 23:59:59.99';";
        //die($sql);
        echo json_encode($db->query($sql));
    }

    function get_average_shift_summary ($request, $db) {
        $sql = "SELECT
                    AVG(ss.part_qty) AS part_qty,
                    AVG(mc.cycle_duration) AS cycle_time,
                    AVG(ss.quality) AS quality,
                    AVG(ss.performance) AS performance,
                    AVG(ss.availability) AS availability,
                    AVG(ss.oee) AS oee
                FROM
                    operator_shift_summaries oss
                    LEFT JOIN shift_summary ss ON ss.ID = oss.shift_summary_ID
                    LEFT JOIN machines m ON m.ID = ss.machine_ID
                    RIGHT JOIN machine_cycles mc ON mc.machine_ID = m.ID
                        AND dbo.getProdDate(mc.cycle_time) = ss.the_date
                        AND dbo.getShift(m.system_ID, mc.cycle_time) = ss.the_shift
                WHERE
                    oss.operator_ID = {$request['operator_ID']}
                    AND ss.the_date >= '{$request['start_date']} 00:00:00'
                    AND ss.the_date <= '{$request['end_date']} 23:59:59.99'
                GROUP BY
                    oss.operator_ID;";
        //die($sql);
        echo json_encode($db->query($sql));
    }

    function get_shift_summaries ($request, $db) {
        $sql = "SELECT
                    ss.the_date,
                    m.machine_name,
                    ss.part_qty,
                    AVG(mc.cycle_duration) AS avg_cycle_time,
                    ss.quality,
                    ss.performance,
                    ss.availability,
                    ss.oee
                FROM
                    operator_shift_summaries oss
                    LEFT JOIN shift_summary ss ON ss.ID = oss.shift_summary_ID
                    LEFT JOIN machines m ON m.ID = ss.machine_ID
                    RIGHT JOIN machine_cycles mc ON mc.machine_ID = m.ID
                        AND dbo.getProdDate(mc.cycle_time) = ss.the_date
                        AND dbo.getShift(m.system_ID, mc.cycle_time) = ss.the_shift
                WHERE
                    oss.operator_ID = {$request['operator_ID']}
                    AND ss.the_date >= '{$request['start_date']} 00:00:00'
                    AND ss.the_date <= '{$request['end_date']} 23:59:59.99'
                GROUP BY
                    ss.the_date,
                    m.machine_name,
                    ss.part_qty,
                    ss.quality,
                    ss.performance,
                    ss.availability,
                    ss.oee;";
		print_r($sql);			
        //die($sql);
        //echo json_encode($db->query($sql));
    }

?>