<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_machines($request, $db) {
		$query = "
			SELECT
				ID AS id
				, machine_number + ' - ' + machine_name AS text
			FROM
				machines
			ORDER BY
				ID";

		echo json_encode($db->query($query));
	}

	function get_tools($request, $db) {
		$query = "
			SELECT
				ID AS id
				,tool_description AS text
			FROM
				tools
			ORDER BY
				tool_description";

		echo json_encode($db->query($query));
	}

	function get_origins($request, $db) {
		$query = "
			SELECT
				ID AS id
				,series + ' - ' + origin_description AS text
			FROM
				machine_defect_origins
			ORDER BY
				series, origin_description";

		echo json_encode($db->query($query));
	}

	function get_params_description_as_id($request, $db) {
		$query = "
			SELECT DISTINCT
				r.[defect_description] AS id
				,r.[defect_description] AS text
				--,(SELECT reason_description FROM [machine_defect_reasons] WHERE ID = r.parent_ID) AS parent
			FROM
				[machine_defect_reasons] r
			WHERE
				active = 1
				AND parent_ID IS NOT NULL
				AND parent_ID > 0
			ORDER BY
				defect_description";
		echo json_encode($db->query($query));
	}

	function get_params($request, $db) {
		$query = "
			SELECT
				dps.ID AS id
				,dps.[data_point_desc] AS text
			FROM
				[data_point_setup] dps
			WHERE
				active = 1
			ORDER BY
				data_point_desc";
	//die($query);
		echo json_encode($db->query($query));
	}

	function get_params_by_machine_type_ID($request, $db) {
		$machine_type_ID = $request['machine_type_ID'];

		$query = "
			SELECT DISTINCT
				ID AS id
				,r.[defect_description] AS text
				--,(SELECT defect_description FROM [machine_defect_reasons] WHERE ID = r.parent_ID) AS parent
			FROM
				[machine_defect_reasons] r
			WHERE
				active = 1
				AND machine_type_ID = " . $machine_type_ID . "
				AND parent_ID IS NOT NULL
				AND parent_ID > 0
			ORDER BY
				defect_description";
		echo json_encode($db->query($query));
	}

	function run_report($request, $db) {
		//define group bys
		require("../../../common/library/php/groupBy.php");
		$gby = new groupBy();
		$gby->set_group("date", "CONVERT(VARCHAR(10), mdl.defect_time, 101)");
		$gby->set_group("machine", "m.machine_name");
		$gby->set_group("sub_machine", "sm.sub_machine_name");
		$gby->set_group("tool", "t.tool_description");
		$gby->set_group("reason_code", "d.reason_description");
		$gby->set_group("week", "DATEPART(wk, mdl.defect_time)");
		$gby->set_group("shift", "dbo.getShift(m.system_ID, mdl.defect_time)");
		$gby->set_group("hour", "DATEPART(hh, mdl.defect_time)");
		$gby->set_group("defect_description", "d.defect_description");
		$gby->set_group("part", "tp.part_number");
		//$gby->set_group("hour", "CAST(mdl.defect_time AS date)", 'date');

		//get group by based on request
		if (isset($request['group_by']))
			$group_by = $gby->get_group_by($request["group_by"]);
		else
			$group_by = $gby->get_group_by(array());

		$query = "SELECT ".implode(",", $group_by->selects) . ", COUNT(*) As 'defects'" ;

		$query .= " FROM
						machine_defect_log mdl
						LEFT JOIN machine_defect_reasons d ON mdl.defect_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t ON mdl.tool_ID = t.ID
						--JOIN tool_parts tp ON tp.ID = mdl.part_ID
					WHERE
						mdl.defect_ID IS NOT NULL
						AND mdl.defect_time >= '".$request["from_date"]." 00:00:00'
						AND mdl.defect_time <= '".$request["to_date"]." 23:59:59'";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.defect_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";

		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";

		if (isset($request["sel_params"]) && $request["sel_params"] != null)
			$query .= " AND d.ID IN (" . implode(",",$request["sel_params"]) . ") ";

		if (isset($request["sel_origins"]) && $request["sel_origins"] != null)
			$query .= " AND mdl.defect_origin_ID IN ('" . implode("','", $request["sel_origins"]) . "') ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";

		/*if (isset($request["sel_params"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_params"]) . "%'";
*/
		$query .= " GROUP BY " . implode(",", $group_by->groups);
		$query .= " ORDER BY " . implode(",", $group_by->groups);
die($query);
		echo json_encode($db->query($query));

	}

	function drilldown($request, $db) {

		$query = "SELECT
						mdl.ID,
						CONVERT(VARCHAR, mdl.defect_time,101)+' '+CONVERT(VARCHAR, mdl.defect_time,108) AS 'datetime',
						m.machine_name AS machine,
						--sm.sub_machine_name AS 'Sub Machine',
						t.tool_description AS tool,
						p.part_number AS 'Part Number',
						d.series + ' - ' + d.defect_description AS reason,
						d.ID AS defect_ID,
						orig.origin_description AS origin,
						mdl.disposition AS comment,
						o.name AS 'Entered By',
						m.machine_type_ID AS machine_type_ID,
						t.ID AS tool_ID,
						m.ID AS machine_ID,
						mdl.part_ID
					FROM
						machine_defect_log mdl
						LEFT JOIN machine_defect_reasons d ON mdl.defect_ID = d.ID
						LEFT JOIN machine_defect_origins orig ON orig.ID = mdl.defect_origin_ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--LEFT OUTER JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t ON mdl.tool_ID = t.ID
						LEFT JOIN [MES_COMMON].dbo.operators o ON o.ID = mdl.operator_ID
						--JOIN tool_parts tp ON tp.ID = mdl.part_ID
						--JOIN parts p ON p.ID = tp.part_ID
						JOIN parts p ON mdl.part_ID = p.ID
					WHERE
						mdl.defect_time >= '{$request["from_date"]} 00:00:00'
						AND mdl.defect_time <= '{$request["to_date"]} 23:59:59' ";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.defect_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";

		if (isset($request["machine"]) && $request["machine"] != null)
			$query .= " AND m.machine_name IN ('" . implode("','", $request["machine"]) . "') ";

		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";

		if (isset($request["sel_params"]) && $request["sel_params"] != null)
			$query .= " AND d.ID = " . $request["sel_params"];

		if (isset($request["sel_origins"]) && $request["sel_origins"] != null)
			$query .= " AND mdl.defect_origin_ID IN ('" . implode("','", $request["sel_origins"]) . "') ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";

		if (isset($request["sel_part"]))
			$query .= " AND tp.part_number IN ('" . implode("','", $request["sel_part"]) . "') ";

	//die($query);
		echo json_encode($db->query($query));

	}

	function sel_shifts($orm) {

		$obj_1 = new stdClass();
		$obj_1->text = "Shift 1";
		$obj_1->value = "1";

		$obj_2 = new stdClass();
		$obj_2->text = "Shift 2";
		$obj_2->value = "2";

		//$obj_3 = new stdClass();
		//$obj_3->text = "Shift 3";
		//$obj_3->value = "3";

		return array($obj_1, $obj_2);

	}

	function get_top($request, $db) {
		$quantify = "";
		if (isset($request['quantify'])) {
			if ($request['quantify'] == "hours") {
				$quantify = " / 60)";
			} else if ($request['quantify'] == "percent") {
				$quantify = " / 1440 ) * 100";
			} else {
				$quantify = ")";
			}
		}

		//define group bys
		require("../../../common/library/php/groupBy.php");
		$gby = new groupBy();
		$gby->set_group("date", "CONVERT(VARCHAR(10), mdl.defect_time, 101)");
		$gby->set_group("machine", "m.machine_name");
		$gby->set_group("sub_machine", "sm.sub_machine_name");
		$gby->set_group("tool", "t.tool_description");
		$gby->set_group("reason_code", "d.defect_description");
		$gby->set_group("week", "DATEPART(wk, mdl.defect_time)");
		$gby->set_group("shift", "dbo.getShift(m.system_ID, mdl.defect_time)");
		$gby->set_group("hour", "DATEPART(hh, mdl.defect_time)");
		//$gby->set_group("hour", "CAST(mdl.defect_time AS date)", 'date');
		$gby->set_group("part", "tp.part_number");

		//get group by based on request
		if (isset($request['group_by']))
			$group_by = $gby->get_group_by($request["group_by"]);
		else
			$group_by = $gby->get_group_by(array());

		$query = "SELECT TOP 5
						d.defect_description
						, COUNT(*) AS 'defects'
					FROM
						machine_defect_log mdl
						LEFT JOIN machine_defect_reasons d ON mdl.defect_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t on mdl.tool_ID = t.ID
						JOIN tool_parts tp ON tp.ID = mdl.part_ID
					WHERE
						mdl.defect_time >= '{$request["from_date"]} 00:00:00'
						AND mdl.defect_time <= '{$request["to_date"]} 23:59:59'";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$query .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";

		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$query .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";

		if (isset($request["sel_params"]) && $request["sel_params"] != null)
			$query .= " AND d.defect_description IN ('" . implode("','", $request["sel_params"]) . "') ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND m.sub_machine_name IN ('" . implode("','", $request["sel_sub_machines"]) . "') ";

		/*if (isset($request["sel_params"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_params"]) . "%'";
		*/
		$query .= " GROUP BY d.defect_description";// . implode(",", $group_by->groups);
		//$query .= " ORDER BY " . implode(",", $group_by->groups);
	//die($query);
		echo json_encode($db->query($query));
	}
	/*
	function get_defect_locations($request, $db) {
		$machine_defect_ID = $request['machine_defect_ID'];

		$query = "SELECT [ID]
				  ,[machine_defect_ID]
				  ,[defect_location_x]
				  ,[defect_location_y]
				FROM
					[machine_defect_locations]
				WHERE
					[machine_defect_ID] = $machine_defect_ID";

		echo json_encode($db->query($query));
	}*/

	function get_defect_locations($request, $db) {

		$results = new stdClass();

		if (isset($request["from_date"]))
			$where = " AND mdl.defect_time >= '".$request["from_date"]." 00:00:00'";

		if (isset($request["to_date"]))
			$where .= " AND mdl.defect_time <= '".$request["to_date"]." 23:59:59'";

		if (isset($request["sel_date"]))
			$where .= " AND CONVERT(VARCHAR(13), mdl.defect_time, 101) = '".$request["sel_date"]."'";

		if (isset($request["sel_shifts"]))
			$where .= " AND dbo.getShift(m.system_ID, mdl.defect_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]) && $request["sel_machines"] != null)
			$where .= " AND m.ID IN ('" . implode("','", $request["sel_machines"]) . "') ";

		if (isset($request["machine_defect_ID"]) && $request["machine_defect_ID"] != null)
			$where .= " AND locs.machine_defect_ID IN ('" . implode("','", $request["machine_defect_ID"]) . "') ";

		if (isset($request["sel_tools"]) && $request["sel_tools"] != null)
			$where .= " AND t.ID IN ('" . implode("','", $request["sel_tools"]) . "') ";

		if (isset($request["sel_part"]) && $request["sel_part"] != null)
			$where .= " AND tp.part_number IN ('" . implode("','", $request["sel_part"]) . "') ";

		if (isset($request["sel_params"]) && $request["sel_params"] != null)
			$where .= " AND d.ID = " . $request["sel_params"];

		if (isset($request["sel_origins"]) && $request["sel_origins"] != null)
			$where .= " AND mdl.defect_origin_ID IN ('" . implode("','", $request["sel_origins"]) . "') ";

		// get defect parts
		$query = "SELECT DISTINCT
						tp.part_ID AS part_ID
						,p.part_number
						,p.part_desc
						,p.part_image
					FROM
						machine_defect_log mdl
						LEFT JOIN machine_defect_reasons d ON mdl.defect_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t ON mdl.tool_ID = t.ID
						JOIN tool_parts tp ON t.ID = tp.tool_ID AND tp.part_ID = mdl.part_ID
						--JOIN parts p ON p.ID = tp.part_ID
						JOIN parts p ON p.ID = mdl.part_ID
						JOIN machine_defect_locations locs ON locs.machine_defect_ID = mdl.ID
					WHERE
						1=1 ";

		$query .= $where;

		$results->parts = $db->query($query);
		$results->part_query = $query;

		// get defect locations
		$query = "SELECT
						tp.ID AS part_ID
						,[defect_location_x]
						,[defect_location_y]
					FROM
						machine_defect_log mdl
						LEFT JOIN machine_defect_reasons d ON mdl.defect_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						--JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						JOIN tools t ON mdl.tool_ID = t.ID
						JOIN tool_parts tp ON t.ID = tp.tool_ID AND tp.part_ID = mdl.part_ID
						JOIN machine_defect_locations locs ON locs.machine_defect_ID = mdl.ID
					WHERE
						mdl.defect_ID IS NOT NULL";

		$query .= $where;
		$query .= " GROUP BY defect_location_x, defect_location_y, tp.ID, mdl.defect_origin_ID";

		$results->locations = $db->query($query);
		$results->loctions_query = $query;
		//die($query);
		echo json_encode($results);
	}
?>