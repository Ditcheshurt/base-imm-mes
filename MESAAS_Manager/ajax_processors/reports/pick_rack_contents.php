<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;
	
if ($action == 'get_pick_rack_contents') {
    global $db;
    $res = new StdClass();

    //pick rack content data
    $sql = "SELECT
                build_order,
                sequence,
                serial,
                pick_rack_number,
                pocket,
                status
            FROM
                v_pick_rack_contents
            WHERE
                pick_rack_number = ".$_REQUEST['pick_rack_number']."
            ORDER BY
                pick_rack_number ASC,
                pocket ASC;";

    $res = $db->query($sql);
    if (!$res) {
        $res = array();
    }
    echo json_encode($res);
}

if ($action == 'get_pick_racks') {
    global $db;
    $res = new StdClass();

    //pick rack numbers
    $sql = "SELECT
                pick_rack_number
            FROM
                pick_racks
            GROUP BY
                pick_rack_number
            ORDER BY
                pick_rack_number;";

    $res = $db->query($sql);
    if (!$res) {
        $res = array();
    }
    echo json_encode($res);
}   

?>