<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_select_content($request, $orm) {

		$response = new stdClass();

		foreach ($request["get"] as $select) {
			switch($select) {

				case "machines":
					$response->machines = sel_machines($orm);
				break;

				case "sub_machines":
					$response->sub_machines = sel_sub_machines($orm);
				break;

				case "shifts":
					$response->shifts = sel_shifts($orm);
				break;

				case "parts":
					$response->parts = sel_parts($orm);
				break;

				case "reasons":
					$response->reasons = sel_reasons($orm);
				break;

				case "tools":
					$response->tools = sel_tools($orm);
				break;

			}
		}

		echo json_encode($response);

	}

	function run_report($request, $db) {
		//define group bys
		require("../../common/library/php/groupBy.php");
		$gby = new groupBy();
		$gby->set_group("date", "CONVERT(VARCHAR(10), mdl.change_time, 103)");
		$gby->set_group("machine", "m.machine_name");
		$gby->set_group("sub_machine", "sm.sub_machine_name");
		$gby->set_group("reason_code", "d.reason_description");
		$gby->set_group("week", "DATEPART(wk, mdl.change_time)");
		$gby->set_group("shift", "dbo.getShift(m.system_ID, mdl.change_time)");
		$gby->set_group("hour", "DATEPART(hh, mdl.change_time)");
		//$gby->set_group("hour", "CAST(mdl.change_time AS date)", 'date');
		//get group by based on request
		if (isset($request['group_by']))
			$group_by = $gby->get_group_by($request["group_by"]);
		else
			$group_by = $gby->get_group_by(array());

		$downtime = "dbo.getDowntime(mdl.ID, '{$request["from_date"]}', '{$request["to_date"]}', 0)";

		$query = "SELECT ".implode(",", $group_by->selects).",";

		if (isset($request["sel_uptime"])) {
			$query .= "CAST((DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') - ROUND(SUM({$downtime}), 3)) as float)
						/ CAST(DATEDIFF(minute, '{$request['from_date']}', '{$request['to_date']}') as float) * 100.0 as 'Uptime (%)'";
		} else {
			$query .= "ROUND(SUM({$downtime}), 3) As 'Downtime (min)'";
		}

		$query .= " FROM
						machine_downtime_log mdl
						JOIN v_downtime_reasons d ON mdl.reason_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						--JOIN tools t on mdl.tool_ID = t.ID
					WHERE
						mdl.up = 0
						AND mdl.time_in_state IS NOT NULL
						AND mdl.reason_ID IS NOT NULL
						AND mdl.change_time >= '{$request["from_date"]}'
						AND mdl.change_time <= '{$request["to_date"]}'";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["sel_machines"]))
			$query .= " AND mdl.machine_ID IN (" . implode(",", $request["sel_machines"]) . ") ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND mdl.sub_machine_ID IN (" . implode(",", $request["sel_sub_machines"]) . ") ";

		if (isset($request["sel_reasons"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_reasons"]) . "%'";

		$query .= " GROUP BY " . implode(",", $group_by->groups);
		$query .= " ORDER BY " . implode(",", $group_by->groups);

		echo json_encode($db->query($query));
		
	}
	
	function drilldown($request, $db) {

		$downtime = "dbo.getDowntime(mdl.ID, '{$request["from_date"]}', '{$request["to_date"]}', 0)";

		$query = "SELECT
						CONVERT(VARCHAR, mdl.change_time,103)+' '+CONVERT(VARCHAR, mdl.change_time,108) as 'Datetime',
						m.machine_name as 'Machine',
						sm.sub_machine_name as 'Sub Machine',
						--t.short_description as 'Tool',
						d.reason_description as 'Reason',
						mdl.comment as 'Comment',
						ROUND(SUM({$downtime}), 3) As 'Downtime (min)'
					FROM
						machine_downtime_log mdl
						JOIN v_downtime_reasons d ON mdl.reason_ID = d.ID
						JOIN machines m ON m.ID = mdl.machine_ID
						LEFT OUTER JOIN sub_machines sm ON sm.ID = mdl.sub_machine_ID
						--JOIN tools t on mdl.tool_ID = t.ID
					WHERE
						mdl.up = 0
						AND mdl.reason_ID IS NOT NULL
						AND mdl.change_time >= '{$request["from_date"]}'
						AND mdl.change_time <= '{$request["to_date"]}' ";

		if (isset($request['date']))
			$query .= " AND CONVERT(VARCHAR(10), mdl.change_time, 103) = '{$request['date']}'";

		if (isset($request['week']))
			$query .= " AND DATEPART(wk, mdl.change_time) = {$request['week']}";

		if (isset($request['hour']))
			$query .= " AND DATEPART(hh, mdl.change_time) = {$request['hour']}";

		if (isset($request["shift"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) = " . $request["shift"] . " ";

		if (isset($request["sel_shifts"]))
			$query .= " AND dbo.getShift(m.system_ID, mdl.change_time) IN (" . implode(",", $request["sel_shifts"]) . ") ";

		if (isset($request["machine"]))
			$query .= " AND m.machine_name = '" . $request["machine"] . "' ";

		if (isset($request["sel_machines"]))
			$query .= " AND mdl.machine_ID IN (" . implode(",", $request["sel_machines"]) . ") ";

		if (isset($request["sub_machine"]))
			$query .= " AND sm.sub_machine_name = '" . $request["sub_machine"] . "' ";

		if (isset($request["sel_sub_machines"]))
			$query .= " AND mdl.sub_machine_ID IN (" . implode(",", $request["sel_sub_machines"]) . ") ";

		if (isset($request["sel_reasons"]))
			$query .= " AND d.S LIKE '%" . implode(",", $request["sel_reasons"]) . "%'";

		if (isset($request["reason_code"]))
			$query .= " AND d.reason_description = '" . $request["reason_code"] . "'";

		$query .= "GROUP BY
					mdl.change_time,
					m.machine_name,
					sm.sub_machine_name,
					--t.short_description,
					d.reason_description,
					mdl.comment ";

		//echo $query;die;
		echo json_encode($db->query($query));

	}

	function sel_reasons($orm) {
		$query = "SELECT S as 'value', grp + ':' + reason_description as 'text' FROM v_downtime_reasons ORDER BY S";
		return $orm->query($query, false);
	}

	function sel_parts($orm) {
		$query = "SELECT ID as 'value', sub_tool_desc + ' (' + part_number + ')' as 'text' FROM tool_parts ORDER BY part_number";
		return $orm->query($query, false);
	}

	function sel_machines($orm) {
		$strSQL = "SELECT ID as 'value', machine_name as 'text' FROM machines ORDER BY machine_name";
		return $orm->query($strSQL);
	}

	function sel_sub_machines($orm) {
		$strSQL = "SELECT ID as 'value', sub_machine_name as 'text' FROM sub_machines ORDER BY sub_machine_name";
		return $orm->query($strSQL, false);
	}

	function sel_tools($orm) {
		$strSQL = "SELECT ID as 'value', short_description as 'text' FROM tools ORDER BY short_description";
		return $orm->query($strSQL, false);
	}

	function sel_shifts($orm) {

		$obj_1 = new stdClass();
		$obj_1->text = "Shift 1";
		$obj_1->value = "1";

		$obj_2 = new stdClass();
		$obj_2->text = "Shift 2";
		$obj_2->value = "2";

		$obj_3 = new stdClass();
		$obj_3->text = "Shift 3";
		$obj_3->value = "3";

		return array($obj_1, $obj_2, $obj_3);

	}
?>