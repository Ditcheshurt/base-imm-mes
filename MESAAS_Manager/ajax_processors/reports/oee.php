<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];


	if ($action == 'get_oee_summary') {
		global $db;
		$res = new StdClass();

		$system_ID = $_REQUEST["system_ID"];

		if ($_REQUEST['shift'] > -1) {
			$shift_str = "AND ss.the_shift = ".$_REQUEST['shift'];
		} else {
			$shift_str = "";
		}
		if ($_REQUEST['machine_ID'] > -1) {
			$machine_str = "AND ss.machine_ID = ".$_REQUEST['machine_ID'];
		} else {
			$machine_str = "";
		}
		if ($_REQUEST['tool_ID'] > -1) {
			$tool_str = " AND ss.tool_ID = ".$_REQUEST['tool_ID'];
		} else {
			$tool_str = "";
		}
		
		$sql = "SELECT
					ISNULL(SUM(ss.part_qty), 0) AS part_qty,
					ISNULL(SUM(ss.part_target_qty), 0) AS part_target_qty,
					ISNULL(SUM(ss.part_goal), 0) AS part_goal,
					ISNULL(SUM(ss.scrap_numerator), 0) AS scrap_numerator,
					ISNULL(SUM(ss.scrap_denominator), 0) AS scrap_denominator,
					ISNULL(SUM(ss.scrap_denominator - ss.scrap_numerator), 0) AS scrap,
					ISNULL(SUM(ss.total_minutes), 0) AS total_minutes,
					ISNULL(SUM(ss.downtime_minutes), 0) AS downtime_minutes
				FROM
					shift_summary ss
					JOIN machines m ON m.ID = ss.machine_ID AND m.system_ID = $system_ID
				WHERE
					ss.the_date >= '".$_REQUEST['start_date']." 00:00:00'
					AND ss.the_date <= '".$_REQUEST['end_date']." 23:59:59'
					".$shift_str."
					".$machine_str."
					".$tool_str."
					AND m.show_reporting = 1;";
		//die($sql);
		$res = $db->query($sql);
		if ($res) {
			$res = $res[0];
			calc_oee($res);
		} else {
			$res = array();
		}
		echo json_encode($res);
	}


	if ($action == 'get_oee_data') {
		global $db;
		$res = new StdClass();

		$system_ID = $_REQUEST["system_ID"];

		if ($_REQUEST['shift'] > -1) {
			$shift_str = "AND ss.the_shift = ".$_REQUEST['shift'];
		} else {
			$shift_str = "";
		}
		if ($_REQUEST['machine_ID'] > -1) {
			$machine_str = "AND ss.machine_ID = ".$_REQUEST['machine_ID'];
		} else {
			$machine_str = "";
		}
		if ($_REQUEST['tool_ID'] > -1) {
			$tool_str = " AND ss.tool_ID = ".$_REQUEST['tool_ID'];
		} else {
			$tool_str = "";
		}

		$sql = "SELECT
					ss.machine_ID,
					m.machine_number,
					m.machine_name,
					t.short_description AS [tool_desc],
					SUM(ss.part_qty) AS part_qty,
					SUM(ss.part_target_qty) AS part_target_qty,
					SUM(ss.part_goal) AS part_goal,
					SUM(ss.scrap_numerator) AS scrap_numerator,
					SUM(ss.scrap_denominator) AS scrap_denominator,
					SUM(ss.scrap_denominator - ss.scrap_numerator) AS scrap,
					SUM(ss.total_minutes) AS total_minutes,
					SUM(ss.downtime_minutes) AS downtime_minutes
				FROM
					shift_summary ss
					JOIN machines m ON m.ID = ss.machine_ID AND m.system_ID = $system_ID
					LEFT JOIN tools t ON t.ID = ss.tool_ID
				WHERE
					ss.the_date >= '".$_REQUEST['start_date']." 00:00:00'
					AND ss.the_date <= '".$_REQUEST['end_date']." 23:59:59'
					".$shift_str."
					".$machine_str."
					".$tool_str."
					AND m.show_reporting = 1
				GROUP BY
					ss.machine_ID,
					m.machine_number,
					m.machine_name,
					t.short_description;";
		//die($sql);
		$res = $db->query($sql);

		if ($res) {
			foreach ($res as $i => &$machine) {
				calc_oee($machine);
			}
		} else {
			$res = array();
		}

		echo json_encode($res);
	}

	function calc_oee(&$data) {
		$data['performance'] = 0;
		$data['quality'] = 0;
		$data['availability'] = 0;

		if ($data['part_target_qty']) {
			$data['performance'] = $data['part_qty'] / $data['part_target_qty'];
		}
		if ($data['scrap_denominator'] != 0) {
			$data['quality'] = $data['scrap_numerator'] / $data['scrap_denominator'];
		}
		if ($data['total_minutes']) {
			$data['availability'] = ($data['total_minutes'] - $data['downtime_minutes']) / $data['total_minutes'];
		}
		
		$data['oee'] = $data['performance'] * $data['quality'] * $data['availability'];
	}

?>