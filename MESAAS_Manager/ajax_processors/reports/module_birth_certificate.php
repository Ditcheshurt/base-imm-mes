<?php

    require_once("../../../init.php");

    $action = $_REQUEST['action'];

    call_user_func($action, $_REQUEST, $db);

    function get_module ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = "";
        $strDB = $result[0]['db'];        

        $query = "SELECT
                    m.ID,
                    m.build_order,
                    m.sequence,
                    m.VIN,
                    m.line_ID,
                    l.line_code,
                    o.name AS operator_name,
                    m.recvd_time,
                    m.loaded_time,
                    m.built_time,
                    m.reject_status
                FROM
                    {$strDB}modules m
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON o.ID = m.operator_ID
                    LEFT JOIN {$strDB}lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = {$request['module_ID']};";
        $result = $db->query($query);

        if ($result) {
            $data = $result[0];
        } else {
            $data = $result;
        }

        echo json_encode($data);
    }

    function get_components ($request, $db) {

        // $query = "SELECT
        //             part_number,
        //             qty
        //         FROM
        //             components_in
        //         WHERE
        //             veh_id = {$request['module_ID']}
        //         ORDER BY part_number ASC;";
        // $result = $db->query($query);
        
        //get the archive db 
        $query = "SELECT dbo.getModuleProdDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db'];    

        $query = "SELECT
                    ci.part_number,
                    ci.qty,
                    mrp.part_description
                    
                FROM
                    {$strDB}components_in ci
                    LEFT JOIN {$strDB}QAD_PART_MSTR mrp ON mrp.cp_cust_part = ci.part_number
                WHERE
                    ci.veh_id = {$request['module_ID']}
				GROUP BY
                    ci.part_number,
                    ci.qty,
                    mrp.part_description
                    
                ORDER BY
                    part_number ASC;";
		//die($query);
        $result = $db->query($query);

        //echo $query;
        echo json_encode($result);
    }

    function get_station_history ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleProdDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 

        // station history
/*         $sql = "SELECT DISTINCT
                    ls.ID AS real_station_ID,
                    msh.station_ID,
                    ls.station_order,
                    ls.station_desc,
                    msh.station_instance,
                    o.name AS operator_name,
                    msh.entry_time,
                    msh.exit_time,
                    DATEDIFF(SECOND, msh.entry_time, msh.exit_time) AS cycle_time
                FROM
                    module_station_history msh
                    JOIN line_stations ls ON ls.line_ID = msh.line_ID
                        AND ls.station_ID = msh.station_ID
                        AND (ls.station_options LIKE '%'+msh.station_instance+'%' OR msh.station_instance IS NULL)
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msh.module_ID = {$request['module_ID']}
                ORDER BY ls.station_order ASC;"; */
			
		$sql = "SELECT DISTINCT
                    ls.station_ID AS real_station_ID,
                    msh.station_ID,
                    ls.station_order,
                    ls.station_desc,
                    --msh.station_instance,
                    o.name AS operator_name,
                    msh.entry_time,
                    msh.exit_time,
                    DATEDIFF(SECOND, msh.entry_time, msh.exit_time) AS cycle_time
                FROM
                    {$strDB}module_station_history msh
					JOIN (
						SELECT
							station_ID,
							line_ID,
							station_order,
							station_desc
						FROM
							{$strDB}line_stations
						GROUP BY
							line_ID,
							station_ID,
							station_order,
							station_desc
					) ls ON ls.line_ID = msh.line_ID
                        AND ls.station_ID = msh.station_ID
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msh.module_ID = {$request['module_ID']}
                ORDER BY ls.station_order ASC;";

        //die($sql);
        $result = $db->query($sql);

        if ($result) {
            foreach($result as $i => &$station) {
                // station instruction history
                $sql = "SELECT DISTINCT
                            msih.ID,
                            ls.station_id,
                            ls.station_desc,
                            msih.instr_order,
                            tt.type_desc,
                            msih.start_time,
                            msih.complete_time,
                            msih.completion_details
							,ethd.ID AS EOL_test_ID, CASE WHEN ethd.passed IS NOT NULL THEN CONCAT(ethd.test_type, ' (',ethd.test_item,')') END AS eol_test, CASE WHEN ethd.passed = 1 THEN 'Passed' ELSE ethd.string_val END AS eol_result
                        FROM
                            {$strDB}module_station_instruction_history msih
                            LEFT JOIN
                                {$strDB}instructions i ON i.part_number = msih.part_number
                                AND i.instr_order = msih.instr_order
                                AND i.station_id = msih.station_id
                                AND i.part_order = msih.part_order
                            LEFT JOIN
                                {$strDB}test_types tt ON tt.ID = i.test_type
                            LEFT JOIN
                                {$strDB}line_stations ls ON ls.station_id = msih.station_id
							LEFT JOIN {$strDB}EOL_test_history_detail ethd ON msih.completion_details LIKE 'TEST ID:%' AND ethd.test_history_ID LIKE REPLACE(msih.completion_details,'TEST ID: ','') --AND ethd.test_type = 'CAMERA'
                        WHERE
                            msih.module_ID = {$request['module_ID']}
                            AND ls.station_ID = {$station['real_station_ID']}
                        ORDER BY
                                ls.station_id,
                                msih.instr_order
								,msih.start_time,ethd.ID;";
                //die($sql);
                $station['station_instruction_history'] = $db->query($sql);
            }
        }
        
        echo json_encode($result);
    }

    function get_rework_station_history ($request, $db) {

        $sql = "SELECT
                    ls.ID AS real_station_ID,
                    ls.station_ID,
                    ls.station_desc,
                    max(mdrih.complete_time) AS complete_time,
                    mdrih.operator_ID,
                    o.name AS operator_name
                FROM
                    module_defect_repair_instruction_history mdrih
                    LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                    LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                    LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                WHERE
                    mdrih.module_ID = {$request['module_ID']}
                GROUP BY
                    ls.ID,
                    ls.station_ID,
                    ls.station_desc,
                    mdrih.operator_ID,
                    o.name
                ORDER BY
                    ls.station_ID ASC;";

        //die($sql);
        $result = $db->query($sql);

        if ($result) {
            foreach ($result as $i => &$station) {
                $sql = "SELECT
                            ls.station_ID,
                            ls.station_desc,
                            i.instr_order,
                            tt.type_desc,
                            mdrih.complete_time,
                            mdrih.completion_details,
                            o.name AS operator_name,
                            mdrih.operator_ID
                        FROM
                            module_defect_repair_instruction_history mdrih
                            LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                            LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                            LEFT JOIN test_types tt ON tt.ID = i.test_type
                            LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                        WHERE
                            mdrih.module_ID = {$request['module_ID']}
                            AND ls.ID = {$station['real_station_ID']}
                        ORDER BY
                            i.station_ID ASC,
                            i.instr_order ASC,
                            mdrih.complete_time ASC;";

                $station['rework_instruction_history'] = $db->query($sql);
            }
        }
        
        echo json_encode($result);
    }

    function get_sub_assembly_history ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleProdDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 

        //get wip parts
        $sql = "SELECT
                    *
                FROM (
                    (
                        -- sub assemblies
                        SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code,
                            wp.line_ID,
                            isnull(wp.revision_level,'') as revision_level
                        FROM
                            {$strDB}wip_parts wp
                            LEFT JOIN {$strDB}wip_part_setup wps ON wps.ID = wp.part_ID
                            LEFT JOIN {$strDB}lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_module_ID = {$request['module_ID']}
                            AND wp.used_time IS NOT NULL
                            AND l.line_type IN (1, 2)
                    ) UNION (
                        -- small part kitting
                        SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code,
                            wp.line_ID,
                            isnull(wp.revision_level,'') as revision_level
                        FROM
                            {$strDB}wip_parts wp
                            LEFT JOIN {$strDB}wip_part_setup wps ON wps.ID = wp.part_ID OR (wp.part_ID = 0 AND wps.line_ID = wp.line_ID)
                            LEFT JOIN {$strDB}lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_module_ID = {$request['module_ID']}
                            AND wp.used_by_wip_part_ID IS NULL
                            AND l.line_type = 5
                    )
                ) AS subs
                ORDER BY
                    line_code ASC;";
        //die($sql);
        $wip_parts = $db->query($sql);

        if ($wip_parts) {
            // get wip station history
            foreach($wip_parts as $i => &$part) {
                //get wip parts wip parts
                $sql = "SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code
                            
                        FROM
                            {$strDB}wip_parts wp
                            LEFT JOIN {$strDB}wip_part_setup wps ON wps.ID = wp.part_ID
                            LEFT JOIN {$strDB}lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_wip_part_ID = {$part['id']}
                            AND wp.used_time IS NOT NULL
                            AND l.line_type IN (1, 2);";

                $part['wip_parts'] = $db->query($sql);

                $sql = "SELECT
                            ls.station_desc,
                            wpsh.entry_time,
                            wpsh.exit_time,
                            op.name AS operator_name,
                            wpsh.station_ID
                        FROM
                            {$strDB}wip_part_station_history wpsh
                            LEFT JOIN {$strDB}operators op ON op.id = wpsh.operator_id
                            LEFT JOIN (SELECT DISTINCT station_ID, station_desc FROM {$strDB}line_stations) ls ON ls.station_id = wpsh.station_id
                        WHERE
                            wip_part_id = {$part['id']};";

                $part['station_history'] = (array) $db->query($sql);
                //die(json_encode($part));

                if ($part['station_history']) {
                    // get wip instruction history
                    foreach($part['station_history'] as $j => &$station) {
                        $sql = "SELECT DISTINCT
                                    wpsih.ID,
                                    ls.station_id,
                                    wpsih.step_order,
                                    tt.type_desc,
                                    wpsih.entry_time,
                                    wpsih.exit_time,
                                    wpsih.completion_details
                                FROM
                                    {$strDB}wip_part_station_instruction_history wpsih
                                    LEFT JOIN
                                        {$strDB}instructions i ON i.part_number = wpsih.part_number
                                        AND i.instr_order = wpsih.step_order
                                        AND i.station_id = wpsih.station_id
                                        AND i.part_order = wpsih.part_order
                                    LEFT JOIN
                                        {$strDB}test_types tt ON tt.ID = i.test_type
                                    LEFT JOIN
                                        (SELECT DISTINCT station_ID FROM {$strDB}line_stations) AS ls ON ls.station_id = wpsih.station_id
                                WHERE
                                    wpsih.wip_part_ID = {$part['id']}
                                    AND wpsih.station_id = {$station['station_ID']}
                                ORDER BY wpsih.step_order;";

                        $station['instruction_history'] = $db->query($sql);
                    }
                }

                $sql = "SELECT TOP 1 * FROM {$strDB}INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wip_part_process_data'";
                $table_data = $db->query($sql);
                if ($table_data) {
                    // get process data
                    $sql = "SELECT
                                wppd.ID,
                                wppd.process_location,
                                REPLACE(REPLACE(SUBSTRING(wppd.process_tag, CHARINDEX('.', wppd.process_tag), LEN(wppd.process_tag)), '.', ''), '_', ' ') AS process_desc,
                                wppd.process_value_desc,
                                wppd.process_value,
                                wppd.process_timestamp,
                                dbo.fn_getJSONStringValue(ls.station_options, 'instance_desc') AS instance_desc
                            FROM
                                {$strDB}wip_part_process_data wppd
                                LEFT JOIN {$strDB}line_stations ls ON dbo.fn_getJSONIntegerValue(ls.station_options, 'station_instance') = CAST(RIGHT(wppd.process_location, LEN(wppd.process_location) - 5) AS INT)
                            WHERE
                                wppd.wip_part_ID = {$part['id']}
                            ORDER BY
                                wppd.process_location ASC,
                                wppd.ID ASC;";

                    $part['process_data'] = $db->query($sql);
                } else {
                    $part['process_data'] = false;
                }


                if ($part['line_ID'] == 160) {
                    // get poly/iso scans
                    $sql = "SELECT
                                completion_details AS poly_scan
                            FROM
                                {$strDB}wip_part_station_instruction_history 
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 25;";
                    $polys = $db->query($sql);
                    $part['poly_scan'] = $polys[0]['poly_scan'];

                    $sql = "SELECT
                                completion_details AS iso_scan
                            FROM
                                {$strDB}wip_part_station_instruction_history 
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 26;";
                    $isos = $db->query($sql);
                    $part['iso_scan'] = $isos[0]['iso_scan'];

                    // get airbag serial
                    $sql = "SELECT
                                CAST(SUBSTRING(completion_details, 6, LEN(completion_details)) AS INT) AS serial
                            FROM
                                NYSUS_SEQUENCE_MES.dbo.wip_part_station_instruction_history
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160010
                                AND part_order = 10
                                AND step_order = 20;";
                    $airbag_serial = $db->query($sql);
                    if (!$airbag_serial) {
                        $airbag_serial = array(0 => array('serial' => -1));
                    }

                    // get skin serial
                    $sql = "SELECT
                                CAST(SUBSTRING(completion_details, 6, LEN(completion_details)) AS INT) AS serial
                            FROM
                                NYSUS_SEQUENCE_MES.dbo.wip_part_station_instruction_history
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 30;";
                    //die($sql);
                    $skin_serial = $db->query($sql);
                    if (!$skin_serial) {
                        $skin_serial = array(0 => array('serial' => -1));
                    }

                    // get airbag and skin
                    $sql = "SELECT
                                wp.ID,
                                created_at AS built_time,
                                wp.internal_serial,
                                ps.cell_ID AS line_ID,
                                c.cell_desc AS line_code,
                                ps.part_desc
                            FROM
                                NYSUS.dbo.MESAAS_wip_parts wp
                                LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = wp.part_ID
                                LEFT JOIN NYSUS.dbo.cells c ON c.ID = ps.cell_ID
                            WHERE
                                wp.ID = {$airbag_serial[0]['serial']}
                                OR wp.ID = {$skin_serial[0]['serial']};";
                    //die($sql);
                    $part['batch_parts'] = $db->query($sql);
                }
            }
        }
        
        echo json_encode($wip_parts);
    }

    function get_batch_assembly_history ($request, $db) {
        // get part
        $sql = "SELECT
                    wp.ID,
                    created_at AS built_time,
                    wp.internal_serial,
                    ps.cell_ID AS line_ID,
                    c.cell_desc AS line_code,
                    ps.part_desc
                FROM
                    NYSUS.dbo.MESAAS_wip_parts wp
                    LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = wp.part_ID
                    LEFT JOIN NYSUS.dbo.cells c ON c.ID = ps.cell_ID
                WHERE
                    wp.ID = {$request['wip_part_ID']};";
        $p = $db->query($sql);

        if ($p) {
            $part = $p[0];

            // get batch wip station history
            $sql = "SELECT
                        wpsh.wip_part_ID,
                        s.station_desc,
                        NULL AS entry_time,
                        NULL AS exit_time,
                        op.name AS operator_name,
                        wpsh.station_order,
                        ps.cell_ID
                    FROM
                        NYSUS.dbo.MESAAS_wip_parts w
                        LEFT JOIN (SELECT DISTINCT wip_part_ID, station_order, operator_ID FROM NYSUS.dbo.MESAAS_wip_part_step_history) wpsh ON wpsh.wip_part_ID = w.ID
                        JOIN MES_COMMON.dbo.operators op ON op.id = wpsh.operator_id
                        JOIN NYSUS.dbo.part_setup ps ON ps.ID = w.part_ID
                        LEFT JOIN NYSUS.dbo.stations s ON s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                    WHERE
                        wpsh.wip_part_ID = {$part['ID']}
                    GROUP BY
                        wpsh.wip_part_ID,
                        s.station_desc,
                        op.name,
                        wpsh.station_order,
                        ps.cell_ID
                    ORDER BY
                        wpsh.station_order ASC;";
            //die($sql);

            $part['station_history'] = $db->query($sql);

            if ($part['station_history']) {
                foreach($part['station_history'] as $j => &$station) {
                    // get wip instruction history
                    $sql = "SELECT
                                wpsh.ID,
                                wpsh.start_time AS entry_time,
                                wpsh.complete_time AS exit_time,
                                op.name AS operator_name,
                                wpsh.instruction_step_order,
                                wpsh.completion_details,
                                tt.type_desc,
                                i.text AS instruction_text
                            FROM
                                NYSUS.dbo.MESAAS_wip_part_step_history wpsh
                                LEFT JOIN NYSUS.dbo.MESAAS_wip_parts w ON w.ID = wpsh.wip_part_ID
                                LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = w.part_ID
                                LEFT JOIN NYSUS.dbo.stations s ON  s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                                LEFT JOIN MES_COMMON.dbo.operators op ON op.ID = wpsh.operator_ID
                                LEFT JOIN NYSUS.dbo.instructions i ON
                                    i.cell_ID = ps.cell_ID
                                    AND i.station_order = wpsh.station_order
                                    AND i.instr_order = wpsh.instruction_step_order
                                    AND i.part_ID = ps.ID
                                LEFT JOIN NYSUS.dbo.test_types tt ON tt.ID = i.test_type
                            WHERE
                                wpsh.wip_part_ID = {$part['ID']}
                                AND s.station_order = {$station['station_order']}
                            ORDER BY
                                wpsh.instruction_step_order ASC;";

                    $station['instruction_history'] = $db->query($sql);
                }
            }
        }

        
        echo json_encode($part);
    }

    function get_eol_test_data ($request, $db) {
		//get the archive db 
        $query = "SELECT dbo.getModuleProdDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = "";
        $strDB = $result[0]['db']; 

		$sql = "SELECT line_ID FROM {$strDB}modules WHERE ID = {$request['module_ID']}";
		$result = $db->query($sql);
		if ($result) {
			$line_ID = $result[0]['line_ID'];
		} else {
			$line_ID = null;
		}

        // new EOL
		$sql = '';
		if($line_ID == '165'){
			$sql = "SELECT
					t.ID,
					t.test_start_time,
					t.test_end_time,
					MIN(t.result) AS result,
                    '' AS detail
				FROM (
					SELECT
						th.ID,
						th.test_start_time,
						th.test_end_time,
						MAX(CAST(tv.passed AS TINYINT)) AS result
					FROM
						{$strDB}EOL_test_history th
						RIGHT JOIN {$strDB}EOL_test_history_detail tv ON tv.test_history_ID = th.ID
					WHERE
						th.mod_ID = {$request['module_ID']}
					GROUP BY
						th.ID,
						th.test_start_time,
						th.test_end_time
				) AS t
				GROUP BY
					t.ID,
					t.test_start_time,
					t.test_end_time
				ORDER BY
					t.test_start_time ASC;";
		}
		else{
        $sql = "SELECT
					t.ID,
					t.test_start_time,
					t.test_end_time,
					MIN(t.result) AS result,
                    '' AS detail
				FROM (
					SELECT
						th.ID,
						th.test_start_time,
						th.test_end_time,
						MAX(CAST(tv.passed AS TINYINT)) AS result
					FROM
						{$strDB}EOL_test_history th
						RIGHT JOIN {$strDB}EOL_test_history_values tv ON tv.EOL_test_ID = th.ID
					WHERE
						th.mod_ID = {$request['module_ID']}
					GROUP BY
						th.ID,
						th.test_start_time,
						th.test_end_time
				) AS t
				GROUP BY
					t.ID,
					t.test_start_time,
					t.test_end_time
				ORDER BY
					t.test_start_time ASC;";
		}
		//die($sql);
        $result = $db->query($sql);

        if ($result) {
            // EOL test detail
            foreach($result as $k => &$test) {
				if($line_ID == '165'){
					$sql = "SELECT	
						thd.ID,
						thd.test_history_ID,
						thd.test_type,
						thd.test_item,
						thd.min_float_val,
						thd.max_float_val,
						thd.float_val,
						thd.string_val,
						thd.passed,
						thd.float_val AS value,
						(thd.test_type + ' ' + thd.test_item) AS label
						FROM
							{$strDB}EOL_test_history_detail thd
						WHERE
							thd.test_history_ID = {$test['ID']}
						ORDER BY
							thd.ID;";
				}
				else{
					$sql = "SELECT
								tv.ID,
								tv.board,
								tv.channel,
								tv.value,
								tv.uom,
								tv.passed,
								cs.type,
								cs.min,
								cs.max,
								cs.label
							FROM
								{$strDB}EOL_test_history_values tv
								LEFT JOIN {$strDB}EOL_channel_setup cs ON tv.board = cs.board AND tv.channel = cs.channel 
							WHERE
								tv.EOL_test_ID = {$test['ID']}
							ORDER BY
								tv.ID;";
				}
                
                $test['detail'] = $db->query($sql);
                //echo $sql;
            }
        }
        
        echo json_encode($result);
    }
	
	function get_dimensional_data ($request, $db){
		$sql = "SELECT dr.*,
	   CASE WHEN dr.value > ld.high_set_point THEN 0
			WHEN dr.value < ld.low_set_point THEN 0
			ELSE 1 END AS passed
		FROM modules m
		JOIN dimensional_results dr ON dr.moduleID = m.ID
		JOIN liftgate_dimensional ld ON ld.Datapoint_Desc = dr.datapoint
		WHERE m.ID = {$request['module_ID']}
		ORDER BY dr.id";
		
		echo json_encode($db->query($sql));
	}

    function get_shipment_info ($request, $db) {
    	//get the archive db 
        $query = "SELECT dbo.getModuleDB({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = "";
        $strDB = $result[0]['db']; 

        $sql = "SELECT
                    s.ID,
                    s.ship_time,
                    RIGHT(CAST(sl.rack_order as varchar(10)), 3) AS rack_order,
                    sl.permanent_rack_ID,
                    si.lane_position,
                    s.shipment_sequence,
                    s.dock_number
                FROM
                    {$strDB}shipments s
                    LEFT JOIN {$strDB}shipment_lanes sl ON sl.shipment_id = s.ID
                    LEFT JOIN {$strDB}shipment_items si ON sl.ID = si.lane_ID
                WHERE
                    si.mod_ID = {$request['module_ID']}
                ORDER BY
                    si.lane_position";
        //die($sql);
        echo json_encode($db->query($sql));
    }

    function get_images ($request, $db) {
        $result = new StdClass();

        $sql = "SELECT sequence, VIN, built_time, line_ID FROM modules WHERE ID = {$request['module_ID']};";
        $modules = $db->query($sql);
        if ($modules) {
            $module = $modules[0];
        } else {
            $module = $modules;
        }

        if ($request['mrp_company_code'] == 'MEB') {
            $date_parts = explode('-', $module['built_time']->format('Y-m-d'));

            $result->main_line_images = array();
            //$dir_path = "/539IP/archive/{$date_parts[0]}/MONTH{$date_parts[1]}/DAY{$date_parts[2]}/MODULE_{$request['module_ID']}/";
			$dir_path = "/MESAAS/content/captured_images/MODULE_{$request['module_ID']}/";
			$real_path = "c:\\wwwroot\\inetpub\\MESAAS\\content\\captured_images\\MODULE_{$request['module_ID']}\\";
			
			if (is_dir($real_path)) {
				$rp = $real_path;
				$files = array_diff(scandir($rp), array('.', '..'));
				
				foreach($files as $i => $file_name) {
					array_push($result->main_line_images, $dir_path . $file_name);
				}
			}
			
            // for($i=1; $i<=4; $i++) {
                // $file_name = $module['sequence'] . '_' . $i . '.jpg';
                // array_push($result->main_line_images, $dir_path . $file_name);
            // }

            //chute images
            // $result->chute_images = array();
            // $real_path = "D:\\539_vision\\Archive\\{$date_parts[0]}\\MONTH{$date_parts[1]}\\DAY{$date_parts[2]}\\MODULE_{$request['module_ID']}";
            // $real_path_2 = "E:\\539IP\\archive\\{$date_parts[0]}\\MONTH{$date_parts[1]}\\DAY{$date_parts[2]}\\MODULE_{$request['module_ID']}";
            // $dir_path = "/539_vision_images/Archive/{$date_parts[0]}/MONTH{$date_parts[1]}/DAY{$date_parts[2]}/MODULE_{$request['module_ID']}/";

            
            // if ($module['line_ID'] == 400) {
                //539X RHD images
                // $image_names = array(
                    // '539xChute' => '',
                    // '539xFront' => '', 
                    // '539xRear' => ''
                // );

                // if (is_dir($real_path) || is_dir($real_path_2)) {
                    // if (is_dir($real_path)) {
                        // $rp = $real_path;
                    // } else {
                        // $rp = $real_path_2;
                    // }
                    // $files = array_diff(scandir($rp), array('.', '..'));
                    
                    // foreach($files as $i => $file_name) {
                        // foreach($image_names as $type => &$max) {
                            // $matches = array();
                            // preg_match('/^'.$type.'[0-9]+\.bmp$/', $file_name, $matches);
                            // if (!empty($matches) && $matches[0] > $max) {
                                // $max = $matches[0];     
                            // }
                        // }
                    // }

                    //no chute images for 540's
                    // if (substr($module['VIN'], 0, 2) == '2L') {
                        // $result->chute_images = null;
                    // } else {
                        // foreach($image_names as $type => $name) {
                            // array_push($result->chute_images, $dir_path . $name);
                        // }
                    // }
                // }
            // } else {
                //539 / 540 / 539X LHD images
                // $image_names = array(
                    // '539Chute' => '',
                    // '539Front' => '', 
                    // '539Rear' => ''
                // );

                // if (is_dir($real_path) || is_dir($real_path_2)) {
                    // if (is_dir($real_path)) {
                        // $rp = $real_path;
                    // } else {
                        // $rp = $real_path_2;
                    // }
                    // $files = array_diff(scandir($rp), array('.', '..'));
                    
                    // foreach($files as $i => $file_name) {
                        // foreach($image_names as $type => &$max) {
                            // $matches = array();
                            // preg_match('/^'.$type.'[0-9]+\.jpg$/', $file_name, $matches);
                            // if (!empty($matches) && $matches[0] > $max) {
                                // $max = $matches[0];     
                            // }
                        // }
                    // }

                    //no chute images for 540's
                    // if (substr($module['VIN'], 0, 2) == '2L') {
                        // $result->chute_images = null;
                    // } else {
                        // foreach($image_names as $type => $name) {
                            // array_push($result->chute_images, $dir_path . $name);
                        // }
                    // }
                // }
            // }
        // }
        
        echo json_encode($result);
    }
}

	function get_module_station_torque_values ($request, $db) {
    	//get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = "";
        $strDB = $result[0]['db']; 

		$sql = "SELECT
					tv.ID,
					tv.station_ID,
					ls.station_desc,
					tv.part_order,
					tv.part_number,
					tv.instr_order,
					tv.torque_time,
					tv.gun_number,
					tv.torque_value,
					tv.angle_value,
					tv.passed,
					tv.cleared,
					tv.attempt_number
				FROM
					{$strDB}module_station_torque_values tv
					LEFT JOIN {$strDB}line_stations ls ON ls.ID = tv.station_ID
				WHERE
					tv.module_ID = {$request['module_ID']}
				ORDER BY
					ls.station_order,
					tv.part_order,
					tv.instr_order,
					tv.torque_time;";
		$result = $db->query($sql);
		
		echo json_encode($result);
	}
	
	function get_module_stored_plc_values ($request, $db) {
		//get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = "";
        $strDB = $result[0]['db']; 

        $sql = "SELECT
					pv.ID,
					pv.station_ID,
					ls.station_desc,
					pv.the_time,
					pv.attempt_number,
					pv.test_status,
					pv.tag_desc,
					pv.tag_value
				FROM
					{$strDB}module_stored_plc_values pv
					LEFT JOIN {$strDB}line_stations ls ON ls.ID = pv.station_ID
				WHERE
					pv.module_ID = {$request['module_ID']}
				ORDER BY
					ls.station_order,
					pv.the_time,
					pv.tag_desc;";
		$result = $db->query($sql);
		
		echo json_encode($result);
	}

    function get_sr_message_info ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 
        
        $vin = $request['VIN'];
        $sql = "SELECT
                    SUBSTRING(raw_data, 11, 8) AS VIN,
                    SUBSTRING(raw_data, 24, 11) AS sequence,
                    sent_time,
                    raw_data
                FROM
                    {$strDB}broadcasts_out b
                    JOIN {$strDB}lines l ON SUBSTRING(b.queue, 4, 7) = SUBSTRING(l.queue_in, 10, 7) AND l.ID = {$request['line_ID']}
                WHERE
                    SUBSTRING(b.raw_data, 11, 8) = '".substr($vin, -8)."'
                    and msg_type = 'SR';";
		//die($sql);
        $result = $db->query($sql);
        
        echo json_encode($result);
    }
	
    function get_pick_info ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 
        
        $sql = "SELECT
                    prm.pocket,
                    prm.status,
                    prm.serial,
                    pr.start_time
                  FROM
                    {$strDB}modules m
                    LEFT OUTER JOIN {$strDB}pick_rack_modules prm ON prm.module_ID = m.ID
                    LEFT OUTER JOIN {$strDB}pick_racks pr ON pr.ID = prm.pick_rack_ID
                  WHERE
                    prm.module_ID = {$request['module_ID']};";
        $result = $db->query($sql);
        
        echo json_encode($result);
    }

    function get_packout_info ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 
        
        $sql = "SELECT
                    s.ID,
                    s.ship_time,
                    RIGHT(CAST(sl.rack_order AS varchar(10)), 3) AS rack_order,
                    permanent_rack_ID,
                    si.lane_position,
                    m.VIN,
                    m.sequence
                FROM
                    {$strDB}shipments s
                    LEFT OUTER JOIN {$strDB}shipment_lanes sl ON sl.shipment_id = s.ID
                    JOIN {$strDB}shipment_items si ON sl.ID = si.lane_ID
                    JOIN {$strDB}modules m ON si.mod_ID = m.ID
                WHERE
                    sl.ID = (SELECT lane_ID FROM shipment_items WHERE mod_ID = {$request['module_ID']})
                    and sl.module_line_id = m.line_id
                ORDER BY
                    si.lane_position;";
        $result = $db->query($sql);
        
        echo json_encode($result);
    }

    function get_bypass_info ($request, $db) {
        //get the archive db 
        $query = "SELECT dbo.getModuleDBProd({$request['module_ID']}) as db";
        $result = $db->query($query);
        $strDB = $result[0]['db']; 
        
        $sql = "SELECT
                    ls.station_desc,
                    --o.name,
                    msih.part_number,
                    msih.start_time,
                    msih.complete_time,
                    msih.completion_details
                FROM
                    {$strDB}modules m
                    LEFT OUTER JOIN {$strDB}module_station_instruction_history msih ON msih.module_ID = m.ID
                    LEFT OUTER JOIN {$strDB}lines l ON l.ID = m.line_ID
                    LEFT OUTER JOIN {$strDB}line_stations ls ON ls.station_ID = msih.station_ID
                    --LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msih.completion_details NOT LIKE '%undefined%' AND o.ID = LEFT(REPLACE(msih.completion_details, 'BYPASS: ', ''), 5)
                WHERE
                    msih.completion_details IS NOT NULL
                    AND msih.completion_details LIKE 'BYPASS: %'
                    AND m.ID = {$request['module_ID']};";
        //die($sql);
        $result = $db->query($sql);
        
        echo json_encode($result);
    }
?>