<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];


	if ($action == 'get_params') {
		global $db;
		$res = new StdClass();

		$sql = "SELECT
					dps.ID AS id
					,dps.data_point_desc AS text
				FROM
					data_point_setup dps
				WHERE
					dps.active = 1;";
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}

	if ($action == 'get_cycle_parts') {
		global $db;
		$res = new StdClass();

		$system_ID = $_REQUEST["system_ID"];

		if ($_REQUEST['machine_ID'] > -1) {
			$machine_str = "AND mc.machine_ID = ".$_REQUEST['machine_ID'];
		} else {
			$machine_str = "";
		}

		$sql = "SELECT
					mcp.ID AS [mcp_id]
					,mcp.cycle_ID
					,CONCAT(m.machine_name, ' (',m.machine_number,')') as [machine]
					,CONCAT(t.tool_description,' (',t.short_description,')') AS [tool]
					,CONCAT(p.part_desc, ' (',p.part_number,')') AS [part]
					,mcp.serial_number
					,mcp.full_barcode
					,mcp.insert_time
					,mcp.disposition
					,mcp.disposition_time
				FROM machine_cycle_parts mcp
				JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
				JOIN machines m ON m.ID = mc.machine_ID
				LEFT JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
				LEFT JOIN tools t ON t.ID = tp.tool_ID
				LEFT JOIN parts p ON p.ID = tp.part_ID
				WHERE mc.cycle_time BETWEEN '".$_REQUEST['start_date']."' AND '".$_REQUEST['end_date']."'
				".$machine_str."
				AND m.system_ID = ".$system_ID."
				ORDER BY m.machine_name, mc.cycle_time;";
		//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
?>