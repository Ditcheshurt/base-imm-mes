<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;
	
if ($action == 'list_built_parts') {
	global $db;
	$res = new StdClass();

	$sql = "SELECT
				bp.ID,
				bp.part_ID,
				ISNULL(bp.serial, bp.external_serial) as serial_number,
				bp.built_time,
				bp.last_station_ID,
				s. station_desc AS last_station_desc
			FROM
				built_parts bp
				LEFT JOIN (SELECT DISTINCT built_part_ID, cell_ID FROM built_part_history) bph ON bph.built_part_ID = bp.ID
				LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
				LEFT JOIN stations s ON s.cell_ID = ps.cell_ID AND s.station_order = bp.last_station_ID
			WHERE
				bp.built_time >= '".$_REQUEST['start_date']." 00:00:00'
				AND bp.built_time <= '".$_REQUEST['end_date']." 23:59:59'
				AND bph.cell_ID =  ".$_REQUEST['line_id'].";";

	$res = $db->query($sql);
	if (!$res) {
		$res = array();
	}
	echo json_encode($res);
}


?>