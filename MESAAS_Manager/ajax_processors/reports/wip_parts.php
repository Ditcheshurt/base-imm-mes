<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];
	$r = $_REQUEST;
	
if ($action == 'list_wip_parts') {
	global $db;
	$res = new StdClass();

	if ($_REQUEST['process_type'] == 'BATCH') {
		$sql = "SELECT
					w.ID,
					NULL AS build_order,
					NULL AS VIN,
					NULL AS station_desc,
					NULL AS sequence,
					ISNULL(w.internal_serial, w.external_serial) as serial_number,
					c.cell_desc AS line_code,
		            NULL AS built_time,
		            NULL AS last_completed_station_ID,
		            MAX(wpsh.station_order) AS last_completed_station_ID,
		            NULL AS last_completed_station_desc
				FROM MESAAS_wip_parts w
					JOIN part_setup ps ON ps.ID = w.part_ID
					JOIN cells c ON c.ID = ps.cell_ID
					--JOIN MESAAS_wip_part_step_history wpsh ON wpsh.wip_part_ID = w.ID
					JOIN (
						SELECT
							*
						FROM
							MESAAS_wip_part_step_history wpsh
						WHERE
							wpsh.complete_time >= '".$_REQUEST['start_date']." 00:00:00'
							AND wpsh.complete_time <= '".$_REQUEST['end_date']." 23:59:59'
					) AS wpsh ON wpsh.wip_part_ID = w.ID
				WHERE
					c.ID = ".$_REQUEST['line_id']."
				GROUP BY
					w.ID,
					w.internal_serial,
					w.external_serial,
					c.cell_desc,
					c.ID;";

	} else {
		$status = $_REQUEST['status'];
		$status_filter = "";
		if ($status == "-1") {
			$status_filter = "AND ((
	                w.built_time >= '".$_REQUEST['start_date']." 00:00:00'
	                AND w.built_time <= '".$_REQUEST['end_date']." 23:59:59'
	            ) OR w.built_time IS NULL)";
		} else if ($status == "0") {
		    $status_filter = "AND w.built_time IS NULL";
		} else if ($status == "1") {
			$status_filter = "AND (
	                w.built_time >= '".$_REQUEST['start_date']." 00:00:00'
	                AND w.built_time <= '".$_REQUEST['end_date']." 23:59:59'
	            )";
		}


		$sql = "SELECT
		            w.ID,
		            m.build_order,
		            m.VIN,
		            ls.station_desc,
		            m.sequence,
		            ISNULL(w.internal_serial, w.external_serial) as serial_number,
		            w.built_time,
		            w.last_completed_station_ID,
		            ls.station_desc AS last_completed_station_desc,
		            l.line_code
		        FROM wip_parts w
		            LEFT JOIN modules m ON w.used_by_module_ID = m.ID
		            LEFT JOIN line_stations ls ON ls.id = w.last_completed_station_ID
		            LEFT JOIN lines l ON l.id = ls.line_id
		        WHERE
		            w.line_ID = ".$_REQUEST['line_id']." ".$status_filter.";";		
	}

	$res = $db->query($sql);
	if (!$res) {
		$res = array();
	}
	echo json_encode($res);
}


?>