<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	/*fill the area select drop down*/
	if($action == "area_select") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT d.ID, d.department_name  
				FROM MES_COMMON.dbo.departments d
				ORDER BY d.department_name";

		$res = $db->query($sql);
		foreach ($res as $i => &$department) {
				$sql = "SELECT a.ID, a.area_desc 
				FROM MES_COMMON.dbo.areas a
				WHERE a.dept_ID = ".$department['ID'];

			$department['areas'] = $db->query($sql);
		}
		echo json_encode($res);
	}

	/*get data for operator selection*/
	else if ($action == "operator_select") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;	

			$sql = "SELECT o.ID, o.name
					FROM MES_COMMON.dbo.operators o
					LEFT JOIN MES_COMMON.dbo.operator_areas oa
					ON oa.operator_ID = o.ID
					WHERE oa.area_ID = ".$r["area_id"]."
					ORDER BY o.name ASC";

		$res = $db->query($sql);
		echo json_encode($res);
	}

	/*get data for table head*/
	else if ($action == "table_head") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

			$sql = "SELECT l.ojt_level, l.level_desc
					FROM MES_COMMON.dbo.ojt_level_descriptions l
					ORDER BY l.ojt_level";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	/*get data for populating table*/
	else if ($action == "training_history") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;	

		$sql = "SELECT ID, ojt_level, level_desc
				FROM MES_COMMON.dbo.ojt_level_descriptions
				WHERE dept_ID = ".$r["dept_id"]."";

		$res->level = $db->query($sql);

			$sql = "DECLARE @COLUMNS as VARCHAR(MAX)
					DECLARE @SQL as VARCHAR(MAX)

					SELECT @COLUMNS = COALESCE(@COLUMNS + ', ', '') + QUOTENAME(ojt_level)
					FROM MES_COMMON.dbo.ojt_level_descriptions

					SET @SQL = 'WITH pivot_data AS (
								SELECT s.the_date,j.job_name, ojt_level
								FROM MES_COMMON.dbo.ojt_job_signoffs s
								LEFT JOIN MES_COMMON.dbo.ojt_jobs j 
								ON s.ojt_job_ID = j.ID 
								LEFT JOIN MES_COMMON.dbo.operators o 
								ON s.operator_ID = o.ID 
								WHERE o.ID = ".$r["operator_id"].")
								SELECT job_name, ' + @COLUMNS + '
								FROM pivot_data
								PIVOT (
									MAX(the_date)
									FOR ojt_level
									IN (' + @COLUMNS + ')
								) AS pivot_results'
					EXEC(@SQL)";
		// die($sql);
		$res->qualification_dates = $db->query($sql);
		echo json_encode($res);
	}	
?>