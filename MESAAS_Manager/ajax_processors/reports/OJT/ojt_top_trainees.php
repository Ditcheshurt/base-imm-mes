<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	/*fill the area select drop down*/
	if($action == "area_select") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT d.ID, d.department_name  
				FROM MES_COMMON.dbo.departments d
				ORDER BY d.department_name";

		$res = $db->query($sql);
		foreach ($res as $i => &$department) {
				$sql = "SELECT a.ID, a.area_desc 
				FROM MES_COMMON.dbo.areas a
				WHERE a.dept_ID = ".$department['ID'];

			$department['areas'] = $db->query($sql);
		}
		echo json_encode($res);
	}

	/*get data to fill the table for most trained*/
	else if ($action == "selected_area_most") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;	

		$sql = "SELECT TOP 20 o.name as name_most, COUNT(*) as num_jobs_most
				FROM MES_COMMON.dbo.ojt_job_signoffs s
				JOIN MES_COMMON.dbo.operators o ON s.operator_ID = o.ID
				WHERE o.ID IN (SELECT operator_ID FROM MES_COMMON.dbo.operator_areas WHERE area_ID = ".$r["area_id"].")
				GROUP BY o.name 
				ORDER BY COUNT(*) DESC";

		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	/*get data to fill the table for least trained*/
	else if ($action == "selected_area_least") {
		global $db;
		$res = new stdClass();
		$r = $_REQUEST;

		$sql = "SELECT TOP 20 o.name as name_least, COUNT(*) as num_jobs_least
				FROM MES_COMMON.dbo.ojt_job_signoffs s 
				JOIN MES_COMMON.dbo.operators o ON s.operator_ID = o.ID
				WHERE o.ID IN (SELECT operator_ID FROM MES_COMMON.dbo.operator_areas WHERE area_ID = ".$r["area_id"].")
				GROUP BY o.name 
				ORDER BY COUNT(*) ASC";

		$res = $db->query($sql);
		echo json_encode($res);
	}
?>