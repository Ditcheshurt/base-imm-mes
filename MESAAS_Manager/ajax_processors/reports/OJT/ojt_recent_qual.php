<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	/*fill the area select drop down*/
	if($action == "area_select") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT d.ID, d.department_name  
				FROM MES_COMMON.dbo.departments d
				ORDER BY d.department_name";

		$res = $db->query($sql);
		foreach ($res as $i => &$department) {
				$sql = "SELECT a.ID, a.area_desc 
				FROM MES_COMMON.dbo.areas a
				WHERE a.dept_ID = ".$department['ID'];

			$department['areas'] = $db->query($sql);
		}
		echo json_encode($res);
	}

	else if($action == "area_date_job") {
		// stuff for specific area selected
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT TOP 50 * 
				FROM MES_COMMON.dbo.ojt_job_signoffs s
				JOIN MES_COMMON.dbo.ojt_jobs j ON s.ojt_job_ID = j.ID 
				JOIN MES_COMMON.dbo.operators o ON s.operator_ID = o.ID 				
				WHERE s.the_date <= '".$r["up_to_date"]." 23:59:59.99' and j.area_ID = '".$r["area_id"]."'
				ORDER BY the_date DESC";

		//die($sql);

		$res = $db->query($sql);

		echo json_encode($res);
	}
?>
