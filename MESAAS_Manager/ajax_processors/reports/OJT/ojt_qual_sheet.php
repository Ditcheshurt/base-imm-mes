<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];


	if ($action == 'get_ojt_data') {
		global $db;
		$res = new StdClass();

		// get jobs
		if ($_REQUEST['type'] == 'plant') {
			$job_condition = "department_ID IS NULL ";
		} else if ($_REQUEST['type'] == 'department') {
			$job_condition = "area_ID IS NULL AND department_ID = ".$_REQUEST['department_ID']." ";
		} else if ($_REQUEST['type'] == 'area') {
			$job_condition = "area_ID = ".$_REQUEST['area_ID']." AND sub_area_ID IS NULL ";
		} else if ($_REQUEST['type'] == 'sub_area') {
			$job_condition = "sub_area_ID = ".$_REQUEST['sub_area_ID']." ";
		}

		// get jobs
		$sql = "SELECT
					ID,
					department_ID,
					area_ID,
					job_name,
					job_details,
					active,
					sub_area_ID,
					ISNULL(min_operators_qualified, 0) AS min_qualified
				FROM
					ojt_jobs
				WHERE
					".$job_condition."
					AND active = 1
				ORDER BY
					job_name;";
		$res->jobs = $db->query($sql);
		if (!$res->jobs) {
			$res->jobs = array();
		}

		if ($_REQUEST['type'] == 'plant') {
			$res->levels = array(array('ojt_level' => 1, 'level_desc' => 'Qualified'));
		} else {
			// get levels
			$sql = "SELECT
						ojt_level,
						level_desc
					FROM
						ojt_level_descriptions
					WHERE
						dept_ID = ".$_REQUEST['department_ID']."
					ORDER BY
						ojt_level;";
			//die($sql);
			$res->levels = $db->query($sql);
			if (!$res->levels) {
				$res->levels = array();
			}
		}


		// get qualifications
		$sql = "EXEC dbo.sp_ojt_getQualifications
					@type = '".$_REQUEST['type']."',
					@department_ID = ".$_REQUEST['department_ID'].",
					@area_ID = ".$_REQUEST['area_ID'].",
					@sub_area_ID = ".$_REQUEST['sub_area_ID'].",
					@active = ".$_REQUEST['active'].";";
		//die($sql);
		$res->operator_qualifications = $db->query($sql);

		foreach ($res->levels as $i => &$level) {
			$level['job_qual_counts'] = array();
			foreach ($res->jobs as $j => $job) {
				$level['job_qual_counts'][$job['ID']] = 0;
				foreach ($res->operator_qualifications as $k => $operator) {
					if ($operator[$job['ID']] >= $level['ojt_level']) {
						$level['job_qual_counts'][$job['ID']]++;
					}
				}
			}
		}

		echo json_encode($res);
	}

?>