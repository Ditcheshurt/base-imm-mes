<?php

require_once("../../init.php");

$db->catalog = "MES_COMMON";
$db->connect();

header("Content-Type: text/html");

$action = isset($_REQUEST['action']);
$actions = isset($_REQUEST['actions']);
$ojt_operator_ID = isset($_REQUEST["ojt_operator_ID"]) ? $_REQUEST["ojt_operator_ID"] : null;
$ojt_group_ID = "0";

if (isset($_REQUEST["action"])) {
	call_user_func($_REQUEST["action"], $db);
	die();
}

if ($ojt_operator_ID == null) {
	start_table();
	echo('<thead><tr><th>Result</th></tr></thead>');
	die('<tbody><tr><td>No operator selected</td></tr></tbody>');
	end_table();
}

// $template_int will have an array of integers that represent all valid signoff templates associated with the given operator
// $templates will contain an array of arrays that has the ID and template_description of the associated signoff templates
$operator_groups_int = get_operator_groups($db);
$template_int = get_templates($db, $operator_groups_int);
$templates = array();

foreach ($template_int as $template) {
	$sql = "SELECT ojt_signoff_templates.ID AS ID, template_description FROM ojt_signoff_templates WHERE ojt_signoff_templates.ID = $template";

	$res = $db->query($sql);

	foreach ($res as $template_) {
		array_push($templates, $template_);
	}
}

// $operator_groups_int will have an array of integers that represent all valid groups associated with the given operator
// $operator_groups will contain an array of arrays that has the ID and group_description of the associated groups
$operator_groups = array();
foreach ($operator_groups_int as $operator_group) {
	$sql = "SELECT ID, group_description FROM ojt_groups WHERE ID = $operator_group";

	$res = $db->query($sql);

	foreach ($res as $operator_group_) {
		array_push($operator_groups, $operator_group_);
	}
}


// build the table for each signoff templates
foreach ($templates as $template) {

	if ($templates == null) {
		start_table();
		echo('<thead><tr><th>Result</th></tr></thead>');
		echo('<tbody><tr><td>No templates for this group</td></tr></tbody>');
		end_table();
	} else {
		echo "<hr /><h4>" . $template["template_description"] . "</h4>";
		start_table();
		create_header($db, $template);
		echo "<tbody>";
		foreach ($operator_groups as $operator_group) {
			populate_table($db, $template["ID"], $operator_group["ID"], $operator_group["group_description"]);
		}
		echo "</tbody>";
		end_table();
	}

}


function start_table()
{
	echo('<table class="table table-striped table-condensed table-responsive table-bordered tbl-signoffs table-hover" style="width:100%">');
}

/**
 * get the signoff templates for a operator
 *
 * @param $db
 * @return mixed
 */
function get_operator_groups($db)
{

	if (isset($_REQUEST["ojt_operator_ID"])) {
		$ojt_operator_ID = $_REQUEST["ojt_operator_ID"];
	} else {
		die("No Operator Set");
	}

	//get all groups that the operator is explicitly assigned

	$groups = array();
	$groups_explicit = array();
	$sql = "SELECT ojt_group_ID FROM ojt_group_operators WHERE operator_ID = $ojt_operator_ID";

	$res = array();
	$res = $db->query($sql);

	if ($res == null) {
		start_table();
		echo('<thead><tr><th>Result</th></tr></thead>');
		echo('<tbody><tr><td>Operator is not part of any groups</td></tr></tbody>');
		end_table();
		die();
	} //get parents of the groups that the operator is in. If any parents are inactive, throw group out
	else {
		foreach ($res as $group_ID) {
			$ojt_group_ID = $group_ID["ojt_group_ID"];

			$sql = "
				WITH parents
					AS (
						SELECT g.ID, g.parent_group_ID, g.group_description, g.active
						FROM ojt_groups g
						JOIN ojt_group_operators gop ON gop.ojt_group_ID = g.ID
						WHERE gop.operator_ID = $ojt_operator_ID
							AND g.ID = $ojt_group_ID
						UNION ALL
						SELECT og.ID, og.parent_group_ID, og.group_description, og.active
						FROM ojt_groups og
						INNER JOIN parents p ON p.parent_group_ID = og.ID
					)
				SELECT DISTINCT * FROM parents";

			$res2 = $db->query($sql);

			if ($res2 == null) {
				$res2 = array();
			} else {

				$bad_group = false;
				foreach ($res2 as $parent) {
					if ($parent["active"] == 0) {
						$bad_group = true;
					}

				}

				if ($bad_group != true) {
					foreach ($res2 as $active_group) {
						if (!in_array($active_group["ID"], $groups)) {
							array_push($groups, $active_group["ID"]);
						}
					}
				}
				if ($bad_group != true) {
					if (!in_array($ojt_group_ID, $groups_explicit)) {
						array_push($groups_explicit, $ojt_group_ID);
					}
				}

			}
		}
	}


	//$groups now contains all groups that the operator is in explicitly and that are active PLUS those groups parents
	//$groups_explicit has all groups that the operator is in explicitly and that are active


	$groups_kids = $groups_explicit;
	$done = false;
	$previous_length = 0;

	while (!$done == true) {
		$get_kids = implode(",", $groups_kids);

		$sql = "SELECT ojt_groups.ID FROM ojt_groups WHERE active = 1 AND parent_group_ID IN ($get_kids)";

		$res = $db->query($sql);

		if ($res == null) {
			$res = array();
			$done = true;
		} else {
			foreach ($res as $kids) {
				if (!in_array($kids["ID"], $groups_kids)) {
					array_push($groups_kids, $kids["ID"]);
				}
			}
			if ($previous_length != count($groups_kids)) {
				$previous_length = count($groups_kids);
			} else if ($previous_length == count($groups_kids)) {
				$done = true;
			}


			foreach ($groups_kids as $kids) {
				if (!in_array($kids, $groups)) {
					array_push($groups, $kids);
				}
			}

		}

	}
	return $groups;
}

function get_templates($db, $operator_groups)
{
	$ojt_operator_ID = $_REQUEST["ojt_operator_ID"];
	$alert_value = $_REQUEST["alert_val"];
	$ojt_operator_groups = $operator_groups;
	$templates = array();

	foreach ($ojt_operator_groups as $op_groups) {
		$sql = "
			SELECT DISTINCT 
				ojt_signoff_template_ID, template_description
			FROM 
				ojt_group_requirements 

			JOIN ojt_requirements

			JOIN ojt_signoff_templates

			ON ojt_signoff_template_ID = ojt_signoff_templates.ID
			
			ON ojt_requirement_ID = ojt_requirements.ID

			WHERE ojt_requirements.active = 1 AND ojt_group_ID = $op_groups";

		$res = $db->query($sql);

		if ($res == null) {
			$res = array();
		} else {

			foreach ($res as $template_ids) {
				if (!in_array($template_ids["ojt_signoff_template_ID"], $templates)) {

					if (($alert_value == 0) && ($template_ids["template_description"] == "Alerts")) {
					} else {
						array_push($templates, $template_ids["ojt_signoff_template_ID"]);
					}
				}
			}
		}

	}


	if ($templates == null) {
		start_table();
		echo('<thead><tr><th>Result</th></tr></thead>');
		echo('<tbody><tr><td>No Requirements Assigned To This Operator</td></tr></tbody>');
		end_table();
		die();
	}


	return $templates;
}


/**
 * creates table head with given the signoff template
 * @param $db
 * @param $signoff_template
 * @return array
 */
function create_header($db, $template)
{
	$signoff_template_ID = $template["ID"];

	$sql = "SELECT
				st.template_description,
				stl.level_description,
				stl.level_order,
				(
					SELECT COUNT(r.ID)
					FROM ojt_signoff_template_levels l
					JOIN ojt_signoff_template_levels_roles r ON r.ojt_signoff_template_level_ID = l.ID
					WHERE l.ojt_signoff_template_ID = st.ID AND stl.ID = l.ID
				) AS role_count
			FROM
				ojt_signoff_templates st
				JOIN ojt_signoff_template_levels stl ON stl.ojt_signoff_template_ID = st.ID
			WHERE
				st.ID = $signoff_template_ID
			ORDER BY
				st.template_description, stl.level_order";

	$res = $db->query($sql);

	echo '<thead><tr><th colspan="2"></th>';
	foreach ($res as $row) {
		echo "<th colspan = " . $row["role_count"] . ">" . $row["level_description"] . "</th>";
	}

	$sql = "SELECT
				r.name AS role
			FROM ojt_signoff_templates st
			JOIN ojt_signoff_template_levels stl ON stl.ojt_signoff_template_ID = st.ID
			JOIN ojt_signoff_template_levels_roles slr ON slr.ojt_signoff_template_level_ID = stl.ID
			JOIN roles r on slr.role_ID = r.ID
			WHERE
				st.ID = $signoff_template_ID

			ORDER BY level_order, role_order";

	$res = $db->query($sql);

	echo "</tr><tr><th>Requirement</th><th>Group</th>";
	foreach ($res as $row) {
		echo "<th>" . $row["role"] . "</th>";

	}

	echo "</tr></thead>";
}

/**
 * @param $db
 * @param $ojt_signoff_template_ID
 * @param $group_ID
 * @param $group_description
 */
function populate_table($db, $ojt_signoff_template_ID, $group_ID, $group_description)
{
	$operator_ID = $_REQUEST["ojt_operator_ID"];
	$ojt_group_ID = $group_ID;

	if (isset($_REQUEST["table_info"])) {
		$table_info = $_REQUEST["table_info"];
	}

	if ($table_info == "signoff_operator") {
		$pre_text = "Signed off by: ";
		$info = "name";
	}
	if ($table_info == "signoff_date") {
		$pre_text = "Signed off on: ";
		$info = "created_date";
	}
	if ($table_info == "signoff_expires") {
		$pre_text = "Sign off expires on: ";
		$info = "expire_date";
	}


	$sql = "SELECT
				ojt_requirement_ID,
				requirement_description
			FROM
				ojt_group_requirements
			JOIN ojt_requirements ON ojt_requirement_ID = ojt_requirements.ID
				AND ojt_group_ID = $ojt_group_ID
				AND ojt_signoff_template_ID = $ojt_signoff_template_ID";

	$res = $db->query($sql);

	if ($res != null) {

		foreach ($res as $requirements) {

			$requirement = $requirements["ojt_requirement_ID"];

			$sql = "SELECT
						ojt_requirement_ID,
						requirement_description,
						level_order,
						level_description,
						level_ID,
						role_order,
						role_ID,
						levels_roles_ID,
						signoff_operator_ID,
						created_date,
						CASE WHEN expire_date IS NULL THEN 'No Expire' ELSE expire_date END AS expire_date,
						name
					FROM operators RIGHT JOIN

						(SELECT 
							c.ojt_requirement_ID, 
							requirement_description, 
							level_order, 
							level_description, 
							level_ID, 
							role_order, 
							role_ID, 
							levels_roles_ID, 
							signoff_operator_ID, 
							CAST(created_date AS nvarchar(30)) AS created_date,
							CAST(expire_date AS nvarchar(30)) AS expire_date
						FROM ojt_signoffs
						RIGHT JOIN

							(SELECT 
								ojt_requirement_ID, 
								requirement_description, 
								level_order, 
								level_description, 
								level_ID, role_order, 
								role_ID, 
								ojt_signoff_template_levels_roles.ID as levels_roles_ID
							FROM ojt_signoff_template_levels_roles
							JOIN

								(SELECT 
									ojt_requirement_ID, 
									requirement_description, 
									level_order, 
									level_description, 
									ojt_signoff_template_levels.ID as level_ID
								FROM ojt_signoff_template_levels
								JOIN

									(SELECT 
										ojt_requirement_ID, 
										requirement_description
									FROM ojt_group_requirements
									JOIN ojt_requirements
					
									ON ojt_requirement_ID = ojt_requirements.ID AND ojt_requirements.active = 1 AND ojt_group_ID = $ojt_group_ID AND ojt_signoff_template_ID = $ojt_signoff_template_ID) as a

								ON ojt_signoff_template_ID = $ojt_signoff_template_ID) as b

							ON level_ID = ojt_signoff_template_level_ID) as c

						ON levels_roles_ID = ojt_signoff_template_levels_roles_ID AND (expire_date > GETDATE() OR expire_date IS NULL) AND c.ojt_requirement_ID = ojt_signoffs.ojt_requirement_ID AND operator_ID = $operator_ID

					WHERE c.ojt_requirement_ID = $requirement) as d

					ON signoff_operator_ID = operators.ID

					ORDER BY ojt_requirement_ID, level_order, role_order";


			$res_ = $db->query($sql);
			if ($res_ == null) {
			} else {
				echo "<tr><td>" . $requirements["requirement_description"] . "</td>";
				echo "<td>" . $group_description . "</td>";
				foreach ($res_ as $signoff) {
					if ($signoff["signoff_operator_ID"] == null) {
						echo '<td class = "danger"></td>';
					} else {
						echo '<td class = "success" style = "color:black">' . $pre_text . $signoff[$info] . "</td>";
					}

				}

			}
			echo "</tr>";
		}

	}

}

function get_warning_class($min, $actual)
{
	$success = "success";
	$danger = "danger";
	$warn = "warning";

	if ($min == null || $min <= 0) {
		return $success;
	}
	if ($actual >= $min) {
		return $success;
	}
	if ($actual < $min && $actual >= $min - ceil($min * .1)) {
		return $warn;
	}
	return $danger;
}

function end_table()
{
	echo('</table>');
}