<?php

	require_once("../../init.php");

	$action = $_REQUEST['action'];

	/*fill the area select drop down*/
	if($action == "area_select") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

		$sql = "SELECT d.ID, d.department_name  
				FROM MES_COMMON.dbo.departments d
				ORDER BY d.department_name";

		$res = $db->query($sql);
		foreach ($res as $i => &$department) {
				$sql = "SELECT a.ID, a.area_desc 
				FROM MES_COMMON.dbo.areas a
				WHERE a.dept_ID = ".$department['ID'];

			$department['areas'] = $db->query($sql);
		}
		echo json_encode($res);
	}

	/*creates table head*/
	else if ($action == "table_head") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;

			$sql = "SELECT l.ojt_level, l.level_desc
					FROM MES_COMMON.dbo.ojt_level_descriptions l
					ORDER BY l.ojt_level";
		
		$res = $db->query($sql);
		echo json_encode($res);
	}

	/*get data for populating table*/
	else if ($action == "selected_area") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;	

		$sql = "SELECT ID, ojt_level, level_desc
				FROM MES_COMMON.dbo.ojt_level_descriptions
				WHERE dept_ID = ".$r["dept_id"]."";

		$res->levels = $db->query($sql);

			$sql = "DECLARE @COLUMNS as VARCHAR(MAX)
					DECLARE @SQL as VARCHAR(MAX)

					SELECT @COLUMNS = COALESCE(@COLUMNS + ', ', '') + QUOTENAME(ojt_level)
					FROM MES_COMMON.dbo.ojt_level_descriptions
					WHERE dept_ID = ".$r["dept_id"]."

					SET @SQL = 'WITH pivot_data AS (
								SELECT 1 AS idx,j.job_name,d.ojt_level
								FROM MES_COMMON.dbo.ojt_jobs j 
								LEFT JOIN MES_COMMON.dbo.ojt_job_signoffs js
								ON j.ID = js.ojt_job_ID
								LEFT JOIN MES_COMMON.dbo.ojt_level_descriptions d 
								ON js.ojt_level = d.ojt_level AND d.dept_ID = ".$r["dept_id"]."
								WHERE j.area_ID = ".$r["area_id"].")
								SELECT job_name, ' + @COLUMNS + '
								FROM pivot_data
								PIVOT (
									COUNT(idx)
									FOR ojt_level
									IN (' + @COLUMNS + ')
								) AS pivot_results'
					EXEC(@SQL)";

		$res->job_qualifications = $db->query($sql);
		echo json_encode($res);
	}
?>