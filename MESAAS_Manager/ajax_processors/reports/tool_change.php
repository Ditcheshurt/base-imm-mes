<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	if ($action == 'get_tool_change_data') {
		global $db;
		$res = new StdClass();

		$system_ID = $_REQUEST["system_id"];

		if ($_REQUEST['machine_id'] > -1) {
			$machine_str = "AND m.ID = ".$_REQUEST['machine_id'];
		} else {
			$machine_str = "";
		}

		$sql = "
			SELECT
				 mdl2.ID
				,mdl2.tool_change_ID
				,mdl2.change_time
				,DATEADD(second,mdl2.time_in_state*60, mdl2.change_time) AS record_end_time
				,ROUND(mdl2.time_in_state*60,1) AS duration
				,mdl2.machine_ID, m.machine_number
				,mdl2.tool_ID,t.short_description
				,(CASE WHEN mdl2.reason_ID IS NULL THEN 'None' ELSE mdr.reason_description END) AS reason
				,(CASE
					WHEN mdl2.tool_change_ID = mdl2.ID THEN 'START'
					WHEN (mdl2.tool_change_ID <> mdl2.ID AND mdl2.is_tool_change_pause = 1 AND mdl2.time_in_state IS NOT NULL AND mdl2.startup_time IS NULL) THEN 'PAUSE'
					WHEN (mdl2.tool_change_ID <> mdl2.ID AND mdl2.is_tool_change_pause = 1 AND mdl2.time_in_state IS NULL AND mdl2.startup_time IS NULL) THEN 'ACTIVE_PAUSE'
					WHEN (mdl2.tool_change_ID <> mdl2.ID AND mdl2.is_tool_change_pause = 0 AND mdl2.time_in_state IS NOT NULL AND mdl2.startup_time IS NULL) THEN 'RESUME'
					WHEN (mdl2.tool_change_ID <> mdl2.ID AND mdl2.is_tool_change_pause = 0 AND mdl2.time_in_state IS NULL AND mdl2.startup_time IS NULL) THEN 'ACTIVE_TOOL_CHANGE'
					WHEN (mdl2.tool_change_ID <> mdl2.ID AND mdl2.time_in_state IS NOT NULL AND mdl2.startup_time = 0) THEN 'END'
					ELSE 'UNKNOWN'
				END) AS change_state
			FROM machine_downtime_log mdl
			JOIN machines m ON m.ID = mdl.machine_ID
			LEFT JOIN machine_downtime_log mdl2 ON mdl2.tool_change_ID = mdl.ID
			LEFT JOIN machine_downtime_reasons mdr ON mdr.ID = mdl2.reason_ID
			LEFT JOIN tools t ON t.ID = mdl2.tool_ID
			WHERE mdl2.tool_change_ID IS NOT NULL
			AND mdl.change_time >= '{$_REQUEST['start_date']}'
			AND mdl.change_time < '{$_REQUEST['end_date']}'
			{$machine_str}
			AND m.system_ID = {$system_ID}
			ORDER BY mdl2.machine_ID ASC, mdl2.ID ASC;
		";
	//die($sql);
		$res = $db->query($sql);

		echo json_encode($res);
	}
?>