<?php

	require_once("../../../init.php");

	$action = $_REQUEST['action'];
	
	if ($action == "hourly_production_summary") {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;		
		
		$sql = "SELECT 
						0 AS is_active,
						ID as cell_ID, 
						cell_desc 
					FROM cells 
					WHERE ID = ".$r['cell_id'];
		
		$res->description = $db->query($sql);
		
		$sql = "SELECT 
						DATEPART(HOUR, built_time) AS [hour],
						COUNT(built_time) /
							CASE WHEN DATEDIFF(DD, MIN(built_time),MAX(built_time)) = 0 
								THEN 1 
								ELSE DATEDIFF(DD, MIN(built_time),MAX(built_time))+1 
							END 
					AS qty_built										
				FROM built_parts bp
				JOIN part_setup ps ON bp.part_ID = ps.ID 
				WHERE dbo.getProdDate(built_time) = dbo.getProdDate(getDate())
					AND cell_ID = ".$r['cell_id']."
				GROUP BY DATEPART(HOUR, built_time)
				ORDER BY [hour] ASC";
		
		$res->built = $db->query($sql);
		
		$sql = "SELECT 
					hour,
					required_qty										
				FROM 
					 displays_hourly_part_requirements				
				WHERE
					 cell_ID = ".$r['cell_id']."
				ORDER BY [hour] ASC";
		
		$res->req = $db->query($sql);

		//echo $sql; die;
		echo json_encode($res);
	}
	
?>