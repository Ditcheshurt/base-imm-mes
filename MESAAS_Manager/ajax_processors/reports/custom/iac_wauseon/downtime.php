<?php

	require_once("../../../init.php");
	
	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_downtimes ($request, $db) {
		$group_by = $request['group_by'];

		//check for line_type
		$sql = "SELECT line_type FROM MES_COMMON.dbo.v_MES_LINES 
				WHERE line_ID = {$request['line_id']}";
		$res = $db->query($sql);

		if ($res[0]['line_type'] == 0) {
			$sql = "SELECT s.line_id
					, CONCAT(s.id, ' : ', s.station_desc) AS station
					, s.station_order
					, SUM(msh.cycle_time - ISNULL(s.target_takt_time, msh.cycle_time)) AS total_downtime
					, AVG(msh.cycle_time - ISNULL(s.target_takt_time, msh.cycle_time)) AS average_downtime
					, s.id
					, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
					, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					, o.ID AS operator_ID
					, COUNT(msh.ID) AS instance_count
				FROM module_station_history msh 
				JOIN line_stations s ON s.station_id = msh.station_id 
					and s.line_id = msh.line_ID
					AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
				JOIN operators o ON o.id = msh.operator_id
				WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
					AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
					AND s.line_id LIKE '{$request['line_id']}'
					AND o.id LIKE '{$request['operator_id']}'
					AND s.id LIKE '{$request['station_id']}'
					AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
					AND msh.cycle_time > ISNULL(s.target_takt_time, 10000000000)
				##GROUPBY##
				ORDER BY
					s.line_id,
					s.id";

			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY o.ID, o.badge_id, o.name, s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else if ($group_by === "Date") {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10), entry_time, 101), s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}

			// if ($_REQUEST['over_target_cycle_time'] == 1) {
			// 	if ($_REQUEST['custom_target_cycle_time']) {
			// 		$target = $_REQUEST['custom_target_cycle_time'];
			// 	} else {
			// 		$target = "s.target_takt_time";
			// 	}
			// 	$target_sql = "HAVING AVG(msh.cycle_time) > ".$target." ";
			// } else {
			// 	$target_sql = "";
			// }
			// $sql = str_replace("##HAVING##", $target_sql, $sql);
			//die($sql);
		} else {
			$sql = "SELECT s.line_id
					, CONCAT(s.id, ' : ', s.station_desc) AS station
					, s.station_order
					, COUNT(msh.entry_time) AS instance_count
					, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time) - ISNULL(s.target_takt_time, DATEDIFF(ss, msh.entry_time, msh.exit_time))) AS average_downtime
					, SUM(DATEDIFF(ss, msh.entry_time, msh.exit_time) - ISNULL(s.target_takt_time, DATEDIFF(ss, msh.entry_time, msh.exit_time))) AS total_downtime
					, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
					, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					, o.ID AS operator_ID
				FROM wip_part_station_history msh 
				JOIN line_stations s ON s.station_id = msh.station_id 
					and s.line_id = msh.line_ID
				JOIN operators o ON o.id = msh.operator_id
				--LEFT JOIN wip_parts wp ON wp.ID = msh.wip_part_ID
				WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
					AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
					AND s.line_id LIKE '{$request['line_id']}'
					AND o.id LIKE '{$request['operator_id']}'
					AND s.id LIKE '{$request['station_id']}'
					--AND DATEDIFF(ss, msh.entry_time, msh.exit_time) < 500 
					--AND DATEDIFF(ss, msh.entry_time, msh.exit_time) > 5 
					AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
					AND DATEDIFF(ss, msh.entry_time, msh.exit_time) > ISNULL(s.target_takt_time, 10000000000)
				##GROUPBY##
				ORDER BY s.line_id, s.id";
			
			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY o.ID, badge_id, name, s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else if ($group_by === "Date") {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time";
				} else {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}
		}
		//die($sql);
		echo json_encode($db->query($sql));
	}

	function get_top_downtimes ($request, $db) {
		$group_by = $request['group_by'];

		//check for line_type
		$sql = "SELECT line_type FROM MES_COMMON.dbo.v_MES_LINES 
				WHERE line_ID = {$request['line_id']}";
		$res = $db->query($sql);

		if ($res[0]['line_type'] == 0) {

			$sql = "SELECT TOP 5
					s.line_id
					, CONCAT(s.id, ' : ', s.station_desc) AS station
					, s.station_order
					, SUM(msh.cycle_time - ISNULL(s.target_takt_time, msh.cycle_time)) AS total_downtime
					, AVG(msh.cycle_time - ISNULL(s.target_takt_time, msh.cycle_time))AS average_downtime
					, s.id
					, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
					, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					, o.ID AS operator_ID
					, COUNT(msh.ID) AS instance_count
				FROM module_station_history msh 
				JOIN line_stations s ON s.station_id = msh.station_id 
					and s.line_id = msh.line_ID
					AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
				JOIN operators o ON o.id = msh.operator_id
				WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
					AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
					AND s.line_id LIKE '{$request['line_id']}'
					AND o.id LIKE '{$request['operator_id']}'
					AND s.id LIKE '{$request['station_id']}'
					AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
					AND msh.cycle_time > ISNULL(s.target_takt_time, 10000000000)
				##GROUPBY##
				ORDER BY
					SUM(msh.cycle_time - s.target_takt_time) DESC;";

			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY o.ID, o.badge_id, o.name, s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else if ($group_by === "Date") {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10), entry_time, 101), s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}
		} else {
			$sql = "SELECT TOP 5
					s.line_id
					, CONCAT(s.id, ' : ', s.station_desc) AS station
					, s.station_order
					, SUM(DATEDIFF(ss, msh.entry_time, msh.exit_time) - ISNULL(s.target_takt_time, DATEDIFF(ss, msh.entry_time, msh.exit_time))) AS total_downtime
					, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time) - ISNULL(s.target_takt_time, DATEDIFF(ss, msh.entry_time, msh.exit_time))) AS average_downtime
					, s.id
					, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
					, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					, o.ID AS operator_ID
					, COUNT(msh.ID) AS instance_count
				-- FROM module_station_history msh 
				-- JOIN line_stations s ON s.station_id = msh.station_id 
				-- 	and s.line_id = msh.line_ID
				-- 	AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
				-- JOIN operators o ON o.id = msh.operator_id

				FROM wip_part_station_history msh 
				JOIN line_stations s ON s.station_id = msh.station_id 
					and s.line_id = msh.line_ID
				JOIN operators o ON o.id = msh.operator_id
				--LEFT JOIN wip_parts wp ON wp.ID = msh.wip_part_ID
				WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
					AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
					AND s.line_id LIKE '{$request['line_id']}'
					AND o.id LIKE '{$request['operator_id']}'
					AND s.id LIKE '{$request['station_id']}'
					AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
					AND DATEDIFF(ss, msh.entry_time, msh.exit_time) > ISNULL(s.target_takt_time, 10000000000)
				##GROUPBY##
				ORDER BY
					SUM(DATEDIFF(ss, msh.entry_time, msh.exit_time) - s.target_takt_time) DESC;";

			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY o.ID, o.badge_id, o.name, s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else if ($group_by === "Date") {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10), entry_time, 101), s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				} else {
					$sql = str_replace(", o.ID AS operator_ID", "", $sql);
					$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY s.ID, s.line_id, s.station_order, s.id, s.station_desc, s.target_takt_time";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}
		}
		//die($sql);
				
		echo json_encode($db->query($sql));
	}


	function downtimes_detail ($request, $db) {
		//check for line_type
		$sql = "SELECT line_type FROM MES_COMMON.dbo.v_MES_LINES 
				WHERE line_ID = {$request['line_id']}";
		$res = $db->query($sql);

		if ($res[0]['line_type'] == 0) {		
			$sql = "SELECT s.line_id					
						, CONCAT(s.ID, ' - ', s.station_desc) AS station
						, s.station_order
						, o.name
						, entry_time
						, exit_time
						, msh.cycle_time - ISNULL(s.target_takt_time, msh.cycle_time) AS downtime
						, msh.station_id
					FROM module_station_history msh 
					JOIN line_stations s ON s.station_id = msh.station_id 
						and s.line_id = msh.line_ID
						AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
					JOIN operators o ON o.id = msh.operator_id
					WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
						AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
						AND s.line_id LIKE '{$request['line_id']}'
						--AND msh.station_id LIKE '{$request['station_id']}'
						AND s.id LIKE '{$request['station_id']}'
						AND o.ID LIKE '{$request['operator_id']}'
						--AND msh.[cycle_time] < 500 
						--AND msh.[cycle_time] > 5 
						AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
						AND msh.cycle_time > ISNULL(s.target_takt_time, 10000000000)
					ORDER BY entry_time DESC";
		} else {

			$sql = "SELECT s.line_id
						, CONCAT(s.ID, ' - ', s.station_desc) AS station
						, s.station_order
						, entry_time
						, exit_time
						, DATEDIFF(ss, entry_time, exit_time) - ISNULL(s.target_takt_time, DATEDIFF(ss, entry_time, exit_time)) AS downtime
						, o.name
						, msh.station_id
					FROM wip_part_station_history msh 
					JOIN line_stations s ON s.station_id = msh.station_id 
						and s.line_id = msh.line_ID
						--AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
					JOIN operators o ON o.id = msh.operator_id
					WHERE entry_time > CASE WHEN '{$request['shift']}' = '3' THEN DATEADD(d, -1, '{$request['start_date']} 23:00:00') ELSE '{$request['start_date']} 00:00:00' END
						AND entry_time < CASE WHEN '{$request['shift']}' = '3' THEN '{$request['end_date']} 23:00:00' ELSE '{$request['end_date']} 23:59:59.99' END
						AND s.line_id LIKE '{$request['line_id']}'
						AND s.ID LIKE '{$request['station_id']}'
						--AND DATEDIFF(ss, entry_time, exit_time) < 500 
						--AND DATEDIFF(ss, entry_time, exit_time) > 5 
						AND dbo.getShift('{$request['line_id']}', entry_time) LIKE '{$request['shift']}'
						AND o.ID LIKE '{$request['operator_id']}'
						AND DATEDIFF(ss, entry_time, exit_time) > ISNULL(s.target_takt_time, 10000000000)
					GROUP BY o.name, s.ID, s.line_id, s.station_order, msh.station_id, s.station_desc, s.target_takt_time
					ORDER BY s.line_id, msh.station_ID";
		}
		echo json_encode($db->query($sql));
	}

?>