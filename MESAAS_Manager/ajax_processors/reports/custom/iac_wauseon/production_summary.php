<?php

	require_once("../../../init.php");

	$action = $r['action'];

	$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
	
	// if ($action == 'lines') {
	// 	global $db;
	// 	$res = new StdClass();
	// 	$r = $_REQUEST;
		
	// 	$sql = "SELECT id, line_code FROM lines WHERE line_type = 0";
		
	// 	$res = $db->query($sql);
	// 	echo json_encode($res);
	// }

	if ($action == "broadcast_summary") {
		global $db;
		$res = new StdClass();
		
		// broadcast data
		$sql = "SELECT 
					(SELECT COUNT(*) FROM modules WHERE line_ID = {$r['line_id']} AND recvd_time >= convert(varchar(10), GETDATE(), 101)) as broadcasts_today,
					(SELECT MAX(recvd_time) FROM modules WHERE line_ID = {$r['line_id']}) as last_broadcast,
					SUM(CASE WHEN status = 0 AND line_ID = {$r['line_id']} THEN 1 ELSE 0 END) as broadcasts_inqueue, 
					SUM(CASE WHEN status = 1 AND line_ID = {$r['line_id']} THEN 1 ELSE 0 END) as broadcasts_inbuild,
					(SELECT MAX(built_time) FROM modules WHERE line_ID = {$r['line_id']} AND built_time IS NOT NULL) as broadcast_lastbuild,
					(SELECT COUNT(*) FROM modules WHERE line_ID = {$r['line_id']} AND built_time >= convert(varchar(10), GETDATE(), 101)) as broadcasts_builttoday
				FROM modules WHERE status < 2";
		
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == "daily_production_summary") {
		global $db;
		$res = new StdClass();

		// production qty
		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT 
						CONVERT(VARCHAR(10), dbo.getProdDate(built_time), 101) AS built_date
						, SUM(CASE WHEN dbo.getShift(built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]	
						, SUM(CASE WHEN dbo.getShift(built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]	
						, SUM(CASE WHEN dbo.getShift(built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE ps.cell_ID = {$r['line_id']}
						AND dbo.getProdDate(built_time) >= '{$r['start_date']} 00:00:00'
						AND dbo.getProdDate(built_time) <= '{$r['end_date']} 23:59:59'
					GROUP BY
						dbo.getProdDate(built_time)
					ORDER BY
						built_date ASC;";
		} else {
			$sql = "SELECT * FROM (
						(
							SELECT 
								CONVERT(VARCHAR(10), dbo.getProdDate('{$r['line_id']}',m.built_time), 101) AS built_date
								, SUM(CASE WHEN dbo.getShift(m.line_ID, m.built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]
								, SUM(CASE WHEN dbo.getShift(m.line_ID, m.built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]
								, SUM(CASE WHEN dbo.getShift(m.line_ID, m.built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
							FROM
								modules m
								JOIN lines l ON m.line_id = l.id
								LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
							WHERE
								m.line_ID LIKE '{$r['line_id']}'
								AND dbo.getProdDate('{$r['line_id']}',m.built_time) >= '{$r['start_date']} 00:00:00'
								AND dbo.getProdDate('{$r['line_id']}',m.built_time) <= '{$r['end_date']} 23:59:59'
								AND mm.model LIKE '{$line_model}'
							GROUP BY
								dbo.getProdDate('{$r['line_id']}',m.built_time)
						) UNION (
							SELECT 
								CONVERT(VARCHAR(10), dbo.getProdDate('{$r['line_id']}',wp.built_time), 101) AS built_date
								, SUM(CASE WHEN dbo.getShift(wp.line_ID, wp.built_time) = 1 THEN 1 ELSE 0 END) AS [first_shift]
								, SUM(CASE WHEN dbo.getShift(wp.line_ID, wp.built_time) = 2 THEN 1 ELSE 0 END) AS [second_shift]
								, SUM(CASE WHEN dbo.getShift(wp.line_ID, wp.built_time) = 3 THEN 1 ELSE 0 END) AS [third_shift]
							FROM
								wip_parts wp
								JOIN lines l ON wp.line_id = l.id
								LEFT JOIN v_module_models mm ON mm.module_ID = wp.used_by_module_ID
							WHERE
								wp.line_ID LIKE '{$r['line_id']}'
								AND dbo.getProdDate('{$r['line_id']}',wp.built_time) >= '{$r['start_date']} 00:00:00'
								AND dbo.getProdDate('{$r['line_id']}',wp.built_time) <= '{$r['end_date']} 23:59:59'
								AND mm.model LIKE '{$line_model}'
							GROUP BY
								dbo.getProdDate('{$r['line_id']}',wp.built_time)
						)
					) AS prod;";
		}
        //die($sql);
		
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "hourly_production_summary") {
		global $db;
		$res = new StdClass();	
		// production qty
		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						DATEPART(HOUR, built_time) AS [hour]
						, COUNT(built_time) / 
							CASE WHEN DATEDIFF(DD, MIN(built_time),MAX(built_time)) = 0 
								THEN 1 
								ELSE DATEDIFF(DD, MIN(built_time),MAX(built_time)) + 1 
							END AS qty_built
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE c.ID = {$r['line_id']}
						AND dbo.getProdDate(built_time) >= '{$r['start_date']} 00:00:00'
						AND dbo.getProdDate(built_time) <= '{$r['end_date']} 23:59:59'
					GROUP BY DATEPART(HOUR, built_time)
					ORDER BY [hour] ASC;";
			//die($sql);
		} else {
			$sql = "SELECT * FROM (
						(
							SELECT
								DATEPART(HOUR, m.built_time) AS [hour]
								, COUNT(m.built_time) / 
									CASE WHEN DATEDIFF(DD, MIN(m.built_time),MAX(m.built_time)) = 0 
										THEN 1 
										ELSE DATEDIFF(DD, MIN(m.built_time),MAX(m.built_time))+1 
									END AS qty_built
							FROM
								modules m
								JOIN lines l ON m.line_id = l.id
								LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
							WHERE
								m.line_ID LIKE '{$r['line_id']}'
								AND dbo.getProdDate('{$r['line_id']}',m.built_time) >= '{$r['start_date']} 00:00:00'
								AND dbo.getProdDate('{$r['line_id']}',m.built_time) <= '{$r['end_date']} 23:59:59'
								AND mm.model LIKE '{$line_model}'
							GROUP BY DATEPART(HOUR, m.built_time)		
						) UNION (
							SELECT
								DATEPART(HOUR, wp.built_time) AS [hour]
								, COUNT(wp.built_time) / 
									CASE WHEN DATEDIFF(DD, MIN(wp.built_time),MAX(wp.built_time)) = 0 
										THEN 1 
										ELSE DATEDIFF(DD, MIN(wp.built_time),MAX(wp.built_time))+1 
									END AS qty_built
							FROM
								wip_parts wp
								JOIN lines l ON wp.line_id = l.id
								LEFT JOIN v_module_models mm ON mm.module_ID = wp.used_by_module_ID
							WHERE
								wp.line_ID LIKE '{$r['line_id']}'
								AND dbo.getProdDate('{$r['line_id']}',wp.built_time) >= '{$r['start_date']} 00:00:00'
								AND dbo.getProdDate('{$r['line_id']}',wp.built_time) <= '{$r['end_date']} 23:59:59'
								AND mm.model LIKE '{$line_model}'
							GROUP BY DATEPART(HOUR, wp.built_time)
						)
					) AS prod
					ORDER BY [hour] ASC;";
			//die($sql);
		}
        //die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == "production_summary") {
		global $db;
		$res = new StdClass();	
		// production
		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						*
						, CONVERT(varchar, built_time, 101) + ' ' + CONVERT(varchar, built_time, 108) AS built_time_formatted
						, NULL AS VIN
						, NULL AS pallet_number
						, NULL AS pallet_position
					FROM built_parts bp
						LEFT JOIN part_setup ps ON ps.ID = bp.part_ID
						LEFT JOIN cells c ON c.ID = ps.cell_ID
					WHERE c.ID = {$r['line_id']}
						AND dbo.getProdDate(built_time) >= '{$r['start_date']} 00:00:00'
						AND dbo.getProdDate(built_time) <= '{$r['end_date']} 23:59:59'
					ORDER BY built_time DESC;";
			//die($sql);
		} else {
			$sql = "SELECT
						m.*
						, CONVERT(varchar, m.built_time, 101) + ' ' + CONVERT(varchar, m.built_time, 108) AS built_time_formatted
					FROM
						modules m
						JOIN lines l ON m.line_id = l.id
						LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
					WHERE
						m.line_ID LIKE '{$r['line_id']}'
						AND dbo.getProdDate('{$r['line_id']}',m.built_time) >= '{$r['start_date']} 00:00:00'
						AND dbo.getProdDate('{$r['line_id']}',m.built_time) <= '{$r['end_date']} 23:59:59'
						AND mm.model LIKE '{$line_model}'
					ORDER BY m.built_time DESC;";
		}
		
      //die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
?>