<?php

	require_once("../../../init.php");

	$action = $r['action'];
	
	if ($action == 'cycle_times') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$group_by = $r['group_by'];
		$station_id = $r['station_id'] == null || $r['station_id'] == -1 ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		
		//check for line_type
		$sql = "SELECT line_type FROM MES_COMMON.dbo.v_MES_LINES 
				WHERE line_ID = ".$r['line_id'];
		$res = $db->query($sql);


		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id
						, CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station
						, bpsh.station_order
						, COUNT(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS num_cycles
						, AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS avg_cycle_time
						, AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) + bpsh.transit_time AS avg_full_cycle_time
						--, NULL AS avg_adjusted_cycle_time
						, bpsh.cycle_time AS target_cycle_time
						, AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) - bpsh.cycle_time AS over_cycle_time
						, AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) + bpsh.transit_time - bpsh.cycle_time AS full_over_cycle_time
						--, NULL AS adjusted_over_cycle_time
						, bpsh.ID
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							s.cycle_time,
							s.transit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							s.cycle_time,
							s.transit_time,
							o.ID,
							o.badge_ID,
							o.name
					) AS bpsh
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.operator_ID LIKE '".$operator_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5
					##GROUPBY##
					##HAVING##
					ORDER BY
						bpsh.cell_ID,
						bpsh.ID;";
			
			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					//$sql = str_replace(", CASE WHEN LEFT(m.VIN, 2) = '2L' THEN '540' WHEN LEFT(m.VIN, 2) = '2F' THEN '539' ELSE NULL END AS program", "", $sql);
					$newGroupBy = "GROUP BY bpsh.badge_ID,bpsh.name,bpsh.cell_ID,bpsh.station_order,bpsh.id,bpsh.station_desc, bpsh.cycle_time, bpsh.transit_time";
				} else if ($group_by === "Date") {
					$sql = str_replace(", CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name", "", $sql);
					//$sql = str_replace(", CASE WHEN LEFT(m.VIN, 2) = '2L' THEN '540' WHEN LEFT(m.VIN, 2) = '2F' THEN '539' ELSE NULL END AS program", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),bpsh.cell_ID,bpsh.station_order,bpsh.ID,bpsh.station_desc, bpsh.cycle_time, bpsh.transit_time";			
				// } else if ($group_by === 'Program') {
				// 	$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
				// 	$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
				// 	$newGroupBy = "GROUP BY LEFT(m.VIN, 2),s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
				} else {
					$sql = str_replace(", CONCAT(bpsh.badge_id, ' : ', bpsh.name) AS operator_name", "", $sql);
					$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
					//$sql = str_replace(", CASE WHEN LEFT(m.VIN, 2) = '2L' THEN '540' WHEN LEFT(m.VIN, 2) = '2F' THEN '539' ELSE NULL END AS program", "", $sql);
					$newGroupBy = "GROUP BY bpsh.cell_ID,bpsh.station_order,bpsh.ID,bpsh.station_desc, bpsh.cycle_time, bpsh.transit_time";
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}

			if ($_REQUEST['over_target_cycle_time'] == 1) {
				if ($_REQUEST['custom_target_cycle_time']) {
					$target = $_REQUEST['custom_target_cycle_time'];
				} else {
					$target = "bpsh.cycle_time";
				}
				$target_sql = "HAVING AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) > ".$target." ";
			} else {
				$target_sql = "";
			}
			$sql = str_replace("##HAVING##", $target_sql, $sql);

		} else if ($r['process_type'] == 'SEQUENCED') {
			if ($res[0]['line_type'] == 0) {
				$sql = "SELECT s.line_id
						, CONCAT(s.id, ' : ', s.station_desc) AS station
						, s.station_order
						, COUNT(msh.cycle_time) AS num_cycles
						, AVG(msh.cycle_time) AS avg_cycle_time
						--, AVG(msh.adjusted_cycle_time) AS avg_adjusted_cycle_time
						, AVG(msh.cycle_time) + s.transit_time AS avg_full_cycle_time
						, s.target_takt_time AS target_cycle_time
						, AVG(msh.cycle_time) - s.target_takt_time AS over_cycle_time
						--, AVG(msh.adjusted_cycle_time) - s.target_takt_time AS adjusted_over_cycle_time
						, AVG(msh.cycle_time) + s.transit_time - s.target_takt_time AS full_over_cycle_time
						, s.id
						, mm.model
						--, CASE WHEN LEFT(m.VIN, 2) = '2L' THEN '540' WHEN LEFT(m.VIN, 2) = '2F' THEN '539' ELSE NULL END AS program
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
					FROM module_station_history msh 
					JOIN line_stations s ON s.station_id = msh.station_id 
						and s.line_id = msh.line_ID
						AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
					JOIN operators o ON o.id = msh.operator_id
					--WHERE entry_time > '".$start_date." 00:00:00' 
					--	AND entry_time < '".$end_date." 23:59:59.99'
					LEFT JOIN modules m ON m.ID = msh.module_ID
					LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND s.line_id LIKE '".$line_id."'
						AND mm.model LIKE '{$line_model}'
						AND o.id LIKE '".$operator_id."'
						AND s.id LIKE '".$station_id."'
						AND msh.[cycle_time] < 500 
						AND msh.[cycle_time] > 5 
						AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
					##GROUPBY##
					##HAVING##
					ORDER BY s.line_id, s.id";
			
				if ($group_by != null && $group_by != "") {
					if ($group_by === "Operator") {
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$sql = str_replace(", mm.model", "", $sql);
						$newGroupBy = "GROUP BY badge_id,name,s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else if ($group_by === "Date") {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", mm.model", "", $sql);
						$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else if ($group_by === 'Model') {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY mm.model,s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else {
						$sql = str_replace(", mm.model", "", $sql);
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					}
					$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
				}

				if ($_REQUEST['over_target_cycle_time'] == 1) {
					if ($_REQUEST['custom_target_cycle_time']) {
						$target = $_REQUEST['custom_target_cycle_time'];
					} else {
						$target = "s.target_takt_time";
					}
					$target_sql = "HAVING AVG(msh.cycle_time) > ".$target." ";
				} else {
					$target_sql = "";
				}
				$sql = str_replace("##HAVING##", $target_sql, $sql);
				//die($sql);
			} else {
				$sql = "SELECT s.line_id
						, CONCAT(s.id, ' : ', s.station_desc) AS station
						, s.station_order
						, COUNT(msh.entry_time) AS num_cycles
						, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)) AS avg_cycle_time
						, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)) + s.transit_time AS avg_full_cycle_time
						--, NULL AS avg_adjusted_cycle_time
						, s.id
						, mm.model
						--, CASE WHEN LEFT(m.VIN, 2) = '2L' THEN '540' WHEN LEFT(m.VIN, 2) = '2F' THEN '539' ELSE NULL END AS program
						, s.target_takt_time AS target_cycle_time
						, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)) - s.target_takt_time AS over_cycle_time
						, AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)) + s.transit_time - s.target_takt_time AS full_over_cycle_time
						--, NULL AS adjusted_over_cycle_time
						, CONVERT(VARCHAR(10), entry_time, 101) AS entry_date
						, CONCAT(o.badge_id, ' : ', o.name) AS operator_name
						, mm.model
					FROM wip_part_station_history msh 
					JOIN line_stations s ON s.station_id = msh.station_id 
						and s.line_id = msh.line_ID
						--AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
					JOIN operators o ON o.id = msh.operator_id
					LEFT JOIN wip_parts wp ON wp.ID = msh.wip_part_ID
					LEFT JOIN modules m ON m.ID = wp.used_by_module_ID
					LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND s.line_id LIKE ".$line_id."
						AND mm.model LIKE '{$line_model}'
						AND o.id LIKE '".$operator_id."'
						AND s.id LIKE '".$station_id."'
						AND DATEDIFF(ss, msh.entry_time, msh.exit_time) < 500 
						AND DATEDIFF(ss, msh.entry_time, msh.exit_time) > 5 
						AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
					##GROUPBY##
					##HAVING##
					ORDER BY s.line_id, s.id";
				
				if ($group_by != null && $group_by != "") {
					if ($group_by === "Operator") {
						$sql = str_replace(", mm.model", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY badge_id,name,s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else if ($group_by === "Date") {
						$sql = str_replace(", mm.model", "", $sql);
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$newGroupBy = "GROUP BY convert(varchar(10),entry_time,101),s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else if ($group_by === 'Program') {
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY mm.model,s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					} else {
						$sql = str_replace(", mm.model", "", $sql);
						$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
						$sql = str_replace(", CONVERT(VARCHAR(10), entry_time, 101) AS entry_date", "", $sql);
						$newGroupBy = "GROUP BY s.ID,s.line_id,s.station_order,s.id,s.station_desc, s.target_takt_time, s.transit_time";
					}
					$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
				}

				if ($_REQUEST['over_target_cycle_time'] == 1) {
					if ($_REQUEST['custom_target_cycle_time']) {
						$target = $_REQUEST['custom_target_cycle_time'];
					} else {
						$target = "s.target_takt_time";
					}
					$target_sql = "HAVING AVG(DATEDIFF(ss, msh.entry_time, msh.exit_time)) > ".$target." ";
				} else {
					$target_sql = "";
				}
				$sql = str_replace("##HAVING##", $target_sql, $sql);
			}
			//die($sql);
		} else if ($r['process_type'] == 'REPETITIVE') {
			
			$sql = "SELECT 
						m.machine_name
						, t.tool_description
						, COUNT(c.cycle_time) AS num_cycles
						, AVG(c.cycle_duration) AS avg_cycle_time
						, NULL AS avg_adjusted_cycle_time
						, convert(varchar(10),c.cycle_time,101) AS entry_date
					  FROM 
						machine_cycles c
					  JOIN machines m 
						ON m.ID = c.machine_ID
					  JOIN tools t 
						ON t.ID = c.tool_ID
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND c.cycle_duration < 500 
						AND c.cycle_duration > 5 
						AND dbo.getShift(c.cycle_time) LIKE '".$shift."'
						AND c.machine_ID = ".$r['machine_ID']."
					##GROUPBY##
					ORDER BY m.machine_name, convert(varchar(10),c.cycle_time,101) DESC";
			
			if ($group_by != null && $group_by != "") {
				if ($group_by === "Operator") {
					$sql = str_replace(", convert(varchar(10),c.cycle_time,101) AS entry_date", "", $sql);
					$newGroupBy = "GROUP BY badge_id,name,m.machine_name, t.tool_description";
				} else if ($group_by === "Date") {
					//$sql = str_replace(", CONCAT(o.badge_id, ' : ', o.name) AS operator_name", "", $sql);
					$newGroupBy = "GROUP BY convert(varchar(10),c.cycle_time,101),m.machine_name, t.tool_description";			
				}
				$sql = str_replace("##GROUPBY##", $newGroupBy, $sql);
			}
		}

		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
	if ($action == 'station_operator_cycle_times') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$station_id = $r['station_id'] == null ? "%" : $r['station_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$program = $r['program'] == null ? "%" : $r['program'];
		
		//check for line_type		
		$sql = "SELECT line_type FROM lines 
				WHERE id = ".$r['line_id'];
		$res = $db->query($sql);

		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id,
						CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station,
						bpsh.station_order,
						COUNT(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS num_cycles,
						AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) AS avg_cycle_time,
						AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) + bpsh.transit_time AS avg_full_cycle_time,
						--NULL AS avg_adjusted_cycle_time,
						bpsh.cycle_time AS target_cycle_time,
						AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) - bpsh.cycle_time AS over_cycle_time,
						AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) + bpsh.transit_time - bpsh.cycle_time AS full_over_cycle_time,
						--NULL AS adjusted_over_cycle_time,
						bpsh.name,
						bpsh.ID
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							s.cycle_time,
							s.transit_time,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							s.cycle_time,
							s.transit_time,
							o.ID,
							o.badge_ID,
							o.name
					) AS bpsh
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND bpsh.operator_ID LIKE '".$operator_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5 
					GROUP BY
						bpsh.cell_ID,
						bpsh.station_order,
						bpsh.ID,
						bpsh.station_desc,
						bpsh.badge_ID,
						bpsh.name,
						bpsh.cycle_time,
						bpsh.transit_time
					##HAVING##
					ORDER BY
						num_cycles DESC,
						bpsh.cell_ID ASC,
						bpsh.ID ASC;";

			if ($_REQUEST['over_target_cycle_time'] == 1) {
				if ($_REQUEST['custom_target_cycle_time']) {
					$target = $_REQUEST['custom_target_cycle_time'];
				} else {
					$target = "bpsh.cycle_time";
				}
				$target_sql = "HAVING AVG(DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time)) > ".$target." ";
			} else {
				$target_sql = "";
			}
			$sql = str_replace("##HAVING##", $target_sql, $sql);
		} else {
		
			if ($res[0]['line_type'] == 0) {
				$sql = "SELECT s.line_id
							, CONCAT(s.ID, ' - ', s.station_desc) AS station
							, s.station_order
							, COUNT(msh.[cycle_time]) AS num_cycles
							, AVG(msh.[cycle_time]) AS avg_cycle_time
							, AVG(msh.[cycle_time]) + s.transit_time AS avg_full_cycle_time
							--, AVG(msh.[adjusted_cycle_time]) AS avg_adjusted_cycle_time
							, s.target_takt_time AS target_cycle_time
							, AVG(msh.[cycle_time]) - s.target_takt_time AS over_cycle_time
							, AVG(msh.[cycle_time]) + s.transit_time - s.target_takt_time AS full_over_cycle_time
							--, AVG(msh.[adjusted_cycle_time]) - s.target_takt_time AS over_cycle_time
							, o.name
							, msh.station_id
						FROM module_station_history msh 
						JOIN line_stations s ON s.station_id = msh.station_id 
							and s.line_id = msh.line_ID
							AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
						JOIN operators o ON o.id = msh.operator_id
						LEFT JOIN modules m ON m.ID = msh.module_ID
						LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
						WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
							AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
							AND s.line_id LIKE '".$line_id."'
							AND s.ID LIKE '".$station_id."'
							AND msh.[cycle_time] < 500 
							AND msh.[cycle_time] > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
							AND o.ID LIKE '".$operator_id."'
							AND mm.model LIKE '{$line_model}'
						GROUP BY o.name,s.ID,s.line_id,s.station_order,msh.station_id,s.station_desc, s.target_takt_time, s.transit_time
						ORDER BY num_cycles desc, s.line_id, msh.station_ID";	
			} else {
				$sql = "SELECT s.line_id
							, CONCAT(s.ID, ' - ', s.station_desc) AS station
							, s.station_order
							, COUNT(msh.[entry_time]) AS num_cycles
							, AVG(DATEDIFF(ss, entry_time, exit_time)) AS avg_cycle_time
							, AVG(DATEDIFF(ss, entry_time, exit_time)) + s.transit_time AS avg_full_cycle_time
							--, NULL AS avg_adjusted_cycle_time
							, s.target_takt_time AS target_cycle_time
							, AVG(DATEDIFF(ss, entry_time, exit_time)) - s.target_takt_time AS over_cycle_time
							, AVG(DATEDIFF(ss, entry_time, exit_time)) + s.transit_time - s.target_takt_time AS full_over_cycle_time
							--, NULL AS adjusted_over_cycle_time
							, o.name
							, msh.station_id
						FROM wip_part_station_history msh 
						JOIN line_stations s ON s.station_id = msh.station_id 
							and s.line_id = msh.line_ID
							--AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
						JOIN operators o ON o.id = msh.operator_id
						LEFT JOIN wip_parts wp ON wp.ID = msh.wip_part_ID
						LEFT JOIN modules m ON m.ID = wp.used_by_module_ID
						LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
						WHERE entry_time > '".$start_date." 00:00:00' 
							AND entry_time < '".$end_date." 23:59:59.99' 
							AND s.line_id LIKE '".$line_id."'
							AND s.ID LIKE '".$station_id."'
							AND DATEDIFF(ss, entry_time, exit_time) < 500 
							AND DATEDIFF(ss, entry_time, exit_time) > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
							AND o.ID LIKE '".$operator_id."'
							AND mm.model LIKE '{$line_model}'
						GROUP BY o.name,s.ID,s.line_id,s.station_order,msh.station_id,s.station_desc, s.target_takt_time, s.transit_time
						ORDER BY num_cycles desc, s.line_id, msh.station_ID";
			
			}
			//die($sql);

			// if ($_REQUEST['over_target_cycle_time'] == 1) {
			// 	if ($_REQUEST['custom_target_cycle_time']) {
			// 		$target = $_REQUEST['custom_target_cycle_time'];
			// 	} else {
			// 		$target = "s.target_takt_time";
			// 	}
			// 	$target_sql = "HAVING AVG(msh.cycle_time) > ".$target." ";
			// } else {
			// 	$target_sql = "";
			// }
			// $sql = str_replace("##HAVING##", $target_sql, $sql);
		}
		//echo $sql; die;
		//die($sql);
		$res = $db->query($sql);
		
		echo json_encode($res);
	}
	
	if ($action == 'cycle_times_detail') {
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		$start_date = $r['start_date'];
		$end_date = $r['end_date'];
		$line_id = $r['line_id'] == null ? "%" : $r['line_id'];
		$line_model = $r['line_model'] == null || $r['line_model'] == -1 ? "%" : $r['line_model'];
		$station_id = $r['station_id'] == null ? "%" : $r['station_id'];
		$operator_id = $r['operator_id'] == null ? "%" : $r['operator_id'];
		$shift = $r['shift'] == null ? "%" : $r['shift'];
		//$group_by = $r['group_by'] == null ? "%" : $r['group_by'];
		$program = $r['program'] == null ? "%" : $r['program'];

		//check for line_type		
		$sql = "SELECT line_type FROM lines 
				WHERE id = ".$r['line_id'];
		$res = $db->query($sql);

		if ($r['process_type'] == 'BATCH') {
			$sql = "SELECT
						bpsh.cell_ID AS line_id,
						CONCAT(bpsh.station_order, ' : ', bpsh.station_desc) AS station,
						bpsh.station_order,
						bpsh.entry_time,
						bpsh.exit_time,
						DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) AS cycle_time,
						DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) + bpsh.transit_time AS full_cycle_time,
						--NULL AS adjusted_cycle_time,
						bpsh.cycle_time AS target_cycle_time,
						DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) - bpsh.cycle_time AS over_cycle_time,
						DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) + bpsh.transit_time - bpsh.cycle_time AS full_over_cycle_time,
						--NULL AS adjusted_over_cycle_time,
						bpsh.name,
						bpsh.ID
					FROM (
						SELECT
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							s.cycle_time,
							s.transit_time,
							min(bph.entry_time) AS entry_time,
							max(bph.exit_time) AS exit_time,
							o.ID AS operator_ID,
							o.badge_id,
							o.name,
							bph.built_part_ID
						FROM built_part_history bph
							LEFT JOIN stations s ON s.station_order = bph.station_ID AND s.cell_ID = bph.cell_ID
							LEFT JOIN MES_COMMON.dbo.operators o ON o.ID = bph.operator
						GROUP BY
							bph.built_part_ID,
							s.ID,
							s.station_order,
							s.cell_ID,
							s.station_desc,
							s.cycle_time,
							s.transit_time,
							o.ID,
							o.badge_ID,
							o.name
						) AS bpsh
					WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND bpsh.cell_ID LIKE '".$line_id."'
						AND bpsh.ID LIKE '".$station_id."'
						AND bpsh.badge_ID LIKE '".$operator_id."'
						AND dbo.getShift(entry_time) LIKE '".$shift."'
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) < 500 
						AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > 5 
						##TARGET##
					GROUP BY
						bpsh.cell_ID,
						bpsh.station_order,
						bpsh.ID,
						bpsh.station_desc,
						bpsh.badge_ID,
						bpsh.name,
						bpsh.entry_time,
                        bpsh.exit_time,
                        bpsh.cycle_time,
                        bpsh.transit_time
					ORDER BY
						bpsh.entry_time DESC;";

			if ($_REQUEST['over_target_cycle_time'] == 1) {
				if ($_REQUEST['custom_target_cycle_time']) {
					$target = $_REQUEST['custom_target_cycle_time'];
				} else {
					$target = "bpsh.cycle_time";
				}
				$target_sql = "AND DATEDIFF(SECOND, bpsh.entry_time, bpsh.exit_time) > ".$target." ";
			} else {
				$target_sql = "";
			}
			$sql = str_replace("##TARGET##", $target_sql, $sql);

			// die($sql);
		} else if ($r['process_type'] == 'SEQUENCED') {

			if ($res[0]['line_type'] == 0) {		
				$sql = "SELECT s.line_id					
							, CONCAT(s.ID, ' - ', s.station_desc) AS station
							, s.station_order
							, o.name
							, entry_time
							, exit_time
							, msh.[cycle_time] AS cycle_time
							, msh.[cycle_time] + s.transit_time AS full_cycle_time
							--, msh.adjusted_cycle_time
							, s.target_takt_time AS target_cycle_time
							, msh.[cycle_time] - s.target_takt_time AS over_cycle_time
							, msh.[cycle_time] + s.transit_time - s.target_takt_time AS full_over_cycle_time
							--, msh.[adjusted_cycle_time] - s.target_takt_time AS adjusted_over_cycle_time
							, msh.station_id
						FROM module_station_history msh 
						JOIN line_stations s ON s.station_id = msh.station_id 
							and s.line_id = msh.line_ID
							AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
						JOIN operators o ON o.id = msh.operator_id
						LEFT JOIN modules m ON m.ID = msh.module_ID
						LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
						WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
							AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
							AND s.line_id LIKE '".$line_id."'
							AND mm.model LIKE '{$line_model}'
							--AND msh.station_id LIKE '".$station_id."'
							AND s.id LIKE '".$station_id."'
							AND o.badge_id LIKE '".$operator_id."'
							--AND LEFT(m.VIN, 2) LIKE '$program'
							AND msh.[cycle_time] < 500 
							AND msh.[cycle_time] > 5 
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'	
							##TARGET##			
						ORDER BY entry_time DESC";

				if ($_REQUEST['over_target_cycle_time'] == 1) {
					if ($_REQUEST['custom_target_cycle_time']) {
						$target = $_REQUEST['custom_target_cycle_time'];
					} else {
						$target = "s.target_takt_time";
					}
					$target_sql = "AND msh.cycle_time > ".$target." ";
				} else {
					$target_sql = "";
				}
				$sql = str_replace("##TARGET##", $target_sql, $sql);

			} else {
				$sql = "SELECT s.line_id					
							, CONCAT(s.ID, ' - ', s.station_desc) AS station
							, s.station_order
							, o.name
							, entry_time
							, exit_time
							, DATEDIFF(ss,entry_time,exit_time) AS cycle_time
							, DATEDIFF(ss,entry_time,exit_time) + s.transit_time AS full_cycle_time
							--, NULL AS adjusted_cycle_time
							, s.target_takt_time AS target_cycle_time
							, DATEDIFF(ss,entry_time,exit_time) - s.target_takt_time AS over_cycle_time
							, DATEDIFF(ss,entry_time,exit_time) + s.transit_time - s.target_takt_time AS full_over_cycle_time
							--, NULL AS adjusted_over_cycle_time
							, msh.station_id
						FROM wip_part_station_history msh 
						JOIN line_stations s ON s.station_id = msh.station_id 
							and s.line_id = msh.line_ID
							--AND s.station_options LIKE CASE WHEN s.station_id = '100180' THEN '%'+msh.station_instance+'%' ELSE '%' END
						JOIN operators o ON o.id = msh.operator_id
						LEFT JOIN wip_parts wp ON wp.ID = msh.wip_part_ID
						LEFT JOIN modules m ON m.ID = wp.used_by_module_ID
						LEFT JOIN v_module_models mm ON mm.module_ID = m.ID
						WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
							AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
							AND s.line_id LIKE '".$line_id."'
							AND mm.model LIKE '{$line_model}'
							--AND msh.station_id LIKE '".$station_id."'
							AND s.id LIKE '".$station_id."'
							AND o.badge_id LIKE '".$operator_id."'
							--AND LEFT(m.VIN, 2) LIKE '$program'
							AND DATEDIFF(ss,entry_time,exit_time) < 500 
							AND DATEDIFF(ss,entry_time,exit_time) > 5
							AND dbo.getShift('".$line_id."', entry_time) LIKE '".$shift."'
							##TARGET##
						ORDER BY entry_time DESC";

				if ($_REQUEST['over_target_cycle_time'] == 1) {
					if ($_REQUEST['custom_target_cycle_time']) {
						$target = $_REQUEST['custom_target_cycle_time'];
					} else {
						$target = "s.target_takt_time";
					}
					$target_sql = "AND DATEDIFF(ss,entry_time,exit_time) > ".$target." ";
				} else {
					$target_sql = "";
				}
				$sql = str_replace("##TARGET##", $target_sql, $sql);
				
			}
			//die($sql);


		} else if ($r['process_type'] == 'REPETITIVE') {
			$sql = "SELECT 
						t.tool_description
						,[cycle_time] AS entry_time
						,[cycle_duration] AS cycle_time
						, NULL AS name
					  FROM [machine_cycles] c
					  JOIN tools t on c.tool_ID = t.ID
					  WHERE entry_time > CASE WHEN '".$shift."' = '3' THEN DATEADD(d, -1, '".$start_date." 23:00:00') ELSE '".$start_date." 00:00:00' END
						AND entry_time < CASE WHEN '".$shift."' = '3' THEN '".$end_date." 23:00:00' ELSE '".$end_date." 23:59:59.99' END
						AND c.machine_ID = ".$r['machine_ID']."
						AND dbo.getShift(cycle_time) LIKE '".$shift."'
					ORDER BY cycle_time DESC";
		}
		
		//echo $sql; die;
		//die($sql);
		$res = $db->query($sql);
		echo json_encode($res);
	}
	
?>