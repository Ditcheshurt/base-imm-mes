<?php

    require_once("../../../init.php");

    $action = $_REQUEST['action'];
    
    if ($action == 'load_data') {
        global $db;
        $res = new StdClass();
        $r = $_REQUEST;
        
        $module_ID = $r['module_ID'];
        
        $sql = "SELECT
                    m.ID,
                    m.build_order,
                    m.sequence,
                    m.VIN,
                    m.line_ID,
                    l.line_code,
                    o.name AS operator_name,
                    m.recvd_time,
                    m.loaded_time,
                    m.built_time,
                    m.reject_status
                FROM
                    modules m
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON o.ID = m.operator_id
                    LEFT JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = ".$module_ID;

        $res->module_info = $db->query($sql);
        
        $vin = $res->module_info[0]['VIN'];
        
        //broadcast info
        $sql = "SELECT
                    l.line_code,
                    m.sequence,
                    m.loaded_time,
                    l.preview_status,
                    l.build_status,
                    l.info_status
                FROM
                    modules m
                    LEFT OUTER JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = ".$module_ID;
        $res->broadcast_info = $db->query($sql);
        
        $mod_line_ID = $res->module_info[0]['line_ID'];
        


        // //service message info
        // $sql = "SELECT
        //             Substring(raw_data, 11, 8) as VIN,
        //             Substring(raw_data, 24, 11) as Sequence,
        //             Sent_Time, raw_data As [Parts Changed]
        //         FROM
        //             broadcasts_out b
        //             JOIN lines l ON SUBSTRING(b.queue, 4, 7) = SUBSTRING(l.queue_in, 10, 7) AND l.ID = " . $mod_line_ID . "
        //         WHERE
        //             SUBSTRING(b.raw_data, 11, 8) = '".substr($vin, -8)."'
        //             and msg_type = 'SR'";

        // $q = $db->query($sql);
        // $res->sr_message_info = $q[0];


        // components
        $sql = "SELECT
                    part_number,
                    qty
                FROM
                    components_in
                WHERE
                    veh_id = ".$module_ID."
                ORDER BY part_number ASC;";

        $res->component_info = $db->query($sql);


        // station history
        $sql = "SELECT DISTINCT
                    ls.ID AS real_station_ID,
                    msh.station_ID,
                    ls.station_order,
                    ls.station_desc,
                    msh.station_instance,
                    o.name AS operator_name,
                    msh.entry_time,
                    msh.exit_time,
                    DATEDIFF(SECOND, msh.entry_time, msh.exit_time) AS cycle_time
                FROM
                    module_station_history msh
                    JOIN line_stations ls ON ls.line_ID = msh.line_ID
                        AND ls.station_ID = msh.station_ID
                        AND ls.station_options LIKE '%'+msh.station_instance+'%'
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msh.module_ID = $module_ID
                ORDER BY ls.station_order ASC;";

        $res->station_history_info = $db->query($sql);

        foreach($res->station_history_info as $i => &$station) {
            // station instruction history
            $sql = "SELECT DISTINCT
                        msih.ID,
                        ls.station_id,
                        ls.station_desc,
                        msih.instr_order,
                        tt.type_desc,
                        msih.start_time,
                        msih.complete_time,
                        msih.completion_details
                    FROM
                        module_station_instruction_history msih
                        LEFT JOIN
                            instructions i ON i.part_number = msih.part_number
                            AND i.instr_order = msih.instr_order
                            AND i.station_id = msih.station_id
                            AND i.part_order = msih.part_order
                        LEFT JOIN
                            test_types tt ON tt.ID = i.test_type
                        LEFT JOIN
                            line_stations ls ON ls.station_id = msih.station_id
                    WHERE
                        msih.module_ID = $module_ID
                        AND ls.ID = ".$station['real_station_ID']."
                    ORDER BY
                            ls.station_id,
                            msih.instr_order;";

            $station['station_instruction_history'] = $db->query($sql);
        }

        if ($res->module_info[0]['reject_status'] == 'REWORKED' || $res->module_info[0]['reject_status'] == 'REJECTED_REWORK') {
            // get rework data
            $sql = "SELECT TOP 1
                        md.defect_time,
                        m.reject_status,
                        md.rework_time,
                        o1.name AS defect_operator,
                        ls.station_desc AS defect_station_desc,
                        o2.name AS rework_operator
                    FROM
                        modules m
                        LEFT JOIN module_defects md ON md.module_ID = m.ID
                        LEFT JOIN operators o1 ON o1.ID = md.defect_operator_ID
                        LEFT JOIN line_stations ls ON ls.station_ID = md.defect_station_ID
                        LEFT JOIN module_defect_repair_instruction_history h ON h.module_ID = md.module_ID
                        LEFT JOIN operators o2 ON o2.ID = h.operator_ID
                    WHERE
                        md.module_ID = {$module_ID}
                        AND m.reject_status LIKE '%REWORK%'
                    GROUP BY
                        md.defect_time,
                        m.reject_status,
                        md.rework_time,
                        o1.name,
                        ls.station_desc,
                        o2.name;";
            $r = $db->query($sql);

            if ($r) {
                $res->rework_info = $r[0];

                // get topper data
                $sql = "SELECT
                            CASE WHEN COUNT(md.ID) > 0 THEN 1 ELSE 0 END AS new_topper
                        FROM
                            module_defects md
                            LEFT JOIN modules m ON m.ID = md.module_ID
                        WHERE
                            md.comments LIKE '%WIP_PARTS_WIPED%'
                            AND m.sequence IN (
                                SELECT sequence FROM modules WHERE ID = {$module_ID}
                            );";
                $t = $db->query($sql);
                $res->rework_info['new_topper'] = $t[0]['new_topper'];
            } else {
                $res->rework_info = $r;
            }
            

            // get rework station history
            $sql = "SELECT
                        ls.ID AS real_station_ID,
                        ls.station_ID,
                        ls.station_desc,
                        max(mdrih.complete_time) AS complete_time,
                        mdrih.operator_ID,
                        o.name AS operator_name
                    FROM
                        module_defect_repair_instruction_history mdrih
                        LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                        LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                        LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                    WHERE
                        mdrih.module_ID = {$module_ID}
                    GROUP BY
                        ls.ID,
                        ls.station_ID,
                        ls.station_desc,
                        mdrih.operator_ID,
                        o.name
                    ORDER BY
                        ls.station_ID ASC;";

            $res->rework_station_history = $db->query($sql);

            foreach ($res->rework_station_history as $i => &$station) {
                // get rework instruction history
                $sql = "SELECT
                            ls.station_ID,
                            ls.station_desc,
                            i.instr_order,
                            tt.type_desc,
                            mdrih.complete_time,
                            mdrih.completion_details,
                            o.name AS operator_name,
                            mdrih.operator_ID
                        FROM
                            module_defect_repair_instruction_history mdrih
                            LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                            LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                            LEFT JOIN test_types tt ON tt.ID = i.test_type
                            LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                        WHERE
                            mdrih.module_ID = {$module_ID}
                            AND ls.ID = {$station['real_station_ID']}
                        ORDER BY
                            i.station_ID ASC,
                            i.instr_order ASC,
                            mdrih.complete_time ASC;";

                $station['rework_instruction_history'] = $db->query($sql);
            }
        }


        // pick info
        $sql = "SELECT
                    prm.pocket,
                    prm.status AS pick_rack_module_status,
                    prm.serial,
                    pr.start_time AS pick_time,
                    pr.status AS pick_rack_status
                FROM
                    modules m
                    LEFT OUTER JOIN pick_rack_modules prm ON prm.module_ID = m.ID
                    LEFT OUTER JOIN pick_racks pr ON pr.ID = prm.pick_rack_ID
                WHERE
                    prm.module_ID = ".$module_ID;

        $res->pick_info = $db->query($sql);


        // sub assembly info

        //get wip parts
        $sql = "SELECT
                    id,
                    internal_serial,
                    built_time
                FROM
                    wip_parts
                WHERE
                    used_by_module_ID = ".$module_ID;

        $res->sub_assembly_info = $db->query($sql);

        if ($res->sub_assembly_info) {
            // get wip station history
            foreach($res->sub_assembly_info as $i => &$part) {
                $sql = "SELECT
                            ls.station_desc,
                            wpsh.entry_time,
                            wpsh.exit_time,
                            op.name AS operator_name,
                            wpsh.station_ID
                        FROM
                            wip_part_station_history wpsh
                            LEFT JOIN operators op ON op.id = wpsh.operator_id
                            LEFT JOIN (SELECT DISTINCT station_ID, station_desc FROM line_stations) ls ON ls.station_id = wpsh.station_id
                        WHERE
                            wip_part_id = ".$part['id'].";";

                $part['station_history'] = (array) $db->query($sql);
                //die(json_encode($part));

                // get wip instruction history
                foreach($part['station_history'] as $j => &$station) {
                    $sql = "SELECT DISTINCT
                                wpsih.ID,
                                ls.station_id,
                                wpsih.step_order,
                                tt.type_desc,
                                wpsih.entry_time,
                                wpsih.exit_time,
                                wpsih.completion_details
                            FROM
                                wip_part_station_instruction_history wpsih
                                LEFT JOIN
                                    instructions i ON i.part_number = wpsih.part_number
                                    AND i.instr_order = wpsih.step_order
                                    AND i.station_id = wpsih.station_id
                                    AND i.part_order = wpsih.part_order
                                LEFT JOIN
                                    test_types tt ON tt.ID = i.test_type
                                LEFT JOIN
                                    (SELECT DISTINCT station_ID FROM line_stations) AS ls ON ls.station_id = wpsih.station_id
                            WHERE
                                wpsih.wip_part_ID = ".$part['id']."
                                AND wpsih.station_id = ".$station['station_ID']."
                            ORDER BY wpsih.step_order;";

                    $station['instruction_history'] = $db->query($sql);
                }
            }
        }

        // // sub-assembly
        // // WIP/Kit Info
        // $sql = "SELECT
        //             --l.line_code,
        //             ls.station_desc,
        //             wp.internal_serial,
        //             wp.external_serial,
        //             o.name AS operator_name,
        //             wpsh.entry_time,
        //             wpsh.exit_time
        //         FROM modules m
        //             LEFT OUTER JOIN kit_container_history kch ON kch.mod_ID = m.ID
        //             LEFT OUTER JOIN lines l ON l.ID = kch.line_ID
        //             LEFT OUTER JOIN line_stations ls ON ls.line_id = l.ID
        //             JOIN wip_parts wp ON wp.used_by_module_ID = m.ID
        //             JOIN wip_part_station_history wpsh ON wpsh.wip_part_ID = wp.ID and wpsh.line_id = l.id
        //             JOIN MES_COMMON.dbo.operators o ON o.ID = wpsh.operator_ID
        //         WHERE
        //             wp.used_by_module_ID = ".$module_ID."
        //         UNION 
        //         SELECT
        //             ls.station_desc,
        //             wp.internal_serial,
        //             wp.external_serial,
        //             o.name AS operator_name,
        //             wpsh.entry_time,
        //             wpsh.exit_time
        //         FROM
        //             wip_part_station_history wpsh 
        //             JOIN wip_parts wp ON wp.id = wpsh.wip_part_ID
        //             JOIN lines l ON l.id = wpsh.line_id
        //             JOIN line_stations ls ON wpsh.station_id = ls.station_id
        //             JOIN MES_COMMON.dbo.operators o ON o.ID = wpsh.operator_ID                  
        //         WHERE
        //             wp.used_by_module_ID = ".$module_ID."
        //             AND line_type > 0
        //             ORDER BY wpsh.entry_time ASC";

        // $res->subassembly_info = $db->query($sql);


        // torque info
        $sql = "SELECT
                    ls.station_desc,
                    o.name AS operator_name,
                    msih.part_number,
                    msh.entry_time,
                    msih.complete_time,
                    i.prompt AS torque,
                    dbo.fn_getJSONIntegerValue(instr_options, 'torques_required') AS torque_count
                FROM
                    module_station_history msh
                    JOIN module_station_instruction_history AS msih ON msh.module_ID = msih.module_ID AND msh.station_ID = msih.station_ID
                    JOIN instructions i ON i.part_number = msih.part_number AND i.instr_order = msih.instr_order AND i.station_id = msih.station_id AND i.test_type in (
                        SELECT ID FROM test_types
                    )
                    JOIN line_stations ls ON ls.station_id = msih.station_id
                    JOIN test_types tt ON i.test_type = tt.ID
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msih.module_ID = ".$module_ID."
                    AND tt.machine_type = 2
                ORDER BY
                    ls.station_order;";

        $res->torque_info = $db->query($sql);


        // // assembly components used
        // $sql = "SELECT DISTINCT
        //                 c.Part_Number,
        //                 COALESCE(mrp.part_desc, qad.pt_part + ':' + qad.pt_desc1 + ' ' + qad.pt_desc2, 'NOT FOUND') as Description,
        //                 c.Qty
        //             FROM
        //                 components_used c
        //                 LEFT OUTER  JOIN (SELECT DISTINCT part_number, part_desc FROM mrp_part_mstr) As mrp ON c.part_number = mrp.part_number
        //                 LEFT OUTER JOIN [TEAMMRP01].[REPL_225].[dbo].[v_NORPLAS_QAD_KL] qad ON c.part_number = qad.cp_cust_part
        //             WHERE
        //                 c.veh_id = '".$request['module_ID']."'
        //             ORDER BY
        //                 c.part_number";

        // $res->assembly_info = $db->query($sql);


        // // packout
        // $sql = "SELECT
        //             s.ID as 'Shipment Number',
        //             s.ship_time as [Shipment Time],
        //             RIGHT(CAST(sl.rack_order as varchar(10)), 3) as [Rack Order],
        //             permanent_rack_ID as [Rack Number],
        //             si.lane_position as [Slot],
        //             m.VIN,
        //             m.Sequence
        //         FROM
        //             shipments s
        //             LEFT OUTER JOIN shipment_lanes sl ON sl.shipment_id = s.ID
        //             JOIN shipment_items si ON sl.ID = si.lane_ID
        //             JOIN modules m ON si.mod_ID = m.ID
        //         WHERE
        //             sl.ID = (SELECT lane_ID FROM shipment_items WHERE mod_ID = '".$request['module_ID']."')
        //             and sl.module_line_id = m.line_id 
        //         ORDER BY
        //             si.lane_position";
        
        // $res->packout_all_info = $db->query($sql);




        // new EOL
        $sql = "SELECT
                    t.ID,
                    t.start_time,
                    MIN(t.result) AS result
                FROM (
                    SELECT
                        th.ID,
                        td.test_type,
                        td.test_item,
                        th.start_time,
                        MAX(CAST(td.passed AS TINYINT)) AS result
                    FROM
                        EOL_test_history th
                        LEFT JOIN EOL_test_history_detail td ON td.test_history_ID = th.ID
                    WHERE
                        th.module_ID = $module_ID
                    GROUP BY
                        th.ID,
                        th.start_time,
                        td.test_type,
                        td.test_item
                ) AS t
                GROUP BY
                    t.ID,
                    t.start_time
                ORDER BY
                    t.start_time ASC;";
        
        $res->eol_test_info = $db->query($sql);

        if ($res->eol_test_info) {
            // EOL test detail
            foreach($res->eol_test_info as $k => &$test) {
                // $sql = "SELECT
                //             thd.ID,
                //             thd.channel_num,
                //             tops.test_description,
                //             thd.resistance_min,
                //             thd.resistance_max,
                //             thd.m_resistance,
                //             thd.test_result
                //         FROM
                //             NYSUS_EOL_TEST.dbo.test_history_detail thd
                //         LEFT JOIN
                //             NYSUS_EOL_TEST.dbo.test_options tops ON tops.ID = thd.test_option_ID
                //         WHERE
                //             thd.test_history_ID = ".$test['ID']."
                //         ORDER BY
                //             thd.channel_num;";

                $sql = "SELECT
                            ID,
                            test_item,
                            min_float_val,
                            max_float_val,
                            float_val,
                            passed
                        FROM
                            EOL_test_history_detail
                        WHERE
                            test_history_ID = ".$test['ID']."
                        ORDER BY
                            ID;";
                
                $test['detail'] = $db->query($sql);
            }
        } else {
            //Old EOL Info
            $sql = "SELECT
                        th.ID,
                        th.start_time,
                        th.end_time
                    FROM
                        NYSUS_EOL_TEST.test_history th
                    WHERE
                        th.module_ID = {$module_ID}";

            $res->eol_info = $db->query($sql);

            //EOL Test History Info
            $sql = "SELECT * FROM (
                        (
                            SELECT
                                th.ID,
                                th.start_time,
                                th.end_time,
                                th.result
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history th
                                LEFT JOIN NYSUS_SEQUENCE_MES.dbo.module_station_instruction_history msih ON th.ID = CAST(REPLACE(msih.completion_details, 'EOL PASSED: ', '') AS INT)
                            WHERE
                                msih.module_ID = ".$module_ID."
                                AND msih.completion_details LIKE 'EOL PASSED: %'
                        ) UNION (
                            SELECT
                                th.ID,
                                th.start_time,
                                th.end_time,
                                th.result
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history th
                                LEFT JOIN NYSUS_SEQUENCE_MES.dbo.module_defect_repair_instruction_history mdrih ON th.ID = CAST(REPLACE(mdrih.completion_details, 'EOL PASSED: ', '') AS INT)
                            WHERE
                                mdrih.module_ID = ".$module_ID."
                                AND mdrih.completion_details LIKE 'EOL PASSED: %'
                        )
                    ) AS th";
            $res->eol_test_info = $db->query($sql);

            if ($res->eol_test_info) {
                // EOL test detail
                foreach($res->eol_test_info as $k => &$test) {
                    $sql = "SELECT
                                thd.ID,
                                --thd.channel_num,
                                tops.test_description AS test_item,
                                thd.resistance_min AS min_float_val,
                                thd.resistance_max AS max_float_val,
                                thd.m_resistance AS float_val,
                                thd.test_result AS passed
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history_detail thd
                            LEFT JOIN
                                NYSUS_EOL_TEST.dbo.test_options tops ON tops.ID = thd.test_option_ID
                            WHERE
                                thd.test_history_ID = ".$test['ID']."
                            ORDER BY
                                thd.channel_num;";

                    // $sql = "SELECT
                    //             ID,
                    //             test_item,
                    //             min_float_val,
                    //             max_float_val,
                    //             float_val,
                    //             passed
                    //         FROM
                    //             EOL_test_history_detail
                    //         WHERE
                    //             test_history_ID = ".$test['ID']."
                    //         ORDER BY
                    //             ID;";
                    
                    $test['detail'] = $db->query($sql);
                }
            }
        }


        // // shipping info
        // $sql = "SELECT
        //             s.ID,
        //             s.shipment_sequence,
        //             s.ship_time,
        //             s.dock_number,
        //             s.shipment_sequence
        //         FROM
        //             shipments s
        //             LEFT OUTER JOIN shipment_lanes sl ON sl.shipment_id = s.ID
        //             LEFT OUTER JOIN shipment_rack_groups srg ON sl.shipment_rack_group_id = srg.ID
        //             LEFT OUTER JOIN shipment_items si ON si.lane_ID = sl.id
        //         WHERE
        //             si.mod_ID  = ".$request['module_ID'];

        // $res->shipping_info = $db->query($sql);


        if ($_REQUEST['mrp_company_code'] == '539IP') {
        //if ($_REQUEST['mrp_company_code'] == '539IP') {
            $date_parts = explode('-', $res->module_info[0]['built_time']->format('Y-m-d'));

            // // main line images
            // if ($res->module_info[0]['reject_status'] != 'REWORKED') {
            //     $res->main_line_images = array();
            //     $dir_path = '/539IP/archive/'.$date_parts[0].'/MONTH'.$date_parts[1].'/DAY'.$date_parts[2].'/MODULE_'.$module_ID.'/';
            //     for($i=1; $i<=4; $i++) {
            //         $file_name = $res->module_info[0]['sequence'] . '_' . $i . '.jpg';
            //         array_push($res->main_line_images, $dir_path . $file_name);
            //     }
            // } else {
            //     $res->main_line_images = null;
            // }

            $res->main_line_images = array();
            $dir_path = '/539IP/archive/'.$date_parts[0].'/MONTH'.$date_parts[1].'/DAY'.$date_parts[2].'/MODULE_'.$module_ID.'/';
            for($i=1; $i<=4; $i++) {
                $file_name = $res->module_info[0]['sequence'] . '_' . $i . '.jpg';
                array_push($res->main_line_images, $dir_path . $file_name);
            }

            //chute images
            $res->chute_images = array();
            $real_path = 'D:\\539_vision\\Archive\\'.$date_parts[0].'\\MONTH'.$date_parts[1].'\\DAY'.$date_parts[2].'\\MODULE_'.$module_ID;
            $real_path_2 = 'E:\\539IP\\archive\\'.$date_parts[0].'\\MONTH'.$date_parts[1].'\\DAY'.$date_parts[2].'\\MODULE_'.$module_ID;
            $dir_path = '/539_vision_images/Archive/'.$date_parts[0].'/MONTH'.$date_parts[1].'/DAY'.$date_parts[2].'/MODULE_'.$module_ID.'/';

            $image_names = array(
                '539Chute' => '',
                '539Front' => '', 
                '539Rear' => ''
            );

            if (is_dir($real_path) || is_dir($real_path_2)) {
                if (is_dir($real_path)) {
                    $rp = $real_path;
                } else {
                    $rp = $real_path_2;
                }
                $files = array_diff(scandir($rp), array('.', '..'));
                
                foreach($files as $i => $file_name) {
                    foreach($image_names as $type => &$max) {
                        $matches = array();
                        preg_match('/^'.$type.'[0-9]+\.jpg$/', $file_name, $matches);
                        if (!empty($matches) && $matches[0] > $max) {
                            $max = $matches[0];     
                        }
                    }
                }

                foreach($image_names as $type => $name) {
                    array_push($res->chute_images, $dir_path . $name);
                }
            }
        }

        echo json_encode($res);
    }

?>