<?php 

	require_once("../../../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);

	function get_production_snapshots ($request, $db) {
		$sql = "SELECT
					l.ID AS line_ID,
					l.line_code,
					--l.module_code,
					ISNULL(ps.prod_shift_1, 0) + ISNULL(ps.prod_shift_2, 0) AS prod_total,
					ISNULL(ps.cust_prod_shift_1, 0) + ISNULL(ps.cust_prod_shift_2, 0) AS cust_prod_total,
					(ISNULL(ps.prod_shift_1, 0) + ISNULL(ps.prod_shift_2, 0)) - (ISNULL(ps.cust_prod_shift_1, 0) + ISNULL(ps.cust_prod_shift_2, 0)) AS diff_total,
					ISNULL(ps.yest_prod_shift_1, 0) + ISNULL(ps.yest_prod_shift_2, 0) AS yest_prod_total,
					ISNULL(ps.last_broadcast_time_ago, 0) AS last_broadcast_time_ago,
					ISNULL(ps.current_buffer, 0) AS current_buffer,
					ISNULL(ps.current_queue, 0) AS current_queue,
					ISNULL(ps.buffer_warning, 0) AS buffer_warning,
					ISNULL(ps.buffer_critical, 0) AS buffer_critical,
					ISNULL(ps.pre_queue, 0) AS pre_queue,
					ISNULL(ps.pre_queue_warning, 0) AS pre_queue_warning,
					ISNULL(ps.pre_queue_critical, 0) AS pre_queue_critical,
					ISNULL(l.parts_per_rack, 0) AS parts_per_rack,
					ISNULL(ps.current_on_conveyor, 0) + ISNULL(ps.current_parts_in_truck, 0) AS built_unshipped,
					--l2.ID,
					'<div>' + CASE
						WHEN l.copy_to_line_ID IS NOT NULL THEN l.module_code + '(L)'
						WHEN l2.ID IS NOT NULL THEN l2.module_code + '(R)'
						ELSE l.module_code
					END + '</div><div style=font-size:7pt;>' + l.line_code + '</div>' AS module_code
				FROM
					production_snapshot ps
					LEFT JOIN lines l ON ps.line_ID = l.ID
					LEFT JOIN lines l2 ON l.ID = l2.copy_to_line_ID
				WHERE
					l.system_ID = {$request['system_ID']}
				ORDER BY
					l.ID;";
		$lines = $db->query($sql);
		echo json_encode($lines);
	}

	function get_time_since_last_broadcast ($request, $db) {
		$sql = "SELECT
					MIN(ps.last_broadcast_time_ago) AS last_broadcast_time_ago
				FROM
					production_snapshot ps
					LEFT JOIN lines l ON ps.line_ID = l.ID
				WHERE
					l.system_ID = {$request['system_ID']};";
		$lines = $db->query($sql);
		if ($lines) {
			$result = $lines[0];
		}
		echo json_encode($result);
	}

	function get_broadcast_issues ($request, $db) {
		$sql = "SELECT
						ID,
						line_code
					FROM
						lines
					WHERE
						system_ID = {$request['system_ID']};";
		$lines = $db->query($sql);
		
		if ($lines) {
			foreach($lines as &$line) {
				$sql = "SELECT TOP 10
								bi.broadcast_type,
								bi.alert_time,
								bi.broadcast_issue,
								bi.broadcast_ID,
								bin.line_ID,
								bin.VIN
							FROM
								broadcast_issues bi
								LEFT JOIN broadcasts_in bin ON bin.ID = bi.broadcast_ID
							WHERE
								bin.line_ID = {$line['ID']}
								bi.ack_time IS NULL
							ORDER BY
								bi.alert_time ASC;";
				
				$line['broadcast_issues'] = $db->query($sql);
			}
		}
		
		echo json_encode($lines);
	}
?>