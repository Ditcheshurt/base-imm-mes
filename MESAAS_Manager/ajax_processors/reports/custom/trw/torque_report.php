<?php

require_once("../../../init.php");

$action = $_REQUEST['action'];

call_user_func($action, $db);

function get_torque_data($db) {	
	// is it a batch line
	if (is_batch_line($db)){
		$res = get_batch_data($db);
	} else {
		$res = get_sequenced_data($db);
	}
	
	if ($res == null) {
		$res = array();		
	}
	echo json_encode($res);

}

function get_batch_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];
	$station_ID = $_REQUEST["stations"];

	if (is_station_auto_cell($db)) {

		$sql = "SELECT
					ls.ID AS station_ID,
					ls.station_desc,
					wp.internal_serial,
					wpsv.the_time,
					wpsv.tag_desc,
					wpsv.tag_value,
					wpsv.test_status,
					wpsv.attempt_number
				FROM
					wip_part_stored_plc_values wpsv
				JOIN
					wip_parts wp ON wpsv.wip_part_ID = wp.ID
				JOIN
					wip_part_setup wps ON wp.part_ID = wps.ID
				JOIN
					line_stations ls ON wpsv.station_ID = ls.id
				WHERE
					CONVERT(DATE, dbo.getProdDate(ls.line_ID, wpsv.the_time), 113) BETWEEN '$start_date' AND '$end_date'
					AND ls.ID = $station_ID";

	} else {
		$sql = "SELECT
					ls.ID AS station_ID,
					ls.station_desc,
					wp.internal_serial,
					i.part_number,
					i.prompt,
					mtv.torque_time,
					mtv.torque_value,
					mtv.angle_value,
					mtv.passed,
					mtv.cleared,
					mtv.attempt_number
				FROM
					module_station_torque_values mtv
				JOIN
					wip_parts wp ON mtv.wip_part_ID = wp.ID
				JOIN
					wip_part_setup wps ON wp.part_ID = wps.ID
				JOIN
					line_stations ls ON mtv.station_ID = ls.id
				JOIN
					lines l ON ls.line_id = l.ID
				JOIN
					instructions i ON mtv.station_ID = i.station_ID
					AND mtv.part_order = i.part_order
					AND mtv.instr_order = i.instr_order
				WHERE
					CONVERT(DATE, dbo.getProdDate(l.ID, mtv.torque_time), 113) BETWEEN '$start_date' AND '$end_date'
					AND ls.ID = $station_ID";
	}

	return $db->query($sql);

}

function get_sequenced_data($db) {
	$start_date = $_REQUEST["start_date"];
	$end_date = $_REQUEST["end_date"];
	$station_ID = $_REQUEST["stations"];

	if (is_station_auto_cell($db)) {
		$sql = "SELECT
					m.VIN,
					m.sequence,
					ls.ID AS station_ID,
					ls.station_desc,
					mspv.tag_desc,
					mspv.tag_value,
					mspv.attempt_number
				FROM
					module_stored_plc_values mspv
				JOIN
					modules m ON mspv.module_ID = m.ID
				JOIN
					line_stations ls ON mspv.station_ID = ls.id
				JOIN
					lines l ON ls.line_id = l.ID
				WHERE
					CONVERT(DATE, dbo.getProdDate(ls.line_ID, mspv.the_time), 113) BETWEEN '$start_date' AND '$end_date'
					AND ls.ID = $station_ID";

	} else {
		$sql = "SELECT
					ls.ID AS station_ID,
					ls.station_desc,
					m.VIN,
					m.sequence,
					i.part_number,
					i.prompt,
					mtv.torque_time,
					mtv.torque_value,
					mtv.angle_value,
					mtv.passed,
					mtv.cleared,
					mtv.attempt_number
				FROM
					module_station_torque_values mtv
				JOIN
					modules m ON mtv.module_ID = m.ID
				--JOIN
				--	module_station_instruction_history msih ON msih.module_ID = m.ID
				--	AND msih.instr_order = mtv.instr_order
				--	AND msih.part_order = mtv.part_order
				--	AND msih.station_ID = mtv.station_ID
				JOIN
					line_stations ls ON mtv.station_ID = ls.id
				JOIN
					lines l ON ls.line_id = l.ID
				JOIN
					instructions i ON mtv.station_ID = i.station_ID
					AND mtv.part_order = i.part_order
					AND mtv.instr_order = i.instr_order
				WHERE
					CONVERT(DATE, dbo.getProdDate(l.ID, mtv.torque_time), 113) BETWEEN '$start_date' AND '$end_date'
					AND ls.ID = $station_ID";
	}
//die($sql);
	return $db->query($sql);
}

/**
 * get the type of station (auto or manual)
 * @param $db
 * @return bool
 */
function is_station_auto_cell($db) {

	$station_ID = $_REQUEST["stations"];

	$sql = "SELECT
				dbo.fn_getJSONStringValue(ls.station_options, 'auto_cell') AS is_autocell
			FROM
				line_stations ls
			WHERE
				ID = $station_ID";

	$res = $db->query($sql);

	if (count($res) > 0) {
		return $res[0]["is_autocell"];
	} else {
		return false;
	}

}

/**
 * is the line a batch line
 * @param $db
 * @return bool
 */
function is_batch_line($db) {

	$line_ID = $_REQUEST["lines"];

	$sql = "SELECT
				line_type
			FROM
				lines l
			WHERE
				ID = $line_ID
				AND line_type IN (1, 7, 8)";

	$res = $db->query($sql);

	if (count($res) > 0) {
		return true;
	} else {
		return false;
	}

}