<?php

    require_once("../../../init.php");

    $action = $_REQUEST['action'];

    call_user_func($action, $_REQUEST, $db);


    function get_module ($request, $db) {

        $query = "SELECT
                    m.ID,
                    m.build_order,
                    m.sequence,
                    m.VIN,
                    m.line_ID,
                    l.line_code,
                    o.name AS operator_name,
                    m.recvd_time,
                    m.loaded_time,
                    m.built_time,
                    m.reject_status
                FROM
                    modules m
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON o.ID = m.operator_ID
                    LEFT JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = {$request['module_ID']};";
        $result = $db->query($query);

        if ($result) {
            $data = $result[0];
        } else {
            $data = $result;
        }

        echo json_encode($data);
    }

    function get_components ($request, $db) {

        $query = "SELECT
                    part_number,
                    qty
                FROM
                    components_in
                WHERE
                    veh_id = {$request['module_ID']}
                ORDER BY part_number ASC;";
        $result = $db->query($query);
        
        echo json_encode($result);
    }

    function get_station_history ($request, $db) {

        // station history
        $sql = "SELECT DISTINCT
                    ls.ID AS real_station_ID,
                    msh.station_ID,
                    ls.station_order,
                    ls.station_desc,
                    msh.station_instance,
                    o.name AS operator_name,
                    msh.entry_time,
                    msh.exit_time,
                    DATEDIFF(SECOND, msh.entry_time, msh.exit_time) AS cycle_time
                FROM
                    module_station_history msh
                    JOIN line_stations ls ON ls.line_ID = msh.line_ID
                        AND ls.station_ID = msh.station_ID
                        AND ls.station_options LIKE '%'+msh.station_instance+'%'
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msh.module_ID = {$request['module_ID']}
                ORDER BY ls.station_order ASC;";

        $result = $db->query($sql);

        if ($result) {
            foreach($result as $i => &$station) {
                // station instruction history
                $sql = "SELECT DISTINCT
                            msih.ID,
                            ls.station_id,
                            ls.station_desc,
                            msih.instr_order,
                            tt.type_desc,
                            msih.start_time,
                            msih.complete_time,
                            msih.completion_details
                        FROM
                            module_station_instruction_history msih
                            LEFT JOIN
                                instructions i ON i.part_number = msih.part_number
                                AND i.instr_order = msih.instr_order
                                AND i.station_id = msih.station_id
                                AND i.part_order = msih.part_order
                            LEFT JOIN
                                test_types tt ON tt.ID = i.test_type
                            LEFT JOIN
                                line_stations ls ON ls.station_id = msih.station_id
                        WHERE
                            msih.module_ID = {$request['module_ID']}
                            AND ls.ID = {$station['real_station_ID']}
                        ORDER BY
                                ls.station_id,
                                msih.instr_order;";

                $station['station_instruction_history'] = $db->query($sql);
            }
        }
        
        echo json_encode($result);
    }

    function get_rework_station_history ($request, $db) {

        $sql = "SELECT
                    ls.ID AS real_station_ID,
                    ls.station_ID,
                    ls.station_desc,
                    max(mdrih.complete_time) AS complete_time,
                    mdrih.operator_ID,
                    o.name AS operator_name
                FROM
                    module_defect_repair_instruction_history mdrih
                    LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                    LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                    LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                WHERE
                    mdrih.module_ID = {$request['module_ID']}
                GROUP BY
                    ls.ID,
                    ls.station_ID,
                    ls.station_desc,
                    mdrih.operator_ID,
                    o.name
                ORDER BY
                    ls.station_ID ASC;";

        $result = $db->query($sql);

        if ($result) {
            foreach ($result as $i => &$station) {
                $sql = "SELECT
                            ls.station_ID,
                            ls.station_desc,
                            i.instr_order,
                            tt.type_desc,
                            mdrih.complete_time,
                            mdrih.completion_details,
                            o.name AS operator_name,
                            mdrih.operator_ID
                        FROM
                            module_defect_repair_instruction_history mdrih
                            LEFT JOIN instructions i ON i.ID = mdrih.instr_ID
                            LEFT JOIN operators o ON o.ID = mdrih.operator_ID
                            LEFT JOIN test_types tt ON tt.ID = i.test_type
                            LEFT JOIN line_stations ls ON ls.ID = i.station_ID AND ls.line_id = i.line_ID
                        WHERE
                            mdrih.module_ID = {$request['module_ID']}
                            AND ls.ID = {$station['real_station_ID']}
                        ORDER BY
                            i.station_ID ASC,
                            i.instr_order ASC,
                            mdrih.complete_time ASC;";

                $station['rework_instruction_history'] = $db->query($sql);
            }
        }
        
        echo json_encode($result);
    }

    function get_sub_assembly_history ($request, $db) {

        //get wip parts
        $sql = "SELECT
                    *
                FROM (
                    (
                        -- sub assemblies
                        SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code,
                            wp.line_ID
                        FROM
                            wip_parts wp
                            LEFT JOIN wip_part_setup wps ON wps.ID = wp.part_ID
                            LEFT JOIN lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_module_ID = {$request['module_ID']}
                            AND wp.used_time IS NOT NULL
                            AND l.line_type IN (1, 2)
                    ) UNION (
                        -- small part kitting
                        SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code,
                            wp.line_ID
                        FROM
                            wip_parts wp
                            LEFT JOIN wip_part_setup wps ON wps.ID = wp.part_ID
                            LEFT JOIN lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_module_ID = {$request['module_ID']}
                            AND wp.used_by_wip_part_ID IS NULL
                            AND l.line_type = 5
                    )
                ) AS subs
                ORDER BY
                    line_code ASC;";

        $wip_parts = $db->query($sql);

        if ($wip_parts) {
            // get wip station history
            foreach($wip_parts as $i => &$part) {
                //get wip parts wip parts
                $sql = "SELECT
                            wp.id,
                            wp.internal_serial,
                            wp.built_time,
                            wps.part_desc,
                            l.line_code
                        FROM
                            wip_parts wp
                            LEFT JOIN wip_part_setup wps ON wps.ID = wp.part_ID
                            LEFT JOIN lines l ON l.ID = wp.line_ID
                        WHERE
                            wp.used_by_wip_part_ID = {$part['id']}
                            AND wp.used_time IS NOT NULL
                            AND l.line_type IN (1, 2);";

                $part['wip_parts'] = $db->query($sql);

                $sql = "SELECT
                            ls.station_desc,
                            wpsh.entry_time,
                            wpsh.exit_time,
                            op.name AS operator_name,
                            wpsh.station_ID
                        FROM
                            wip_part_station_history wpsh
                            LEFT JOIN operators op ON op.id = wpsh.operator_id
                            LEFT JOIN (SELECT DISTINCT station_ID, station_desc FROM line_stations) ls ON ls.station_id = wpsh.station_id
                        WHERE
                            wip_part_id = {$part['id']};";

                $part['station_history'] = (array) $db->query($sql);
                //die(json_encode($part));

                if ($part['station_history']) {
                    // get wip instruction history
                    foreach($part['station_history'] as $j => &$station) {
                        $sql = "SELECT DISTINCT
                                    wpsih.ID,
                                    ls.station_id,
                                    wpsih.step_order,
                                    tt.type_desc,
                                    wpsih.entry_time,
                                    wpsih.exit_time,
                                    wpsih.completion_details
                                FROM
                                    wip_part_station_instruction_history wpsih
                                    LEFT JOIN
                                        instructions i ON i.part_number = wpsih.part_number
                                        AND i.instr_order = wpsih.step_order
                                        AND i.station_id = wpsih.station_id
                                        AND i.part_order = wpsih.part_order
                                    LEFT JOIN
                                        test_types tt ON tt.ID = i.test_type
                                    LEFT JOIN
                                        (SELECT DISTINCT station_ID FROM line_stations) AS ls ON ls.station_id = wpsih.station_id
                                WHERE
                                    wpsih.wip_part_ID = {$part['id']}
                                    AND wpsih.station_id = {$station['station_ID']}
                                ORDER BY wpsih.step_order;";

                        $station['instruction_history'] = $db->query($sql);
                    }
                }

                $sql = "SELECT TOP 1 * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'wip_part_process_data'";
                $table_data = $db->query($sql);
                if ($table_data) {
                    // get process data
                    $sql = "SELECT
                                wppd.ID,
                                wppd.process_location,
                                REPLACE(REPLACE(SUBSTRING(wppd.process_tag, CHARINDEX('.', wppd.process_tag), LEN(wppd.process_tag)), '.', ''), '_', ' ') AS process_desc,
                                wppd.process_value_desc,
                                wppd.process_value,
                                wppd.process_timestamp,
                                dbo.fn_getJSONStringValue(ls.station_options, 'instance_desc') AS instance_desc
                            FROM
                                wip_part_process_data wppd
                                LEFT JOIN line_stations ls ON dbo.fn_getJSONIntegerValue(ls.station_options, 'station_instance') = CAST(RIGHT(wppd.process_location, LEN(wppd.process_location) - 5) AS INT)
                            WHERE
                                wppd.wip_part_ID = {$part['id']}
                            ORDER BY
                                wppd.process_location ASC,
                                wppd.ID ASC;";

                    $part['process_data'] = $db->query($sql);
                } else {
                    $part['process_data'] = false;
                }


                if ($part['line_ID'] == 160) {
                    // get poly/iso scans
                    $sql = "SELECT
                                completion_details AS poly_scan
                            FROM
                                wip_part_station_instruction_history 
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 25;";
                    $polys = $db->query($sql);
                    $part['poly_scan'] = $polys[0]['poly_scan'];

                    $sql = "SELECT
                                completion_details AS iso_scan
                            FROM
                                wip_part_station_instruction_history 
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 26;";
                    $isos = $db->query($sql);
                    $part['iso_scan'] = $isos[0]['iso_scan'];

                    // get airbag serial
                    $sql = "SELECT
                                CAST(SUBSTRING(completion_details, 6, LEN(completion_details)) AS INT) AS serial
                            FROM
                                NYSUS_SEQUENCE_MES.dbo.wip_part_station_instruction_history
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160010
                                AND part_order = 10
                                AND step_order = 20;";
                    $airbag_serial = $db->query($sql);
                    if (!$airbag_serial) {
                        $airbag_serial = array(0 => array('serial' => -1));
                    }

                    // get skin serial
                    $sql = "SELECT
                                CAST(SUBSTRING(completion_details, 6, LEN(completion_details)) AS INT) AS serial
                            FROM
                                NYSUS_SEQUENCE_MES.dbo.wip_part_station_instruction_history
                            WHERE
                                wip_part_ID = {$part['id']}
                                AND station_ID = 160030
                                AND part_order = 10
                                AND step_order = 30;";
                    //die($sql);
                    $skin_serial = $db->query($sql);
                    if (!$skin_serial) {
                        $skin_serial = array(0 => array('serial' => -1));
                    }

                    // get airbag and skin
                    $sql = "SELECT
                                wp.ID,
                                created_at AS built_time,
                                wp.internal_serial,
                                ps.cell_ID AS line_ID,
                                c.cell_desc AS line_code,
                                ps.part_desc
                            FROM
                                NYSUS.dbo.MESAAS_wip_parts wp
                                LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = wp.part_ID
                                LEFT JOIN NYSUS.dbo.cells c ON c.ID = ps.cell_ID
                            WHERE
                                wp.ID = {$airbag_serial[0]['serial']}
                                OR wp.ID = {$skin_serial[0]['serial']};";
                    //die($sql);
                    $part['batch_parts'] = $db->query($sql);
                }
            }
        }
        
        echo json_encode($wip_parts);
    }

    function get_batch_assembly_history ($request, $db) {
        // get part
        $sql = "SELECT
                    wp.ID,
                    created_at AS built_time,
                    wp.internal_serial,
                    ps.cell_ID AS line_ID,
                    c.cell_desc AS line_code,
                    ps.part_desc
                FROM
                    NYSUS.dbo.MESAAS_wip_parts wp
                    LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = wp.part_ID
                    LEFT JOIN NYSUS.dbo.cells c ON c.ID = ps.cell_ID
                WHERE
                    wp.ID = {$request['wip_part_ID']};";
        $p = $db->query($sql);

        if ($p) {
            $part = $p[0];

            // get batch wip station history
            $sql = "SELECT
                        wpsh.wip_part_ID,
                        s.station_desc,
                        NULL AS entry_time,
                        NULL AS exit_time,
                        op.name AS operator_name,
                        wpsh.station_order,
                        ps.cell_ID
                    FROM
                        NYSUS.dbo.MESAAS_wip_parts w
                        LEFT JOIN (SELECT DISTINCT wip_part_ID, station_order, operator_ID FROM NYSUS.dbo.MESAAS_wip_part_step_history) wpsh ON wpsh.wip_part_ID = w.ID
                        JOIN MES_COMMON.dbo.operators op ON op.id = wpsh.operator_id
                        JOIN NYSUS.dbo.part_setup ps ON ps.ID = w.part_ID
                        LEFT JOIN NYSUS.dbo.stations s ON s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                    WHERE
                        wpsh.wip_part_ID = {$part['ID']}
                    GROUP BY
                        wpsh.wip_part_ID,
                        s.station_desc,
                        op.name,
                        wpsh.station_order,
                        ps.cell_ID
                    ORDER BY
                        wpsh.station_order ASC;";
            //die($sql);

            $part['station_history'] = $db->query($sql);

            if ($part['station_history']) {
                foreach($part['station_history'] as $j => &$station) {
                    // get wip instruction history
                    $sql = "SELECT
                                wpsh.ID,
                                wpsh.start_time AS entry_time,
                                wpsh.complete_time AS exit_time,
                                op.name AS operator_name,
                                wpsh.instruction_step_order,
                                wpsh.completion_details,
                                tt.type_desc,
                                i.text AS instruction_text
                            FROM
                                NYSUS.dbo.MESAAS_wip_part_step_history wpsh
                                LEFT JOIN NYSUS.dbo.MESAAS_wip_parts w ON w.ID = wpsh.wip_part_ID
                                LEFT JOIN NYSUS.dbo.part_setup ps ON ps.ID = w.part_ID
                                LEFT JOIN NYSUS.dbo.stations s ON  s.cell_ID = ps.cell_ID AND s.station_order = wpsh.station_order
                                LEFT JOIN MES_COMMON.dbo.operators op ON op.ID = wpsh.operator_ID
                                LEFT JOIN NYSUS.dbo.instructions i ON
                                    i.cell_ID = ps.cell_ID
                                    AND i.station_order = wpsh.station_order
                                    AND i.instr_order = wpsh.instruction_step_order
                                    AND i.part_ID = ps.ID
                                LEFT JOIN NYSUS.dbo.test_types tt ON tt.ID = i.test_type
                            WHERE
                                wpsh.wip_part_ID = {$part['ID']}
                                AND s.station_order = {$station['station_order']}
                            ORDER BY
                                wpsh.instruction_step_order ASC;";

                    $station['instruction_history'] = $db->query($sql);
                }
            }
        }

        
        echo json_encode($part);
    }

    function get_eol_test_data ($request, $db) {

        // new EOL
        $sql = "SELECT
                    t.ID,
                    t.start_time,
                    MIN(t.result) AS result
                FROM (
                    SELECT
                        th.ID,
                        td.test_type,
                        td.test_item,
                        th.start_time,
                        MAX(CAST(td.passed AS TINYINT)) AS result
                    FROM
                        EOL_test_history th
                        LEFT JOIN EOL_test_history_detail td ON td.test_history_ID = th.ID
                    WHERE
                        th.module_ID = {$request['module_ID']}
                    GROUP BY
                        th.ID,
                        th.start_time,
                        td.test_type,
                        td.test_item
                ) AS t
                GROUP BY
                    t.ID,
                    t.start_time
                ORDER BY
                    t.start_time ASC;";
        
        $result = $db->query($sql);

        if ($result) {
            // EOL test detail
            foreach($result as $k => &$test) {
                $sql = "SELECT
                            ID,
                            test_item,
                            min_float_val,
                            max_float_val,
                            float_val,
                            passed
                        FROM
                            EOL_test_history_detail
                        WHERE
                            test_history_ID = {$test['ID']}
                        ORDER BY
                            ID;";
                
                $test['detail'] = $db->query($sql);
            }
        } else {
            //Old EOL Info
            $sql = "SELECT
                        th.ID,
                        th.start_time,
                        th.end_time
                    FROM
                        NYSUS_EOL_TEST.test_history th
                    WHERE
                        th.module_ID = {$request['module_ID']}";

            $eol_info = $db->query($sql);

            //EOL Test History Info
            $sql = "SELECT * FROM (
                        (
                            SELECT
                                th.ID,
                                th.start_time,
                                th.end_time,
                                th.result
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history th
                                LEFT JOIN NYSUS_SEQUENCE_MES.dbo.module_station_instruction_history msih ON th.ID = CAST(REPLACE(msih.completion_details, 'EOL PASSED: ', '') AS INT)
                            WHERE
                                msih.module_ID = {$request['module_ID']}
                                AND msih.completion_details LIKE 'EOL PASSED: %'
                        ) UNION (
                            SELECT
                                th.ID,
                                th.start_time,
                                th.end_time,
                                th.result
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history th
                                LEFT JOIN NYSUS_SEQUENCE_MES.dbo.module_defect_repair_instruction_history mdrih ON th.ID = CAST(REPLACE(mdrih.completion_details, 'EOL PASSED: ', '') AS INT)
                            WHERE
                                mdrih.module_ID = {$request['module_ID']}
                                AND mdrih.completion_details LIKE 'EOL PASSED: %'
                        )
                    ) AS th";
            $result = $db->query($sql);

            if ($result) {
                // EOL test detail
                foreach($result as $k => &$test) {
                    $sql = "SELECT
                                thd.ID,
                                tops.test_description AS test_item,
                                thd.resistance_min AS min_float_val,
                                thd.resistance_max AS max_float_val,
                                thd.m_resistance AS float_val,
                                thd.test_result AS passed
                            FROM
                                NYSUS_EOL_TEST.dbo.test_history_detail thd
                            LEFT JOIN
                                NYSUS_EOL_TEST.dbo.test_options tops ON tops.ID = thd.test_option_ID
                            WHERE
                                thd.test_history_ID = {$test['ID']}
                            ORDER BY
                                thd.channel_num;";
                    $test['detail'] = $db->query($sql);
                }
            }
        }
        
        echo json_encode($result);
    }

    function get_images ($request, $db) {
        $result = new StdClass();

        $sql = "SELECT sequence, VIN, built_time, line_ID FROM modules WHERE ID = {$request['module_ID']};";
        $modules = $db->query($sql);
        if ($modules) {
            $module = $modules[0];
        } else {
            $module = $modules;
        }

        if ($request['mrp_company_code'] == '539IP') {
        //if ($_REQUEST['mrp_company_code'] == '539IP') {
            $date_parts = explode('-', $module['built_time']->format('Y-m-d'));

            $result->main_line_images = array();
            $dir_path = "/539IP/archive/{$date_parts[0]}/MONTH{$date_parts[1]}/DAY{$date_parts[2]}/MODULE_{$request['module_ID']}/";
            for($i=1; $i<=4; $i++) {
                $file_name = $module['sequence'] . '_' . $i . '.jpg';
                array_push($result->main_line_images, $dir_path . $file_name);
            }

            //chute images
            $result->chute_images = array();
            $real_path = "D:\\539_vision\\Archive\\{$date_parts[0]}\\MONTH{$date_parts[1]}\\DAY{$date_parts[2]}\\MODULE_{$request['module_ID']}";
            $real_path_2 = "E:\\539IP\\archive\\{$date_parts[0]}\\MONTH{$date_parts[1]}\\DAY{$date_parts[2]}\\MODULE_{$request['module_ID']}";
            $dir_path = "/539_vision_images/Archive/{$date_parts[0]}/MONTH{$date_parts[1]}/DAY{$date_parts[2]}/MODULE_{$request['module_ID']}/";

            
            if ($module['line_ID'] == 400) {
                // 539X RHD images
                $image_names = array(
                    '539xChute' => '',
                    '539xFront' => '', 
                    '539xRear' => ''
                );

                if (is_dir($real_path) || is_dir($real_path_2)) {
                    if (is_dir($real_path)) {
                        $rp = $real_path;
                    } else {
                        $rp = $real_path_2;
                    }
                    $files = array_diff(scandir($rp), array('.', '..'));
                    
                    foreach($files as $i => $file_name) {
                        foreach($image_names as $type => &$max) {
                            $matches = array();
                            preg_match('/^'.$type.'[0-9]+\.bmp$/', $file_name, $matches);
                            if (!empty($matches) && $matches[0] > $max) {
                                $max = $matches[0];     
                            }
                        }
                    }

                    // no chute images for 540's
                    if (substr($module['VIN'], 0, 2) == '2L') {
                        $result->chute_images = null;
                    } else {
                        foreach($image_names as $type => $name) {
                            array_push($result->chute_images, $dir_path . $name);
                        }
                    }
                }
            } else {
                // 539 / 540 / 539X LHD images
                $image_names = array(
                    '539Chute' => '',
                    '539Front' => '', 
                    '539Rear' => ''
                );

                if (is_dir($real_path) || is_dir($real_path_2)) {
                    if (is_dir($real_path)) {
                        $rp = $real_path;
                    } else {
                        $rp = $real_path_2;
                    }
                    $files = array_diff(scandir($rp), array('.', '..'));
                    
                    foreach($files as $i => $file_name) {
                        foreach($image_names as $type => &$max) {
                            $matches = array();
                            preg_match('/^'.$type.'[0-9]+\.jpg$/', $file_name, $matches);
                            if (!empty($matches) && $matches[0] > $max) {
                                $max = $matches[0];     
                            }
                        }
                    }

                    // no chute images for 540's
                    if (substr($module['VIN'], 0, 2) == '2L') {
                        $result->chute_images = null;
                    } else {
                        foreach($image_names as $type => $name) {
                            array_push($result->chute_images, $dir_path . $name);
                        }
                    }
                }
            }
        }
        
        echo json_encode($result);
    }

	function get_module_station_torque_values ($request, $db) {
		$sql = "SELECT
					tv.ID,
					tv.station_ID,
					ls.station_desc,
					tv.part_order,
					tv.part_number,
					tv.instr_order,
					tv.torque_time,
					tv.gun_number,
					tv.torque_value,
					tv.angle_value,
					tv.passed,
					tv.cleared,
					tv.attempt_number
				FROM
					module_station_torque_values tv
					LEFT JOIN line_stations ls ON ls.ID = tv.station_ID
				WHERE
					tv.module_ID = {$request['module_ID']}
				ORDER BY
					ls.station_order,
					tv.part_order,
					tv.instr_order,
					tv.torque_time;";
		$result = $db->query($sql);
		
		echo json_encode($result);
	}
	
	function get_module_stored_plc_values ($request, $db) {
		$sql = "SELECT
					pv.ID,
					pv.station_ID,
					ls.station_desc,
					pv.the_time,
					pv.attempt_number,
					pv.test_status,
					pv.tag_desc,
					pv.tag_value
				FROM
					module_stored_plc_values pv
					LEFT JOIN line_stations ls ON ls.ID = pv.station_ID
				WHERE
					pv.module_ID = {$request['module_ID']}
				ORDER BY
					ls.station_order,
					pv.the_time,
					pv.tag_desc;";
		$result = $db->query($sql);
		
		echo json_encode($result);
	}
	
?>