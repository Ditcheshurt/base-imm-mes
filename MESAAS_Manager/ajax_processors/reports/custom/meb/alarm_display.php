<?php

	require_once("../../../init.php");

	$action = $_REQUEST['action'];

	if ($action == "get_alarms") {	
		global $db;
		$res = new StdClass();
		$r = $_REQUEST;
		
		// alarms
		$sql = "select top 10
				datediff(minute, mc.cycle_time, getdate()) as 'cycle_time',
				mcp.serial_calc,
				mcp.disposition,
				dps.data_point_desc,
				concat(mcdp.data_point_value,' ',dps.units) as 'value',
				concat(mtcl.lower_ctrl_limit,' - ',mtcl.upper_ctrl_limit,' ',dps.units) as 'range'
				from
				machine_monitor_alerts mma
				join machine_cycle_data_points mcdp on mcdp.id = mma.machine_cycle_datapoint_ID
				join data_point_setup dps on dps.id = mcdp.data_point_ID
				join machine_cycles mc on mc.id = mcdp.cycle_ID
				join machines m on m.id = mc.machine_ID
				join machine_cycle_parts mcp on mcp.cycle_ID = mc.ID
				join machine_tool_ctrl_limits mtcl ON mtcl.data_point_ID = mcdp.data_point_ID AND mtcl.machine_ID = mc.machine_ID AND mtcl.tool_ID = mc.tool_ID AND mtcl.deactivated = 0
				where
				m.id = '".$r['machine_id']."'
				and mma.limit_triggered = 1
				order by mc.id desc, serial_calc desc";
				
		$res->alarms = $db->query($sql);
		
		echo json_encode($res);
	}
	
//	if ($action == "get_module_detail") {
//		global $db;
//		$res = new StdClass();
//		$r = $_REQUEST;
//		
//		$sql = "SELECT wp.ID, wp.part_ID,
//					CASE WHEN wps.part_desc IS NULL THEN '' ELSE wps.part_desc END AS part_desc,
//					wp.internal_serial,
//		            wp.line_ID, wp.used_by_wip_part_ID, m.build_order, k.container_id
//				FROM dbo.wip_parts wp
//				LEFT JOIN
//					wip_part_setup wps ON wps.id = wp.part_ID
//				JOIN modules m on m.id = wp.used_by_module_id
//				JOIN kit_container_history k on k.mod_id = m.id
//				WHERE
//					used_by_module_id = ".$r['module_id'];
//					
//		$res->module_detail = $db->query($sql);
//		
//		echo json_encode($res);
//	}

?>