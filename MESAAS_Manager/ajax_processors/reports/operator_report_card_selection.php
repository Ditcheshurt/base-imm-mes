<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];

    call_user_func($action, $_REQUEST, $db);


    function get_operators ($request, $db) {
        $sql = "SELECT
                    o.ID,
                    o.first_name,
                    o.last_name,
                    o.badge_ID
                FROM
                    MES_COMMON.dbo.operators o
                ORDER BY
                    o.name;";
        
        echo json_encode($db->query($sql));
    }

?>