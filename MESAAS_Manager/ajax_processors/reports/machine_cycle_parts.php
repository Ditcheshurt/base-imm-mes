<?php

	require_once("../init.php");

	$action = $_REQUEST['action'];

	call_user_func($action, $_REQUEST, $db);
	
	function get_parts($request, $db) {
		$sql = "SELECT ID, part_number, part_desc FROM parts ORDER BY part_number ASC;";
		echo json_encode($db->query($sql));
	}

	function get_machine_cycle_parts ($request, $db) {
		$system_ID = $_REQUEST["system_ID"];

		if (array_key_exists('status', $request) && ($request['status'] === null || $request['status'] < 0)) {
			$status = '%';
		} else {
			$status = $request['status'];
		}

		if (array_key_exists('part_ID', $request) && ($request['part_ID'] === null || $request['part_ID'] < 0)) {
			$part_ID = '%';
		} else {
			$part_ID = $request['part_ID'];
		}

		$sql = "SELECT
					mcp.serial_number,
					p.part_number,
					p.part_desc,
					MIN(mcph.cycle_start_time) AS start_time,
					MAX(mcph2.cycle_complete_time) AS complete_time
				FROM
					machine_cycle_parts mcp
					LEFT JOIN tool_parts tp ON tp.ID = mcp.tool_part_ID
					LEFT JOIN parts p ON p.ID = tp.part_ID
					LEFT JOIN machine_cycle_part_history mcph ON mcph.serial = mcp.serial_number
					LEFT JOIN machine_cycle_part_history mcph2 ON mcph2.serial = mcp.serial_number
						AND mcph2.machine_ID = (
							SELECT TOP 1 ID FROM machines ORDER BY op_order DESC
						)
					LEFT JOIN machine_cycles mc ON mc.ID = mcp.cycle_ID
					LEFT JOIN machines m ON m.ID = mc.machine_ID
				WHERE
					p.ID LIKE '{$part_ID}'
					AND m.system_ID = $system_ID
				GROUP BY
					mcp.serial_number,
					p.part_number,
					p.part_desc
				HAVING
					(
						MIN(mcph.cycle_start_time) IS NULL OR (
							MIN(mcph.cycle_start_time) >= '{$request['start_date']} 00:00:00'
							AND MIN(mcph.cycle_start_time) <= '{$request['end_date']} 23:59:59.99'
						)
					) AND (
						CASE
							WHEN MIN(mcph.cycle_start_time) IS NOT NULL AND MAX(mcph2.cycle_complete_time) IS NULL THEN 1
							WHEN MIN(mcph.cycle_start_time) IS NOT NULL AND MAX(mcph2.cycle_complete_time) IS NOT NULL THEN 2
							ELSE 0
						END LIKE '$status'
					)";
		//die($sql);
		$result = $db->query($sql);
		if ($result === null) {
			$result = array();
		}
		echo json_encode($result);
	}

// if ($action == 'list_wip_parts') {
// 	global $db;
// 	$res = new StdClass();

// 	if ($_REQUEST['process_type'] == 'BATCH') {
// 		$sql = "SELECT
// 					w.ID,
// 					NULL AS build_order,
// 					NULL AS VIN,
// 					NULL AS station_desc,
// 					NULL AS sequence,
// 					ISNULL(w.internal_serial, w.external_serial) as serial_number,
// 					c.cell_desc AS line_code,
// 		            NULL AS built_time,
// 		            NULL AS last_completed_station_ID,
// 		            MAX(wpsh.station_order) AS last_completed_station_ID,
// 		            NULL AS last_completed_station_desc
// 				FROM MESAAS_wip_parts w
// 					JOIN part_setup ps ON ps.ID = w.part_ID
// 					JOIN cells c ON c.ID = ps.cell_ID
// 					--JOIN MESAAS_wip_part_step_history wpsh ON wpsh.wip_part_ID = w.ID
// 					JOIN (
// 						SELECT
// 							*
// 						FROM
// 							MESAAS_wip_part_step_history wpsh
// 						WHERE
// 							wpsh.complete_time >= '".$_REQUEST['start_date']." 00:00:00'
// 							AND wpsh.complete_time <= '".$_REQUEST['end_date']." 23:59:59'
// 					) AS wpsh ON wpsh.wip_part_ID = w.ID
// 				WHERE
// 					c.ID = ".$_REQUEST['line_id']."
// 				GROUP BY
// 					w.ID,
// 					w.internal_serial,
// 					w.external_serial,
// 					c.cell_desc,
// 					c.ID;";

// 	} else {
// 		$status = $_REQUEST['status'];
// 		$status_filter = "";
// 		if ($status == "-1") {
// 			$status_filter = "AND ((
// 	                w.built_time >= '".$_REQUEST['start_date']." 00:00:00'
// 	                AND w.built_time <= '".$_REQUEST['end_date']." 23:59:59'
// 	            ) OR w.built_time IS NULL)";
// 		} else if ($status == "0") {
// 		    $status_filter = "AND w.built_time IS NULL";
// 		} else if ($status == "1") {
// 			$status_filter = "AND (
// 	                w.built_time >= '".$_REQUEST['start_date']." 00:00:00'
// 	                AND w.built_time <= '".$_REQUEST['end_date']." 23:59:59'
// 	            )";
// 		}


// 		$sql = "SELECT
// 		            w.ID,
// 		            m.build_order,
// 		            m.VIN,
// 		            ls.station_desc,
// 		            m.sequence,
// 		            ISNULL(w.internal_serial, w.external_serial) as serial_number,
// 		            w.built_time,
// 		            w.last_completed_station_ID,
// 		            ls.station_desc AS last_completed_station_desc,
// 		            l.line_code
// 		        FROM wip_parts w
// 		            LEFT JOIN modules m ON w.used_by_module_ID = m.ID
// 		            LEFT JOIN line_stations ls ON ls.id = w.last_completed_station_ID
// 		            LEFT JOIN lines l ON l.id = ls.line_id
// 		        WHERE
// 		            w.line_ID = ".$_REQUEST['line_id']." ".$status_filter.";";		
// 	}

// 	$res = $db->query($sql);
// 	if (!$res) {
// 		$res = array();
// 	}
// 	echo json_encode($res);
// }


?>