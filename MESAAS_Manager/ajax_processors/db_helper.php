<?php
	set_error_handler(create_function('$severity, $message, $file, $line', 'echo "{\"error\":\"$message\"}";exit(1);'));
	
    header("Content-Type: application/json");

	error_reporting(E_ALL);
	ini_set('display_errors', 'On');	

	$GLOBALS['APP_CONFIG'] = json_decode(file_get_contents(dirname(dirname(__FILE__)).'\config\app_config.json'), true);

	require($_SERVER['DOCUMENT_ROOT'].$GLOBALS['APP_CONFIG']['db_class']);

	session_save_path(dirname(__DIR__).$GLOBALS['APP_CONFIG']['session_path']);
	session_start();

	function fixDB($s) {
	    $s = str_replace("'", "''", $s);
		return $s;
	}
	
	class dbhelper extends db {
		private $res;
		public $sql = [];
		public $err_msg = [];
		

		function __construct(&$sql, &$em) {
			$this->sql =& $sql;
			$this->err_msg =& $em;
			$this->res = new StdClass();
			
			if (!array_key_exists('data', $this->res)) {
				$this->res->data = [];
			}
			if (!array_key_exists('errors', $this->res)) {
				$this->res->errors = [];
			}
			if (!array_key_exists('general', $this->res)) {
				$this->res->general = [];
				$this->res->general['return_code'] = 0;
			}

			foreach($_REQUEST as $k => $v) {
				$t = explode(';',$v);
				if (count($t) == 1) {
					$var = $k;
					$GLOBALS[$var] = fixDB($t[0]);
				}
			}
			
			if (isset($_REQUEST['debug'])) {
				$this->debug = (strtoupper($_REQUEST['debug'])=='TRUE'?true:false);
			} else {
				$this->debug = false;
			}
			
			$this->server = $GLOBALS['APP_CONFIG']['server'];
			$this->catalog = $GLOBALS['APP_CONFIG']['catalog'];
			$this->username = $GLOBALS['APP_CONFIG']['username'];
			$this->password = $GLOBALS['APP_CONFIG']['password'];
			$this->testactive = $GLOBALS['APP_CONFIG']['testactive'];
			if (isset($database)) {
				$this->catalog = $database;
			}
			$this->connect();
		}

		function processSqlRequest() {
			$ec = 0;
			for ($i = 0; $i<count($this->sql); $i++) {
				$this->res->data[$i] = [];
				$this->res->data[$i] = $this->query($this->sql[$i]);
				switch(true) {
					case is_bool($this->res->data[$i]): // bad request
						if ($this->res->data[$i]) {
							// not sure what to do with this yet
						} else {
							$ec = -2;
							$this->res->errors[$i] = [];
							$this->res->errors[$i]['error_msg'] = $this->err_msg[$i];
						}
						break;
				}
			}
			$this->res->general['return_code'] = $ec;
		}
		
		function missingActionHandler() {
			$this->res->general['return_code'] = -1;
		}
		
		function rtnResults() {
			$msg = '';
			switch($this->res->general['return_code']) {
				case -1:
					$msg = "No handler defined for action: '{$action}'";
					break;
				case -2:
					$msg = 'A query failed result validation';
					break;
				default:
					 $msg = 'Success';
			}
			$this->res->general['return_msg'] = $msg;
			if(($this->debug || $this->res->general['return_code'] < 0) && isset($this->sql)){
				$this->res->query = $this->sql;
			}
			echo json_encode($this->res);
		}
	}
?>