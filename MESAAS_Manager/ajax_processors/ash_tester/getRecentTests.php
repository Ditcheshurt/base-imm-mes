<?php
	set_error_handler(
		create_function(
			'$severity, $message, $file, $line', 'throw new errorexception($message, $severity, $severity, $file, $line);'
		)
    );
	
	$ash_tester_IP = '10.49.105.124';
	$num_recs = 15;

	try {
		$ctx = stream_context_create(array('http'=>array('timeout'=>5)));
		$csv_str = file_get_contents("http://{$ash_tester_IP}/testdata.csv?RowFirst=0&RowLast={$num_recs}",false,$ctx);
		
		$csv_str = mb_convert_encoding($csv_str, 'UTF-8');
		$csv_str = strtolower($csv_str);

		$patterns = array();$replacements = array();
		$patterns[0] = '/\ \+\/-\ /';$replacements[0] = '+-';
		$patterns[2] = '/\ /';$replacements[2] = '_';
		$patterns[3] = '/\//';$replacements[3] = ' per ';
		$patterns[4] = '/\?/';$replacements[4] = 'deg';
		$csv_str = preg_replace($patterns,$replacements,$csv_str);
		$csv_str = str_replace('degc','degC',$csv_str);
				
		$csv_arr = explode("\n", $csv_str);
		unset($csv_arr[count($csv_arr)-1]);
		$csv = array_map('str_getcsv',$csv_arr);
		array_walk($csv, function(&$a) use ($csv) {
		  $a = array_combine($csv[0], $a);
		});
		array_shift($csv); # remove column header
		
		for ($i = 0; $i < count($csv); $i++) {
			foreach ($csv[$i] as $k => $v) {
				$key = preg_replace('/\ /','_',$k);
				$val = preg_replace('/\ /','_',$v);
				$test[$i][$key] = $val;
			}
		}
		
		echo json_encode($test);
	} catch (Exception $e) {
		echo json_encode(array('success'=>false,'err_code'=>$e->getCode(), 'err_msg'=>$e->getFile() . ' on line ' . $e->getLine()));
	}
?>