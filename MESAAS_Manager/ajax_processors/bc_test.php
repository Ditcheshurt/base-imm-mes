<?php

    require_once("../init.php");

    $action = $_REQUEST['action'];
    
    if ($action == 'load_data') {
        global $db;
        $res = new StdClass();
        $r = $_REQUEST;
        
        $module_ID = $r['module_ID'];       
        
        //module data
        // $sql = "SELECT
        //          m.sequence,
        //          m.VIN,
        //          m.line_ID,
        //          o.name,
        //          m.built_time,
        //          case when m.cart_ID is null then convert(varchar, 'No cart data') else convert(varchar, m.cart_ID) end as cart_ID
        //      FROM
        //          modules m
        //          LEFT OUTER JOIN MES_COMMON.dbo.operators o ON o.ID = m.operator_id
        //      WHERE
        //          m.ID = ".$request['module_ID'];
        
        $sql = "SELECT
                    m.sequence,
                    m.VIN,
                    m.line_ID,
                    o.name AS operator_name,
                    m.built_time
                FROM
                    modules m
                    LEFT OUTER JOIN
                        MES_COMMON.dbo.operators o ON o.ID = m.operator_id
                WHERE
                    m.ID = ".$module_ID;

        $res->module_info = $db->query($sql);
        //die(var_dump($res));
        $vin = $res->module_info[0]['VIN'];
        
        //broadcast info
        $sql = "SELECT
                    l.line_code,
                    m.sequence,
                    m.loaded_time,
                    l.preview_status,
                    l.build_status,
                    l.info_status
                FROM
                    modules m
                    LEFT OUTER JOIN lines l ON l.ID = m.line_ID
                WHERE
                    m.ID = ".$module_ID;
        $res->broadcast_info = $db->query($sql);
        
        $mod_line_ID = $res->module_info[0]['line_ID'];
        


        // //service message info
        // $sql = "SELECT
        //             Substring(raw_data, 11, 8) as VIN,
        //             Substring(raw_data, 24, 11) as Sequence,
        //             Sent_Time, raw_data As [Parts Changed]
        //         FROM
        //             broadcasts_out b
        //             JOIN lines l ON SUBSTRING(b.queue, 4, 7) = SUBSTRING(l.queue_in, 10, 7) AND l.ID = " . $mod_line_ID . "
        //         WHERE
        //             SUBSTRING(b.raw_data, 11, 8) = '".substr($vin, -8)."'
        //             and msg_type = 'SR'";

        // $q = $db->query($sql);
        // $res->sr_message_info = $q[0];


        // station info
        $sql = "SELECT
                    ls.station_order,
                    ls.station_desc,
                    o.name AS operator_name,
                    msh.entry_time,
                    msh.exit_time,
                    DATEDIFF(SECOND, msh.entry_time, msh.exit_time) AS cycle_time
                FROM
                    module_station_history msh
                    JOIN line_stations ls ON msh.station_ID = ls.station_ID
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msh.module_ID = ".$module_ID."
                ORDER BY ls.station_order ASC;";

        $res->station_info = $db->query($sql);


        // pick info
        $sql = "SELECT
                    prm.pocket,
                    prm.status AS pick_rack_module_status,
                    prm.serial,
                    pr.start_time AS pick_time,
                    pr.status AS pick_rack_status
                FROM
                    modules m
                    LEFT OUTER JOIN pick_rack_modules prm ON prm.module_ID = m.ID
                    LEFT OUTER JOIN pick_racks pr ON pr.ID = prm.pick_rack_ID
                WHERE
                    prm.module_ID = ".$module_ID;

        $res->pick_info = $db->query($sql);


        // sub-assembly
        // WIP/Kit Info
        $sql = "SELECT
                    --l.line_code,
                    ls.station_desc,
                    wp.internal_serial,
                    wp.external_serial,
                    o.name AS operator_name,
                    wpsh.entry_time,
                    wpsh.exit_time
                FROM modules m
                    LEFT OUTER JOIN kit_container_history kch ON kch.mod_ID = m.ID
                    LEFT OUTER JOIN lines l ON l.ID = kch.line_ID
                    LEFT OUTER JOIN line_stations ls ON ls.line_id = l.ID
                    JOIN wip_parts wp ON wp.used_by_module_ID = m.ID
                    JOIN wip_part_station_history wpsh ON wpsh.wip_part_ID = wp.ID and wpsh.line_id = l.id
                    JOIN MES_COMMON.dbo.operators o ON o.ID = wpsh.operator_ID
                WHERE
                    wp.used_by_module_ID = ".$module_ID."
                UNION 
                SELECT
                    ls.station_desc,
                    wp.internal_serial,
                    wp.external_serial,
                    o.name AS operator_name,
                    wpsh.entry_time,
                    wpsh.exit_time
                FROM
                    wip_part_station_history wpsh 
                    JOIN wip_parts wp ON wp.id = wpsh.wip_part_ID
                    JOIN lines l ON l.id = wpsh.line_id
                    JOIN line_stations ls ON wpsh.station_id = ls.station_id
                    JOIN MES_COMMON.dbo.operators o ON o.ID = wpsh.operator_ID                  
                WHERE
                    wp.used_by_module_ID = ".$module_ID."
                    AND line_type > 0
                    ORDER BY wpsh.entry_time ASC";

        $res->subassembly_info = $db->query($sql);


        // torque info
        $sql = "SELECT
                    ls.station_desc,
                    o.name AS operator_name,
                    msih.part_number,
                    msh.entry_time,
                    msih.complete_time,
                    i.prompt AS torque,
                    dbo.fn_getJSONIntegerValue(instr_options, 'torques_required') AS torque_count
                FROM
                    module_station_history msh
                    JOIN module_station_instruction_history AS msih ON msh.module_ID = msih.module_ID AND msh.station_ID = msih.station_ID
                    JOIN instructions i ON i.part_number = msih.part_number AND i.instr_order = msih.instr_order AND i.station_id = msih.station_id AND i.test_type in (
                        SELECT ID FROM test_types
                    )
                    JOIN line_stations ls ON ls.station_id = msih.station_id
                    JOIN test_types tt ON i.test_type = tt.ID
                    LEFT OUTER JOIN MES_COMMON.dbo.operators o ON msh.operator_ID = o.ID
                WHERE
                    msih.module_id = ".$module_ID."
                    AND tt.machine_type = 2
                ORDER BY
                    ls.station_order;";

        $res->torque_info = $db->query($sql);


        // // assembly components used
        // $sql = "SELECT DISTINCT
        //                 c.Part_Number,
        //                 COALESCE(mrp.part_desc, qad.pt_part + ':' + qad.pt_desc1 + ' ' + qad.pt_desc2, 'NOT FOUND') as Description,
        //                 c.Qty
        //             FROM
        //                 components_used c
        //                 LEFT OUTER  JOIN (SELECT DISTINCT part_number, part_desc FROM mrp_part_mstr) As mrp ON c.part_number = mrp.part_number
        //                 LEFT OUTER JOIN [TEAMMRP01].[REPL_225].[dbo].[v_NORPLAS_QAD_KL] qad ON c.part_number = qad.cp_cust_part
        //             WHERE
        //                 c.veh_id = '".$request['module_ID']."'
        //             ORDER BY
        //                 c.part_number";

        // $res->assembly_info = $db->query($sql);


        // // packout
        // $sql = "SELECT
        //             s.ID as 'Shipment Number',
        //             s.ship_time as [Shipment Time],
        //             RIGHT(CAST(sl.rack_order as varchar(10)), 3) as [Rack Order],
        //             permanent_rack_ID as [Rack Number],
        //             si.lane_position as [Slot],
        //             m.VIN,
        //             m.Sequence
        //         FROM
        //             shipments s
        //             LEFT OUTER JOIN shipment_lanes sl ON sl.shipment_id = s.ID
        //             JOIN shipment_items si ON sl.ID = si.lane_ID
        //             JOIN modules m ON si.mod_ID = m.ID
        //         WHERE
        //             sl.ID = (SELECT lane_ID FROM shipment_items WHERE mod_ID = '".$request['module_ID']."')
        //             and sl.module_line_id = m.line_id 
        //         ORDER BY
        //             si.lane_position";
        
        // $res->packout_all_info = $db->query($sql);



        //EOL Info
        $sql = "SELECT
                    e.ID,
                    e.test_end_time
                FROM
                    eol_test_history e
                WHERE
                    e.mod_ID = ".$module_ID;

        $res->eol_info = $db->query($sql);

        //EOL data for module found?
        if (count($res->eol_info) > 0) {

            //EOL Test History Info
            $sql = "SELECT
                        cs.label,
                        thv.value,
                        thv.uom,
                        thv.passed,
                        thv.board,
                        thv.channel
                    FROM
                        EOL_test_history_values thv
                        JOIN EOL_channel_setup cs ON thv.board = cs.board AND thv.channel = cs.channel AND cs.line_ID = ".$mod_line_ID."
                    WHERE
                        thv.EOL_test_ID = '".$res->eol_info[0]->ID."'
                    ORDER BY
                        thv.id,
                        thv.board,
                        thv.channel";

            $res->eol_test_info = $db->query($sql);

        } else {
            $res->eol_test_info = array();
        }

        // // shipping info
        // $sql = "SELECT
        //             s.ID,
        //             s.shipment_sequence,
        //             s.ship_time,
        //             s.dock_number,
        //             s.shipment_sequence
        //         FROM
        //             shipments s
        //             LEFT OUTER JOIN shipment_lanes sl ON sl.shipment_id = s.ID
        //             LEFT OUTER JOIN shipment_rack_groups srg ON sl.shipment_rack_group_id = srg.ID
        //             LEFT OUTER JOIN shipment_items si ON si.lane_ID = sl.id
        //         WHERE
        //             si.mod_ID  = ".$request['module_ID'];

        // $res->shipping_info = $db->query($sql);
        
        echo json_encode($res);

    }

?>