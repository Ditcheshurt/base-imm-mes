<?php
require ('load_config.php');
require ('load_template_engine.php');

if (! isset ( $_SESSION ['op_ID'] )) {
	$smarty->display ( './templates/bad_session.html' );
} else {
	// load page based on type
	// switch($_REQUEST['type']) {
	// case "stations":
	// $smarty->display('./templates/options/stations.html');
	// break;
	// case "printers":
	// $smarty->display('./templates/options/printers.html');
	// break;
	// case "shift_times":
	// $smarty->display('./templates/options/shift_times.html');
	// break;
	// case "instructions":
	// $smarty->display('./templates/options/instructions.html');
	// break;
	// case "parts":
	// $smarty->display('./templates/options/parts.html');
	// break;
	// case "departments":
	// $smarty->display('./templates/options/departments.html');
	// break;
	// case "andon_station_parts":
	// $smarty->display('./templates/options/andon_station_parts.html');
	// break;
	// case "shifts":
	// $smarty->display('./templates/options/shifts.html');
	// break;
	
	// case "machines":
	// $smarty->display('./templates/options/machines.html');
	// break;
	// case "machine_types":
	// $smarty->display('./templates/options/machine_types.html');
	// break;
	// case "tools":
	// $smarty->display('./templates/options/tools.html');
	// break;
	// case "downtime_codes":
	// $smarty->display('./templates/options/downtime_codes.html');
	// break;
	// case "defect_codes":
	// $smarty->display('./templates/options/defect_codes.html');
	// break;
	
	// //OJT & alerts
	// case "ojt_jobs":
	// $smarty->display('./templates/options/ojt_jobs.html');
	// break;
	// case "alerts":
	// $smarty->display('./templates/options/alerts.html');
	// break;
	
	// case "defects":
	// $smarty->display('./templates/options/defects.html');
	// break;
	// case "downtime_codes":
	// $smarty->display('./templates/options/downtime_codes.html');
	// break;
	
	// }
	
	$smarty->display ( './templates/options/' . $_REQUEST ['type'] . '.html' );
}

?>