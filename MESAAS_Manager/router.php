<?php
require('load_config.php');
require('load_template_engine.php');

$smarty->assign('globals', $GLOBALS['APP_CONFIG']);

if (strpos($_REQUEST['type'], 'reports') === false && !isset($_SESSION['op_ID'])) {
	$smarty->display('./templates/bad_session.html');
} else {
	$smarty->display($_REQUEST['type'] . '.html');
}

?>